
BMesh-RS
========

This is an experimental port of Blender's BMesh C-API to Rust,
with the aim of keeping equivalent performance and memory usage.

- See the `BMesh design introduction
  <https://wiki.blender.org/index.php/Dev:Source/Modeling/BMesh/Design>`__
  page for an overview of the API.
- See the `C source code <http://developer.blender.org/diffusion/B/browse/master/source/blender/bmesh>`__.

This is a work in progress,
currently most development is being done in the ``intern/bmesh/`` and ``app_engine`` crates,
to get tests working on various parts of the API.

The initial plan is to:

- Port over the C-API as-is
  *(where function names and arguments are written in Rust but follow the C-API conventions)*.
- Wrap the C-style API by adding methods to structs,
  providing a more elegant interface.

  This wrapper in-lines functions to avoid adding overhead.


Safety
------

Given the inter-linking nature of a mesh API which stores topological connectivity,
using abstractions such as ``Rc`` adds memory overhead which limits the complexity
of geometry which can be edited at once.

For this reason, ``bmesh-rs`` relies on unsafe raw-pointers,
the intention that developers are not frequently manipulating pointers directly
and can rely on API's that give stable access to mesh editing operations.


Motivation
----------

Once the BMesh API is ported to Rust, it may be used as a basis for 3D-modelling software,
which can take advantage of Rust's safety options on an application-wide basis,
where ``bmesh-rs`` is only a single component.


Directory Layout
----------------

- ``extern/``
  Crates in this directory are stand-alone, complete and generally useful.

  .. note::

     There is an exception
     ``plain_ptr`` crate is used by multiple crates in this directory,
     However in general inter-dependence's are to be avoided.

- ``extern/util_macros/``
  Utilities for maintenance and porting.
- ``extern/plain_ptr/``
  Convenient access for raw-pointers with zero-overhead.
- ``extern/mempool/``
  Memory pool for storing geometry elements (vertices, edges and faces).
- ``extern/polyfill_2d/``
  Small geometry library, implementations of higher level geometry algorithms.
- ``math_misc/``
  Small math library, used by bmesh for vector and matrix math.

- ``intern/``
  Crates in this directory are internal to this project,
  only proving functionality as needed, not necessarily useful general purpose tasks.

  They may use ``extern`` crates as needed.
- ``intern/bmesh/``
  Main source location for mesh editing functions and tests.
- ``intern/pxbuf/``
  Image API, providing convenient manipulation of 32bit RGBA images, with sub-modules for:

  - Color blending.
  - Basic drawing (limited to primitive shapes, lines rectangles).

  Intended for real-time display, favoring speed over accuracy,
  if high quality versions of functions are needed,
  they can be added as separate functions as needed.

Test Application

- ``app_engine``
  The back-end for an application, handles events and window drawing.
- ``app_driver_headless``
  Test for the ``app_engine`` that just writes out the viewport.
- ``app_driver_sdl``
  Experimental application that uses SDL for windowing.

Non-source directories.

- ``dev_utils/`` utilities for maintenance and porting.


Building
--------

BMesh
^^^^^

Currently there are only tests, which can be run from the ``intern/bmesh`` directory.

.. code-block:: sh

   cd intern/bmesh
   cargo test

See ``intern/bmesh/tests/*.rs`` for test code showing examples of API use.


Application
^^^^^^^^^^^

To use the test application, run:

.. code-block:: sh

   cd app_driver_sdl
   cargo run --release

.. note::

   There is also a Wayland driver,
   but its currently quite slow.


Work in Progress
----------------

Currently this is still very much work-in-progress.

- Custom data (UV's, vertex colors etc...) is currently stubbed in ``custom_data.rs``.
- ``/* STUB */`` is used for functions not yet ported over.
- ``RS-TODO`` is used to declare TODO's for the C to Rust port of BMesh.
- Many comments are still C/C++ ``doxygen`` formatted.
