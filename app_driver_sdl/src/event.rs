// Licensed: GPL v2+

// use sdl2::event::Event;
// use sdl2::keyboard::Keycode;

use sdl2;

use app_engine::prelude::*;
use app_engine::wm;

use ::std::collections::BTreeMap;

fn sdl_event_convert_mouse_button(
    button: sdl2::mouse::MouseButton,
) -> wm::event_system::PointerButton {
    use app_engine::wm::event_system::PointerButton;
    use sdl2::mouse::MouseButton;

    match button {
        MouseButton::Left =>    { PointerButton::LEFT },
        MouseButton::Right =>   { PointerButton::RIGHT },
        MouseButton::Middle =>  { PointerButton::MIDDLE },
        MouseButton::X1 =>      { PointerButton::BUTTON_4 },
        MouseButton::X2 =>      { PointerButton::BUTTON_5 },
        // XXX
        MouseButton::Unknown => { PointerButton::LEFT },
    }
}

fn sdl_event_convert_mouse_coords(
    win: *const Win, x: i32, y: i32,
) -> [f64; 2] {
    // ymin is always zero, no need to get the size.
    let size_y = unsafe { (*win).welem_root.rect.ymax };
    return [x as f64, (size_y - y) as f64];
}

fn sdl_event_convert_key_mod(
    keymod: sdl2::keyboard::Mod,
) -> wm::event_system::KeyMod {
    use app_engine::wm::event_system::KeyMod;
    let mask =
        (KeyMod::LCTRL  | KeyMod::RCTRL  |
         KeyMod::LALT   | KeyMod::RALT   |
         KeyMod::LSHIFT | KeyMod::RSHIFT |
         KeyMod::LGUI   | KeyMod::RGUI).0;
    return wm::event_system::KeyMod((keymod.bits() as u16) & mask);
}

fn sdl_event_convert_scan_code(
    scan_code: sdl2::keyboard::Scancode,
) -> Option<wm::event_system::ScanCode> {

    use app_engine::wm::event_system::ScanCode;
    use sdl2::keyboard::Scancode;

    // TODO, there are quite some unknown vars still!
    let wm_code = match scan_code {
        Scancode::A => { ScanCode::A },
        Scancode::B => { ScanCode::B },
        Scancode::C => { ScanCode::C },
        Scancode::D => { ScanCode::D },
        Scancode::E => { ScanCode::E },
        Scancode::F => { ScanCode::F },
        Scancode::G => { ScanCode::G },
        Scancode::H => { ScanCode::H },
        Scancode::I => { ScanCode::I },
        Scancode::J => { ScanCode::J },
        Scancode::K => { ScanCode::K },
        Scancode::L => { ScanCode::L },
        Scancode::M => { ScanCode::M },
        Scancode::N => { ScanCode::N },
        Scancode::O => { ScanCode::O },
        Scancode::P => { ScanCode::P },
        Scancode::Q => { ScanCode::Q },
        Scancode::R => { ScanCode::R },
        Scancode::S => { ScanCode::S },
        Scancode::T => { ScanCode::T },
        Scancode::U => { ScanCode::U },
        Scancode::V => { ScanCode::V },
        Scancode::W => { ScanCode::W },
        Scancode::X => { ScanCode::X },
        Scancode::Y => { ScanCode::Y },
        Scancode::Z => { ScanCode::Z },
        Scancode::Num1 => { ScanCode::NUM_1 },
        Scancode::Num2 => { ScanCode::NUM_2 },
        Scancode::Num3 => { ScanCode::NUM_3 },
        Scancode::Num4 => { ScanCode::NUM_4 },
        Scancode::Num5 => { ScanCode::NUM_5 },
        Scancode::Num6 => { ScanCode::NUM_6 },
        Scancode::Num7 => { ScanCode::NUM_7 },
        Scancode::Num8 => { ScanCode::NUM_8 },
        Scancode::Num9 => { ScanCode::NUM_9 },
        Scancode::Num0 => { ScanCode::NUM_0 },

        Scancode::Return => { ScanCode::RET },
        Scancode::Escape => { ScanCode::ESC },
        Scancode::Backspace => { ScanCode::BACKSPACE },
        Scancode::Tab => { ScanCode::TAB },
        Scancode::Space => { ScanCode::SPACE },
        Scancode::Minus => { ScanCode::MINUS },
        Scancode::Equals => { ScanCode::EQUAL },
        Scancode::LeftBracket => { ScanCode::LEFT_BRACKET },
        Scancode::RightBracket => { ScanCode::RIGHT_BRACKET },
        Scancode::Backslash => { ScanCode::BACKSLASH },
        Scancode::NonUsHash => { ScanCode::UNKNOWN },
        Scancode::Semicolon => { ScanCode::SEMICOLON },
        Scancode::Apostrophe => { ScanCode::UNKNOWN },
        Scancode::Grave => { ScanCode::ACCENT_GRAVE },
        Scancode::Comma => { ScanCode::COMMA },
        Scancode::Period => { ScanCode::PERIOD },
        Scancode::Slash => { ScanCode::SLASH },
        Scancode::CapsLock => { ScanCode::CAPS_LOCK },

        Scancode::F1 => { ScanCode::F1 },
        Scancode::F2 => { ScanCode::F2 },
        Scancode::F3 => { ScanCode::F3 },
        Scancode::F4 => { ScanCode::F4 },
        Scancode::F5 => { ScanCode::F5 },
        Scancode::F6 => { ScanCode::F6 },
        Scancode::F7 => { ScanCode::F7 },
        Scancode::F8 => { ScanCode::F8 },
        Scancode::F9 => { ScanCode::F9 },
        Scancode::F10 => { ScanCode::F10 },
        Scancode::F11 => { ScanCode::F11 },
        Scancode::F12 => { ScanCode::F12 },

        Scancode::PrintScreen => { ScanCode::UNKNOWN },
        Scancode::ScrollLock => { ScanCode::SCROLL_LOCK },

        Scancode::Pause => { ScanCode::PAUSE },
        Scancode::Insert => { ScanCode::INSERT },
        Scancode::Home => { ScanCode::HOME },
        Scancode::PageUp => { ScanCode::PAGE_UP },
        Scancode::Delete => { ScanCode::DEL },
        Scancode::End => { ScanCode::END },
        Scancode::PageDown => { ScanCode::PAGE_DOWN },

        Scancode::Right => { ScanCode::RIGHT_ARROW },
        Scancode::Left => { ScanCode::LEFT_ARROW },
        Scancode::Down => { ScanCode::DOWN_ARROW },
        Scancode::Up => { ScanCode::UP_ARROW },

        Scancode::NumLockClear => { ScanCode::UNKNOWN },
        Scancode::KpDivide => { ScanCode::PAD_SLASH },
        Scancode::KpMultiply => { ScanCode::PAD_ASTER },
        Scancode::KpMinus => { ScanCode::PAD_MINUS },
        Scancode::KpPlus => { ScanCode::PAD_PLUS },
        Scancode::KpEnter => { ScanCode::PAD_ENTER },

        Scancode::Kp1 => { ScanCode::PAD_1 },
        Scancode::Kp2 => { ScanCode::PAD_2 },
        Scancode::Kp3 => { ScanCode::PAD_3 },
        Scancode::Kp4 => { ScanCode::PAD_4 },
        Scancode::Kp5 => { ScanCode::PAD_5 },
        Scancode::Kp6 => { ScanCode::PAD_6 },
        Scancode::Kp7 => { ScanCode::PAD_7 },
        Scancode::Kp8 => { ScanCode::PAD_8 },
        Scancode::Kp9 => { ScanCode::PAD_9 },
        Scancode::Kp0 => { ScanCode::PAD_0 },

        Scancode::KpPeriod => { ScanCode::PAD_PERIOD },

        Scancode::NonUsBackslash => { ScanCode::UNKNOWN },
        Scancode::Application => { ScanCode::UNKNOWN },
        Scancode::Power => { ScanCode::UNKNOWN },
        Scancode::KpEquals => { ScanCode::UNKNOWN },

        Scancode::F13 => { ScanCode::F13 },
        Scancode::F14 => { ScanCode::F14 },
        Scancode::F15 => { ScanCode::F15 },
        Scancode::F16 => { ScanCode::F16 },
        Scancode::F17 => { ScanCode::F17 },
        Scancode::F18 => { ScanCode::F18 },
        Scancode::F19 => { ScanCode::F19 },

        Scancode::F20 => { ScanCode::UNKNOWN },
        Scancode::F21 => { ScanCode::UNKNOWN },
        Scancode::F22 => { ScanCode::UNKNOWN },
        Scancode::F23 => { ScanCode::UNKNOWN },
        Scancode::F24 => { ScanCode::UNKNOWN },
        Scancode::Execute => { ScanCode::UNKNOWN },
        Scancode::Help => { ScanCode::UNKNOWN },
        Scancode::Menu => { ScanCode::UNKNOWN },
        Scancode::Select => { ScanCode::UNKNOWN },
        Scancode::Stop => { ScanCode::UNKNOWN },
        Scancode::Again => { ScanCode::UNKNOWN },
        Scancode::Undo => { ScanCode::UNKNOWN },
        Scancode::Cut => { ScanCode::UNKNOWN },
        Scancode::Copy => { ScanCode::UNKNOWN },
        Scancode::Paste => { ScanCode::UNKNOWN },
        Scancode::Find => { ScanCode::UNKNOWN },
        Scancode::Mute => { ScanCode::UNKNOWN },
        Scancode::VolumeUp => { ScanCode::UNKNOWN },
        Scancode::VolumeDown => { ScanCode::UNKNOWN },
        Scancode::KpComma => { ScanCode::UNKNOWN },
        Scancode::KpEqualsAS400 => { ScanCode::UNKNOWN },
        Scancode::International1 => { ScanCode::UNKNOWN },
        Scancode::International2 => { ScanCode::UNKNOWN },
        Scancode::International3 => { ScanCode::UNKNOWN },
        Scancode::International4 => { ScanCode::UNKNOWN },
        Scancode::International5 => { ScanCode::UNKNOWN },
        Scancode::International6 => { ScanCode::UNKNOWN },
        Scancode::International7 => { ScanCode::UNKNOWN },
        Scancode::International8 => { ScanCode::UNKNOWN },
        Scancode::International9 => { ScanCode::UNKNOWN },
        Scancode::Lang1 => { ScanCode::UNKNOWN },
        Scancode::Lang2 => { ScanCode::UNKNOWN },
        Scancode::Lang3 => { ScanCode::UNKNOWN },
        Scancode::Lang4 => { ScanCode::UNKNOWN },
        Scancode::Lang5 => { ScanCode::UNKNOWN },
        Scancode::Lang6 => { ScanCode::UNKNOWN },
        Scancode::Lang7 => { ScanCode::UNKNOWN },
        Scancode::Lang8 => { ScanCode::UNKNOWN },
        Scancode::Lang9 => { ScanCode::UNKNOWN },
        Scancode::AltErase => { ScanCode::UNKNOWN },
        Scancode::SysReq => { ScanCode::UNKNOWN },
        Scancode::Cancel => { ScanCode::UNKNOWN },
        Scancode::Clear => { ScanCode::UNKNOWN },
        Scancode::Prior => { ScanCode::UNKNOWN },
        Scancode::Return2 => { ScanCode::UNKNOWN },
        Scancode::Separator => { ScanCode::UNKNOWN },
        Scancode::Out => { ScanCode::UNKNOWN },
        Scancode::Oper => { ScanCode::UNKNOWN },
        Scancode::ClearAgain => { ScanCode::UNKNOWN },
        Scancode::CrSel => { ScanCode::UNKNOWN },
        Scancode::ExSel => { ScanCode::UNKNOWN },
        Scancode::Kp00 => { ScanCode::UNKNOWN },
        Scancode::Kp000 => { ScanCode::UNKNOWN },
        Scancode::ThousandsSeparator => { ScanCode::UNKNOWN },
        Scancode::DecimalSeparator => { ScanCode::UNKNOWN },
        Scancode::CurrencyUnit => { ScanCode::UNKNOWN },
        Scancode::CurrencySubUnit => { ScanCode::UNKNOWN },
        Scancode::KpLeftParen => { ScanCode::UNKNOWN },
        Scancode::KpRightParen => { ScanCode::UNKNOWN },
        Scancode::KpLeftBrace => { ScanCode::UNKNOWN },
        Scancode::KpRightBrace => { ScanCode::UNKNOWN },
        Scancode::KpTab => { ScanCode::UNKNOWN },
        Scancode::KpBackspace => { ScanCode::UNKNOWN },
        Scancode::KpA => { ScanCode::UNKNOWN },
        Scancode::KpB => { ScanCode::UNKNOWN },
        Scancode::KpC => { ScanCode::UNKNOWN },
        Scancode::KpD => { ScanCode::UNKNOWN },
        Scancode::KpE => { ScanCode::UNKNOWN },
        Scancode::KpF => { ScanCode::UNKNOWN },
        Scancode::KpXor => { ScanCode::UNKNOWN },
        Scancode::KpPower => { ScanCode::UNKNOWN },
        Scancode::KpPercent => { ScanCode::UNKNOWN },
        Scancode::KpLess => { ScanCode::UNKNOWN },
        Scancode::KpGreater => { ScanCode::UNKNOWN },
        Scancode::KpAmpersand => { ScanCode::UNKNOWN },
        Scancode::KpDblAmpersand => { ScanCode::UNKNOWN },
        Scancode::KpVerticalBar => { ScanCode::UNKNOWN },
        Scancode::KpDblVerticalBar => { ScanCode::UNKNOWN },
        Scancode::KpColon => { ScanCode::UNKNOWN },
        Scancode::KpHash => { ScanCode::UNKNOWN },
        Scancode::KpSpace => { ScanCode::UNKNOWN },
        Scancode::KpAt => { ScanCode::UNKNOWN },
        Scancode::KpExclam => { ScanCode::UNKNOWN },
        Scancode::KpMemStore => { ScanCode::UNKNOWN },
        Scancode::KpMemRecall => { ScanCode::UNKNOWN },
        Scancode::KpMemClear => { ScanCode::UNKNOWN },
        Scancode::KpMemAdd => { ScanCode::UNKNOWN },
        Scancode::KpMemSubtract => { ScanCode::UNKNOWN },
        Scancode::KpMemMultiply => { ScanCode::UNKNOWN },
        Scancode::KpMemDivide => { ScanCode::UNKNOWN },
        Scancode::KpPlusMinus => { ScanCode::UNKNOWN },
        Scancode::KpClear => { ScanCode::UNKNOWN },
        Scancode::KpClearEntry => { ScanCode::UNKNOWN },
        Scancode::KpBinary => { ScanCode::UNKNOWN },
        Scancode::KpOctal => { ScanCode::UNKNOWN },
        Scancode::KpDecimal => { ScanCode::UNKNOWN },
        Scancode::KpHexadecimal => { ScanCode::UNKNOWN },

        Scancode::LCtrl => { ScanCode::LEFT_CTRL },
        Scancode::LShift => { ScanCode::LEFT_SHIFT },
        Scancode::LAlt => { ScanCode::LEFT_ALT },
        Scancode::LGui => { ScanCode::LEFT_SUPER },
        Scancode::RCtrl => { ScanCode::RIGHT_CTRL },
        Scancode::RShift => { ScanCode::RIGHT_SHIFT },
        Scancode::RAlt => { ScanCode::RIGHT_ALT },
        Scancode::RGui => { ScanCode::RIGHT_SUPER },

        Scancode::Mode => { ScanCode::UNKNOWN },

        Scancode::AudioNext => { ScanCode::MEDIA_NEXT },
        Scancode::AudioPrev => { ScanCode::MEDIA_PREV },
        Scancode::AudioStop => { ScanCode::MEDIA_STOP },
        Scancode::AudioPlay => { ScanCode::MEDIA_PLAY },

        Scancode::AudioMute => { ScanCode::UNKNOWN },
        Scancode::MediaSelect => { ScanCode::UNKNOWN },
        Scancode::Www => { ScanCode::UNKNOWN },
        Scancode::Mail => { ScanCode::UNKNOWN },
        Scancode::Calculator => { ScanCode::UNKNOWN },
        Scancode::Computer => { ScanCode::UNKNOWN },
        Scancode::AcSearch => { ScanCode::UNKNOWN },
        Scancode::AcHome => { ScanCode::UNKNOWN },
        Scancode::AcBack => { ScanCode::UNKNOWN },
        Scancode::AcForward => { ScanCode::UNKNOWN },
        Scancode::AcStop => { ScanCode::UNKNOWN },
        Scancode::AcRefresh => { ScanCode::UNKNOWN },
        Scancode::AcBookmarks => { ScanCode::UNKNOWN },
        Scancode::BrightnessDown => { ScanCode::UNKNOWN },
        Scancode::BrightnessUp => { ScanCode::UNKNOWN },
        Scancode::DisplaySwitch => { ScanCode::UNKNOWN },
        Scancode::KbdIllumToggle => { ScanCode::UNKNOWN },
        Scancode::KbdIllumDown => { ScanCode::UNKNOWN },
        Scancode::KbdIllumUp => { ScanCode::UNKNOWN },
        Scancode::Eject => { ScanCode::UNKNOWN },
        Scancode::Sleep => { ScanCode::UNKNOWN },
        Scancode::App1 => { ScanCode::UNKNOWN },
        Scancode::App2 => { ScanCode::UNKNOWN },
        Scancode::Num => { ScanCode::NUM_LOCK },
    };

    if wm_code != ScanCode::UNKNOWN {
        return Some(wm_code);
    } else {
        return None;
    }
}

pub fn sdl_event_as_wm_event(
    wm_win_id_map: &BTreeMap<u32, *const Win>,
    wm_event_state: &mut wm::event_system::EventState,
    sdl_event_generic: sdl2::event::Event,
) -> Option<(*const Win, wm::event_system::Event)> {

    match sdl_event_generic {
        sdl2::event::Event::MouseMotion { window_id, x, y, .. } => {
            let win = *wm_win_id_map.get(&window_id).unwrap();
            wm_event_state.co = sdl_event_convert_mouse_coords(win, x, y);
            return Some((
                win,
                wm::event_system::Event::new(
                    wm::event_system::EventID::PointerMotion {},
                    wm_event_state.clone(),
                )),
            );
        },
        sdl2::event::Event::MouseButtonUp { window_id, x, y, mouse_btn, .. } => {
            let win = *wm_win_id_map.get(&window_id).unwrap();
            wm_event_state.co = sdl_event_convert_mouse_coords(win, x, y);
            return Some((
                win,
                wm::event_system::Event::new(
                    wm::event_system::EventID::PointerButton {
                        button: sdl_event_convert_mouse_button(mouse_btn),
                        value: false,
                    },
                    wm_event_state.clone(),
                )
            ));
        },
        sdl2::event::Event::MouseButtonDown { window_id, x, y, mouse_btn, .. } => {
            let win = *wm_win_id_map.get(&window_id).unwrap();
            wm_event_state.co = sdl_event_convert_mouse_coords(win, x, y);
            return Some((
                win,
                wm::event_system::Event::new(
                    wm::event_system::EventID::PointerButton {
                        button: sdl_event_convert_mouse_button(mouse_btn),
                        value: true,
                    },
                    wm_event_state.clone(),
                )
            ));
        },
        sdl2::event::Event::KeyDown { window_id, scancode, keymod, repeat, .. } => {
            let win = *wm_win_id_map.get(&window_id).unwrap();
            wm_event_state.key_mod = sdl_event_convert_key_mod(keymod);
            if let Some(sdl_scancode) = scancode {
                if let Some(wm_scan_code) = sdl_event_convert_scan_code(sdl_scancode) {

                    let wm_event_state_clone = wm_event_state.clone();
                    wm_event_state.scan_keys.insert(wm_scan_code);
                    wm_event_state.update_map_keys();

                    return Some((
                        win,
                        wm::event_system::Event::new(
                            wm::event_system::EventID::Keyboard {
                                scan_code: wm_scan_code,
                                value: true,
                                repeat: repeat,
                            },
                            wm_event_state_clone,
                        )
                    ));
                }
            }
        },
        sdl2::event::Event::KeyUp { window_id, scancode, keymod, .. } => {
            let win = *wm_win_id_map.get(&window_id).unwrap();
            wm_event_state.key_mod = sdl_event_convert_key_mod(keymod);
            if let Some(sdl_scancode) = scancode {
                if let Some(wm_scan_code) = sdl_event_convert_scan_code(sdl_scancode) {

                    wm_event_state.scan_keys.remove(&wm_scan_code);
                    let wm_event_state_clone = wm_event_state.clone();
                    wm_event_state.update_map_keys();

                    return Some((
                        win,
                        wm::event_system::Event::new(
                            wm::event_system::EventID::Keyboard {
                                scan_code: wm_scan_code,
                                value: true,
                                repeat: false,
                            },
                            wm_event_state_clone,
                        )
                    ));
                }
            }
        },
        sdl2::event::Event::Window { window_id, win_event, .. } => {
            let win = *wm_win_id_map.get(&window_id).unwrap();
            match win_event {
                // ... or Resize, for now it doesn't matter, we get both.
                sdl2::event::WindowEvent::SizeChanged(x, y) => {
                    let wm_event_state_clone = wm_event_state.clone();
                    return Some((
                        win,
                        wm::event_system::Event::new(
                            wm::event_system::EventID::Window {
                                action: wm::event_system::WindowActionType::Resize(
                                    x as u32, y as u32),
                            },
                            wm_event_state_clone,
                        )
                    ));
                },
                sdl2::event::WindowEvent::Close => {
                    // TODO
                }
                _ => {
                    // lots more, ignore for now
                    println!("Window event: {:?} {:?}", sdl_event_generic, win_event);
                }
            }
        },

        _ => {
            println!("Unhandled event: {:?}", sdl_event_generic);
        },
    }

    return None;
}
