// Licensed: GPL v2+

// AppEngine
extern crate app_engine;
use app_engine::wm;

extern crate util_macros;

// SDL
extern crate sdl2;

use sdl2::event::Event;
// use sdl2::keyboard::Keycode;

// Own modules
mod event;

use ::std::collections::BTreeMap;

fn main() {
    // The window-manager may resize this immediately, its only used for floating windows.
    // We may make this a preference, add command line access
    // or inspect the main monitor size.
    //
    // For now leave as-is.
    //
    let size: [u32; 2] = [1024, 768];

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window(
        "BMesh-RS", size[0], size[1],
    ).position_centered().resizable().build().unwrap();
    let window_id = window.id();

    let mut renderer = window.renderer().accelerated().build().unwrap();

    renderer.set_draw_color(sdl2::pixels::Color::RGBA(0, 0, 0, 255));

    // let mut timer = sdl_context.timer().unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();


    // Application Init
    use app_engine::prelude::*;
    use app_engine::exports::{
        PtrMut,
        PtrConst,
        null_mut,
    };

    let mut app = App::new();

    app_env_new(&mut app);
    {
        // Figure out mutability stuff!
        let mut env = app.envs.pop().unwrap();
        app_startup_env_init(&mut env);
        app.envs.push(env);

        app_startup_prefs_init(&mut app);
    }
    // ugly but works
    app_window_new(&mut app, &[size[0] as i32, size[1] as i32]);
    let win = app.windows.last().unwrap();

    let mut win_id_map = BTreeMap::<u32, *const Win>::new();
    win_id_map.insert(window_id, win as *const Win);

    let mut image = win.pxbuf_create();


    // TODO, we could init with initial mouse coords for eg, for now dont!
    let mut event_state = wm::event_system::EventState::default();


    // let mut temp_surface = sdl2::surface::Surface::new(
    //     size[0], size[1], sdl2::pixels::PixelFormatEnum::BGRA8888).unwrap();

    // let mut texture = renderer.create_texture_from_surface(&temp_surface).unwrap();
    // No need to blend with the existing data.
    let mut texture = renderer.create_texture(
        sdl2::pixels::PixelFormatEnum::ABGR8888,
        sdl2::render::TextureAccess::Static,
        size[0], size[1],
    ).unwrap();

    texture.set_blend_mode(sdl2::render::BlendMode::None);

    let mut ctx = AppContext {
        app: PtrConst(&app),
        window: PtrConst(win),

        env: PtrMut(app.envs.last_mut().unwrap()),
        wchain: null_mut(),
    };

    let mut running = true;
    while running {
        for event in event_pump.poll_iter() {
            match event {
                /*
                Event::Quit {..} | Event::KeyDown {keycode: Some(Keycode::Escape), ..} => {
                    running = false;
                },
                */
                Event::Quit {..} => {
                    running = false;
                },
                _ => {
                    if let Some((
                        wm_win,
                        wm_event,
                    )) = event::sdl_event_as_wm_event(
                        &win_id_map,
                        &mut event_state,
                        event)
                    {
                        ctx.window = PtrConst(wm_win);
                        unsafe {
                            // How to solve? We want to run an update only!
                            // This modifies welem_root but not the window.
                            // FIXME!
                            &mut *(((win as *const Win) as usize) as *mut Win)
                        }.welem_root.handle_event(PtrMut(&mut ctx), &wm_event, null_mut());

                    }
                }
            }
        }

        // let ticks = timer.ticks();

        {
            // let win_mut = app.windows.last_mut().unwrap();
            unsafe {
                // How to solve? We want to run an update only!
                // This modifies welem_root but not the window.
                // FIXME!
                &mut *(((win as *const Win) as usize) as *mut Win)
            }.welem_root.update(PtrMut(&mut ctx));
        }

        use std::time::SystemTime;
        let now = SystemTime::now();

        // ensure image size is correct
        {
            let size_new = win.welem_root.rect.size();
            if {
                let size_old = image.size().clone();
                size_old != size_new
            } {
                image.reshape_uninitialized(&size_new);

                drop(texture);
                texture = renderer.create_texture(
                    sdl2::pixels::PixelFormatEnum::ABGR8888,
                    sdl2::render::TextureAccess::Static,
                    size_new[0] as u32, size_new[1] as u32,
                ).unwrap();
                println!("Resizing!");

            }
        }


        win.welem_root.composite(&mut image);

        let sec = match now.elapsed() {
            Ok(elapsed) => {
                (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0)
            }
            Err(_e) => {
                -1.0
            }
        };

        if false {
            println!("FPS: {}", 1.0 / sec);
        }

        match texture.update(None, image.as_slice_u8(), (image.size()[0] * 4) as usize) {
            Ok(()) => {
                renderer.set_blend_mode(sdl2::render::BlendMode::None);
                // Flip on the Y axis (applications pixels start at the bottom)
                match renderer.copy_ex(&texture, None, None, 0.0, None, false, true) {
                    Ok(()) => {
                        renderer.present();
                    },
                    Err(e) => {
                        println!("Failed to draw {:?}!", e);
                    }
                }
            },
            Err(e) => {
                println!("Failed to update {:?}!", e);
            }
        }

        // Do something so we can see animation!
        /*
        {
            let mut env = app.envs.last_mut().unwrap();
            app_env_temp_view3d_spin(&mut env);
        }
        */

        // std::thread::sleep(Duration::from_millis(100));
    }
}
