// Licensed: GPL v2+

// use sdl2::event::Event;
// use sdl2::keyboard::Keycode;

// use sdl2;

use app_engine::prelude::*;
use app_engine::wm;

use wayland_client::protocol::{
    wl_keyboard,
    wl_pointer,
};

fn wl_event_convert_mouse_button(
    button: u32,
) -> wm::event_system::PointerButton {
    use app_engine::wm::event_system::PointerButton;
    match button {
        272 => { PointerButton::LEFT },
        273 => { PointerButton::RIGHT },
        274 => { PointerButton::MIDDLE },

        // 275 => { PointerButton::BUTTON_4 },
        // 276 => { PointerButton::BUTTON_5 },

        // XXX, sorry, my middle mouse is physically broken and I
        // didn't find a way to remap it in Wayland yet!
        276 => { PointerButton::MIDDLE },

        _ => { PointerButton::LEFT },
    }
}

fn wl_event_convert_mouse_coords(
    win: *const Win, x: f64, y: f64,
) -> [f64; 2] {
    let size_y = unsafe {
        (*win).welem_root.rect.size_y()
    };
    return [x as f64, (size_y as f64 - y) as f64];
}

fn wl_event_convert_scan_code(
    scan_code: u32,
) -> Option<wm::event_system::ScanCode> {

    use app_engine::wm::event_system::ScanCode;

    let wm_code = match scan_code {
        1 => { ScanCode::ESC },
        2 => { ScanCode::NUM_1 },
        3 => { ScanCode::NUM_2 },
        4 => { ScanCode::NUM_3 },
        5 => { ScanCode::NUM_4 },
        6 => { ScanCode::NUM_5 },
        7 => { ScanCode::NUM_6 },
        8 => { ScanCode::NUM_7 },
        9 => { ScanCode::NUM_8 },
        10 => { ScanCode::NUM_9 },
        11 => { ScanCode::NUM_0 },
        12 => { ScanCode::MINUS },
        13 => { ScanCode::EQUAL },
        14 => { ScanCode::BACKSPACE },
        15 => { ScanCode::TAB },
        16 => { ScanCode::Q },
        17 => { ScanCode::W },
        18 => { ScanCode::E },
        19 => { ScanCode::R },
        20 => { ScanCode::T },
        21 => { ScanCode::Y },
        22 => { ScanCode::U },
        23 => { ScanCode::I },
        24 => { ScanCode::O },
        25 => { ScanCode::P },
        26 => { ScanCode::LEFT_BRACKET },
        27 => { ScanCode::RIGHT_BRACKET },
        28 => { ScanCode::RET },
        29 => { ScanCode::LEFT_CTRL },
        30 => { ScanCode::A },
        31 => { ScanCode::S },
        32 => { ScanCode::D },
        33 => { ScanCode::F },
        34 => { ScanCode::G },
        35 => { ScanCode::H },
        36 => { ScanCode::J },
        37 => { ScanCode::K },
        38 => { ScanCode::L },
        39 => { ScanCode::SEMICOLON },
        40 => { ScanCode::QUOTE },
        41 => { ScanCode::ACCENT_GRAVE },
        42 => { ScanCode::LEFT_SHIFT },
        43 => { ScanCode::BACKSLASH },
        44 => { ScanCode::Z },
        45 => { ScanCode::X },
        46 => { ScanCode::C },
        47 => { ScanCode::V },
        48 => { ScanCode::B },
        49 => { ScanCode::N },
        50 => { ScanCode::M },
        51 => { ScanCode::COMMA },
        52 => { ScanCode::PERIOD },
        53 => { ScanCode::SLASH },
        54 => { ScanCode::RIGHT_SHIFT },
        55 => { ScanCode::PAD_ASTER },
        56 => { ScanCode::LEFT_ALT },
        57 => { ScanCode::SPACE },
        58 => { ScanCode::CAPS_LOCK },
        59 => { ScanCode::F1 },
        60 => { ScanCode::F2 },
        61 => { ScanCode::F3 },
        62 => { ScanCode::F4 },
        63 => { ScanCode::F5 },
        64 => { ScanCode::F6 },
        65 => { ScanCode::F7 },
        66 => { ScanCode::F8 },
        67 => { ScanCode::F9 },
        68 => { ScanCode::F10 },
        69 => { ScanCode::NUM_LOCK },
        70 => { ScanCode::SCROLL_LOCK },
        71 => { ScanCode::PAD_7 },
        72 => { ScanCode::PAD_8 },
        73 => { ScanCode::PAD_9 },
        74 => { ScanCode::PAD_MINUS },
        75 => { ScanCode::PAD_4 },
        76 => { ScanCode::PAD_5 },
        77 => { ScanCode::PAD_6 },
        78 => { ScanCode::PAD_PLUS },
        79 => { ScanCode::PAD_1 },
        80 => { ScanCode::PAD_2 },
        81 => { ScanCode::PAD_3 },
        82 => { ScanCode::PAD_0 },
        83 => { ScanCode::PAD_PERIOD },

        // undefined
        84 => { ScanCode::UNKNOWN },
        85 => { ScanCode::UNKNOWN },
        86 => { ScanCode::UNKNOWN },

        87 => { ScanCode::F11 },
        88 => { ScanCode::F12 },

        // undefined
        89 => { ScanCode::UNKNOWN },
        90 => { ScanCode::UNKNOWN },
        91 => { ScanCode::UNKNOWN },
        92 => { ScanCode::UNKNOWN },
        93 => { ScanCode::UNKNOWN },
        94 => { ScanCode::UNKNOWN },
        95 => { ScanCode::UNKNOWN },

        96 => { ScanCode::PAD_ENTER },
        97 => { ScanCode::RIGHT_CTRL },
        98 => { ScanCode::PAD_SLASH },
        99 => { ScanCode::PRINT_SCREEN },
        100 => { ScanCode::LEFT_ALT },

        101 => { ScanCode::UNKNOWN },

        102 => { ScanCode::HOME },
        103 => { ScanCode::UP_ARROW },
        104 => { ScanCode::PAGE_UP },
        105 => { ScanCode::LEFT_ARROW },
        106 => { ScanCode::RIGHT_ARROW },
        107 => { ScanCode::END },
        108 => { ScanCode::DOWN_ARROW },
        109 => { ScanCode::PAGE_DOWN },

        110 => { ScanCode::INSERT },
        111 => { ScanCode::DEL },

        112 => { ScanCode::UNKNOWN },
        113 => { ScanCode::UNKNOWN },
        114 => { ScanCode::UNKNOWN },
        115 => { ScanCode::UNKNOWN },
        116 => { ScanCode::UNKNOWN },
        117 => { ScanCode::UNKNOWN },
        118 => { ScanCode::UNKNOWN },

        119 => { ScanCode::PAUSE },

        126 => { ScanCode::RIGHT_SUPER },

        _ => { ScanCode::UNKNOWN },
    };

    if wm_code != ScanCode::UNKNOWN {
        return Some(wm_code);
    } else {
        return None;
    }
}


pub fn wl_event_as_wm_event_pointer_button(
    wm_event_state: &mut wm::event_system::EventState,
    button: u32,
    state: wl_pointer::ButtonState,
) -> Option<(*const Win, wm::event_system::Event)> {

    let value = match state {
        wl_pointer::ButtonState::Pressed => { true },
        wl_pointer::ButtonState::Released => { false },
    };

    return Some((
        ::std::ptr::null_mut(), // XXX, win!
        wm::event_system::Event::new(
            wm::event_system::EventID::PointerButton {
                button: wl_event_convert_mouse_button(button),
                value: value,
            },
            wm_event_state.clone(),
        )
    ));
}

pub fn wl_event_as_wm_event_pointer_motion(
    win: *const Win,
    wm_event_state: &mut wm::event_system::EventState,
    x: f64, y: f64,
) -> Option<(*const Win, wm::event_system::Event)> {
    wm_event_state.co = wl_event_convert_mouse_coords(win, x, y);
    return Some((
        ::std::ptr::null_mut(), // XXX, win!
        wm::event_system::Event::new(
            wm::event_system::EventID::PointerMotion{},
            wm_event_state.clone(),
        )),
    );
}

pub fn wl_event_as_wm_event_kb(
    wm_event_state: &mut wm::event_system::EventState,
    key: u32,
    state: wl_keyboard::KeyState,
) -> Option<(*const Win, wm::event_system::Event)> {

    if let Some(wm_scan_code) = wl_event_convert_scan_code(key) {
        let repeat = false;
        let value = match state {
            wl_keyboard::KeyState::Pressed => { true },
            wl_keyboard::KeyState::Released => { false },
        };

        let wm_event_state_clone = {
            if value {
                let wm_event_state_clone = wm_event_state.clone();
                wm_event_state.scan_keys.insert(wm_scan_code);
                wm_event_state.update_map_keys();
                wm_event_state_clone
            } else {
                wm_event_state.scan_keys.remove(&wm_scan_code);
                let wm_event_state_clone = wm_event_state.clone();
                wm_event_state.update_map_keys();
                wm_event_state_clone
            }
        };

        return Some((
            ::std::ptr::null(),
            wm::event_system::Event::new(
                wm::event_system::EventID::Keyboard {
                    scan_code: wm_scan_code,
                    value: value,
                    repeat: repeat,
                },
                wm_event_state_clone,
            )
        ));
    }

    return None;
}
