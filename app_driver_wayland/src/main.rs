// Licensed: GPL v2+

// Warning, this is very slow - while it works its currently more proof
// the AppEngine can be made to work against different interfaces.
//
// For real use, the SDL version has better performance.
//
// Even so, keep this functional for now since the issues can be resolved later on.
//
// TODO: mmap the file to avoid using file writing to update pixel data!
//
// TODO: window resizing.
// TODO: window decorations.
//

// AppEngine
extern crate app_engine;
use app_engine::wm;
use app_engine::exports::{
    PtrMut,
    PtrConst,
    null_mut,
};

#[macro_use]
extern crate util_macros;

// Own modules
mod event;

// Wayland
#[macro_use]
extern crate wayland_client;

extern crate tempfile;

extern crate byteorder;
use byteorder::{WriteBytesExt, NativeEndian};

use std::io::{Write, Seek};
use std::os::unix::io::AsRawFd;

use wayland_client::{EventQueueHandle, EnvHandler};
use wayland_client::protocol::{
    wl_compositor,
    wl_keyboard,
    wl_pointer,
    wl_seat,
    wl_shell,
    wl_shell_surface,
    wl_shm,
    wl_surface,
};

wayland_env!(WaylandEnv,
    compositor: wl_compositor::WlCompositor,
    seat: wl_seat::WlSeat,
    shell: wl_shell::WlShell,
    shm: wl_shm::WlShm
);

// Would be local vars, except that we need to access from handlers.
struct MyVars {
    event_state: *mut wm::event_system::EventState,
    ctx: *mut app_engine::prelude::AppContext,
}

struct AppLocalHandlerData {
    vars: *mut MyVars,
}

// XXX, this is wrong!
// (just to get it compiling).
unsafe impl Send for AppLocalHandlerData {}
unsafe impl Sync for AppLocalHandlerData {}


impl wl_shell_surface::Handler for AppLocalHandlerData {
    fn ping(
        &mut self,
        _: &mut EventQueueHandle,
        me: &wl_shell_surface::WlShellSurface,
        serial: u32,
    ) {
        me.pong(serial);
    }

    // we ignore the other methods in this example, by default they do nothing
}

declare_handler!(
    AppLocalHandlerData,
    wl_shell_surface::Handler,
    wl_shell_surface::WlShellSurface);

use app_engine::prelude::*;
use app_engine::wm::event_system::Event;

fn wl_handle_event_helper(ctx: &mut AppContext, wm_event: Event) {
    // XXX, we should get the event from the window
    // ctx.window = wm_win;
    unsafe {
        // How to solve? We want to run an update only!
        // This modifies welem_root but not the window.
        // FIXME!
        &mut *((ctx.window.as_ptr() as usize) as *mut Win)
    }.welem_root.handle_event(PtrMut(ctx), &wm_event, null_mut());
}

impl wl_pointer::Handler for AppLocalHandlerData {
    fn enter(&mut self, _: &mut EventQueueHandle, _me: &wl_pointer::WlPointer,
             _serial: u32, _surface: &wl_surface::WlSurface, surface_x: f64, surface_y: f64) {
        println!("Pointer entered surface at ({},{}).", surface_x, surface_y);
    }
    fn leave(&mut self, _: &mut EventQueueHandle, _me: &wl_pointer::WlPointer,
             _serial: u32, _surface: &wl_surface::WlSurface) {
        println!("Pointer left surface.");
    }
    fn motion(
        &mut self, _: &mut EventQueueHandle, _me: &wl_pointer::WlPointer,
        _time: u32, surface_x: f64, surface_y: f64,
    ) {
        let mut ctx = unsafe { &mut *(*self.vars).ctx };
        let mut event_state = unsafe { &mut *(*self.vars).event_state };

        if let Some((
            _wm_win,
            wm_event,
        )) = event::wl_event_as_wm_event_pointer_motion(
            ctx.window.as_ptr(),
            &mut event_state,
            surface_x, surface_y,
        ) {
            wl_handle_event_helper(ctx, wm_event);
        }
    }

    fn button(
        &mut self, _: &mut EventQueueHandle, _me: &wl_pointer::WlPointer,
        _serial: u32, _time: u32, button: u32, state: wl_pointer::ButtonState,
    ) {
        let mut ctx = unsafe { &mut *(*self.vars).ctx };
        let mut event_state = unsafe { &mut *(*self.vars).event_state };

        if let Some((
            _wm_win,
            wm_event,
        )) = event::wl_event_as_wm_event_pointer_button(
            &mut event_state,
            button, state,
        ) {
            wl_handle_event_helper(ctx, wm_event);
        }
    }

    // we ignore the other methods in this example, by default they do nothing
}

declare_handler!(
    AppLocalHandlerData,
    wl_pointer::Handler,
    wl_pointer::WlPointer);


impl wl_keyboard::Handler for AppLocalHandlerData {
    fn enter(
        &mut self,
        _: &mut EventQueueHandle,
        _me: &wl_keyboard::WlKeyboard,
        // _format: wayland_client::protocol::wl_keyboard::KeymapFormat,
        _format: u32,
        _: &wl_surface::WlSurface,
        _: std::vec::Vec<u8>,
        // _fd: ::std::os::unix::io::RawFd,
        // _size: u32,
    ) {
        println!("Kb entered surface at ({},{}).", 0, 0);
    }
    fn leave(
        &mut self,
        _: &mut EventQueueHandle,
        _me: &wl_keyboard::WlKeyboard,
        _serial: u32,
        _surface: &wl_surface::WlSurface,
    ) {
        println!("Kb left surface.");
    }

    fn key(
        &mut self,
        _evqh: &mut EventQueueHandle,
        _proxy: &wl_keyboard::WlKeyboard,
        _serial: u32,
        _time: u32,
        key: u32,
        state: wl_keyboard::KeyState,
    ) {
        println!("Key {} {:?} !", key, state);

        // hardcode escape
        if key == 1 {
            panic!();
        }

        let mut ctx = unsafe { &mut *(*self.vars).ctx };
        let mut event_state = unsafe { &mut *(*self.vars).event_state };

        if let Some((
            _wm_win,
            wm_event,
        )) = event::wl_event_as_wm_event_kb(
            &mut event_state,
            key, state,
        ) {
            // ctx.window = wm_win;
            wl_handle_event_helper(ctx, wm_event);
        }
    }

    fn modifiers(
        &mut self,
        _evqh: &mut EventQueueHandle,
        _proxy: &wl_keyboard::WlKeyboard,
        _serial: u32,
        _mods_depressed: u32,
        _mods_latched: u32,
        _mods_locked: u32,
        _group: u32,
    ) {
        println!("Key Mod!");
    }

    // we ignore the other methods in this example, by default they do nothing
}

declare_handler!(
    AppLocalHandlerData,
    wl_keyboard::Handler,
    wl_keyboard::WlKeyboard);

use ::std::collections::BTreeMap;


fn main() {
    let (display, mut event_queue) = match wayland_client::default_connect() {
        Ok(ret) => ret,
        Err(e) => panic!("Cannot connect to wayland server: {:?}", e)
    };

    event_queue.add_handler(EnvHandler::<WaylandEnv>::new());
    let registry = display.get_registry().expect("Display cannot be already destroyed.");
    event_queue.register::<_, EnvHandler<WaylandEnv>>(&registry,0);
    event_queue.sync_roundtrip().unwrap();


    let size: [u32; 2] = [1024, 768];
    let buf_x = size[0];
    let buf_y = size[1];

    // -----
    // Application Init
    use app_engine::prelude::*;
    let mut app = App::new();

    app_env_new(&mut app);
    {
        // Figure out mutability stuff!
        let mut env = app.envs.pop().unwrap();
        app_startup_env_init(&mut env);
        app.envs.push(env);

        app_startup_prefs_init(&mut app);
    }
    // ugly but works
    app_window_new(&mut app, &[size[0] as i32, size[1] as i32]);
    let win = app.windows.last().unwrap();

    let window_id = 0;
    let mut win_id_map = BTreeMap::<u32, *const Win>::new();
    win_id_map.insert(window_id, win as *const Win);

    let mut image = win.pxbuf_create();

    // TODO, we could init with initial mouse coords for eg, for now dont!
    let mut event_state = wm::event_system::EventState::default();
    // -----

    let mut ctx = AppContext {
        app: PtrConst(&app),
        window: PtrConst(win),

        env: PtrMut(app.envs.last_mut().unwrap()),
        wchain: null_mut(),
    };

    // -----

    // create a tempfile to write the conents of the window on
    let mut tmp = tempfile::tempfile().ok().expect("Unable to create a tempfile.");
    // write the contents to it, lets put a nice color gradient
    for _ in 0..(buf_x * buf_y) {
        let _ = tmp.write_u32::<NativeEndian>(0);
    }
    let _ = tmp.flush();

    // prepare the wayland surface
    let (shell_surface, surface, buffer, pointer, keyboard) = {
        // introduce a new scope because .state() borrows the event_queue
        let state = event_queue.state();
        // retrieve the EnvHandler
        let env = state.get_handler::<EnvHandler<WaylandEnv>>(0);
        let surface = env.compositor.create_surface().expect(
            "Compositor cannot be destroyed");
        let shell_surface = env.shell.get_shell_surface(&surface).expect(
            "Shell cannot be destroyed");

        let pool = env.shm.create_pool(
            tmp.as_raw_fd(), (buf_x * buf_y * 4) as i32,
        ).expect("Shm cannot be destroyed");
        // match a buffer on the part we wrote on
        let buffer = pool.create_buffer(
            0, buf_x as i32, buf_y as i32, (buf_x * 4) as i32, wl_shm::Format::Xrgb8888,
        ).expect("The pool cannot be already dead");

        // make our surface as a toplevel one
        shell_surface.set_toplevel();
        // attach the buffer to it
        surface.attach(Some(&buffer), 0, 0);
        // commit
        surface.commit();

        let pointer = env.seat.get_pointer().expect("Seat cannot be destroyed.");
        let keyboard = env.seat.get_keyboard().expect("Seat cannot be destroyed.");

        // we can let the other objects go out of scope
        // their associated wyland objects won't automatically be destroyed
        // and we don't need them in this example
        (shell_surface, surface, buffer, pointer, keyboard)
    };

    let mut my_vars = MyVars {
        ctx: &mut ctx as *mut _,
        event_state: &mut event_state as *mut _,
    };

    event_queue.add_handler(AppLocalHandlerData {
        vars: &mut my_vars as *mut _,
    });

    event_queue.register::<_, AppLocalHandlerData>(&shell_surface, 1);
    event_queue.register::<_, AppLocalHandlerData>(&pointer, 1);
    event_queue.register::<_, AppLocalHandlerData>(&keyboard, 1);

    loop {
        println!("Loop");

        event_queue.dispatch().unwrap();

        // ensure image size is correct
        {
            let size_new = win.welem_root.rect.size();
            if {
                let size_old = image.size().clone();
                size_old[0] != size_new[0] ||
                size_old[1] != size_new[1]
            } {
                image.reshape_uninitialized(&size_new);
                println!("Resizing!");
            }
        }

        unsafe {
            // How to solve? We want to run an update only!
            // This modifies welem_root but not the window.
            // FIXME!
            &mut *(((win as *const Win) as usize) as *mut Win)
        }.welem_root.update(PtrMut(&mut ctx));

        {
            win.welem_root.composite(&mut image);
        }

        surface.damage(0, 0, size[0] as i32, size[1] as i32);

        let _ = tmp.seek(std::io::SeekFrom::Start(0));
        if false {
            // Currently upside down!!!
            for rgba in image.as_slice_u32() {
                let _ = tmp.write_u32::<NativeEndian>(*rgba);
            }
        } else {
            // Grr, we need to reverse scan-lines and RGB components, but not A.
            // Currently upside down!!!

            #[inline]
            fn color_rgba_from_bgra(color: u32) -> u32 {
                let r = (color >>  0) & 0xFF;
                let g = (color >>  8) & 0xFF;
                let b = (color >> 16) & 0xFF;
                let a = (color >> 24) & 0xFF;
                // swap r & b
                return {
                    (b <<  0) |
                    (g <<  8) |
                    (r << 16) |
                    (a << 24)
                };
            }

            let _ = tmp.seek(std::io::SeekFrom::Start(0));
            let slice = image.as_slice_u32();

            for row in slice.chunks(size[0] as usize).rev() {
                // probably better to convert a row
                // to write the entire row at once!
                for rgba in row {
                    let color = color_rgba_from_bgra(*rgba);
                    let _ = tmp.write_u32::<NativeEndian>(color);
                }
            }
        }
        let _ = tmp.flush();


        surface.attach(Some(&buffer), 0, 0);
        surface.commit();

        display.flush().unwrap();

        // Do something so we can see animation!
        {
            // let mut env = app.envs.last_mut().unwrap();
            // app_env_temp_view3d_spin(&mut env);
        }
    }
}
