// Licensed: Apache 2.0

mod slice_ext;

pub use slice_ext::{
    SliceIndexOfExt,
};
