// Licensed: Apache 2.0

pub trait SliceIndexOfExt<T> {
    unsafe fn index_of_unchecked(&self, item: &T) -> usize;
    fn index_of(&self, item: &T) -> Option<usize>;
}

impl <T> SliceIndexOfExt<T> for [T] {

    unsafe fn index_of_unchecked(&self, item: &T) -> usize {
        // don't support array of '()'.
        debug_assert!(::std::mem::size_of::<T>() != 0);
        let a = item as *const _ as usize;
        let b = self.get_unchecked(0) as *const _ as usize;
        return a.wrapping_sub(b) / ::std::mem::size_of::<T>()
    }

    fn index_of(&self, item: &T) -> Option<usize> {
        let index = unsafe { self.index_of_unchecked(item) };
        if index < self.len() {
            return Some(index);
        } else {
            return None;
        }
    }

}
