// Licensed: Apache 2.0

///
/// General note:
///
/// While this is intended to be an optimized drawing library.
/// The first goal is to get basics working so there is some room for fine tuning still.
///
///
/// API Conventions
/// ===============
///
/// Naming:
///
/// * All functions use the 'draw_*' prefix.
/// * Functions don't clip input unless they use the '_clip' suffix.
///
/// Source:
///
/// * Implementations of drawing algorithms are stored in 'draw_impl'.
/// * All methods are declared in the root level (Here).
///

extern crate pxbuf;

#[macro_use]
mod util_macros;
mod draw_impl;


use pxbuf::{
    PxBuf,
    color,
};

use pxbuf::util::{
    index_from_pos,
};

pub trait PxBufDraw {
    // Pixel
    fn draw_pixel(&mut self, color: u32, pos: &[i32; 2]);
    fn draw_pixel_size(&mut self, color: u32, radius: i32, pos: &[i32; 2]);

    // Line
    fn draw_line(
        &mut self, color: u32,
        pos_src: &[i32; 2], pos_dst: &[i32; 2]
    );
    fn draw_line_clip(
        &mut self, color: u32,
        pos_src: &[i32; 2], pos_dst: &[i32; 2]
    );
    fn draw_line_width_clip(
        &mut self, color: u32,
        pos_src: &[i32; 2], pos_dst: &[i32; 2], width: i32
    );
    fn draw_line_blend_clip(
        &mut self,
        color_src: u32, color_dst: u32,
        pos_src: &[i32; 2], pos_dst: &[i32; 2],
    );
    fn draw_line_width_blend_clip(
        &mut self,
        color_src: u32, color_dst: u32,
        pos_src: &[i32; 2], pos_dst: &[i32; 2],
        width: i32,
    );

    // Rectangle
    fn draw_rect(&mut self, color: u32, pos_min: &[i32; 2], pos_max: &[i32; 2]);
    fn draw_rect_clip(&mut self, color: u32, pos_min: &[i32; 2], pos_max: &[i32; 2]);

    // Triangle
    fn draw_tri(&mut self, color: u32, p1: &[i32; 2], p2: &[i32; 2], p3: &[i32; 2]);
    fn draw_tri_clip(&mut self, color: u32, p1: &[i32; 2], p2: &[i32; 2], p3: &[i32; 2]);
}

impl PxBufDraw for PxBuf {

    // ------------------------------------------------------------------------
    // Pixel

    #[inline]
    fn draw_pixel(&mut self, color: u32, pos: &[i32; 2]) {
        self.pixel_set(color, pos);
    }

    #[inline]
    fn draw_pixel_size(&mut self, color: u32, radius: i32, pos: &[i32; 2]) {
        debug_assert!(pos[0] >= 0 && pos[0] < self.size()[0] &&
                      pos[1] >= 0 && pos[1] < self.size()[1]);
        debug_assert!(radius >= 0);

        use ::std::cmp::{min, max};

        let xmax = self.size()[0] - 1;
        let ymax = self.size()[1] - 1;

        self.draw_rect(
            color,
            &[max(0, pos[0] - radius),
              max(0, pos[1] - radius)],
            &[min(xmax, pos[0] + radius),
              min(ymax, pos[1] + radius)],
        );
    }

    // ------------------------------------------------------------------------
    // Line

    fn draw_line(
        &mut self,
        color: u32,
        pos_src: &[i32; 2],
        pos_dst: &[i32; 2],
    ) {
        use draw_impl::plot_line_noclip::plot_line_noclip_v2v2i;
        let w = self.size()[0];
        let h = self.size()[1];

        let mut callback = |x, y| {
            debug_assert!(x >= 0 && x < w &&
                          y >= 0 && y < h);
            // having checked the bounds, we _know_ the index is in-range.
            self.pixel_set(color, &[x, y]);
        };

        plot_line_noclip_v2v2i(
            pos_src, pos_dst,
            &mut callback,
        );
    }

    fn draw_line_clip(
        &mut self,
        color: u32,
        pos_src: &[i32; 2],
        pos_dst: &[i32; 2],
    ) {
        use draw_impl::plot_line_clip::plot_line_clip_v2v2i;
        let w = self.size()[0];
        let h = self.size()[1];

        let mut callback = |x, y| {
            debug_assert!(x >= 0 && x < w &&
                          y >= 0 && y < h);
            // having checked the bounds, we _know_ the index is in-range.
            self.pixel_set(color, &[x, y]);
        };

        plot_line_clip_v2v2i(
            pos_src, pos_dst,
            0, 0, w - 1, h - 1,
            &mut callback,
        );
    }

    fn draw_line_width_clip(
        &mut self,
        color: u32,
        pos_src: &[i32; 2],
        pos_dst: &[i32; 2],
        width: i32,
    ) {
        use draw_impl::plot_line_width_clip::plot_line_width_clip_v2v2i;
        let w = self.size()[0];
        let h = self.size()[1];

        let mut callback = |x, y, _x_center, _y_center| {
            debug_assert!(x >= 0 && x < w &&
                          y >= 0 && y < h);
            // having checked the bounds, we _know_ the index is in-range.
            self.pixel_set(color, &[x, y]);
        };

        plot_line_width_clip_v2v2i(
            pos_src, pos_dst,
            width,
            0, 0, w - 1, h - 1,
            &mut callback,
        );
    }

    // Blended versions of line drawing functions
    fn draw_line_blend_clip(
        &mut self,
        color_src: u32,
        color_dst: u32,
        pos_src: &[i32; 2],
        pos_dst: &[i32; 2],
    ) {
        use draw_impl::plot_line_clip::plot_line_clip_v2v2i;
        let w = self.size()[0];
        let h = self.size()[1];

        let mut dx = pos_src[0] - pos_dst[0];
        let mut dy = pos_src[1] - pos_dst[1];

        if dx.abs() > dy.abs() {
            if dx == 0 {
                dx = 1; // just avoid divide by zero
            }
            let mut callback = |x, y| { // blend on X axis
                debug_assert!(x >= 0 && x < w &&
                              y >= 0 && y < h);
                let blend = (((pos_src[0] - x) * 255) / dx) as u32;
                let color = color::rgb_interp(color_src, color_dst, blend);
                self.pixel_set(color, &[x, y]);
            };
            // same as below
            plot_line_clip_v2v2i(pos_src, pos_dst, 0, 0, w - 1, h - 1, &mut callback);
        } else {
            if dy == 0 {
                dy = 1; // just avoid divide by zero
            }
            let mut callback = |x, y| { // blend on Y axis
                debug_assert!(x >= 0 && x < w &&
                              y >= 0 && y < h);
                let blend = (((pos_src[1] - y) * 255) / dy) as u32;
                let color = color::rgb_interp(color_src, color_dst, blend);
                self.pixel_set(color, &[x, y]);
            };
            // same as above
            plot_line_clip_v2v2i(pos_src, pos_dst, 0, 0, w - 1, h - 1, &mut callback);
        }
    }

    fn draw_line_width_blend_clip(
        &mut self,
        color_src: u32,
        color_dst: u32,
        pos_src: &[i32; 2],
        pos_dst: &[i32; 2],
        width: i32,
    ) {
        use draw_impl::plot_line_width_clip::plot_line_width_clip_v2v2i;
        let w = self.size()[0];
        let h = self.size()[1];

        let mut dx = pos_src[0] - pos_dst[0];
        let mut dy = pos_src[1] - pos_dst[1];

        if dx.abs() > dy.abs() {
            if dx == 0 {
                dx = 1; // just avoid divide by zero
            }
            // so we don't over blend
            let mut callback = |x, y, x_center, _y_center| { // blend on X axis
                debug_assert!(x >= 0 && x < w &&
                              y >= 0 && y < h);
                let blend = (((pos_src[0] - x_center) * 255) / dx) as u32;
                let color = color::rgb_interp(color_src, color_dst, blend);
                self.pixel_set(color, &[x, y]);
            };
            // same as below
            plot_line_width_clip_v2v2i(
                pos_src, pos_dst, width, 0, 0, w - 1, h - 1, &mut callback);
        } else {
            if dy == 0 {
                dy = 1; // just avoid divide by zero
            }
            // so we don't over blend
            let mut callback = |x, y, _x_center, y_center| { // blend on Y axis
                debug_assert!(x >= 0 && x < w &&
                              y >= 0 && y < h);
                // grr, min needed because wide line may exceed range on axis
                let blend = (((pos_src[1] - y_center) * 255) / dy) as u32;
                let color = color::rgb_interp(color_src, color_dst, blend);
                self.pixel_set(color, &[x, y]);
            };
            // same as above
            plot_line_width_clip_v2v2i(
                pos_src, pos_dst, width, 0, 0, w - 1, h - 1, &mut callback);
        }
    }

    // ------------------------------------------------------------------------
    // Rectangle

    // for callers that do own checking
    fn draw_rect(&mut self, color: u32, pos_min: &[i32; 2], pos_max: &[i32; 2]) {
        let xmin = pos_min[0];
        let ymin = pos_min[1];
        let xmax = pos_max[0];
        let ymax = pos_max[1];
        let x_span = ((xmax - xmin) + 1) as usize; // add one because slices aren't inclusive
        let mut index = index_from_pos(&[xmin, ymin], self.size());
        let index_stride = self.size()[0] as usize;
        let rect = self.rect_mut();
        for _y in ymin..(ymax + 1) {
            // no need to slow down debug builds further
            // debug_assert!(index == index_from_pos(&[xmin, y], self.size()));
            for p in &mut rect[index..(index + x_span)] {
                *p = color;
            }
            index += index_stride;
        }
    }

    fn draw_rect_clip(&mut self, color: u32, pos_min: &[i32; 2], pos_max: &[i32; 2]) {
        use ::std::cmp::{min, max};
        let xmin = max(pos_min[0], 0);
        let ymin = max(pos_min[1], 0);
        let xmax = min(pos_max[0], self.size()[0] - 1);
        let ymax = min(pos_max[1], self.size()[1] - 1);

        if xmin <= xmax && ymin <= ymax {
            self.draw_rect(color, &[xmin, ymin], &[xmax, ymax]);
        }
    }

    // ------------------------------------------------------------------------
    // Triangle

    fn draw_tri(&mut self, color: u32, p1: &[i32; 2], p2: &[i32; 2], p3: &[i32; 2]) {
        use draw_impl::fill_tri_noclip::draw_tri_noclip_v2i;

        // could use 'x_end + 1', but this overlaps adjacent triangles
        let alpha = color::rgba_chan_value_u32(color, 3);
        if alpha != 0xff {
            let mut callback = |x, y, x_end| {
                // could use 'x_end + 1', but this overlaps adjacent triangles
                for p in self.pixel_xslice_get_mut(x, x_end, y) {
                    *p = color::rgb_interp(*p, color, alpha);
                }
            };
            draw_tri_noclip_v2i(
                p1, p2, p3,
                &mut callback,
            );
        } else {
            let mut callback = |x, y, x_end| {
                for p in self.pixel_xslice_get_mut(x, x_end, y) {
                    *p = color;
                }
            };
            draw_tri_noclip_v2i(
                p1, p2, p3,
                &mut callback,
            );
        }
    }

    fn draw_tri_clip(&mut self, color: u32, p1: &[i32; 2], p2: &[i32; 2], p3: &[i32; 2]) {
        use draw_impl::fill_tri_clip::draw_tri_clip_v2i;
        let w = self.size()[0];
        let h = self.size()[1];

        // could use 'x_end + 1', but this overlaps adjacent triangles
        let alpha = color::rgba_chan_value_u32(color, 3);
        if alpha != 0xff {
            let mut callback = |x, y, x_end| {
                for p in self.pixel_xslice_get_mut(x, x_end, y) {
                    *p = color::rgb_interp(*p, color, alpha);
                }
            };
            draw_tri_clip_v2i(
                p1, p2, p3,
                0, 0, w - 1, h - 1,
                &mut callback,
            );
        } else {
            let mut callback = |x, y, x_end| {
                for p in self.pixel_xslice_get_mut(x, x_end, y) {
                    *p = color;
                }
            };
            draw_tri_clip_v2i(
                p1, p2, p3,
                0, 0, w - 1, h - 1,
                &mut callback,
            );
        }
    }
}
