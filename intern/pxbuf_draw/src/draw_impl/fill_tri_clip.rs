// Licensed: Apache 2.0

/// Fill a triangle
///
/// Note: changes here should be applied to 'fill_tri_noclip.rs'
/// since quite some logic is shared.
///
/// See this file for comments on implementation.
/// Clipping adds some extra checks, otherwise same logic.
///

#[inline]
fn inv_slope(a: &[i32; 2], b: &[i32; 2]) -> f64 {
    return (a[0] - b[0]) as f64 /
           (a[1] - b[1]) as f64;
}

/// *---*
///  \ /
///   *
///
fn draw_tri_flat_max<F>(
    p: &[i32; 2],
    mut y_max: i32,
    inv_slope1: f64,
    inv_slope2: f64,
    clip_xmin: i32,
    clip_ymin: i32,
    clip_xmax: i32,
    clip_ymax: i32,
    callback: &mut F)
    where
    F: FnMut(i32, i32, i32),
{
    use ::std::cmp::{min, max};
    let mut cur_x1 = p[0] as f64;
    let mut cur_x2 = cur_x1;

    // start-end inclusive
    let mut y_min = p[1];

    // skip last part of the loop
    if y_max > clip_ymax {
        y_max = clip_ymax;
    }
    // skip first part (more involved)
    if y_min < clip_ymin {
        let y_delta = (clip_ymin - y_min) as f64;
        assert!(y_delta >= 0.0);
        cur_x1 += inv_slope1 * y_delta;
        cur_x2 += inv_slope2 * y_delta;
        y_min = clip_ymin;
    }

    for scanline_y in y_min..(y_max + 1) {
        let cur_x1i = cur_x1 as i32;
        let cur_x2i = cur_x2 as i32;
        if cur_x1i <= clip_xmax && cur_x2i >= clip_xmin {
            callback(max(cur_x1i, clip_xmin), scanline_y, min(cur_x2i, clip_xmax));
        }
        cur_x1 += inv_slope1;
        cur_x2 += inv_slope2;
    }
}

///   *
///  / \
/// *---*
///
fn draw_tri_flat_min<F>(
    p: &[i32; 2],
    mut y_min: i32,
    inv_slope1: f64,
    inv_slope2: f64,
    clip_xmin: i32,
    clip_ymin: i32,
    clip_xmax: i32,
    clip_ymax: i32,
    callback: &mut F)
    where
    F: FnMut(i32, i32, i32),
{
    use ::std::cmp::{min, max};
    let mut cur_x1 = p[0] as f64;
    let mut cur_x2 = cur_x1;

    // start-end inclusive
    let mut y_max = p[1];

    // skip last part of the loop
    if y_min < clip_ymin {
        y_min = clip_ymin;
    }
    // skip first part (more involved)
    if y_max > clip_ymax {
        let y_delta = (y_max - clip_ymax) as f64;
        assert!(y_delta >= 0.0);
        cur_x1 -= inv_slope1 * y_delta;
        cur_x2 -= inv_slope2 * y_delta;
        y_max = clip_ymax;
    }

    for scanline_y in (y_min..(y_max + 1)).rev() {
        let cur_x1i = cur_x1 as i32;
        let cur_x2i = cur_x2 as i32;
        if cur_x1i <= clip_xmax && cur_x2i >= clip_xmin {
            callback(max(cur_x1i, clip_xmin), scanline_y, min(cur_x2i, clip_xmax));
        }
        cur_x1 -= inv_slope1;
        cur_x2 -= inv_slope2;
    }
}

pub fn draw_tri_clip_v2i<F>(
    p1: &[i32; 2],
    p2: &[i32; 2],
    p3: &[i32; 2],
    clip_xmin: i32,
    clip_ymin: i32,
    clip_xmax: i32,
    clip_ymax: i32,
    callback: &mut F)
    where
    F: FnMut(i32, i32, i32),
{
    // At first sort the three vertices by y-coordinate ascending so p1 is the top-most vertice
    let (p1, p2, p3) = order_args_by!(p1, p2, p3, [1]);

    // Simple bound-box check (clip-only)
    {
        if p1[1] > clip_ymax || p3[1] < clip_ymin {
            return;
        }

        let (x1, _x2, x3) = order_args!(p1[0], p2[0], p3[0]);
        if x1 > clip_xmax || x3 < clip_xmin {
            return;
        }
    }

    // debug_assert!(p1[1] <= p2[1] && p2[1] <= p3[1]);

    // Check for trivial case of bottom-flat triangle.
    if p2[1] == p3[1] {
        let (inv_slope1, inv_slope2) = order_args!(inv_slope(p2, p1), inv_slope(p3, p1));
        draw_tri_flat_max(
            p1, p2[1],
            inv_slope1, inv_slope2,
            clip_xmin,
            clip_ymin,
            clip_xmax,
            clip_ymax,
            callback);
    } else if p1[1] == p2[1] {
        // Check for trivial case of top-flat triangle.
        let (inv_slope2, inv_slope1) = order_args!(inv_slope(p3, p1), inv_slope(p3, p2));
        draw_tri_flat_min(
            p3, p2[1] + 1, // avoid overlap
            inv_slope1, inv_slope2,
            clip_xmin,
            clip_ymin,
            clip_xmax,
            clip_ymax,
            callback);
    } else {
        // General case - split the triangle in a top-flat and bottom-flat one.
        let inv_slope_p21 = inv_slope(p2, p1);
        let inv_slope_p31 = inv_slope(p3, p1);
        let inv_slope_p32 = inv_slope(p3, p2);

        // We can order once instead.
        // let (inv_slope1_max, inv_slope2_max) = order_args!(inv_slope_p21, inv_slope_p31);
        // let (inv_slope2_min, inv_slope1_min) = order_args!(inv_slope_p31, inv_slope_p32);
        let (
            inv_slope1_max, inv_slope2_max,
            inv_slope2_min, inv_slope1_min,
        ) = {
            if inv_slope_p21 < inv_slope_p31 {
                (inv_slope_p21, inv_slope_p31,
                 inv_slope_p31, inv_slope_p32)
            } else {
                (inv_slope_p31, inv_slope_p21,
                 inv_slope_p32, inv_slope_p31)
            }
        };

        if p2[1] >= clip_ymin {
            draw_tri_flat_max(
                p1, p2[1],
                inv_slope1_max,
                inv_slope2_max,
                clip_xmin,
                clip_ymin,
                clip_xmax,
                clip_ymax,
                callback);
        }

        if p2[1] <= clip_ymax {
            draw_tri_flat_min(
                p3, p2[1] + 1, // avoid overlap
                inv_slope1_min,
                inv_slope2_min,
                clip_xmin,
                clip_ymin,
                clip_xmax,
                clip_ymax,
                callback);
        }
    }
}
