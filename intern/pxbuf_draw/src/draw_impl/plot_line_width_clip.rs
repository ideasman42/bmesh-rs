// Licensed: Apache 2.0

/// Plot a line, wrapper for 'plot_line_clip_v2v2i'
///
/// Cheap trick, use _very_ simple line width trick:
/// just draw horizontal or vertical strips at each line segment.
///
/// Not at all accurate, but OK for basic on-screen display with fairly thin lines.
///
/// Later on we may want to investigate something better.
///

use ::std::cmp::{min, max};

pub fn plot_line_width_clip_v2v2i<F>(
    p1: &[i32; 2],
    p2: &[i32; 2],
    width: i32,
    clip_xmin: i32,
    clip_ymin: i32,
    clip_xmax: i32,
    clip_ymax: i32,
    callback: &mut F,
)
    where
    F: FnMut(i32, i32, i32, i32),
{
    let offset = (width) / 2;
    let dx = p1[0] - p2[0];
    let dy = p1[1] - p2[1];
    let dx_abs = dx.abs();
    let dy_abs = dy.abs();

    if dx_abs < dy_abs {
        let mut callback_wrapper_x = |x, y| {
            if y >= clip_ymin && y <= clip_ymax {
                let mut x_start = x - offset;
                let mut x_end = x_start + width;
                x_start = max(x_start, clip_xmin);
                x_end = min(x_end, clip_xmax);

                for x_sub in x_start..x_end {
                    callback(x_sub, y, x, y);
                }
            }
        };

        ::draw_impl::plot_line_clip::plot_line_clip_v2v2i(
            p1, p2,
            clip_xmin - offset, clip_ymin - offset,
            clip_xmax + offset, clip_ymax + offset,
            &mut callback_wrapper_x,
        );
    } else {
        let mut callback_wrapper_y = |x, y| {
            if x >= clip_xmin && x <= clip_xmax {
                let mut y_start = y - offset;
                let mut y_end = y_start + width;
                y_start = max(y_start, clip_ymin);
                y_end = min(y_end, clip_ymax);

                for y_sub in y_start..y_end {
                    callback(x, y_sub, x, y);
                }
            }
        };

        ::draw_impl::plot_line_clip::plot_line_clip_v2v2i(
            p1, p2,
            clip_xmin - offset, clip_ymin - offset,
            clip_xmax + offset, clip_ymax + offset,
            &mut callback_wrapper_y,
        );
    }
}
