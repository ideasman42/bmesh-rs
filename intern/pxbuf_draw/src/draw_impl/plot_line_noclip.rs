// Licensed: Apache 2.0

/// Plot a line, identical to clip version,
/// without clipping checks.

pub fn plot_line_noclip_v2v2i<F>(
    p1: &[i32; 2],
    p2: &[i32; 2],
    callback: &mut F)
    where
    F: FnMut(i32, i32),
{
    let mut x1 = p1[0];
    let mut y1 = p1[1];
    let mut x2 = p2[0];
    let mut y2 = p2[1];

    // Vertical line
    if x1 == x2 {
        if y1 <= y2 {
            for y in y1..(y2 + 1) {
                callback(x1, y);
            }
        } else {
            for y in (y2..(y1 + 1)).rev() {
                callback(x1, y);
            }
        }
        return;
    }

    // Horizontal line
    if y1 == y2 {

        if x1 <= x2 {
            for x in x1..(x2 + 1) {
                callback(x, y1);
            }
        } else {
            for x in x2..(x1 + 1) {
                callback(x, y1);
            }
        }
        return;
    }

    // Now simple cases are handled, perform diagonal line drawing
    let sign_x;
    let sign_y;

    if x1 < x2 {
        sign_x = 1;
    } else {
        sign_x = -1;

        // Invert sign, invert again right before plotting.
        x1 = -x1;
        x2 = -x2;
    }

    if y1 < y2 {
        sign_y = 1;
    } else {
        sign_y = -1;

        // Invert sign, invert again right before plotting.
        y1 = -y1;
        y2 = -y2;
    }

    let delta_x = x2 - x1;
    let delta_y = y2 - y1;

    let mut delta_x_step = 2 * delta_x;
    let mut delta_y_step = 2 * delta_y;

    // Plotting values
    let mut x_pos = x1;
    let mut y_pos = y1;

    if delta_x >= delta_y {
        let mut error = delta_y_step - delta_x;
        let mut x_pos_end = x2 + 1;

        if sign_y == -1 {
            y_pos = -y_pos
        }

        if sign_x == -1 {
            x_pos = -x_pos;
            x_pos_end = -x_pos_end;
        }

        delta_x_step -= delta_y_step;

        while x_pos != x_pos_end {
            callback(x_pos, y_pos);

            if error >= 0 {
                y_pos += sign_y;
                error -= delta_x_step;
            } else {
                error += delta_y_step;
            }

            x_pos += sign_x;
        }
    } else {
        // Line is steep '/' (delta_x < delta_y).
        // Same as previous block of code with swapped x/y axis.

        let mut error = delta_x_step - delta_y;
        let mut y_pos_end = y2 + 1;

        if sign_x == -1 {
            x_pos = -x_pos;
        }

        if sign_y == -1 {
            y_pos = -y_pos;
            y_pos_end = -y_pos_end;
        }

        delta_y_step -= delta_x_step;

        while y_pos != y_pos_end {
            callback(x_pos, y_pos);

            if error >= 0 {
                x_pos += sign_x;
                error -= delta_y_step;
            } else {
                error += delta_x_step;
            }

            y_pos += sign_y;
        }
    }
}
