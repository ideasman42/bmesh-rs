// Licensed: Apache 2.0

/// Fill a triangle
///
/// Standard algorithm,
/// See: http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html
///
/// Note: changes here should be applied to 'fill_tri_clip.rs'
/// since quite some logic is shared.
///
/// Changes to the basic implementation:
///
/// * Reuse slope calculation when drawing the second triangle.
/// * Don't calculate the 4th point at all for the triangle split.
/// * Order line drawing from left to right (minor detail).
/// * 1-pixel offsets are applied so adjacent triangles don't overlap.
///

#[inline]
fn inv_slope(a: &[i32; 2], b: &[i32; 2]) -> f64 {
    return (a[0] - b[0]) as f64 /
           (a[1] - b[1]) as f64;
}

/// *---*
///  \ /
///   *
///
fn draw_tri_flat_max<F>(
    p: &[i32; 2],
    y_max: i32,
    inv_slope1: f64,
    inv_slope2: f64,
    callback: &mut F)
    where
    F: FnMut(i32, i32, i32),
{
    let mut cur_x1 = p[0] as f64;
    let mut cur_x2 = cur_x1;

    // start-end inclusive
    let y_min = p[1];

    for scanline_y in y_min..(y_max + 1) {
        let cur_x1i = cur_x1 as i32;
        let cur_x2i = cur_x2 as i32;
        callback(cur_x1i, scanline_y, cur_x2i);
        cur_x1 += inv_slope1;
        cur_x2 += inv_slope2;
    }
}

///   *
///  / \
/// *---*
///
fn draw_tri_flat_min<F>(
    p: &[i32; 2],
    y_min: i32,
    inv_slope1: f64,
    inv_slope2: f64,
    callback: &mut F)
    where
    F: FnMut(i32, i32, i32),
{
    let mut cur_x1 = p[0] as f64;
    let mut cur_x2 = cur_x1;

    // start-end inclusive
    let y_max = p[1];

    for scanline_y in (y_min..(y_max + 1)).rev() {
        let cur_x1i = cur_x1 as i32;
        let cur_x2i = cur_x2 as i32;
        callback(cur_x1i, scanline_y, cur_x2i);
        cur_x1 -= inv_slope1;
        cur_x2 -= inv_slope2;
    }
}

pub fn draw_tri_noclip_v2i<F>(
    p1: &[i32; 2],
    p2: &[i32; 2],
    p3: &[i32; 2],
    callback: &mut F)
    where
    F: FnMut(i32, i32, i32),
{
    // At first sort the three vertices by y-coordinate ascending so p1 is the top-most vertice
    let (p1, p2, p3) = order_args_by!(p1, p2, p3, [1]);

    // debug_assert!(p1[1] <= p2[1] && p2[1] <= p3[1]);

    // Check for trivial case of bottom-flat triangle.
    if p2[1] == p3[1] {
        let (inv_slope1, inv_slope2) = order_args!(inv_slope(p2, p1), inv_slope(p3, p1));
        draw_tri_flat_max(
            p1, p2[1],
            inv_slope1, inv_slope2,
            callback);
    } else if p1[1] == p2[1] {
        // Check for trivial case of top-flat triangle.
        let (inv_slope2, inv_slope1) = order_args!(inv_slope(p3, p1), inv_slope(p3, p2));
        draw_tri_flat_min(
            p3, p2[1] + 1, // avoid overlap
            inv_slope1, inv_slope2,
            callback);
    } else {
        // General case - split the triangle in a top-flat and bottom-flat one.
        let inv_slope_p21 = inv_slope(p2, p1);
        let inv_slope_p31 = inv_slope(p3, p1);
        let inv_slope_p32 = inv_slope(p3, p2);

        // We can order once instead.
        // let (inv_slope1_max, inv_slope2_max) = order_args!(inv_slope_p21, inv_slope_p31);
        // let (inv_slope2_min, inv_slope1_min) = order_args!(inv_slope_p31, inv_slope_p32);
        let (
            inv_slope1_max, inv_slope2_max,
            inv_slope2_min, inv_slope1_min,
        ) = {
            if inv_slope_p21 < inv_slope_p31 {
                (inv_slope_p21, inv_slope_p31,
                 inv_slope_p31, inv_slope_p32)
            } else {
                (inv_slope_p31, inv_slope_p21,
                 inv_slope_p32, inv_slope_p31)
            }
        };

        draw_tri_flat_max(
            p1, p2[1],
            inv_slope1_max, inv_slope2_max,
            callback);
        draw_tri_flat_min(
            p3, p2[1] + 1, // avoid overlap
            inv_slope1_min, inv_slope2_min,
            callback);
    }
}
