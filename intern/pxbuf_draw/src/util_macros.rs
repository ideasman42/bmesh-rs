// Licensed: Apache 2.0

///
/// Order Args Macro
///
/// Takes args and returns them sorted smallest to biggest.
/// Usable for small number of args only (otherwise use proper sorting API).
///
/// Typical usage
///
/// ```
/// #[macro_use] extern crate pxbuf_draw;
/// # fn main() {
/// // Sorted values
/// let (x, y, z) = (6, -1, 3);
/// let (a, b, c) = order_args!(x, y, z);
/// assert_eq!((a, b, c), (-1, 3, 6));
/// # }
/// ```
///
/// ```
/// #[macro_use] extern crate pxbuf_draw;
/// # fn main() {
/// // Sorted the Z axis
/// let (p1, p2, p3) = ([1.1, 2.3, 4.9], [3.5, 9.3, 1.2], [-7.1, 6.3, -2.2]);
/// let (p1, p2, p3) = order_args_by!(p1, p2, p3, [2]);
/// assert_eq!((p1[2], p2[2], p3[2]), (-2.2, 1.2, 4.9));
/// # }
/// ```

// so: a <= b
#[macro_export]
macro_rules! order_args {
    ($a:expr, $b:expr) => {
        {
            let a = $a;
            let b = $b;
            if b >= a {
                (a, b)
            } else {
                (b, a)
            }
        }
    };

    ($a:expr, $b:expr, $c:expr) => {
        {
            let a = $a;
            let b = $b;
            let c = $c;
            if c >= b && c >= a  {
                if b > a {
                    (a, b, c)
                } else {
                    (b, a, c)
                }
            } else if b >= a && b >= c  {
                if c > a {
                    (a, c, b)
                } else {
                    (c, a, b)
                }
            } else {
                if c > b {
                    (b, c, a)
                } else {
                    (c, b, a)
                }
            }
        }
    };
}

// so: a <= b <= c
#[macro_export]
macro_rules! order_args_by {
    ($a:expr, $b:expr, $attr:tt) => {
        {
            let a = $a;
            let b = $b;
            if b$attr >= a$attr {
                (a, b)
            } else {
                (b, a)
            }
        }
    };

    ($a:expr, $b:expr, $c:expr, $attr:tt) => {
        {
            let a = $a;
            let b = $b;
            let c = $c;
            if c$attr >= b$attr && c$attr >= a$attr  {
                if b$attr > a$attr {
                    (a, b, c)
                } else {
                    (b, a, c)
                }
            } else if b$attr >= a$attr && b$attr >= c$attr  {
                if c$attr > a$attr {
                    (a, c, b)
                } else {
                    (c, a, b)
                }
            } else {
                if c$attr > b$attr {
                    (b, c, a)
                } else {
                    (c, b, a)
                }
            }
        }
    };
}
