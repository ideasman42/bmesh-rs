// Licensed: Apache 2.0

// tests for code defined in 'bmesh_primitives.rs'.

#[macro_use]
extern crate bmesh;

use bmesh::prelude::*;

use bmesh::primitives::{
    BMeshPartialGeom,
};

// ---------------------------------------------------------------------------
// Test BMesh.edge_split

#[test]
fn edge_edge_split() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::edge(&mut bm, 1.0, &mut geom);

    assert_mesh_len_eq!(bm, v:2, e:1, f:0);

    if let Some(edges) = geom.edges.clone() {
        let e_old = edges[0];
        let (v_new, e_new) = bm.edge_split(e_old, e_old.verts[0], 0.5);

        assert_eq!(1, e_old.other_vert(v_new).edge_count());
        assert_eq!(1, e_new.other_vert(v_new).edge_count());
        assert_eq!(2, v_new.edge_count());
    };
    assert_mesh_len_eq!(bm, v:3, e:2, f:0);
    assert_eq!(true, bm.validate());
}

#[test]
fn plane_edge_split() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::plane(&mut bm, 1.0, &mut geom);

    assert_mesh_len_eq!(bm, v:4, e:4, f:1);

    if let Some(edges) = geom.edges.clone() {
        let e_old = edges[0];
        let (v_new, e_new) = bm.edge_split(e_old, e_old.verts[0], 0.5);

        assert_eq!(2, e_old.other_vert(v_new).edge_count());
        assert_eq!(2, e_new.other_vert(v_new).edge_count());
        assert_eq!(2, v_new.edge_count());
    };
    assert_mesh_len_eq!(bm, v:5, e:5, f:1);

    assert_eq!(true, bm.validate());
}

#[test]
fn cube_edge_split() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::cube(&mut bm, 1.0, &mut geom);

    assert_mesh_len_eq!(bm, v:8, e:12, f:6);

    if let Some(edges) = geom.edges.clone() {
        let e_old = edges[0];
        let (v_new, e_new) = bm.edge_split(e_old, e_old.verts[0], 0.5);

        assert_eq!(3, e_old.other_vert(v_new).edge_count());
        assert_eq!(3, e_new.other_vert(v_new).edge_count());
        assert_eq!(2, v_new.edge_count());
    }
    assert_mesh_len_eq!(bm, v:9, e:13, f:6);
    assert_eq!(true, bm.validate());
}

#[test]
fn cube_edge_split_all() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::cube(&mut bm, 1.0, &mut geom);

    if let Some(edges) = geom.edges.clone() {
        for e_old in edges {
            let (v_new, e_new) = bm.edge_split(e_old, e_old.verts[0], 0.5);
            assert_eq!(3, e_old.other_vert(v_new).edge_count());
            assert_eq!(3, e_new.other_vert(v_new).edge_count());
            assert_eq!(2, v_new.edge_count());
        }
    }
    assert_mesh_len_eq!(bm, v:20, e:24, f:6);
    assert_eq!(true, bm.validate());

    {
        let mut v_mid = 0;
        let mut b_cnr = 0;
        for v in bm.verts.iter_mut() {
            let count = v.edge_count();
            if count == 2 {
                v_mid += 1;
            } else if count == 3 {
                b_cnr += 1;
            } else {
                panic!();
            }
        }
        assert_eq!(12, v_mid);
        assert_eq!(8, b_cnr);
    }

    {
        for f in bm.faces.iter_mut() {
            assert_eq!(8, f.len);
        }
    }
}


// ---------------------------------------------------------------------------
// Test BMesh.face_split

#[test]
fn plane_face_split() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::plane(&mut bm, 1.0, &mut geom);

    assert_mesh_len_eq!(bm, v:4, e:4, f:1);

    if let Some(faces) = geom.faces.clone() {
        let f = faces[0];
        let l_a = f.l_first;
        let l_b = l_a.next.next;

        let (_v_new, l_new) = bm.face_split(f, l_a, l_b, None, true);

        assert_eq!(3, l_new.v.edge_count());
        assert_eq!(3, l_new.next.v.edge_count());
        assert_eq!(2, l_new.e.face_count());
        assert_eq!(3, l_new.f.len);
        assert_eq!(3, l_new.radial_next.f.len);
    };

    assert_mesh_len_eq!(bm, v:4, e:5, f:2);
    assert_eq!(true, bm.validate());
}

#[test]
fn cube_face_split_all() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::cube(&mut bm, 1.0, &mut geom);

    if let Some(faces) = geom.faces.clone() {
        for f_old in faces {
            let l_a = f_old.l_first;
            let l_b = l_a.next.next;

            let (_f_new, l_new) = bm.face_split(f_old, l_a, l_b, None, true);

            assert_eq!(2, l_new.e.face_count());
            assert_eq!(3, l_new.f.len);
            assert_eq!(3, l_new.radial_next.f.len);
        }
    }
    assert_mesh_len_eq!(bm, v:8, e:18, f:12);
    assert_eq!(true, bm.validate());
}


// ---------------------------------------------------------------------------
// Test BMesh.vert_dissolve

#[test]
fn vert_dissolve_circle() {
    // segs can be any even number.
    let segs = 8;
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, segs, true, true, &mut geom);
    assert_geom_len_eq!(&geom, v:segs + 1, e:segs * 2, f:segs);
    assert_eq!(true, bm.validate());

    // find the central vertex
    let v_pivot = {
        let f = bm.faces.iter_mut().nth(0).unwrap();
        let mut l = null_const();
        bm_iter_loops_of_face_cycle!(f, l_iter, {
            if l_iter.e.is_boundary() {
                l = l_iter;
                break;
            }
        });

        l.prev.v
    };

    assert_eq!(true, bm.vert_dissolve(v_pivot));
    assert_mesh_len_eq!(bm, v:segs, e:segs, f:1);

    {
        let f = bm.faces.iter_mut().nth(0).unwrap();
        assert_eq!(segs, f.len as usize);
    }
    assert_eq!(true, bm.validate());

    // now dissolve every second vertex,
    // halving the total number.
    {
        let f = bm.faces.iter_mut().nth(0).unwrap();
        let mut i = 0;
        let mut verts: Vec<BMVertMutP> = Vec::with_capacity(segs / 2);
        bm_iter_loops_of_face_cycle!(f, l_iter, {
            if (i % 2) == 0 {
                verts.push(l_iter.v.clone());
            }
            i += 1;
        });

        for v in verts.clone() {
            assert_eq!(true, bm.vert_dissolve(v));
        }

        {
            let f = bm.faces.iter_mut().nth(0).unwrap();
            assert_eq!(segs / 2, f.len as usize);
        }
        assert_eq!(true, bm.validate());
    }

}

