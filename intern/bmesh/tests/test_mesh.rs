// Licensed: Apache 2.0

// tests for code defined in 'bmesh_primitives.rs'.

#[macro_use]
extern crate bmesh;
extern crate math_misc;

use bmesh::types::prelude::*;

use bmesh::primitives::{
    BMeshPartialGeom,
};

use math_misc::{
    angle_normalized_v3v3,
    normalize_v3_v3,
};

#[test]
fn update_normal_cube() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    bmesh::primitives::cube(&mut bm, 1.0, &mut geom);

    bm.normal_update();

    // we know all normals will point *outwards*, assert this to be the case.
    for v in bm.verts.iter() {
        let mut n: [f64; 3] = [0.0, 0.0, 0.0];
        normalize_v3_v3(&mut n, &v.co);
        assert!(angle_normalized_v3v3(&n, &v.no) < 1e-6);
    }

    for f in bm.faces.iter() {
        let center = f.calc_center_median();
        let mut n: [f64; 3] = [0.0, 0.0, 0.0];
        normalize_v3_v3(&mut n, &center);
        assert!(angle_normalized_v3v3(&n, &f.no) < 1e-6);
    }

    assert_eq!(true, bm.validate());
}

