// Licensed: Apache 2.0

// tests for code defined in 'bmesh_primitives.rs'.

#[macro_use]
extern crate bmesh;
extern crate math_misc;

use bmesh::types::prelude::*;

use bmesh::primitives::{
    BMeshPartialGeom,
};

use math_misc::{
    angle_normalized_v3v3,
};

fn ngon_from_coords(bm: &mut BMesh, coords: &[[f64; 3]]) -> BMFaceMutP {

    let verts: Vec<BMVertMutP> = coords.into_iter().map(|co| {
        bm.verts.new(co, None, BMElemCreate::NOP)
    }).collect();

    let edges = {
        let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(coords.len());
        unsafe { edges.set_len(coords.len()); }
        bm.edges.from_verts_ensure(&mut edges, &verts);
        edges
    };

    return bm.faces.new(&verts, &edges, None, BMElemCreate::NOP);
}


#[test]
fn normal_simple() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    bmesh::primitives::plane(&mut bm, 1.0, &mut geom);

    if let Some(faces) = geom.faces.clone() {
        let f = faces[0].clone();
        f.clone().normal_update();
        assert_eq!([0.0, 0.0, -1.0], f.no);
        bm.face_flip(f);
        f.clone().normal_update();
        assert_eq!([0.0, 0.0, 1.0], f.no);
    }

    assert_eq!(true, bm.validate());
}


#[test]
fn triangulate_plane() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    bmesh::primitives::plane(&mut bm, 1.0, &mut geom);

    let mut f = bm.faces.iter_mut().nth(0).unwrap();

    let mut faces_double: Vec<BMFaceMutP> = Vec::with_capacity(0);

    f.normal_update();

    assert_mesh_len_eq!(bm, v:4, e:4, f:1);

    bm.face_triangulate(f, &mut None, &mut None, &mut faces_double, false);

    assert_mesh_len_eq!(bm, v:4, e:5, f:2);

    assert_eq!(true, bm.validate());
}


#[test]
fn triangulate_ngon_concave() {
    // Note that polyfill_2d crate has many of its own tests
    // so this isn't intended to be a comprehensive test
    // of tessellation, only to check the bmesh api is capable of tessellation.

    let coords: Vec<[f64; 3]> = vec![
        [-3.0, 2.0, 0.0],
        [-3.0, -2.0, 0.0],
        [-1.0, -1.0, 0.0],
        [-2.0, -3.0, 0.0],
        [3.0, -3.0, 0.0],
        [3.0, 2.0, 0.0],
        [1.0, 1.0, 0.0],
        [2.0, 3.0, 0.0],
        [-2.0, 3.0, 0.0],
        [2.0, -2.0, 0.0],
    ];

    let mut bm = BMesh::new();
    let mut faces_new: Vec<BMFaceMutP> = Vec::with_capacity(coords.len() - 3);
    let mut edges_new: Vec<BMEdgeMutP> = Vec::with_capacity(coords.len() - 3);
    let normal_prev: [f64; 3];

    let area_prev;
    {
        let mut f = ngon_from_coords(&mut bm, &coords);
        area_prev = f.calc_area();

        let mut faces_double: Vec<BMFaceMutP> = Vec::with_capacity(0);

        f.normal_update();
        normal_prev = f.no;

        assert_mesh_len_eq!(bm, v:coords.len(), e:coords.len(), f:1);

        bm.face_triangulate(
            f,
            &mut Some(&mut faces_new), &mut Some(&mut edges_new),
            &mut faces_double, false,
        );

        assert_eq!(0, faces_double.len());
        assert_eq!(coords.len() - 3, faces_new.len());  // doesn't include input face
        assert_eq!(coords.len() - 3, edges_new.len());
    }

    assert_mesh_len_eq!(
        bm, v:coords.len(), e:coords.len() + coords.len() - 3, f:coords.len() - 2);

    assert_eq!(true, bm.validate());

    {
        let area_tess: f64 = bm.faces.iter().map(|f| { f.calc_area() }).sum();
        assert!((area_tess - area_prev).abs() < 1e-6);
    }

    {
        let normal_difference: f64 = bm.faces.iter_mut().map(|f| {
            f.clone().normal_update();
            angle_normalized_v3v3(&f.no, &normal_prev)
        }).sum();

        assert!(normal_difference < 1e-6);
    }

}

