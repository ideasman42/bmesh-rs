// Licensed: Apache 2.0

// tests for code defined in 'bmesh_core.rs'.

#[macro_use]
extern crate bmesh;

use bmesh::prelude::*;

// ------------
// Test Helpers

fn create_edge(
    bm: &mut BMesh,
) -> (Vec<BMVertMutP>, Vec<BMEdgeMutP>, Vec<BMFaceMutP>) {

    let verts = vec![
        bm.verts.new(&[0.0, 0.0, 0.0], None, BMElemCreate::NOP),
        bm.verts.new(&[1.0, 0.0, 0.0], None, BMElemCreate::NOP),
    ];

    let edges = vec![
        bm.edges.new(&[verts[0], verts[1]], None, BMElemCreate::NOP),
    ];
    let faces = vec![
    ];

    return (verts, edges, faces);
}

fn create_quad(
    bm: &mut BMesh,
) -> (Vec<BMVertMutP>, Vec<BMEdgeMutP>, Vec<BMFaceMutP>) {

    let verts = vec![
        bm.verts.new(&[0.0, 0.0, 0.0], None, BMElemCreate::NOP),
        bm.verts.new(&[1.0, 0.0, 0.0], None, BMElemCreate::NOP),
        bm.verts.new(&[1.0, 1.0, 0.0], None, BMElemCreate::NOP),
        bm.verts.new(&[0.0, 1.0, 0.0], None, BMElemCreate::NOP),
    ];
    let edges = vec![
        bm.edges.new(&[verts[0], verts[1]], None, BMElemCreate::NOP),
        bm.edges.new(&[verts[1], verts[2]], None, BMElemCreate::NOP),
        bm.edges.new(&[verts[2], verts[3]], None, BMElemCreate::NOP),
        bm.edges.new(&[verts[3], verts[0]], None, BMElemCreate::NOP),
    ];
    let faces = vec![
        bm.faces.new(&verts, &edges, None, BMElemCreate::NOP),
    ];

    return (verts, edges, faces);
}

fn test_bm_is_cleared(bm: &BMesh) -> bool {
    return
        bm.verts.is_empty() &&
        bm.edges.is_empty() &&
        bm.loops.is_empty() &&
        bm.faces.is_empty();
}


// -----
// Tests

#[test]
fn elem_flag() {
    let mut bm = BMesh::new();
    let mut v1 = bm.verts.new(&[0.0, 0.0, 0.0], None, BMElemCreate::NOP);

    v1.head.hflag = BM_ELEM_SELECT | BM_ELEM_SEAM;
}

#[test]
fn remove_vert_by_vert() {
    let mut bm = BMesh::new();
    let v1 = bm.verts.new(&[0.0, 0.0, 0.0], None, BMElemCreate::NOP);
    bm.verts.remove(v1);
    assert!(test_bm_is_cleared(&bm));
    // assert!((&bm));
    // assert_eq!(bmesh_mesh_elem_check);
}

#[test]
fn remove_edge_by_vert() {
    let mut bm = BMesh::new();
    let (verts, _edges, _faces) = create_edge(&mut bm);
    bm.verts.remove(verts[1]);
    assert_mesh_len_eq!(bm, v:1, e:0, f:0);
    bm.verts.remove(verts[0]);
    assert!(test_bm_is_cleared(&bm));
}

#[test]
fn remove_edge_by_edge() {
    let mut bm = BMesh::new();
    let (verts, edges, _faces) = create_edge(&mut bm);
    bm.edges.remove(edges[0]);
    assert_mesh_len_eq!(bm, v:2, e:0, f:0);
    bm.verts.remove(verts[0]);
    bm.verts.remove(verts[1]);
    assert!(test_bm_is_cleared(&bm));
}

#[test]
fn remove_face_by_vert() {
    let mut bm = BMesh::new();
    let (verts, _edges, _faces) = create_quad(&mut bm);
    for v in verts {
        bm.verts.remove(v);
    }
    assert_mesh_len_eq!(bm, v:0, e:0, f:0);
    assert!(test_bm_is_cleared(&bm));
}

#[test]
fn remove_face_by_edge() {
    let mut bm = BMesh::new();
    let (verts, edges, _faces) = create_quad(&mut bm);
    for e in edges {
        bm.edges.remove(e);
    }

    assert_mesh_len_eq!(bm, v:4, e:0, f:0);
    for v in verts {
        bm.verts.remove(v);
    }
    assert!(test_bm_is_cleared(&bm));
}

#[test]
fn remove_face_by_face() {
    let mut bm = BMesh::new();

    let (verts, _edges, faces) = create_quad(&mut bm);

    for f in faces {
        assert!(f.contains_vert(verts[0]));
        bm.faces.remove(f);
    }
    assert_mesh_len_eq!(bm, v:4, e:4, f:0);
    for v in verts {
        bm.verts.remove(v);
    }
    assert!(test_bm_is_cleared(&bm));
}

#[test]
fn join_faces() {
    use bmesh::primitives::{BMeshPartialGeom, cube};

    for do_delete in &[false, true] {
        let mut bm = BMesh::new();
        let mut geom = BMeshPartialGeom::default();
        cube(&mut bm, 1.0, &mut geom);

        let mut faces = Vec::with_capacity(3);
        let v = bm.verts.iter_mut().nth(0).unwrap();
        bm_iter_loops_of_vert!(v, l_iter, {
            faces.push(l_iter.f);
        });
        assert_eq!(3, faces.len());


        let f = bm.face_join_n(&faces[..], *do_delete);
        assert_eq!(6, f.len);

        if *do_delete == false {
            assert_mesh_len_eq!(bm, v:8, e:12, f:4);
        } else {
            assert_mesh_len_eq!(bm, v:8 - 1, e:12 - 3, f:4);
        }

        assert_eq!(true, bm.validate());
    }
}

#[test]
fn vert_separate() {
    use bmesh::primitives::{BMeshPartialGeom, cube};

    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    cube(&mut bm, 1.0, &mut geom);

    let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(3);
    let v = bm.verts.iter_mut().nth(0).unwrap();
    bm_iter_edges_of_vert_mut!(v, e_iter, {
        edges.push(e_iter);
    });
    assert_eq!(3, edges.len());

    assert_mesh_len_eq!(bm, v:8, e:12, f:6, l:4 * 6);

    let mut verts = bm.vert_separate(v, &edges, false);

    assert_mesh_len_eq!(bm, v:10, e:15, f:6, l:4 * 6);

    assert_eq!(true, bm.validate());

    for v in verts.iter_mut() {
        assert_eq!(1, v.face_count());
        assert_eq!(2, v.edge_count());
    }
}

#[test]
fn loop_separate() {
    use bmesh::primitives::{BMeshPartialGeom, cube};

    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    cube(&mut bm, 1.0, &mut geom);

    let mut loops: Vec<BMLoopMutP> = Vec::with_capacity(3);
    let v = bm.verts.iter_mut().nth(0).unwrap();
    bm_iter_loops_of_vert_mut!(v, l_iter, {
        loops.push(l_iter);
    });
    assert_eq!(3, loops.len());

    assert_mesh_len_eq!(bm, v:8, e:12, f:6, l:4 * 6);

    let mut verts: Vec<BMVertMutP> = Vec::with_capacity(3);
    for l in loops.iter_mut() {
        let v = bm.face_loop_separate(*l);
        verts.push(v);
    }

    assert_mesh_len_eq!(bm, v:11, e:15, f:6, l:4 * 6);

    assert_eq!(true, bm.validate());

    assert_eq!(0, v.face_count());
    for l in loops.iter_mut() {
        assert_eq!(1, l.v.face_count());
        assert_eq!(2, l.v.edge_count());
    }
}

#[test]
fn loop_separate_multi() {
    use bmesh::primitives::{BMeshPartialGeom, circle};

    let segs = 32;
    for fan_split_groups in &[1, 2, 3, 4, 8, 16] {
        // println!("group: {}", fan_split_groups);

        let mut bm = BMesh::new();
        let mut geom = BMeshPartialGeom::default();
        circle(&mut bm, 1.0, segs, true, true, &mut geom);

        // find the central vertex
        let l_pivot = {
            let f = bm.faces.iter_mut().nth(0).unwrap();
            let mut l = null_const();
            bm_iter_loops_of_face_cycle!(f, l_iter, {
                if l_iter.e.is_boundary() {
                    l = l_iter;
                    break;
                }
            });
            l.prev
        };

        let mut loops_a: Vec<BMLoopMutP> = Vec::with_capacity(segs / 2);
        let mut loops_b: Vec<BMLoopMutP> = Vec::with_capacity(segs / 2);

        let mut l_iter = l_pivot;
        let mut e_prev = l_pivot.prev.e;

        let mut loop_num_region = 0;
        loop {
            // One day this might be supported!
            // http://stackoverflow.com/questions/40563116

            if ((loop_num_region / fan_split_groups) % 2) != 0 {
                loops_a.push(l_iter);
            } else {
                loops_b.push(l_iter);
            }

            // awkward alternative
            {
                let tmp = l_iter.step_fan_mut(e_prev);
                l_iter = tmp.0;
                e_prev = tmp.1;
            }

            if l_iter == l_pivot {
                break;
            }

            loop_num_region += 1;
        }

        let v_separate = bm.face_loop_separate_multi(&loops_a);

        {
            for l in loops_a {
                assert!(l.v == v_separate);
            }

            for l in loops_b {
                assert!(l.v == l_pivot.v);
            }
        }

        // a little involved to figure out how many vert/edge/faces are remaining
        assert_mesh_len_eq!(
            bm,
            v:(segs + 1) + 1,
            e:(segs * 2) + (segs / fan_split_groups),
            f:segs
        );

        assert_eq!(true, bm.validate());
    }
}

