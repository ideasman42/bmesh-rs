// Licensed: Apache 2.0

// tests for code defined in 'bmesh_queries.rs'.

#[macro_use]
extern crate bmesh;

use bmesh::types::prelude::*;
use bmesh::prelude::null_mut;

use bmesh::primitives::{
    BMeshPartialGeom,
};

#[test]
fn circle_empty() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, 6, false, false, &mut geom);
    assert_geom_len_eq!(&geom, v:6, e:6, f:0);
    assert_eq!(true, bm.validate());
    assert_eq!(null_mut(), bm.faces.get(&geom.verts.unwrap()[..]));
}

#[test]
fn circle_ngon() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, 6, true, false, &mut geom);
    assert_geom_len_eq!(&geom, v:6, e:6, f:1);
    assert_eq!(true, bm.validate());
    assert_ne!(null_mut(), bm.faces.get(&geom.verts.unwrap()[..]));
}
