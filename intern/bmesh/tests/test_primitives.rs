// Licensed: Apache 2.0

// tests for code defined in 'bmesh_primitives.rs'.

#[macro_use]
extern crate bmesh;

use bmesh::types::prelude::*;

use bmesh::primitives::{
    BMeshPartialGeom,
};

#[test]
fn edge() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    bmesh::primitives::edge(&mut bm, 1.0, &mut geom);

    assert_geom_len_eq!(&geom, v:2, e:1, f:0);
    assert_eq!(true, bm.validate());
}

#[test]
fn plane() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    bmesh::primitives::plane(&mut bm, 1.0, &mut geom);

    assert_geom_len_eq!(&geom, v:4, e:4, f:1);
    assert_eq!(true, bm.validate());
}

#[test]
fn cube() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    bmesh::primitives::cube(&mut bm, 1.0, &mut geom);

    for v in bm.verts.iter() {
        assert_eq!(v.edge_count(), 3);
    }

    for e in bm.edges.iter() {
        assert_eq!(e.face_count(), 2);
    }

    for f in bm.faces.iter() {
        assert_eq!(f.len, 4);
    }

    assert_geom_len_eq!(&geom, v:8, e:12, f:6);
    assert_eq!(true, bm.validate());
}

#[test]
fn circle_empty() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, 6, false, false, &mut geom);
    assert_geom_len_eq!(&geom, v:6, e:6, f:0);
    assert_eq!(true, bm.validate());
}

#[test]
fn circle_ngon() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, 6, true, false, &mut geom);
    assert_geom_len_eq!(&geom, v:6, e:6, f:1);
    assert_eq!(true, bm.validate());
}

#[test]
fn circle_tris() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, 6, true, true, &mut geom);
    assert_geom_len_eq!(&geom, v:7, e:12, f:6);
    assert_eq!(true, bm.validate());
}

#[test]
fn torus() {
    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();

    let major_seg = 48;
    let minor_seg = 12;

    bmesh::primitives::torus(&mut bm, 1.0, 0.25, major_seg, minor_seg, &mut geom);

    let total = major_seg * minor_seg;
    assert_geom_len_eq!(&geom, v: total, e:total * 2, f:total);
    assert_eq!(true, bm.validate());

    /*
    use bmesh::test_utils;
    test_utils::export_mesh_to_obj_from_filepath(
        &mut bm,
        &String::from("out.obj")).expect("failed");
    */
}
