// Licensed: Apache 2.0

macro_rules! bm_elem_api_flag_enable  {
    ($elem:expr, $flag:expr) => {
        {
            $elem.head.api_flag |=  $flag;
        }
    }
}
macro_rules! bm_elem_api_flag_disable {
    ($elem:expr, $flag:expr) => {
        {
            $elem.head.api_flag &= !$flag;
        }
    }
}
macro_rules! bm_elem_api_flag_test {
    ($elem:expr, $flag:expr) => {
        {
            ($elem.head.api_flag & $flag) != 0
        }
    }
}
#[allow(unused_macros)]
macro_rules! bm_elem_api_flag_clear {
    ($elem:expr, $flag:expr) => {
        {
            use bmesh_class::bmesh_bitflag_bmelem_api_flag::BMElemApiFlag;
            $elem.head.api_flag = BMElemApiFlag(0);
        }
    }
}

macro_rules! bm_check_element {
    ($elem:expr) => {
        {
            use intern::bmesh_core::bmesh_elem_check;
            debug_assert!(
                bmesh_elem_check($elem.as_elem(), $elem.head.htype) == 0);
        }
    }
}


// ----------------------------
// Utility Helpers for Counting

macro_rules! BM_vert_edge_count_is_equal {
    ($e:expr, $n:expr) => {
        {
            use intern::bmesh_queries::BM_vert_edge_count_ex;
            BM_vert_edge_count_ex($e, $n + 1) == $n
        }
    }
}
#[allow(unused_macros)]
macro_rules! BM_vert_edge_count_is_over  {
    ($e:expr, $n:expr) => {
        {
            use intern::bmesh_queries::BM_vert_edge_count_ex;
            BM_vert_edge_count_ex($e, $n + 1) == $n + 1
        }
    }
}
#[allow(unused_macros)]
macro_rules! BM_edge_face_count_is_equal {
    ($e:expr, $n:expr) => {
        {
            use intern::bmesh_queries::BM_edge_face_count_ex;
            BM_edge_face_count_ex($e, $n + 1) == $n
        }
    }
}
macro_rules! BM_edge_face_count_is_over  {
    ($e:expr, $n:expr) => {
        {
            use intern::bmesh_queries::BM_edge_face_count_ex;
            BM_edge_face_count_ex($e, $n + 1) == $n + 1
        }
    }
}
macro_rules! BM_vert_face_count_is_equal {
    ($e:expr, $n:expr) => {
        {
            use intern::bmesh_queries::BM_vert_face_count_ex;
            BM_vert_face_count_ex($e, $n + 1) == $n
        }
    }
}
#[allow(unused_macros)]
macro_rules! BM_vert_face_count_is_over {
    ($e:expr, $n:expr) => {
        {
            use intern::bmesh_queries::BM_vert_face_count_ex;
            BM_vert_face_count_ex($e, $n + 1) == $n + 1
        }
    }
}

