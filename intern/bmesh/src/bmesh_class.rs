// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use mempool::{
    MemPool,
    MemPoolElemUtils,
};

use std::ptr;

use plain_ptr::{
    PtrMut,
    PtrConst,
    // null,
    null_mut,
};


// ---------------------------------------------------------------------------
// External Modules

/// public types
pub mod types {
    pub use bmesh_class::{
        BMesh,

        BMHeader,
        BMElem,
        BMVert,
        BMEdge,
        BMLoop,
        BMFace,
        BMDiskLink,

        BMHeaderMutP,
        BMElemMutP,
        BMVertMutP,
        BMEdgeMutP,
        BMLoopMutP,
        BMFaceMutP,
        BMDiskLinkMutP,

        BMHeaderConstP,
        BMElemConstP,
        BMVertConstP,
        BMEdgeConstP,
        BMLoopConstP,
        BMFaceConstP,
        BMDiskLinkConstP,

        BMVertSeq,
        BMEdgeSeq,
        BMLoopSeq,
        BMFaceSeq,
    };

    pub use plain_ptr::{
        PtrMut,
        PtrConst,
    };
}

/// consts for struct members
pub mod consts {
    pub use bmesh_class::bmesh_bitflag_bmelem_hflag::*;
    pub use bmesh_class::bmesh_bitflag_bmelem_htype::*;

}

/// consts for function arguments
pub mod fn_consts {
    pub use intern::bmesh_core::{
        BMElemCreate,
    };
    pub use intern::bmesh_marking_builder::{
        BMElemEditFlagBuilder,
        BMElemFlushFlagBuilder,
    };
}

pub mod consts_default {
    pub const CHUNK_SIZE: usize = 128;

    /// Default allocation sizes for vectors.
    /// Keep within limits of stack memory.

    /// Number of vertices for a face.
    pub const BM_DEFAULT_NGON_STACK_SIZE: usize = 32;
    /// Number of edges connected to a vertex.
    pub const BM_DEFAULT_DISK_STACK_SIZE: usize = 16;
    /// Number of faces connected to an edge.
    pub const BM_DEFAULT_RADIAL_STACK_SIZE: usize = 8;
}

macro_rules! bm_elem_mempool_impl {
    ($($t:ty)*) => ($(
        impl MemPoolElemUtils for $t {
            #[inline]
            fn default_chunk_size() -> usize {
                return consts_default::CHUNK_SIZE;
            }
            #[inline]
            fn free_ptr_get(&self) -> *mut Self {
                return self.head.data as *mut Self;
            }
            fn free_ptr_set(&mut self, ptr: *mut Self) {
                self.head.data = ptr as *mut CustomDataElem;
                self.head.htype = BMElemType(0xff);
            }
            fn free_ptr_test(&self) -> bool {
                return self.head.htype == BMElemType(0xff);
            }
        }
    )*)
}

bm_elem_mempool_impl! { BMVert BMEdge BMLoop BMFace BMElem }

/// Used everywhere, convenience types.
pub type BMHeaderConstP = PtrConst<BMHeader>;
pub type BMElemConstP = PtrConst<BMElem>;
pub type BMVertConstP = PtrConst<BMVert>;
pub type BMEdgeConstP = PtrConst<BMEdge>;
pub type BMLoopConstP = PtrConst<BMLoop>;
pub type BMFaceConstP = PtrConst<BMFace>;
pub type BMDiskLinkConstP = PtrConst<BMDiskLink>;

pub type BMHeaderMutP = PtrMut<BMHeader>;
pub type BMElemMutP = PtrMut<BMElem>;
pub type BMVertMutP = PtrMut<BMVert>;
pub type BMEdgeMutP = PtrMut<BMEdge>;
pub type BMLoopMutP = PtrMut<BMLoop>;
pub type BMFaceMutP = PtrMut<BMFace>;
pub type BMDiskLinkMutP = PtrMut<BMDiskLink>;

use custom_data::{
    CustomData,
    CustomDataElem,
};

#[derive(Debug)]
#[repr(C)]
pub struct BMHeader {
    pub data: *mut CustomDataElem,
    pub index: i32,

    /// element geometric type (verts/edges/loops/faces)
    pub htype: BMElemType,
    /// this would be a CD layer, see below
    pub hflag: BMElemFlag,

    /// internal use only!
    /// note,.we are very picky about not bloating this struct
    /// but in this case its padded up to 16 bytes anyway,
    /// so adding a flag here gives no increase in size
    pub api_flag: BMElemApiFlag,
}

impl Default for BMHeader {
    fn default() -> Self {
        BMHeader {
            data: ptr::null_mut(),
            index: -1,
            htype: BMElemType(0),
            hflag: BMElemFlag(0),
            api_flag: BMElemApiFlag(0),
        }
    }
}

// Any struct with a 'head' member
#[derive(Debug)]
#[repr(C)]
pub struct BMElem {
    pub head: BMHeader,
}

#[derive(Debug)]
#[repr(C)]
pub struct BMVert {
    pub head: BMHeader,

    /// vertex coords
    pub co: [f64; 3],
    /// vertex normal
    pub no: [f64; 3],
    /// pointer to (any) edge using this vertex (for disk cycles)
    ///
    /// note: some higher level functions set this to different edges that use this vertex,
    /// which is a bit of an abuse of internal bmesh data but also works OK for now
    /// (use with care!).
    pub e: BMEdgeMutP,
}

impl Default for BMVert {
    fn default() -> Self {
        BMVert {
            head: BMHeader { htype: BM_VERT, ..BMHeader::default() },
            co: [0.0, 0.0, 0.0],
            no: [0.0, 0.0, 0.0],
            e: null_mut(),
        }
    }
}

/// Disk link structure, only used by edges.
#[derive(Debug)]
#[repr(C)]
pub struct BMDiskLink {
    pub next: BMEdgeMutP,
    pub prev: BMEdgeMutP,
}

impl Default for BMDiskLink {
    fn default() -> Self {
        BMDiskLink {
            next: null_mut(),
            prev: null_mut(),
        }
    }
}


#[derive(Debug)]
#[repr(C)]
pub struct BMEdge {
    pub head: BMHeader,

    /// Vertices (unordered).
    pub verts: [BMVertMutP; 2],

    /// The list of loops around the edge (use l.radial_prev/next)
    /// to access the other loops using the edge.
    pub l: BMLoopMutP,

    /// disk cycle pointers
    /// relative data: d1 indicates indicates the next/prev edge
    /// around vertex v1 and d2 does the same for v2.

    pub disk_links: [BMDiskLink; 2],
}

impl Default for BMEdge {
    fn default() -> Self {
        BMEdge {
            head: BMHeader { htype: BM_EDGE, ..BMHeader::default() },
            verts: [null_mut(), null_mut()],
            l: null_mut(),
            disk_links: [BMDiskLink::default(), BMDiskLink::default()],
        }
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct BMLoop {
    pub head: BMHeader,

    /// notice no flags layer

    pub v: BMVertMutP,
    /// edge, using verts (v, next.v)
    pub e: BMEdgeMutP,
    pub f: BMFaceMutP,

    /// circular linked list of loops which all use the same edge as this one '->e',
    /// but not necessarily the same vertex (can be either v1 or v2 of our own '->e')
    pub radial_next: BMLoopMutP,
    pub radial_prev: BMLoopMutP,

    /// These were originally commented as private but are used all over the code.

    /// next/prev verts around the face
    pub next: BMLoopMutP,
    pub prev: BMLoopMutP,
}

impl Default for BMLoop {
    fn default() -> Self {
        BMLoop {
            head: BMHeader { htype: BM_LOOP, ..BMHeader::default() },
            v: null_mut(),
            e: null_mut(),
            f: null_mut(),
            radial_next: null_mut(),
            radial_prev: null_mut(),
            next: null_mut(),
            prev: null_mut(),
        }
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct BMFace {
    pub head: BMHeader,

    pub l_first: BMLoopMutP,

    /// number of vertices in the face
    pub len: i32,
    /// face normal
    pub no: [f64; 3],
    /// material index
    pub mat_nr: i16,
}

impl Default for BMFace {
    fn default() -> Self {
        BMFace {
            head: BMHeader { htype: BM_FACE, ..BMHeader::default() },
            l_first: null_mut(),
            len: 0,
            no: [0.0, 0.0, 0.0],
            mat_nr: 0,
        }
    }
}

impl Default for BMElem {
    #[allow(unreachable_code)]
    fn default() -> Self {
        // needed to satisfy BMElemSeq, in practice
        unreachable!();
        BMElem {
            head: BMHeader { htype: BM_FACE, ..BMHeader::default() },
        }
    }
}


// ----------------------------------------------------------------------------
// Element Type Casting

impl BMElem {
    pub fn as_vert(&mut self) -> BMVertConstP {
        debug_assert!(self.head.htype == BM_VERT);
        return PtrConst((self as *mut _) as *mut BMVert);
    }
    pub fn as_vert_mut(&mut self) -> BMVertMutP {
        debug_assert!(self.head.htype == BM_VERT);
        return PtrMut((self as *mut _) as *mut BMVert);
    }

    pub fn as_edge(&mut self) -> BMEdgeConstP {
        debug_assert!(self.head.htype == BM_EDGE);
        return PtrConst((self as *mut _) as *mut BMEdge);
    }
    pub fn as_edge_mut(&mut self) -> BMEdgeMutP {
        debug_assert!(self.head.htype == BM_EDGE);
        return PtrMut((self as *mut _) as *mut BMEdge);
    }

    pub fn as_face(&mut self) -> BMFaceConstP {
        debug_assert!(self.head.htype == BM_FACE);
        return PtrConst((self as *mut _) as *mut BMFace);
    }
    pub fn as_face_mut(&mut self) -> BMFaceMutP {
        debug_assert!(self.head.htype == BM_FACE);
        return PtrMut((self as *mut _) as *mut BMFace);
    }
}

macro_rules! bm_elem_cast_impl {
    ($($t:ty)*) => ($(
        impl $t {
            pub fn as_elem_mut(&mut self) -> BMElemMutP {
                return PtrMut((self as *mut _) as *mut BMElem);
            }
            pub fn as_elem(&self) -> BMElemConstP {
                return PtrConst((self as *const _) as *const BMElem);
            }
        }
    )*)
}

bm_elem_cast_impl! { BMVert BMEdge BMLoop BMFace }

// ----------------------------------------------------------------------------
// Collection types
//
// Note that similar functionality is grouped
// across different types.

pub struct BMVertSeq(MemPool<BMVert>);
pub struct BMEdgeSeq(MemPool<BMEdge>);
pub struct BMLoopSeq(MemPool<BMLoop>);
pub struct BMFaceSeq(MemPool<BMFace>);
// only for casts
pub struct BMElemSeq(MemPool<BMElem>);

// -----------
// New Default

impl BMVertSeq {
    pub fn new_default(&mut self) -> BMVertMutP {
        return self.alloc_elem_from(BMVert::default());
    }
}
impl BMEdgeSeq {
    pub fn new_default(&mut self) -> BMEdgeMutP {
        return self.alloc_elem_from(BMEdge::default());
    }
}
impl BMLoopSeq {
    pub fn new_default(&mut self) -> BMLoopMutP {
        return self.alloc_elem_from(BMLoop::default());
    }
}
impl BMFaceSeq {
    pub fn new_default(&mut self) -> BMFaceMutP {
        return self.alloc_elem_from(BMFace::default());
    }
}


macro_rules! bm_elem_seq_impl {
    ($seq_type:ty, $ele_type:ty) => {
        impl $seq_type {

            // Alloc free
            #[inline]
            pub fn alloc_elem_from(&mut self, ele: $ele_type) -> PtrMut<$ele_type> {
                PtrMut(self.0.alloc_elem_from(ele))
            }
            #[inline]
            pub fn alloc_elem(&mut self) -> PtrMut<$ele_type> {
                PtrMut(unsafe { self.0.alloc_elem_uninitialized() })
            }
            #[inline]
            pub fn free_elem(&mut self, ele: PtrMut<$ele_type>) {
                self.0.free_elem(ele.as_ptr());
            }

            // Iterator access
            #[inline]
            pub fn len(&self) -> usize {
                self.0.len()
            }

            #[inline]
            pub fn is_empty(&self) -> bool {
                self.0.is_empty()
            }

            #[inline]
            pub fn iter_mut(&mut self) -> ::mempool::MemPoolIterMut<$ele_type> {
                return self.0.iter_mut();
            }

            #[inline]
            pub fn iter(&self) -> ::mempool::MemPoolIterConst<$ele_type> {
                return self.0.iter();
            }

            #[inline]
            pub fn clear(&mut self) {
                return self.0.clear();
            }

            /// Allow iterating over specific types as 'BMElem'.
            ///
            /// This allows generalizing over each element type, eg:
            /// ```
            /// for ele_seq in [
            ///     bm.verts.as_elem_seq(),
            ///     bm.edges.as_elem_seq(),
            ///     bm.faces.as_elem_seq(),
            ///  ] {
            ///      for ele in ele_seq.iter_mut() {
            ///          ele.head.hflag &= !hflag;
            ///      }
            ///  }
            /// ```
            pub fn as_elem_seq_mut(&mut self) -> &mut BMElemSeq {
                use ::std::mem::transmute;
                return unsafe { transmute::<&mut $seq_type, &mut BMElemSeq>(self) };
            }
            pub fn as_elem_seq(&self) -> &BMElemSeq {
                use ::std::mem::transmute;
                return unsafe { transmute::<&$seq_type, &BMElemSeq>(self) };
            }
        }
    }
}

bm_elem_seq_impl!(BMVertSeq, BMVert);
bm_elem_seq_impl!(BMEdgeSeq, BMEdge);
bm_elem_seq_impl!(BMLoopSeq, BMLoop);
bm_elem_seq_impl!(BMFaceSeq, BMFace);
bm_elem_seq_impl!(BMElemSeq, BMElem);


macro_rules! bm_elem_seq_noloop_impl {
    ($seq_type:ty, $ele_type:ty, $ele_flag:ident, $seq_attr:ident, $seq_table:ident) => {
        impl $seq_type {
            /// Index lookup, using pre-calculated table.
            #[inline]
            pub fn nth_mut(&mut self, index: usize) -> PtrMut<$ele_type> {
                let bm = unsafe { parent_of_mut!(self, BMesh, $seq_attr) };
                debug_assert!(index < bm.$seq_table.len());
                debug_assert!((bm.elem_table_dirty & $ele_flag) == 0);
                return unsafe {
                    *bm.$seq_table.get_unchecked_mut(index)
                };
            }

            /// Index lookup, using pre-calculated table.
            #[inline]
            pub fn nth(&mut self, index: usize) -> PtrConst<$ele_type> {
                let bm = unsafe { parent_of_mut!(self, BMesh, $seq_attr) };
                debug_assert!(index < bm.$seq_table.len());
                debug_assert!((bm.elem_table_dirty & $ele_flag) == 0);
                return unsafe {
                    bm.$seq_table.get_unchecked(index).as_const()
                };
            }
        }
    }
}

bm_elem_seq_noloop_impl!(BMVertSeq, BMVert, BM_VERT, verts, vtable);
bm_elem_seq_noloop_impl!(BMEdgeSeq, BMEdge, BM_EDGE, edges, etable);
bm_elem_seq_noloop_impl!(BMFaceSeq, BMFace, BM_FACE, faces, ftable);

/*
impl BMVertSeq {
}
impl BMEdgeSeq {
}
impl BMLoopSeq {
}
impl BMFaceSeq {
}
*/

pub struct BMesh {
    // int totvert, totedge, totloop, totface;
    /// flag index arrays as being dirty so we can check if they are clean and
    /// avoid looping over the entire vert/edge/face/loop array in those cases.
    /// valid flags are - BM_VERT | BM_EDGE | BM_FACE | BM_LOOP.
    pub elem_index_dirty: BMElemType,

    /// flag array table as being dirty so we know when its safe to use it,
    /// or when it needs to be re-created.
    pub elem_table_dirty: BMElemType,

    // verts: MemPool<BMVert>,
    pub verts: BMVertSeq,
    pub edges: BMEdgeSeq,
    pub loops: BMLoopSeq,
    pub faces: BMFaceSeq,

    // element pools
    // struct BLI_mempool *vpool, *epool, *lpool, *fpool;

    /// mempool lookup tables (optional)
    /// index tables, to map indices to elements via
    /// BM_mesh_elem_table_ensure and associated functions.  don't
    /// touch this or read it directly.\
    /// Use BM_mesh_elem_table_ensure(), BM_vert/edge/face_at_index()
    pub vtable: Vec<BMVertMutP>,
    pub etable: Vec<BMEdgeMutP>,
    pub ftable: Vec<BMFaceMutP>,

    /// operator api stuff (must be all NULL or all alloc'd)
    // struct BLI_mempool *vtoolflagpool, *etoolflagpool, *ftoolflagpool;

    // use_toolflags: bool,

    pub vdata: CustomData,
    pub edata: CustomData,
    pub ldata: CustomData,
    pub fdata: CustomData,

    /// stored in BMEditMesh too, this is a bit confusing,
    /// make sure they're in sync!
    /// Only use when the edit mesh cant be accessed - campbell.
    pub select_mode: BMElemType,

    // ID of the shape key this bmesh came from
    // int shapenr;
}

impl Default for BMesh {
    fn default() -> Self {
        BMesh {
            elem_index_dirty: BMElemType(0),
            elem_table_dirty: BMElemType(0),

            verts: BMVertSeq(MemPool::with_chunk_size(consts_default::CHUNK_SIZE * 2)),
            edges: BMEdgeSeq(MemPool::with_chunk_size(consts_default::CHUNK_SIZE * 2)),
            loops: BMLoopSeq(MemPool::with_chunk_size(consts_default::CHUNK_SIZE * 4)),
            faces: BMFaceSeq(MemPool::with_chunk_size(consts_default::CHUNK_SIZE)),

            // these may be never needed, use zero capacity.
            vtable: Vec::with_capacity(0),
            etable: Vec::with_capacity(0),
            ftable: Vec::with_capacity(0),

            vdata: CustomData::default(),
            edata: CustomData::default(),
            ldata: CustomData::default(),
            fdata: CustomData::default(),

            select_mode: BM_VERT,
        }
    }
}

use self::bmesh_bitflag_bmelem_hflag::*;
use self::bmesh_bitflag_bmelem_htype::*;
use self::bmesh_bitflag_bmelem_api_flag::*;

/// BMHeader.htype
pub mod bmesh_bitflag_bmelem_htype {
    struct_bitflag_impl!(pub struct BMElemType(pub u8));


    pub const BM_VERT: BMElemType = BMElemType(1);
    pub const BM_EDGE: BMElemType = BMElemType(2);
    pub const BM_LOOP: BMElemType = BMElemType(4);
    pub const BM_FACE: BMElemType = BMElemType(8);

    pub const BM_HTYPE_ALL: BMElemType = BMElemType(1 | 2 | 4 | 8);
    pub const BM_HTYPE_ALL_NOLOOP: BMElemType = BMElemType(1 | 2 | 8);
}

/// BMHeader.hflag
pub mod bmesh_bitflag_bmelem_hflag {
    struct_bitflag_impl!(pub struct BMElemFlag(pub u8));

    pub const BM_ELEM_SELECT: BMElemFlag = BMElemFlag(1 << 0);
    pub const BM_ELEM_HIDDEN: BMElemFlag = BMElemFlag(1 << 1);
    pub const BM_ELEM_SEAM:   BMElemFlag = BMElemFlag(1 << 2);

    /// used for faces and edges, note from the user POV,
    /// this is a sharp edge when disabled
    pub const BM_ELEM_SMOOTH: BMElemFlag = BMElemFlag(1 << 3);

    /// internal flag, used for ensuring correct normals
    /// during multires interpolation, and any other time
    /// when temp tagging is handy.
    /// always assume dirty & clear before use.
    pub const BM_ELEM_TAG: BMElemFlag     = BMElemFlag(1 << 4);

    /// edge display
    pub const BM_ELEM_DRAW: BMElemFlag    = BMElemFlag(1 << 5);

    /// spare tag, assumed dirty, use define in each function to name based on use
    // _BM_ELEM_TAG_ALT: BMElemFlag = BMElemFlag(1 << 6),  // UNUSED

    /// for low level internal API tagging,
    /// since tools may want to tag verts and
    /// not have functions clobber them
    pub const BM_ELEM_INTERNAL_TAG: BMElemFlag = BMElemFlag(1 << 7);
}

/// BMHeader.api_flag
pub mod bmesh_bitflag_bmelem_api_flag {
     // Internal BMHeader.api_flag
     // \note Ensure different parts of the API do not conflict
     // o ng these internal flags!*/

    struct_bitflag_impl!(pub struct BMElemApiFlag(pub u8));

    // join faces
    pub const FLAG_JF: BMElemApiFlag          = BMElemApiFlag(1 << 0);
    // make face
    pub const FLAG_MF: BMElemApiFlag          = BMElemApiFlag(1 << 1);
    // make face, vertex
    pub const FLAG_MV: BMElemApiFlag          = BMElemApiFlag(1 << 1);
    // general overlap flag
    pub const FLAG_OVERLAP: BMElemApiFlag     = BMElemApiFlag(1 << 2);
    // general walk flag (keep clean)
    pub const FLAG_WALK: BMElemApiFlag        = BMElemApiFlag(1 << 3);
    // same as _FLAG_WALK, for when a second tag is needed
    #[allow(dead_code)]
    pub const FLAG_WALK_ALT: BMElemApiFlag    = BMElemApiFlag(1 << 4);

    // reserved for bmesh_elem_check
    #[allow(dead_code)]
    pub const FLAG_ELEM_CHECK: BMElemApiFlag  = BMElemApiFlag(1 << 7);
}
