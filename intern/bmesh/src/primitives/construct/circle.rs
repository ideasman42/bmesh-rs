// Licensed: Apache 2.0

use prelude::*;
use primitives::local_prelude::*;

pub fn construct(
    bm: &mut BMesh,
    scale: f64,
    segs: usize,
    cap_ends: bool,
    cap_tris: bool,
    r_geom: &mut BMeshPartialGeom,
) {
    // use std::f64::consts;
    let mut coords: Vec<[f64; 3]> = Vec::with_capacity(
        segs + { if cap_tris { 1 } else { 0 } }
    );

    let phi_step = (2.0 * ::std::f64::consts::PI) / (segs as f64);

    for i in 0..segs {
        let phi = phi_step * (i as f64);

        coords.push([
            -scale * phi.sin(),
            scale * phi.cos(),
            0.0,
        ]);

    }

    let mut edge_indices: Vec<[usize; 2]> = Vec::with_capacity(0);
    let mut face_indices: Vec<usize> = Vec::with_capacity(0);

    if cap_ends {
        if cap_tris {
            face_indices.reserve(segs * 4);
            let mut i_prev = segs - 1;
            for i in 0..segs {
                face_indices.extend(&[i_prev, i, segs, NIL]);
                i_prev = i;
            }
            coords.push([0.0, 0.0, 0.0]);
        } else {
            face_indices.reserve(segs + 1);
            face_indices.extend((0..segs));
            face_indices.push(NIL);
        }
    } else {
        edge_indices.reserve(segs);
        let mut i_prev = segs - 1;
        for i in 0..segs {
            edge_indices.push([i_prev, i]);
            i_prev = i;
        }
    }

    mesh_from_data(
        bm,
        &coords[..],
        &edge_indices[..],
        &face_indices[..],
        r_geom,
    );
}
