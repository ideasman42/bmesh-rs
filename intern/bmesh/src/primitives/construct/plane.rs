// Licensed: Apache 2.0

use prelude::*;
use primitives::local_prelude::*;

pub fn construct(
    bm: &mut BMesh,
    scale: f64,
    r_geom: &mut BMeshPartialGeom,
) {
    let coords: Vec<[f64; 3]> = vec![
        [scale * -1.0, scale * -1.0, 0.0],
        [scale * -1.0, scale *  1.0, 0.0],
        [scale *  1.0, scale *  1.0, 0.0],
        [scale *  1.0, scale * -1.0, 0.0],
    ];
    let edge_indices: Vec<[usize; 2]> = vec![
    ];
    let face_indices: Vec<usize> = vec![
        0, 1, 2, 3, NIL,
    ];

    mesh_from_data(
        bm,
        &coords[..],
        &edge_indices[..],
        &face_indices[..],
        r_geom,
    );
}
