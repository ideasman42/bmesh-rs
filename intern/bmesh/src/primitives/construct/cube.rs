// Licensed: Apache 2.0

use prelude::*;
use primitives::local_prelude::*;

pub fn construct(
    bm: &mut BMesh,
    scale: f64,
    r_geom: &mut BMeshPartialGeom,
) {
    let mut coords: Vec<[f64; 3]> = Vec::with_capacity(8);
    for x in &[-scale, scale] {
        for y in &[-scale, scale] {
            for z in &[-scale, scale] {
                coords.push([*x, *y, *z]);
            }
        }
    }

    let face_indices: Vec<usize> = vec![
        0, 1, 3, 2, NIL,
        2, 3, 7, 6, NIL,
        6, 7, 5, 4, NIL,
        4, 5, 1, 0, NIL,
        2, 6, 4, 0, NIL,
        7, 3, 1, 5, NIL,
    ];

    mesh_from_data(
        bm,
        &coords[..],
        &[],
        &face_indices[..],
        r_geom,
    );
}
