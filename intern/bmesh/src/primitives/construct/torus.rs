// Licensed: Apache 2.0

use prelude::*;
use primitives::local_prelude::*;

use math_misc::{
    axis_angle_to_mat3_single,
    mul_m3_v3,
};

pub fn construct(
    bm: &mut BMesh,
    major_rad: f64,
    minor_rad: f64,
    major_seg: usize,
    minor_seg: usize,
    r_geom: &mut BMeshPartialGeom,
) {

    let pi_2 = 2.0 * ::std::f64::consts::PI;

    let mut i1 = 0;

    let tot_coords = major_seg * minor_seg;

    let mut coords: Vec<[f64; 3]> = Vec::with_capacity(tot_coords);
    let mut face_indices: Vec<usize> = Vec::with_capacity(tot_coords * 5);

    for major_index in 0..major_seg {
        let matrix = axis_angle_to_mat3_single(
            'Z', (major_index as f64 / major_seg as f64) * pi_2);

        for minor_index in 0..minor_seg {
            let angle = pi_2 * (minor_index as f64 / minor_seg as f64);

            coords.push(
                mul_m3_v3(
                    &matrix,
                    &[major_rad + (angle.cos() * minor_rad),
                      0.0,
                      angle.sin() * minor_rad,
                    ],
                )
            );

            let mut i2;
            let mut i3;
            let mut i4;

            if minor_index + 1 == minor_seg {
                i2 = major_index * minor_seg;
                i3 = i1 + minor_seg;
                i4 = i2 + minor_seg;
            } else {
                i2 = i1 + 1;
                i3 = i1 + minor_seg;
                i4 = i3 + 1;
            }

            if i2 >= tot_coords {
                i2 = i2 - tot_coords;
            }
            if i3 >= tot_coords {
                i3 = i3 - tot_coords;
            }
            if i4 >= tot_coords {
                i4 = i4 - tot_coords;
            }

            face_indices.extend(&[i1, i3, i4, i2, NIL]);

            i1 += 1;
        }
    }

    debug_assert!(coords.len() == tot_coords);
    debug_assert!(face_indices.len() == tot_coords * 5);

    mesh_from_data(
        bm,
        &coords[..],
        &[],
        &face_indices[..],
        r_geom,
    );
}
