// Licensed: Apache 2.0

// Utility functions, to be used for mesh creation.

use prelude::*;
use primitives::local_prelude::*;


/// Terminator for the face array,
/// so arbitrary sized faces can be stored in a flat array.
pub const NIL: usize = ::std::usize::MAX;

/// Initialize mesh data from vert/edge/face indices.
pub fn mesh_from_data(
    bm: &mut BMesh,
    coords: &[[f64; 3]],
    edge_indices: &[[usize; 2]],
    face_indices: &[usize],
    r_geom: &mut BMeshPartialGeom,
) {
    let mut verts = Vec::with_capacity(coords.len());
    for co in coords {
        verts.push(bm.verts.new(co, None, BMElemCreate::NOP));
    }

    let mut edges = Vec::with_capacity(edge_indices.len());
    for e in edge_indices {
        edges.push(bm.edges.new(&[verts[e[0]], verts[e[1]]], None, BMElemCreate::NOP));
    }
    if !face_indices.is_empty() {
        let mut faces_len = 1;
        let mut faces_last_index = 0;
        let mut face_len_max = 0;
        for (index, i) in face_indices.iter().enumerate() {
            if *i == NIL {
                faces_len += 1;
                let f_len = index - faces_last_index;
                assert!(f_len >= 3);
                face_len_max = ::std::cmp::max(face_len_max, f_len);
                faces_last_index = index;
            }
        }
        // ensure a trailing NIL
        assert!(*face_indices.last().unwrap() == NIL);
        let mut faces: Vec<BMFaceMutP> = Vec::with_capacity(faces_len);
        let mut f_verts: Vec<BMVertMutP> = Vec::with_capacity(face_len_max);
        let mut f_edges: Vec<BMEdgeMutP> = Vec::with_capacity(face_len_max);
        let mut f_indices: Vec<usize> = Vec::with_capacity(face_len_max);
        for i in face_indices {
            if *i == NIL {
                for vi in f_indices.iter().cloned() {
                    f_verts.push(verts[vi]);
                }

                unsafe {
                    f_edges.set_len(f_verts.len());
                }

                {
                    let mut i_prev = f_verts.len() - 1;
                    for i in 0..f_verts.len() {
                        let v1_src = unsafe { *f_verts.get_unchecked(i_prev) };
                        let v2_src = unsafe { *f_verts.get_unchecked(i) };
                        let e_dst = unsafe { f_edges.get_unchecked_mut(i_prev) };
                        *e_dst = {
                            let mut e_new = bm.edges.get_mut(v1_src, v2_src);
                            if e_new == null() {
                                e_new = bm.edges.new(&[v1_src, v2_src], None, BMElemCreate::NOP);
                                edges.push(e_new);
                            }
                            e_new
                        };
                        i_prev = i;
                    }
                }

                faces.push(bm.faces.new(&f_verts[..], &f_edges[..], None, BMElemCreate::NOP));

                f_verts.clear();
                f_edges.clear();
                f_indices.clear();
            } else {
                f_indices.push(*i);
            }
        }

        if let Some(ref mut r_faces) = r_geom.faces {
            r_faces.extend(faces);
        }
    }

    if let Some(ref mut r_verts) = r_geom.verts {
        r_verts.extend(verts);
    }

    if let Some(ref mut r_edges) = r_geom.edges {
        r_edges.extend(edges);
    }
}
