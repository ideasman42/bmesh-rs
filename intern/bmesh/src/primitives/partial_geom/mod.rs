// Licensed: Apache 2.0

use prelude::*;

mod marking_builder;

use self::marking_builder::*;

pub struct BMeshPartialGeom {
    pub verts: Option<Vec<BMVertMutP>>,
    pub edges: Option<Vec<BMEdgeMutP>>,
    pub faces: Option<Vec<BMFaceMutP>>,
}

impl Default for BMeshPartialGeom {
    fn default() -> Self {
        BMeshPartialGeom {
            verts: Some(Vec::new()),
            edges: Some(Vec::new()),
            faces: Some(Vec::new()),
        }
    }
}

impl BMeshPartialGeom {
    #[allow(dead_code)]
    fn empty() -> BMeshPartialGeom {
        BMeshPartialGeom {
            verts: None,
            edges: None,
            faces: None,
        }
    }

    // Matching BMesh.elem_hflag_edit_builder
    #[inline]
    pub fn elem_hflag_edit_builder(
        &mut self,
        hflag: BMElemFlag,
    ) -> BMPartialGeomElemEditFlagBuilder {
        return bm_partial_geom_elem_hflag_edit_builder(self, hflag);
    }
}
