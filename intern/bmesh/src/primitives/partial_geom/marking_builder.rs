// Licensed: GPL v2+
// (c) Blender Foundation

use prelude::*;

use super::BMeshPartialGeom;

// ----------------------------------------------------------------------------
// BMElem Edit Flag Builder

pub struct BMPartialGeomElemEditFlagBuilder<'a> {
    bm_geom: &'a mut BMeshPartialGeom,

    hflag: BMElemFlag,
    hflag_skip: BMElemFlag,
    // Could use ArrayVec[BMELEM_EDIT_FLAG_NUM]
    actions: Vec<elem_edit_flag_builder::BMElemEditFlag>,
}

mod elem_edit_flag_builder {
    use super::BMeshPartialGeom;
    use super::BMPartialGeomElemEditFlagBuilder;

    // Internal, added via builder methods
    //
    // - TODO: set from closure
    #[repr(u8)]
    pub enum BMElemEditFlag {
        SetVertFromValue(bool),
        SetEdgeFromValue(bool),
        SetFaceFromValue(bool),
    }
    // Must be updated manually, grr!
    // TODO: use fancy proc_macro when its in stable:
    // http://stackoverflow.com/a/41638362/432509
    pub const BMELEM_EDIT_FLAG_NUM: usize = 3;

    use prelude::*;

    macro_rules! build_fn_bool {
        ($fn_id:ident, $fn_enum:ident) => {
            pub fn $fn_id(mut self, value: bool) -> BMPartialGeomElemEditFlagBuilder<'a> {
                self.actions.push(BMElemEditFlag::$fn_enum(value));
                return self;
            }
        }
    }

    impl <'a> BMPartialGeomElemEditFlagBuilder<'a> {
        pub fn new<'b>(
            bm_geom: &'b mut BMeshPartialGeom,
            hflag: BMElemFlag,
        ) -> BMPartialGeomElemEditFlagBuilder<'b> {
            return BMPartialGeomElemEditFlagBuilder::<'b> {
                bm_geom: bm_geom,
                hflag: hflag,
                hflag_skip: BMElemFlag(0),
                actions: Vec::with_capacity(BMELEM_EDIT_FLAG_NUM),
            };
        }

        pub fn exec(self) {
            macro_rules! is_elem_skip {
                ($ele:ident) => {
                    (self.hflag_skip == 0) ||
                    (self.hflag_skip & $ele.head.hflag) == 0
                }
            }

            macro_rules! set_elem_from_value {
                ($ele_seq:ident, $value:ident) => {
                    if let Some(ref mut ele_seq) = self.bm_geom.$ele_seq {
                        for ele in ele_seq.iter_mut() {
                            if is_elem_skip!(ele) {
                                ele.head.hflag.set(self.hflag, $value);
                            }
                        }
                    } else {
                        unreachable!();
                    }
                }
            }

            for action in &self.actions {
                use self::BMElemEditFlag::*;
                match action {
                    // Elem From Value
                    &SetVertFromValue(value) => {set_elem_from_value!(verts, value)},
                    &SetEdgeFromValue(value) => {set_elem_from_value!(edges, value)},
                    &SetFaceFromValue(value) => {set_elem_from_value!(faces, value)},
                }
            }
        }

        pub fn skip(mut self, hflag_skip: BMElemFlag) -> Self {
            debug_assert!(self.hflag_skip == 0);
            debug_assert!(self.actions.is_empty());
            self.hflag_skip = hflag_skip;
            self
        }

        build_fn_bool!(set_vert_from_value, SetVertFromValue);
        build_fn_bool!(set_edge_from_value, SetEdgeFromValue);
        build_fn_bool!(set_face_from_value, SetFaceFromValue);
    }
}

pub fn bm_partial_geom_elem_hflag_edit_builder(
    bm_geom: &mut BMeshPartialGeom,
    hflag: BMElemFlag,
) -> BMPartialGeomElemEditFlagBuilder {
    return BMPartialGeomElemEditFlagBuilder::new(bm_geom, hflag);
}
