// Licensed: Apache 2.0


// Only expose within this module,
// grab-bag of helpers for mesh creation.
mod local_prelude {
    pub use primitives::partial_geom::BMeshPartialGeom;
    pub use primitives::util::NIL;
    pub use primitives::util::mesh_from_data;
}


// ----------------
// Partial Geometry
mod partial_geom;
pub use self::partial_geom::BMeshPartialGeom;


// ---------------
// Primitive Utils
mod util;


// ----------------
// Primitive Shapes

mod construct;

///
/// Simply expand:
///
///    pub use self::construct::foo::construct as foo;
macro_rules! mod_primitive {
    ($id:ident) => {
        pub use self::construct::$id::construct as $id;
    }
}

mod_primitive!(circle);
mod_primitive!(cube);
mod_primitive!(edge);
mod_primitive!(plane);
mod_primitive!(torus);

