// Licensed: GPL v2+

// Delete API, note this doesn't use Blender's code,
// instead use the builder pattern.

//
// BMO_mesh_delete_oflag_tagged(BMesh *bm, const short oflag, const char htype);
// BM_mesh_delete_hflag_tagged(BMesh *bm, const char hflag, const char htype);
//
// BMO_mesh_delete_oflag_context(BMesh *bm, const short oflag, const int type);
// BM_mesh_delete_hflag_context(BMesh *bm, const char hflag, const int type);

use prelude::*;

pub struct BMElemDeleteBuilder<'a> {
    bm_internal: &'a mut BMesh,
    actions: Vec<delete_builder::BMElemDeleteFn<'a>>,
}

pub fn delete_builder(bm: &mut BMesh) -> BMElemDeleteBuilder {
    return BMElemDeleteBuilder::new(bm);
}

mod delete_builder {
    use prelude::*;
    use super::{
        BMElemDeleteBuilder,
    };

    pub enum BMElemDeleteFn<'a> {
        Vert { filter_fn: Box<Fn(BMVertMutP) -> bool + 'a> },
        Edge { filter_fn: Box<Fn(BMEdgeMutP) -> bool + 'a> },
        Face { filter_fn: Box<Fn(BMFaceMutP) -> bool + 'a> },
        EdgeLoose { filter_fn: Box<Fn(BMEdgeMutP) -> bool + 'a> },
        FaceLoose { filter_fn: Box<Fn(BMFaceMutP) -> bool + 'a> },
    }

    impl <'a> BMElemDeleteBuilder<'a> {
        pub fn new<'b>(bm: &'b mut BMesh) -> BMElemDeleteBuilder<'b> {
            return BMElemDeleteBuilder::<'b> {
                bm_internal: bm,
                actions: Vec::new(),
            };
        }

        pub fn exec(mut self) {
            let mut bm = PtrMut(self.bm_internal);
            for action in self.actions.drain(..) {
                match action {
                    BMElemDeleteFn::Vert { filter_fn } => {
                        for v in bm.clone().verts.iter_mut() {
                            if filter_fn(v) {
                                bm.verts.remove(v);
                            }
                        }
                    },
                    BMElemDeleteFn::Edge { filter_fn } => {
                        for e in bm.clone().edges.iter_mut() {
                            if filter_fn(e) {
                                bm.edges.remove(e);
                            }
                        }
                    },
                    BMElemDeleteFn::Face { filter_fn } => {
                        for f in bm.clone().faces.iter_mut() {
                            if filter_fn(f) {
                                bm.faces.remove(f);
                            }
                        }
                    },
                    BMElemDeleteFn::EdgeLoose { filter_fn } => {
                        for e in bm.clone().edges.iter_mut() {
                            if filter_fn(e) {
                                bm.edges.remove_loose(e);
                            }
                        }
                    },
                    BMElemDeleteFn::FaceLoose { filter_fn } => {
                        for f in bm.clone().faces.iter_mut() {
                            if filter_fn(f) {
                                bm.faces.remove_loose(f);
                            }
                        }
                    },
                }
            }
        }

        pub fn verts<F: Fn(BMVertMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.actions.push(BMElemDeleteFn::Vert {filter_fn: Box::new(filter_fn),});
            self
        }
        pub fn edges<F: Fn(BMEdgeMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.actions.push(BMElemDeleteFn::Edge {filter_fn: Box::new(filter_fn),});
            self
        }
        pub fn faces<F: Fn(BMFaceMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.actions.push(BMElemDeleteFn::Face {filter_fn: Box::new(filter_fn),});
            self
        }
        pub fn edges_loose<F: Fn(BMEdgeMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.actions.push(BMElemDeleteFn::EdgeLoose {filter_fn: Box::new(filter_fn),});
            self
        }
        pub fn faces_loose<F: Fn(BMFaceMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.actions.push(BMElemDeleteFn::FaceLoose {filter_fn: Box::new(filter_fn),});
            self
        }
    }
}
