// Licensed: GPL v2+

use prelude::*;

pub struct BMEdgeFaceAddBuilder<'a> {
    bm_internal: &'a mut BMesh,
    filter_vert_fn: Option<Box<Fn(BMVertMutP) -> bool + 'a>>,
    filter_edge_fn: Option<Box<Fn(BMEdgeMutP) -> bool + 'a>>,

    out_edge_fn: Option<Box<Fn(BMEdgeMutP) + 'a>>,
    out_face_fn: Option<Box<Fn(BMFaceMutP) + 'a>>,
}

pub fn edge_face_add_builder(bm: &mut BMesh) -> BMEdgeFaceAddBuilder {
    return BMEdgeFaceAddBuilder::new(bm);
}

mod edge_face_add_builder {
    use prelude::*;
    use super::{
        BMEdgeFaceAddBuilder,
    };

    impl <'a> BMEdgeFaceAddBuilder<'a> {
        pub fn new<'b>(bm: &'b mut BMesh) -> BMEdgeFaceAddBuilder<'b> {
            return BMEdgeFaceAddBuilder::<'b> {
                bm_internal: bm,
                filter_vert_fn: None,
                filter_edge_fn: None,
                out_edge_fn: None,
                out_face_fn: None,
            };
        }

        pub fn exec(self) {
            let mut bm = PtrMut(self.bm_internal);
            let mut verts = Vec::<BMVertMutP>::new();
            let mut edges = Vec::<BMEdgeMutP>::new();

            if let Some(filter_vert_fn) = self.filter_vert_fn {
                for v in bm.verts.iter_mut() {
                    if filter_vert_fn(v) {
                        verts.push(v);
                    }
                }
            }
            if let Some(filter_edge_fn) = self.filter_edge_fn {
                for e in bm.edges.iter_mut() {
                    if filter_edge_fn(e) {
                        edges.push(e);
                    }
                }
            }

            if verts.len() == 2 {
                let e = bm.edges.new(&[verts[0], verts[1]], None, BMElemCreate::NO_DOUBLE);
                if let Some(ref out_edge_fn) = self.out_edge_fn {
                    out_edge_fn(e);
                }
                return;
            }

            if verts.len() == 3 {
                let mut f_edges: [BMEdgeMutP; 3] = [null_mut(); 3];
                bm.edges.from_verts_ensure(&mut f_edges, &verts);
                let f = bm.faces.new(&verts[..], &f_edges, None, BMElemCreate::NO_DOUBLE);
                if let Some(ref out_face_fn) = self.out_face_fn {
                    out_face_fn(f);
                }
                if let Some(ref out_edge_fn) = self.out_edge_fn {
                    for e in &f_edges {
                        out_edge_fn(*e);
                    }
                }
                return;
            }

        }

        pub fn verts<F: Fn(BMVertMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.filter_vert_fn = Some(Box::new(filter_fn));
            self
        }
        pub fn edges<F: Fn(BMEdgeMutP) -> bool + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.filter_edge_fn = Some(Box::new(filter_fn));
            self
        }
        pub fn edges_out<F: Fn(BMEdgeMutP) + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.out_edge_fn = Some(Box::new(filter_fn));
            self
        }
        pub fn faces_out<F: Fn(BMFaceMutP) + 'a>(
            mut self, filter_fn: F,
        ) -> Self {
            self.out_face_fn = Some(Box::new(filter_fn));
            self
        }
    }
}
