// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use plain_ptr::{
    PtrMut,
    PtrConst,
};


pub struct MDisps {
    _totdisp: u32,
/*
    // Strange bug in SDNA: if disps pointer comes first, it fails to see totdisp
    int totdisp;
    int level;
    float (*disps)[3];

    // Used for hiding parts of a multires mesh.
    // Essentially the multires equivalent of MVert.flag's ME_HIDE bit.
    // NOTE: This is a bitmap, keep in sync with type used in BLI_bitmap.h
    unsigned int *hidden;
 */

}

pub const CD_INVALID_OFFSET: isize = -1;

// TODO, move into cd_types block
pub const CD_MDISPS: u32 = 0;


#[derive(Default)]
pub struct CustomDataElem {
    _todo: u8,
}

#[derive(Debug)]
pub struct CustomData {
    _todo: usize,
}

impl Default for CustomData {
    fn default() -> Self {
        CustomData {
            _todo: 0,
        }
    }
}

pub fn CustomData_bmesh_interp(
    _data: &mut CustomData,
    _src_blocks: &[PtrConst<CustomDataElem>],
    _weights: &[f64],
    _sub_weights: Option<&[f64]>,
    _dst_block: PtrMut<CustomDataElem>,
) {
    // pass
}

pub fn CustomData_get_offset(
    _data: &CustomData,
    _cd_type: u32,
) -> isize {
    return CD_INVALID_OFFSET;
}
