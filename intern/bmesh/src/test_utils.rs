// Licensed: Apache 2.0

use prelude::*;

use primitives::{
    BMeshPartialGeom,
};

pub const INVALID: usize = ::std::usize::MAX;

pub fn geom_len_vert_edge_face_(geom: &BMeshPartialGeom) -> (usize, usize, usize) {
    (
        if let Some(ref v) = geom.verts { v.len() } else { INVALID },
        if let Some(ref v) = geom.edges { v.len() } else { INVALID },
        if let Some(ref v) = geom.faces { v.len() } else { INVALID },
    )
}

#[macro_export]
macro_rules! assert_geom_len_eq {
    ($g:expr, v:$v:expr, e:$e:expr, f:$f:expr) => {
        assert_eq!(($v, $e, $f), ::bmesh::test_utils::geom_len_vert_edge_face_($g));
    }
}

#[macro_export]
macro_rules! assert_mesh_len_eq {
    ($bm:expr, v:$v:expr, e:$e:expr, f:$f:expr) => {
        assert_eq!(($v, $e, $f),
                   ($bm.verts.len(), $bm.edges.len(), $bm.faces.len()));
    };
    ($bm:expr, v:$v:expr, e:$e:expr, f:$f:expr, l:$l:expr) => {
        assert_eq!(($v, $e, $f, $l),
                   ($bm.verts.len(), $bm.edges.len(), $bm.faces.len(), $bm.loops.len()));
    };
}

pub fn export_mesh_to_obj_from_file(
    bm: &mut BMesh,
    mut file: &::std::fs::File,
) -> Result<(), ::std::io::Error> {

    use std::io::prelude::Write;

    // f.write(b"hello")?;
    for v in bm.verts.iter() {
        file.write_fmt(format_args!(
            "v {} {} {}\n",
            v.co[0], v.co[1], v.co[2],
        ))?;
    }

    bm.index_update(BM_VERT);


    for e in bm.edges.iter() {
        if e.is_wire() {
            file.write_fmt(format_args!(
                "f {} {}",
                e.verts[0].head.index + 1,
                e.verts[1].head.index + 1,
            ))?;
        }
    }

    for f in bm.faces.iter() {
        file.write(b"f")?;
        bm_iter_loops_of_face_cycle!(f, l_iter, {
            file.write_fmt(format_args!(
                " {}",
                l_iter.v.head.index + 1,
            ))?;
        });
        file.write(b"\n")?;
    }

    return Ok(());
}

pub fn export_mesh_to_obj_from_filepath(
    bm: &mut BMesh,
    filepath: &str,
) -> Result<(), ::std::io::Error> {
    let file = ::std::fs::File::create(filepath).expect("open failed");
    return export_mesh_to_obj_from_file(bm, &file);
}
