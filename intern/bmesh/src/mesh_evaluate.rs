// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use custom_data::MDisps;

///
/// Flip a single MLoop's #MDisps structure,
/// low level function to be called from face-flipping code
/// which re-arranged the mdisps themselves.
///
/// 'BKE_mesh_mdisp_flip' in Blender.
///
pub fn bke_mesh_mdisp_flip(
    _md: &mut MDisps,
    _use_loop_mdisp_flip: bool,
) {
    /* STUB */

/*
    if (unlikely!(md->totdisp == 0 || md->disps == 0)) {
        return;
    }

    const int sides = (int)sqrt(md->totdisp);
    float (*co)[3] = md->disps;

    for (int x = 0; x < sides; x++) {
        float *co_a, *co_b;

        for (int y = 0; y < x; y++) {
            co_a = co[y * sides + x];
            co_b = co[x * sides + y];

            swap_v3_v3(co_a, co_b);
            SWAP(float, co_a[0], co_a[1]);
            SWAP(float, co_b[0], co_b[1]);

            if (use_loop_mdisp_flip) {
                co_a[2] *= -1.0f;
                co_b[2] *= -1.0f;
            }
        }

        co_a = co[x * sides + x];

        SWAP(float, co_a[0], co_a[1]);

        if (use_loop_mdisp_flip) {
            co_a[2] *= -1.0f;
        }
    }
*/
}

