// Licensed: Apache 2.0

/// There are const/mut versions of each iterator.
/// `mut` versions use unsafe, which is OK in this instance
/// because all input is tested to be mutable too.



// Private macro, don't use outside this file.
// ugh... its used indirectly, and so must need to be exported.
#[macro_export]
macro_rules! _exec_iteration {
    ($code: block) => {
        {
            let mut loop_state = false;
            loop {
                if loop_state == true {
                    // Got back here after having preparing for break - it
                    // means a continue happened.
                    break;
                }
                // Prepare for break.
                loop_state = false;
                {
                    $code
                }
                // Quiet warning.
                if loop_state {}
                loop_state = true;
                break;
            }
            // Evaluate to true when neither break nor continue occurred.
            loop_state
        }
    }
}

// Private macros so we can have 'mut' variants.
#[macro_export]
macro_rules! _bm_iter_qual_mut   { ($v:ident) => { mut $v } }
#[macro_export]
macro_rules! _bm_iter_qual_none { ($v:ident) => { $v } }

#[macro_export]
macro_rules! bm_iter_loops_of_face_cycle {
    (_impl $f:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        let l_iter_first = $f.l_first.as_const();
        let mut l_iter = l_iter_first;
        loop {
            {
                let $l_iter_qual!($l_iter) = l_iter;
                if _exec_iteration!($code) == false {
                    break;
                }
            }
            l_iter = l_iter.next.as_const();
            if l_iter == l_iter_first {
                break;
            }
        }
    };
    ($f:expr, $l_iter:ident, $code:block) => {
        bm_iter_loops_of_face_cycle!(_impl $f, $l_iter, $code, _bm_iter_qual_none)
    };
    ($f:expr, mut $l_iter:ident, $code:block) => {
        bm_iter_loops_of_face_cycle!(_impl $f, $l_iter, $code, _bm_iter_qual_mut)
    };
}
#[macro_export]
macro_rules! bm_iter_loops_of_face_cycle_mut {
    (_impl $f:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        if false { let _: BMFaceMutP = $f; }
        bm_iter_loops_of_face_cycle!($f, l_iter, {
            let $l_iter_qual!($l_iter) = unsafe { l_iter.as_mut() };
            $code
        });
    };
    ($f:expr, $l_iter:ident, $code:block) => {
        bm_iter_loops_of_face_cycle_mut!(_impl $f, $l_iter, $code, _bm_iter_qual_none)
    };
    ($f:expr, mut $l_iter:ident, $code:block) => {
        bm_iter_loops_of_face_cycle_mut!(_impl $f, $l_iter, $code, _bm_iter_qual_mut)
    };
}

#[macro_export]
macro_rules! bm_iter_loops_of_edge_radial {
    (_impl $e:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        let l_iter_first = $e.l.as_const();
        if l_iter_first != ::std::ptr::null() {
            let mut l_iter = l_iter_first;
            loop {
                {
                    let $l_iter_qual!($l_iter) = l_iter;
                    if _exec_iteration!($code) == false {
                        break;
                    }
                }
                l_iter = l_iter.radial_next.as_const();
                if l_iter == l_iter_first {
                    break;
                }
            }
        }
    };
    ($e:expr, $l_iter:ident, $code:block) => {
        bm_iter_loops_of_edge_radial!(_impl $e, $l_iter, $code, _bm_iter_qual_none)
    };
    ($e:expr, mut $l_iter:ident, $code:block) => {
        bm_iter_loops_of_edge_radial!(_impl $e, $l_iter, $code, _bm_iter_qual_mut)
    };
}
#[macro_export]
macro_rules! bm_iter_loops_of_edge_radial_mut {
    (_impl $e:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        if false { let _: BMEdgeMutP = $e; }
        bm_iter_loops_of_edge_radial!($e, l_iter, {
            let $l_iter_qual!($l_iter) = unsafe { l_iter.as_mut() };
            $code
        });
    };
    ($e:expr, $l_iter:ident, $code:block) => {
        bm_iter_loops_of_edge_radial_mut!(_impl $e, $l_iter, $code, _bm_iter_qual_none)
    };
    ($e:expr, mut $l_iter:ident, $code:block) => {
        bm_iter_loops_of_edge_radial_mut!(_impl $e, $l_iter, $code, _bm_iter_qual_mut)
    };
}


#[macro_export]
macro_rules! bm_iter_loops_of_loop_radial {
    (_impl $l:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        let l_iter_first = $l.as_const();
        let mut l_iter = l_iter_first.radial_next.as_const();
        if l_iter != l_iter_first {
            loop {
                {
                    let $l_iter_qual!($l_iter) = l_iter;
                    if _exec_iteration!($code) == false {
                        break;
                    }
                }
                l_iter = l_iter.radial_next.as_const();
                if l_iter == l_iter_first {
                    break;
                }
            }
        }
    };
    ($l:expr, $l_iter:ident, $code:block) => {
        bm_iter_loops_of_loop_radial!(_impl $l, $l_iter, $code, _bm_iter_qual_none)
    };
    ($l:expr, mut $l_iter:ident, $code:block) => {
        bm_iter_loops_of_loop_radial!(_impl $l, $l_iter, $code, _bm_iter_qual_mut)
    };
}
#[macro_export]
macro_rules! bm_iter_loops_of_loop_radial_mut {
    (_impl $l:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        if false { let _: BMEdgeMutP = $l; }
        bm_iter_loops_of_loop_radial!($l, l_iter, {
            let $l_iter_qual($l_iter) = unsafe { l_iter.as_mut() };
            $code
        });
    };
    ($l:expr, $l_iter:ident, $code:block) => {
        bm_iter_loops_of_loop_radial_mut!(_impl $l, $l_iter, $code, _bm_iter_qual_none)
    };
    ($l:expr, mut $l_iter:ident, $code:block) => {
        bm_iter_loops_of_loop_radial_mut!(_impl $l, $l_iter, $code, _bm_iter_qual_mut)
    };
}


#[macro_export]
macro_rules! bm_iter_edges_of_vert {
    (_impl $v:expr, $e_iter:ident, $code:block, $e_iter_qual:ident) => {
        let e_iter_first = $v.e.as_const();
        let mut e_iter = e_iter_first;
        loop {
            {
                let $e_iter_qual!($e_iter) = e_iter;
                if _exec_iteration!($code) == false {
                    break;
                }
            }
            e_iter = BMVert::disk_edge_next(&$v, e_iter);
            if e_iter == e_iter_first {
                break;
            }
        }
    };
    ($v:expr, $e_iter:ident, $code:block) => {
        bm_iter_edges_of_vert!(_impl $v, $e_iter, $code, _bm_iter_qual_none)
    };
    ($v:expr, mut $e_iter:ident, $code:block) => {
        bm_iter_edges_of_vert!(_impl $v, $e_iter, $code, _bm_iter_qual_mut)
    };
}
#[macro_export]
macro_rules! bm_iter_edges_of_vert_mut {
    (_impl $v:expr, $e_iter:ident, $code:block, $e_iter_qual:ident) => {
        if false { let _: BMVertMutP = $v; }
        bm_iter_edges_of_vert!($v, e_iter, {
            let $e_iter_qual!($e_iter) = unsafe { e_iter.as_mut() };
            $code
        });
    };
    ($v:expr, $e_iter:ident, $code:block) => {
        bm_iter_edges_of_vert_mut!(_impl $v, $e_iter, $code, _bm_iter_qual_none)
    };
    ($v:expr, mut $e_iter:ident, $code:block) => {
        bm_iter_edges_of_vert_mut!(_impl $v, $e_iter, $code, _bm_iter_qual_mut)
    };
}


#[macro_export]
macro_rules! bm_iter_loops_of_vert {
    (_impl $v:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        let e_iter_first = $v.e.as_const();
        let mut e_iter = e_iter_first;
        'outer:
        loop {
            let l_iter_first = e_iter.l.as_const();
            if l_iter_first != ::std::ptr::null() {
                let mut l_iter = l_iter_first;
                loop {
                    if l_iter.v == $v {
                        let $l_iter_qual!($l_iter) = l_iter;
                        if _exec_iteration!($code) == false {
                            break 'outer;
                        }
                    }
                    l_iter = l_iter.radial_next.as_const();
                    if l_iter == l_iter_first {
                        break;
                    }
                }
            }
            e_iter = BMVert::disk_edge_next(&$v, e_iter);
            if e_iter == e_iter_first {
                break;
            }
        }
    };
    ($v:expr, $e_iter:ident, $code:block) => {
        bm_iter_loops_of_vert!(_impl $v, $e_iter, $code, _bm_iter_qual_none)
    };
    ($v:expr, mut $e_iter:ident, $code:block) => {
        bm_iter_loops_of_vert!(_impl $v, $e_iter, $code, _bm_iter_qual_mut)
    };
}
#[macro_export]
macro_rules! bm_iter_loops_of_vert_mut {
    (_impl $v:expr, $l_iter:ident, $code:block, $l_iter_qual:ident) => {
        if false { let _: BMVertMutP = $v; }
        bm_iter_loops_of_vert!($v, l_iter, {
            let $l_iter_qual!($l_iter) = unsafe { l_iter.as_mut() };
            $code
        });
    };
    ($v:expr, $e_iter:ident, $code:block) => {
        bm_iter_loops_of_vert_mut!(_impl $v, $e_iter, $code, _bm_iter_qual_none)
    };
    ($v:expr, mut $e_iter:ident, $code:block) => {
        bm_iter_loops_of_vert_mut!(_impl $v, $e_iter, $code, _bm_iter_qual_mut)
    };
}

#[macro_export]
macro_rules! bm_iter_verts_of_edge {
    (_impl $e:expr, $v_iter:ident, $code:block, $v_iter_qual:ident) => {
        for v_iter in &$e.verts {
            let $v_iter_qual!($v_iter) = v_iter;
            if _exec_iteration!($code) == false {
                break;
            }
        }
    };
    ($e:expr, $v_iter:ident, $code:block) => {
        bm_iter_verts_of_edge!(_impl $e, $v_iter, $code, _bm_iter_qual_none)
    };
    ($e:expr, mut $v_iter:ident, $code:block) => {
        bm_iter_verts_of_edge!(_impl $e, $v_iter, $code, _bm_iter_qual_mut)
    };
}
#[macro_export]
macro_rules! bm_iter_verts_of_edge_mut {
    (_impl $e:expr, $v_iter:ident, $code:block, $v_iter_qual:ident) => {
        if false { let _: BMEdgeMutP = $e; }
        bm_iter_verts_of_edge!($e, v_iter, {
            let $v_iter_qual!($v_iter) = unsafe { v_iter.as_mut() };
            $code
        });
    };
    ($e:expr, $v_iter:ident, $code:block) => {
        bm_iter_verts_of_edge_mut!(_impl $e, $v_iter, $code, _bm_iter_qual_none)
    };
    ($e:expr, mut $v_iter:ident, $code:block) => {
        bm_iter_verts_of_edge_mut!(_impl $e, $v_iter, $code, _bm_iter_qual_mut)
    };
}

