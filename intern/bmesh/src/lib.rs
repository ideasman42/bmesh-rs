// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

// ---------------------------------------------------------------------------
// Crates

extern crate mempool;

extern crate plain_ptr;

extern crate math_misc;
extern crate polyfill_2d;

#[macro_use]
extern crate util_macros;


// ---------------------------------------------------------------------------
// Macros

#[macro_use]
pub mod bmesh_iter_macros;
#[macro_use]
pub mod bmesh_flag_macros;
#[macro_use]
pub mod bmesh_data_macros;

// ---------------------------------------------------------------------------
// Public Modules


// high level API to expose
pub mod types;
pub mod primitives;
pub mod ops;


// shared utilities for tests to reuse
// #[cfg(test)]
#[macro_use]
pub mod test_utils;

// ---------
// Constants
//
// Note this isn't real nice, and they aren't used often.
// Users can explicitly import.
pub mod limits {
    pub const BM_LOOP_RADIAL_MAX: usize = 10000;
    pub const BM_LOOP_CYCLE_MAX: i32 = 10000;
}


/// For *typical* bmesh use.
pub mod prelude {
    pub use bmesh_class::types::*;
    pub use bmesh_class::consts::*;
    pub use bmesh_class::consts_default::*;
    pub use bmesh_class::fn_consts::*;
    pub use custom_data::*;
    pub use plain_ptr::{
        null_const,
        null_mut,
    };
    pub use ::std::ptr::null;
}

// ---------------------------------------------------------------------------
// Private Modules

// low-level modules
mod bmesh_class;

// internal non object-oriented b-mesh API
mod intern;

// custom mesh data

#[allow(non_snake_case)]
mod custom_data;

// only for mdisp, could move
mod mesh_evaluate;

