// Licensed: GPL v2+
// (c) Campbell Barton

/// External API for struct method access.
///
/// This module mainly wraps 'bmesh/intern' modules,
/// exposing them as methods to BMesh, BMVert, BMEdge, BMLoop and BMFace types.


// ----------------------------------------------------------------------------
// Element New/Remove

// ---
// New
//
// Topology aware `new` functions.

mod impl_intern_bmesh_core {
    use bmesh_class::types::*;
    use intern::bmesh_core::*;

    impl BMVertSeq {
        #[inline]
        pub fn new(
            &mut self,
            co: &[f64; 3],
            v_ref: Option<BMVertConstP>,
            create_flag: BMElemCreate,
        ) -> BMVertMutP {
            let bm = unsafe { parent_of_mut!(self, BMesh, verts) };
            return BM_vert_create(bm, co, v_ref, create_flag);
        }
    }

    impl BMEdgeSeq {
        #[inline]
        pub fn new(
            &mut self,
            verts: &[BMVertMutP; 2],
            e_ref: Option<BMEdgeConstP>,
            create_flag: BMElemCreate,
        ) -> BMEdgeMutP {
            let bm = unsafe { parent_of_mut!(self, BMesh, edges) };
            return BM_edge_create(bm, verts, e_ref, create_flag);
        }
    }

    // impl BMLoopSeq {
        /// Exception: this isn't public!
        /// Only used by `BMFaceSeq::create`.
    // }

    impl BMFaceSeq {
        #[inline]
        pub fn new(
            &mut self,
            verts: &[BMVertMutP],
            edges: &[BMEdgeMutP],
            f_ref: Option<BMFaceConstP>,
            create_flag: BMElemCreate,
        ) -> BMFaceMutP {
            let bm = unsafe { parent_of_mut!(self, BMesh, faces) };
            return BM_face_create(bm, verts, edges, f_ref, create_flag);
        }
    }

    // ------
    // Remove
    //
    // Topology aware removal.

    impl BMVertSeq {
        #[inline]
        pub fn remove(&mut self, v: BMVertMutP) {
            let bm = unsafe { parent_of_mut!(self, BMesh, verts) };
            BM_vert_kill(bm, v);
        }
    }
    impl BMEdgeSeq {
        #[inline]
        pub fn remove(&mut self, e: BMEdgeMutP) {
            let bm = unsafe { parent_of_mut!(self, BMesh, edges) };
            BM_edge_kill(bm, e);
        }
        #[inline]
        pub fn remove_loose(&mut self, e: BMEdgeMutP) {
            let bm = unsafe { parent_of_mut!(self, BMesh, edges) };
            BM_edge_kill_loose(bm, e);
        }
    }
    // impl BMLoopSeq {
    // }
    impl BMFaceSeq {
        #[inline]
        pub fn remove(&mut self, f: BMFaceMutP) {
            let bm = unsafe { parent_of_mut!(self, BMesh, faces) };
            BM_face_kill(bm, f);
        }
        #[inline]
        pub fn remove_loose(&mut self, f: BMFaceMutP) {
            let bm = unsafe { parent_of_mut!(self, BMesh, faces) };
            BM_face_kill_loose(bm, f);
        }
    }
}

// ----------------------------------------------------------------------------
// Element Marking Conventions

mod impl_intern_bmesh_marking {
    use bmesh_class::types::*;
    use bmesh_class::consts::*;
    use intern::bmesh_marking::*;
    use intern::bmesh_marking_builder::*;

    macro_rules! bm_elem_marking_hide_select_impl {
        ($($t:ty)*) => ($(
            impl $t {
                #[inline]
                pub fn is_hide(&self) -> bool {
                    (self.head.hflag & BM_ELEM_HIDDEN) != 0
                }
                #[inline]
                pub fn is_select(&self) -> bool {
                    (self.head.hflag & BM_ELEM_SELECT) != 0
                }
            }
        )*)
    }

    bm_elem_marking_hide_select_impl! { BMVert BMEdge BMFace }

    impl BMVert {
        #[inline]
        pub fn select_set(&mut self, select: bool) {
            BM_vert_select_set(PtrMut(self), select);
        }
        #[inline]
        pub fn hide_set(&mut self, hide: bool) {
            BM_vert_hide_set(PtrMut(self), hide);
        }
        #[inline]
        pub fn hide_set_noflush(&mut self, hide: bool) {
            BM_vert_hide_set_noflush(PtrMut(self), hide);
        }
    }

    impl BMEdge {
        #[inline]
        pub fn select_set(&mut self, select: bool, select_mode: BMElemType) {
            BM_edge_select_set(PtrMut(self), select, select_mode);
        }
        #[inline]
        pub fn select_set_noflush(&mut self, select: bool) {
            BM_edge_select_set_noflush(PtrMut(self), select);
        }
        #[inline]
        pub fn hide_set(&mut self, hide: bool) {
            BM_edge_hide_set(PtrMut(self), hide);
        }
        #[inline]
        pub fn hide_set_noflush(&mut self, hide: bool) {
            BM_edge_hide_set_noflush(PtrMut(self), hide);
        }
    }

    impl BMFace {
        #[inline]
        pub fn select_set(&mut self, select: bool, select_mode: BMElemType) {
            BM_face_select_set(PtrMut(self), select, select_mode);
        }
        #[inline]
        pub fn select_set_noflush(&mut self, select: bool) {
            BM_face_select_set_noflush(PtrMut(self), select);
        }
        #[inline]
        pub fn hide_set(&mut self, hide: bool) {
            BM_face_hide_set(PtrMut(self), hide);
        }
        #[inline]
        pub fn hide_set_noflush(&mut self, hide: bool) {
            BM_face_hide_set_noflush(PtrMut(self), hide);
        }
    }

    impl BMesh {
        #[inline]
        pub fn elem_hflag_edit_builder(&mut self, hflag: BMElemFlag) -> BMElemEditFlagBuilder {
            return BM_mesh_elem_hflag_edit_builder(self, hflag);
        }
        #[inline]
        pub fn elem_hflag_flush_builder(&mut self, hflag: BMElemFlag) -> BMElemFlushFlagBuilder {
            return BM_mesh_elem_hflag_flush_builder(self, hflag);
        }
    }
}


// ----------------------------------------------------------------------------
// Element Topology
//
// Functions to maintain topology when adding/removing elements.

mod impl_intern_bmesh_structure {
    /// Note: radial and disk editing is very low level
    /// we may not want to expose these.
    use bmesh_class::types::*;
    use intern::bmesh_structure::*;

    impl BMVert {
        #[inline]
        pub fn disk_edge_append(&mut self, e: BMEdgeMutP) {
            bmesh_disk_edge_append(e, PtrMut(self));
        }
        #[inline]
        pub fn disk_edge_remove(&mut self, e: BMEdgeMutP) {
            bmesh_disk_edge_remove(e, PtrMut(self));
        }

        #[inline]
        pub fn disk_edge_next_mut(&mut self, e: BMEdgeMutP) -> BMEdgeMutP {
            return bmesh_disk_edge_next_mut(e, PtrMut(self));
        }
        #[inline]
        pub fn disk_edge_prev_mut(&mut self, e: BMEdgeMutP) -> BMEdgeMutP {
            return bmesh_disk_edge_prev_mut(e, PtrMut(self));
        }

        #[inline]
        pub fn disk_edge_next(&self, e: BMEdgeConstP) -> BMEdgeConstP {
            return bmesh_disk_edge_next(e, PtrConst(self));
        }
        #[inline]
        pub fn disk_edge_prev(&self, e: BMEdgeConstP) -> BMEdgeConstP {
            return bmesh_disk_edge_prev(e, PtrConst(self));
        }
    }

    impl BMEdge {
        #[allow(dead_code)]
        #[inline]
        fn disk_link_from_vert_mut(&mut self, v: BMVertMutP) -> BMDiskLinkMutP {
            return bmesh_disk_edge_link_from_vert_mut(PtrMut(self), v);
        }
        #[allow(dead_code)]
        #[inline]
        fn disk_link_from_vert(&self, v: BMVertConstP) -> BMDiskLinkConstP {
            return bmesh_disk_edge_link_from_vert(PtrConst(self), v);
        }

        #[inline]
        pub fn radial_loop_append(&mut self, l: BMLoopMutP) {
            bmesh_radial_loop_append(PtrMut(self), l);
        }
        #[inline]
        pub fn radial_loop_remove(&mut self, l: BMLoopMutP) {
            bmesh_radial_loop_remove(PtrMut(self), l);
        }
    }
}

mod impl_intern_bmesh_queries {
    use bmesh_class::types::*;
    use intern::bmesh_queries::*;

    // --------
    // Elements

    impl BMVert {
        #[inline]
        pub fn edge_count(&self) -> usize {
            return BM_vert_edge_count(PtrConst(self));
        }
        #[inline]
        pub fn edge_count_ex(&self, count_max: usize) -> usize {
            return BM_vert_edge_count_ex(PtrConst(self), count_max);
        }
        #[inline]
        pub fn face_count(&self) -> usize {
            return BM_vert_face_count(PtrConst(self));
        }
        #[inline]
        pub fn face_count_ex(&self, count_max: usize) -> usize {
            return BM_vert_face_count_ex(PtrConst(self), count_max);
        }
    }

    impl BMEdge {
        #[inline]
        pub fn is_manifold(&self) -> bool {
            return BM_edge_is_manifold(PtrConst(self));
        }
        #[inline]
        pub fn is_boundary(&self) -> bool {
            return BM_edge_is_boundary(PtrConst(self));
        }
        #[inline]
        pub fn is_wire(&self) -> bool {
            return BM_edge_is_wire(PtrConst(self));
        }

        #[inline]
        pub fn other_vert<V>(&self, v: V) -> BMVertConstP
            where
            V: Into<BMVertConstP>,
        {
            return BM_edge_other_vert(PtrConst(self), v);
        }
        #[inline]
        pub fn contains_vert<V>(&self, v: V) -> bool
            where
            V: Into<BMVertConstP>,
        {
            return BM_vert_in_edge(PtrConst(self), v);
        }
        #[inline]
        pub fn contains_vert_pair<V>(&self, v1: V, v2: V) -> bool
            where
            V: Into<BMVertConstP>,
        {
            return BM_verts_in_edge(v1, v2, PtrConst(self));
        }
        #[inline]
        pub fn find_double(&self) -> BMEdgeConstP {
            return BM_edge_find_double(PtrConst(self));
        }
        #[inline]
        pub fn face_count(&self) -> usize {
            return BM_edge_face_count(PtrConst(self));
        }
        #[inline]
        pub fn face_count_ex(&self, count_max: usize) -> usize {
            return BM_edge_face_count_ex(PtrConst(self), count_max);
        }

        #[inline]
        pub fn calc_length(&self) -> f64 {
            return BM_edge_calc_length(PtrConst(self));
        }
        #[inline]
        pub fn calc_length_squared(&self) -> f64 {
            return BM_edge_calc_length_squared(PtrConst(self));
        }
    }

    impl BMLoop {
        #[inline]
        pub fn step_fan<E>(&self, e: E) -> (BMLoopConstP, BMEdgeConstP)
            where
            E: Into<BMEdgeConstP>,
        {
            return BM_vert_step_fan_loop(PtrConst(self), e);
        }
        #[inline]
        pub fn step_fan_mut(&mut self, e: BMEdgeMutP) -> (BMLoopMutP, BMEdgeMutP)
        {
            return BM_vert_step_fan_loop_mut(PtrMut(self), e);
        }

        #[inline]
        pub fn calc_face_angle(&self) -> f64
        {
            return BM_loop_calc_face_angle(PtrConst(self));
        }
        #[inline]
        pub fn is_convex(&self) -> bool
        {
            return BM_loop_is_convex(PtrConst(self));
        }
    }

    impl BMFace {
        #[inline]
        pub fn contains_vert<V>(&self, v: V) -> bool
            where
            V: Into<BMVertConstP>,
        {
            return BM_vert_in_face(v, PtrConst(self));
        }
        #[inline]
        pub fn contains_edge<E>(&self, e: E) -> bool
            where
            E: Into<BMEdgeConstP>,
        {
            return BM_edge_in_face(e, PtrConst(self));
        }
    }

    // ----------------
    // Element Sequence

    impl BMEdgeSeq {
        #[inline]
        pub fn get<V>(&mut self, v1: V, v2: V) -> BMEdgeConstP
            where
            V: Into<BMVertConstP>,
        {
            return BM_edge_exists(v1, v2);
        }
        #[inline]
        pub fn get_mut(&mut self, v1: BMVertMutP, v2: BMVertMutP) -> BMEdgeMutP
        {
            return BM_edge_exists_mut(v1, v2);
        }
    }

    impl BMFaceSeq {
        #[inline]
        pub fn get<V>(&mut self, varr: &[V]) -> BMFaceConstP
            where
            V: Into<BMVertConstP> + Copy,
        {
            return BM_face_exists(varr);
        }
        #[inline]
        pub fn get_mut(&mut self, varr: &[BMVertMutP]) -> BMFaceMutP
        {
            return BM_face_exists_mut(varr);
        }
    }
}

mod impl_intern_bmesh_construct {
    use bmesh_class::types::*;
    use intern::bmesh_construct::*;

    // ----------------
    // Element Sequence

    impl BMEdgeSeq {
        pub fn from_verts(
            &mut self,
            edge_arr: &mut [BMEdgeMutP],
            vert_arr: &[BMVertMutP],
        ) -> bool {
            let bm = unsafe { parent_of_mut!(self, BMesh, edges) };
            return BM_edges_from_verts(bm, edge_arr, vert_arr);
        }
    }

    impl BMEdgeSeq {
        pub fn from_verts_ensure(
            &mut self,
            edge_arr: &mut [BMEdgeMutP],
            vert_arr: &[BMVertMutP],
        ) {
            let bm = unsafe { parent_of_mut!(self, BMesh, edges) };
            BM_edges_from_verts_ensure(bm, edge_arr, vert_arr);
        }
    }
}

mod impl_intern_bmesh_polygon {
    use bmesh_class::types::*;
    use intern::bmesh_polygon::*;

    // ----------------
    // Element Sequence

    impl BMFace {
        #[inline]
        pub fn normal_update(&mut self) {
            BM_face_normal_update(PtrMut(self));
        }
        #[inline]
        pub fn calc_area(&self) -> f64 {
            return BM_face_calc_area(PtrConst(self));
        }
        #[inline]
        pub fn calc_center_median(&self) -> [f64; 3] {
            return BM_face_calc_center_mean(PtrConst(self));
        }
    }

    impl BMesh {

        #[inline]
        pub fn face_flip_ex(
            &mut self,
            f: BMFaceMutP,
            cd_loop_mdisp_offset: isize,
            use_loop_mdisp_flip: bool,
        ) {
            return BM_face_normal_flip_ex(self, f, cd_loop_mdisp_offset, use_loop_mdisp_flip);
        }
        #[inline]
        pub fn face_flip(
            &mut self,
            f: BMFaceMutP,
        ) {
            return BM_face_normal_flip(self, f);
        }


        #[inline]
        pub fn face_triangulate(
            &mut self,
            f: BMFaceMutP,
            r_faces_new: &mut Option<&mut Vec<BMFaceMutP>>,
            r_edges_new: &mut Option<&mut Vec<BMEdgeMutP>>,
            r_faces_double: &mut Vec<BMFaceMutP>,
            use_tag: bool,
        ) {
            BM_face_triangulate(
                self,
                f,
                r_faces_new,
                r_edges_new,
                r_faces_double,
                use_tag,
            );
        }
    }
}

// ----------------------------------------------------------------------------
// BMesh


mod bmesh_impl {
    use bmesh_class::{
        BMesh,
    };
    impl BMesh {
        #[inline]
        pub fn new() -> Self {
            let bm = BMesh::default();
            return bm;
        }

        #[inline]
        pub fn clear(&mut self) {
            use intern::bmesh_mesh::BM_mesh_clear;
            BM_mesh_clear(self);
        }

        #[inline]
        pub fn validate(&mut self) -> bool {
            use intern::bmesh_mesh_validate::BM_mesh_validate;
            return BM_mesh_validate(self);
        }
    }
}


mod bmesh_impl_mods {
    use bmesh_class::types::*;
    pub use bmesh_class::consts::*;

    impl BMesh {

        #[inline]
        pub fn vert_splice(
            &mut self,
            v_dst: BMVertMutP,
            v_src: BMVertMutP,
        ) -> bool {
            use intern::bmesh_core::BM_vert_splice;
            return BM_vert_splice(self, v_dst, v_src);
        }

        #[inline]
        pub fn vert_dissolve(
            &mut self,
            v: BMVertMutP,
        ) -> bool {
            use intern::bmesh_mods::BM_vert_dissolve;
            return BM_vert_dissolve(self, v);
        }

        #[inline]
        pub fn vert_separate(
            &mut self,
            v: BMVertMutP,
            e_in: &[BMEdgeMutP],
            copy_select: bool,
        ) -> Vec<BMVertMutP> {
            use intern::bmesh_core::BM_vert_separate;
            return BM_vert_separate(self, v, e_in, copy_select);
        }

        #[inline]
        pub fn edge_split(
            &mut self,
            e: BMEdgeMutP, v: BMVertMutP,
            fac: f64,
        ) -> (BMVertMutP, BMEdgeMutP) {
            use intern::bmesh_mods::BM_edge_split;
            return BM_edge_split(self, e, v, fac);
        }

        #[inline]
        pub fn face_split(
            &mut self,
            f: BMFaceMutP,
            l_a: BMLoopMutP,
            l_b: BMLoopMutP,
            e_ref: Option<BMEdgeConstP>,
            no_double: bool,
        ) -> (BMFaceMutP, BMLoopMutP) {
            use intern::bmesh_mods::BM_face_split;
            return BM_face_split(self, f, l_a, l_b, e_ref, no_double);
        }

        #[inline]
        pub fn face_join_n(
            &mut self,
            faces: &[BMFaceMutP],
            do_del: bool,
        ) -> BMFaceMutP {
            use intern::bmesh_core::BM_faces_join;
            return BM_faces_join(self, faces, do_del);
        }

        #[inline]
        pub fn face_join_pair(
            &mut self,
            l_a: BMLoopMutP,
            l_b: BMLoopMutP,
            do_del: bool,
        ) -> BMFaceMutP {
            use intern::bmesh_mods::BM_faces_join_pair;
            return BM_faces_join_pair(self, l_a, l_b, do_del);
        }

        #[inline]
        pub fn face_loop_separate(
            &mut self,
            l: BMLoopMutP,
        ) -> BMVertMutP {
            use intern::bmesh_mods::BM_face_loop_separate;
            return BM_face_loop_separate(self, l);
        }

        #[inline]
        pub fn face_loop_separate_multi(
            &mut self,
            l_arr: &[BMLoopMutP],
        ) -> BMVertMutP {
            use intern::bmesh_mods::BM_face_loop_separate_multi;
            return BM_face_loop_separate_multi(self, l_arr);
        }

        #[inline]
        pub fn normal_update(
            &mut self,
        ) {
            use intern::bmesh_mesh::BM_mesh_normals_update;
            return BM_mesh_normals_update(self);
        }
        #[inline]
        pub fn index_update(
            &mut self,
            htype: BMElemType,
        ) {
            use intern::bmesh_mesh::BM_mesh_elem_index_ensure;
            BM_mesh_elem_index_ensure(self, htype);
        }
    }
}

// ----------------------------------------------------------------------------
// Element Delete
//
// Helpers for removing elements.

/// For *typical* bmesh use.
pub mod prelude {
    pub use bmesh_class::types::*;
    pub use bmesh_class::consts::*;
    pub use bmesh_class::fn_consts::*;
    pub use custom_data::*;
}
