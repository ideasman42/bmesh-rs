// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton


use prelude::*;

use intern::bmesh_core::{
    BM_edge_create,
    BM_face_create,
};

use intern::bmesh_structure::{
    bmesh_disk_edge_next_mut,
};

use intern::bmesh_queries::{
    BM_edge_other_vert_mut,
};

use intern::bmesh_marking::{
    BM_elem_select_set,
};

#[allow(dead_code)]
pub fn BM_verts_from_edges( /* STUB */ ) {}

///
/// Fill in an edge array from a vertex array (connected polygon loop).
///
/// returns false if any edges aren't found.
///
pub fn BM_edges_from_verts(
    bm: &mut BMesh,
    edge_arr: &mut [BMEdgeMutP],
    vert_arr: &[BMVertMutP],
) -> bool {
    debug_assert!(edge_arr.len() == vert_arr.len());
    let len = vert_arr.len();
    let mut i_prev = len - 1;
    for i in 0..len {
        let e_dst = unsafe { edge_arr.get_unchecked_mut(i_prev) };
        let v1_src = unsafe { *vert_arr.get_unchecked(i_prev) };
        let v2_src = unsafe { *vert_arr.get_unchecked(i) };

        *e_dst = BM_edge_create(bm, &[v1_src, v2_src], None, BMElemCreate::NOP);
        if *e_dst == null() {
            return false;
        }

        i_prev = i;
    }

    return true;
}

///
/// Fill in an edge array from a vertex array (connected polygon loop).
/// Creating edges as-needed.
///
pub fn BM_edges_from_verts_ensure(
    bm: &mut BMesh,
    edge_arr: &mut [BMEdgeMutP],
    vert_arr: &[BMVertMutP],
) {
    debug_assert!(edge_arr.len() == vert_arr.len());
    let len = vert_arr.len();
    let mut i_prev = len - 1;
    for i in 0..len {
        let e_dst = unsafe { edge_arr.get_unchecked_mut(i_prev) };
        let v1_src = unsafe { *vert_arr.get_unchecked(i_prev) };
        let v2_src = unsafe { *vert_arr.get_unchecked(i) };

        *e_dst = BM_edge_create(bm, &[v1_src, v2_src], None, BMElemCreate::NO_DOUBLE);

        i_prev = i;
    }
}
#[allow(dead_code)]
pub fn BM_face_create_quad_tri( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_copy_shared( /* STUB */ ) {}

///
/// Given an array of edges,
/// order them using the winding defined by \a v1 & \a v2
/// into \a edges_sort & \a verts_sort.
///
/// All arrays must be \a len long.
///
fn bm_edges_sort_winding(
    v1: BMVertMutP,
    v2: BMVertMutP,
    edges: &[BMEdgeMutP],
    edges_sort: &mut[BMEdgeMutP],
    verts_sort: &mut[BMVertMutP],
) -> bool {
    use bmesh_class::bmesh_bitflag_bmelem_api_flag::{
        FLAG_MF,
        FLAG_MV,
    };

    // simulate 'goto error;'
'error:
loop {

    // all flags _must_ be cleared on exit!

    for e in edges {
        let mut e = e.clone();  // make borrow checker happy
        bm_elem_api_flag_enable!(e, FLAG_MF);
        bm_elem_api_flag_enable!(e.verts[0], FLAG_MV);
        bm_elem_api_flag_enable!(e.verts[1], FLAG_MV);
    }

    // find first edge
    let mut i = 0;
    let mut v_iter = v1;

    let mut e_first = v1.e;
    let mut e_iter = e_first;
    loop {
        if bm_elem_api_flag_test!(e_iter, FLAG_MF) &&
           BM_edge_other_vert_mut(e_iter, v_iter) == v2
        {
            i = 1;
            break;
        }
        e_iter = bmesh_disk_edge_next_mut(e_iter, v_iter);
        if e_iter == e_first {
            break;
        }
    }

    if i == 0 {
        break 'error;
    }

    i = 0;
    loop {
        // entering loop will always succeed
        if bm_elem_api_flag_test!(e_iter, FLAG_MF) {
            if unlikely!(bm_elem_api_flag_test!(v_iter, FLAG_MV) == false) {
                // vert is in loop multiple times
                break 'error;
            }

            bm_elem_api_flag_disable!(e_iter, FLAG_MF);
            edges_sort[i] = e_iter;

            bm_elem_api_flag_disable!(v_iter, FLAG_MV);
            verts_sort[i] = v_iter;

            i += 1;

            // walk onto the next vertex
            v_iter = BM_edge_other_vert_mut(e_iter, v_iter);
            if i == edges.len() {
                if unlikely!(v_iter != verts_sort[0]) {
                    break 'error;
                }
                break;
            }

            e_first = e_iter;
        }

        e_iter = bmesh_disk_edge_next_mut(e_iter, v_iter);
        if e_iter == e_first {
            break;
        }
    }

    if i == edges.len() {
        return true;
    }

}   // simulate goto!

    for e in edges {
        let mut e = e.clone();  // make borrow checker happy
        bm_elem_api_flag_disable!(e, FLAG_MF);
        bm_elem_api_flag_disable!(e.verts[0], FLAG_MV);
        bm_elem_api_flag_disable!(e.verts[1], FLAG_MV);
    }

    return false;
}

///
/// \brief Make NGon
///
/// Makes an ngon from an unordered list of edges.
/// Verts \a v1 and \a v2 define the winding of the new face.
///
/// \a edges are not required to be ordered, simply to to form
/// a single closed loop as a whole.
///
/// \note While this function will work fine when the edges
/// are already sorted, if the edges are always going to be sorted,
/// #BM_face_create should be considered over this function as it
/// avoids some unnecessary work.
///
pub fn BM_face_create_ngon(
    bm: &mut BMesh,
    v1: BMVertMutP,
    v2: BMVertMutP,
    edges: &[BMEdgeMutP],
    f_ref: Option<BMFaceConstP>,
    create_flag: BMElemCreate,
) -> BMFaceMutP {

    let mut edges_sort: Vec<BMEdgeMutP> = Vec::with_capacity(edges.len());
    let mut verts_sort: Vec<BMVertMutP> = Vec::with_capacity(edges.len());

    debug_assert!(!edges.is_empty() && v1 != null() && v2 != null());

    unsafe {
        edges_sort.set_len(edges.len());
        verts_sort.set_len(edges.len());
    }

    if bm_edges_sort_winding(v1, v2, edges, &mut edges_sort[..], &mut verts_sort[..]) {
        return BM_face_create(bm, &verts_sort, &edges_sort, f_ref, create_flag);
    }

    return null_mut();
}

#[allow(dead_code)]
pub fn BM_face_create_ngon_verts( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_create_ngon_vcloud( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_vert_attrs_copy( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_edge_attrs_copy( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_loop_attrs_copy( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_face_attrs_copy( /* STUB */ ) {}


///
/// Copies attributes, e.g. custom-data, header flags, etc, from one element
/// to another of the same type.
///
pub fn BM_elem_attrs_copy_ex(
    mut _bm_src: &mut Option<&mut BMesh>,
    _bm_dst: &mut BMesh,
    _ele_src: /* const */ BMElemConstP, _ele_dst: BMElemConstP,
    _hflag_mask: BMElemFlag,
) {
/*
    debug_assert!(ele_src.htype == ele_dst.htype);
    debug_assert!(ele_src != ele_dst);

    if (hflag_mask & BM_ELEM_SELECT) == 0 {
        // First we copy select
        if (BM_elem_flag_test((BMElem *)ele_src, BM_ELEM_SELECT)) {
            BM_elem_select_set(bm_dst, (BMElem *)ele_dst, true);
        }
    }

    // Now we copy flags
    if hflag_mask == 0 {
        ele_dst.hflag = ele_src.hflag;
    } else if hflag_mask == 0xff {
        // pass
    } else {
        ele_dst.hflag = (ele_dst.hflag & hflag_mask) | (ele_src.hflag & ~hflag_mask);
    }

    // Copy specific attributes
    match ele_dst.htype {
        BM_VERT => {
            bm_vert_attrs_copy(bm_src, bm_dst, (const BMVert *)ele_src, (BMVert *)ele_dst);
        }
        BM_EDGE => {
            bm_edge_attrs_copy(bm_src, bm_dst, (const BMEdge *)ele_src, (BMEdge *)ele_dst);
        }
        BM_LOOP => {
            bm_loop_attrs_copy(bm_src, bm_dst, (const BMLoop *)ele_src, (BMLoop *)ele_dst);
        }
        BM_FACE => {
            bm_face_attrs_copy(bm_src, bm_dst, (const BMFace *)ele_src, (BMFace *)ele_dst);
        }
        _ => {
            panic!();
        }
    }
*/
}
pub fn BM_elem_attrs_copy(
    bm_src: &mut Option<&mut BMesh>,
    bm_dst: &mut BMesh,
    ele_src: /* const */ BMElemConstP, ele_dst: BMElemConstP,
) {
    BM_elem_attrs_copy_ex(bm_src, bm_dst, ele_src, ele_dst, BM_ELEM_SELECT);
}

pub fn BM_elem_select_copy<E>(
    bm: &mut BMesh,
    ele_dst: BMElemMutP, ele_src: E,
)
    where
    E: Into<BMElemConstP>,
{
    into_expand!(ele_src);

    debug_assert!(ele_dst.head.htype == ele_src.head.htype);

    if (ele_src.head.hflag & BM_ELEM_SELECT) != (ele_dst.head.hflag & BM_ELEM_SELECT) {
        BM_elem_select_set(ele_dst, (ele_src.head.hflag & BM_ELEM_SELECT) != 0, bm.select_mode);
    }
}

#[allow(dead_code)]
fn bm_mesh_copy_new_face( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_copy_init_customdata( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_copy( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_flag_from_mflag( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_flag_from_mflag( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_flag_from_mflag( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_flag_to_mflag( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_flag_to_mflag( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_flag_to_mflag( /* STUB */ ) {}

