// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use prelude::*;

#[allow(dead_code)]
fn bm_data_interp_from_elem( /* STUB */ ) {}

pub fn BM_data_interp_from_verts<V>(
    _bm: &mut BMesh,
    _v_src_1: V, _v_src_2: V,
    _v_dst: BMVertMutP,
    _fac: f64,
)
    where
    V: Into<BMVertConstP>,
{
    /* STUB */
}
#[allow(dead_code)]
pub fn BM_data_interp_from_edges( /* STUB */ ) {}
#[allow(dead_code)]
fn UNUSED_BM_Data_Vert_Average( /* STUB */ ) {}

pub fn BM_data_interp_face_vert_edge<V, E>(
    _bm: &mut BMesh,
    _v_src_1: V, _v_src_2: V,
    _v: BMVertMutP,
    _e: E, _fac: f64,
)
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    /* STUB */
}

#[allow(dead_code)]
pub fn BM_face_interp_from_face_ex( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_interp_from_face( /* STUB */ ) {}
#[allow(dead_code)]
fn compute_mdisp_quad( /* STUB */ ) {}
#[allow(dead_code)]
fn mdisp_axis_from_quad( /* STUB */ ) {}
#[allow(dead_code)]
fn mdisp_in_mdispquad( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_loop_flip_equotion( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_loop_flip_disp( /* STUB */ ) {}

pub fn BM_loop_interp_multires_ex<F>(
    _bm: &mut BMesh,
    _l_dst: BMLoopMutP,
    _f_src: F,
    _f_dst_center: &[f64; 3],
    _f_src_center: &[f64; 3],
    _cd_loop_mdisp_offset: isize,
)
    where
    F: Into<BMFaceConstP>,
{
    /* STUB */
}

#[allow(dead_code)]
pub fn BM_loop_interp_multires() {
    /* STUB */
}

pub fn BM_face_interp_multires_ex<F>(
    _bm: &mut BMesh, _f_dst: BMFaceMutP, _f_src: F,
    _f_dst_center: &[f64; 3], _f_src_center: &[f64; 3], _cd_loop_mdisp_offset: isize,
)
    where
    F: Into<BMFaceConstP>,
{

    //
}

#[allow(dead_code)]
pub fn BM_face_interp_multires( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_multires_bounds_smooth( /* STUB */ ) {}

///
/// projects a single loop, target, onto f_src
/// for custom-data interpolation. multires is handled.
///
/// if do_vertex is true, target's vert data will also get interpolated.
///
#[allow(dead_code)]
pub fn BM_loop_interp_from_face<F>(
    _bm: &mut BMesh, _l_dst: BMLoopMutP, _f_src: F,
    _do_vertex: bool, _do_multires: bool,
)
    where
    F: Into<BMFaceConstP>,
{
    /* STUB */
}

#[allow(dead_code)]
pub fn BM_vert_interp_from_face( /* STUB */ ) {}
#[allow(dead_code)]
fn update_data_blocks( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_elem_float_data_get( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_elem_float_data_set( /* STUB */ ) {}
#[allow(dead_code)]
fn LoopWalkCtx( /* STUB */ ) {}
#[allow(dead_code)]
fn cd_layer_offset( /* STUB */ ) {}
#[allow(dead_code)]
fn loop_weights( /* STUB */ ) {}
#[allow(dead_code)]
pub fn arena( /* STUB */ ) {}
#[allow(dead_code)]
fn data_ref( /* STUB */ ) {}
#[allow(dead_code)]
fn weight_accum( /* STUB */ ) {}
#[allow(dead_code)]
fn data_array( /* STUB */ ) {}
#[allow(dead_code)]
fn data_index_array( /* STUB */ ) {}
#[allow(dead_code)]
fn weight_array( /* STUB */ ) {}
#[allow(dead_code)]
fn LoopGroupCD( /* STUB */ ) {}
#[allow(dead_code)]
pub fn data( /* STUB */ ) {}
#[allow(dead_code)]
fn data_weights( /* STUB */ ) {}
#[allow(dead_code)]
fn data_index( /* STUB */ ) {}
#[allow(dead_code)]
fn data_len( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_loop_walk_add( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_loop_walk_data( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_loop_groups_data_layer_create( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_vert_loop_groups_data_layer_merge__single( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_vert_loop_groups_data_layer_merge_weights__single( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_loop_groups_data_layer_merge( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_loop_groups_data_layer_merge_weights( /* STUB */ ) {}

