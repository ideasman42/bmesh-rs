// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use prelude::*;

use intern::bmesh_queries::{
    BM_vert_in_edge,
};


pub fn bmesh_disk_vert_swap(mut e: BMEdgeMutP, v_dst: BMVertMutP, v_src: BMVertMutP) {
    if e.verts[0] == v_src {
        e.verts[0] = v_dst;
        e.disk_links[0].next = null_mut();
        e.disk_links[0].prev = null_mut();
    } else if e.verts[1] == v_src {
        e.verts[1] = v_dst;
        e.disk_links[1].next = null_mut();
        e.disk_links[1].prev = null_mut();
    } else {
        panic!();
    }
}

///
/// Handles all connected data, use with care.
///
/// Assumes caller has setup correct state before the swap is done.
///
pub fn bmesh_edge_vert_swap(e: BMEdgeMutP, v_dst: BMVertMutP, v_src: BMVertMutP) {
    // swap out loops
    if e.l != null() {
        let l_first = e.l;
        let mut l_iter = l_first;
        loop {
            if l_iter.v == v_src {
                l_iter.v = v_dst;
            } else if l_iter.next.v == v_src {
                l_iter.next.v = v_dst;
            } else {
                debug_assert!(l_iter.prev.v != v_src);
            }

            l_iter = l_iter.radial_next;
            if l_iter == l_first {
                break;
            }
        }
    }

    // swap out edges
    bmesh_disk_vert_replace(e, v_dst, v_src);
}

/// Low level: don't expose for now
pub fn bmesh_disk_vert_replace(e: BMEdgeMutP, v_dst: BMVertMutP, v_src: BMVertMutP) {
    debug_assert!(e.verts[0] == v_src || e.verts[1] == v_src);
    bmesh_disk_edge_remove(e, v_src);       // remove e from tv's disk cycle
    bmesh_disk_vert_swap(e, v_dst, v_src);  // swap out tv for v_new in e
    bmesh_disk_edge_append(e, v_dst);       // add e to v_dst's disk cycle
    debug_assert!(e.verts[0] != e.verts[1]);
}


/// Exposed as `BMVert.disk_edge_append`
pub fn bmesh_disk_edge_append(e: BMEdgeMutP, mut v: BMVertMutP) {
    if v.e == null() {
        let mut dl1 = bmesh_disk_edge_link_from_vert_mut(e, v);
        v.e = e;
        dl1.next = e;
        dl1.prev = e;
    } else {
        let mut dl1 = bmesh_disk_edge_link_from_vert_mut(e, v);
        let mut dl2 = bmesh_disk_edge_link_from_vert_mut(v.e, v);
        let mut dl3 =
            if dl2.prev != null() {
                bmesh_disk_edge_link_from_vert_mut(dl2.prev, v)
            } else {
                null_mut()
            };

        dl1.next = v.e;
        dl1.prev = dl2.prev;

        dl2.prev = e;
        if dl3 != null() {
            dl3.next = e;
        }
    }
}

/// Exposed as `BMVert.disk_edge_remove`
pub fn bmesh_disk_edge_remove(e: BMEdgeMutP, mut v: BMVertMutP) {
    let mut dl1 = bmesh_disk_edge_link_from_vert_mut(e, v);
    if dl1.prev != null() {
        let mut dl2 = bmesh_disk_edge_link_from_vert_mut(dl1.prev, v);
        dl2.next = dl1.next;
    }

    if dl1.next != null() {
        let mut dl2 = bmesh_disk_edge_link_from_vert_mut(dl1.next, v);
        dl2.prev = dl1.prev;
    }

    if v.e == e {
        v.e = if e != dl1.next {
            dl1.next
        } else {
            null_mut()
        };
    }

    dl1.next = null_mut();
    dl1.prev = null_mut();
}

// Keep this as a stub .its unused for now.
// pub fn bmesh_disk_edge_exists() {}

pub fn bmesh_disk_count<V>(v: V) -> usize
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let mut count = 0;
    if v.e != null() {
        let e_first = v.e.as_const();
        let mut e_iter = e_first;
        loop {
            count += 1;
            e_iter = bmesh_disk_edge_next(e_iter, v);
            if e_iter == e_first {
                break;
            }
        }
    }
    return count;
}

pub fn bmesh_disk_count_ex<V>(v: V, count_max: usize) -> usize
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let mut count = 0;
    if v.e != null() {
        let e_first = v.e.as_const();
        let mut e_iter = e_first;
        loop {
            count += 1;
            if count == count_max {
                break;
            }
            e_iter = bmesh_disk_edge_next(e_iter, v);
            if e_iter == e_first {
                break;
            }
        }
    }
    return count;
}

pub fn bmesh_disk_validate<V, E>(len: usize, e: E, v: V) -> bool
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(e, v);

    if !BM_vert_in_edge(e, v) {
        return false;
    }
    if len == 0 || bmesh_disk_count_ex(v, len + 1) != len {
        return false;
    }

    let mut e_iter = e;
    loop {
        if len != 1 && bmesh_disk_edge_prev(e_iter, v) == e_iter {
            return false;
        }
        e_iter = bmesh_disk_edge_next(e_iter, v);
        if e_iter == e {
            break;
        }
    }

    return true;
}

///
/// \brief DISK COUNT FACE VERT
///
/// Counts the number of loop users
/// for this vertex. Note that this is
/// equivalent to counting the number of
/// faces incident upon this vertex
///
pub fn bmesh_disk_facevert_count<V>(v: V) -> usize
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    // is there an edge on this vert at all
    let mut count = 0;
    if v.e != null() {
        // first, loop around edge
        let e_first = v.e.as_const();
        let mut e_iter = e_first;
        loop {
            if e_iter.l != null() {
                count += bmesh_radial_facevert_count(e_iter.l, v);
            }
            e_iter = bmesh_disk_edge_next(e_iter, v);
            if e_iter == e_first {
                break
            }
        }
    }
    return count;
}

pub fn bmesh_disk_facevert_count_ex<V>(v: V, count_max: usize) -> usize
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    // is there an edge on this vert at all
    let mut count = 0;
    if v.e != null() {
        // first, loop around edge
        let e_first = v.e.as_const();
        let mut e_iter = e_first;
        loop {
            if e_iter.l != null() {
                count += bmesh_radial_facevert_count_ex(e_iter.l, v, count_max - count);
                if count == count_max {
                    break;
                }
            }
            e_iter = bmesh_disk_edge_next(e_iter, v);
            if e_iter == e_first {
                break
            }
        }
    }
    return count;
}

/**
 * \brief FIND FIRST FACE EDGE
 *
 * Finds the first edge in a vertices
 * Disk cycle that has one of this
 * vert's loops attached
 * to it.
 */
#[allow(dead_code)]
pub fn bmesh_disk_faceedge_find_first<V, E>(e: E, v: V) -> BMEdgeConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(e, v);

    let mut e_iter = e;
    loop {
        if e_iter.l != null() {
            return {
                if e_iter.l.v == v {
                    e_iter
                } else {
                    e_iter.l.next.e.as_const()
                }
            };
        }
        e_iter = bmesh_disk_edge_next(e_iter, v);
        if e_iter == e {
            break;
        }
    }
    return null_const();
}
#[allow(dead_code)]
pub fn bmesh_disk_faceedge_find_next() { /* STUB */ }


pub fn bmesh_disk_faceloop_find_first<V, E>(e: E, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(e, v);

    let mut e_iter = e;
    loop {
        if e_iter.l != null() {
            return {
                if e_iter.l.v == v {
                    e_iter.l.as_const()
                } else {
                    e_iter.l.next.as_const()
                }
            };
        }
        e_iter = bmesh_disk_edge_next(e_iter, v);
        if e_iter == e {
            break;
        }
    }
    return null_const();
}
#[allow(dead_code)]
pub fn bmesh_disk_faceloop_find_next() { /* STUB */ }



pub fn bmesh_radial_validate<L>(radial_len: usize, l: L) -> bool
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);

    let mut l_iter = l;
    let mut i = 0;
    if bmesh_radial_length(l) != radial_len {
        return false;
    }

    loop {
        if unlikely!(l_iter == null()) {
            debug_assert!(false);
            return false;
        }

        if l_iter.e != l.e {
            return false;
        }
        if l_iter.v != l.e.verts[0] && l_iter.v != l.e.verts[1] {
            return false;
        }
        if unlikely!(i > ::limits::BM_LOOP_RADIAL_MAX) {
            debug_assert!(false);
            return false;
        }

        i += 1;
        l_iter = l_iter.radial_next.as_const();
        if l_iter == l {
            break;
        }
    }

    return true;
}


/// Exposed as: BMEdge.radial_loop_append()
pub fn bmesh_radial_loop_append(mut e: BMEdgeMutP, mut l: BMLoopMutP) {
    if e.l == null() {
        e.l = l;
        l.radial_next = l;
        l.radial_prev = l;
    } else {
        l.radial_prev = e.l;
        l.radial_next = e.l.radial_next;

        e.l.radial_next.radial_prev = l;
        e.l.radial_next = l;

        e.l = l;
    }

    if unlikely!(l.e != null() && l.e != e) {
        // l is already in a radial cycle for a different edge
        debug_assert!(false);
    }

    l.e = e;
}

pub fn bmesh_radial_loop_remove(mut e: BMEdgeMutP, mut l: BMLoopMutP) {
    debug_assert!(l.e == e);

    if l.radial_next != l {
        if l == e.l {
            e.l = l.radial_next;
        }
        l.radial_next.radial_prev = l.radial_prev;
        l.radial_prev.radial_next = l.radial_next;
    } else {
        if l == e.l {
            e.l = null_mut();
        }
    }
    // l is no longer in a radial cycle; empty the links
    // to the cycle and the link back to an edge.
    l.radial_next = null_mut();
    l.radial_prev = null_mut();
    l.e = null_mut();
}

///
/// A version of #bmesh_radial_loop_remove which only performs the radial unlink,
/// leaving the edge untouched.
///
/// Exposed as: BMEdge.radial_loop_remove()
///
pub fn bmesh_radial_loop_unlink(mut l: BMLoopMutP) {
    if l.radial_next != l {
        l.radial_next.radial_prev = l.radial_prev;
        l.radial_prev.radial_next = l.radial_next;
    }

    // l is no longer in a radial cycle; empty the links
    // to the cycle and the link back to an edge.
    l.radial_next = null_mut();
    l.radial_prev = null_mut();
    l.e = null_mut();
}
#[allow(dead_code)]
pub fn bmesh_radial_faceloop_find_first() { /* STUB */ }
#[allow(dead_code)]
pub fn bmesh_radial_faceloop_find_next() { /* STUB */ }

pub fn bmesh_radial_length<L>(l: L) -> usize
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);

    let mut l_iter = l;
    let mut i = 0;

    if l != null() {
        loop {
            if unlikely!(l_iter == null()) {
                // radial cycle is broken (not a circulat loop)
                debug_assert!(false);
                return 0;
            }

            i += 1;
            if unlikely!(i >= ::limits::BM_LOOP_RADIAL_MAX) {
                debug_assert!(false);
                return ::std::usize::MAX;
            }

            l_iter = l_iter.radial_next.as_const();
            if l_iter == l {
                break;
            }

        }
    }
    return i;
}

///
/// \brief RADIAL COUNT FACE VERT
///
/// Returns the number of times a vertex appears
/// in a radial cycle
///
pub fn bmesh_radial_facevert_count<V, L>(l: L, v: V) -> usize
    where
    V: Into<BMVertConstP>,
    L: Into<BMLoopConstP>,
{
    into_expand!(l, v);

    let mut count = 0;
    let mut l_iter = l;
    loop {
        if l_iter.v == v {
            count += 1;
        }
        l_iter = l_iter.radial_next.as_const();
        if l_iter == l {
            break;
        }
    }
    return count;
}

pub fn bmesh_radial_facevert_count_ex<V, L>(l: L, v: V, count_max: usize) -> usize
    where
    V: Into<BMVertConstP>,
    L: Into<BMLoopConstP>,
{
    into_expand!(l, v);

    let mut count = 0;
    let mut l_iter = l;
    loop {
        if l_iter.v == v {
            count += 1;
            if count == count_max {
                break;
            }
        }
        l_iter = l_iter.radial_next.as_const();
        if l_iter == l {
            break;
        }
    }
    return count;
}

#[allow(dead_code)]
pub fn bmesh_radial_facevert_check() { /* STUB */ }

pub fn bmesh_loop_validate<F>(f: F) -> bool
    where
    F: Into<BMFaceConstP>,
{
    into_expand!(f);

    let len = f.len;

    let l_first = f.l_first;
    if l_first == null() {
        return false;
    }

    // Validate that the face loop cycle is the length specified by f.len
    let mut l_iter = l_first.next;
    for _ in 1..len {
        if (l_iter.f != f) ||
           (l_iter == l_first)
        {
            return false;
        }
        l_iter = l_iter.next;
    }

    if l_iter != l_first {
        return false;
    }

    // Validate the loop.prev links also form a cycle of length f.len
    let mut l_iter = l_first.prev;
    for _ in 1..len {
        if l_iter == l_first {
            return false;
        }
        l_iter = l_iter.prev;
    }
    if l_iter != l_first {
        return false;
    }

    return true;
}


// -----------------------------------------------------------------------------
// Inline

#[inline]
/// Exposed as: BMEdge.disk_link_from_vert_mut()
pub fn bmesh_disk_edge_link_from_vert_mut(e: BMEdgeMutP, v: BMVertMutP) -> BMDiskLinkMutP {
    debug_assert!(e.contains_vert(v));
    let index = if v == e.verts[0] { 0 } else { 1 };
    return PtrMut(&mut e.clone().disk_links[index]);
}
/// Exposed as: BMEdge.disk_link_from_vert()
pub fn bmesh_disk_edge_link_from_vert<V, E>(e: E, v: V) -> BMDiskLinkConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(e, v);

    debug_assert!(e.contains_vert(v));
    let index = if v == e.verts[0] { 0 } else { 1 };
    return PtrConst(&e.disk_links[index]);
}

#[allow(dead_code)]
pub fn bmesh_disk_edge_next_safe() { /* STUB */ }
#[allow(dead_code)]
pub fn bmesh_disk_edge_prev_safe() { /* STUB */ }


/// Exposed as `BMVert.disk_edge_next_mut`
#[inline]
pub fn bmesh_disk_edge_next_mut(e: BMEdgeMutP, v: BMVertMutP) -> BMEdgeMutP {
    return bmesh_disk_edge_link_from_vert_mut(e, v).next;
}
/// Exposed as `BMVert.disk_edge_prev_mut`
#[inline]
pub fn bmesh_disk_edge_prev_mut(e: BMEdgeMutP, v: BMVertMutP) -> BMEdgeMutP {
    return bmesh_disk_edge_link_from_vert_mut(e, v).prev;
}

/// Exposed as `BMVert.disk_edge_next`
#[inline]
pub fn bmesh_disk_edge_next<V, E>(e: E, v: V) -> BMEdgeConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    return bmesh_disk_edge_link_from_vert(e, v).next.as_const();
}
/// Exposed as `BMVert.disk_edge_prev`
#[inline]
pub fn bmesh_disk_edge_prev<V, E>(e: E, v: V) -> BMEdgeConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    return bmesh_disk_edge_link_from_vert(e, v).prev.as_const();
}

