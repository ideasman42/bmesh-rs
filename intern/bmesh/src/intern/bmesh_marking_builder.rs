// Licensed: GPL v2+
// (c) Blender Foundation

use prelude::*;

// ----------------------------------------------------------------------------
// BMElem Edit Flag Builder

pub struct BMElemEditFlagBuilder<'a> {
    bm: &'a mut BMesh,

    hflag: BMElemFlag,
    hflag_skip: BMElemFlag,
    // Could use ArrayVec[BMELEM_EDIT_FLAG_NUM]
    actions: Vec<elem_edit_flag_builder::BMElemEditFlag>,
}

mod elem_edit_flag_builder {

    // Internal, added via builder methods
    //
    // - TODO: set from closure
    #[repr(u8)]
    pub enum BMElemEditFlag {
        SetVertFromValue(bool),
        SetEdgeFromValue(bool),
        SetFaceFromValue(bool),
    }
    // Must be updated manually, grr!
    // TODO: use fancy proc_macro when its in stable:
    // http://stackoverflow.com/a/41638362/432509
    pub const BMELEM_EDIT_FLAG_NUM: usize = 3;

    use prelude::*;

    macro_rules! build_fn_bool {
        ($fn_id:ident, $fn_enum:ident) => {
            pub fn $fn_id(mut self, value: bool) -> BMElemEditFlagBuilder<'a> {
                self.actions.push(BMElemEditFlag::$fn_enum(value));
                return self;
            }
        }
    }

    impl <'a> BMElemEditFlagBuilder<'a> {
        pub fn new<'b>(
            bm: &'b mut BMesh,
            hflag: BMElemFlag,
        ) -> BMElemEditFlagBuilder<'b> {
            return BMElemEditFlagBuilder::<'b> {
                bm: bm,
                hflag: hflag,
                hflag_skip: BMElemFlag(0),
                actions: Vec::with_capacity(BMELEM_EDIT_FLAG_NUM),
            };
        }

        pub fn exec(self) {
            macro_rules! is_elem_skip {
                ($ele:ident) => {
                    (self.hflag_skip == 0) ||
                    (self.hflag_skip & $ele.head.hflag) == 0
                }
            }

            macro_rules! set_elem_from_value {
                ($ele_seq:ident, $value:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            ele.head.hflag.set(self.hflag, $value);
                        }
                    }
                }
            }

            for action in &self.actions {
                use self::BMElemEditFlag::*;
                match action {
                    // Elem From Value
                    &SetVertFromValue(value) => {set_elem_from_value!(verts, value)},
                    &SetEdgeFromValue(value) => {set_elem_from_value!(edges, value)},
                    &SetFaceFromValue(value) => {set_elem_from_value!(faces, value)},
                }
            }
        }

        pub fn skip(mut self, hflag_skip: BMElemFlag) -> Self {
            debug_assert!(self.hflag_skip == 0);
            debug_assert!(self.actions.is_empty());
            self.hflag_skip = hflag_skip;
            self
        }

        build_fn_bool!(set_vert_from_value, SetVertFromValue);
        build_fn_bool!(set_edge_from_value, SetEdgeFromValue);
        build_fn_bool!(set_face_from_value, SetFaceFromValue);
    }
}

pub fn BM_mesh_elem_hflag_edit_builder(
    bm: &mut BMesh,
    hflag: BMElemFlag,
) -> BMElemEditFlagBuilder {
    return BMElemEditFlagBuilder::new(bm, hflag);
}


// ----------------------------------------------------------------------------
// BMElem Flush Flag Builder

pub struct BMElemFlushFlagBuilder<'a> {
    bm: &'a mut BMesh,

    hflag: BMElemFlag,
    hflag_skip: BMElemFlag,
    // Could use ArrayVec[BMELEM_ELEM_FLUSH_FLAG_NUM]
    actions: Vec<elem_flush_flag_builder::BMElemFlushFlag>,
}

mod elem_flush_flag_builder {

    // Internal, added via builder methods
    //
    // - TODO: loops (add if they're needed).
    // - TODO: Along with 'Any' / 'All' we could have 'None' (!All).
    #[repr(u8)]
    pub enum BMElemFlushFlag {

        SetVertFromEdgeAny,
        SetVertFromEdgeAll,
        SetVertFromFaceAny,
        SetVertFromFaceAll,
        SetEdgeFromVertAny,
        SetEdgeFromVertAll,
        SetEdgeFromFaceAny,
        SetEdgeFromFaceAll,
        SetFaceFromVertAny,
        SetFaceFromVertAll,
        SetFaceFromEdgeAny,
        SetFaceFromEdgeAll,

        AddVertFromEdgeAny,
        AddVertFromEdgeAll,
        AddVertFromFaceAny,
        AddVertFromFaceAll,
        AddEdgeFromVertAny,
        AddEdgeFromVertAll,
        AddEdgeFromFaceAny,
        AddEdgeFromFaceAll,
        AddFaceFromVertAny,
        AddFaceFromVertAll,
        AddFaceFromEdgeAny,
        AddFaceFromEdgeAll,

        SubVertFromEdgeAny,
        SubVertFromEdgeAll,
        SubVertFromFaceAny,
        SubVertFromFaceAll,
        SubEdgeFromVertAny,
        SubEdgeFromVertAll,
        SubEdgeFromFaceAny,
        SubEdgeFromFaceAll,
        SubFaceFromVertAny,
        SubFaceFromVertAll,
        SubFaceFromEdgeAny,
        SubFaceFromEdgeAll,
    }
    // Must be updated manually, grr!
    // TODO: use fancy proc_macro when its in stable:
    // http://stackoverflow.com/a/41638362/432509
    pub const BMELEM_FLUSH_FLAG_NUM: usize = 39;

    use prelude::*;

    #[allow(unused_macros)]
    macro_rules! build_fn_bool {
        ($fn_id:ident, $fn_enum:ident) => {
            pub fn $fn_id(mut self, value: bool) -> BMElemFlushFlagBuilder<'a> {
                self.actions.push(BMElemFlushFlag::$fn_enum(value));
                return self;
            }
        }
    }

    macro_rules! build_fn {
        ($fn_id:ident, $fn_enum:ident) => {
            pub fn $fn_id(mut self) -> BMElemFlushFlagBuilder<'a> {
                self.actions.push(BMElemFlushFlag::$fn_enum);
                return self;
            }
        }
    }


    impl <'a> BMElemFlushFlagBuilder<'a> {
        pub fn new<'b>(bm: &'b mut BMesh, hflag: BMElemFlag) -> BMElemFlushFlagBuilder<'b> {
            return BMElemFlushFlagBuilder::<'b> {
                bm: bm,
                hflag: hflag,
                hflag_skip: BMElemFlag(0),
                actions: Vec::with_capacity(BMELEM_FLUSH_FLAG_NUM),
            };
        }

        pub fn exec(self) {
            use intern::bmesh_queries::{
                BM_vert_is_any_edge_flag_test,
                BM_vert_is_all_edge_flag_test,
                BM_vert_is_any_face_flag_test,
                BM_vert_is_all_face_flag_test,

                BM_edge_is_any_vert_flag_test,
                BM_edge_is_all_vert_flag_test,
                BM_edge_is_any_face_flag_test,
                BM_edge_is_all_face_flag_test,

                BM_face_is_any_vert_flag_test,
                BM_face_is_all_vert_flag_test,
                BM_face_is_any_edge_flag_test,
                BM_face_is_all_edge_flag_test,
            };

            macro_rules! is_elem_skip {
                ($ele:ident) => {
                    (self.hflag_skip == 0) ||
                    (self.hflag_skip & $ele.head.hflag) == 0
                }
            }

            #[allow(unused_macros)]
            macro_rules! set_elem_from_value {
                ($ele_seq:ident, $value:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            ele.head.hflag.set(self.hflag, $value);
                        }
                    }
                }
            }

            macro_rules! set_any {
                ($ele_seq:ident, $ele_test_any_fn:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            let is_set = $ele_test_any_fn(ele, self.hflag);
                            ele.head.hflag.set(self.hflag, is_set);
                        }
                    }
                }
            }

            macro_rules! set_all {
                ($ele_seq:ident, $ele_test_all_fn:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            let is_set = $ele_test_all_fn(ele, self.hflag);
                            ele.head.hflag.set(self.hflag, is_set);
                        }
                    }
                }
            }

            macro_rules! add_any {
                ($ele_seq:ident, $ele_test_any_fn:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            if (ele.head.hflag & self.hflag) == 0 {
                                if $ele_test_any_fn(ele, self.hflag) {
                                    ele.head.hflag |= self.hflag;
                                }
                            }
                        }
                    }
                }
            }

            macro_rules! add_all {
                ($ele_seq:ident, $ele_test_all_fn:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            if (ele.head.hflag & self.hflag) == 0 {
                                if $ele_test_all_fn(ele, self.hflag) {
                                    ele.head.hflag |= self.hflag;
                                }
                            }
                        }
                    }
                }
            }

            macro_rules! sub_any {
                ($ele_seq:ident, $ele_test_any_fn:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            if (ele.head.hflag & self.hflag) != 0 {
                                if !$ele_test_any_fn(ele, self.hflag) {
                                    ele.head.hflag &= !self.hflag;
                                }
                            }
                        }
                    }
                }
            }

            macro_rules! sub_all {
                ($ele_seq:ident, $ele_test_all_fn:ident) => {
                    for mut ele in self.bm.$ele_seq.iter_mut() {
                        if is_elem_skip!(ele) {
                            if (ele.head.hflag & self.hflag) != 0 {
                                if !$ele_test_all_fn(ele, self.hflag) {
                                    ele.head.hflag &= !self.hflag;
                                }
                            }
                        }
                    }
                }
            }

            for action in &self.actions {
                use self::BMElemFlushFlag::*;

                match action {

                    // Vert From Edge
                    &SetVertFromEdgeAny => { set_any!(verts, BM_vert_is_any_edge_flag_test) },
                    &SetVertFromEdgeAll => { set_all!(verts, BM_vert_is_all_edge_flag_test) },
                    &AddVertFromEdgeAny => { add_any!(verts, BM_vert_is_any_edge_flag_test) },
                    &AddVertFromEdgeAll => { add_all!(verts, BM_vert_is_all_edge_flag_test) },
                    &SubVertFromEdgeAny => { sub_any!(verts, BM_vert_is_any_edge_flag_test) },
                    &SubVertFromEdgeAll => { sub_all!(verts, BM_vert_is_all_edge_flag_test) },

                    // Vert From Face
                    &SetVertFromFaceAny => { set_any!(verts, BM_vert_is_any_face_flag_test) },
                    &SetVertFromFaceAll => { set_all!(verts, BM_vert_is_all_face_flag_test) },
                    &AddVertFromFaceAny => { add_any!(verts, BM_vert_is_any_face_flag_test) },
                    &AddVertFromFaceAll => { add_all!(verts, BM_vert_is_all_face_flag_test) },
                    &SubVertFromFaceAny => { sub_any!(verts, BM_vert_is_any_face_flag_test) },
                    &SubVertFromFaceAll => { sub_all!(verts, BM_vert_is_all_face_flag_test) },

                    // Edge From Vert
                    &SetEdgeFromVertAny => { set_any!(edges, BM_edge_is_any_vert_flag_test) },
                    &SetEdgeFromVertAll => { set_all!(edges, BM_edge_is_all_vert_flag_test) },
                    &AddEdgeFromVertAny => { add_any!(edges, BM_edge_is_any_vert_flag_test) },
                    &AddEdgeFromVertAll => { add_all!(edges, BM_edge_is_all_vert_flag_test) },
                    &SubEdgeFromVertAny => { sub_any!(edges, BM_edge_is_any_vert_flag_test) },
                    &SubEdgeFromVertAll => { sub_all!(edges, BM_edge_is_all_vert_flag_test) },

                    // Edge From Face
                    &SetEdgeFromFaceAny => { set_any!(edges, BM_edge_is_any_face_flag_test) },
                    &SetEdgeFromFaceAll => { set_all!(edges, BM_edge_is_all_face_flag_test) },
                    &AddEdgeFromFaceAny => { add_any!(edges, BM_edge_is_any_face_flag_test) },
                    &AddEdgeFromFaceAll => { add_all!(edges, BM_edge_is_all_face_flag_test) },
                    &SubEdgeFromFaceAny => { sub_any!(edges, BM_edge_is_any_face_flag_test) },
                    &SubEdgeFromFaceAll => { sub_all!(edges, BM_edge_is_all_face_flag_test) },

                    // Face From Vert
                    &SetFaceFromVertAny => { set_any!(faces, BM_face_is_any_vert_flag_test) },
                    &SetFaceFromVertAll => { set_all!(faces, BM_face_is_all_vert_flag_test) },
                    &AddFaceFromVertAny => { add_any!(faces, BM_face_is_any_vert_flag_test) },
                    &AddFaceFromVertAll => { add_all!(faces, BM_face_is_all_vert_flag_test) },
                    &SubFaceFromVertAny => { sub_any!(faces, BM_face_is_any_vert_flag_test) },
                    &SubFaceFromVertAll => { sub_all!(faces, BM_face_is_all_vert_flag_test) },

                    // Face From Edge
                    &SetFaceFromEdgeAny => { set_any!(faces, BM_face_is_any_edge_flag_test) },
                    &SetFaceFromEdgeAll => { set_all!(faces, BM_face_is_all_edge_flag_test) },
                    &AddFaceFromEdgeAny => { add_any!(faces, BM_face_is_any_edge_flag_test) },
                    &AddFaceFromEdgeAll => { add_all!(faces, BM_face_is_all_edge_flag_test) },
                    &SubFaceFromEdgeAny => { sub_any!(faces, BM_face_is_any_edge_flag_test) },
                    &SubFaceFromEdgeAll => { sub_all!(faces, BM_face_is_all_edge_flag_test) },
                }
            }
        }

        pub fn skip(mut self, hflag_skip: BMElemFlag) -> Self {
            debug_assert!(self.hflag_skip == 0);
            debug_assert!(self.actions.is_empty());
            self.hflag_skip = hflag_skip;
            self
        }

        build_fn!(set_vert_from_edge_any, SetVertFromEdgeAny);
        build_fn!(set_vert_from_edge_all, SetVertFromEdgeAll);
        build_fn!(set_vert_from_face_any, SetVertFromFaceAny);
        build_fn!(set_vert_from_face_all, SetVertFromFaceAll);
        build_fn!(set_edge_from_vert_any, SetEdgeFromVertAny);
        build_fn!(set_edge_from_vert_all, SetEdgeFromVertAll);
        build_fn!(set_edge_from_face_any, SetEdgeFromFaceAny);
        build_fn!(set_edge_from_face_all, SetEdgeFromFaceAll);
        build_fn!(set_face_from_vert_any, SetFaceFromVertAny);
        build_fn!(set_face_from_vert_all, SetFaceFromVertAll);
        build_fn!(set_face_from_edge_any, SetFaceFromEdgeAny);
        build_fn!(set_face_from_edge_all, SetFaceFromEdgeAll);

        build_fn!(add_vert_from_edge_any, AddVertFromEdgeAny);
        build_fn!(add_vert_from_edge_all, AddVertFromEdgeAll);
        build_fn!(add_vert_from_face_any, AddVertFromFaceAny);
        build_fn!(add_vert_from_face_all, AddVertFromFaceAll);
        build_fn!(add_edge_from_vert_any, AddEdgeFromVertAny);
        build_fn!(add_edge_from_vert_all, AddEdgeFromVertAll);
        build_fn!(add_edge_from_face_any, AddEdgeFromFaceAny);
        build_fn!(add_edge_from_face_all, AddEdgeFromFaceAll);
        build_fn!(add_face_from_vert_any, AddFaceFromVertAny);
        build_fn!(add_face_from_vert_all, AddFaceFromVertAll);
        build_fn!(add_face_from_edge_any, AddFaceFromEdgeAny);
        build_fn!(add_face_from_edge_all, AddFaceFromEdgeAll);

        build_fn!(sub_vert_from_edge_any, SubVertFromEdgeAny);
        build_fn!(sub_vert_from_edge_all, SubVertFromEdgeAll);
        build_fn!(sub_vert_from_face_any, SubVertFromFaceAny);
        build_fn!(sub_vert_from_face_all, SubVertFromFaceAll);
        build_fn!(sub_edge_from_vert_any, SubEdgeFromVertAny);
        build_fn!(sub_edge_from_vert_all, SubEdgeFromVertAll);
        build_fn!(sub_edge_from_face_any, SubEdgeFromFaceAny);
        build_fn!(sub_edge_from_face_all, SubEdgeFromFaceAll);
        build_fn!(sub_face_from_vert_any, SubFaceFromVertAny);
        build_fn!(sub_face_from_vert_all, SubFaceFromVertAll);
        build_fn!(sub_face_from_edge_any, SubFaceFromEdgeAny);
        build_fn!(sub_face_from_edge_all, SubFaceFromEdgeAll);
    }
}

///
/// Note, this isn't from Blender, where flushing was always very general
/// and did a lot of over-calculating.
///
/// This function gives more control, so you can define exactly what flushes.
///
pub fn BM_mesh_elem_hflag_flush_builder(
    bm: &mut BMesh,
    hflag: BMElemFlag,
) -> BMElemFlushFlagBuilder {
    return BMElemFlushFlagBuilder::new(bm, hflag);
}
