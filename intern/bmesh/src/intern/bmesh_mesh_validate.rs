// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use prelude::*;

use intern::bmesh_core::{
    bmesh_elem_check,
};
use intern::bmesh_mesh::{
    BM_mesh_elem_index_ensure,
};
use intern::bmesh_queries::{
    BM_vert_in_edge,
};

#[inline]
fn key_from_edge<E>(e: E) -> u64
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let mut pair: [i32; 2] = [e.verts[0].head.index, e.verts[1].head.index];
    if pair[0] > pair[1] {
        pair.swap(0, 1);
        // ::std::mem::swap(&mut pair[0], &mut pair[1]);
    }
    return * unsafe { ::std::mem::transmute::<&[i32; 2], &u64>(&pair) };
}

macro_rules! errmsg {
    ($e:ident) => ({
        (print!("\n"));
        $e += 1;
    });
    ($e:ident, $fmt:expr) => ({
        (print!(concat!($fmt, "\n")));
        $e += 1;
    });
    ($e:ident, $fmt:expr, $($arg:tt)*) => ({
        (print!(concat!($fmt, "\n"), $($arg)*));
        $e += 1;
    });
}


///
/// Check of this BMesh is valid,
/// this function can be slow since its intended to help with debugging.
///
/// \return true when the mesh is valid.
///
/// Exposed as: BMesh.validate()
pub fn BM_mesh_validate(bm: &mut BMesh) -> bool {

    use std::collections::HashMap;

    let mut edge_hash: HashMap<u64, BMEdgeConstP> =
        HashMap::with_capacity(bm.edges.len());

    let mut errtot: isize = 0;

    // too verbose!
    if false {
        println!("");
        errmsg!(errtot, concat!(
                "This is a debugging function and not intended for general use, ",
                "running slow test!"));
    }

    /* force recalc, even if tagged as valid, since this mesh is suspect! */
    bm.elem_index_dirty |= BM_HTYPE_ALL;
    BM_mesh_elem_index_ensure(bm, BM_HTYPE_ALL);

    for (i, v) in bm.verts.iter().enumerate() {
        if v.head.hflag & (BM_ELEM_SELECT | BM_ELEM_HIDDEN) == (BM_ELEM_SELECT | BM_ELEM_HIDDEN) {
            errmsg!(errtot, "vert {}: is hidden and selected", i);
        }

        if v.e != null() {
            if !BM_vert_in_edge(v.e, v) {
                errmsg!(errtot,
                        "vert {}: is not in its referenced edge: {}",
                        i, v.e.head.index);
            }
        }
    }

    // check edges
    for (i, e) in bm.edges.iter().enumerate() {
        if e.verts[0] == e.verts[1] {
            errmsg!(errtot,
                    "edge {}: duplicate index: {}",
                    i, e.verts[0].head.index);
        }

        // build edge-hash at the same time
        use std::collections::hash_map::Entry::{Occupied, Vacant};
        match edge_hash.entry(key_from_edge(e)) {
            Occupied(val) => {
                let e_other = val.get();
                errmsg!(errtot,
                        "edge {}, {}: are duplicates",
                        i, e_other.head.index);
            },
            Vacant(entry) => {
                entry.insert(e);
            },
        }
    }

    // edge radial structure
    for (i, e) in bm.edges.iter().enumerate() {
        if (e.head.hflag & (BM_ELEM_SELECT | BM_ELEM_HIDDEN)) ==
            (BM_ELEM_SELECT | BM_ELEM_HIDDEN)
        {
            errmsg!(errtot, "edge {}: is hidden and selected", i);
        }

        if e.l != null() {
            let l_first = e.l;
            let mut l_iter = l_first;
            // we could do more checks here, but save for face checks
            loop {
                if l_iter.e != e {
                    errmsg!(errtot,
                            "edge {}: has invalid loop, loop is of face {}",
                            i, l_iter.f.head.index);
                }
                else if BM_vert_in_edge(e, l_iter.v) == false {
                    errmsg!(errtot,
                            "edge {}: has invalid loop with vert not in edge, loop is of face {}",
                            i, l_iter.f.head.index);
                }
                else if BM_vert_in_edge(e, l_iter.next.v) == false {
                    errmsg!(errtot, concat!(
                            "edge {}: has invalid loop with next vert not in edge, ",
                            "loop is of face {}"),
                           i, l_iter.f.head.index);
                }
                l_iter = l_iter.radial_next;
                if l_iter == l_first {
                    break;
                }
            }
        }
    }

    // face structure
    for (i, f) in bm.faces.iter().enumerate() {
        if f.head.hflag & (BM_ELEM_SELECT | BM_ELEM_HIDDEN) == (BM_ELEM_SELECT | BM_ELEM_HIDDEN) {
            errmsg!(errtot, "face {}: is hidden and selected", i);
        }

        let l_first = f.l_first;
        let mut l_iter = l_first;

        loop {
            l_iter.head.hflag   &= !BM_ELEM_INTERNAL_TAG;
            l_iter.v.head.hflag &= !BM_ELEM_INTERNAL_TAG;
            l_iter.e.head.hflag &= !BM_ELEM_INTERNAL_TAG;

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }

        let mut j = 0;

        l_iter = l_first;
        loop {
            if (l_iter.head.hflag & BM_ELEM_INTERNAL_TAG) != 0 {
                errmsg!(errtot,
                        "face {}: has duplicate loop at corner: {}",
                        i, j);
            }
            if (l_iter.v.head.hflag & BM_ELEM_INTERNAL_TAG) != 0 {
                errmsg!(errtot,
                        "face {}: has duplicate vert: {}, at corner: {}",
                        i, l_iter.v.head.index, j);
            }
            if (l_iter.e.head.hflag & BM_ELEM_INTERNAL_TAG) != 0 {
                errmsg!(errtot,
                        "face {}: has duplicate edge: {}, at corner: {}",
                        i,  l_iter.e.head.index, j);
            }

            /* adjacent data checks */
            if l_iter.f != f {
                errmsg!(errtot,
                        "face {}: has loop that points to face: {} at corner: {}",
                        i, l_iter.f.head.index, j);
            }
            if l_iter != l_iter.prev.next {
                errmsg!(errtot,
                        "face {}: has invalid 'prev/next' at corner: {}",
                        i, j);
            }
            if l_iter != l_iter.next.prev {
                errmsg!(errtot,
                        "face {}: has invalid 'next/prev' at corner: {}",
                        i, j);
            }
            if l_iter != l_iter.radial_prev.radial_next {
                errmsg!(errtot,
                        "face {}: has invalid 'radial_prev/radial_next' at corner: {}",
                        i, j);
            }
            if l_iter != l_iter.radial_next.radial_prev {
                errmsg!(errtot,
                        "face {}: has invalid 'radial_next/radial_prev' at corner: {}",
                        i, j);
            }

            l_iter.head.hflag   |= BM_ELEM_INTERNAL_TAG;
            l_iter.v.head.hflag |= BM_ELEM_INTERNAL_TAG;
            l_iter.e.head.hflag |= BM_ELEM_INTERNAL_TAG;

            j += 1;

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }

        if j != f.len {
            errmsg!(errtot, "face {}: has length of {} but should be {}", i, f.len, j);
        }

        l_iter = l_first;
        loop {
            l_iter.head.hflag   &= !BM_ELEM_INTERNAL_TAG;
            l_iter.v.head.hflag &= !BM_ELEM_INTERNAL_TAG;
            l_iter.e.head.hflag &= !BM_ELEM_INTERNAL_TAG;

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }

    }

    drop(edge_hash);


    // Note that this isn't in the C version of BM_mesh_validate,
    // its an extra set of checks to be totally sure the mesh is OK.
    {
        for (i, v) in bm.verts.iter().enumerate() {
            let errflag = bmesh_elem_check(v.as_elem(), BM_VERT);
            if errflag != 0 {
                errmsg!(errtot, "vert {}: has invalid element (flag={:?})", i, errflag);
            }
        }

        for (i, e) in bm.edges.iter().enumerate() {
            let errflag = bmesh_elem_check(e.as_elem(), BM_EDGE);
            if errflag != 0 {
                errmsg!(errtot, "edge {}: has invalid element (flag={:?})", i, errflag);
            }
        }

        let mut j = 0;
        for (i, f) in bm.faces.iter().enumerate() {
            {
                let errflag = bmesh_elem_check(f.as_elem(), BM_FACE);
                if errflag != 0 {
                    errmsg!(errtot, "face {}: has invalid element (flag={:?})", i, errflag);
                }
            }

            bm_iter_loops_of_face_cycle!(f, l_iter, {
                let errflag = bmesh_elem_check(l_iter.as_elem(), BM_LOOP);
                if errflag != 0 {
                    errmsg!(errtot, "loop {}: has invalid element (flag={:?})", j, errflag);
                }
                j += 1;
            });
        }
    }

    // too verbose
    if false {
        errmsg!(errtot, "Finished - errors {}", errtot);
    }

    return errtot == 0;
}
