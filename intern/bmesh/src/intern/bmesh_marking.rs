// Licensed: GPL v2+
// (c) Blender Foundation

use prelude::*;

use intern::bmesh_structure::{
    bmesh_disk_edge_next,
    bmesh_disk_edge_next_mut,
};

// ----------------------------------------------------------------------------
// Public Function Arg Types

#[allow(dead_code)]
fn recount_totsels( /* STUB */ ) {}

fn bm_vert_is_edge_select_any_other<V, E>(v: V, e_first: E) -> bool
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(v, e_first);

    let mut e_iter = e_first;

    // start by stepping over the current edge
    loop {
        e_iter = bmesh_disk_edge_next(e_iter, v);
        if e_iter == e_first {
            break;
        }

        if (e_iter.head.hflag & BM_ELEM_SELECT) != 0 {
            return true;
        }
    }
    return false;
}

fn bm_vert_is_edge_visible_any<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);
    bm_iter_edges_of_vert!(v, e_iter, {
        if (e_iter.head.hflag & BM_ELEM_HIDDEN) != 0 {
            return true;
        }
    });
    return false;
}

// bm_edge_is_face_select_any (unused)

fn bm_edge_is_face_select_any_other<L>(l_first: L) -> bool
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l_first);

    let mut l_iter = l_first;

    // start by stepping over the current face
    loop {
        l_iter = l_iter.radial_next.as_const();
        if l_iter == l_first {
            break;
        }

        if (l_iter.f.head.hflag & BM_ELEM_SELECT) != 0 {
            return true;
        }
    }
    return false;
}


fn bm_edge_is_face_visible_any<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);
    bm_iter_loops_of_edge_radial!(e, l_iter, {
        if (l_iter.f.head.hflag & BM_ELEM_HIDDEN) != 0 {
            return true;
        }
    });
    return false;
}

#[allow(dead_code)]
pub fn BM_mesh_select_mode_clean_ex( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_select_mode_clean( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_select_mode_flush_ex( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_select_mode_flush( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_deselect_flush( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_select_flush( /* STUB */ ) {}



///
/// \brief Select Vert
///
/// Changes selection state of a single vertex
/// in a mesh
///
pub fn BM_vert_select_set(mut v: BMVertMutP, select: bool) {
    debug_assert!(v.head.htype == BM_VERT);

    if (v.head.hflag & BM_ELEM_HIDDEN) != 0 {
        return;
    }

    if select {
        if (v.head.hflag & BM_ELEM_SELECT) == 0 {
            v.head.hflag |= BM_ELEM_SELECT;
        }
    } else {
        if (v.head.hflag & BM_ELEM_SELECT) != 0 {
            v.head.hflag &= !BM_ELEM_SELECT;
        }
    }
}
///
/// \brief Select Edge
///
/// Changes selection state of a single edge in a mesh.
///
pub fn BM_edge_select_set(mut e: BMEdgeMutP, select: bool, select_mode: BMElemType) {
    debug_assert!(e.head.htype == BM_EDGE);

    if (e.head.hflag & BM_ELEM_HIDDEN) != 0 {
        return;
    }

    if select {
        if (e.head.hflag & BM_ELEM_SELECT) == 0 {
            e.head.hflag |= BM_ELEM_SELECT;
        }
        BM_vert_select_set(e.verts[0], true);
        BM_vert_select_set(e.verts[1], true);
    } else {
        if (e.head.hflag & BM_ELEM_SELECT) != 0 {
            e.head.hflag &= !BM_ELEM_SELECT;
        }

        if (select_mode & BM_VERT) == 0 {
            // check if the vert is used by a selected edge
            for v in &e.verts {
                if bm_vert_is_edge_select_any_other(*v, e) == false {
                    BM_vert_select_set(*v, false);
                }
            }
        } else {
            for v in &e.verts {
                BM_vert_select_set(*v, false);
            }
        }
    }
}
///
/// \brief Select Face
///
/// Changes selection state of a single
/// face in a mesh.
///
pub fn BM_face_select_set(mut f: BMFaceMutP, select: bool, select_mode: BMElemType) {
    debug_assert!(f.head.htype == BM_FACE);

    if (f.head.hflag & BM_ELEM_HIDDEN) != 0 {
        return;
    }

    if select {
        if (f.head.hflag & BM_ELEM_SELECT) == 0 {
            f.head.hflag |= BM_ELEM_SELECT;
        }

        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            BM_vert_select_set(l_iter.v, true);
            BM_edge_select_set(l_iter.e, true, select_mode);
            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
    } else {
        if (f.head.hflag & BM_ELEM_SELECT) != 0 {
            f.head.hflag &= !BM_ELEM_SELECT;
        }

        // \note This allows a temporarily invalid state - where for eg
        // an edge bay be de-selected, but an adjacent face remains selected.
        //
        // Rely on #BM_mesh_select_mode_flush to correct these cases.
        //
        // \note flushing based on mode, see T46494
        if (select_mode & BM_VERT) != 0 {
            let l_first = f.l_first;
            let mut l_iter = l_first;
            loop {
                BM_vert_select_set(l_iter.v, false);
                BM_edge_select_set_noflush(l_iter.e, false);
                l_iter = l_iter.next;
                if l_iter == l_first {
                    break;
                }
            }
        } else {
            // \note use #BM_edge_select_set_noflush,
            // vertex flushing is handled last.
            if (select_mode & BM_EDGE) != 0 {
                let l_first = f.l_first;
                let mut l_iter = l_first;
                loop {
                    BM_edge_select_set_noflush(l_iter.e, false);
                    l_iter = l_iter.next;
                    if l_iter == l_first {
                        break;
                    }
                }
            } else {
                let l_first = f.l_first;
                let mut l_iter = l_first;
                loop {
                    if bm_edge_is_face_select_any_other(l_iter) == false {
                        BM_edge_select_set_noflush(l_iter.e, false);
                    }
                    l_iter = l_iter.next;
                    if l_iter == l_first {
                        break;
                    }
                }
            }

            // flush down to verts
            {
                let l_first = f.l_first;
                let mut l_iter = l_first;
                loop {
                    if bm_vert_is_edge_select_any_other(l_iter.v, l_iter.e) == false {
                        BM_vert_select_set(l_iter.v, false);
                    }
                    l_iter = l_iter.next;
                    if l_iter == l_first {
                        break;
                    }
                }
            }
        }
    }
}

pub fn BM_edge_select_set_noflush(mut e: BMEdgeMutP, select: bool) {
    debug_assert!(e.head.htype == BM_EDGE);

    if (e.head.hflag & BM_ELEM_HIDDEN) != 0 {
        return;
    }

    if select {
        if (e.head.hflag & BM_ELEM_SELECT) == 0 {
            e.head.hflag |= BM_ELEM_SELECT;
        }
    } else {
        if (e.head.hflag & BM_ELEM_SELECT) != 0 {
            e.head.hflag &= !BM_ELEM_SELECT;
        }
    }
}

pub fn BM_face_select_set_noflush(mut f: BMFaceMutP, select: bool) {
    debug_assert!(f.head.htype == BM_FACE);

    if (f.head.hflag & BM_ELEM_HIDDEN) != 0 {
        return;
    }

    if select {
        if (f.head.hflag & BM_ELEM_SELECT) == 0 {
            f.head.hflag |= BM_ELEM_SELECT;
        }
    } else {
        if (f.head.hflag & BM_ELEM_SELECT) != 0 {
            f.head.hflag &= !BM_ELEM_SELECT;
        }
    }
}


#[allow(dead_code)]
pub fn BM_mesh_select_mode_set( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_mesh_flag_count( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_elem_hflag_count_enabled( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_elem_hflag_count_disabled( /* STUB */ ) {}

pub fn BM_elem_select_set(mut ele: BMElemMutP, select: bool, select_mode: BMElemType)
{
    match ele.head.htype {
        BM_VERT => {
            BM_vert_select_set(ele.as_vert_mut(), select);
        }
        BM_EDGE => {
            BM_edge_select_set(ele.as_edge_mut(), select, select_mode);
        }
        BM_FACE => {
            BM_face_select_set(ele.as_face_mut(), select, select_mode);
        }
        _ => {
            debug_assert!(false);
        }
    }
}

#[allow(dead_code)]
pub fn BM_mesh_active_face_set( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_active_face_get( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_active_edge_get( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_active_vert_get( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_active_elem_get( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_editselection_center( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_editselection_normal( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_editselection_plane( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_select_history_create( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_check( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_remove( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_store_notest( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_store_head_notest( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_store( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_store_head( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_store_after_notest( /* STUB */ ) {}
#[allow(dead_code)]
pub fn _bm_select_history_store_after( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_select_history_clear( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_select_history_validate( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_select_history_active_get( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_select_history_map_create( /* STUB */ ) {}

#[allow(dead_code)]
pub fn BM_mesh_elem_hflag_disable_test(
    bm: &mut BMesh, htype: BMElemType, hflag: BMElemFlag,
    respect_hide: bool, overwrite: bool, hflag_test: BMElemFlag,
) {
    let iter_ele_seq_all = [
        bm.verts.as_elem_seq_mut(),
        bm.edges.as_elem_seq_mut(),
        bm.faces.as_elem_seq_mut(),
    ];

    let flag_types: [BMElemType; 3] = [BM_VERT, BM_EDGE, BM_FACE];

    let hflag_nosel = hflag & !BM_ELEM_SELECT;

    assert!((htype & !BM_HTYPE_ALL_NOLOOP) == 0);

    /*
    if (hflag & BM_ELEM_SELECT) {
        BM_select_history_clear(bm);
    }
    */

    if (htype == (BM_VERT | BM_EDGE | BM_FACE)) &&
       (hflag == BM_ELEM_SELECT) &&
       (respect_hide == false) &&
       (hflag_test == 0)
    {
        // fast path for deselect all, avoid topology loops
        // since we know all will be de-selected anyway.

        // can be threaded
        for i in 0..3 {
            for mut ele in iter_ele_seq_all[i].iter_mut() {
                ele.head.hflag &= !BM_ELEM_SELECT;
            }
        }
    } else {
        for i in 0..3 {
            if (htype & flag_types[i]) != 0 {
                for mut ele in iter_ele_seq_all[i].iter_mut() {
                    if unlikely!(respect_hide && ((ele.head.hflag & BM_ELEM_HIDDEN) != 0)) {
                        // pass
                    } else if hflag_test == 0 || ((ele.head.hflag & hflag_test) != 0) {
                        if (hflag & BM_ELEM_SELECT) != 0 {
                            BM_elem_select_set(ele, false, bm.select_mode);
                        }
                        ele.head.hflag &= !hflag;
                    } else if overwrite {
                        // no match!
                        if (hflag & BM_ELEM_SELECT) != 0 {
                            BM_elem_select_set(ele, true, bm.select_mode);
                        }
                        ele.head.hflag |= hflag_nosel;
                    }
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn BM_mesh_elem_hflag_enable_test(
    bm: &mut BMesh, htype: BMElemType, hflag: BMElemFlag,
    respect_hide: bool, overwrite: bool, hflag_test: BMElemFlag,
) {
    let iter_ele_seq_all = [
        bm.verts.as_elem_seq_mut(),
        bm.edges.as_elem_seq_mut(),
        bm.faces.as_elem_seq_mut(),
    ];

    let flag_types: [BMElemType; 3] = [BM_VERT, BM_EDGE, BM_FACE];

    // use the nosel version when setting so under no
    // condition may a hidden face become selected.
    // Applying other flags to hidden faces is OK.
    let hflag_nosel = hflag & !BM_ELEM_SELECT;

    assert!((htype & !BM_HTYPE_ALL_NOLOOP) == 0);

    /*
    if (hflag & BM_ELEM_SELECT) != 0 {
        BM_select_history_clear(bm);
    }
    */

    // note, better not attempt a fast path for selection as done with de-select
    // because hidden geometry and different selection modes can give different results,
    // we could of course check for no hidden faces and
    // then use quicker method but its not worth it.

    for i in 0..3 {
        if (htype & flag_types[i]) != 0 {
            for mut ele in iter_ele_seq_all[i].iter_mut() {
                if unlikely!(respect_hide && ((ele.head.hflag & BM_ELEM_HIDDEN) != 0)) {
                    /* pass */
                }
                else if hflag_test == 0 || ((ele.head.hflag & hflag_test) != 0) {
                    /* match! */
                    if (hflag & BM_ELEM_SELECT) != 0 {
                        BM_elem_select_set(ele, true, bm.select_mode);
                    }
                    ele.head.hflag |= hflag_nosel;
                } else if overwrite {
                    /* no match! */
                    if (hflag & BM_ELEM_SELECT) != 0 {
                        BM_elem_select_set(ele, false, bm.select_mode);
                    }
                    ele.head.hflag &= !hflag;
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn BM_mesh_elem_hflag_disable_all(
    bm: &mut BMesh, htype: BMElemType, hflag: BMElemFlag,
    respect_hide: bool,
) {
    // call with 0 hflag_test
    BM_mesh_elem_hflag_disable_test(bm, htype, hflag, respect_hide, false, BMElemFlag(0));
}

#[allow(dead_code)]
pub fn BM_mesh_elem_hflag_enable_all( /* STUB */ ) {}

/// Hide unless any connected elements are visible.
/// Run this after hiding a connected edge or face.
fn vert_flush_hide_set(mut v: BMVertMutP) {
    let hide = !bm_vert_is_edge_visible_any(v);
    v.head.hflag.set(BM_ELEM_HIDDEN, hide);
}

/// Hide unless any connected elements are visible.
/// Run this after hiding a connected face.
fn edge_flush_hide_set(mut e: BMEdgeMutP) {
    let hide = !bm_edge_is_face_visible_any(e);
    e.head.hflag.set(BM_ELEM_HIDDEN, hide);
}

pub fn BM_vert_hide_set(mut v: BMVertMutP, hide: bool) {
    debug_assert!((v.head.hflag & BM_ELEM_SELECT) == 0);

    v.head.hflag.set(BM_ELEM_HIDDEN, hide);

    if v.e != null() {
        let e_first = v.e;
        let mut e_iter = e_first;
        loop {
            e_iter.head.hflag.set(BM_ELEM_HIDDEN, hide);
            if e_iter.l != null() {
                let l_radial_first = e_iter.l;
                let mut l_radial_iter = l_radial_first;
                loop {
                    l_radial_iter.f.head.hflag.set(BM_ELEM_HIDDEN, hide);

                    l_radial_iter = l_radial_iter.radial_next;
                    if l_radial_iter == l_radial_first {
                        break;
                    }
                }
            }

            e_iter = bmesh_disk_edge_next_mut(e_iter, v);
            if e_iter == e_first {
                break;
            }
        }
    }
}

pub fn BM_edge_hide_set(mut e: BMEdgeMutP, hide: bool) {
    debug_assert!((e.head.hflag & BM_ELEM_SELECT) == 0);

    // edge hiding: faces around the edge
    bm_iter_loops_of_edge_radial_mut!(e, mut l_iter, {
        l_iter.f.head.hflag.set(BM_ELEM_HIDDEN, hide);
    });

    e.head.hflag.set(BM_ELEM_HIDDEN, hide);

    // hide vertices if necessary
    if hide {
        for v in &e.verts {
            vert_flush_hide_set(*v);
        }
    } else {
        for v in &mut e.verts {
            vert_flush_hide_set(*v);
            (*v).head.hflag &= !BM_ELEM_HIDDEN;
        }
    }
}

pub fn BM_face_hide_set(mut f: BMFaceMutP, hide: bool) {
    debug_assert!((f.head.hflag & BM_ELEM_SELECT) == 0);

    f.head.hflag.set(BM_ELEM_HIDDEN, hide);

    if hide {
        bm_iter_loops_of_face_cycle_mut!(f, l_iter, {
            edge_flush_hide_set(l_iter.e);
        });
        bm_iter_loops_of_face_cycle_mut!(f, l_iter, {
            vert_flush_hide_set(l_iter.v);
        });
    } else {
        bm_iter_loops_of_face_cycle_mut!(f, mut l_iter, {
            l_iter.e.head.hflag.set(BM_ELEM_HIDDEN, hide);
            l_iter.v.head.hflag.set(BM_ELEM_HIDDEN, hide);
        });
    }
}

#[allow(dead_code)]
pub fn BM_vert_hide_set_noflush(mut v: BMVertMutP, hide: bool) {
    v.head.hflag.set(BM_ELEM_HIDDEN, hide);
}
#[allow(dead_code)]
pub fn BM_edge_hide_set_noflush(mut e: BMEdgeMutP, hide: bool) {
    debug_assert!((e.head.hflag & BM_ELEM_SELECT) == 0);
    e.head.hflag.set(BM_ELEM_HIDDEN, hide);
}
#[allow(dead_code)]
pub fn BM_face_hide_set_noflush(mut f: BMFaceMutP, hide: bool) {
    debug_assert!((f.head.hflag & BM_ELEM_SELECT) == 0);
    f.head.hflag.set(BM_ELEM_HIDDEN, hide);
}

#[allow(dead_code)]
pub fn _bm_elem_hide_set( /* STUB */ ) {}

