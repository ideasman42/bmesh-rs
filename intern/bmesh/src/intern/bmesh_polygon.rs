// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

extern crate polyfill_2d;

use prelude::*;

use intern::bmesh_core::{
    bmesh_kernel_loop_reverse,
    BM_face_create_verts,
    bmesh_face_swap_data,
    BM_face_kill,
};

use intern::bmesh_queries::{
    BM_face_is_normal_valid,
};
use intern::bmesh_construct::{
    BM_elem_attrs_copy,
};

use intern::bmesh_interp::{
    BM_face_interp_multires_ex,
};

use math_misc::{
    add_newell_cross_v3_v3v3,
    iadd_v3_v3,
    imul_v3_fl,
    len_v3,
    negate_v3,
    normalize_v3,
    mul_m3v3_as_v2,
    axis_dominant_v3_to_m3_negate,
};

fn bm_face_calc_poly_normal(
    f: BMFaceConstP,
    n: &mut [f64; 3],
) -> f64 {
    // Newell's Method
    bm_iter_loops_of_face_cycle!(f, l_iter, {
        add_newell_cross_v3_v3v3(n, &l_iter.v.co, &l_iter.next.v.co);
    });

    return normalize_v3(n);
}

#[allow(dead_code)]
fn bm_face_calc_poly_normal_vertex_cos( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_face_calc_poly_center_mean_vertex_cos( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_tessellation( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_point_in_face( /* STUB */ ) {}

pub fn BM_face_calc_area<F>(f: F) -> f64
    where
    F: Into<BMFaceConstP>,
{
    into_expand!(f);

    let mut n: [f64; 3] = [0.0, 0.0, 0.0];

    bm_iter_loops_of_face_cycle!(f, l_iter, {
        add_newell_cross_v3_v3v3(&mut n, &l_iter.v.co, &l_iter.next.v.co);
    });

    return len_v3(&n) * 0.5;
}

#[allow(dead_code)]
pub fn BM_face_calc_perimeter( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_vert_tri_find_unique_edge( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_tri_calc_tangent_edge( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_tri_calc_tangent_edge_pair( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_tangent_edge( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_tangent_edge_pair( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_tangent_edge_diagonal( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_tangent_vert_diagonal( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_tangent_auto( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_center_bounds( /* STUB */ ) {}

///
/// Computes the center of a face, using the mean average.
///
pub fn BM_face_calc_center_mean<F>(f: F) -> [f64; 3]
    where
    F: Into<BMFaceConstP>,
{
    into_expand!(f);

    let mut center = [0.0; 3];
    bm_iter_loops_of_face_cycle!(f, l_iter, {
        iadd_v3_v3(&mut center, &l_iter.v.co);
    });
    imul_v3_fl(&mut center, 1.0 / f.len as f64);
    return center;
}

#[allow(dead_code)]
pub fn BM_face_calc_center_mean_weighted( /* STUB */ ) {}
#[allow(dead_code)]
pub fn poly_rotate_plane( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_normals_update( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_loop_normal_accum( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_calc_normal( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_normal_update_all( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_normal_update( /* STUB */ ) {}

pub fn BM_face_calc_normal<F>(f: F, r_no: &mut [f64; 3]) -> f64
    where
    F: Into<BMFaceConstP>,
{
    into_expand!(f);

    // common cases first

    /*
    BMLoop *l;
    match f.len {
        4 => {
            let l = f.l_first;
            let co1 = l.co; l = l.next;
            let co2 = l.co; l = l.next;
            let co3 = l.co; l = l.next;
            let co4 = l.co;
            return normal_quad_v3(r_no, co1, co2, co3, co4);
        }
        3 => {
            let l = f.l_first;
            let co1 = l.co; l = l.next;
            let co2 = l.co; l = l.next;
            let co3 = l.co;
            return normal_tri_v3(r_no, co1, co2, co3);
        }
        _ => {
            return bm_face_calc_poly_normal(f, r_no);
        }
    }
    */
    return bm_face_calc_poly_normal(f, r_no);
}


/// Exposed as: BMFace.normal_update()
pub fn BM_face_normal_update(f: BMFaceMutP) {
    BM_face_calc_normal(f, &mut f.clone().no);
}

#[allow(dead_code)]
pub fn BM_face_calc_normal_vcos( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_normal_subset( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_calc_center_mean_vcos( /* STUB */ ) {}

///
/// \brief Face Flip Normal
///
/// Reverses the winding of a face.
/// \note This updates the calculated normal.
///
/// Exposed as: BMesh.face_flip_ex()
///
pub fn BM_face_normal_flip_ex(
    bm: &mut BMesh,
    f: BMFaceMutP,
    cd_loop_mdisp_offset: isize,
    use_loop_mdisp_flip: bool,
) {
    bmesh_kernel_loop_reverse(bm, f, cd_loop_mdisp_offset, use_loop_mdisp_flip);
    negate_v3(&mut f.clone().no);
}

///
/// Exposed as: BMesh.face_flip()
///
pub fn BM_face_normal_flip(
    bm: &mut BMesh,
    f: BMFaceMutP,
) {
    let cd_loop_mdisp_offset = CustomData_get_offset(&bm.ldata, CD_MDISPS);
    BM_face_normal_flip_ex(bm, f, cd_loop_mdisp_offset, true);
}

#[allow(dead_code)]
pub fn BM_face_point_inside_test( /* STUB */ ) {}

///
/// \brief BMESH TRIANGULATE FACE
///
/// Breaks all quads and ngons down to triangles.
/// It uses polyfill for the ngons splitting, and
/// the beautify operator when use_beauty is true.
///
/// \param r_faces_new if non-null, must be an array of BMFace pointers,
/// with a length equal to (f->len - 3). It will be filled with the new
/// triangles (not including the original triangle).
///
/// \param r_faces_double: When newly created faces are duplicates of existing faces,
/// they're added to this list.
/// Caller must handle de-duplication.
/// This is done because its possible _all_ faces exist already,
/// and in that case we would have to remove all faces including the one passed,
/// which causes complications adding/removing faces while looking over them.
///
/// \note The number of faces is _almost_ always (f->len - 3),
///       However there may be faces that already occupying the
///       triangles we would make, so the caller must check \a r_faces_new_tot.
///
/// \note use_tag tags new flags and edges.
///
/// RS-TODO, MOD_TRIANGULATE_NGON_BEAUTY support!

/// RS-TODO, We may want to replace multiple args with callbacks which can manage double faces,
/// 'use_tag', edges-new faces-new.
///
pub fn BM_face_triangulate(
    bm: &mut BMesh,
    f: BMFaceMutP,
    r_faces_new: &mut Option<&mut Vec<BMFaceMutP>>,
    r_edges_new: &mut Option<&mut Vec<BMEdgeMutP>>,
    r_faces_double: &mut Vec<BMFaceMutP>,
    use_tag: bool,

/*
    LinkNode **r_faces_double,
    const int quad_method,
    const int ngon_method,
    const bool use_tag,
    /* use for ngons only! */
    MemArena *pf_arena,

    /* use for MOD_TRIANGULATE_NGON_BEAUTY only! */
    struct Heap *pf_heap, struct EdgeHash *pf_ehash
*/
) {
    let cd_loop_mdisp_offset = CustomData_get_offset(&bm.ldata, CD_MDISPS);
    // let use_beauty = false /* (ngon_method == MOD_TRIANGULATE_NGON_BEAUTY); */ ;
    // BMLoop *l_first, *l_new;
    let mut f_new: BMFaceMutP = null_mut();
    // int nf_i = 0;
    // int ne_i = 0;

    debug_assert!(BM_face_is_normal_valid(f));
    debug_assert!(f.len > 3);

    let len = f.len as usize;

    {
        let mut loops: Vec<BMLoopMutP> = Vec::with_capacity(len);
        let mut tris: Vec<[u32; 3]> = Vec::with_capacity(len);
        // let totfilltri = len - 2;
        let last_tri = len - 3;

        /*
        if f.len == 4 {
            // even though we're not using BLI_polyfill, fill in 'tris' and 'loops'
            // so we can share code to handle face creation afterwards.
            BMLoop *l_v1, *l_v2;

            let l_first = f.l_first;

            switch (quad_method) {
                case MOD_TRIANGULATE_QUAD_FIXED:
                {
                    l_v1 = l_first;
                    l_v2 = l_first.next.next;
                    break;
                }
                case MOD_TRIANGULATE_QUAD_ALTERNATE:
                {
                    l_v1 = l_first.next;
                    l_v2 = l_first.prev;
                    break;
                }
                case MOD_TRIANGULATE_QUAD_SHORTEDGE:
                case MOD_TRIANGULATE_QUAD_BEAUTY:
                default:
                {
                    BMLoop *l_v3, *l_v4;
                    bool split_24;

                    l_v1 = l_first.next;
                    l_v2 = l_first.next.next;
                    l_v3 = l_first.prev;
                    l_v4 = l_first;

                    if (quad_method == MOD_TRIANGULATE_QUAD_SHORTEDGE) {
                        float d1, d2;
                        d1 = len_squared_v3v3(l_v4.v.co, l_v2.v.co);
                        d2 = len_squared_v3v3(l_v1.v.co, l_v3.v.co);
                        split_24 = ((d2 - d1) > 0.0f);
                    }
                    else {
                        // first check if the quad is concave on either diagonal
                        const int flip_flag = is_quad_flip_v3(
                            l_v1.v.co, l_v2.v.co, l_v3.v.co, l_v4.v.co);
                        if (unlikely!(flip_flag & (1 << 0))) {
                            split_24 = true;
                        }
                        else if (unlikely!(flip_flag & (1 << 1))) {
                            split_24 = false;
                        }
                        else {
                            split_24 = (BM_verts_calc_rotate_beauty(
                                l_v1.v, l_v2.v, l_v3.v, l_v4.v, 0, 0) > 0.0f);
                        }
                    }

                    /* named confusingly, l_v1 is in fact the second vertex */
                    if (split_24) {
                        l_v1 = l_v4;
                        //l_v2 = l_v2;
                    }
                    else {
                        //l_v1 = l_v1;
                        l_v2 = l_v3;
                    }
                    break;
                }
            }

            loops[0] = l_v1;
            loops[1] = l_v1.next;
            loops[2] = l_v2;
            loops[3] = l_v2.next;

            ARRAY_SET_ITEMS(tris[0], 0, 1, 2);
            ARRAY_SET_ITEMS(tris[1], 0, 2, 3);
        } else
        */
        {
            let mut projverts: Vec<[f64; 2]> = Vec::with_capacity(len);

            let axis_mat = axis_dominant_v3_to_m3_negate(&f.no);

            bm_iter_loops_of_face_cycle_mut!(f, l_iter, {
                loops.push(l_iter);
                projverts.push(mul_m3v3_as_v2(&axis_mat, &l_iter.v.co));
            });

            /* RS-TODO, pf_arena */
            use polyfill_2d;
            polyfill_2d::calc(&projverts, 1, &mut tris /*, pf_arena */);

            /* RS-TODO */
            /*
            if (use_beauty) {
                BLI_polyfill_beautify(
                        (const float (*)[2])projverts, f.len, tris,
                        pf_arena, pf_heap, pf_ehash);
            }
            */

            // BLI_memarena_clear(pf_arena);
        }

        let f_center = {
            if cd_loop_mdisp_offset != -1 {
                Some(BM_face_calc_center_mean(f))
            } else {
                None
            }
        };

        debug_assert!(tris.len() == len - 2);
        // loop over calculated triangles and create new geometry
        for (i, t) in tris.iter().enumerate() {
            let l_tri = [
                loops[t[0] as usize],
                loops[t[1] as usize],
                loops[t[2] as usize],
            ];

            let v_tri = [
                l_tri[0].v,
                l_tri[1].v,
                l_tri[2].v,
            ];

            f_new = BM_face_create_verts(bm, &v_tri, Some(f.as_const()), BMElemCreate::NOP, true);
            let l_new = f_new.l_first;

            debug_assert!(v_tri[0] == l_new.v);

            // check for duplicate
            if l_new.radial_next != l_new {
                let mut l_iter = l_new.radial_next;
                loop {
                    if unlikely!((l_iter.f.len == 3) && (l_new.prev.v == l_iter.prev.v)) {
                        // Check the last tri because we swap last f_new with f at the end...
                        r_faces_double.push(if i != last_tri { f_new } else { f });
                        break;
                    }
                    l_iter = l_iter.radial_next;
                    if l_iter == l_new {
                        break;
                    }
                }
            }

            // copy CD data
            BM_elem_attrs_copy(&mut None, bm, l_tri[0].as_elem(), l_new.as_elem());
            BM_elem_attrs_copy(&mut None, bm, l_tri[1].as_elem(), l_new.next.as_elem());
            BM_elem_attrs_copy(&mut None, bm, l_tri[2].as_elem(), l_new.prev.as_elem());

            // add all but the last face which is swapped and removed (below)
            if i != last_tri {
                if use_tag {
                    f_new.head.hflag |= BM_ELEM_TAG;
                }
                if let Some(faces_new) = r_faces_new.as_mut() {
                    faces_new.push(f_new);
                }
            }

            if use_tag || r_edges_new.is_none() == false {
                // new faces loops

                let l_first = l_new;
                let mut l_iter = l_first;
                loop {
                    let mut e = l_iter.e;
                    // confusing! if its not a boundary now, we know it will be later
                    // since this will be an edge of one of the new faces
                    // which we're in the middle of creating
                    let is_new_edge = l_iter == l_iter.radial_next;

                    if is_new_edge {
                        if use_tag {
                            e.head.hflag |= BM_ELEM_TAG;
                        }
                        if let Some(edges_new) = r_edges_new.as_mut() {
                            edges_new.push(e);
                        }
                    }
                    // note, never disable tag's
                    l_iter = l_iter.next;
                    if l_iter == l_first {
                        break;
                    }
                }
            }

            if cd_loop_mdisp_offset != -1 {
                let f_new_center = BM_face_calc_center_mean(f_new);
                BM_face_interp_multires_ex(
                    bm, f_new, f, &f_new_center, &f_center.unwrap(), cd_loop_mdisp_offset);
            }
        }

        {
            // We can't delete the real face,
            // because some of the callers expect it to remain valid.
            // So swap data and delete the last created tri
            bmesh_face_swap_data(f, f_new);
            BM_face_kill(bm, f_new);
        }
    }
    bm.elem_index_dirty |= BM_FACE;
}

#[allow(dead_code)]
pub fn BM_face_splits_check_legal( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_splits_check_optimal( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_as_array_vert_tri( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_as_array_vert_quad( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_as_array_loop_tri( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_as_array_loop_quad( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_calc_tessellation( /* STUB */ ) {}

