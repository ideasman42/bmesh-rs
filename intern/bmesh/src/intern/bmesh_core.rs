// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use prelude::*;

use intern::bmesh_construct::{
    BM_elem_attrs_copy,
    BM_elem_select_copy,
    BM_face_create_ngon,
    BM_edges_from_verts,
    BM_edges_from_verts_ensure,
};
use intern::bmesh_interp::{
    BM_loop_interp_multires_ex,
};
use intern::bmesh_polygon::{
    BM_face_calc_center_mean,
};
use intern::bmesh_queries::{
    BM_edge_exists,
    BM_edge_exists_mut,
    BM_edge_is_boundary,
    BM_edge_other_vert,
    BM_edge_other_vert_mut,
    BM_face_exists_mut,
    BM_vert_in_edge,
    BM_vert_pair_share_face_check,
    BM_verts_in_edge,
};
use intern::bmesh_structure::{
    bmesh_disk_count,
    bmesh_disk_count_ex,
    bmesh_disk_edge_append,
    bmesh_disk_edge_next,
    bmesh_disk_edge_next_mut,
    bmesh_disk_edge_remove,
    bmesh_disk_validate,
    bmesh_disk_vert_replace,
    bmesh_edge_vert_swap,
    bmesh_loop_validate,
    bmesh_radial_length,
    bmesh_radial_loop_append,
    bmesh_radial_loop_remove,
    bmesh_radial_loop_unlink,
    bmesh_radial_validate,
};

use bmesh_class::bmesh_bitflag_bmelem_api_flag::BMElemApiFlag;


// ----------------------------------------------------------------------------
// Public Function Arg Types

struct_bitflag_impl!(pub struct BMElemCreate(pub u8));
impl BMElemCreate {
    pub const NOP: Self = BMElemCreate(0);
    // faces and edges only
    pub const NO_DOUBLE: Self = BMElemCreate(1 << 1);
    // Skip CustomData - for all element types data,
    // use if we immediately write customdata
    // into the element so this skips copying from 'example'
    // args or setting defaults, speeds up conversion when data is converted all at once.
    pub const SKIP_CD: Self = BMElemCreate(1 << 2);
}


// ----------------------------------------------------------------------------
// Element Create API

/// Exposed as BMVert::new()
pub fn BM_vert_create(
    bm: &mut BMesh,
    co: &[f64; 3],
    v_ref: Option<BMVertConstP>,
    _create_flag: BMElemCreate,
) -> BMVertMutP
{
    // BMVert v = {co: *co, index: self.len(), ..BMVert::default()};
    let mut v = bm.verts.alloc_elem();

    // TODO. write into all members for now init to default;
    *v = BMVert::default();

    v.head.htype = BM_VERT;
    v.head.index = -1;
    v.co = *co;

    // may add to middle of the pool
    bm.elem_index_dirty |= BM_VERT;

    if let Some(v_ref) = v_ref {
        v.no = v_ref.no;
    }

    return v;
}

/// Exposed as BMEdge::new()
pub fn BM_edge_create(
    bm: &mut BMesh,
    verts: &[BMVertMutP; 2],
    e_ref: Option<BMEdgeConstP>,
    create_flag: BMElemCreate,
) -> BMEdgeMutP
{
    debug_assert!(verts[0] != null() && verts[1] != null());

    if (create_flag & BMElemCreate::NO_DOUBLE) != 0 {
        let e = BM_edge_exists_mut(verts[0], verts[1]);
        if e != null() {
            return e;
        }
    }

    let mut e = bm.edges.alloc_elem();

    // TODO. write into all members for now init to default;
    *e = BMEdge::default();

    e.head.htype = BM_EDGE;
    e.head.index = -1;

    e.verts = *verts;

    // may add to middle of the pool
    bm.elem_index_dirty |= BM_EDGE;

    // clone is a nop, make borrow checker happy
    e.verts[0].clone().disk_edge_append(e);
    e.verts[1].clone().disk_edge_append(e);

    if let Some(_e_ref) = e_ref {
        // TODO
    }
    return e;
}

fn bm_loop_create(
    bm: &mut BMesh,
    v: BMVertMutP,
    e: BMEdgeMutP,
    f: BMFaceMutP,
    l_ref: Option<BMLoopConstP>,
    _create_flag: BMElemCreate,
) -> BMLoopMutP {

    let mut l = bm.loops.alloc_elem();
    // TODO. write into all members for now init to default;
    *l = BMLoop::default();

    l.head.htype = BM_LOOP;
    l.head.index = -1;

    l.v = v;
    l.e = e;
    l.f = f;

    l.radial_next = null_mut();
    l.radial_prev = null_mut();

    l.next = null_mut();
    l.prev = null_mut();

    // may add to middle of the pool
    bm.elem_index_dirty |= BM_LOOP;

    if let Some(_l_ref) = l_ref {
        // TODO
    }
    return l;
}

fn bm_face_boundary_add(
    bm: &mut BMesh,
    mut f: BMFaceMutP,
    startv: BMVertMutP,
    starte: BMEdgeMutP,
    create_flag: BMElemCreate,
) -> BMLoopMutP {
    let l = bm_loop_create(bm, startv, starte, f, None, create_flag);

    bmesh_radial_loop_append(starte, l);

    f.l_first = l;

    return l;
}

pub fn BM_face_copy(
    bm_dst: &mut BMesh,
    bm_src: &mut Option<&mut BMesh>,
    f: BMFaceMutP,
    copy_verts: bool, copy_edges: bool
) -> BMFaceMutP {
    let mut verts: Vec<BMVertMutP> = Vec::with_capacity(f.len as usize);
    let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(f.len as usize);

    debug_assert!(bm_src.is_none() || (copy_verts && copy_edges));

    {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        unsafe { verts.set_len(f.len as usize); }
        for v in verts.iter_mut() {
            *v = {
                if copy_verts {
                    BM_vert_create(bm_dst, &l_iter.v.co,
                                   Some(l_iter.v.as_const()), BMElemCreate::NOP)
                } else {
                    l_iter.v
                }
            };
            l_iter = l_iter.next;
        }
    }

    {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        for i in 0..(f.len as usize) {
            unsafe {
                *edges.get_unchecked_mut(i) = {
                    if copy_edges {
                        let i_next = (i + 1) % f.len as usize;
                        let e_verts = {
                            if l_iter.e.verts[0] == verts[i] {
                                [verts[i], verts[i_next]]
                            } else {
                                [verts[i_next], verts[i]]
                            }
                        };
                        BM_edge_create(bm_dst, &e_verts,
                                       Some(l_iter.e.as_const()), BMElemCreate::NOP)
                    } else {
                        l_iter.e
                    }
                };
                l_iter = l_iter.next;
            }
        }
    }

    let f_copy = BM_face_create(bm_dst, &verts[..], &edges[..], None, BMElemCreate::SKIP_CD);

    BM_elem_attrs_copy(bm_src, bm_dst, f.as_elem(), f_copy.as_elem());

    {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        let mut l_copy = f_copy.l_first;
        loop {
            BM_elem_attrs_copy(bm_src, bm_dst, l_iter.as_elem(), l_copy.as_elem());

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
            l_copy = l_copy.next;
        }
    }

    return f_copy;
}

fn bm_face_create__internal(bm: &mut BMesh) -> BMFaceMutP {
    let mut f = bm.faces.alloc_elem();

    // TODO. write into all members for now init to default;
    *f = BMFace::default();

    f.head.htype = BM_FACE;
    f.head.index = -1;

    // may add to middle of the pool
    bm.elem_index_dirty |= BM_FACE;

    return f;
}

/// Exposed as BMFace::new()
pub fn BM_face_create(
    bm: &mut BMesh,
    verts: &[BMVertMutP],
    edges: &[BMEdgeMutP],
    f_ref: Option<BMFaceConstP>,
    create_flag: BMElemCreate,
) -> BMFaceMutP {

    if verts.is_empty() {
        return null_mut();
    }

    if (create_flag & BMElemCreate::NO_DOUBLE) != 0 {
        let f = BM_face_exists_mut(verts);
        if f != null() {
            return f;
        }
    }

    debug_assert!(verts.len() == edges.len());

    let mut f = bm_face_create__internal(bm);

    let mut startl = bm_face_boundary_add(bm, f, verts[0], edges[0], create_flag);
    let mut lastl = startl;

    // for (mut v, mut e) in verts.iter().cloned().zip(edges.iter().cloned()).skip(1) {
    for i in 1..verts.len() {
        let v = unsafe { *verts.get_unchecked(i) };
        let e = unsafe { *edges.get_unchecked(i) } ;
        let mut l = bm_loop_create(bm, v, e, f, None, create_flag);

        debug_assert!(lastl.e.contains_vert_pair(v, lastl.v));

        bmesh_radial_loop_append(e, l);

        l.prev = lastl;
        lastl.next = l;
        lastl = l;
    }

    startl.prev = lastl;
    lastl.next = startl;

    f.len = verts.len() as i32;

    // TODO, create_flag

    if let Some(mut _f_ref) = f_ref {
        // TODO
    }
    return f;
}

pub fn BM_face_create_verts(
    bm: &mut BMesh,
    vert_arr: &[BMVertMutP],
    f_example: Option<BMFaceConstP>,
    create_flag: BMElemCreate,
    create_edges: bool,
) -> BMFaceMutP {

    let mut edge_arr = Vec::with_capacity(vert_arr.len());
    unsafe {
        edge_arr.set_len(vert_arr.len());
    }

    if create_edges {
        BM_edges_from_verts_ensure(bm, &mut edge_arr, vert_arr);
    } else {
        if BM_edges_from_verts(bm, &mut edge_arr, vert_arr) == false {
            return null_mut();
        }
    }

    return BM_face_create(bm, vert_arr, &edge_arr, f_example, create_flag);
}

#[allow(dead_code)]
pub fn BM_face_edges_kill(
    bm: &mut BMesh,
    f: BMFaceMutP,
) {
    let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(f.len as usize);
    unsafe {
        edges.set_len(f.len as usize);
    }
    let mut l_iter = f.l_first;
    for e in edges.iter_mut() {
        *e = l_iter.e;
        l_iter = l_iter.next;
    }
    for e in edges.iter_mut() {
        BM_edge_kill(bm, *e);
    }
}

pub fn BM_face_verts_kill(
    bm: &mut BMesh,
    f: BMFaceMutP,
) {
    let mut verts: Vec<BMVertMutP> = Vec::with_capacity(f.len as usize);
    unsafe {
        verts.set_len(f.len as usize);
    }
    let mut l_iter = f.l_first;
    for v in verts.iter_mut() {
        *v = l_iter.v;
        l_iter = l_iter.next;
    }
    for v in verts.iter_mut() {
        BM_vert_kill(bm, *v);
    }
}

pub fn BM_face_kill(bm: &mut BMesh, f: BMFaceMutP) {
    if f.l_first != null() {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            let l_next = l_iter.next;
            bmesh_radial_loop_remove(l_iter.e, l_iter);
            bm_loop_kill_only(bm, l_iter);

            l_iter = l_next;
            if l_iter == l_first {
                break;
            }
        }
    }
    bm_face_kill_only(bm, f);
}

#[allow(dead_code)]
pub fn BM_face_kill_loose(bm: &mut BMesh, f: BMFaceMutP) {

    bm_check_element!(f);

    if f.l_first != null() {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            let l_next = l_iter.next;

            let e = l_iter.e;
            bmesh_radial_loop_remove(e, l_iter);
            bm_loop_kill_only(bm, l_iter);

            if e.l == null() {
                let verts: [BMVertMutP; 2] = e.verts;

                for v in &e.verts {
                    bmesh_disk_edge_remove(e, *v);
                }

                bm_edge_kill_only(bm, e);

                for v in &verts {
                    if v.e == null() {
                        bm_vert_kill_only(bm, *v);
                    }
                }
            }

            l_iter = l_next;
            if l_iter == l_first {
                break;
            }
        }
    }

    bm_face_kill_only(bm, f);
}

pub fn BM_edge_kill(bm: &mut BMesh, e: BMEdgeMutP) {
    while e.l != null() {
        debug_assert!(e.l.head.htype == BM_LOOP);
        BM_face_kill(bm, e.l.f);
    }

    for v in &e.verts {
        bmesh_disk_edge_remove(e, *v);
    }

    bm_edge_kill_only(bm, e);
}

pub fn BM_edge_kill_loose(bm: &mut BMesh, e: BMEdgeMutP) {
    while e.l != null() {
        debug_assert!(e.l.head.htype == BM_LOOP);
        BM_face_kill(bm, e.l.f);
    }

    for v in &e.verts {
        bmesh_disk_edge_remove(e, *v);
        // remove loose
        if v.e == null() {
            BM_vert_kill(bm, *v);
        }
    }

    bm_edge_kill_only(bm, e);
}

pub fn BM_vert_kill(bm: &mut BMesh, v: BMVertMutP) {
    while v.e != null() {
        debug_assert!(v.head.htype == BM_VERT);
        debug_assert!(v.e.head.htype == BM_EDGE);
        BM_edge_kill(bm, v.e);
    }
    bm_vert_kill_only(bm, v);
}

#[allow(dead_code)]
fn UNUSED_bm_loop_length( /* STUB */ ) {}

///
/// \brief Loop Reverse
///
/// Changes the winding order of a face from CW to CCW or vice versa.
///
/// \param cd_loop_mdisp_offset: Cached result of `CustomData_get_offset(&bm->ldata, CD_MDISPS)`.
/// \param use_loop_mdisp_flip: When set, flip the Z-depth of the mdisp,
/// (use when flipping normals, disable when mirroring, eg: symmetrize).
///
pub fn bmesh_kernel_loop_reverse(
    bm: &mut BMesh,
    f: BMFaceMutP,
    cd_loop_mdisp_offset: isize,
    use_loop_mdisp_flip: bool,
) {

    let l_first = f.l_first;

    // track previous cycles radial state
    let mut e_prev = l_first.prev.e;
    let mut l_prev_radial_next = l_first.prev.radial_next;
    let mut l_prev_radial_prev = l_first.prev.radial_prev;
    let mut is_prev_boundary = l_prev_radial_next == l_prev_radial_next.radial_next;

    let mut l_iter = l_first;
    loop {
        let mut e_iter = l_iter.e;
        let l_iter_radial_next = l_iter.radial_next;
        let l_iter_radial_prev = l_iter.radial_prev;
        let is_iter_boundary = l_iter_radial_next == l_iter_radial_next.radial_next;

        if false {
            bmesh_radial_loop_remove(e_iter, l_iter);
            bmesh_radial_loop_append(e_prev, l_iter);
        } else {
            // inline loop reversal
            if is_prev_boundary {
                // boundary
                l_iter.radial_next = l_iter;
                l_iter.radial_prev = l_iter;
            } else {
                // non-boundary, replace radial links
                l_iter.radial_next = l_prev_radial_next;
                l_iter.radial_prev = l_prev_radial_prev;
                l_prev_radial_next.radial_prev = l_iter;
                l_prev_radial_prev.radial_next = l_iter;
            }

            if e_iter.l == l_iter {
                e_iter.l = l_iter.next;
            }
            l_iter.e = e_prev;
        }

        ::std::mem::swap(
            &mut l_iter.clone().next,
            &mut l_iter.clone().prev,
        );

        if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
            let md: &mut MDisps = bm_elem_cd_get_void_p!(l_iter, MDisps, cd_loop_mdisp_offset);
            use mesh_evaluate::bke_mesh_mdisp_flip;
            bke_mesh_mdisp_flip(md, use_loop_mdisp_flip);
        }

        e_prev = e_iter;
        l_prev_radial_next = l_iter_radial_next;
        l_prev_radial_prev = l_iter_radial_prev;
        is_prev_boundary = is_iter_boundary;

        // step to next (now swapped)
        l_iter = l_iter.prev;
        if l_iter == l_first {
            break;
        }
    }

    // validate radial
    if cfg!(debug_assertions) {
        let mut l_iter = l_first;
        loop {
            bm_check_element!(l_iter);
            bm_check_element!(l_iter.e);
            bm_check_element!(l_iter.v);
            bm_check_element!(l_iter.f);

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
        bm_check_element!(f);
    }

    // Loop indices are no more valid!
    bm.elem_index_dirty |= BM_LOOP;
}

fn bm_elements_systag_enable(
    // eles: &[BMElemMutP],
    eles: &[BMFaceMutP], // XXX, should be cast to BMElemMutP
    api_flag: BMElemApiFlag,
) {
    for e in eles {
        bm_elem_api_flag_enable!(e.clone(), api_flag);
    }
}

fn bm_elements_systag_disable(
    // eles: &[BMElemMutP],
    eles: &[BMFaceMutP], // XXX, should be cast to BMElemMutP
    api_flag: BMElemApiFlag,
) {
    for e in eles {
        bm_elem_api_flag_disable!(e.clone(), api_flag);
    }
}

fn bm_loop_systag_count_radial<L>(l: L, api_flag: BMElemApiFlag) -> usize
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);

    let mut count: usize = 0;
    bm_iter_loops_of_edge_radial!(l.e, l_iter, {
        if bm_elem_api_flag_test!(l_iter.f, api_flag) {
            count += 1;
        }
    });
    return count;
}

#[allow(dead_code)]
fn UNUSED_bm_vert_systag_count_disk( /* STUB */ ) {}

fn bm_vert_is_manifold_flagged<V>(v: V, api_flag: BMElemApiFlag) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let mut e = v.e.as_const();
    if e == null() {
        return false;
    }

    loop {
        let mut l = e.l.as_const();

        if l == null() {
            return false;
        }

        if BM_edge_is_boundary(l.e) {
            return false;
        }

        loop {
            if !bm_elem_api_flag_test!(l.f, api_flag) {
                return false;
            }
            l = l.radial_next.as_const();
            if l == e.l {
                break;
            }
        }

        e = bmesh_disk_edge_next(e, v);
        if e == v.e {
            break;
        }
    }

    return true;
}

///
/// \brief Join Connected Faces
///
/// Joins a collected group of faces into one. Only restriction on
/// the input data is that the faces must be connected to each other.
///
/// \return The newly created combine BMFace.
///
/// \note If a pair of faces share multiple edges,
/// the pair of faces will be joined at every edge.
///
/// \note this is a generic, flexible join faces function,
/// almost everything uses this, including #BM_faces_join_pair
///
/// Exposed as: BMesh.face_join_n()
pub fn BM_faces_join(
    bm: &mut BMesh,
    faces: &[BMFaceMutP],
    do_del: bool,
) -> BMFaceMutP {
    let cd_loop_mdisp_offset = CustomData_get_offset(&bm.ldata, CD_MDISPS);

    let mut v1: BMVertMutP = null_mut();
    let mut v2: BMVertMutP = null_mut();

    match faces.len() {
        0 => {
            debug_assert!(false);
            return null_mut();
        }
        1 => {
            return faces[0];
        }
        _ => {
            // pass
        }
    }

    use bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_JF;

    let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(BM_DEFAULT_NGON_STACK_SIZE);
    let mut delverts: Vec<BMVertMutP> = Vec::with_capacity(BM_DEFAULT_NGON_STACK_SIZE);
    let mut deledges: Vec<BMEdgeMutP> = Vec::with_capacity(BM_DEFAULT_NGON_STACK_SIZE);

    // simulate 'goto error;'
    'error:
    loop {
        bm_elements_systag_enable(faces, FLAG_JF);

        for f in faces {
            let l_first = f.l_first;
            let mut l_iter = l_first;
            loop {
                let rlen = bm_loop_systag_count_radial(l_iter, FLAG_JF);

                if rlen > 2 {
                    // Input faces do not form a contiguous manifold region
                    // goto error;
                    break 'error;
                } else if rlen == 1 {
                    edges.push(l_iter.e);

                    if v1 == null() {
                        v1 = l_iter.v;
                        v2 = BM_edge_other_vert_mut(l_iter.e, l_iter.v);
                    }
                } else if rlen == 2 {
                    let d1 = bm_vert_is_manifold_flagged(l_iter.e.verts[0], FLAG_JF);
                    let d2 = bm_vert_is_manifold_flagged(l_iter.e.verts[1], FLAG_JF);

                    if !d1 && !d2 && !bm_elem_api_flag_test!(l_iter.e, FLAG_JF) {
                        // don't remove an edge it makes up the side of another face
                        // else this will remove the face as well - campbell
                        if !BM_edge_face_count_is_over!(l_iter.e, 2) {
                            if do_del {
                                deledges.push(l_iter.e);
                            }
                            bm_elem_api_flag_enable!(l_iter.e, FLAG_JF);
                        }
                    } else {
                        if d1 && !bm_elem_api_flag_test!(l_iter.e.verts[0], FLAG_JF) {
                            if do_del {
                                delverts.push(l_iter.e.verts[0]);
                            }
                            bm_elem_api_flag_enable!(l_iter.e.verts[0], FLAG_JF);
                        }
                        if d2 && !bm_elem_api_flag_test!(l_iter.e.verts[1], FLAG_JF) {
                            if do_del {
                                delverts.push(l_iter.e.verts[1]);
                            }
                            bm_elem_api_flag_enable!(l_iter.e.verts[1], FLAG_JF);
                        }
                    }
                }

                l_iter = l_iter.next;
                if l_iter == l_first {
                    break;
                }
            }
        }

        // create region face
        let mut f_new = {
            if !edges.is_empty() {
                BM_face_create_ngon(
                    bm, v1, v2,
                    &edges[..], Some(faces[0].as_const()), BMElemCreate::NOP)
            } else {
                null_mut()
            }
        };

        if unlikely!(f_new == null()) {
            // Invalid boundary region to join faces
            // goto error;
            break 'error;
        }

        // copy over loop data
        {
            let l_first = f_new.l_first;
            let mut l_iter = l_first;
            loop {
                let mut l2 = l_iter.radial_next;
                loop {
                    if bm_elem_api_flag_test!(l2.f, FLAG_JF) {
                        break;
                    }
                    l2 = l2.radial_next;
                    if l2 == l_iter {
                        break;
                    }
                }
                if l2 != l_iter {
                    // loops share an edge, shared vert depends on winding
                    if l2.v != l_iter.v {
                        l2 = l2.next;
                    }
                    debug_assert!(l_iter.v == l2.v);
                    BM_elem_attrs_copy(&mut None, bm, l2.as_elem(), l_iter.as_elem());
                }
                l_iter = l_iter.next;
                if l_iter == l_first {
                    break;
                }
            }
        }

        bm_elements_systag_disable(faces, FLAG_JF);
        bm_elem_api_flag_disable!(f_new, FLAG_JF);

        // handle multi-res data
        if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
            let mut faces_center: Vec<[f64; 3]> = Vec::with_capacity(faces.len());
            let f_center = BM_face_calc_center_mean(f_new);
            for f_iter in faces {
                faces_center.push(BM_face_calc_center_mean(*f_iter));
            }
            let l_first = f_new.l_first;
            let mut l_iter = l_first;
            loop {
                for (f_iter, f_iter_center) in faces.iter().zip(faces_center.iter()) {
                    BM_loop_interp_multires_ex(
                        bm, l_iter, *f_iter,
                        &f_center, &f_iter_center, cd_loop_mdisp_offset);
                }
                l_iter = l_iter.next;
                if l_iter == l_first {
                    break;
                }
            }
        }

        // delete old geometry
        if do_del {
            for e_iter in deledges.clone() {
                BM_edge_kill(bm, e_iter);
            }
            for v_iter in delverts.clone() {
                BM_vert_kill(bm, v_iter);
            }
        } else {
            // otherwise we get both old and new faces
            for f_iter in faces {
                BM_face_kill(bm, *f_iter);
            }
        }

        drop(edges);
        drop(deledges);
        drop(delverts);

        bm_check_element!(f_new);
        return f_new;
    }  // end error check

    bm_elements_systag_disable(faces, FLAG_JF);
    drop(edges);
    drop(deledges);
    drop(delverts);

    return null_mut();
}

fn bm_face_create__sfme(
    bm: &mut BMesh,
    f_ref: BMFaceConstP,
) -> BMFaceMutP {
    let f = bm_face_create__internal(bm);

    BM_elem_attrs_copy(&mut None, bm, f_ref.as_elem(), f.as_elem());

    return f;
}

///
/// \brief Split Face Make Edge (SFME)
///
/// \warning this is a low level function, most likely you want to use #BM_face_split()
///
/// Takes as input two vertices in a single face.
/// An edge is created which divides the original face into two distinct regions.
/// One of the regions is assigned to the original face and it is closed off.
/// The second region has a new face assigned to it.
///
/// \par Examples:
/// ```text
///
/// Before:               After:
///  +--------+           +--------+
///  |        |           |        |
///  |        |           |   f1   |
/// v1   f1   v2          v1======v2
///  |        |           |   f2   |
///  |        |           |        |
///  +--------+           +--------+
/// ```
///
/// \note the input vertices can be part of the same edge. This will
/// result in a two edged face. This is desirable for advanced construction
/// tools and particularly essential for edge bevel. Because of this it is
/// up to the caller to decide what to do with the extra edge.
///
/// \note If \a holes is NULL, then both faces will lose
/// all holes from the original face.  Also, you cannot split between
/// a hole vert and a boundary vert; that case is handled by higher-
/// level wrapping functions (when holes are fully implemented, anyway).
///
/// \note that holes represents which holes goes to the new face, and of
/// course this requires removing them from the existing face first, since
/// you cannot have linked list links inside multiple lists.
///
/// \return A BMFace pointer
///
pub fn bmesh_kernel_split_face_make_edge(
    bm: &mut BMesh,
    mut f: BMFaceMutP,
    mut l_v1: BMLoopMutP,
    mut l_v2: BMLoopMutP,
    e_ref: Option<BMEdgeConstP>,
    no_double: bool,
) -> (BMFaceMutP, BMLoopMutP) {
    let v1 = l_v1.v;
    let v2 = l_v2.v;

    debug_assert!(f == l_v1.f && f == l_v2.f);

    // allocate new edge between v1 and v2
    let e = BM_edge_create(
        bm, &[v1, v2], e_ref,
        if no_double { BMElemCreate::NO_DOUBLE } else { BMElemCreate::NOP });

    let mut f2 = bm_face_create__sfme(bm, f.as_const());
    let mut l_f1 = bm_loop_create(bm, v2, e, f, Some(l_v2.as_const()), BMElemCreate::NOP);
    let mut l_f2 = bm_loop_create(bm, v1, e, f2, Some(l_v1.as_const()), BMElemCreate::NOP);

    l_f1.prev = l_v2.prev;
    l_f2.prev = l_v1.prev;
    l_v2.prev.next = l_f1;
    l_v1.prev.next = l_f2;

    l_f1.next = l_v1;
    l_f2.next = l_v2;
    l_v1.prev = l_f1;
    l_v2.prev = l_f2;

    // find which of the faces the original first loop is in
    let mut first_loop_f1 = false;
    {
        let l_first = l_f1;
        let mut l_iter = l_first;
        loop {
            if l_iter == f.l_first {
                first_loop_f1 = true;
            }

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
    }

    if first_loop_f1 {
        // original first loop was in f1, find a suitable first loop for f2
        // which is as similar as possible to f1. the order matters for tools
        // such as duplifaces.
        f2.l_first = {
            if f.l_first.prev == l_f1 {
                l_f2.prev
            } else if f.l_first.next == l_f1 {
                l_f2.next
            } else {
                l_f2
            }
        };
    } else {
        // original first loop was in f2, further do same as above
        f2.l_first = f.l_first;

        f.l_first = {
            if f.l_first.prev == l_f2 {
                l_f1.prev
            } else if f.l_first.next == l_f2 {
                l_f1.next
            } else {
                l_f1
            }
        };
    }

    // validate both loop
    //
    // I don't know how many loops are supposed to be in each face at this point! FIXME

    // go through all of f2's loops and make sure they point to it properly
    {
        let l_first = f2.l_first;
        let mut l_iter = l_first;
        let mut f2len = 0;
        loop {
            l_iter.f = f2;
            f2len += 1;

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
        f2.len = f2len;
    }

    // link up the new loops into the new edges radial
    bmesh_radial_loop_append(e, l_f1);
    bmesh_radial_loop_append(e, l_f2);

    {
        let mut f1len = 0;
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            f1len += 1;

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
        f.len = f1len;
    }

    bm_check_element!(e);
    bm_check_element!(f);
    bm_check_element!(f2);

    return (f2, l_f2);
}

///
/// \brief Split Edge Make Vert (SEMV)
///
/// Takes \a e edge and splits it into two, creating a new vert.
/// \a tv should be one end of \a e : the newly created edge
/// will be attached to that end and is returned in \a r_e.
///
/// \par Examples:
///
/// ```text
/// Before: OV-------------TV
///             E       RE
/// After:  OV------NV-----TV
/// ```
///
/// \return The newly created BMVert pointer.
///
pub fn bmesh_kernel_split_edge_make_vert(
    bm: &mut BMesh,
    tv: BMVertMutP,
    mut e: BMEdgeMutP,
) -> (BMVertMutP, BMEdgeMutP) {
    debug_assert!(BM_vert_in_edge(e, tv));

    let v_old = BM_edge_other_vert_mut(e, tv);

    // debug only!
    let (valence1, valence2) = {
        if cfg!(debug_assertions) {
            (bmesh_disk_count(v_old), bmesh_disk_count(tv))
        } else {
            (0, 0)  // will get optimized out
        }
    };

    // order of 'e_new' verts should match 'e'
    // (so extruded faces don't flip)
    let v_new = BM_vert_create(bm, &tv.co, Some(tv.as_const()), BMElemCreate::NOP);
    let e_new = BM_edge_create(bm, &[tv, v_new], Some(e.as_const()), BMElemCreate::NOP);

    bmesh_disk_edge_remove(e_new, tv);
    bmesh_disk_edge_remove(e_new, v_new);

    bmesh_disk_vert_replace(e, v_new, tv);

    // add e_new to v_new's disk cycle
    bmesh_disk_edge_append(e_new, v_new);

    // add e_new to tv's disk cycle
    bmesh_disk_edge_append(e_new, tv);

    // verify disk cycles
    debug_assert!(bmesh_disk_validate(valence1, v_old.e, v_old));
    debug_assert!(bmesh_disk_validate(valence2, tv.e, tv));
    debug_assert!(bmesh_disk_validate(2, v_new.e, v_new));

    // Split the radial cycle if present
    let mut l_next = e.l;
    e.l = null_mut();
    if l_next != null() {
        let radlen = {
            if cfg!(debug_assertions) {
                bmesh_radial_length(l_next)
            } else {
                0  // will get optimized out
            }
        };

        let mut is_first = true;

        // Take the next loop. Remove it from radial. Split it. Append to appropriate radials
        while l_next != null() {
            let mut l = l_next;
            l.f.len += 1;
            l_next = if l_next != l_next.radial_next { l_next.radial_next } else { null_mut() };
            bmesh_radial_loop_unlink(l);

            let mut l_new = bm_loop_create(
                bm, null_mut(), null_mut(), l.f,
                Some(l.as_const()), BMElemCreate::NOP);
            l_new.prev = l;
            l_new.next = l.next;
            l_new.prev.next = l_new;
            l_new.next.prev = l_new;
            l_new.v = v_new;

            // NOTE: the block below can be de-duplicated, but not urgent since it works OK.

            // assign the correct edge to the correct loop
            if BM_verts_in_edge(l_new.v, l_new.next.v, e) {
                l_new.e = e;
                l.e = e_new;

                // append l into e_new's rad cycle
                if is_first == true {
                    is_first = false;
                    l.radial_next = null_mut();
                    l.radial_prev = null_mut();
                }

                bmesh_radial_loop_append(l_new.e, l_new);
                bmesh_radial_loop_append(l.e, l);
            } else if BM_verts_in_edge(l_new.v, l_new.next.v, e_new)
            {
                l_new.e = e_new;
                l.e = e;

                // append l into e_new's rad cycle
                if is_first == true {
                    is_first = false;
                    l.radial_next = null_mut();
                    l.radial_prev = null_mut();
                }

                bmesh_radial_loop_append(l_new.e, l_new);
                bmesh_radial_loop_append(l.e, l);
            }

        }

        if cfg!(debug_assertions) {
            // verify length of radial cycle
            debug_assert!(bmesh_radial_validate(radlen, e.l));
            debug_assert!(bmesh_radial_validate(radlen, e_new.l));

            // verify loop.v and loop.next.v pointers for e
            let mut l;
            l = e.l;
            for _ in 0..radlen {
                debug_assert!(l.e == e);
                // debug_assert!(l.radial_next == l);
                debug_assert!(!(l.prev.e != e_new && l.next.e != e_new));

                debug_assert!(BM_verts_in_edge(l.v, l.next.v, e));
                debug_assert!(l.v != l.next.v);
                debug_assert!(l.e != l.next.e);

                /* verify loop cycle for kloop.f */
                bm_check_element!(l);
                bm_check_element!(l.v);
                bm_check_element!(l.e);
                bm_check_element!(l.f);

                l = l.radial_next;
            }
            // verify loop.v and loop.next.v pointers for e_new
            l = e_new.l;
            for _ in 0..radlen {
                debug_assert!(l.e == e_new);
                // debug_assert!(l.radial_next == l);
                debug_assert!(!(l.prev.e != e && l.next.e != e));
                debug_assert!(BM_verts_in_edge(l.v, l.next.v, e_new));
                debug_assert!(l.v != l.next.v);
                debug_assert!(l.e != l.next.e);

                bm_check_element!(l);
                bm_check_element!(l.v);
                bm_check_element!(l.e);
                bm_check_element!(l.f);

                l = l.radial_next;
            }
        }
        // debug_assertions
    }

    bm_check_element!(e_new);
    bm_check_element!(v_new);
    bm_check_element!(v_old);
    bm_check_element!(e);
    bm_check_element!(tv);

    return (v_new, e_new);
}

///
/// \brief Join Edge Kill Vert (JEKV)
///
/// Takes an edge \a e_kill and pointer to one of its vertices \a v_kill
/// and collapses the edge on that vertex.
///
/// \par Examples:
///
/// ```text
///
/// Before:    e_old  e_kill
///          +-------+-------+
///          |       |       |
///          v_old   v_kill  v_target
///
/// After:           e_old
///          +---------------+
///          |               |
///          v_old           v_target
/// ```
///
/// \par Restrictions:
/// KV is a vertex that must have a valance of exactly two. Furthermore
/// both edges in KV's disk cycle (OE and KE) must be unique (no double edges).
///
/// \return The resulting edge, NULL for failure.
///
/// \note This euler has the possibility of creating
/// faces with just 2 edges. It is up to the caller to decide what to do with
/// these faces.
///
pub fn bmesh_kernel_join_edge_kill_vert(
    bm: &mut BMesh,
    e_kill: BMEdgeMutP,
    mut v_kill: BMVertMutP,
    do_del: bool,
    check_edge_double: bool,
    kill_degenerate_faces: bool,
) -> BMEdgeMutP {
    debug_assert!(BM_vert_in_edge(e_kill, v_kill));

    if BM_vert_in_edge(e_kill, v_kill) == false {
        return null_mut();
    }

    if bmesh_disk_count_ex(v_kill, 3) == 2 {
        let e_old = bmesh_disk_edge_next_mut(e_kill, v_kill);
        let v_target = BM_edge_other_vert_mut(e_kill, v_kill);
        let v_old = BM_edge_other_vert_mut(e_old, v_kill);

        // check for double edges
        if BM_verts_in_edge(v_kill, v_target, e_old) {
            return null_mut();
        } else {
            // could make Option<Vec>, only needed when degenerate faces as used.
            let mut faces_degenerate: Vec<BMFaceMutP> = Vec::with_capacity(0);

            // For verification later, count valence of 'v_old' and 'v_target'
            let (valence1, valence2) = {
                if cfg!(debug_assertions) {
                    (bmesh_disk_count(v_old), bmesh_disk_count(v_target))
                } else {
                    (0, 0)
                }
            };

            let e_splice = {
                if check_edge_double {
                    BM_edge_exists_mut(v_target, v_old)
                } else {
                    null_mut()
                }
            };


            bmesh_disk_vert_replace(e_old, v_target, v_kill);

            // remove e_kill from 'v_target's disk cycle
            bmesh_disk_edge_remove(e_kill, v_target);

            // deal with radial cycle of e_kill
            let radlen = {
                if cfg!(debug_assertions) {
                    bmesh_radial_length(e_kill.l)
                } else {
                    0
                }
            };

            if e_kill.l != null() {
                // fix the neighboring loops of all loops in e_kill's radial cycle
                let mut l_kill = e_kill.l;
                loop {
                    // relink loops and fix vertex pointer
                    if l_kill.next.v == v_kill {
                        l_kill.next.v = v_target;
                    }

                    l_kill.next.prev = l_kill.prev;
                    l_kill.prev.next = l_kill.next;
                    if l_kill.f.l_first == l_kill {
                        l_kill.f.l_first = l_kill.next;
                    }

                    // fix len attribute of face
                    l_kill.f.len -= 1;
                    if kill_degenerate_faces {
                        if l_kill.f.len < 3 {
                            faces_degenerate.push(l_kill.f);
                        }
                    }

                    let l_kill_next = l_kill.radial_next;

                    bm_loop_kill_only(bm, l_kill);

                    l_kill = l_kill_next;
                    if l_kill == e_kill.l {
                        break;
                    }
                }
                // `e_kill.l` is invalid but the edge is freed next.

                // Validate radial cycle of e_old
                debug_assert!(bmesh_radial_validate(radlen, e_old.l));
            }
            // deallocate edge
            bm_edge_kill_only(bm, e_kill);

            // deallocate vertex
            if do_del {
                bm_vert_kill_only(bm, v_kill);
            } else {
                v_kill.e = null_mut();
            }

            if cfg!(debug_assertions) {
                // Validate disk cycle lengths of 'v_old', 'v_target' are unchanged
                debug_assert!(bmesh_disk_validate(valence1, v_old.e, v_old));
                debug_assert!(bmesh_disk_validate(valence2, v_target.e, v_target));

                // Validate loop cycle of all faces attached to 'e_old'
                let mut l = e_old.l;
                for _ in 0..radlen {
                    debug_assert!(l.e == e_old);
                    debug_assert!(BM_verts_in_edge(l.v, l.next.v, e_old));
                    debug_assert!(bmesh_loop_validate(l.f));

                    bm_check_element!(l);
                    bm_check_element!(l.v);
                    bm_check_element!(l.e);
                    bm_check_element!(l.f);
                    l = l.radial_next;
                }
            }

            if check_edge_double {
                if e_splice != null() {
                    // removes e_splice
                    BM_edge_splice(bm, e_old, e_splice);
                }
            }

            if kill_degenerate_faces {
                for f_kill in faces_degenerate {
                    BM_face_kill(bm, f_kill);
                }
            }

            bm_check_element!(v_old);
            bm_check_element!(v_target);
            bm_check_element!(e_old);

            return e_old;
        }
    }
    return null_mut();
}


///
/// \brief Join Vert Kill Edge (JVKE)
///
/// Collapse an edge, merging surrounding data.
///
/// Unlike #BM_vert_collapse_edge & #bmesh_kernel_join_edge_kill_vert which only handle 2 valence verts,
/// this can handle any number of connected edges/faces.
///
/// ```text
///
/// Before: -> After:
/// +-+-+-+    +-+-+-+
/// | | | |    | \ / |
/// +-+-+-+    +--+--+
/// | | | |    | / \ |
/// +-+-+-+    +-+-+-+
/// ```
///

#[allow(dead_code)]
pub fn bmesh_kernel_join_vert_kill_edge(
    bm: &mut BMesh,
    e_kill: BMEdgeMutP, v_kill: BMVertMutP,
    do_del: bool,
    check_edge_double: bool,
    kill_degenerate_faces: bool,
) -> BMVertMutP
{
    // typically will be only 2
    let mut faces_degenerate = Vec::with_capacity(4);
    let v_target = BM_edge_other_vert_mut(e_kill, v_kill);

    debug_assert!(BM_vert_in_edge(e_kill, v_kill));

    if e_kill.l != null() {
        // l_kill = l_first = e_kill.l;

        let l_first = e_kill.l;
        let mut l_kill = l_first;
        loop {
            // relink loops and fix vertex pointer
            if l_kill.next.v == v_kill {
                l_kill.next.v = v_target;
            }

            l_kill.next.prev = l_kill.prev;
            l_kill.prev.next = l_kill.next;
            if l_kill.f.l_first == l_kill {
                l_kill.f.l_first = l_kill.next;
            }

            // fix len attribute of face
            l_kill.f.len -= 1;
            if kill_degenerate_faces {
                if l_kill.f.len < 3 {
                    faces_degenerate.push(l_kill.f);
                }
            }

            let l_kill_next = l_kill.radial_next;

            bm_loop_kill_only(bm, l_kill);

            l_kill = l_kill_next;
            if l_kill == l_first {
                break;
            }
        }

        e_kill.clone().l = null_mut();
    }

    BM_edge_kill(bm, e_kill);
    bm_check_element!(v_kill);
    bm_check_element!(v_target);

    if v_target.e != null() &&
       v_kill.e != null()
    {
        // inline BM_vert_splice(bm, v_target, v_kill);
        while v_kill.e != null() {
            let e = v_kill.e;

            let e_target = {
                if check_edge_double {
                    BM_edge_exists_mut(v_target, BM_edge_other_vert_mut(e, v_kill))
                } else {
                    null_mut()  // unused
                }
            };

            bmesh_edge_vert_swap(e, v_target, v_kill);
            debug_assert!(e.verts[0] != e.verts[1]);

            if check_edge_double {
                if e_target != null() {
                    BM_edge_splice(bm, e_target, e);
                }
            }
        }
    }

    if kill_degenerate_faces {
        for f_kill in faces_degenerate {
            BM_face_kill(bm, f_kill);
        }
    }

    if do_del {
        debug_assert!(v_kill.e == null());
        bm_vert_kill_only(bm, v_kill);
    }

    return v_target;
}

// UNUSED
/*
pub fn bmesh_kernel_join_face_kill_edge() {}
*/

#[allow(dead_code)]
pub fn BM_vert_splice_check_double<V>(v_a: V, v_b: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v_a, v_b);

    let mut is_double: bool = false;

    debug_assert!(BM_edge_exists(v_a, v_b) == null());

    if v_a.e != null() && v_b.e != null() {

        use bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_WALK;
        const VERT_VISIT: BMElemApiFlag = FLAG_WALK;

        /* tag 'v_a' */
        {
            let e_first = v_a.e.as_const();
            let mut e = e_first;
            loop {
                let v_other = BM_edge_other_vert(e, v_a);
                debug_assert!(!bm_elem_api_flag_test!(v_other, VERT_VISIT));
                unsafe {
                    bm_elem_api_flag_enable!(v_other.as_mut(), VERT_VISIT);
                }
                e = bmesh_disk_edge_next(e, v_a);
                if e == e_first {
                    break;
                }
            }
        }

        /* check 'v_b' connects to 'v_a' edges */
        {
            let e_first = v_b.e.as_const();
            let mut e = e_first;
            loop {
                let v_other = BM_edge_other_vert(e, v_b);
                if bm_elem_api_flag_test!(v_other, VERT_VISIT) {
                    is_double = true;
                    break;
                }
                e = bmesh_disk_edge_next(e, v_a);
                if e == e_first {
                    break;
                }
            }
        }

        /* cleanup */
        {
            let e_first = v_a.e.as_const();
            let mut e = e_first;
            loop {
                let v_other = BM_edge_other_vert(e, v_a);
                debug_assert!(bm_elem_api_flag_test!(v_other, VERT_VISIT));
                unsafe {
                    bm_elem_api_flag_disable!(v_other.as_mut(), VERT_VISIT);
                }
                e = bmesh_disk_edge_next(e, v_a);
                if e == e_first {
                    break;
                }
            }
        }
    }

    return is_double;
}

///
/// \brief Splice Vert
///
/// Merges two verts into one
/// (\a v_src into \a v_dst, removing \a v_src).
///
/// \return Success
///
/// \warning This doesn't work for collapsing edges,
/// where \a v and \a vtarget are connected by an edge
/// (assert checks for this case).
///
pub fn BM_vert_splice(
    bm: &mut BMesh,
    v_dst: BMVertMutP,
    v_src: BMVertMutP,
) -> bool {
    // verts already spliced
    if v_src == v_dst {
        return false;
    }

    debug_assert!(BM_vert_pair_share_face_check(v_src, v_dst) == false);

    // move all the edges from 'v_src' disk to 'v_dst'
    while v_src.e != null() {
        let e = v_src.e;
        bmesh_edge_vert_swap(e, v_dst, v_src);
        debug_assert!(e.verts[0] != e.verts[1]);
    }

    bm_check_element!(v_src);
    bm_check_element!(v_dst);

    // 'v_src' is unused now, and can be killed
    BM_vert_kill(bm, v_src);

    return true;
}

#[allow(dead_code)]
fn bm_edge_supports_separate<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    return (e.l != null()) && (e.l.radial_next != e.l);
}

///
/// \brief Separate Vert
///
/// Separates all disjoint fans that meet at a vertex, making a unique
/// vertex for each region. returns an array of all resulting vertices.
///
/// \note this is a low level function, bm_edge_separate needs to run on edges first
/// or, the faces sharing verts must not be sharing edges for them to split at least.
///
/// \return Success
///
#[allow(dead_code)]
pub fn bmesh_kernel_vert_separate(
    bm: &mut BMesh,
    v: BMVertMutP,
    copy_select: bool,
) -> Vec<BMVertMutP> {
    // BMVert ***r_vout, int *r_vout_len

    // Detailed notes on array use since this is stack memory, we have to be careful

    // newly created vertices, only use when 'r_vout' is set
    // (total size will be number of fans)
    let mut verts_new: Vec<BMVertMutP> = Vec::with_capacity(BM_DEFAULT_DISK_STACK_SIZE);
    // initialize with first
    verts_new.push(v);
    // fill with edges from the face-fan, clearing on completion
    // (total size will be max fan edge count)
    let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(BM_DEFAULT_DISK_STACK_SIZE);
    // temp store edges to walk over when filling 'edges',
    // (total size will be max radial edges of any edge)
    let mut edges_search: Vec<BMEdgeMutP> = Vec::with_capacity(BM_DEFAULT_RADIAL_STACK_SIZE);

    // track the total number of edges handled, so we know when we've found the last fan
    let mut edges_found = 0;

    use bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_WALK;
    const EDGE_VISIT: BMElemApiFlag = FLAG_WALK;

    let mut v_edges_num = 0;

    // count and flag at once
    if v.e != null() {
        let e_first = v.e;
        let mut e_iter = v.e;
        loop {
            v_edges_num += 1;

            debug_assert!(!bm_elem_api_flag_test!(e_iter, EDGE_VISIT));
            bm_elem_api_flag_enable!(e_iter, EDGE_VISIT);

            e_iter = bmesh_disk_edge_next_mut(e_iter, v);
            if e_iter == e_first {
                break;
            }
        }
    }

    loop {
        // Considering only edges and faces incident on vertex v, walk
        // the edges & collect in the 'edges' list for splitting

        let mut e = v.e;
        bm_elem_api_flag_disable!(e, EDGE_VISIT);

        loop {
            debug_assert!(!bm_elem_api_flag_test!(e, EDGE_VISIT));
            edges.push(e);
            edges_found += 1;

            if e.l != null() {
                let l_first = e.l;
                let mut l_iter = l_first;
                loop {
                    let mut l_adjacent = {
                        if l_iter.v == v {
                            l_iter.prev
                        } else {
                            l_iter.next
                        }
                    };
                    debug_assert!(BM_vert_in_edge(l_adjacent.e, v));
                    if bm_elem_api_flag_test!(l_adjacent.e, EDGE_VISIT) {
                        bm_elem_api_flag_disable!(l_adjacent.e, EDGE_VISIT);
                        edges_search.push(l_adjacent.e);
                    }

                    l_iter = l_iter.radial_next;
                    if l_iter == l_first {
                        break;
                    }
                }
            }
            if edges_search.is_empty() {
                break;
            }
            e = edges_search.pop().unwrap();
        }

        // now we have all edges connected to 'v.e'

        debug_assert!(edges_found <= v_edges_num);

        if edges_found == v_edges_num {
            // We're done! The remaining edges in 'edges' form the last fan,
            // which can be left as is.
            // if 'edges' were alloc'd it'd be freed here.
            break;
        } else {
            let mut v_new = BM_vert_create(bm, &v.co, Some(v.as_const()), BMElemCreate::NOP);
            if copy_select {
                BM_elem_select_copy(bm, v_new.as_elem_mut(), v.as_elem());
            }

            while let Some(e) = edges.pop() {
                bmesh_edge_vert_swap(e, v_new, v);
            }

            verts_new.push(v_new);
        }
    }

    return verts_new;
}
///
/// Utility function for #BM_vert_separate
///
/// Takes a list of edges, which have been split from their original.
///
/// Any edges which failed to split off in #bmesh_kernel_vert_separate
/// will be merged back into the original edge.
///
/// \param edges_separate
/// A list-of-lists, each list is from a single original edge (the first edge is the original),
/// Check for duplicates (not just with the first) but between all.
/// This is O(n2) but radial edges are very rarely >2 and almost never >~10.
///
/// \note typically its best to avoid creating the data in the first place,
/// but inspecting all loops connectivity is quite involved.
///
/// \note this function looks like it could become slow,
/// but in common cases its only going to iterate a few times.
///
fn bmesh_kernel_vert_separate__cleanup(bm: &mut BMesh, edges_separate_group: Vec<Vec<BMEdgeMutP>>) {
    for mut edges_separate in edges_separate_group {
        let mut i_orig: usize = 0;
        while i_orig + 1 < edges_separate.len() {
            let e_orig = edges_separate[i_orig];
            let mut i_step: usize = i_orig + 1;
            while i_step < edges_separate.len() {
                let e = edges_separate[i_step];
                if (e.verts[0] == e_orig.verts[0]) &&
                   (e.verts[1] == e_orig.verts[1])
                {
                    BM_edge_splice(bm, e_orig, e);
                    // don't visit again
                    edges_separate.swap_remove(i_step);
                }
                else {
                    i_step += 1;
                }
            }
            i_orig += 1;
        }
    }
}

pub fn BM_vert_separate(
    bm: &mut BMesh, v: BMVertMutP,
    e_in: &[BMEdgeMutP],
    copy_select: bool,
) -> Vec<BMVertMutP> {
    let mut edges_separate = Vec::new();

    for &e in e_in {
        if bm_edge_supports_separate(e) {
            let mut edges_orig = Vec::new();
            loop {
                let l_sep = e.l;
                bmesh_kernel_edge_separate(bm, e, l_sep, copy_select);
                edges_orig.push(l_sep.e);
                debug_assert!(e != l_sep.e);

                if bm_edge_supports_separate(e) == false {
                    break;
                }
            }
            edges_orig.push(e);
            edges_separate.push(edges_orig);
        }
    }

    let v_out = bmesh_kernel_vert_separate(bm, v, copy_select);

    if !edges_separate.is_empty() {
        bmesh_kernel_vert_separate__cleanup(bm, edges_separate);
    }

    return v_out;
}
#[allow(dead_code)]
pub fn BM_vert_separate_hflag( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_separate_wire_hflag( /* STUB */ ) {}

///
/// \brief Splice Edge
///
/// Splice two unique edges which share the same two vertices into one edge.
///  (\a e_src into \a e_dst, removing e_src).
///
/// \return Success
///
/// \note Edges must already have the same vertices.
///
pub fn BM_edge_splice(
    bm: &mut BMesh,
    e_dst: BMEdgeMutP,
    e_src: BMEdgeMutP,
) -> bool {
    if !BM_vert_in_edge(e_src, e_dst.verts[0]) ||
       !BM_vert_in_edge(e_src, e_dst.verts[1])
    {
        // not the same vertices can't splice

        // the caller should really make sure this doesn't happen ever
        // so assert on release builds
        debug_assert!(false);

        return false;
    }

    while e_src.l != null() {
        let l = e_src.l;
        debug_assert!(BM_vert_in_edge(e_dst, l.v));
        debug_assert!(BM_vert_in_edge(e_dst, l.next.v));
        bmesh_radial_loop_remove(e_src, l);
        bmesh_radial_loop_append(e_dst, l);
    }

    debug_assert!(bmesh_radial_length(e_src.l) == 0);

    bm_check_element!(e_src);
    bm_check_element!(e_dst);

    // removes from disks too
    BM_edge_kill(bm, e_src);

    return true;
}

///
/// \brief Separate Edge
///
/// Separates a single edge into two edge: the original edge and
/// a new edge that has only \a l_sep in its radial.
///
/// \return Success
///
/// \note Does nothing if \a l_sep is already the only loop in the
/// edge radial.
///
pub fn bmesh_kernel_edge_separate(
    bm: &mut BMesh, mut e: BMEdgeMutP, mut l_sep: BMLoopMutP,
    copy_select: bool,
) {
    let radlen = {
        if cfg!(debug_assertions) {
            bmesh_radial_length(e.l)
        } else {
            0
        }
    };

    debug_assert!(l_sep.e == e);
    debug_assert!(e.l != null());

    if BM_edge_is_boundary(e) {
        debug_assert!(false); // no cut required
        return;
    }

    if l_sep == e.l {
        e.l = l_sep.radial_next;
    }

    let mut e_new = BM_edge_create(bm, &e.verts, Some(e.as_const()), BMElemCreate::NOP);
    bmesh_radial_loop_remove(e, l_sep);
    bmesh_radial_loop_append(e_new, l_sep);
    l_sep.e = e_new;

    if copy_select {
        BM_elem_select_copy(bm, e_new.as_elem_mut(), e.as_elem());
    }

    debug_assert!(bmesh_radial_length(e.l) == radlen - 1);
    debug_assert!(bmesh_radial_length(e_new.l) == 1);

    bm_check_element!(e_new);
    bm_check_element!(e);
}

///
/// \brief Un-glue Region Make Vert (URMV)
///
/// Disconnects a face from its vertex fan at loop \a l_sep
///
/// \return The newly created BMVert
///
/// \note Will be a no-op and return original vertex if only two edges at that vertex.
///
pub fn bmesh_kernel_unglue_region_make_vert(
    bm: &mut BMesh,
    l_sep: BMLoopMutP,
) -> BMVertMutP {

    let mut v_sep = l_sep.v;

    // peel the face from the edge radials on both sides of the
    // loop vert, disconnecting the face from its fan
    if !BM_edge_is_boundary(l_sep.e) {
        bmesh_kernel_edge_separate(bm, l_sep.e, l_sep, false);
    }
    if !BM_edge_is_boundary(l_sep.prev.e) {
        bmesh_kernel_edge_separate(bm, l_sep.prev.e, l_sep.prev, false);
    }

    // do inline, below
    if false {
        if BM_vert_edge_count_is_equal!(v_sep, 2) {
            return v_sep;
        }
    } else {
        // Search for an edge unattached to this loop
        let mut e_iter = v_sep.e.as_const();
        while !elem!(e_iter, l_sep.e, l_sep.prev.e) {
            e_iter = bmesh_disk_edge_next(e_iter, v_sep);

            // We've come back around to the initial edge, all touch this loop.
            // If there are still only two edges out of v_sep,
            // then this whole URMV was just a no-op, so exit now.
            if e_iter == v_sep.e {
                debug_assert!(BM_vert_edge_count_is_equal!(v_sep, 2));
                return v_sep;
            }
        }
    }

    v_sep.e = l_sep.e;

    let v_new = BM_vert_create(bm, &v_sep.co, Some(v_sep.as_const()), BMElemCreate::NOP);

    let edges = [l_sep.e, l_sep.prev.e];
    for e in &edges {
        bmesh_edge_vert_swap(*e, v_new, v_sep);
    }

    debug_assert!(v_sep != l_sep.v);
    debug_assert!(v_sep.e != l_sep.v.e);

    bm_check_element!(l_sep);
    bm_check_element!(v_sep);
    bm_check_element!(edges[0]);
    bm_check_element!(edges[1]);
    bm_check_element!(v_new);

    return v_new;
}

///
/// A version of #bmesh_kernel_unglue_region_make_vert that disconnects multiple loops at once.
/// The loops must all share the same vertex, can be in any order
/// and are all moved to use a single new vertex - which is returned.
///
/// This function handles the details of finding fans boundaries.
///
pub fn bmesh_kernel_unglue_region_make_vert_multi(bm: &mut BMesh, larr: &[BMLoopMutP]) -> BMVertMutP {

    let v_sep = larr[0].v;
    /* any edges not owned by 'larr' loops connected to 'v_sep'? */
    let mut is_mixed_edge_any = false;
    /* any loops not owned by 'larr' radially connected to 'larr' loop edges? */
    let mut is_mixed_loop_any = false;

    use bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_WALK;
    const LOOP_VISIT: BMElemApiFlag = FLAG_WALK;
    const EDGE_VISIT: BMElemApiFlag = FLAG_WALK;

    let mut edges_len_prealloc: usize = 0;

    for l_sep in larr {
        let mut l_sep = *l_sep;  // remove?

        // all must be from the same vert!
        debug_assert!(v_sep == l_sep.v);

        debug_assert!(!bm_elem_api_flag_test!(l_sep, LOOP_VISIT));
        bm_elem_api_flag_enable!(l_sep, LOOP_VISIT);

        // weak! but it makes it simpler to check for edges to split
        // while doing a radial loop (where loops may be adjacent)
        bm_elem_api_flag_enable!(l_sep.next, LOOP_VISIT);
        bm_elem_api_flag_enable!(l_sep.prev, LOOP_VISIT);

        for &mut mut e in &mut [l_sep.e, l_sep.prev.e] {
            if !bm_elem_api_flag_test!(e, EDGE_VISIT) {
                bm_elem_api_flag_enable!(e, EDGE_VISIT);
                edges_len_prealloc += 1;
            }
        }
    }

    let mut edges: Vec<BMEdgeMutP> = Vec::with_capacity(edges_len_prealloc);


    bm_iter_edges_of_vert_mut!(v_sep, mut e_iter, {
        if bm_elem_api_flag_test!(e_iter, EDGE_VISIT) {
            let mut is_mixed_loop = false;
            let l_first = e_iter.l;
            let mut l_iter = l_first;
            loop {
                if !bm_elem_api_flag_test!(l_iter, LOOP_VISIT) {
                    is_mixed_loop = true;
                    break;
                }
                l_iter = l_iter.radial_next;
                if l_iter == l_first {
                    break;
                }
            }

            if is_mixed_loop {
                // ensure the first loop is one we don't own so we can do a quick check below
                // on the edge's loop-flag to see if the edge is mixed or not.
                e_iter.l = l_iter;

                is_mixed_loop_any = true;
            }
            edges.push(e_iter);
            //
        } else {
            /* at least one edge attached isn't connected to our loops */
            is_mixed_edge_any = true;
        }
    });

    debug_assert_eq!(edges.len(), edges_len_prealloc);

    let v_new: BMVertMutP;

    if is_mixed_loop_any == false && is_mixed_edge_any == false {
        // all loops in 'larr' are the sole owners of their edges.
        // nothing to split away from, this is a no-op
        v_new = v_sep;
    } else {
        debug_assert!(!edges.is_empty());

        v_new = BM_vert_create(bm, &v_sep.co, Some(v_sep.as_const()), BMElemCreate::NOP);

        for mut e in edges {
            // disable so copied edge isn't left dirty (loop edges are cleared last too)
            bm_elem_api_flag_disable!(e, EDGE_VISIT);

            /* will always be false when (is_mixed_loop_any == false) */
            if !bm_elem_api_flag_test!(e.l, LOOP_VISIT) {
                // edge has some loops owned by us, some owned by other loops
                let e_new_v_pair = {
                    if e.verts[0] == v_sep {
                        [v_new, e.verts[1]]
                    } else {
                        debug_assert!(v_sep == e.verts[1]);
                        [e.verts[0], v_new]
                    }
                };

                let e_new = BM_edge_create(bm, &e_new_v_pair,
                                           Some(e.as_const()), BMElemCreate::NOP);

                // now moved all loops from 'larr' to this newly created edge
                let l_first = e.l;
                let mut l_iter = l_first;
                loop {
                    let l_next = l_iter.radial_next;
                    if bm_elem_api_flag_test!(l_iter, LOOP_VISIT) {
                        bmesh_radial_loop_remove(e, l_iter);
                        bmesh_radial_loop_append(e_new, l_iter);
                        l_iter.e = e_new;
                    }

                    l_iter = l_next;
                    if l_iter == l_first {
                        break;
                    }
                }
            } else {
                // we own the edge entirely, replace the vert
                bmesh_disk_vert_replace(e, v_new, v_sep);
            }

            // loop vert is handled last!
        }
    }

    for l_sep in larr {
        let mut l_sep = *l_sep; // remove?

        l_sep.v = v_new;

        debug_assert!(bm_elem_api_flag_test!(l_sep, LOOP_VISIT));
        debug_assert!(bm_elem_api_flag_test!(l_sep.prev, LOOP_VISIT));
        debug_assert!(bm_elem_api_flag_test!(l_sep.next, LOOP_VISIT));
        bm_elem_api_flag_disable!(l_sep, LOOP_VISIT);
        bm_elem_api_flag_disable!(l_sep.prev, LOOP_VISIT);
        bm_elem_api_flag_disable!(l_sep.next, LOOP_VISIT);

        bm_elem_api_flag_disable!(l_sep.prev.e, EDGE_VISIT);
        bm_elem_api_flag_disable!(l_sep.e, EDGE_VISIT);
    }

    return v_new;
}

#[allow(dead_code)]
fn bmesh_edge_vert_swap__recursive(e: BMEdgeMutP, v_dst: BMVertMutP, v_src: BMVertMutP) {

    debug_assert!(elem!(v_src, e.verts[0], e.verts[1]));
    bmesh_disk_vert_replace(e, v_dst, v_src);

    let l_first = e.l;
    let mut l_iter = l_first;
    loop {
        if l_iter.v == v_src {
            l_iter.v = v_dst;
            if BM_vert_in_edge(l_iter.prev.e, v_src) {
                bmesh_edge_vert_swap__recursive(l_iter.prev.e, v_dst, v_src);
            }
        } else if l_iter.next.v == v_src {
            l_iter.next.v = v_dst;
            if BM_vert_in_edge(l_iter.next.e, v_src) {
                bmesh_edge_vert_swap__recursive(l_iter.next.e, v_dst, v_src);
            }
        } else {
            debug_assert!(l_iter.prev.v != v_src);
        }

        l_iter = l_iter.radial_next;
        if l_iter == l_first {
            break;
        }
    }
}

#[allow(dead_code)]
pub fn bmesh_kernel_unglue_region_make_vert_multi_isolated(
    bm: &mut BMesh, l_sep: BMLoopMutP,
) -> BMVertMutP {
    let v_new = BM_vert_create(bm, &l_sep.v.co, Some(l_sep.v.as_const()), BMElemCreate::NOP);
    // passing either 'l_sep.e', 'l_sep.prev.e' will work
    bmesh_edge_vert_swap__recursive(l_sep.e, v_new, l_sep.v);
    debug_assert!(l_sep.v == v_new);
    return v_new;
}

pub fn bmesh_face_swap_data(mut f_a: BMFaceMutP, mut f_b: BMFaceMutP) {
    debug_assert!(f_a != f_b);

    let l_first = f_a.l_first;
    let mut l_iter = l_first;
    loop {
        l_iter.f = f_b;
        l_iter = l_iter.next;
        if l_iter == l_first {
            break;
        }
    }

    let l_first = f_b.l_first;
    let mut l_iter = l_first;
    loop {
        l_iter.f = f_a;
        l_iter = l_iter.next;
        if l_iter == l_first {
            break;
        }
    }

    use ::std::mem::swap;
    swap(&mut (*f_a), &mut (*f_b));

    /* swap back */
    swap(&mut f_a.head.data, &mut f_b.head.data);
    swap(&mut f_a.head.index, &mut f_b.head.index);
}


// ----------------------------------------------------------------------------
// Element Remove API

// -----------
// Remove Only
//
// Simply remove, don't update topology (private).

fn bm_vert_kill_only(bm: &mut BMesh, v: BMVertMutP) {
    debug_assert!(v.head.htype == BM_VERT);
    bm.elem_index_dirty |= BM_VERT;
    bm.elem_table_dirty |= BM_VERT;
    // TODO: free v.head.data
    bm.verts.free_elem(v);
}
fn bm_edge_kill_only(bm: &mut BMesh, e: BMEdgeMutP) {
    debug_assert!(e.head.htype == BM_EDGE);
    bm.elem_index_dirty |= BM_EDGE;
    bm.elem_table_dirty |= BM_EDGE;
    // TODO: free v.head.data
    bm.edges.free_elem(e)
}
fn bm_loop_kill_only(bm: &mut BMesh, l: BMLoopMutP) {
    debug_assert!(l.head.htype == BM_LOOP);
    bm.elem_index_dirty |= BM_LOOP;
    bm.elem_table_dirty |= BM_LOOP;
    // TODO: free v.head.data
    bm.loops.free_elem(l)
}
fn bm_face_kill_only(bm: &mut BMesh, f: BMFaceMutP) {
    debug_assert!(f.head.htype == BM_FACE);
    bm.elem_index_dirty |= BM_FACE;
    bm.elem_table_dirty |= BM_FACE;
    // TODO: free v.head.data
    bm.faces.free_elem(f)
}

// ------
// Remove
//
// Topology aware remove (public).

// pub fn BM_loop_kill(bm: &mut BMesh, v: BMEdgeConstP) {
// }

///
/// Check the element is valid.
///
/// BMESH_TODO, when this raises an error the output is incredibly confusing.
/// need to have some nice way to print/debug what the heck's going on.
///

pub mod bm_elem_check_flags {
    struct_bitflag_impl!(pub struct BMElemCheckFlag(pub u32));

    pub const IS_NULL: BMElemCheckFlag                                = BMElemCheckFlag(1 << 0);
    pub const IS_WRONG_TYPE: BMElemCheckFlag                          = BMElemCheckFlag(1 << 1);

    pub const IS_VERT_WRONG_EDGE_TYPE: BMElemCheckFlag                = BMElemCheckFlag(1 << 2);

    pub const IS_EDGE_NULL_DISK_LINK: BMElemCheckFlag                 = BMElemCheckFlag(1 << 3);
    pub const IS_EDGE_WRONG_LOOP_TYPE: BMElemCheckFlag                = BMElemCheckFlag(1 << 4);
    pub const IS_EDGE_WRONG_FACE_TYPE: BMElemCheckFlag                = BMElemCheckFlag(1 << 5);
    pub const IS_EDGE_NULL_RADIAL_LINK: BMElemCheckFlag               = BMElemCheckFlag(1 << 6);
    pub const IS_EDGE_ZERO_FACE_LENGTH: BMElemCheckFlag               = BMElemCheckFlag(1 << 7);

    pub const IS_LOOP_WRONG_FACE_TYPE: BMElemCheckFlag                = BMElemCheckFlag(1 << 8);
    pub const IS_LOOP_WRONG_EDGE_TYPE: BMElemCheckFlag                = BMElemCheckFlag(1 << 9);
    pub const IS_LOOP_WRONG_VERT_TYPE: BMElemCheckFlag                = BMElemCheckFlag(1 << 10);
    pub const IS_LOOP_VERT_NOT_IN_EDGE: BMElemCheckFlag               = BMElemCheckFlag(1 << 11);
    pub const IS_LOOP_NULL_CYCLE_LINK: BMElemCheckFlag                = BMElemCheckFlag(1 << 12);
    pub const IS_LOOP_ZERO_FACE_LENGTH: BMElemCheckFlag               = BMElemCheckFlag(1 << 13);
    pub const IS_LOOP_WRONG_FACE_LENGTH: BMElemCheckFlag              = BMElemCheckFlag(1 << 14);
    pub const IS_LOOP_WRONG_RADIAL_LENGTH: BMElemCheckFlag            = BMElemCheckFlag(1 << 15);

    pub const IS_FACE_NULL_LOOP: BMElemCheckFlag                      = BMElemCheckFlag(1 << 16);
    pub const IS_FACE_WRONG_LOOP_FACE: BMElemCheckFlag                = BMElemCheckFlag(1 << 17);
    pub const IS_FACE_NULL_EDGE: BMElemCheckFlag                      = BMElemCheckFlag(1 << 18);
    pub const IS_FACE_NULL_VERT: BMElemCheckFlag                      = BMElemCheckFlag(1 << 19);
    pub const IS_FACE_LOOP_VERT_NOT_IN_EDGE: BMElemCheckFlag          = BMElemCheckFlag(1 << 20);
    pub const IS_FACE_LOOP_WRONG_RADIAL_LENGTH: BMElemCheckFlag       = BMElemCheckFlag(1 << 21);
    pub const IS_FACE_LOOP_WRONG_DISK_LENGTH: BMElemCheckFlag         = BMElemCheckFlag(1 << 22);
    pub const IS_FACE_LOOP_DUPE_LOOP: BMElemCheckFlag                 = BMElemCheckFlag(1 << 23);
    pub const IS_FACE_LOOP_DUPE_VERT: BMElemCheckFlag                 = BMElemCheckFlag(1 << 24);
    pub const IS_FACE_LOOP_DUPE_EDGE: BMElemCheckFlag                 = BMElemCheckFlag(1 << 25);
    pub const IS_FACE_WRONG_LENGTH: BMElemCheckFlag                   = BMElemCheckFlag(1 << 26);
}

pub fn bmesh_elem_check(
    element: BMElemConstP,
    htype: BMElemType,
) -> bm_elem_check_flags::BMElemCheckFlag {
    use self::bm_elem_check_flags::*;

    use intern::bmesh_queries::*;
    use intern::bmesh_structure::*;

    let mut err: BMElemCheckFlag = BMElemCheckFlag(0);

    if element == null() {
        return IS_NULL;
    }

    if element.head.htype != htype {
        return IS_WRONG_TYPE;
    }

    match htype {
        BM_VERT => {
            let v: BMVertConstP = PtrConst(element.as_ptr() as *mut BMVert);
            if v.e != null() && (v.e.head.htype != BM_EDGE) {
                err |= IS_VERT_WRONG_EDGE_TYPE;
            }
        }

        BM_EDGE => {
            let e: BMEdgeConstP = PtrConst(element.as_ptr() as *mut BMEdge);
            if e.disk_links[0].prev == null() ||
               e.disk_links[1].prev == null() ||
               e.disk_links[0].next == null() ||
               e.disk_links[1].next == null()
            {
                err |= IS_EDGE_NULL_DISK_LINK;
            }

            if e.l != null() && (e.l.head.htype != BM_LOOP) {
                err |= IS_EDGE_WRONG_LOOP_TYPE;
            }
            if e.l != null() && (e.l.f.head.htype != BM_FACE) {
                err |= IS_EDGE_WRONG_FACE_TYPE;
            }
            if e.l != null() &&
               (e.l.radial_next == null() || e.l.radial_prev == null())
            {
                err |= IS_EDGE_NULL_RADIAL_LINK;
            }
            if e.l != null() && (e.l.f.len <= 0) {
                err |= IS_EDGE_ZERO_FACE_LENGTH;
            }
        }

        BM_LOOP => {
            let l: BMLoopConstP = PtrConst(element.as_ptr() as *mut BMLoop);

            if l.f.head.htype != BM_FACE {
                err |= IS_LOOP_WRONG_FACE_TYPE;
            }
            if l.e.head.htype != BM_EDGE {
                err |= IS_LOOP_WRONG_EDGE_TYPE;
            }
            if l.v.head.htype != BM_VERT {
                err |= IS_LOOP_WRONG_VERT_TYPE;
            }
            if !BM_vert_in_edge(l.e, l.v) {
                err |= IS_LOOP_VERT_NOT_IN_EDGE;
            }

            if l.radial_next == null() || l.radial_prev == null() {
                err |= IS_LOOP_NULL_CYCLE_LINK;
            }
            if l.f.len <= 0 {
                err |= IS_LOOP_ZERO_FACE_LENGTH;
            }

            // validate boundary loop -- invalid for hole loops, of course,
            // but we won't be allowing those for a while yet
            let mut l2 = l;
            let mut i: i32 = 0;
            loop {
                if i == ::limits::BM_LOOP_CYCLE_MAX {
                    break;
                }
                i += 1;
                l2 = l2.next.as_const();
                if l2 == l {
                    break;
                }
            }

            if i != l.f.len || l2 != l {
                err |= IS_LOOP_WRONG_FACE_LENGTH;
            }

            if !bmesh_radial_validate(bmesh_radial_length(l), l) {
                err |= IS_LOOP_WRONG_RADIAL_LENGTH;
            }
        }

        BM_FACE => {
            use std::collections::HashSet;
            let f: BMFaceConstP = PtrConst(element.as_ptr() as *mut BMFace);
            let mut f_vert_set = HashSet::with_capacity(f.len as usize);
            let mut f_edge_set = HashSet::with_capacity(f.len as usize);
            let mut f_loop_set = HashSet::with_capacity(f.len as usize);
            let mut len = 0;

            if f.l_first == null() {
                err |= IS_FACE_NULL_LOOP;
            }
            let l_first = f.l_first.as_const();
            let mut l_iter = l_first;
            loop {
                if l_iter.f != f {
                    err |= IS_FACE_WRONG_LOOP_FACE;
                }

                if l_iter.e == null() {
                    err |= IS_FACE_NULL_EDGE;
                }
                if l_iter.v == null() {
                    err |= IS_FACE_NULL_VERT;
                }
                if l_iter.e != null() && l_iter.v != null() {
                    if !BM_vert_in_edge(l_iter.e, l_iter.v) ||
                       !BM_vert_in_edge(l_iter.e, l_iter.next.v)
                    {
                        err |= IS_FACE_LOOP_VERT_NOT_IN_EDGE;
                    }

                    if !bmesh_radial_validate(bmesh_radial_length(l_iter), l_iter) {
                        err |= IS_FACE_LOOP_WRONG_RADIAL_LENGTH;
                    }

                    if bmesh_disk_count_ex(l_iter.v, 2) < 2 {
                        err |= IS_FACE_LOOP_WRONG_DISK_LENGTH;
                    }
                }

                // check for duplicates
                if l_iter.v != null() &&
                   f_vert_set.insert(l_iter.v.as_ptr()) == false
                {
                    err |= IS_FACE_LOOP_DUPE_VERT;
                }
                if l_iter.e != null() &&
                   f_edge_set.insert(l_iter.e.as_ptr()) == false
                {
                    err |= IS_FACE_LOOP_DUPE_EDGE;
                }
                if f_loop_set.insert(l_iter.as_ptr()) == false {
                    err |= IS_FACE_LOOP_DUPE_LOOP;
                }

                len += 1;

                l_iter = l_iter.next.as_const();
                if l_iter == l_first {
                    break;
                }
            }

            if len != f.len {
                err |= IS_FACE_WRONG_LENGTH;
            }
        }
        _ => {
            return IS_WRONG_TYPE;
        }
    }

    debug_assert_eq!(err, 0);

    return err;
}

