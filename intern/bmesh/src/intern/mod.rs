// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

#![allow(non_snake_case)]

/// This is a low-level API,
/// limited to functional (non-object-oriented) style calls.
///
/// The `bmesh::api` module exposes these low level functions
/// for external access, attaching methods to structs.

pub mod bmesh_core;

pub mod bmesh_construct;
pub mod bmesh_interp;
pub mod bmesh_marking;
pub mod bmesh_marking_builder;
pub mod bmesh_mesh;
pub mod bmesh_mesh_validate;
pub mod bmesh_mods;
pub mod bmesh_polygon;
pub mod bmesh_queries;
pub mod bmesh_structure;
