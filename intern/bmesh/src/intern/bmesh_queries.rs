// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

///
/// This file contains functions for answering common
/// Topological and geometric queries about a mesh, such
/// as, "What is the angle between these two faces?" or,
/// "How many faces are incident upon this vertex?" Tool
/// authors should use the functions in this file instead
/// of inspecting the mesh structure directly.
///

use prelude::*;

use intern::bmesh_structure::*;
use intern::bmesh_polygon::*;

use math_misc::{
    angle_v3v3v3,
    cross_v3v3,
    dot_v3v3,
    equals_v3v3,
    len_squared_v3v3,
    len_v3v3,
    sub_v3v3,
};

///
/// \brief Other Loop in Face Sharing an Edge
///
/// Finds the other loop that shares \a v with \a e loop in \a f.
/// ```text
///     +----------+
///     |          |
///     |    f     |
///     |          |
///     +----------+ <-- return the face loop of this vertex.
///     v --> e
///     ^     ^ <------- These vert args define direction
///                      in the face to check.
///                      The faces loop direction is ignored.
/// ```
///
/// \note caller must ensure \a e is used in \a f
///
#[allow(dead_code)]
pub fn BM_face_other_edge_loop<V, E, F>(f: F, e: E, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
    F: Into<BMFaceConstP>,
{
    into_expand!(v, e, f);

    let l = BM_face_edge_share_loop(f, e);
    debug_assert!(l != null());
    return BM_loop_other_edge_loop(l, v);
}
#[allow(dead_code)]
pub fn BM_face_other_edge_loop_mut() { /* STUB */ }

///
/// See #BM_face_other_edge_loop This is the same functionality
/// to be used when the edges loop is already known.
///
pub fn BM_loop_other_edge_loop<V, L>(l: L, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    L: Into<BMLoopConstP>,
{
    into_expand!(v, l);

    debug_assert!(BM_vert_in_edge(l.e, v));
    return { if l.v == v { l.prev } else { l.next } }.as_const();
}
#[allow(dead_code)]
pub fn BM_loop_other_edge_loop_mut() { /* STUB */ }

///
/// \brief Other Loop in Face Sharing a Vertex
///
/// Finds the other loop in a face.
///
/// This function returns a loop in \a f that shares an edge with \a v
/// The direction is defined by \a v_prev, where the return value is
/// the loop of what would be 'v_next'
/// ```text
/// +----------+ <-- return the face loop of this vertex.
/// |          |
/// |    f     |
/// |          |
/// +----------+
/// v_prev --> v
/// ^^^^^^     ^ <-- These vert args define direction
///                  in the face to check.
///                  The faces loop direction is ignored.
/// ```
///
/// \note \a v_prev and \a v _implicitly_ define an edge.
///
#[allow(dead_code)]
pub fn BM_face_other_vert_loop<V, F>(f: F, v_prev: V, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    F: Into<BMFaceConstP>,
{
    into_expand!(f, v_prev, v);

    let l_iter = BM_face_vert_share_loop(f, v);

    debug_assert!(BM_edge_exists(v_prev, v) != null());


    if l_iter != null() {
        if l_iter.prev.v == v_prev {
            return l_iter.next.as_const();
        } else if l_iter.next.v == v_prev {
            return l_iter.prev.as_const();
        } else {
            // invalid args
            debug_assert!(false);
            return null_const();
        }
    } else {
        // invalid args
        debug_assert!(false);
        return null_const();
    }
}
#[allow(dead_code)]
pub fn BM_face_other_vert_loop_mut() { /* STUB */ }

///
/// \brief Other Loop in Face Sharing a Vert
///
/// Finds the other loop that shares \a v with \a e loop in \a f.
/// ```text
/// +----------+ <-- return the face loop of this vertex.
/// |          |
/// |          |
/// |          |
/// +----------+ <-- This vertex defines the direction.
///       l    v
///       ^ <------- This loop defines both the face to search
///                  and the edge, in combination with 'v'
///                  The faces loop direction is ignored.
/// ```
///
#[allow(dead_code)]
pub fn BM_loop_other_vert_loop<V, L>(l: L, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    L: Into<BMLoopConstP>,
{
    // works but slow
    // return BM_face_other_vert_loop(l.f, BM_edge_other_vert(l.e, v), v);

    into_expand!(l, v);

    let e = l.e;
    let v_prev = BM_edge_other_vert(e, v);
    if l.v == v {
        if l.prev.v == v_prev {
            return l.next.as_const();
        } else {
            debug_assert!(l.next.v == v_prev);

            return l.prev.as_const();
        }
    } else {
        debug_assert!(l.v == v_prev);

        if l.prev.v == v {
            return l.prev.prev.as_const();
        } else {
            debug_assert!(l.next.v == v);
            return l.next.next.as_const();
        }
    }
}
#[allow(dead_code)]
pub fn BM_loop_other_vert_loop_mut() { /* STUB */ }


pub fn BM_vert_pair_share_face_check<V>(v_a: V, v_b: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v_a, v_b);

    bm_iter_loops_of_vert!(v_a, l_iter, {
        if BM_vert_in_face(v_b, l_iter.f) {
            return true;
        }
    });
    return false
}

#[allow(dead_code)]
pub fn BM_vert_pair_share_face_check_cb() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_pair_share_face_by_len() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_pair_share_face_by_len() { /* STUB */ }
#[allow(dead_code)]
fn bm_face_calc_split_dot() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_point_side_of_loop_test() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_point_side_of_edge_test() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_pair_share_face_by_angle() { /* STUB */ }

///
/// Get the first loop of a vert.
/// Uses the same initialization code for the first loop of the iterator API
///
#[allow(dead_code)]
pub fn BM_vert_find_first_loop<V>(v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    return {
        if v.e != null() {
            bmesh_disk_faceloop_find_first(v.e, v)
        } else {
            null_const()
        }
    };
}

#[allow(dead_code)]
pub fn BM_vert_find_first_loop_mut() { /* STUB */ }

///
/// Returns true if the vertex is used in a given face.
///
pub fn BM_vert_in_face<V, F>(v: V, f: F) -> bool
    where
    V: Into<BMVertConstP>,
    F: Into<BMFaceConstP>,
{
    into_expand!(v, f);

    bm_iter_loops_of_face_cycle!(f, l_iter, {
        if l_iter.v == v {
            return true;
        }
    });
    return false;
}

///
/// Compares the number of vertices in an array
/// that appear in a given face
///
#[allow(dead_code)]
pub fn BM_verts_in_face_count<V, F>(varr: &[V], f: F) -> usize
    where
    V: Into<BMVertConstP> + Copy,
    F: Into<BMFaceConstP>,

{
    into_expand!(f);

    use bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_OVERLAP;

    let mut count: usize = 0;

    for v in varr {
        let v: BMVertConstP = (*v).into();
        // accept this because its an API flag which is reset
        unsafe {
            let mut v = v.as_mut();
            bm_elem_api_flag_enable!(v, FLAG_OVERLAP);
        }
    }

    {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            if bm_elem_api_flag_test!(l_iter.v, FLAG_OVERLAP) {
                count += 1;
            }
            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
    }

    for v in varr {
        let v: BMVertConstP = (*v).into();
        unsafe {
            let mut v = v.as_mut();
            bm_elem_api_flag_disable!(v, FLAG_OVERLAP);
        }
    }

    return count;
}

///
/// Return true if all verts are in the face.
///
#[allow(dead_code)]
pub fn BM_verts_in_face<V, F>(varr: &[V], f: F) -> bool
    where
    V: Into<BMVertConstP> + Copy,
    F: Into<BMFaceConstP>,
{
    into_expand!(f);

    use bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_OVERLAP;
    // BMLoop *l_iter, *l_first;

    let mut ok = true;

    // simple check, we know can't succeed
    if (f.len as usize) < varr.len() {
        return false;
    }

    for v in varr {
        let v: BMVertConstP = (*v).into();
        // accept this because its an API flag which is reset
        unsafe {
            let mut v = v.as_mut();
            bm_elem_api_flag_enable!(v, FLAG_OVERLAP);
        }
    }

    {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            if bm_elem_api_flag_test!(l_iter.v, FLAG_OVERLAP) {
                // pass
            } else {
                ok = false;
                break;
            }

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
    }

    for v in varr {
        let v: BMVertConstP = (*v).into();
        unsafe {
            let mut v = v.as_mut();
            bm_elem_api_flag_disable!(v, FLAG_OVERLAP);
        }
    }

    return ok;
}

///
/// Returns whether or not a given edge is part of a given face.
///
pub fn BM_edge_in_face<E, F>(e: E, f: F) -> bool
    where
    E: Into<BMEdgeConstP>,
    F: Into<BMFaceConstP>,
{
    into_expand!(e, f);

    bm_iter_loops_of_edge_radial!(e, l_iter, {
        if l_iter.f == f {
            return true;
        }
    });
    return false;
}

///
/// Given a edge and a loop (assumes the edge is manifold).
/// Returns the other faces loop, sharing the same vertex.
///
/// ```text
/// +-------------------+
/// |                   |
/// |                   |
/// |l_other <-- return |
/// +-------------------+ <-- A manifold edge between 2 faces
/// |l    e  <-- edge   |
/// |^ <-------- loop   |
/// |                   |
/// +-------------------+
/// ```
///
pub fn BM_edge_other_loop(e: BMEdgeConstP, l: BMLoopConstP) -> BMLoopConstP {
    // TOO strict, just check if we have another radial face
    // debug_assert!(BM_edge_is_manifold(e));

    debug_assert!(e.l != null() && e.l.radial_next != e.l);
    debug_assert!(BM_vert_in_edge(e, l.v));

    let mut l_other = {
        if l.e == e  {
            l
        } else {
            l.prev.as_const()
        }
    }.radial_next.as_const();
    debug_assert!(l_other.e == e);

    if l_other.v == l.v {
        // pass
    } else if l_other.next.v == l.v {
        l_other = l_other.next.as_const();
    } else {
        debug_assert!(false);
    }

    return l_other;
}

///
/// Utility function to step around a fan of loops,
/// using an edge to mark the previous side.
///
/// \note all edges must be manifold,
/// once a non manifold edge is hit, return null().
///
/// ```text
///                ,.,-->|
///            _,-'      |
///          ,'          | (notice how 'e_step'
///         /            |  and 'l' define the
///        /             |  direction the arrow
///       |     return   |  points).
///       |     loop --> |
/// ---------------------+---------------------
///         ^      l --> |
///         |            |
///  assign e_step       |
///                      |
///   begin e_step ----> |
///                      |
/// ```
///
pub fn BM_vert_step_fan_loop<E, L>(l: L, e_step: E) -> (BMLoopConstP, BMEdgeConstP)
    where
    E: Into<BMEdgeConstP>,
    L: Into<BMLoopConstP>,
{
    into_expand!(l, e_step);

    let e_next;
    if l.e == e_step {
        e_next = l.prev.e.as_const();
    } else if l.prev.e == e_step {
        e_next = l.e.as_const();
    } else {
        debug_assert!(false);
        return (null_const(), null_const());
    }

    if BM_edge_is_manifold(e_next) {
        return (BM_edge_other_loop(e_next, l), e_next);
    } else {
        return (null_const(), null_const());
    }
}
#[inline]
pub fn BM_vert_step_fan_loop_mut(l: BMLoopMutP, e_step: BMEdgeMutP) -> (BMLoopMutP, BMEdgeMutP) {
    let tmp = BM_vert_step_fan_loop(l, e_step);
    return unsafe {
        (tmp.0.as_mut(), tmp.1.as_mut())
    };
}

///
/// The function takes a vertex at the center of a fan and returns the opposite edge in the fan.
/// All edges in the fan must be manifold, otherwise return null().
///
/// \note This could (probably) be done more efficiently.
///
#[allow(dead_code)]
pub fn BM_vert_other_disk_edge<V, E>(v: V, e_first: E) -> BMEdgeConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(v, e_first);

    let mut tot = 0;

    debug_assert!(BM_vert_in_edge(e_first, v));

    let mut l_a = e_first.l.as_const();
    loop {
        let mut l_a = BM_loop_other_vert_loop(l_a, v);
        l_a = if BM_vert_in_edge(l_a.e, v) { l_a } else { l_a.prev.as_const() };
        if BM_edge_is_manifold(l_a.e) {
            l_a = l_a.radial_next.as_const();
        } else {
            return null_const();
        }

        tot += 1;

        if l_a == e_first.l {
            break;
        }
    }

    // we know the total, now loop half way
    tot /= 2;
    let mut i = 0;

    l_a = e_first.l.as_const();
    loop {
        if i == tot {
            l_a = if BM_vert_in_edge(l_a.e, v) { l_a } else { l_a.prev.as_const() };
            return l_a.e.as_const();
        }

        l_a = BM_loop_other_vert_loop(l_a, v);
        l_a = if BM_vert_in_edge(l_a.e, v) { l_a } else { l_a.prev.as_const() };
        if BM_edge_is_manifold(l_a.e) {
            l_a = l_a.radial_next.as_const();
        }
        // this wont have changed from the previous loop

        i += 1;

        if l_a == e_first.l {
            break;
        }
    }

    return null_const();
}
#[allow(dead_code)]
pub fn BM_vert_other_disk_edge_mut() { /* STUB */ }

pub fn BM_edge_calc_length<E>(e: E) -> f64
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    return len_v3v3(&e.verts[0].co, &e.verts[1].co);
}

pub fn BM_edge_calc_length_squared<E>(e: E) -> f64
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    return len_squared_v3v3(&e.verts[0].co, &e.verts[1].co);
}

///
/// Utility function, since enough times we have an edge
/// and want to access 2 connected faces.
///
/// \return true when only 2 faces are found.
///
#[allow(dead_code)]
pub fn BM_edge_face_pair<E>(e: E) -> (BMFaceConstP, BMFaceConstP)
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let l_a = e.l;
    if l_a != null() {
        let l_b = l_a.radial_next;
        if l_a != l_b {
            if l_a.radial_next == l_b {
                return (l_a.f.as_const(), l_b.f.as_const());
            }
        }
    }
    return (null_const(), null_const());
}
#[allow(dead_code)]
pub fn BM_edge_face_pair_mut() { /* STUB */ }

///
/// Utility function, since enough times we have an edge
/// and want to access 2 connected loops.
///
/// \return true when only 2 faces are found.
///
#[allow(dead_code)]
pub fn BM_edge_loop_pair<E>(e: E) -> (BMLoopConstP, BMLoopConstP)
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let l_a = e.l.as_const();
    if l_a != null() {
        let l_b = l_a.radial_next.as_const();
        if l_a != l_b {
            if l_a.radial_next == l_b {
                return (l_a, l_b);
            }
        }
    }
    return (null_const(), null_const());
}
#[allow(dead_code)]
pub fn BM_edge_loop_pair_mut() { /* STUB */ }

///
/// Fast alternative to ``(BM_vert_edge_count(v) == 2)``
///
#[allow(dead_code)]
pub fn BM_vert_is_edge_pair<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let e = v.e;
    if e != null() {
        let e_other = bmesh_disk_edge_next(e, v);
        return (e_other != e) && (bmesh_disk_edge_next(e_other, v) == e);
    }
    return false;
}

///
/// Access a verts 2 connected edges.
///
/// \return true when only 2 verts are found.
///
#[allow(dead_code)]
pub fn BM_vert_edge_pair<V>(v: V) -> (BMEdgeConstP, BMEdgeConstP)
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let e_a = v.e.as_const();
    if e_a != null() {
        let e_b = bmesh_disk_edge_next(e_a, v);
        if (e_b != e_a) && (bmesh_disk_edge_next(e_b, v) == e_a) {
            return (e_a, e_b);
        }
    }
    return (null_const(), null_const());
}
#[allow(dead_code)]
pub fn BM_vert_edge_pair_mut() { /* STUB */ }

#[inline]
pub fn BM_vert_edge_count<V>(v: V) -> usize
    where
    V: Into<BMVertConstP>,
{
    return bmesh_disk_count(v);
}
#[inline]
pub fn BM_vert_edge_count_ex<V>(v: V, count_max: usize) -> usize
    where
    V: Into<BMVertConstP>,
{
    return bmesh_disk_count_ex(v, count_max);
}

#[allow(dead_code)]
pub fn BM_vert_edge_count_nonwire<V>(v: V) -> usize
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let mut count = 0;
    bm_iter_edges_of_vert!(v, e_iter, {
        if e_iter.l != null() {
            count += 1;
        }
    });
    return count;
}

///
/// Returns the number of faces around this edge.
///
pub fn BM_edge_face_count<E>(e: E) -> usize
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let mut count = 0;

    if e.l != null() {
        bm_iter_loops_of_edge_radial!(e, _l_iter, {
            count += 1;
        });
    }

    return count;
}

pub fn BM_edge_face_count_ex<E>(e: E, count_max: usize) -> usize
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let mut count = 0;

    if e.l != null() {
        bm_iter_loops_of_edge_radial!(e, _l_iter, {
            count += 1;
            if count == count_max {
                break;
            }
        });
    }

    return count;
}

#[inline]
pub fn BM_vert_face_count<V>(v: V) -> usize
    where
    V: Into<BMVertConstP>,
{
   return bmesh_disk_facevert_count(v);
}
#[inline]
pub fn BM_vert_face_count_ex<V>(v: V, count_max: usize) -> usize
    where
    V: Into<BMVertConstP>,
{
   return bmesh_disk_facevert_count_ex(v, count_max);
}

#[allow(dead_code)]
pub fn BM_vert_face_check<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    bm_iter_edges_of_vert!(v, e_iter, {
        if e_iter.l != null() {
            return true;
        }
    });
    return false;
}

#[allow(dead_code)]
pub fn BM_vert_is_wire<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    if v.e != null() {
        bm_iter_edges_of_vert!(v, e_iter, {
            if e_iter.l != null() {
                return false;
            }
        });
        return true;
    }
    return false;
}

///
/// A vertex is non-manifold if it meets the following conditions:
/// 1: Loose - (has no edges/faces incident upon it).
/// 2: Joins two distinct regions - (two pyramids joined at the tip).
/// 3: Is part of an edge with more than 2 faces.
/// 4: Is part of a wire edge.
///
pub fn BM_vert_is_manifold<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    if v.e == null() {
        // loose vert
        return false;
    }

    let mut loop_num = 0;
    let mut boundary_num = 0;

    // count edges while looking for non-manifold edges
    let mut e_first = v.e.as_const();
    let mut e_iter = e_first;
    let mut l_first = e_iter.l.as_const();
    loop {
        // loose edge or edge shared by more than two faces,
        // edges with 1 face user are OK, otherwise we could
        // use BM_edge_is_manifold() here
        if e_iter.l == null() || (e_iter.l != e_iter.l.radial_next.radial_next) {
            return false;
        }

        // count radial loops
        if e_iter.l.v == v {
            loop_num += 1;
        }

        if !BM_edge_is_boundary(e_iter) {
            // non boundary check opposite loop
            if e_iter.l.radial_next.v == v {
                loop_num += 1;
            }
        } else {
            // start at the boundary
            l_first = e_iter.l.as_const();
            boundary_num += 1;
            // >2 boundaries cant be manifold
            if boundary_num == 3 {
                return false;
            }
        }

        e_iter = bmesh_disk_edge_next(e_iter, v);
        if e_iter == e_first {
            break;
        }
    }

    e_first = l_first.e.as_const();
    l_first = {
        if l_first.v == v {
            l_first
        } else {
            l_first.next.as_const()
        }
    };
    debug_assert!(l_first.v == v);

    let mut l_iter = l_first;
    let mut e_prev = e_first;

    let mut loop_num_region = 0;
    loop {
        loop_num_region += 1;
        // One day this might be supported!
        // http://stackoverflow.com/questions/40563116

        // (l_iter, e_prev) = BM_vert_step_fan_loop(l_iter, e_prev);

        // awkward alternative
        {
            let tmp = BM_vert_step_fan_loop(l_iter, e_prev);
            l_iter = tmp.0;
            e_prev = tmp.1;
        }

        if l_iter == l_first || l_iter == null() {
            break;
        }
    }

    return loop_num == loop_num_region;
}

use bmesh_class;
// use bmesh_class::bmesh_bitflag_bmelem_api_flag::BMElemApiFlag;
// only for bm_loop_region_count__recursive
#[allow(dead_code)]
const EDGE_VISIT: bmesh_class::bmesh_bitflag_bmelem_api_flag::BMElemApiFlag =
    bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_WALK;
#[allow(dead_code)]
const LOOP_VISIT: bmesh_class::bmesh_bitflag_bmelem_api_flag::BMElemApiFlag =
    bmesh_class::bmesh_bitflag_bmelem_api_flag::FLAG_WALK;

#[allow(dead_code)]
fn bm_loop_region_count__recursive(mut e: BMEdgeMutP, v: BMVertMutP) -> usize {
    // BMLoop *l_iter, *l_first;
    let mut count = 0;

    debug_assert!(!bm_elem_api_flag_test!(e, EDGE_VISIT));
    bm_elem_api_flag_enable!(e, EDGE_VISIT);

    let l_first = e.l;
    let mut l_iter = l_first;
    loop {
        if l_iter.v == v {
            let e_other = l_iter.prev.e;
            if !bm_elem_api_flag_test!(l_iter, LOOP_VISIT) {
                bm_elem_api_flag_enable!(l_iter, LOOP_VISIT);
                count += 1;
            }
            if !bm_elem_api_flag_test!(e_other, EDGE_VISIT) {
                count += bm_loop_region_count__recursive(e_other, v);
            }
        } else if l_iter.next.v == v {
            let e_other = l_iter.next.e;
            if !bm_elem_api_flag_test!(l_iter.next, LOOP_VISIT) {
                bm_elem_api_flag_enable!(l_iter.next, LOOP_VISIT);
                count += 1;
            }
            if !bm_elem_api_flag_test!(e_other, EDGE_VISIT) {
                count += bm_loop_region_count__recursive(e_other, v);
            }
        } else {
            debug_assert!(false);
        }

        l_iter = l_iter.radial_next;
        if l_iter == l_first {
            break;
        }
    }

    return count;
}
#[allow(dead_code)]
fn bm_loop_region_count__clear(l: BMLoopConstP) -> usize {
    let mut count = 0;
    // BMEdge *e_iter, *e_first;

    // clear flags
    let e_first = l.e;
    let mut e_iter = e_first;
    loop {
        bm_elem_api_flag_disable!(e_iter, EDGE_VISIT);
        if e_iter.l != null() {
            // BMLoop *l_iter, *l_first;
            let l_first = e_iter.l;
            let mut l_iter = l_first;
            loop {
                if l_iter.v == l.v {
                    bm_elem_api_flag_disable!(l_iter, LOOP_VISIT);
                    count += 1;
                }
                l_iter = l_iter.radial_next;
                if l_iter == l_first {
                    break;
                }
            }
        }
        e_iter = bmesh_disk_edge_next_mut(e_iter, l.v);
        if e_iter == e_first {
            break;
        }
    }

    return count;
}

#[allow(dead_code)]
pub fn BM_loop_region_loops_count_ex<L>(l: L) -> (usize, usize)
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);
    let count       = bm_loop_region_count__recursive(l.e, l.v);
    let count_total = bm_loop_region_count__clear(l);
    return (count, count_total);
}

#[allow(dead_code)]
pub fn BM_loop_region_loops_count<L>(l: L) -> usize
    where
    L: Into<BMLoopConstP>,
{
    let (count, _count_total) = BM_loop_region_loops_count_ex(l);
    return count;
}

///
/// A version of #BM_vert_is_manifold
/// which only checks if we're connected to multiple isolated regions.
///
#[allow(dead_code)]
pub fn BM_vert_is_manifold_region<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let l_first = BM_vert_find_first_loop(v);
    if l_first != null() {
        let (count, count_total) = BM_loop_region_loops_count_ex(l_first);
        return count == count_total;
    }
    return true;
}

///
/// Check if the edge is convex or concave
/// (depends on face winding)
///
/// Exposed as: BMEdge.is_convex()
#[allow(dead_code)]
pub fn BM_edge_is_convex<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    if BM_edge_is_manifold(e) {
        let l1 = e.l;
        let l2 = e.l.radial_next;
        if !equals_v3v3(&l1.f.no, &l2.f.no) {
            let cross = cross_v3v3(&l1.f.no, &l2.f.no);
            // we assume contiguous normals, otherwise the result isn't meaningful
            let l_dir = sub_v3v3(&l1.next.v.co, &l1.v.co);
            return dot_v3v3(&l_dir, &cross) > 0.0;
        }
    }
    return true;
}


#[allow(dead_code)]
pub fn BM_edge_is_contiguous_loop_cd() { /* STUB */ }

#[allow(dead_code)]
pub fn BM_vert_is_boundary<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    if v.e != null() {
        bm_iter_edges_of_vert!(v, e_iter, {
            if BM_edge_is_boundary(e_iter) {
                return true;
            }
        });
        return false;
    } else {
        return false;
    }
}

#[allow(dead_code)]
pub fn BM_face_share_face_count() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_share_face_check() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_share_edge_count() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_share_edge_check() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_share_vert_count() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_share_vert_check() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_share_edge_check() { /* STUB */ }

///
/// Test if e1 shares any faces with e2
///
pub fn BM_edge_share_face_check<E>(e1: E, e2: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e1, e2);

    if e1.l != null() &&
       e2.l != null()
    {
        let mut l = e1.l;
        loop {
            let f = l.f.as_const();
            if BM_edge_in_face(e2, f) {
                return true;
            }
            l = l.radial_next;
            if l == e1.l {
                break;
            }
        }
    }
    return false;
}
#[allow(dead_code)]
pub fn BM_edge_share_quad_check() { /* STUB */ }

#[allow(dead_code)]
pub fn BM_edge_share_vert_check<E>(e1: E, e2: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e1, e2);

    return e1.verts[0] == e2.verts[0] ||
           e1.verts[0] == e2.verts[1] ||
           e1.verts[1] == e2.verts[0] ||
           e1.verts[1] == e2.verts[1];
}

///
/// Return the shared vertex between the two edges or NULL
///
#[allow(dead_code)]
pub fn BM_edge_share_vert<E>(e1: E, e2: E) -> BMVertConstP
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e1, e2);

    debug_assert!(e1 != e2);
    if BM_vert_in_edge(e2, e1.verts[0]) {
        return e1.verts[0].as_const();
    } else if BM_vert_in_edge(e2, e1.verts[1]) {
        return e1.verts[1].as_const();
    } else {
        return null_const();
    }
}
#[allow(dead_code)]
pub fn BM_edge_share_vert_mut() { /* STUB */ }

///
/// \brief Return the Loop Shared by Edge and Vert
///
/// Finds the loop used which uses \a  in face loop \a l
///
/// \note this function takes a loop rather then an edge
/// so we can select the face that the loop should be from.
///
#[allow(dead_code)]
pub fn BM_edge_vert_share_loop<V, L>(l: L, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    L: Into<BMLoopConstP>,
{
    into_expand!(l, v);

    debug_assert!(BM_vert_in_edge(l.e, v));
    if l.v == v {
        return l;
    } else {
        return l.next.as_const();
    }
}
#[allow(dead_code)]
pub fn BM_edge_vert_share_loop_mut() { /* STUB */ }

///
/// \brief Return the Loop Shared by Face and Vertex
///
/// Finds the loop used which uses \a v in face loop \a l
///
/// \note currently this just uses simple loop in future may be sped up
/// using radial vars
///
pub fn BM_face_vert_share_loop<V, F>(f: F, v: V) -> BMLoopConstP
    where
    V: Into<BMVertConstP>,
    F: Into<BMFaceConstP>,
{
    into_expand!(f, v);

    let l_first = f.l_first.as_const();
    let mut l_iter = l_first;
    loop {
        if l_iter.v == v {
            return l_iter;
        }

        l_iter = l_iter.next.as_const();
        if l_iter == l_first {
            break;
        }
    }

    return null_const();
}
pub fn BM_face_vert_share_loop_mut(
    f: BMFaceMutP,
    v: BMVertMutP,
) -> BMLoopMutP {
    let l = BM_face_vert_share_loop(f, v);
    return unsafe { l.as_mut() };
}

///
/// \brief Return the Loop Shared by Face and Edge
///
/// Finds the loop used which uses \a e in face loop \a l
///
/// \note currently this just uses simple loop in future may be sped up
/// using radial vars
///
pub fn BM_face_edge_share_loop<E, F>(f: F, e: E) -> BMLoopConstP
    where
    E: Into<BMEdgeConstP>,
    F: Into<BMFaceConstP>,
{
    into_expand!(f, e);

    let l_first = e.l.as_const();
    let mut l_iter = l_first;
    loop {
        if l_iter.f == f {
            return l_iter;
        }
        l_iter = l_iter.radial_next.as_const();
        if l_iter == l_first {
            break;
        }
    }
    return null_const();
}

#[allow(dead_code)]
pub fn BM_face_edge_share_loop_mut(
    f: BMFaceMutP,
    e: BMEdgeMutP,
) -> BMLoopMutP {
    let l = BM_face_edge_share_loop(f, e);
    return unsafe { l.as_mut() };
}

#[allow(dead_code)]
pub fn BM_edge_ordered_verts_ex() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_ordered_verts() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_find_prev_nodouble() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_find_next_nodouble() { /* STUB */ }

///
/// Check if the loop is convex or concave
/// (depends on face normal)
///
/// Exposed as: BMLoop.is_convex()
pub fn BM_loop_is_convex<L>(l: L) -> bool
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);

    let e_dir_prev = sub_v3v3(&l.prev.v.co, &l.v.co);
    let e_dir_next = sub_v3v3(&l.next.v.co, &l.v.co);
    let l_no = cross_v3v3(&e_dir_next, &e_dir_prev);
    return dot_v3v3(&l_no, &l.f.no) > 0.0;
}

///
/// Calculates the angle between the previous and next loops
/// (angle at this loops face corner).
///
/// \return angle in radians
///
/// Exposed as: BMLoop.calc_face_angle()
///
pub fn BM_loop_calc_face_angle<L>(l: L) -> f64
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);

    return angle_v3v3v3(&l.prev.v.co,
                        &l.v.co,
                        &l.next.v.co);
}

#[allow(dead_code)]
pub fn BM_loop_calc_face_normal() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_calc_face_direction() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_loop_calc_face_tangent() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_calc_face_angle_ex() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_calc_face_angle() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_calc_face_angle_signed_ex() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_calc_face_angle_signed() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_edge_calc_face_tangent() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_calc_edge_angle_ex() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_calc_edge_angle() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_calc_shell_factor() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_calc_shell_factor_ex() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_vert_calc_mean_tagged_edge_length() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_find_shortest_loop() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_find_longest_loop() { /* STUB */ }

pub fn BM_edge_exists<V>(v_a: V, v_b: V) -> BMEdgeConstP
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v_a, v_b);

    // speedup by looping over both edges verts
    // where one vert may connect to many edges but not the other.

    let e_a = v_a.e.as_const();
    let e_b = v_b.e.as_const();
    debug_assert!(v_a != v_b);

    if e_a != null_const() && e_b != null_const() {
        let mut e_a_iter = e_a;
        let mut e_b_iter = e_b;
        loop {
            if BM_vert_in_edge(e_a_iter, v_b) {
                return e_a_iter;
            }
            if BM_vert_in_edge(e_b_iter, v_a) {
                return e_b_iter;
            }

            e_a_iter = bmesh_disk_edge_next(e_a_iter, v_a);
            if e_a_iter == e_a {
                break;
            }
            e_b_iter = bmesh_disk_edge_next(e_b_iter, v_b);
            if e_b_iter == e_b {
                break;
            }
        }
    }
    return null_const();
}
#[inline]
pub fn BM_edge_exists_mut(v_a: BMVertMutP, v_b: BMVertMutP) -> BMEdgeMutP {
    let e = BM_edge_exists(v_a, v_b);
    return unsafe { e.as_mut() };
}

pub fn BM_edge_find_double<E>(e: E) -> BMEdgeConstP
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let v = e.verts[0].as_const();
    let v_other = e.verts[1].as_const();

    let mut e_iter = e;
    loop {
        e_iter = bmesh_disk_edge_next(e_iter, v);
        if e_iter == e {
            break;
        }
        if unlikely!(BM_vert_in_edge(e_iter, v_other)) {
            return e_iter;
        }
    }

    return null_const();
}
#[allow(dead_code)]
pub fn BM_edge_find_double_mut<E>(e: E) -> BMEdgeMutP
    where
    E: Into<BMEdgeConstP>,
{
    let e = BM_edge_find_double(e);
    return unsafe { e.as_mut() };
}

///
/// Given a set of vertices (varr), find out if
/// there is a face with exactly those vertices
/// (and only those vertices).
///
/// \note there used to be a BM_face_exists_overlap function that checks for partial overlap.
///
pub fn BM_face_exists<V>(varr: &[V]) -> BMFaceConstP
    where
    V: Into<BMVertConstP> + Copy,
{
    if varr[0].into().e != null() {
        let len = varr.len();
        let e_first = varr[0].into().e.as_const();
        let mut e_iter = e_first;

        // would normally use BM_LOOPS_OF_VERT, but this runs so often,
        // its faster to iterate on the data directly
        loop {
            if e_iter.l != null() {
                let l_first_radial = e_iter.l.as_const();
                let mut l_iter_radial = l_first_radial;

                loop {
                    if (l_iter_radial.v == varr[0].into()) &&
                       (l_iter_radial.f.len as usize == len)
                    {
                        // the fist 2 verts match, now check the remaining (len - 2) faces do too
                        // winding isn't known, so check in both directions
                        let mut i_walk = 2;

                        if l_iter_radial.next.v == varr[1].into() {
                            let mut l_walk = l_iter_radial.next.next.as_const();
                            loop {
                                if l_walk.v != varr[i_walk].into() {
                                    break;
                                }
                                l_walk = l_walk.next.as_const();
                                i_walk += 1;
                                if i_walk == len {
                                    break;
                                }
                            }
                        } else if l_iter_radial.prev.v == varr[1].into() {
                            let mut l_walk = l_iter_radial.prev.prev.as_const();
                            loop {
                                if l_walk.v != varr[i_walk].into() {
                                    break;
                                }

                                l_walk = l_walk.prev.as_const();
                                i_walk += 1;
                                if i_walk == len {
                                    break;
                                }
                            }
                        }

                        if i_walk == len {
                            return l_iter_radial.f.as_const();
                        }
                    }

                    l_iter_radial = l_iter_radial.radial_next.as_const();
                    if l_iter_radial == l_first_radial {
                        break;
                    }
                }
            }

            e_iter = bmesh_disk_edge_next(e_iter, varr[0]);
            if e_iter == e_first {
                break;
            }
        }
    }

    return null_const();
}
#[inline]
pub fn BM_face_exists_mut<V>(varr: &[V]) -> BMFaceMutP
    where
    V: Into<BMVertConstP> + Copy,
{
    let f = BM_face_exists(varr);
    return unsafe { f.as_mut() };
}

#[allow(dead_code)]
pub fn BM_face_exists_multi() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_exists_multi_edge() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_exists_overlap() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_face_exists_overlap_subset() { /* STUB */ }

#[inline]
fn bm_vert_is_any_edge_flag_test_value<V>(v: V, flag: BMElemFlag, value: bool) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);
    bm_iter_edges_of_vert!(v, e_iter, {
        if ((e_iter.head.hflag & flag) != 0) == value {
            return true;
        }
    });
    return false;
}
#[allow(dead_code)]
pub fn BM_vert_is_all_edge_flag_test<V: Into<BMVertConstP>>(v: V, flag: BMElemFlag) -> bool {
    return !bm_vert_is_any_edge_flag_test_value(v, flag, false);
}
pub fn BM_vert_is_any_edge_flag_test<V: Into<BMVertConstP>>(v: V, flag: BMElemFlag) -> bool {
    return bm_vert_is_any_edge_flag_test_value(v, flag, true);
}

#[inline]
fn bm_vert_is_any_face_flag_test_value<V>(v: V, flag: BMElemFlag, value: bool) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);
    bm_iter_loops_of_vert!(v, l_iter, {
        if ((l_iter.f.head.hflag & flag) != 0) == value {
            return true;
        }
    });
    return false;
}
#[allow(dead_code)]
pub fn BM_vert_is_all_face_flag_test<V: Into<BMVertConstP>>(v: V, flag: BMElemFlag) -> bool {
    return !bm_vert_is_any_face_flag_test_value(v, flag, false);
}
pub fn BM_vert_is_any_face_flag_test<V: Into<BMVertConstP>>(v: V, flag: BMElemFlag) -> bool {
    return bm_vert_is_any_face_flag_test_value(v, flag, true);
}

#[inline]
fn bm_edge_is_any_vert_flag_test_value<E>(e: E, flag: BMElemFlag, value: bool) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);
    bm_iter_verts_of_edge!(e, v_iter, {
        if ((v_iter.head.hflag & flag) != 0) == value {
            return true;
        }
    });
    return false;
}
pub fn BM_edge_is_all_vert_flag_test<E: Into<BMEdgeConstP>>(e: E, flag: BMElemFlag) -> bool {
    return !bm_edge_is_any_vert_flag_test_value(e, flag, false);
}
#[allow(dead_code)]
pub fn BM_edge_is_any_vert_flag_test<E: Into<BMEdgeConstP>>(e: E, flag: BMElemFlag) -> bool {
    return bm_edge_is_any_vert_flag_test_value(e, flag, true);
}

#[inline]
fn bm_edge_is_any_face_flag_test_value<E>(e: E, flag: BMElemFlag, value: bool) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);
    bm_iter_loops_of_edge_radial!(e, l_iter, {
        if ((l_iter.f.head.hflag & flag) != 0) == value {
            return true;
        }
    });
    return false;
}
#[allow(dead_code)]
pub fn BM_edge_is_all_face_flag_test<E: Into<BMEdgeConstP>>(e: E, flag: BMElemFlag) -> bool {
    return !bm_edge_is_any_face_flag_test_value(e, flag, false);
}
pub fn BM_edge_is_any_face_flag_test<E: Into<BMEdgeConstP>>(e: E, flag: BMElemFlag) -> bool {
    return bm_edge_is_any_face_flag_test_value(e, flag, true);
}

#[inline]
fn bm_face_is_any_vert_flag_test_value<F>(f: F, flag: BMElemFlag, value: bool) -> bool
    where
    F: Into<BMFaceConstP>,
{
    into_expand!(f);
    bm_iter_loops_of_face_cycle!(f, l_iter, {
        if ((l_iter.v.head.hflag & flag) != 0) == value {
            return true;
        }
    });
    return false;
}
pub fn BM_face_is_all_vert_flag_test<F: Into<BMFaceConstP>>(f: F, flag: BMElemFlag) -> bool {
    return !bm_face_is_any_vert_flag_test_value(f, flag, false);
}
#[allow(dead_code)]
pub fn BM_face_is_any_vert_flag_test<F: Into<BMFaceConstP>>(f: F, flag: BMElemFlag) -> bool {
    return bm_face_is_any_vert_flag_test_value(f, flag, true);
}

#[inline]
fn bm_face_is_any_edge_flag_test_value<F>(f: F, flag: BMElemFlag, value: bool) -> bool
    where
    F: Into<BMFaceConstP>,
{
    into_expand!(f);
    bm_iter_loops_of_face_cycle!(f, l_iter, {
        if ((l_iter.e.head.hflag & flag) != 0) == value {
            return true;
        }
    });
    return false;
}
pub fn BM_face_is_all_edge_flag_test<F: Into<BMFaceConstP>>(f: F, flag: BMElemFlag) -> bool {
    return !bm_face_is_any_edge_flag_test_value(f, flag, false);
}
#[allow(dead_code)]
pub fn BM_face_is_any_edge_flag_test<F: Into<BMFaceConstP>>(f: F, flag: BMElemFlag) -> bool {
    return bm_face_is_any_edge_flag_test_value(f, flag, true);
}

// end flag checks


pub fn BM_face_is_normal_valid<F>(f: F) -> bool
    where
    F: Into<BMFaceConstP> + Copy,
{
    into_expand!(f);

    let eps = 0.0001;
    let mut no: [f64; 3] = [0.0, 0.0, 0.0];
    BM_face_calc_normal(f, &mut no);
    return len_squared_v3v3(&no, &f.no) < (eps * eps);
}

#[allow(dead_code)]
fn bm_mesh_calc_volume_face() { /* STUB */ }

#[allow(dead_code)]
pub fn BM_mesh_calc_volume(_bm: &BMesh) -> f64 {
    /* STUB */
    return 0.0;
}

#[allow(dead_code)]
pub fn BM_mesh_calc_face_groups() { /* STUB */ }
#[allow(dead_code)]
pub fn BM_mesh_calc_edge_groups() { /* STUB */ }
#[allow(dead_code)]
pub fn bmesh_subd_falloff_calc() { /* STUB */ }

// -----------------------------------------------------------------------------
// Inline

///
/// Returns whether or not a given vertex is
/// is part of a given edge.
///
#[inline]
pub fn BM_vert_in_edge<V, E>(e: E, v: V) -> bool
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(e, v);

    return elem!(v, e.verts[0], e.verts[1]);
}

///
/// Returns whether or not a given edge is part of a given loop.
///
#[allow(dead_code)]
#[inline]
pub fn BM_edge_in_loop<E, L>(e: E, l: L) -> bool
    where
    E: Into<BMEdgeConstP>,
    L: Into<BMLoopConstP>,
{
    into_expand!(e, l);

    return elem!(e, l.e, l.prev.e);
}

///
/// Returns whether or not two vertices are in
/// a given edge
///
#[inline]
pub fn BM_verts_in_edge<V, E>(v1: V, v2: V, e: E) -> bool
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(v1, v2, e);

    return (v1 == e.verts[0] && v2 == e.verts[1]) ||
           (v2 == e.verts[0] && v1 == e.verts[1]);
}

///
/// Given a edge and one of its vertices, returns
/// the other vertex.
///
#[inline]
pub fn BM_edge_other_vert<V, E>(e: E, v: V) -> BMVertConstP
    where
    V: Into<BMVertConstP>,
    E: Into<BMEdgeConstP>,
{
    into_expand!(e, v);

    if v == e.verts[1] {
        return e.verts[0].as_const();
    } else if v == e.verts[0] {
        return e.verts[1].as_const();
    } else {
        return null_const();
    }
}
#[inline]
pub fn BM_edge_other_vert_mut(e: BMEdgeMutP, v: BMVertMutP) -> BMVertMutP {
    let v = BM_edge_other_vert(e, v);
    return unsafe { v.as_mut() };
}

///
/// Tests whether or not the edge is part of a wire.
/// (ie: has no faces attached to it)
///
#[inline]
pub fn BM_edge_is_wire<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    return e.l == null();
}

///
/// Tests whether or not this edge is manifold.
/// A manifold edge has exactly 2 faces attached to it.
///
#[inline]
pub fn BM_edge_is_manifold<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let l = e.l;
    return l != null() && {
        {
            (l.radial_next != l) && // not 0 or 1 face users
            (l.radial_next.radial_next == l) // 2 face users
        }
    };
}

///
/// Tests that the edge is manifold and
/// that both its faces point the same way.
///
#[allow(dead_code)]
#[inline]
pub fn BM_edge_is_contiguous<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let l = e.l;
    return l != null() && {
        let l_other = l.radial_next;
        {
            (l_other != l) && // not 0 or 1 face users
            (l_other.radial_next == l) && // 2 face users
            (l_other.v != l.v)
        }
    };
}

///
/// Tests whether or not an edge is on the boundary
/// of a shell (has one face associated with it)
///
#[inline]
pub fn BM_edge_is_boundary<E>(e: E) -> bool
    where
    E: Into<BMEdgeConstP>,
{
    into_expand!(e);

    let l = e.l;
    return l != null() && (l.radial_next == l);
}

///
/// Tests whether one loop is next to another within the same face.
///
#[inline]
pub fn BM_loop_is_adjacent<L>(l_a: L, l_b: L) -> bool
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l_a, l_b);

    debug_assert!(l_a.f == l_b.f);
    debug_assert!(l_a != l_b);
    return elem!(l_b, l_a.next, l_a.prev);
}

#[allow(dead_code)]
#[inline]
pub fn BM_loop_is_manifold<L>(l: L) -> bool
    where
    L: Into<BMLoopConstP>,
{
    into_expand!(l);

    return (l != l.radial_next) &&
           (l == l.radial_next.radial_next);
}

///
/// Check if we have a single wire edge user.
///
#[allow(dead_code)]
#[inline]
pub fn BM_vert_is_wire_endpoint<V>(v: V) -> bool
    where
    V: Into<BMVertConstP>,
{
    into_expand!(v);

    let e = v.e.as_const();
    if (e != null()) && (e.l == null_mut()) {
        return bmesh_disk_edge_next(e, v) == e;
    }
    return false;
}

