// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use prelude::*;
use intern::bmesh_polygon::*;

use math_misc::{
    dot_v3v3,
    imadd_v3_v3fl,
    normalize_v3,
    normalize_v3_v3,
    normalize_sub_v3_v3v3,
    zero_v3,
};


#[allow(dead_code)]
pub fn bm_mesh_allocsize_default( /* STUB */ ) {}
#[allow(dead_code)]
pub fn bm_mesh_chunksize_default( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_mempool_init_ex( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_mempool_init( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_elem_toolflags_ensure( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_free( /* STUB */ ) {}

// Split into 2x functions compared to C version
fn bm_mesh_edges_calc_vectors(
    bm: &mut BMesh,
    edgevec: &mut Vec<[f64; 3]>,
) {
    let mut index: i32 = 0;
    for (mut e, e_vec) in bm.edges.iter_mut().zip(edgevec.iter_mut()) {
        e.head.index = index; // set_inline
        if e.l != null() {
            normalize_sub_v3_v3v3(e_vec, &e.verts[1].co, &e.verts[0].co);
        } else {
            // the edge vector will not be needed when the edge has no radial
        }
        index += 1;
    }
    bm.elem_index_dirty &= !BM_EDGE;
}

#[allow(dead_code)]
fn bm_mesh_edges_calc_vectors_vcos(
    bm: &mut BMesh,
    vcos: &Vec<[f64; 3]>,
    edgevec: &mut Vec<[f64; 3]>,
) {
    BM_mesh_elem_index_ensure(bm, BM_VERT);

    let mut index: i32 = 0;
    for (mut e, e_vec) in bm.edges.iter_mut().zip(edgevec.iter_mut()) {
        e.head.index = index; // set_inline
        if e.l != null() {
            normalize_sub_v3_v3v3(
                e_vec,
                &vcos[e.verts[1].head.index as usize],
                &vcos[e.verts[0].head.index as usize],
            );
        } else {
            // the edge vector will not be needed when the edge has no radial
        }
        index += 1;
    }
    bm.elem_index_dirty &= !BM_EDGE;
}


fn bm_mesh_verts_calc_normals(
    bm: &mut BMesh,
    edgevec: &mut Vec<[f64; 3]>,
) {
    BM_mesh_elem_index_ensure(bm, BM_EDGE);

    // add weighted face normals to vertices
    for f in bm.faces.iter() {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            // calculate the dot product of the two edges that
            // meet at the loop's vertex
            let (e1diff, e2diff) = unsafe {(
                edgevec.get_unchecked(l_iter.prev.e.head.index as usize),
                edgevec.get_unchecked(l_iter.e.head.index as usize),
            )};
            let mut dotprod = dot_v3v3(e1diff, e2diff);

            // edge vectors are calculated from e.v1 to e.v2, so
            // adjust the dot product if one but not both loops
            // actually runs from from e.v2 to e.v1
            if (l_iter.prev.e.verts[0] == l_iter.prev.v) ^ (l_iter.e.verts[0] == l_iter.v) {
                dotprod = -dotprod;
            }
            let fac = (-dotprod).acos();

            // accumulate weighted face normal into the vertex's normal
            imadd_v3_v3fl(&mut l_iter.v.no, &f.no, fac);

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
    }

    // normalize the accumulated vertex normals
    for mut v in bm.verts.iter_mut() {
        if unlikely!(normalize_v3(&mut v.no) == 0.0) {
            normalize_v3_v3(&mut v.clone().no, &v.co);
        }
    }
}
#[allow(dead_code)]
fn bm_mesh_verts_calc_normals_vcos(
    bm: &mut BMesh,
    edgevec: &mut Vec<[f64; 3]>,

    fnos: &Vec<[f64; 3]>,
    vcos: &Vec<[f64; 3]>,
    vnos: &mut Vec<[f64; 3]>,
) {
    BM_mesh_elem_index_ensure(bm, BM_EDGE | BM_VERT);

    // add weighted face normals to vertices
    for (f, f_no) in bm.faces.iter().zip(fnos) {
        let l_first = f.l_first;
        let mut l_iter = l_first;
        loop {
            // calculate the dot product of the two edges that
            // meet at the loop's vertex
            let (e1diff, e2diff) = unsafe {(
                edgevec.get_unchecked(l_iter.prev.e.head.index as usize),
                edgevec.get_unchecked(l_iter.e.head.index as usize),
            )};
            let mut dotprod = dot_v3v3(e1diff, e2diff);

            // edge vectors are calculated from e.v1 to e.v2, so
            // adjust the dot product if one but not both loops
            // actually runs from from e.v2 to e.v1
            if (l_iter.prev.e.verts[0] == l_iter.prev.v) ^ (l_iter.e.verts[0] == l_iter.v) {
                dotprod = -dotprod;
            }

            let fac = (-dotprod).acos();

            // accumulate weighted face normal into the vertex's normal
            imadd_v3_v3fl(&mut vnos[l_iter.v.head.index as usize], f_no, fac);

            l_iter = l_iter.next;
            if l_iter == l_first {
                break;
            }
        }
    }

    // normalize the accumulated vertex normals
    for (v_no, v_co) in vnos.iter_mut().zip(vcos) {
        if unlikely!(normalize_v3(v_no) == 0.0) {
            normalize_v3_v3(v_no, v_co);
        }
    }
}


pub fn BM_mesh_normals_update(
    bm: &mut BMesh,
) {
    let mut edgevec: Vec<[f64; 3]> = Vec::with_capacity(bm.edges.len());
    unsafe {
        edgevec.set_len(bm.edges.len());
    }

    // each block can be threaded here

// #pragma omp parallel sections if (bm->totvert + bm->totedge + bm->totface >= BM_OMP_LIMIT)
    {
// #pragma omp section
        {
            // calculate all face normals
            let mut i: i32 = 0;
            for mut f in bm.faces.iter_mut() {
                f.head.index = i; // set_inline
                BM_face_normal_update(f);
                i += 1;
            }
            bm.elem_index_dirty &= !BM_FACE;
        }
// #pragma omp section
        {
            // Zero out vertex normals
            let mut i: i32 = 0;
            for mut v in bm.verts.iter_mut() {
                v.head.index = i; // set_inline
                zero_v3(&mut v.no);
                i += 1;
            }
            bm.elem_index_dirty &= !BM_VERT;
        }
// #pragma omp section
        {
            // Compute normalized direction vectors for each edge.
            // Directions will be used for calculating the weights
            // of the face normals on the vertex normals.
            bm_mesh_edges_calc_vectors(bm, &mut edgevec);
        }
    }
    // end omp

    // Add weighted face normals to vertices, and normalize vert normals.
    bm_mesh_verts_calc_normals(bm, &mut edgevec);
    drop(edgevec);

}

#[allow(dead_code)]
pub fn BM_verts_calc_normal_vcos(
    bm: &mut BMesh,
    fnos: &Vec<[f64; 3]>,
    vcos: &Vec<[f64; 3]>,
    vnos: &mut Vec<[f64; 3]>,
) {
    let mut edgevec: Vec<[f64; 3]> = Vec::with_capacity(bm.edges.len());
    unsafe {
        edgevec.set_len(bm.edges.len());
    }

    // Compute normalized direction vectors for each edge.
    // Directions will be used for calculating the weights
    // of the face normals on the vertex normals.
    bm_mesh_edges_calc_vectors_vcos(bm, vcos, &mut edgevec);

    /* Add weighted face normals to vertices, and normalize vert normals. */
    bm_mesh_verts_calc_normals_vcos(bm, &mut edgevec, fnos, vcos, vnos);
    drop(edgevec);
}

#[allow(dead_code)]
fn bm_mesh_edges_sharp_tag( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_mesh_loops_calc_normals( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_mesh_loops_calc_normals_no_autosmooth( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_loops_calc_normal_vcos( /* STUB */ ) {}
#[allow(dead_code)]
fn UNUSED_bm_mdisps_space_set( /* STUB */ ) {}
#[allow(dead_code)]
pub fn bmesh_edit_begin( /* STUB */ ) {}
#[allow(dead_code)]
pub fn bmesh_edit_end( /* STUB */ ) {}

// ------------
// Index Access

/// Exposed as: BMesh.index_update()
pub fn BM_mesh_elem_index_ensure(
    bm: &mut BMesh,
    htype: BMElemType,
) {
    let htype_needed = bm.elem_index_dirty & htype;
    if htype_needed == 0 {
        return;
    }

    if (htype_needed & BM_VERT) != 0 {
        for (i, mut ele) in (0..(bm.verts.len() as i32)).zip(bm.verts.iter_mut()) {
            ele.head.index = i;
        }
        bm.elem_index_dirty &= !BM_VERT;
    }

    if (htype_needed & BM_EDGE) != 0 {
        for (i, mut ele) in (0..(bm.edges.len() as i32)).zip(bm.edges.iter_mut()) {
            ele.head.index = i;
        }
        bm.elem_index_dirty &= !BM_EDGE;
    }

    if (htype_needed & BM_FACE) != 0 {
        for (i, mut ele) in (0..(bm.faces.len() as i32)).zip(bm.faces.iter_mut()) {
            ele.head.index = i;
        }
        bm.elem_index_dirty &= !BM_FACE;
    }

    // TODO, BM_LOOP (only used in rare cases)
}

#[allow(dead_code)]
pub fn BM_vert_at_index( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_at_index( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_at_index( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_at_index_find( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_at_index_find( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_at_index_find( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_vert_at_index_find_or_table( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_at_index_find_or_table( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_face_at_index_find_or_table( /* STUB */ ) {}
#[allow(dead_code)]
fn bm_mesh_remap_cd_update( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_remap( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_rebuild( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_mesh_toolflags_set( /* STUB */ ) {}

pub fn BM_mesh_data_free(
    bm: &mut BMesh,
) {
    bm.verts.clear();
    bm.edges.clear();
    bm.loops.clear();
    bm.faces.clear();

    bm.vtable.clear();
    bm.etable.clear();
    bm.ftable.clear();
}

pub fn BM_mesh_clear(
    bm: &mut BMesh,
) {
    BM_mesh_data_free(bm);

    bm.elem_index_dirty = BMElemType(0);
    bm.elem_table_dirty = BMElemType(0);
    bm.select_mode = BM_VERT;

    /*
    CustomData_reset(bm.vdata);
    CustomData_reset(bm.edata);
    CustomData_reset(bm.ldata);
    CustomData_reset(bm.pdata);
    */
}


#[allow(dead_code)]
fn BM_mesh_elem_index_validate(
    _bm: &mut BMesh,
    _htype: BMElemType,
) {
    // STUB
    panic!();
}
