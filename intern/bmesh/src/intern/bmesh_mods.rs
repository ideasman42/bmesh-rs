// Licensed: GPL v2+
// (c) Blender Foundation
//     Campbell Barton

use prelude::*;

use math_misc::{
    sub_v3v3,
    madd_v3v3fl,
};

use intern::bmesh_core::{
    BM_face_copy,
    BM_face_kill,
    BM_face_verts_kill,
    BM_faces_join,
    BM_vert_kill,
    bmesh_kernel_join_edge_kill_vert,
    bmesh_kernel_join_vert_kill_edge,
    bmesh_kernel_loop_reverse,
    bmesh_kernel_split_edge_make_vert,
    bmesh_kernel_split_face_make_edge,
    bmesh_kernel_unglue_region_make_vert,
    bmesh_kernel_unglue_region_make_vert_multi,
    bmesh_kernel_unglue_region_make_vert_multi_isolated,
};

use intern::bmesh_construct::{
    BM_elem_attrs_copy,
};

use intern::bmesh_queries::{
    BM_edge_is_boundary,
    BM_edge_is_manifold,
    BM_edge_is_wire,
    BM_edge_other_vert_mut,
    BM_edge_share_face_check,
    BM_face_vert_share_loop_mut,
    BM_loop_is_adjacent,
    BM_vert_edge_count_ex,
    BM_vert_in_edge,
    BM_vert_is_manifold,
};

use intern::bmesh_interp::{
    BM_data_interp_from_verts,
    BM_data_interp_face_vert_edge,
    BM_face_interp_multires_ex,
    BM_loop_interp_from_face,
};

use intern::bmesh_polygon::{
    BM_face_calc_center_mean,
};

use intern::bmesh_structure::{
    bmesh_disk_count_ex,
    bmesh_disk_edge_next_mut,
};

///
/// \brief Dissolve Vert
///
/// Turns the face region surrounding a manifold vertex into a single polygon.
///
/// \par Example:
/// ```text
///          +---------+             +---------+
///          |  \   /  |             |         |
/// Before:  |    v    |      After: |         |
///          |  /   \  |             |         |
///          +---------+             +---------+
/// ```
///
/// This function can also collapse edges too
/// in cases when it cant merge into faces.
///
/// \par Example:
/// ```text
///     Before:  +----v----+      After: +---------+
/// ```
///
/// \note dissolves vert, in more situations then BM_disk_dissolve
/// (e.g. if the vert is part of a wire edge, etc).
///
pub fn BM_vert_dissolve(bm: &mut BMesh, v: BMVertMutP) -> bool {
    // logic for 3 or more is identical
    let len = BM_vert_edge_count_ex(v, 3);

    if len == 1 {
        // will kill edges too
        BM_vert_kill(bm, v);
        return true;
    } else if !BM_vert_is_manifold(v) {
        if v.e == null() {
            BM_vert_kill(bm, v);
            return true;
        } else if v.e.l == null() {
            if len == 2 {
                return BM_vert_collapse_edge(bm, v.e, v, true, true) != null_mut();
            } else {
                // used to kill the vertex here, but it may be connected to faces.
                // so better do nothing
                return false;
            }
        } else {
            return false;
        }
    } else if len == 2 && BM_vert_face_count_is_equal!(v, 1) {
        // boundary vertex on a face
        return BM_vert_collapse_edge(bm, v.e, v, true, true) != null_mut();
    } else {
        return BM_disk_dissolve(bm, v);
    }
}

///
/// Dissolves all faces around a vert, and removes it.
///
fn BM_disk_dissolve(bm: &mut BMesh, v: BMVertMutP) -> bool
{
    if !BM_vert_is_manifold(v) {
        return false;
    }

    let mut e: BMEdgeMutP = null_mut();
    let mut keepedge: BMEdgeMutP = null_mut();
    let mut baseedge: BMEdgeMutP = null_mut();
    let mut len = 0;

    if v.e != null() {
        // v.e we keep, what else
        e = v.e;
        loop {
            e = bmesh_disk_edge_next_mut(e, v);
            if !BM_edge_share_face_check(e, v.e) {
                keepedge = e;
                baseedge = v.e;
                break;
            }
            len += 1;
            if e == v.e {
                break;
            }
        }
    }

    // this code for handling 2 and 3-valence verts
    // may be totally bad
    if keepedge == null() && len == 3 {
        if false {
            // Handle specific case for three-valence.
            // Solve it by increasing valence to four.
            // this may be hackish. .  */
            let l_a = BM_face_vert_share_loop_mut(e.l.f, v);
            let l_b = {
                if e.l.v == v {
                    e.l
                } else {
                    e.l.next
                }
            };

            if BM_face_split(
                bm, e.l.f, l_a, l_b, None, false) == (null_mut(), null_mut())
            {
                return false;
            }

            if !BM_disk_dissolve(bm, v) {
                return false;
            }
        } else {
            if unlikely!(BM_faces_join_pair(
                             bm, e.l, e.l.radial_next, true) == null_mut())
            {
                return false;
            } else if unlikely!(BM_vert_collapse_faces(
                                  bm, v.e, v, 1.0, true, false, true) == null_mut())
            {
                return false;
            }
        }
        return true;
    } else if keepedge == null() && len == 2 {
        // collapse the vertex
        e = BM_vert_collapse_faces(bm, v.e, v, 1.0, true, true, true);

        if e == null() {
            return false;
        }

        // handle two-valence
        if e.l != e.l.radial_next {
            if BM_faces_join_pair(bm, e.l, e.l.radial_next, true) == null_mut() {
                return false;
            }
        }

        return true;
    }

    if keepedge != null() {
        let mut done = false;

        while done == false {
            done = true;
            e = v.e;
            loop {
                let mut f = null_mut();
                if BM_edge_is_manifold(e) && (e != baseedge) && (e != keepedge) {
                    f = BM_faces_join_pair(bm, e.l, e.l.radial_next, true);
                    // return if couldn't join faces in manifold
                    // conditions */

                    // !disabled for testing why bad things happen
                    if f == null() {
                        return false;
                    }
                }

                if f != null() {
                    done = false;
                    break;
                }
                e = bmesh_disk_edge_next_mut(e, v);
                if e == v.e {
                    break;
                }
            }
        }

        // collapse the vertex

        // note, the baseedge can be a boundary of manifold, use this as join_faces arg
        e = BM_vert_collapse_faces(
            bm, baseedge, v, 1.0, true, !BM_edge_is_boundary(baseedge), true);

        if e == null() {
            return false;
        }

        if e.l != null() {
            // get remaining two faces
            if e.l != e.l.radial_next {
                // join two remaining faces
                if BM_faces_join_pair(bm, e.l, e.l.radial_next, true) == null_mut() {
                    return false;
                }
            }
        }
    }

    return true;
}

///
/// \brief Faces Join Pair
///
/// Joins two adjacent faces together.
///
/// \note This method calls to #BM_faces_join to do its work.
/// This means connected edges which also share the two faces will be joined.
///
/// If the windings do not match the winding of the new face will follow
/// \a l_a's winding (i.e. \a l_b will be reversed before the join).
///
/// \return The combined face or NULL on failure.
///
pub fn BM_faces_join_pair(
    bm: &mut BMesh,
    l_a: BMLoopMutP,
    l_b: BMLoopMutP,
    do_del: bool,
) -> BMFaceMutP {

    debug_assert!((l_a != l_b) && (l_a.e == l_b.e));

    if l_a.v == l_b.v {
        let cd_loop_mdisp_offset = CustomData_get_offset(&bm.ldata, CD_MDISPS);
        bmesh_kernel_loop_reverse(bm, l_b.f, cd_loop_mdisp_offset, true);
    }

    return BM_faces_join(bm, &[l_a.f, l_b.f], do_del);
}

///
/// \brief Face Split
///
/// Split a face along two vertices. returns the newly made face, and sets
/// the \a r_l member to a loop in the newly created edge.
///
/// \param bm The bmesh
/// \param f the original face
/// \param l_a, l_b  Loops of this face, their vertices define
/// the split edge to be created (must be differ and not can't be adjacent in the face).
/// \param r_l pointer which will receive the BMLoop for the split edge in the new face
/// \param example Edge used for attributes of splitting edge, if non-NULL
/// \param no_double: Use an existing edge if found
///
/// \return Pointer to the newly created face representing one side of the split
/// if the split is successful (and the original original face will be the
/// other side). NULL if the split fails.
///
pub fn BM_face_split(
    bm: &mut BMesh,
    f: BMFaceMutP,
    l_a: BMLoopMutP,
    l_b: BMLoopMutP,
    e_ref: Option<BMEdgeConstP>,
    no_double: bool,
) -> (BMFaceMutP, BMLoopMutP) {

    let cd_loop_mdisp_offset = CustomData_get_offset(&bm.ldata, CD_MDISPS);

    debug_assert!(l_a != l_b);
    debug_assert!(f == l_a.f && f == l_b.f);
    debug_assert!(!BM_loop_is_adjacent(l_a, l_b));

    // could be an assert
    if unlikely!(BM_loop_is_adjacent(l_a, l_b)) ||
       unlikely!(f != l_a.f || f != l_b.f)
    {
        return (null_mut(), null_mut());
    }

    // do we have a multires layer?
    let mut f_tmp = null_mut();
    if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
        f_tmp = BM_face_copy(
            bm, &mut None, f, false, false);
    }

    let (f_new, l_new) = bmesh_kernel_split_face_make_edge(bm, f, l_a, l_b, e_ref, no_double);

    if f_new != null() {
        // handle multires update
        if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
            let f_src_center = BM_face_calc_center_mean(f_tmp);

            let f_dst_center = BM_face_calc_center_mean(f);
            BM_face_interp_multires_ex(
                bm, f, f_tmp, &f_dst_center, &f_src_center, cd_loop_mdisp_offset);

            let f_dst_center = BM_face_calc_center_mean(f_new);
            BM_face_interp_multires_ex(
                bm, f_new, f_tmp, &f_dst_center, &f_src_center, cd_loop_mdisp_offset);

            // BM_face_multires_bounds_smooth doesn't flip displacement correct
/*
            BM_face_multires_bounds_smooth(bm, f);
            BM_face_multires_bounds_smooth(bm, f_new);
*/
        }
    }

    if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
        BM_face_kill(bm, f_tmp);
    }

    return (f_new, l_new);
}

///
/// \brief Face Split with intermediate points
///
/// Like BM_face_split, but with an edge split by \a n intermediate points with given coordinates.
///
/// \param bm The bmesh
/// \param f the original face
/// \param l_a, l_b vertices which define the split edge, must be different
/// \param cos Array of coordinates for intermediate points
/// \param n Length of \a cos (must be > 0)
/// \param r_l pointer which will receive the BMLoop
/// for the first split edge (from \a l_a) in the new face.
/// \param example Edge used for attributes of splitting edge, if non-NULL
///
/// \return Pointer to the newly created face representing one side of the split
/// if the split is successful (and the original original face will be the
/// other side). NULL if the split fails.
///
#[allow(dead_code)]
pub fn BM_face_split_n( /* STUB */
    bm: &mut BMesh,
    f: BMFaceMutP,
    l_a: BMLoopMutP,
    l_b: BMLoopMutP,
    cos: &[[f64; 3]],
    e_ref: Option<BMEdgeConstP>,
) -> (BMFaceMutP, BMLoopMutP) {

    // BMVert *v_a = l_a.v; /* UNUSED */
    let v_b = l_b.v;
    // int i, j;

    debug_assert!(l_a != l_b);
    debug_assert!(f == l_a.f && f == l_b.f);
    debug_assert!(!(cos.is_empty() && BM_loop_is_adjacent(l_a, l_b)));

    /* could be an assert */
    if unlikely!(cos.is_empty() && BM_loop_is_adjacent(l_a, l_b)) ||
       unlikely!(l_a.f != l_b.f)
    {
        return (null_mut(), null_mut());
    }

    let f_tmp = BM_face_copy(bm, &mut None, f, true, true);

    let (f_new, l_new) = bmesh_kernel_split_face_make_edge(bm, f, l_a, l_b, e_ref, false);
    // bmesh_kernel_split_face_make_edge returns in 'l_new' a Loop for f_new going from 'v_a' to 'v_b'.
    // The radial_next is for 'f' and goes from 'v_b' to 'v_a'

    if f_new != null() {
        let mut e = l_new.e;
        for co in cos {

            let (mut v_new, e_new) = bmesh_kernel_split_edge_make_vert(bm, v_b, e);
            debug_assert!(v_new != null());
            // bmesh_kernel_split_edge_make_vert returns in 'e_new' the edge going from 'v_new' to 'v_b'
            v_new.co = *co;

            // interpolate the loop data for the loops with (v == v_new), using orig face
            for &e_iter in &[e, e_new] {
                let mut l_iter = e_iter.l;
                loop {
                    if l_iter.v == v_new {
                        // this interpolates both loop and vertex data
                        BM_loop_interp_from_face(bm, l_iter, f_tmp, true, true);
                    }
                    l_iter = l_iter.radial_next;
                    if l_iter == e_iter.l {
                        break;
                    }
                }
            }
            e = e_new;
        }
    }

    BM_face_verts_kill(bm, f_tmp);

    return (f_new, l_new);
}


///
/// \brief Vert Collapse Faces
///
/// Collapses vertex \a v_kill that has only two manifold edges
/// onto a vertex it shares an edge with.
/// \a fac defines the amount of interpolation for Custom Data.
///
/// \note that this is not a general edge collapse function.
///
/// \note this function is very close to #BM_vert_collapse_edge,
/// both collapse a vertex and return a new edge.
/// Except this takes a factor and merges custom data.
///
/// \param bm The bmesh
/// \param e_kill The edge to collapse
/// \param v_kill The vertex  to collapse into the edge
/// \param fac The factor along the edge
/// \param join_faces When true the faces around the vertex will be joined
/// otherwise collapse the vertex by merging the 2 edges this vert touches into one.
/// \param kill_degenerate_faces Removes faces with less than 3 verts after collapsing.
///
/// \returns The New Edge
///
pub fn BM_vert_collapse_faces(
    bm: &mut BMesh,
    e_kill: BMEdgeMutP,
    v_kill: BMVertMutP,
    fac: f64,
    do_del: bool,
    join_faces: bool,
    kill_degenerate_faces: bool,
) -> BMEdgeMutP {
    let tv = BM_edge_other_vert_mut(e_kill, v_kill);

    // Only intended to be called for 2-valence vertices
    debug_assert!(bmesh_disk_count_ex(v_kill, 3) <= 2);

    // first modify the face loop data

    if e_kill.l != null() {
        let w = [1.0 - fac, fac];

        let mut l_iter = e_kill.l;
        loop {
            if (l_iter.v == tv) && (l_iter.next.v == v_kill) {
                let tvloop = l_iter;
                let kvloop = l_iter.next;

                let src: [PtrConst<CustomDataElem>; 2] = [
                    PtrConst(kvloop.head.data),
                    PtrConst(tvloop.head.data),
                ];

                CustomData_bmesh_interp(
                    &mut bm.ldata, &src, &w[..], None, PtrMut(kvloop.head.data));
            }

            l_iter = l_iter.radial_next;
            if l_iter == e_kill.l {
                break;
            }
        }
    }

    // now interpolate the vertex data
    BM_data_interp_from_verts(bm, v_kill, tv, v_kill, fac);

    let e2: BMEdgeMutP = bmesh_disk_edge_next_mut(e_kill, v_kill);
    let tv2: BMVertMutP = BM_edge_other_vert_mut(e2, v_kill);

    let mut e_new;

    if join_faces {
        let mut faces: Vec<BMFaceMutP> = Vec::with_capacity(8);

        bm_iter_loops_of_vert_mut!(v_kill, l_iter, {
            faces.push(l_iter.f);
        });
        e_new = null_mut();

        if faces.len() >= 2 {
            let f2 = BM_faces_join(bm, &faces[..], true);
            if f2 != null() {
                if let Some(l_a) = BM_face_vert_share_loop_mut(f2, tv).as_option() {
                    if let Some(l_b) = BM_face_vert_share_loop_mut(f2, tv2).as_option() {
                        let (_f_new, l_new) = BM_face_split(bm, f2, l_a, l_b, None, false);

                        if l_new != null() {
                            e_new = l_new.e;
                        }
                    }
                }
            }
        }

        debug_assert!(faces.len() < 8);

        // free(faces)
    } else {
        // Single face or no faces.

        // Same as BM_vert_collapse_edge() however we already
        // have vars to perform this operation so don't call.
        e_new = bmesh_kernel_join_edge_kill_vert(bm, e_kill, v_kill, do_del, true, kill_degenerate_faces);
        // e_new = BM_edge_exists(tv, tv2); // same as return above
    }

    return e_new;
}

pub fn BM_vert_collapse_edge(
    bm: &mut BMesh, e_kill: BMEdgeMutP, v_kill: BMVertMutP,
    do_del: bool, kill_degenerate_faces: bool,
) -> BMEdgeMutP {
    // nice example implementation but we want loops to have their customdata
    // accounted for
    if false {
        let mut e_new: BMEdgeMutP = null_mut();

        // Collapse between 2 edges

        // in this case we want to keep all faces and not join them,
        // rather just get rid of the vertex - see bug [#28645]
        let tv: BMVertMutP = BM_edge_other_vert_mut(e_kill, v_kill);
        if tv != null() {
            let e2 = bmesh_disk_edge_next_mut(e_kill, v_kill);
            if e2 != null() {
                let tv2: BMVertMutP = BM_edge_other_vert_mut(e2, v_kill);
                if tv2 != null() {
                    // only action, other calls here only get the edge to return
                    e_new = bmesh_kernel_join_edge_kill_vert(bm, e_kill, v_kill, do_del, true, kill_degenerate_faces);
                }
            }
        }

        return e_new;
    } else {
        // with these args faces are never joined, same as above
        // but account for loop customdata
        return BM_vert_collapse_faces(
            bm, e_kill, v_kill, 1.0,
            do_del, false, kill_degenerate_faces);
    }
}

#[allow(dead_code)]
pub fn BM_edge_collapse(
    bm: &mut BMesh,
    e_kill: BMEdgeMutP,
    v_kill: BMVertMutP,
    do_del: bool,
    kill_degenerate_faces: bool
) -> BMVertMutP
{
    return bmesh_kernel_join_vert_kill_edge(bm, e_kill, v_kill, do_del, true, kill_degenerate_faces);
}

///
/// \brief Edge Split
///
/// ```text
/// Before: v
///         +-----------------------------------+
///                           e
///
/// After:  v                 v_new (returned)
///         +-----------------+-----------------+
///                 r_e                e
/// ```
///
/// \param e  The edge to split.
/// \param v  One of the vertices in \a e and defines the "from" end of the splitting operation,
/// the new vertex will be \a fac of the way from \a v to the other end.
/// \param r_e  The newly created edge.
/// \return  The new vertex.
///
pub fn BM_edge_split(
    bm: &mut BMesh,
    e: BMEdgeMutP,
    v: BMVertMutP,
    fac: f64
) -> (BMVertMutP, BMEdgeMutP) {
    use bmesh_class::bmesh_bitflag_bmelem_api_flag::{
        FLAG_OVERLAP,
    };

    let cd_loop_mdisp_offset = {
        if BM_edge_is_wire(e) {
            CD_INVALID_OFFSET
        } else {
            CustomData_get_offset(&bm.ldata, CD_MDISPS)
        }
    };

    debug_assert!(BM_vert_in_edge(e, v));

    // we need this for handling multi-res

    let mut oldfaces: Vec<BMFaceMutP> = Vec::new();

    // do we have a multi-res layer?
    if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
        bm_iter_loops_of_edge_radial!(e, l_iter, {
            oldfaces.push(l_iter.f);
        });

        // flag existing faces so we can differentiate oldfaces from new faces
        for f_old in &mut oldfaces {
            bm_elem_api_flag_enable!(*f_old, FLAG_OVERLAP);
            *f_old = BM_face_copy(
                bm, &mut None, *f_old, true, true);
            bm_elem_api_flag_disable!(*f_old, FLAG_OVERLAP);
        }
    }

    let v_other = BM_edge_other_vert_mut(e, v);

    let (mut v_new, mut e_new) = bmesh_kernel_split_edge_make_vert(bm, v, e);

    debug_assert!(v_new != null());
    debug_assert!(BM_vert_in_edge(e_new, v) &&
                  BM_vert_in_edge(e_new, v_new));
    debug_assert!(BM_vert_in_edge(e, v_new) &&
                  BM_vert_in_edge(e, v_other));

    v_new.co = sub_v3v3(&v_other.co, &v.co);
    v_new.co = madd_v3v3fl(&v.co, &v_new.co, fac);

    e_new.head.hflag = e.head.hflag;
    BM_elem_attrs_copy(&mut None, bm, e.as_elem(), e_new.as_elem());

    // v.v_new.v2
    BM_data_interp_face_vert_edge(bm, v_other, v, v_new, e, fac);
    BM_data_interp_from_verts(bm, v, v_other, v_new, fac);

    if cd_loop_mdisp_offset != CD_INVALID_OFFSET {
        // interpolate new/changed loop data from copied old faces
        for f_old in &oldfaces {
            let f_center_old = BM_face_calc_center_mean(*f_old);

            for e1 in &[e, e_new] {
                let mut l = e1.l;

                if unlikely!(l == null()) {
                    debug_assert!(false);
                    break;
                }

                loop {
                    // check this is an old face
                    if bm_elem_api_flag_test!(l.f, FLAG_OVERLAP) {
                        let f_center = BM_face_calc_center_mean(l.f);
                        BM_face_interp_multires_ex(
                            bm, l.f, *f_old,
                            &f_center, &f_center_old,
                            cd_loop_mdisp_offset);
                    }

                    l = l.radial_next;
                    if l == e1.l {
                        break;
                    }
                }
            }
        }

        // destroy the old faces
        for f_old in &oldfaces {
            BM_face_verts_kill(bm, *f_old);
        }

        // fix boundaries a bit, doesn't work too well quite yet
/*
        for (j = 0; j < 2; j++) {
            BMEdge *e1 = j ? e_new : e;
            BMLoop *l, *l2;

            l = e1.l;
            if (unlikely!(!l)) {
                BMESH_ASSERT(0);
                break;
            }

            do {
                BM_face_multires_bounds_smooth(bm, l.f);
                l = l.radial_next;
            } while (l != e1.l);
        }
*/

        // free oldfaces
    }

    return (v_new, e_new);
}

#[allow(dead_code)]
pub fn BM_edge_split_n( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_calc_rotate( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_rotate_check( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_rotate_check_degenerate( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_rotate_check_beauty( /* STUB */ ) {}
#[allow(dead_code)]
pub fn BM_edge_rotate( /* STUB */ ) {}

///
/// \brief Rip a single face from a vertex fan
///

pub fn BM_face_loop_separate(
    bm: &mut BMesh,
    l_sep: BMLoopMutP,
) -> BMVertMutP {
    return bmesh_kernel_unglue_region_make_vert(bm, l_sep);
}

#[allow(dead_code)]
pub fn BM_face_loop_separate_multi_isolated(
    bm: &mut BMesh,
    l_sep: BMLoopMutP,
) -> BMVertMutP {
    return bmesh_kernel_unglue_region_make_vert_multi_isolated(bm, l_sep);
}

pub fn BM_face_loop_separate_multi(
    bm: &mut BMesh,
    larr: &[BMLoopMutP],
    ) -> BMVertMutP {
    return bmesh_kernel_unglue_region_make_vert_multi(bm, larr);
}

