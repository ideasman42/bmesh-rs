// Licensed: Apache 2.0

macro_rules! bm_elem_cd_get_void_p {
    ($e:expr, $t:ty, $offset:expr) => {
        {
            unsafe {
                &mut *(($e.head.data.offset($offset) as *mut u8) as *mut $t)
            }
        }
    }
}

