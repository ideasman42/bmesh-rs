// Licensed: Apache 2.0

/// Basic font drawing,
/// uses line drawing so the font can scale
/// for quick scaleale font drawing.
///
/// May want to use free-type eventually.
///


extern crate pxbuf;
extern crate pxbuf_draw;

use pxbuf_draw::PxBufDraw;

mod font_data;

// TODO, expose multiple fonts
use ::std::ops::Range;

pub struct PxBufFont {
    pub width: i32,
    pub height: i32,
    pub ascent: i32,
    pub descent: i32,

    edges: Vec<[[i8; 2]; 2]>,
    chars: Vec<Range<usize>>,
}

impl PxBufFont {
    pub fn new_fixed_12() -> PxBufFont {
        return font_data::fixed_width_12::new();
    }
}

impl PxBufFont {
    pub fn draw_char(
        &self,
        pxbuf: &mut pxbuf::PxBuf,
        color: u32,
        co: &[i32; 2],
        scale: i32,
        ch: char,
    ) {
        let a = &self.chars[ch as usize];
        for e in &self.edges[a.start..a.end] {
            pxbuf.draw_line_width_clip(
                color,
                &[co[0] + e[0][0] as i32 * scale, co[1] + e[0][1] as i32 * scale],
                &[co[0] + e[1][0] as i32 * scale, co[1] + e[1][1] as i32 * scale],
                scale,
            );
        }
    }

    pub fn draw_string(
        &self,
        pxbuf: &mut pxbuf::PxBuf,
        color: u32,
        co: &[i32; 2],
        scale: i32,
        text: &str,
    ) -> i32 {
        let char_width_scale = self.width * scale;
        let mut co_step = [co[0], co[1]];
        for ch in text.chars() {
            self.draw_char(pxbuf, color, &co_step, scale, ch);
            co_step[0] += char_width_scale;
        }
        return co_step[0];
    }

    /// Returns cursor position and number of lines.
    pub fn draw_string_wrap(
        &self,
        pxbuf: &mut pxbuf::PxBuf,
        color: u32,
        co: &[i32; 2],
        scale: i32,
        text: &str,
        wrap_width: i32,
    ) -> (i32, i32) {
        // TODO, wrap on whitespace boundaries.
        let char_width_scale = self.width * scale;
        let mut co_step = [co[0], co[1]];
        // wrap vars
        let mut lines = 1;
        let wrap_width_offset = co[0] + wrap_width;
        for ch in text.chars() {
            let mut co_step_x_next = co_step[0] + char_width_scale;
            if co_step_x_next >= wrap_width_offset {
                co_step[0] = co[0];
                co_step[1] -= self.height;
                co_step_x_next = co_step[0] + char_width_scale;
                lines += 1;
            }
            self.draw_char(pxbuf, color, &co_step, scale, ch);
            co_step[0] = co_step_x_next;
        }
        self.height;  // UNUSED

        return (co_step[0], lines);
    }
}

