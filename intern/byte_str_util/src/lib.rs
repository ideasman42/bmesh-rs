// Licensed: Apache 2.0

///
/// Copy a string to a fixed-size byte-array without truncating utf8 characters.
///
/// Note: differs from C's `strlcpy` since it returns the number of bytes written to
/// (including the NULL terminator).
///
pub fn strlcpy_str_utf8(utf8_dst: &mut [u8], str_src: &str) -> usize {
    let utf8_dst_len = utf8_dst.len();
    if utf8_dst_len == 0 {
        return 0;
    }
    let mut last_index = str_src.len();
    if last_index >= utf8_dst_len {
        // truncate if 'str_src' is too long
        last_index = utf8_dst_len - 1;
        // no need to check last_index > 0 here,
        // is_char_boundary covers that case
        while !str_src.is_char_boundary(last_index) {
            last_index -= 1;
        }
    }
    utf8_dst[0..last_index].clone_from_slice(&str_src.as_bytes()[0..last_index]);
    utf8_dst[last_index] = 0;
    return last_index + 1;
}

/// Put an extension on a fixed sized buffer,
/// truncating the end of the 'head' to make room for the 'tail' as needed.
///
/// Returns the number of bytes written to.
pub fn strlext_str_utf8(utf8_dst: &mut [u8], head: &str, tail: &str) -> usize {
    if utf8_dst.len() == 0 {
        return 0;
    }
    let dst_len = utf8_dst.len();
    let mut head_len = head.len();
    let tail_len = tail.len();
    let mut head_tail_len = head_len + tail_len;
    let ret: usize;
    if tail_len < dst_len {
        if head_tail_len < dst_len {
            utf8_dst[0..head_len].clone_from_slice(head.as_bytes());
        } else {
            head_len = strlcpy_str_utf8(&mut utf8_dst[0..(dst_len - tail_len)], head) - 1;
            head_tail_len = head_len + tail_len;
        }
        utf8_dst[head_len..head_tail_len].clone_from_slice(tail.as_bytes());
        utf8_dst[head_tail_len] = 0;
        ret = head_tail_len + 1;
    } else {
        // only tail
        ret = strlcpy_str_utf8(utf8_dst, tail);
    }
    debug_assert!(utf8_dst[ret - 1] == 0);
    return ret;
}

pub fn strlen(byte_str: &[u8]) -> usize {
    let mut len = 0_usize;
    for b in byte_str {
        if *b == 0 {
            break;
        }
        len += 1;
    }
    debug_assert!(byte_str[len] == 0);
    return len;
}

pub unsafe fn str_from_u8_nul_utf8_unchecked(utf8_src: &[u8]) -> &str {
    // currently the closure isn't optimized out, so use a regular loop
    let mut nul_range_end = 1_usize;
    for b in utf8_src {
        if *b == 0 {
            break;
        }
        nul_range_end += 1;
    }
    debug_assert!(utf8_src[nul_range_end] == 0);
    return ::std::str::from_utf8_unchecked(&utf8_src[0..nul_range_end]);
}
