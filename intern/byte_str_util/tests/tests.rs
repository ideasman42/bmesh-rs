// Licensed: Apache 2.0

extern crate byte_str_util;

use byte_str_util::{
    strlen,
};

fn as_str(buf: &[u8]) -> &str {
    return ::std::str::from_utf8(&buf[0..strlen(&buf)]).unwrap();
}

mod strlext {

    macro_rules! run {
        ($buf_size:expr, $head:expr, $tail:expr => $expected_output:expr) => {
            {
                let head = $head;
                let tail = $tail;
                let mut buf: [u8; $buf_size] = [0; $buf_size];
                ::byte_str_util::strlext_str_utf8(&mut buf, head, tail);
                assert_eq!(super::as_str(&buf), $expected_output);
            }
        }
    }

    #[test]
    fn strlext_1() {
        run!(1, "", "" => "");
        run!(1, "a", "" => "");
        run!(1, "", "b" => "");
        run!(1, "a", "b" => "");
        run!(1, "abc", "def" => "");
    }

    #[test]
    fn strlext_2() {
        run!(2, "", "" => "");
        run!(2, "a", "" => "a");
        run!(2, "", "b" => "b");
        run!(2, "a", "b" => "b");
        run!(2, "", "def" => "d");
        run!(2, "abc", "def" => "d");
    }

    #[test]
    fn strlext_3() {
        run!(3, "ab", "" => "ab");
        run!(3, "ab", "d" => "ad");
        run!(3, "abc", "" => "ab");
        run!(3, "abc", "d" => "ad");
        run!(3, "", "def" => "de");
        run!(3, "abc", "def" => "de");
    }

    #[test]
    fn strlext_misc() {
        run!(6, "hello ", "world" => "world");
        run!(7, "hello ", "world" => "hworld");
        run!(11, "hello ", "world" => "helloworld");
        run!(12, "hello ", "world" => "hello world");
        run!(13, "hello ", "world" => "hello world");
    }

    /* Using chars:
     * ƒ𝘍→⇒≥≤≡≢ ‥…×÷∅
     */
    #[test]
    fn strlext_utf8() {
        run!(1, "ƒ", "𝘍" => "");
        run!(2, "ƒ", "𝘍" => "");
        run!(3, "ƒ", "𝘍" => "");
        run!(4, "ƒ", "𝘍" => "");
        run!(5, "ƒ", "𝘍" => "𝘍");
        run!(6, "ƒ", "𝘍" => "𝘍");
        run!(7, "ƒ", "𝘍" => "ƒ𝘍");

        run!(7, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢");
        run!(8, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢");
        run!(9, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢");

        run!(10, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥");
        run!(11, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥");
        run!(12, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥");

        run!(13, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…");
        run!(14, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…");

        run!(15, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×");
        run!(16, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×");

        run!(17, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×÷");
        run!(18, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×÷");
        run!(19, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×÷");

        run!(20, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×÷∅");
        run!(21, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "≡≢‥…×÷∅");

        // include the head now
        run!(22, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ≡≢‥…×÷∅");
        run!(23, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ≡≢‥…×÷∅");
        run!(24, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ≡≢‥…×÷∅");
        run!(25, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ≡≢‥…×÷∅");
        run!(26, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ𝘍≡≢‥…×÷∅");

        // skip until near the end

        // snip a single char
        run!(35, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ𝘍→⇒≥≡≢‥…×÷∅");
        run!(36, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ𝘍→⇒≥≡≢‥…×÷∅");
        run!(37, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ𝘍→⇒≥≡≢‥…×÷∅");

        // full string
        run!(38, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ𝘍→⇒≥≤≡≢‥…×÷∅");
        run!(39, "ƒ𝘍→⇒≥≤", "≡≢‥…×÷∅" => "ƒ𝘍→⇒≥≤≡≢‥…×÷∅");
    }
}

