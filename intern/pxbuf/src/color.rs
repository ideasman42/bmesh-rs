// Licensed: Apache 2.0

#[macro_export]
macro_rules! rgba_as_u32 {
    ($r:expr, $g:expr, $b:expr, $a:expr) => {
        {
            debug_assert!($r <= 0xff && $g <= 0xff && $b <= 0xff && $a <= 0xff);
            ($r | $g << 8 | $b << 16 | $a << 24)
        }
    }
}

#[macro_export]
macro_rules! rgb_as_u32 {
    ($r:expr, $g:expr, $b:expr) => {
        {
            debug_assert!($r <= 0xff && $g <= 0xff && $b <= 0xff);
            ($r | $g << 8 | $b << 16 | 0xff << 24)
        }
    }
}

#[macro_export]
macro_rules! rgba_hex_u32 {
    ($hex:expr) => {
        {
            let hex: u32 = $hex;
            hex.to_be()
        }
    }
}

#[macro_export]
macro_rules! rgb_hex_u32 {
    ($hex:expr) => {
        {
            let hex: u32 = $hex;
            (0x00_00_00_FF_u32.to_le() | (hex << 8)).to_be()
        }
    }
}

#[inline]
pub fn rgba_chan_value_u32(color: u32, chan: usize) -> u32 {
    if cfg!(target_endian = "big") {
        return {
            match chan {
                3 => { (color)       & 0xFF },
                2 => { (color >>  8) & 0xFF },
                1 => { (color >> 16) & 0xFF },
                0 => { (color >> 24) },
                _ => { unreachable!(); },
            }
        };
    } else {
        return {
            match chan {
                0 => { (color)       & 0xFF },
                1 => { (color >>  8) & 0xFF },
                2 => { (color >> 16) & 0xFF },
                3 => { (color >> 24) },
                _ => { unreachable!(); },
            }
        };
    }
}

#[inline]
pub fn rgba_chan_value_u8(color: u32, chan: usize) -> u8 {
    return rgba_chan_value_u32(color, chan) as u8;
}


pub fn rgba_interp(src: u32, dst: u32, t: u32) -> u32 {
    debug_assert!(t <= 255);
    let s = 255 - t;
    return
        (((((src >> 0)  & 0xff).wrapping_mul(s) +
           ((dst >> 0)  & 0xff).wrapping_mul(t)) >> 8)) |
        (((((src >> 8)  & 0xff).wrapping_mul(s) +
           ((dst >> 8)  & 0xff).wrapping_mul(t))     )  & !0xff) |
        (((((src >> 16) & 0xff).wrapping_mul(s) +
           ((dst >> 16) & 0xff).wrapping_mul(t)) << 8)  & !0xffff) |
        (((((src >> 24) & 0xff).wrapping_mul(s) +
           ((dst >> 24) & 0xff).wrapping_mul(t)) << 16) & !0xffffff)
    ;
}

pub fn rgb_interp(src: u32, dst: u32, t: u32) -> u32 {
    debug_assert!(t <= 255);
    let s = 255 - t;
    if cfg!(target_endian = "big") {
        return
            (((((src >> 24) & 0xff).wrapping_mul(s) +
               ((dst >> 24) & 0xff).wrapping_mul(t)) << 16) & !0xffffff) |
            (((((src >> 16) & 0xff).wrapping_mul(s) +
               ((dst >> 16) & 0xff).wrapping_mul(t)) << 8)  & !0xffff) |
            (((((src >> 8)  & 0xff).wrapping_mul(s) +
               ((dst >> 8)  & 0xff).wrapping_mul(t))     )  & !0xff) |
            0xff
        ;
    } else {
        return
            (((((src >> 0)  & 0xff).wrapping_mul(s) +
               ((dst >> 0)  & 0xff).wrapping_mul(t)) >> 8)) |
            (((((src >> 8)  & 0xff).wrapping_mul(s) +
               ((dst >> 8)  & 0xff).wrapping_mul(t))     )  & !0xff) |
            (((((src >> 16) & 0xff).wrapping_mul(s) +
               ((dst >> 16) & 0xff).wrapping_mul(t)) << 8)  & !0xffff) |
            0xff000000
        ;
    }
}
