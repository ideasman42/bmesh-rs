// Licensed: Apache 2.0

/// Minimal pixel manipulation library
/// This is mainly to have convenient access to an RGBA buffer,
/// and should not add high-level image processing abstractions.

pub mod color;
pub mod util;

// ----------------------------------------------------------------------------
// PxBuf API

pub struct PxBuf {
    size: [i32; 2],
    rect: Vec<u32>,
}

impl PxBuf {
    pub fn new_empty() -> PxBuf {
        let buf = vec![0; 0];
        PxBuf {
            size: [0, 0],
            rect: buf,
        }
    }
    pub fn new(size: &[i32; 2], color: u32) -> Self {
        debug_assert!(size[0] >= 0 && size[1] >= 0);
        let buf_size = (size[0] * size[1]) as usize;
        let buf = vec![color; buf_size];
        PxBuf {
            size: *size,
            rect: buf,
        }
    }

    pub fn new_uninitialized(size: &[i32; 2]) -> PxBuf {
        debug_assert!(size[0] >= 0 && size[1] >= 0);
        let buf_size = (size[0] * size[1]) as usize;
        let buf = {
            let mut buf = Vec::with_capacity(buf_size);
            // The worst that happens is uninitialized pixels look bad,
            // (not all that unsafe), caller is responsible for filling them in.
            unsafe {
                buf.set_len(buf_size);
            }
            buf
        };
        PxBuf {
            size: *size,
            rect: buf,
        }
    }

    pub fn reshape(&mut self, size: &[i32; 2], color: u32) {
        use ::std::cmp::Ordering;

        let buf_size_old = self.rect.len();
        let buf_size_new = (size[0] * size[1]) as usize;
        self.size = *size;

        match buf_size_new.cmp(&buf_size_old) {
            Ordering::Equal => {},
            Ordering::Greater => {
                self.rect.resize(buf_size_new, color);
            },
            Ordering::Less => {
                self.rect.truncate(buf_size_new);
                // not essential, but may save some memory longer term.
                self.rect.shrink_to_fit();
            }
        }
    }

    /// Reshape, new pixels are uninitialized!
    pub fn reshape_uninitialized(&mut self, size: &[i32; 2]) {
        use ::std::cmp::Ordering;

        let buf_size_old = self.rect.len();
        let buf_size_new = (size[0] * size[1]) as usize;
        self.size = *size;

        match buf_size_new.cmp(&buf_size_old) {
            Ordering::Equal => {},
            Ordering::Greater => {
                self.rect.reserve_exact(buf_size_new - buf_size_old);
                unsafe {
                    self.rect.set_len(buf_size_new);
                }
            },
            Ordering::Less => {
                self.rect.truncate(buf_size_new);
                // not essential, but may save some memory longer term.
                self.rect.shrink_to_fit();
            }
        }
    }

    pub fn clone_from(&mut self, offset: &[i32; 2], other: &PxBuf) {
        // trivial case
        if offset[0] == 0 &&
           offset[1] == 0 &&
           self.size == other.size
        {
            self.rect.clone_from(&other.rect);
        } else {
            if offset[0] >= 0 &&
               offset[1] >= 0 &&
               offset[0] + other.size[0] <= self.size[0] &&
               offset[1] + other.size[1] <= self.size[1]
            {
                let self_size_x = self.size[0] as usize;
                let other_size_x = other.size[0] as usize;
                // we're within bounds!
                // simple copy
                let mut self_offset = (offset[1] as usize * self_size_x) + offset[0] as usize;
                for other_row in other.rect.as_slice().chunks(other_size_x) {
                    let self_offset_next = self_offset + self_size_x;
                    self.rect[
                        self_offset..(self_offset + other_size_x)
                    ].clone_from_slice(other_row);
                    self_offset = self_offset_next;
                }

            } else {
                // TODO: support clipped cloning!
                // (Not needed currently, but should support eventually).
                panic!();
            }
        }
    }

    #[inline]
    pub fn clear(&mut self, color: u32) {
        for px in &mut self.rect {
            *px = color;
        }
    }

    #[inline]
    pub fn as_slice_u32(&self) -> &[u32] {
        return &self.rect[..];
    }

    #[inline]
    pub fn as_mut_slice_u32(&mut self) -> &mut [u32] {
        return &mut self.rect[..];
    }

    #[inline]
    pub fn as_slice_u8(&self) -> &[u8] {
        return unsafe {
            use ::std::slice;
            slice::from_raw_parts(
                self.as_slice_u32().as_ptr() as *const u8,
                ((self.size[0] * self.size[1]) * 4) as usize,
            )
        };

    }

    #[inline]
    pub fn as_mut_slice_u8(&mut self) -> &mut [u8] {
        return unsafe {
            use ::std::slice;
            slice::from_raw_parts_mut(
                self.as_mut_slice_u32().as_mut_ptr() as *mut u8,
                ((self.size[0] * self.size[1]) * 4) as usize,
            )
        };
    }

    #[inline]
    pub fn size(&self) -> &[i32; 2] {
        return &self.size;
    }

    #[inline]
    pub fn rect(&self) -> &[u32] {
        return self.rect.as_slice();
    }

    #[inline]
    pub fn rect_mut(&mut self) -> &mut [u32] {
        return self.rect.as_mut_slice();
    }

    /// Drawing is done outside this crate,
    /// just include as a fast inline function for drawing functions to use.
    #[inline]
    pub fn pixel_set(&mut self, color: u32, pos: &[i32; 2]) {
        unsafe {
            *self.rect.get_unchecked_mut(util::index_from_pos(pos, &self.size)) = color;
        }
    }
    #[inline]
    pub fn pixel_get_mut(&mut self, pos: &[i32; 2]) -> &mut u32 {
        unsafe {
            return self.rect.get_unchecked_mut(util::index_from_pos(pos, &self.size));
        }
    }
    #[inline]
    pub fn pixel_get_ref(&self, pos: &[i32; 2]) -> &u32 {
        unsafe {
            return self.rect.get_unchecked(util::index_from_pos(pos, &self.size));
        }
    }

    #[inline]
    /// 'x_end' is _not_ inclusive
    pub fn pixel_xslice_get_mut(&mut self, x_start: i32, x_end: i32, y: i32) -> &mut [u32] {
        return &mut self.rect[util::xrange_from_pos(x_start, x_end, y, &self.size)];
    }

    #[inline]
    pub fn pixel_xslice_get_ref(&self, x_start: i32, x_end: i32, y: i32) -> &[u32] {
        return &self.rect[util::xrange_from_pos(x_start, x_end, y, &self.size)];
    }

}

