// Licensed: Apache 2.0

use ::std::ops::{
    Range,
};

#[inline]
pub fn index_from_pos(pos: &[i32; 2], size: &[i32; 2]) -> usize {
    debug_assert!(pos[0] >= 0 && pos[0] < size[0] &&
                  pos[1] >= 0 && pos[1] < size[1]);
    return ((size[0] * pos[1]) + pos[0]) as usize;
}

#[inline]
pub fn xrange_from_pos(x_start: i32, x_end: i32, y: i32, size: &[i32; 2]) -> Range<usize> {
    debug_assert!(x_start <= x_end &&
                  x_start >= 0 && x_end <= size[0] &&
                  y >= 0 && y < size[1]);
    let y = size[0] * y;
    return Range {
        start: (y + x_start) as usize,
        end:   (y + x_end) as usize,
    };
}
