extern crate bmesh;

use bmesh::prelude::*;
use bmesh::primitives::{
    BMeshPartialGeom,
};

fn main() {
    let segs = 10;

    let mut bm = BMesh::new();
    let mut geom = BMeshPartialGeom::default();
    bmesh::primitives::circle(&mut bm, 1.0, segs, true, true, &mut geom);

    if let Some(ref f) = geom.faces {
        let area = f[0].calc_area();
        println!("Face Area {}!", area);
    }
}
