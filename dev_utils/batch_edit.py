#!/usr/bin/env python3
# Apache License, Version 2.0
# <pep8 compliant>

import os
# if you want to operate on a subdir, eg: "intern"
SUBDIR = ""
CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
RUST_DIR = os.path.normpath(os.path.join(CURRENT_DIR, "..", SUBDIR))


def rust_files(path):
    for dirpath, dirnames, filenames in os.walk(path):
        # skip '.git'
        dirnames[:] = [d for d in dirnames if not d.startswith(".")]
        for filename in filenames:
            if filename.startswith("."):
                continue
            ext = os.path.splitext(filename)[1]
            if ext.lower() == ".rs":
                yield os.path.join(dirpath, filename)


def main():
    for fn in rust_files(RUST_DIR):
        with open(fn, "r", encoding="utf-8") as f:
            data_src = f.read()
            data_dst = operation(fn, data_src)

        if data_dst is None or (data_src == data_dst):
            continue

        with open(fn, "w", encoding="utf-8") as f:
            data_src = f.write(data_dst)


# ---------------------------------------
# Custom Code - do whatever you like here
# (only commit reusable examples).
#
# Functions take a (file_name, file_contents)
# returns the new file_contents (or None to skip the operation)


def strip_trailing_space(fn, data_src):
    """
    Strips trailing whitespace
    """

    lines = data_src.split("\n")

    for i, l in enumerate(lines):
        l = l.rstrip()
        lines[i] = l

    data_dst = "\n".join(lines)

    # 2 empty lines max
    data_dst = data_dst.replace("\n\n\n\n", "\n\n\n")
    return data_dst


def find_and_replace(fn, data_src):
    """
    Simply finds and replaces text
    """
    # handy for replacing exact words
    use_whole_words = False

    find_replace_pairs = (
        ("OpStateType", "OpState"),
        )


    if use_whole_words:
        import re
        find_replace_pairs_re = [
                (re.compile("\b" + src + "\b"), dst)
                for src, dst in find_replace_pairs
                ]

    lines = data_src.split("\n")

    for i, l in enumerate(lines):
        old_l = l
        do_replace = False
        if use_whole_words:
            for src_re, dst in find_replace_pairs_re:
                l = re.sub(src_re, dst, l)
        else:
            for src, dst in find_replace_pairs:
                l = l.replace(src, dst)

        if old_l != l:
            lines[i] = l
            # print ("Replaced:", old_l, "\n    With:", l, "\n")

    data_dst = "\n".join(lines)
    return data_dst



# define the operation to call
operation = find_and_replace


if __name__ == "__main__":
    main()
