#!/bin/bash

# Utility that watches files and builds,
# running an editor on the first warning/error.
#
# eg:
#    BUILD_DIR=app_engine ./dev_utils/cargo-watch-edit.sh run
#

# set -x

if [[ -z $CARGO_BIN ]]; then
	CARGO_BIN=cargo
fi

# support other build dir
if [[ -z $BUILD_DIR ]]; then
	BUILD_DIR=$PWD
fi
cd "$BUILD_DIR"
unset BUILD_DIR

if [ ! $# -eq 0 ]; then
	CARGO_ARGS=${*:1}
else
	CARGO_ARGS=$'run'
	echo "No arguments supplied, using 'run'."
fi

# Find outer-most directory we should watch for changes
function find_root() {
	PARENT=""
	GRANDPARENT="../"
	unset PROJECT_ROOT
	while [ ! -e "$GRANDPARENT/.git" ] && [ "$GRANDPARENT" != "/" ] ; do
		PARENT=$GRANDPARENT
		GRANDPARENT="$(cd $PARENT/..; pwd)"
		# echo $GRANDPARENT
	done
	if [ ! -z "$GRANDPARENT" ] && [ "$GRANDPARENT" != "/" ] ; then
		PROJECT_ROOT=$GRANDPARENT
	fi
	unset PARENT
	unset GRANDPARENT
}

find_root

OUT="/tmp/cargo.txt"

tput reset
# export RUSTFLAGS="$RUSTFLAGS -A dead_code --verbose"
export RUSTFLAGS="$RUSTFLAGS --verbose"

while inotifywait -e modify `find "$PROJECT_ROOT" -name "*.rs" ! -name ".*"` || true; do
	tput reset
	rm -f "$OUT"

	# test requires different build args
	if [[ "$CARGO_ARGS" == *"test"* ]]; then
		CARGO_BUILD_ARGS="test --no-run"
	else
		CARGO_BUILD_ARGS="build"
	fi

	RUST_BACKTRACE=0 \
		$CARGO_BIN $CARGO_BUILD_ARGS --color=always 2>&1 | tee "$OUT"
	CARGO_STATUS=${PIPESTATUS[0]}

	FILE_LINE=$(
		cat "$OUT" |
		pcregrep -o1 "\-\-> ([^\s]+)" |
		head -n 1)

	rm -f "$OUT"

	# echo "Opening $FILE_LINE"
	if [ ! -z "$FILE_LINE" ]; then
		# use '&' since this might open a new session
		ce "$FILE_LINE" &
	fi

	if [ $CARGO_STATUS -eq 0 ]; then
		RUST_BACKTRACE=1 \
			$CARGO_BIN $CARGO_ARGS --color=always
	fi
done
