#!/usr/bin/env python3

import os
import sys

VERBOSE = os.environ.get("VERBOSE", False)

def run(cmd):
    if VERBOSE:
        print(">>> ", " ".join(cmd))
    import subprocess
    proc = subprocess.Popen(
        " ".join(cmd),
        #  cmd,
        shell=True,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )

    result = proc.stdout.read()
    return result

CURRENT_DIR = os.path.dirname(__file__)
BLENDER_SOURCE = os.environ.get("BLENDER_SOURCE", os.path.join(CURRENT_DIR, "../../blender"))

source_files = (
    # "source/blender/bmesh/intern/bmesh_callback_generic.c",
    "source/blender/bmesh/intern/bmesh_construct.c",
    "source/blender/bmesh/intern/bmesh_core.c",
    # "source/blender/bmesh/intern/bmesh_delete.c",
    # "source/blender/bmesh/intern/bmesh_edgeloop.c",
    "source/blender/bmesh/intern/bmesh_interp.c",
    # "source/blender/bmesh/intern/bmesh_iterators.c",
    # "source/blender/bmesh/intern/bmesh_log.c",
    "source/blender/bmesh/intern/bmesh_marking.c",
    "source/blender/bmesh/intern/bmesh_mesh.c",
    # "source/blender/bmesh/intern/bmesh_mesh_conv.c",
    "source/blender/bmesh/intern/bmesh_mesh_validate.c",
    "source/blender/bmesh/intern/bmesh_mods.c",
    # "source/blender/bmesh/intern/bmesh_opdefines.c",
    # "source/blender/bmesh/intern/bmesh_operators.c",
    "source/blender/bmesh/intern/bmesh_polygon.c",
    # "source/blender/bmesh/intern/bmesh_polygon_edgenet.c",
    "source/blender/bmesh/intern/bmesh_queries.c",
    "source/blender/bmesh/intern/bmesh_structure.c",
    # "source/blender/bmesh/intern/bmesh_walkers.c",
    # "source/blender/bmesh/intern/bmesh_walkers_impl.c",
)

source_includes = (
    "intern/guardedalloc",
    "source/blender/blenlib",
    "source/blender/blenkernel",
    "source/blender/makesdna",
    "source/blender/bmesh",
    "source/blender/blentranslation",
    "extern/rangetree",
)

source_defines = (
    "__FLT_EPSILON__",
    "__FLT_MAX__",
    "__COVERITY__",
)

def main():
    root = BLENDER_SOURCE
    for source in source_files:
        source_full = os.path.join(root, source)


        if os.path.exists("tags"):
            os.unlink("tags")

        run([
            "ctags",
            "-x",
            "--c-kinds=fp",
            source_full,
        ] + [("-I" + os.path.join(root, s)) for s in source_includes] +
            [("-D" + s) for s in source_defines]
        )
        print("\n/// " + os.path.basename(source))

        def line_number(d):
            return int(d[2].strip(";\""))

        with open("tags", "r") as f:
            lines = [
                l.split() for l in f
                if not l.startswith("!")
                if l[3] not in {"d"}]
            lines.sort(key=line_number)

            func_ls = []
            func_dict = {}
            for l in lines:
                func_dict.setdefault(l[0], []).append(l)

            for l in lines:
                if source_full == l[1]:
                    func = l[0]
                    func_ls.append(func)

            # slow, but it doesn't matter :)
            funcs_pub = set()
            for f in func_ls:
                for l in func_dict[f]:
                    if l[1].endswith(".h"):
                        funcs_pub.add(f)
                        break

            #  print(func_ls)
            for f in func_ls:
                is_pub = f in funcs_pub

                l = "fn " + f
                if is_pub:
                    l = "pub " + l

                # TODO, for now do manually
                args = " /* STUB */ "
                ret = ""

                l += "(" + args + ") {" + ret + "}"

                print(l)

    if os.path.exists("tags"):
        os.unlink("tags")

if __name__ == "__main__":
    main()

