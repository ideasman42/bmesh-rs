// Licensed: Apache 2.0

use std::ptr;
use MemPool;
use MemPoolElemUtils;

struct TestElem {
    value: usize,
    link: *mut TestElem,
}

impl MemPoolElemUtils for TestElem {
    fn default_chunk_size() -> usize {
        panic!();
    }
    fn free_ptr_get(&self) -> *mut TestElem {
        return self.link;
    }
    fn free_ptr_set(&mut self, ptr: *mut TestElem) {
        self.link = ptr;
        self.value = usize::max_value();
    }
    fn free_ptr_test(&self) -> bool {
        return self.value == usize::max_value();
    }
}

impl Default for TestElem {
    fn default() -> Self {
        TestElem {
            value: 0,
            link: ptr::null_mut(),
        }
    }
}

fn fill_range(
    pool: &mut MemPool<TestElem>,
    start: usize,
    total: usize,
) -> *mut TestElem {
    let mut elem = unsafe { pool.alloc_elem_uninitialized() };
    unsafe {
        (*elem).value = start;
    }
    for i in 1..total {
        let elem_next = unsafe { pool.alloc_elem_uninitialized() };
        let elem_prev = elem;
        elem = elem_next;
        unsafe {
            (*elem).value = start + i;
            (*elem).link = elem_prev;
        }
    }
    return elem;
}

#[test]
fn test_mempool() {
    let total = 128;
    let chunk_size = 2;
    let mut pool: MemPool<TestElem> = MemPool::with_chunk_size(chunk_size);

    for _ in 0..2 {
        let mut elem = fill_range(&mut pool, 0, total);

        assert!(pool.len() == total);

        for i in (0..total).rev() {
            unsafe {
                assert!((*elem).value == i);
                let elem_next = (*elem).link;
                pool.free_elem(elem);
                elem = elem_next;
            }
        }

        assert!(pool.is_empty());
    }
}

#[test]
fn test_mempool_iter() {
    let total = 128;
    let chunk_size = 2;
    let mut pool: MemPool<TestElem> = MemPool::with_chunk_size(chunk_size);
    let _elem_first = fill_range(&mut pool, 0, total);

    let mut v = pool.as_vec_mut();

    for i in (0..total).rev() {
        if unsafe { (*v[i]).value } % 2 == 0 {
            pool.free_elem(v[i]);
        }
    }
    assert!(pool.len() == total / 2);
    v.clear();

    // vector version
    let v = pool.as_vec_mut();
    let mut i = 1;
    for elem in v {
        unsafe {
            assert!((*elem).value % 2 != 0);
            assert!((*elem).value == i);
            i += 2;
        }
    }

    // iterator version
    let mut i = 1;
    for elem in pool.iter() {
        assert!((*elem).value % 2 != 0);
        assert!((*elem).value == i);
        i += 2;
    }
}

