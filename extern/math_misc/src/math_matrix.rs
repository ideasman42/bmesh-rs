// Licensed: GPL v2+
// (c) Blender Foundation

use math_vector::{
    cross_v3v3,
    dot_v3v3,
    div_v3fl,
};

/// NOP for now, keep since this may be supported later.
macro_rules! unlikely {
    ($body:expr) => {
        $body
    }
}

macro_rules! swap_value {
    ($a_ref:expr, $b_ref:expr) => {
        {
            let t = *$a_ref;
            *$a_ref = *$b_ref;
            *$b_ref = t;
        }
    }
}

pub fn zero_m3_ret() -> [[f64; 3]; 3] {
    [[0.0; 3]; 3]
}

pub fn zero_m4_ret() -> [[f64; 4]; 4] {
    [[0.0; 4]; 4]
}

pub fn unit_m4_ret() -> [[f64; 4]; 4] {
    [
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ]
}

pub fn unit_m3_ret() -> [[f64; 3]; 3] {
    [
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0],
    ]
}

pub fn mul_m4m4(m3: &[[f64; 4]; 4], m2: &[[f64; 4]; 4]) -> [[f64; 4]; 4] {
    [[m2[0][0] * m3[0][0] + m2[0][1] * m3[1][0] + m2[0][2] * m3[2][0] + m2[0][3] * m3[3][0],
      m2[0][0] * m3[0][1] + m2[0][1] * m3[1][1] + m2[0][2] * m3[2][1] + m2[0][3] * m3[3][1],
      m2[0][0] * m3[0][2] + m2[0][1] * m3[1][2] + m2[0][2] * m3[2][2] + m2[0][3] * m3[3][2],
      m2[0][0] * m3[0][3] + m2[0][1] * m3[1][3] + m2[0][2] * m3[2][3] + m2[0][3] * m3[3][3],
     ],
     [m2[1][0] * m3[0][0] + m2[1][1] * m3[1][0] + m2[1][2] * m3[2][0] + m2[1][3] * m3[3][0],
      m2[1][0] * m3[0][1] + m2[1][1] * m3[1][1] + m2[1][2] * m3[2][1] + m2[1][3] * m3[3][1],
      m2[1][0] * m3[0][2] + m2[1][1] * m3[1][2] + m2[1][2] * m3[2][2] + m2[1][3] * m3[3][2],
      m2[1][0] * m3[0][3] + m2[1][1] * m3[1][3] + m2[1][2] * m3[2][3] + m2[1][3] * m3[3][3],
     ],
     [m2[2][0] * m3[0][0] + m2[2][1] * m3[1][0] + m2[2][2] * m3[2][0] + m2[2][3] * m3[3][0],
      m2[2][0] * m3[0][1] + m2[2][1] * m3[1][1] + m2[2][2] * m3[2][1] + m2[2][3] * m3[3][1],
      m2[2][0] * m3[0][2] + m2[2][1] * m3[1][2] + m2[2][2] * m3[2][2] + m2[2][3] * m3[3][2],
      m2[2][0] * m3[0][3] + m2[2][1] * m3[1][3] + m2[2][2] * m3[2][3] + m2[2][3] * m3[3][3],
     ],
     [m2[3][0] * m3[0][0] + m2[3][1] * m3[1][0] + m2[3][2] * m3[2][0] + m2[3][3] * m3[3][0],
      m2[3][0] * m3[0][1] + m2[3][1] * m3[1][1] + m2[3][2] * m3[2][1] + m2[3][3] * m3[3][1],
      m2[3][0] * m3[0][2] + m2[3][1] * m3[1][2] + m2[3][2] * m3[2][2] + m2[3][3] * m3[3][2],
      m2[3][0] * m3[0][3] + m2[3][1] * m3[1][3] + m2[3][2] * m3[2][3] + m2[3][3] * m3[3][3],
     ],
    ]
}

///
/// invertmat -
/// Computes the inverse of mat and puts it in inverse.  Returns
/// true on success (i.e. can always find a pivot) and false on failure.
/// Uses Gaussian Elimination with partial (maximal column) pivoting.
///
///  Mark Segal - 1992
///
pub fn inverted_m4(mat: &[[f64; 4]; 4]) -> Option<[[f64; 4]; 4]>
{
    /*
    int i, j, k;
    double temp;
    float tempmat[4][4];
    float max;
    int maxj;
    */

    // Set inverse to identity
    let mut inverse = unit_m4_ret();
    let mut tempmat = mat.clone();

    for i in 0..4 {
        // Look for row with max pivot
        let mut max = tempmat[i][i].abs();
        let mut maxj = i;
        for j in (i + 1)..4 {
            if tempmat[j][i].abs() > max {
                max = tempmat[j][i].abs();
                maxj = j;
            }
        }
        // Swap rows if necessary
        if maxj != i {
            for k in 0..4 {
                swap_value!(&mut tempmat[i][k], &mut tempmat[maxj][k]);
                swap_value!(&mut inverse[i][k], &mut inverse[maxj][k]);
                // swap(&mut tempmat[i][k], &mut tempmat[maxj][k]);
                // swap(&mut inverse[i][k], &mut inverse[maxj][k]);
            }
        }

        if unlikely!(tempmat[i][i] == 0.0) {
            // No non-zero pivot
            return None;
        }
        let temp = tempmat[i][i];
        for k in 0..4 {
            tempmat[i][k] = tempmat[i][k] / temp;
            inverse[i][k] = inverse[i][k] / temp;
        }
        for j in 0..4 {
            if j != i {
                let temp = tempmat[j][i];
                for k in 0..4 {
                    tempmat[j][k] -= tempmat[i][k] * temp;
                    inverse[j][k] -= inverse[i][k] * temp;
                }
            }
        }
    }
    return Some(inverse);
}

pub fn inverted_m3(mat: &[[f64; 3]; 3]) -> Option<[[f64; 3]; 3]>
{
    // calc adjoint
    let mut inverse = adjoint_m3(mat);

    // then determinant old matrix!
    let det = determinant_m3_array(mat);
    if unlikely!(det == 0.0) {
        return None;
    }

    imul_m3_fl(&mut inverse, 1.0 / det);
    return Some(inverse);
}

pub fn adjoint_m3(m: &[[f64; 3]; 3]) -> [[f64; 3]; 3]
{
    [[ m[1][1] * m[2][2] - m[1][2] * m[2][1],
      -m[0][1] * m[2][2] + m[0][2] * m[2][1],
       m[0][1] * m[1][2] - m[0][2] * m[1][1],
    ],

    [-m[1][0] * m[2][2] + m[1][2] * m[2][0],
      m[0][0] * m[2][2] - m[0][2] * m[2][0],
     -m[0][0] * m[1][2] + m[0][2] * m[1][0],
    ],

    [ m[1][0] * m[2][1] - m[1][1] * m[2][0],
     -m[0][0] * m[2][1] + m[0][1] * m[2][0],
      m[0][0] * m[1][1] - m[0][1] * m[1][0],
    ]]
}


pub fn determinant_m2(
    a: f64, b: f64, c: f64, d: f64,
) -> f64 {
    return a * d - b * c
}

pub fn determinant_m3(
    a1: f64, a2: f64, a3: f64,
    b1: f64, b2: f64, b3: f64,
    c1: f64, c2: f64, c3: f64,
) -> f64 {
    return a1 * determinant_m2(b2, b3, c2, c3) -
           b1 * determinant_m2(a2, a3, c2, c3) +
           c1 * determinant_m2(a2, a3, b2, b3);
}

pub fn determinant_m3_array(m: &[[f64; 3]; 3]) -> f64
{
    return m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
           m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1]) +
           m[2][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1]);
}

pub fn determinant_m4(m: &[[f64; 4]; 4]) -> f64
{
    let a1 = m[0][0];
    let b1 = m[0][1];
    let c1 = m[0][2];
    let d1 = m[0][3];

    let a2 = m[1][0];
    let b2 = m[1][1];
    let c2 = m[1][2];
    let d2 = m[1][3];

    let a3 = m[2][0];
    let b3 = m[2][1];
    let c3 = m[2][2];
    let d3 = m[2][3];

    let a4 = m[3][0];
    let b4 = m[3][1];
    let c4 = m[3][2];
    let d4 = m[3][3];

    return a1 * determinant_m3(b2, b3, b4, c2, c3, c4, d2, d3, d4) -
           b1 * determinant_m3(a2, a3, a4, c2, c3, c4, d2, d3, d4) +
           c1 * determinant_m3(a2, a3, a4, b2, b3, b4, d2, d3, d4) -
           d1 * determinant_m3(a2, a3, a4, b2, b3, b4, c2, c3, c4);
}

pub fn transpose_m3(mat: &mut [[f64; 3]; 3])
{
    let mut t;
    t = mat[0][1];
    mat[0][1] = mat[1][0];
    mat[1][0] = t;
    t = mat[0][2];
    mat[0][2] = mat[2][0];
    mat[2][0] = t;
    t = mat[1][2];
    mat[1][2] = mat[2][1];
    mat[2][1] = t;
}

pub fn transposed_m3(mat: &[[f64; 3]; 3]) -> [[f64; 3]; 3]
{
    [
        [mat[0][0], mat[1][0], mat[2][0]],
        [mat[0][1], mat[1][1], mat[2][1]],
        [mat[0][2], mat[1][2], mat[2][2]],
    ]
}

pub fn transposed_m4(mat: &[[f64; 4]; 4]) -> [[f64; 4]; 4]
{
    [
        [mat[0][0], mat[1][0], mat[2][0], mat[3][0]],
        [mat[0][1], mat[1][1], mat[2][1], mat[3][1]],
        [mat[0][2], mat[1][2], mat[2][2], mat[3][2]],
        [mat[0][3], mat[1][3], mat[2][3], mat[3][3]],
    ]
}

pub fn is_negative_m3(mat: &[[f64; 3]; 3]) -> bool
{
    let v = cross_v3v3(&mat[0], &mat[1]);
    return dot_v3v3(&v, &mat[2]) < 0.0;
}

pub fn is_negative_m4(mat: &[[f64; 4]; 4]) -> bool
{
    // let a: [f64; 3] = [0.0, 0.0, 0.0];
    // let b: [f64; 3] = *as_v3!(&a);
    let v = cross_v3v3(as_v3!(&mat[0]), as_v3!(&mat[1]));
    return dot_v3v3(&v, as_v3!(&mat[2])) < 0.0;
}

pub fn imul_m3_fl(m: &mut [[f64; 3]; 3], f: f64)
{
    m[0][0] *= f;
    m[0][1] *= f;
    m[0][2] *= f;

    m[1][0] *= f;
    m[1][1] *= f;
    m[1][2] *= f;

    m[2][0] *= f;
    m[2][1] *= f;
    m[2][2] *= f;
}

pub fn mul_m3_v3(m: &[[f64; 3]; 3], a: &[f64; 3]) -> [f64; 3]
{
    return [
        m[0][0] * a[0] + m[1][0] * a[1] + m[2][0] * a[2],
        m[0][1] * a[0] + m[1][1] * a[1] + m[2][1] * a[2],
        m[0][2] * a[0] + m[1][2] * a[1] + m[2][2] * a[2],
    ];
}

// was mul_v2_m3v3
pub fn mul_m3v3_as_v2(m: &[[f64; 3]; 3], a: &[f64; 3]) -> [f64; 2]
{
    return [
        m[0][0] * a[0] + m[1][0] * a[1] + m[2][0] * a[2],
        m[0][1] * a[0] + m[1][1] * a[1] + m[2][1] * a[2],
    ];
}

/// was mul_v4_m4v4
/// note that 'v[3]' is implicitly 1.0.
///
pub fn mul_m4v3_as_v4(mat: &[[f64; 4]; 4], v: &[f64; 3]) -> [f64; 4] {
    [
        v[0] * mat[0][0] + v[1] * mat[1][0] + v[2] * mat[2][0] + mat[3][0] /* * v[3] */ ,
        v[0] * mat[0][1] + v[1] * mat[1][1] + v[2] * mat[2][1] + mat[3][1] /* * v[3] */ ,
        v[0] * mat[0][2] + v[1] * mat[1][2] + v[2] * mat[2][2] + mat[3][2] /* * v[3] */ ,
        v[0] * mat[0][3] + v[1] * mat[1][3] + v[2] * mat[2][3] + mat[3][3] /* * v[3] */ ,
    ]
}

pub fn mul_m4v4(mat: &[[f64; 4]; 4], v: &[f64; 4]) -> [f64; 4] {
    [
        v[0] * mat[0][0] + v[1] * mat[1][0] + v[2] * mat[2][0] + mat[3][0] * v[3],
        v[0] * mat[0][1] + v[1] * mat[1][1] + v[2] * mat[2][1] + mat[3][1] * v[3],
        v[0] * mat[0][2] + v[1] * mat[1][2] + v[2] * mat[2][2] + mat[3][2] * v[3],
        v[0] * mat[0][3] + v[1] * mat[1][3] + v[2] * mat[2][3] + mat[3][3] * v[3],
    ]
}

pub fn mul_m4v3(mat: &[[f64; 4]; 4], v: &[f64; 3]) -> [f64; 3] {
    [
        v[0] * mat[0][0] + v[1] * mat[1][0] + v[2] * mat[2][0] + mat[3][0],
        v[0] * mat[0][1] + v[1] * mat[1][1] + v[2] * mat[2][1] + mat[3][1],
        v[0] * mat[0][2] + v[1] * mat[1][2] + v[2] * mat[2][2] + mat[3][2],
    ]
}

///
/// Convenience function to get the projected depth of a position.
/// This avoids creating a temporary 4D vector and multiplying it - only for the 4th component.
///
/// Matches logic for:
///
/// \code{.c}
/// float co_4d[4] = {co[0], co[1], co[2], 1.0};
/// mul_m4_v4(mat, co_4d);
/// return co_4d[3];
/// \endcode
///
pub fn mul_project_m4_v3_zfac(mat: &[[f64; 4]; 4], co: &[f64; 3]) -> f64 {
    return
        (mat[0][3] * co[0]) +
        (mat[1][3] * co[1]) +
        (mat[2][3] * co[2]) + mat[3][3];
}

pub fn mul_project_m4_v3(mat: &[[f64; 4]; 4], co: &[f64; 3]) -> [f64; 3] {
    // Don't use abs, this isn't meaningful

    // absolute value to not flip the frustum upside down behind the camera
    // const float w = fabsf(mul_project_m4_v3_zfac(mat, vec));
    let w = mul_project_m4_v3_zfac(mat, co);
    return div_v3fl(&mul_m4v3(mat, co), w);
}

pub fn translate_m4(mat: &mut [[f64; 4]; 4], ofs: &[f64; 3]) {
    mat[3][0] += ofs[0] * mat[0][0] + ofs[1] * mat[1][0] + ofs[2] * mat[2][0];
    mat[3][1] += ofs[0] * mat[0][1] + ofs[1] * mat[1][1] + ofs[2] * mat[2][1];
    mat[3][2] += ofs[0] * mat[0][2] + ofs[1] * mat[1][2] + ofs[2] * mat[2][2];
}

///
/// Rotate a matrix in-place.
///
/// \note To create a new rotation matrix see:
/// #axis_angle_to_mat4_single, #axis_angle_to_mat3_single, #angle_to_mat2
/// (axis & angle args are compatible).
///
pub fn rotate_m4(mat: &mut [[f64; 4]; 4], axis: char, angle: f64)
{
    let angle_cos = angle.cos();
    let angle_sin = angle.sin();
    match axis {
        'X' => {
            for col in 0..4 {
                let f       =  angle_cos * mat[1][col] + angle_sin * mat[2][col];
                mat[2][col] = -angle_sin * mat[1][col] + angle_cos * mat[2][col];
                mat[1][col] =  f;
            }
        },
        'Y' => {
            for col in 0..4 {
                let temp    =  angle_cos * mat[0][col] - angle_sin * mat[2][col];
                mat[2][col] =  angle_sin * mat[0][col] + angle_cos * mat[2][col];
                mat[0][col] =  temp;
            }
        },
        'Z' => {
            for col in 0..4 {
                let temp    =  angle_cos * mat[0][col] + angle_sin * mat[1][col];
                mat[1][col] = -angle_sin * mat[0][col] + angle_cos * mat[1][col];
                mat[0][col] =  temp;
            }
        },
        _ => {
            panic!();
        }
    }
}

// method matches mesa's glFrustum (_math_matrix_frustum)
// in this case we don't multiply, just create.
pub fn perspective_m4(
    left: f64, right: f64,
    bottom: f64, top: f64,
    nearval: f64, farval: f64,
) -> [[f64; 4]; 4] {

    let x = (2.0 * nearval) / (right - left);
    let y = (2.0 * nearval) / (top - bottom);
    let a = (right + left)  / (right - left);
    let b = (top + bottom)  / (top - bottom);
    let c = -(farval + nearval) / ( farval - nearval);
    let d = -(2.0 * farval * nearval) / (farval - nearval);  /* error? */

    return [
        [  x,  0.0,   0.0,  0.0],
        [0.0,    y,   0.0,  0.0],
        [  a,    b,     c, -1.0],
        [0.0,  0.0,     d,  0.0],
    ];
}

// method matches mesa's glOrtho (_math_matrix_ortho)
pub fn orthographic_m4(
    left: f64, right: f64,
    bottom: f64, top: f64,
    nearval: f64, farval: f64,
) -> [[f64; 4]; 4] {

    let xs = right - left;
    let xa = right + left;

    let ys = top - bottom;
    let ya = top + bottom;

    let zs = farval - nearval;
    let za = farval + nearval;

    return [
        [2.0 / xs,      0.0,       0.0, 0.0],
        [     0.0, 2.0 / ys,       0.0, 0.0],
        [     0.0,      0.0, -2.0 / zs, 0.0],
        [-xa / xs, -ya / ys,  -za / zs, 1.0],
   ];
}

