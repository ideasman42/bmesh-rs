// Licensed: GPL v2+
// (c) Blender Foundation

pub trait ClampExt {
    fn clamp(&self, min: Self, max: Self) -> Self;
}

macro_rules! clamp_ext_impl {
    ($($t:ty)*) => ($(
        impl ClampExt for $t {
            #[inline]
            fn clamp(&self, min: Self, max: Self) -> Self {
                debug_assert!((max < min) == false);
                if *self < min {
                    min
                } else if *self > max {
                    max
                } else {
                    *self
                }
            }
        }
    )*)
}
clamp_ext_impl! { u8 u16 u32 u64 usize i8 i16 i32 i64 isize f32 f64 }


pub trait AcosSafeExt {
    fn acos_safe(&self) -> Self;
}
macro_rules! acos_safe_ext_impl {
    ($($t:ty)*) => ($(
        impl AcosSafeExt for $t {
            #[inline]
            fn acos_safe(&self) -> Self {
                self.clamp(-1.0, 1.0).acos()
            }
        }
    )*)
}
acos_safe_ext_impl! { f32 f64 }

///
/// Modulo that handles negative numbers, works the same as Python's.
///
/// eg: `(a + b).modulo(c)`
///
pub trait ModuloSignedExt {
    fn modulo(&self, n: Self) -> Self;
}
macro_rules! modulo_signed_ext_impl {
    ($($t:ty)*) => ($(
        impl ModuloSignedExt for $t {
            #[inline]
            fn modulo(&self, n: Self) -> Self {
                (self % n + n) % n
            }
        }
    )*)
}
modulo_signed_ext_impl! { i8 i16 i32 i64 }
