// Licensed: GPL v2+
// (c) Blender Foundation

use math_vector::{
    normalized_v3,
};

const QUAT_EPSILON: f64 = 0.0001;

pub fn unit_qt_ret() -> [f64; 4] {
    [1.0, 0.0, 0.0, 0.0]
}

pub fn unit_qt(q: &mut [f64; 4]) {
    q[0] = 1.0;
    q[1] = 0.0;
    q[2] = 0.0;
    q[3] = 0.0;
}


pub fn dot_qtqt(q1: &[f64; 4], q2: &[f64; 4]) -> f64 {
    return q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3];
}


pub fn mul_qtqt(q1: &[f64; 4], q2: &[f64; 4]) -> [f64; 4] {
    [
        q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3],
        q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2],
        q1[0] * q2[2] + q1[2] * q2[0] + q1[3] * q2[1] - q1[1] * q2[3],
        q1[0] * q2[3] + q1[3] * q2[0] + q1[1] * q2[2] - q1[2] * q2[1],
    ]
}

pub fn mul_qt_qtfl(r: &mut [f64; 4], q: &[f64; 4], f: f64) {
    for i in 0..q.len() {
        r[i] = q[i] * f;
    }
}

pub fn imul_qtqt(q1: &mut [f64; 4], q2: &[f64; 4]) {
    *q1 = mul_qtqt(q1, q2);
}

pub fn imul_qt_fl(q: &mut [f64; 4], f: f64) {
    q[0] *= f;
    q[1] *= f;
    q[2] *= f;
    q[3] *= f;
}

#[inline]
pub fn normalize_qt_qt(
    r: &mut [f64; 4], q: &[f64; 4],
) -> f64 {
    let mut d = dot_qtqt(q, q);
    if d != 0.0 {
        d = d.sqrt();
        mul_qt_qtfl(r, q, 1.0 / d);
    } else {
        r[0] = 0.0;
        r[1] = 1.0;
        r[2] = 0.0;
        r[3] = 0.0;
        d = 0.0;
    }
    return d;
}

#[inline]
pub fn normalize_qt(
    q: &mut [f64; 4],
) -> f64 {
    let tmp: [f64; 4] = *q;
    let len = normalize_qt_qt(q, &tmp);
    return len;
}

#[inline]
pub fn normalized_qt(
    q: &[f64; 4],
) -> ([f64; 4], f64) {
    let mut tmp: [f64; 4] = [0.0; 4];
    let len = normalize_qt_qt(&mut tmp, q);
    return (tmp, len);
}

// ----------------------------------------------------------------------------
// Axis Angle

pub fn axis_angle_normalized_to_quat(axis: &[f64; 3], angle: f64) -> [f64; 4] {
    let phi = 0.5 * angle;
    let si = phi.sin();
    let co = phi.cos();
    debug_assert_unit_v3!(axis);
    return [
        co,
        axis[0] * si,
        axis[1] * si,
        axis[2] * si,
    ];
}

pub fn axis_angle_to_quat(axis: &[f64; 3], angle: f64) -> [f64; 4] {
    let (nor, len) = normalized_v3(axis);

    if len != 0.0 {
        return axis_angle_normalized_to_quat(&nor, angle);
    }
    else {
        return unit_qt_ret();
    }
}

pub fn axis_angle_to_quat_single(axis: char, angle: f64) -> [f64; 4] {
    let angle_half = angle * 0.5;
    let angle_cos = angle_half.cos();
    let angle_sin = angle_half.sin();

    return {
        match axis {
            'X' => { [angle_cos, angle_sin, 0.0, 0.0] },
            'Y' => { [angle_cos, 0.0, angle_sin, 0.0] },
            'Z' => { [angle_cos, 0.0, 0.0, angle_sin] },
            _ => {
                unreachable!();
            }
        }
    };
}

// rotation matrix from a single axis
pub fn axis_angle_to_mat3_single(axis: char, angle: f64) -> [[f64; 3]; 3] {
    let cos = angle.cos();
    let sin = angle.sin();

    return {
        match axis {
            // rotation around X
            'X' => {
                [
                    [1.0,  0.0, 0.0],
                    [0.0,  cos, sin],
                    [0.0, -sin, cos],
                ]
            },
            // rotation around Y
            'Y' => {
                [
                    [cos, 0.0, sin],
                    [0.0, 1.0, 0.0],
                    [sin, 0.0, cos],
                ]
            },
            // rotation around Z
            'Z' => {
                [
                    [ cos, sin, 0.0],
                    [-sin, cos, 0.0],
                    [ 0.0, 0.0, 1.0],
                ]
            },
            _ => {
                unreachable!();
            }
        }
    };
}

pub fn quat_to_mat3(q: &[f64; 4]) -> [[f64; 3]; 3]
{
    if cfg!(debug_assertions) {
        let q0 = dot_qtqt(q, q);

        if !(q0 == 0.0 || ((q0 - 1.0).abs() < QUAT_EPSILON)) {
            println!(
                concat!(
                "Warning! quat_to_mat3() called with non-normalized: ",
                "size {} *** report a bug ***\n"), q0,
            );
        }
    }

    use ::std::f64::consts::SQRT_2;

    let q0 = SQRT_2 * q[0];
    let q1 = SQRT_2 * q[1];
    let q2 = SQRT_2 * q[2];
    let q3 = SQRT_2 * q[3];

    let qda = q0 * q1;
    let qdb = q0 * q2;
    let qdc = q0 * q3;
    let qaa = q1 * q1;
    let qab = q1 * q2;
    let qac = q1 * q3;
    let qbb = q2 * q2;
    let qbc = q2 * q3;
    let qcc = q3 * q3;

    return [
        [1.0 - qbb - qcc,       qdc + qab,      -qdb + qac],
        [     -qdc + qab, 1.0 - qaa - qcc,       qda + qbc],
        [      qdb + qac,      -qda + qbc, 1.0 - qaa - qbb],
    ];
}

pub fn quat_to_mat4(q: &[f64; 4]) -> [[f64; 4]; 4]
{
    if cfg!(debug_assertions) {
        let q0 = dot_qtqt(q, q);

        if !(q0 == 0.0 || ((q0 - 1.0).abs() < QUAT_EPSILON)) {
            println!(
                concat!(
                "Warning! quat_to_mat4() called with non-normalized: ",
                "size {} *** report a bug ***\n"), q0,
            );
        }
    }

    use ::std::f64::consts::SQRT_2;

    let q0 = SQRT_2 * q[0];
    let q1 = SQRT_2 * q[1];
    let q2 = SQRT_2 * q[2];
    let q3 = SQRT_2 * q[3];

    let qda = q0 * q1;
    let qdb = q0 * q2;
    let qdc = q0 * q3;
    let qaa = q1 * q1;
    let qab = q1 * q2;
    let qac = q1 * q3;
    let qbb = q2 * q2;
    let qbc = q2 * q3;
    let qcc = q3 * q3;

    return [
        [1.0 - qbb - qcc,       qdc + qab,      -qdb + qac, 0.0],
        [     -qdc + qab, 1.0 - qaa - qcc,       qda + qbc, 0.0],
        [      qdb + qac,      -qda + qbc, 1.0 - qaa - qbb, 0.0],
        [            0.0,             0.0,             0.0, 1.0],
    ];
}


// 'mod_inline(-3, 4)= 1', 'fmod(-3, 4)= -3'
fn mod_inline(a: f64, b: f64) -> f64
{
    return a - (b * (a / b).floor());
}

pub fn angle_wrap_rad(angle: f64) -> f64
{
    use ::std::f64::consts::PI;
    return mod_inline(angle + PI, PI * 2.0) - PI;
}

pub fn angle_wrap_deg(angle: f64) -> f64
{
    return mod_inline(angle + 180.0, 360.0) - 180.0;
}

// returns an angle compatible with angle_compat
pub fn angle_compat_rad(angle: f64, angle_compat: f64) -> f64
{
    return angle_compat + angle_wrap_rad(angle - angle_compat);
}
