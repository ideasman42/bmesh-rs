// Licensed: Apache 2.0

macro_rules! debug_assert_unit_epsilon {
    () => {
        0.0002
    }
}

#[macro_export]
macro_rules! as_v3 {
    ($v:expr) => {
        {
            if $v.len() < 3 {
                panic!();
            }
            unsafe { &*($v as *const _ as *const [_; 3]) }
        }
    }
}
#[macro_export]
macro_rules! as_v3_mut {
    ($v:expr) => {
        {
            if $v.len() < 3 {
                panic!();
            }
            unsafe { &mut *($v as *mut _ as *mut [_; 3]) }
        }
    }
}

#[macro_export]
macro_rules! debug_assert_unit_v2 {
    ($v:expr) => {
        let test_unit = len_squared_v2($v);
        let eps = debug_assert_unit_epsilon!();
        debug_assert!(((test_unit - 1.0).abs() < eps) ||
                      (test_unit.abs()         < eps));
    }
}

#[macro_export]
macro_rules! debug_assert_unit_v3 {
    ($v:expr) => {
        let test_unit = ::math_vector::len_squared_v3($v);
        let eps = debug_assert_unit_epsilon!();
        debug_assert!(((test_unit - 1.0).abs() < eps) ||
                      (test_unit.abs()         < eps));
    }
}
