// Licensed: GPL v2+
// (c) Blender Foundation

use math_base::{
    AcosSafeExt,
};

// sub_v#v#
pub fn sub_v2v2(v1: &[f64; 2], v2: &[f64; 2]) -> [f64; 2] {
    let mut tmp: [f64; 2] = [0.0; 2];
    for i in 0..tmp.len() {
        tmp[i] = v1[i] - v2[i];
    }
    return tmp;
}
pub fn sub_v3v3(v1: &[f64; 3], v2: &[f64; 3]) -> [f64; 3] {
    let mut tmp: [f64; 3] = [0.0; 3];
    for i in 0..tmp.len() {
        tmp[i] = v1[i] - v2[i];
    }
    return tmp;
}

// mul_v#_v#fl
pub fn mul_v2_v2fl(r: &mut [f64; 2], v: &[f64; 2], f: f64) {
    for i in 0..v.len() {
        r[i] = v[i] * f;
    }
}
pub fn mul_v3_v3fl(r: &mut [f64; 3], v: &[f64; 3], f: f64) {
    for i in 0..v.len() {
        r[i] = v[i] * f;
    }
}

// mul_v#fl
pub fn mul_v2fl(v: &[f64; 2], f: f64) -> [f64; 2] {
    let mut tmp: [f64; 2] = [0.0; 2];
    for i in 0..v.len() {
        tmp[i] = v[i] * f;
    }
    return tmp;
}
pub fn mul_v3fl(v: &[f64; 3], f: f64) -> [f64; 3] {
    let mut tmp: [f64; 3] = [0.0; 3];
    for i in 0..v.len() {
        tmp[i] = v[i] * f;
    }
    return tmp;
}

// imul_v#_fl
pub fn imul_v2_fl(v: &mut [f64; 2], f: f64) {
    for i in 0..v.len() {
        v[i] *= f;
    }
}
pub fn imul_v3_fl(v: &mut [f64; 3], f: f64) {
    for i in 0..v.len() {
        v[i] *= f;
    }
}

// div_v#_v#fl
pub fn div_v2_v2fl(r: &mut [f64; 2], v: &[f64; 2], f: f64) {
    for i in 0..v.len() {
        r[i] = v[i] / f;
    }
}
pub fn div_v3_v3fl(r: &mut [f64; 3], v: &[f64; 3], f: f64) {
    for i in 0..v.len() {
        r[i] = v[i] / f;
    }
}

// div_v#fl
pub fn div_v2fl(v: &[f64; 2], f: f64) -> [f64; 2] {
    let mut tmp: [f64; 2] = [0.0; 2];
    for i in 0..v.len() {
        tmp[i] = v[i] / f;
    }
    return tmp;
}
pub fn div_v3fl(v: &[f64; 3], f: f64) -> [f64; 3] {
    let mut tmp: [f64; 3] = [0.0; 3];
    for i in 0..v.len() {
        tmp[i] = v[i] / f;
    }
    return tmp;
}

// idiv_v#_fl
pub fn idiv_v2_fl(v: &mut [f64; 2], f: f64) {
    for i in 0..v.len() {
        v[i] /= f;
    }
}
pub fn idiv_v3_fl(v: &mut [f64; 3], f: f64) {
    for i in 0..v.len() {
        v[i] /= f;
    }
}

////////////////////


// iadd_v#_v#
pub fn iadd_v3_v3(v1: &mut [f64; 3], v2: &[f64; 3]) {
    for i in 0..v1.len() {
        v1[i] += v2[i];
    }
}

// madd_v#_v#fl
pub fn madd_v2v2fl(v1: &[f64; 2], v2: &[f64; 2], f: f64) -> [f64; 2] {
    let mut tmp: [f64; 2] = [0.0; 2];
    for i in 0..v1.len() {
        tmp[i] = v1[i] + v2[i] * f;
    }
    return tmp;
}
pub fn madd_v3v3fl(v1: &[f64; 3], v2: &[f64; 3], f: f64) -> [f64; 3] {
    let mut tmp: [f64; 3] = [0.0; 3];
    for i in 0..v1.len() {
        tmp[i] = v1[i] + v2[i] * f;
    }
    return tmp;
}

// madd_v#_v#fl
pub fn imadd_v3_v3fl(r: &mut [f64; 3], v: &[f64; 3], f: f64) {
    for i in 0..v.len() {
        r[i] += v[i] * f;
    }
}

// negate_v#
#[inline]
pub fn negate_v2(v: &mut [f64; 2]) {
    for i in 0..v.len() {
        v[i] *= -1.0;
    }
}
#[inline]
pub fn negate_v3(v: &mut [f64; 3]) {
    for i in 0..v.len() {
        v[i] *= -1.0;
    }
}

// negated_v#
#[inline]
pub fn negated_v2(v: &[f64; 2]) -> [f64; 2] {
    return [
        -v[0],
        -v[1],
    ];
}
#[inline]
pub fn negated_v3(v: &[f64; 3]) -> [f64; 3] {
    return [
        -v[0],
        -v[1],
        -v[2],
    ];
}


// interp_v#v#
#[inline]
pub fn interp_v2v2(a: &[f64; 2], b: &[f64; 2], t: f64) -> [f64; 2] {
    let s = 1.0 - t;
    return [
        s * a[0] + t * b[0],
        s * a[1] + t * b[1],
    ];
}

#[inline]
pub fn interp_v3v3(a: &[f64; 3], b: &[f64; 3], t: f64) -> [f64; 3] {
    let s = 1.0 - t;
    return [
        s * a[0] + t * b[0],
        s * a[1] + t * b[1],
        s * a[2] + t * b[2],
    ];
}

#[inline]
pub fn interp_v4v4(a: &[f64; 4], b: &[f64; 4], t: f64) -> [f64; 4] {
    let s = 1.0 - t;
    return [
        s * a[0] + t * b[0],
        s * a[1] + t * b[1],
        s * a[2] + t * b[2],
        s * a[3] + t * b[3],
    ];
}

// Newell's Method
// excuse this fairly specific function,
// its used for polygon normals all over the place
// could use a better name

#[inline]
pub fn add_newell_cross_v3_v3v3(
    n: &mut [f64; 3],
    v_prev: &[f64; 3],
    v_curr: &[f64; 3],
) {
    n[0] += (v_prev[1] - v_curr[1]) * (v_prev[2] + v_curr[2]);
    n[1] += (v_prev[2] - v_curr[2]) * (v_prev[0] + v_curr[0]);
    n[2] += (v_prev[0] - v_curr[0]) * (v_prev[1] + v_curr[1]);
}

// cross_v#v#
#[inline]
pub fn cross_v2v2(a: &[f64; 2], b: &[f64; 2]) -> f64
{
    return a[0] * b[1] - a[1] * b[0];
}
#[inline]
pub fn cross_v3v3(a: &[f64; 3], b: &[f64; 3]) -> [f64; 3]
{
    return [
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0],
    ];
}

// zero_v#v#
#[inline]
pub fn zero_v2(v: &mut [f64; 2]) {
    for i in 0..v.len() {
        v[i] = 0.0;
    }
}
#[inline]
pub fn zero_v3(v: &mut [f64; 3]) {
    for i in 0..v.len() {
        v[i] = 0.0;
    }
}

// dot_v#v#
#[inline]
pub fn dot_v2v2(a: &[f64; 2], b: &[f64; 2]) -> f64 {
    return a[0] * b[0] + a[1] * b[1];
}
#[inline]
pub fn dot_v3v3(a: &[f64; 3], b: &[f64; 3]) -> f64 {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

// normalize_v#_v#_length
#[inline]
pub fn normalize_v2_v2_length(
    r: &mut [f64; 2], a: &[f64; 2],
    unit_length: f64,
) -> f64 {
    let mut d = dot_v2v2(a, a);
    if d > 1.0e-35 {
        d = d.sqrt();
        mul_v2_v2fl(r, a, unit_length / d);
    }
    else {
        zero_v2(r);
        d = 0.0;
    }
    return d;
}
#[inline]
pub fn normalize_v3_v3_length(
    r: &mut [f64; 3], a: &[f64; 3],
    unit_length: f64,
) -> f64 {
    let mut d = dot_v3v3(a, a);
    if d > 1.0e-35 {
        d = d.sqrt();
        mul_v3_v3fl(r, a, unit_length / d);
    }
    else {
        zero_v3(r);
        d = 0.0;
    }
    return d;
}

// normalize_v#_v#
#[inline]
pub fn normalize_v2_v2(r: &mut [f64; 2], a: &[f64; 2]) -> f64 {
    return normalize_v2_v2_length(r, a, 1.0);
}
#[inline]
pub fn normalize_v3_v3(r: &mut [f64; 3], a: &[f64; 3]) -> f64 {
    return normalize_v3_v3_length(r, a, 1.0);
}

// normalize_v#_length
#[inline]
pub fn normalize_v2_length(
    a: &mut [f64; 2],
    unit_length: f64,
) -> f64 {
    let mut d = dot_v2v2(a, a);
    if d > 1.0e-35 {
        d = d.sqrt();
        imul_v2_fl(a, unit_length / d);
    }
    else {
        zero_v2(a);
        d = 0.0;
    }
    return d;
}
#[inline]
pub fn normalize_v3_length(
    a: &mut [f64; 3],
    unit_length: f64,
) -> f64 {
    let mut d = dot_v3v3(a, a);
    if d > 1.0e-35 {
        d = d.sqrt();
        imul_v3_fl(a, unit_length / d);
    }
    else {
        zero_v3(a);
        d = 0.0;
    }
    return d;
}

// normalize_v#
#[inline]
pub fn normalize_v2(n: &mut [f64; 2]) -> f64 {
    return normalize_v2_length(n, 1.0);
}
#[inline]
pub fn normalize_v3(n: &mut [f64; 3]) -> f64 {
    return normalize_v3_length(n, 1.0);
}

// normalized_v#
#[inline]
pub fn normalized_v2(n: &[f64; 2]) -> ([f64; 2], f64) {
    let mut tmp: [f64; 2] = [0.0; 2];
    let len = normalize_v2_v2(&mut tmp, n);
    return (tmp, len);
}
#[inline]
pub fn normalized_v3(n: &[f64; 3]) -> ([f64; 3], f64) {
    let mut tmp: [f64; 3] = [0.0; 3];
    let len = normalize_v3_v3(&mut tmp, n);
    return (tmp, len);
}

// normalize_sub_v#_v#v#
#[inline]
pub fn normalize_sub_v2_v2v2(r: &mut [f64; 2], a: &[f64; 2], b: &[f64; 2]) -> f64 {
    return normalize_v2_v2(r, &sub_v2v2(a, b));
}
#[inline]
pub fn normalize_sub_v3_v3v3(r: &mut [f64; 3], a: &[f64; 3], b: &[f64; 3]) -> f64 {
    return normalize_v3_v3(r, &sub_v3v3(a, b));
}

// normalized_sub_v#v#
#[inline]
pub fn normalized_sub_v2v2(a: &[f64; 2], b: &[f64; 2]) -> ([f64; 2], f64) {
    return normalized_v2(&sub_v2v2(a, b));
}
#[inline]
pub fn normalized_sub_v3v3(a: &[f64; 3], b: &[f64; 3]) -> ([f64; 3], f64) {
    return normalized_v3(&sub_v3v3(a, b));
}

// len_squared_v#
#[inline]
pub fn len_squared_v2(v: &[f64; 2]) -> f64 {
    return v[0] * v[0] + v[1] * v[1];
}
#[inline]
pub fn len_squared_v3(v: &[f64; 3]) -> f64 {
    return v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
}

// len_squared_v#v#
#[inline]
pub fn len_squared_v2v2(v_a: &[f64; 2], v_b: &[f64; 2]) -> f64 {
    return len_squared_v2(&sub_v2v2(v_a, v_b));
}
#[inline]
pub fn len_squared_v3v3(v_a: &[f64; 3], v_b: &[f64; 3]) -> f64 {
    return len_squared_v3(&sub_v3v3(v_a, v_b));
}

// len_v#
#[inline]
pub fn len_v2(v: &[f64; 2]) -> f64 {
    return len_squared_v2(v).sqrt();
}
#[inline]
pub fn len_v3(v: &[f64; 3]) -> f64 {
    return len_squared_v3(v).sqrt();
}

// len_v#v#
#[inline]
pub fn len_v2v2(v_a: &[f64; 2], v_b: &[f64; 2]) -> f64 {
    return len_squared_v2v2(v_a, v_b).sqrt();
}
#[inline]
pub fn len_v3v3(v_a: &[f64; 3], v_b: &[f64; 3]) -> f64 {
    return len_squared_v3v3(v_a, v_b).sqrt();
}

#[inline]
pub fn angle_normalized_v2v2(v_a: &[f64; 2], v_b: &[f64; 2]) -> f64
{
    // double check they are normalized
    debug_assert_unit_v2!(v_a);
    debug_assert_unit_v2!(v_b);

    return dot_v2v2(v_a, v_b).acos_safe();
}

#[inline]
pub fn angle_normalized_v3v3(v_a: &[f64; 3], v_b: &[f64; 3]) -> f64
{
    // double check they are normalized
    debug_assert_unit_v3!(v_a);
    debug_assert_unit_v3!(v_b);

    return dot_v3v3(v_a, v_b).acos_safe();
}

/// Return the angle in radians between vecs 1-2 and 2-3 in radians
/// If v1 is a shoulder, v2 is the elbow and v3 is the hand,
/// this would return the angle at the elbow.
///
/// note that when v1/v2/v3 represent 3 points along a straight line
/// that the angle returned will be pi (180deg), rather then 0.0

// angle_v#v#v#
pub fn angle_v3v3v3(v1: &[f64; 3], v2: &[f64; 3], v3: &[f64; 3]) -> f64
{
    let (vec1, _) = normalized_sub_v3v3(v2, v1);
    let (vec2, _) = normalized_sub_v3v3(v2, v3);
    return angle_normalized_v3v3(&vec1, &vec2);
}


///
/// Takes a vector and computes 2 orthogonal directions.
///
/// \note if \a n is n unit length, computed values will be too.
///
pub fn ortho_basis_v3v3_v3(n: &[f64; 3]) -> ([f64; 3], [f64; 3])
{
    let eps = ::std::f64::EPSILON;
    let f = len_squared_v2(&[n[0], n[1]]);
    return {
        let n1;
        let n2;
        if f > eps {
            let d = 1.0 / f.sqrt();

            debug_assert!(d.is_finite());

            n1 = [n[1] * d, -n[0] * d, 0.0];
            n2 = [-n[2] * n1[1], n[2] * n1[0], n[0] * n1[1] - n[1] * n1[0]];
        } else {
            // degenerate case
            n1 = [if n[2] < 0.0 { -1.0 } else { 1.0 }, 0.0, 0.0];
            n2 = [0.0, 1.0, 0.0];
        }

        (n1, n2)
    };
}


/**
 * Has the effect of #mul_m3_v3(), on a single axis.
 */
#[inline]
pub fn dot_m3_v3_row_x(m: &[[f64; 3]; 3], a: &[f64; 3]) -> f64
{
    return m[0][0] * a[0] + m[1][0] * a[1] + m[2][0] * a[2];
}
#[inline]
pub fn dot_m3_v3_row_y(m: &[[f64; 3]; 3], a: &[f64; 3]) -> f64
{
    return m[0][1] * a[0] + m[1][1] * a[1] + m[2][1] * a[2];
}
#[inline]
pub fn dot_m3_v3_row_z(m: &[[f64; 3]; 3], a: &[f64; 3]) -> f64
{
    return m[0][2] * a[0] + m[1][2] * a[1] + m[2][2] * a[2];
}

/**
 * Has the effect of #mul_mat3_m4_v3(), on a single axis.
 * (no adding translation)
 */
#[inline]
pub fn dot_m4_v3_row_x(m: &[[f64; 4]; 4], a: &[f64; 3]) -> f64
{
    return m[0][0] * a[0] + m[1][0] * a[1] + m[2][0] * a[2];
}
#[inline]
pub fn dot_m4_v3_row_y(m: &[[f64; 4]; 4], a: &[f64; 3]) -> f64
{
    return m[0][1] * a[0] + m[1][1] * a[1] + m[2][1] * a[2];
}
#[inline]
pub fn dot_m4_v3_row_z(m: &[[f64; 4]; 4], a: &[f64; 3]) -> f64
{
    return m[0][2] * a[0] + m[1][2] * a[1] + m[2][2] * a[2];
}

// is_zero_v#
pub fn is_zero_v2(v: &[f64; 2]) -> bool
{
    return v[0] == 0.0 && v[1] == 0.0;
}
pub fn is_zero_v3(v: &[f64; 3]) -> bool
{
    return v[0] == 0.0 && v[1] == 0.0 && v[2] == 0.0;
}
pub fn is_zero_v4(v: &[f64; 4]) -> bool
{
    return v[0] == 0.0 && v[1] == 0.0 && v[2] == 0.0 && v[3] == 0.0;
}

pub fn is_one_v3(v: &[f64; 3]) -> bool
{
    return v[0] == 1.0 && v[1] == 1.0 && v[2] == 1.0;
}

/** \name Vector Comparison
 *
 * \note use ``value <= limit``, so a limit of zero doesn't fail on an exact match.
 * \{ */

pub fn equals_v2v2(v_a: &[f64; 2], v_b: &[f64; 2]) -> bool
{
    return
        (v_a[0] == v_b[0]) &&
        (v_a[1] == v_b[1]);
}

pub fn equals_v3v3(v_a: &[f64; 3], v_b: &[f64; 3]) -> bool
{
    return
        (v_a[0] == v_b[0]) &&
        (v_a[1] == v_b[1]) &&
        (v_a[2] == v_b[2]);
}

pub fn equals_v4v4(v_a: &[f64; 4], v_b: &[f64; 4]) -> bool
{
    return
        (v_a[0] == v_b[0]) &&
        (v_a[1] == v_b[1]) &&
        (v_a[2] == v_b[2]) &&
        (v_a[3] == v_b[3]);
}


// ----------------------------------------------------------------------------
// Projections


///
/// Project \a p onto \a v_proj
///
/// was: project_v2_v2v2
///
pub fn project_v2v2(
    p: &[f64; 2], v_proj: &[f64; 2],
) -> [f64; 2] {
    return mul_v2fl(
        v_proj,
        dot_v2v2(p, v_proj) / dot_v2v2(v_proj, v_proj),
    );
}

///
/// Project \a p onto \a v_proj
///
/// was: project_v3_v3v3
///
pub fn project_v3v3(
    p: &[f64; 3], v_proj: &[f64; 3],
) -> [f64; 3] {
    let mul = dot_v3v3(p, v_proj) / dot_v3v3(v_proj, v_proj);
    return mul_v3fl(v_proj, mul);
}

///
/// In this case plane is a 3D vector only (no 4th component).
///
/// Projecting will make \a c a copy of \a v orthogonal to \a v_plane.
///
/// \note If \a v is exactly perpendicular to \a v_plane, \a c will just be a copy of \a v.
///
/// \note This function is a convenience to call:
/// \code{.c}
/// project_v3_v3v3(c, v, v_plane);
/// sub_v3_v3v3(c, v, c);
/// \endcode
///
/// was: project_plane_v3_v3v3
///
pub fn project_plane_v3v3(
    p: &[f64; 3], v_plane: &[f64; 3],
) -> [f64; 3] {
    let mul = dot_v3v3(p, v_plane) / dot_v3v3(v_plane, v_plane);
    return sub_v3v3(p, &mul_v3fl(v_plane, mul));
}

///
/// was: project_plane_v2_v2v2
///
pub fn project_plane_v2v2(
    p: &[f64; 2], v_plane: &[f64; 2],
) -> [f64; 2] {
    let mul = dot_v2v2(p, v_plane) / dot_v2v2(v_plane, v_plane);
    return sub_v2v2(p, &mul_v2fl(v_plane, mul));
}

///
/// project a vector on a plane defined by normal and a plane point p
///
/// note: unlike Blender, `p` isn't modified in-place.
///
pub fn project_v3_plane(
    p: &[f64; 3], plane_no: &[f64; 3], plane_co: &[f64; 3],
) -> [f64; 3] {
    let mul = dot_v3v3(&sub_v3v3(p, plane_co), plane_no) / len_squared_v3(plane_no);
    return sub_v3v3(p, &mul_v3fl(plane_no, mul));
}
