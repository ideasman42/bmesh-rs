// Licensed: GPL v2+
// (c) Blender Foundation

use ::math_vector::{
    dot_m3_v3_row_z,
    len_squared_v2v2,
    len_squared_v3v3,
    dot_v2v2,
    dot_v3v3,
    madd_v2v2fl,
    madd_v3v3fl,
    sub_v2v2,
    sub_v3v3,
    is_zero_v3,
    len_squared_v3,
    mul_v3fl,
    negated_v3,
    normalized_v3,
    ortho_basis_v3v3_v3,
};
use ::math_matrix::{
    transpose_m3,
    is_negative_m3,
};

///
/// \brief Normal to x,y matrix
///
/// Creates a 3x3 matrix from a normal.
/// This matrix can be applied to vectors so their 'z' axis runs along \a normal.
/// In practice it means you can use x,y as 2d coords. \see
///
/// \param r_mat The matrix to return.
/// \param normal A unit length vector.
///
/*
void axis_dominant_v3_to_m3(float r_mat[3][3], const float normal[3])
{
	BLI_ASSERT_UNIT_V3(normal);

	copy_v3_v3(r_mat[2], normal);
	ortho_basis_v3v3_v3(r_mat[0], r_mat[1], r_mat[2]);

	BLI_ASSERT_UNIT_V3(r_mat[0]);
	BLI_ASSERT_UNIT_V3(r_mat[1]);

	transpose_m3(r_mat);

	BLI_assert(!is_negative_m3(r_mat));
	BLI_assert((fabsf(dot_m3_v3_row_z(r_mat, normal) - 1.0f) < BLI_ASSERT_UNIT_EPSILON) ||
               is_zero_v3(normal));
}
*/

///
/// Same as axis_dominant_v3_to_m3, but flips the normal
///
pub fn axis_dominant_v3_to_m3_negate(
    normal: &[f64; 3]
) -> [[f64; 3]; 3] {
    debug_assert_unit_v3!(normal);

    let axis = negated_v3(normal);
    // negate_v3_v3(r_mat[2], normal);
    let (n1, n2) = ortho_basis_v3v3_v3(&axis);

    let mut mat: [[f64; 3]; 3] = [
        n1,
        n2,
        axis,
    ];

    debug_assert_unit_v3!(&mat[0]);
    debug_assert_unit_v3!(&mat[1]);

    transpose_m3(&mut mat);

    debug_assert!(!is_negative_m3(&mat));
    debug_assert!((dot_m3_v3_row_z(&mat, normal) < debug_assert_unit_epsilon!()) ||
                  is_zero_v3(normal));

    return mat;
}




// ----------------------------------------------------------------------------
// Distance

/// find closest point to p on line through (l1, l2) and return lambda,
/// where (0 <= lambda <= 1) when cp is in the line segment (l1, l2)

pub fn closest_to_line_v2(p: &[f64; 2], l1: &[f64; 2], l2: &[f64; 2]) -> ([f64; 2], f64)
{
    let u = sub_v2v2(l2, l1);
    let h = sub_v2v2(p, l1);
    let lambda = dot_v2v2(&u, &h) / dot_v2v2(&u, &u);
    return (
        madd_v2v2fl(&l1, &u, lambda),
        lambda,
    );
}

pub fn closest_to_line_v3(p: &[f64; 3], l1: &[f64; 3], l2: &[f64; 3]) -> ([f64; 3], f64)
{
    let u = sub_v3v3(l2, l1);
    let h = sub_v3v3(p, l1);
    let lambda = dot_v3v3(&u, &h) / dot_v3v3(&u, &u);
    return (
        madd_v3v3fl(&l1, &u, lambda),
        lambda,
    );
}

// point closest to v1 on line v2-v3 in 3D
// a version of closest_to_line_v3 that clamps
pub fn closest_to_line_segment_v2(p: &[f64; 2], l1: &[f64; 2], l2: &[f64; 2]) -> ([f64; 2], f64)
{
    let u = sub_v2v2(l2, l1);
    let h = sub_v2v2(p, l1);
    let lambda = dot_v2v2(&u, &h) / dot_v2v2(&u, &u);
    return {
        if !(lambda > 0.0) {
            (l1.clone(), 0.0)
        } else if !(lambda < 1.0) {
            (l2.clone(), 1.0)
        } else {
            (madd_v2v2fl(&l1, &u, lambda), lambda)
        }
    }
}
pub fn closest_to_line_segment_v3(p: &[f64; 3], l1: &[f64; 3], l2: &[f64; 3]) -> ([f64; 3], f64)
{
    let u = sub_v3v3(l2, l1);
    let h = sub_v3v3(p, l1);
    let lambda = dot_v3v3(&u, &h) / dot_v3v3(&u, &u);
    return {
        if !(lambda > 0.0) {
            (l1.clone(), 0.0)
        } else if !(lambda < 1.0) {
            (l2.clone(), 1.0)
        } else {
            (madd_v3v3fl(&l1, &u, lambda), lambda)
        }
    }
}

pub fn dist_squared_to_line_segment_v2(p: &[f64; 2], l1: &[f64; 2], l2: &[f64; 2]) -> f64 {
    let (closest, _lambda) = closest_to_line_segment_v2(p, l1, l2);
    return len_squared_v2v2(&closest, p);
}
pub fn dist_squared_to_line_segment_v3(p: &[f64; 3], l1: &[f64; 3], l2: &[f64; 3]) -> f64 {
    let (closest, _lambda) = closest_to_line_segment_v3(p, l1, l2);
    return len_squared_v3v3(&closest, p);
}

pub fn dist_to_line_segment_v2(p: &[f64; 2], l1: &[f64; 2], l2: &[f64; 2]) -> f64 {
    return dist_squared_to_line_segment_v2(p, l1, l2).sqrt();
}
pub fn dist_to_line_segment_v3(p: &[f64; 3], l1: &[f64; 3], l2: &[f64; 3]) -> f64 {
    return dist_squared_to_line_segment_v3(p, l1, l2).sqrt();
}

// ----------------------------------------------------------------------------
// Planes

///
/// Calculate a plane from a point and a direction,
/// \note \a point_no isn't required to be normalized.
///
pub fn plane_from_point_normal_v3(
    plane_co: &[f64; 3], plane_no: &[f64; 3],
) -> [f64; 4] {
    [
        plane_no[0],
        plane_no[1],
        plane_no[2],
        -dot_v3v3(plane_no, plane_co),
    ]
}

///
/// Get a point and a direction from a plane.
///
pub fn plane_to_point_vector_v3(
    plane: &[f64; 4],
) -> ([f64; 3], [f64; 3]) {
    (
        mul_v3fl(as_v3!(plane), -plane[3] / len_squared_v3(as_v3!(plane))),
        *as_v3!(plane),
    )
}

///
/// version of #plane_to_point_vector_v3 that gets a unit length vector.
///
pub fn plane_to_point_vector_v3_normalized(
    plane: &[f64; 4],
) -> ([f64; 3], [f64; 3]) {
    let (plane_no, length) = normalized_v3(as_v3!(plane));
    return (
        mul_v3fl(&plane_no, (-plane[3] / length)),
        plane_no,
    );
}

pub fn plane_point_side_v3(
    plane: &[f64; 4], co: &[f64; 3],
) -> f64 {
    return dot_v3v3(co, as_v3!(plane)) + plane[3];
}
