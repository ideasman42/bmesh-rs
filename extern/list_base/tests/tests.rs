// Apache License, Version 2.0

extern crate list_base;

extern crate plain_ptr;
use ::plain_ptr::{
    PtrMut,
    // PtrConst,
    null_mut,
    // null_const,
};

use list_base::{
    ListBase,
    ListBaseElemUtils,
};

#[derive(Debug)]
struct Item {
    next: PtrMut<Item>,
    prev: PtrMut<Item>,
    value: i64,
}

impl Item {
    pub fn new(value: i64) -> Self {
        Item {
            next: null_mut(),
            prev: null_mut(),
            value: value,
        }
    }
}

impl ListBaseElemUtils for Item {
    #[inline] fn next_get(&self) -> PtrMut<Self> { self.next }
    #[inline] fn prev_get(&self) -> PtrMut<Self> { self.prev }
    #[inline] fn next_set(&mut self, ptr: PtrMut<Self>) { self.next = ptr; }
    #[inline] fn prev_set(&mut self, ptr: PtrMut<Self>) { self.prev = ptr; }
}

// ------------
// Test Helpers

/* local validation function */
fn listbase_is_valid(listbase: &ListBase<Item>) -> bool {
    macro_rules! test_fail {
        ($test:expr) => {
            if !($test) {
                return false;
            }
        }
    }

    if listbase.head != null_mut() {
        let mut prev;
        let mut link;
        link = listbase.head;
        test_fail!(link.prev == null_mut());

        link = listbase.tail;
        test_fail!(link.next == null_mut());

        prev = null_mut();
        link = listbase.head;
        loop {
            test_fail!(link.prev == prev);

            prev = link;
            if prev == null_mut() {
                break;
            }
            link = link.next;
            if link == null_mut() {
                break;
            }
        }
        test_fail!(prev == listbase.tail);

        prev = null_mut();
        link = listbase.tail;
        loop {
            test_fail!(link.next == prev);

            prev = link;
            if prev == null_mut() {
                break;
            }
            link = link.prev;
            if link == null_mut() {
                break;
            }
        }
        test_fail!(prev == listbase.head);
    } else {
        test_fail!(listbase.tail == null_mut());
    }

    return true;
}

fn as_vec(ls: &ListBase<Item>) -> Vec<i64> {
    assert!(listbase_is_valid(ls));
    return ls.iter().map(|x| {x.value}).collect::<Vec<i64>>();
}

macro_rules! item_tuple {
    ($($arg:expr),*) => {
        (
            $(Item::new($arg),)*
        )
    }
}

macro_rules! ptr_tuple {
    ($($arg:expr),*) => {
        (
            $(Item::new($arg),)*
        )
    }
}

#[test]
fn test_construct() {
    let mut ls = ListBase::<Item>::new();
    let (mut a, mut b, mut c) = item_tuple!(1, 10, 100);
    ls.push_back(PtrMut(&mut b));
    ls.push_front(PtrMut(&mut a));
    ls.push_back(PtrMut(&mut c));

    assert_eq!(3, ls.len_calc());

    assert_eq!(vec![1, 10, 100], as_vec(&ls));
}

#[test]
fn test_reverse() {
    let mut ls = ListBase::<Item>::new();

    assert_eq!(true, ls.is_empty());
    ls.reverse();
    assert_eq!(true, ls.is_empty());

    let (mut a, mut b, mut c, mut d) = item_tuple!(1, 10, 100, 1000);
    for i in &mut [&mut a, &mut b, &mut c, &mut d] {
        ls.push_back(PtrMut(*i));
    }

    assert_eq!(vec![1, 10, 100, 1000], as_vec(&ls));
    ls.reverse();
    assert_eq!(vec![1000, 100, 10, 1], as_vec(&ls));

    ls.remove(PtrMut(&mut b));

    assert_eq!(vec![1000, 100, 1], as_vec(&ls));

    ls.reverse();

    assert_eq!(vec![1, 100, 1000], as_vec(&ls));

    assert_eq!(3, ls.len_calc());

    ls.remove(PtrMut(&mut a));
    ls.remove(PtrMut(&mut c));

    assert_eq!(vec![1000], as_vec(&ls));
    ls.reverse();
    assert_eq!(vec![1000], as_vec(&ls));
    assert_eq!(1, ls.len_calc());
}

#[test]
fn test_swap() {
    let mut ls = ListBase::<Item>::new();
    let (mut a, mut b, mut c, mut d) = item_tuple!(1, 10, 100, 1000);
    for i in &mut [&mut a, &mut b, &mut c, &mut d] {
        ls.push_back(PtrMut(*i));
    }

    // head/tail
    assert_eq!(vec![1, 10, 100, 1000], as_vec(&ls));
    ls.swap_links(PtrMut(&mut a), PtrMut(&mut d));
    assert_eq!(vec![1000, 10, 100, 1], as_vec(&ls));
    ls.swap_links(PtrMut(&mut a), PtrMut(&mut d));

    // head 2x
    assert_eq!(vec![1, 10, 100, 1000], as_vec(&ls));
    ls.swap_links(PtrMut(&mut a), PtrMut(&mut b));
    assert_eq!(vec![10, 1, 100, 1000], as_vec(&ls));
    ls.swap_links(PtrMut(&mut a), PtrMut(&mut b));

    // tail 2x
    assert_eq!(vec![1, 10, 100, 1000], as_vec(&ls));
    ls.swap_links(PtrMut(&mut c), PtrMut(&mut d));
    assert_eq!(vec![1, 10, 1000, 100], as_vec(&ls));
    ls.swap_links(PtrMut(&mut c), PtrMut(&mut d));
}

#[test]
fn test_index() {
    let mut ls = ListBase::<Item>::new();
    let (mut a, mut b, mut c, mut d) = item_tuple!(1, 10, 100, 1000);
    // intentionally omit 'd'
    let mut index = 0;
    for i in &mut [&mut a, &mut b, &mut c] {
        assert_eq!(false, ls.contains(PtrMut(*i)));
        assert_eq!(true, ls.index_at(PtrMut(*i)).is_none());
        assert_eq!(true, ls.at_index(index).is_none());
        ls.push_back(PtrMut(*i));
        assert_eq!(true, ls.contains(PtrMut(*i)));
        assert_eq!(index, ls.index_at(PtrMut(*i)).unwrap());
        assert_eq!(PtrMut(*i), ls.at_index(index).unwrap());
        index += 1;
    }

    assert_eq!(vec![1, 10, 100], as_vec(&ls));

    assert_eq!(false, ls.contains(PtrMut(&mut d)));
    ls.push_front(PtrMut(&mut d));
    assert_eq!(true, ls.contains(PtrMut(&mut d)));
}
