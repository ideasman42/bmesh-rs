// Apache License, Version 2.0

extern crate polyfill_2d;
use polyfill_2d::Real;

macro_rules! elem {
    ($val:expr, $($var:expr), *) => {
        $($val == $var) || *
    }
}

macro_rules! poly_tri_count { ($body:expr) => { ($body) - 2 } }

mod math {
    use polyfill_2d::Real;

    ///
    /// Scalar cross product of a 2d polygon.
    ///
    /// - equivalent to ``area * 2``
    /// - useful for checking polygon winding (a positive value is clockwise).
    ///
    pub fn cross_poly_v2(coords: &Vec<[Real; 2]>) -> Real {
        // The Trapezium Area Rule
        let mut co_prev = &coords[coords.len() - 1];
        let mut cross: Real = 0.0;
        for co_curr in coords {
            cross += (co_curr[0] - co_prev[0]) * (co_curr[1] + co_prev[1]);
            co_prev = co_curr;
        }
        return cross;
    }

    pub fn area_poly_v2(coords: &Vec<[Real; 2]>) -> Real {
        (0.5 * cross_poly_v2(coords)).abs()
    }

    pub fn cross_tri_v2(v1: &[Real; 2], v2: &[Real; 2], v3: &[Real; 2]) -> Real {
        (v1[0] - v2[0]) * (v2[1] - v3[1]) +
        (v1[1] - v2[1]) * (v3[0] - v2[0])
    }

    pub fn area_tri_signed_v2(v1: &[Real; 2], v2: &[Real; 2], v3: &[Real; 2]) -> Real {
        0.5 * ((v1[0] - v2[0]) * (v2[1] - v3[1]) + (v1[1] - v2[1]) * (v3[0] - v2[0]))
    }

    pub fn area_tri_v2(v1: &[Real; 2], v2: &[Real; 2], v3: &[Real; 2]) -> Real {
        area_tri_signed_v2(v1, v2, v3).abs()
    }

}


///
/// Basic check for face index values:
///
/// - no duplicates.
/// - all tris set.
/// - all verts used at least once.
///
fn test_polyfill_simple(coords: &Vec<[Real; 2]>, tris: &Vec<[u32; 3]>) {
    let mut tot_used: Vec<u32> = vec![0; coords.len()];
    for t in tris {
        for j in 0..3 {
            assert!((t[j] as usize) < coords.len());
            tot_used[(t[j] as usize)] += 1;
        }
        assert!(t[0] != t[1]);
        assert!(t[1] != t[2]);
        assert!(t[2] != t[0]);
    }
    for i in tot_used {
        assert!(i != 0);
    }
}
fn test_polyfill_topology(coords: &Vec<[Real; 2]>, tris: &Vec<[u32; 3]>) {
    use std::collections::HashMap;
    use std::mem::swap;

    let edgehash_final_len = coords.len() + coords.len() - 3;
    let mut edgehash: HashMap<(u32, u32), usize> = HashMap::with_capacity(edgehash_final_len);
    for t in tris {
        let mut k = 2;
        for j in 0..3 {
            let mut edge = (t[j], t[k]);
            if edge.0 > edge.1 { swap(&mut edge.0, &mut edge.1); }
            let counter = edgehash.entry(edge).or_insert(0);
            *counter += 1;
            k = j;
        }
    }
    assert_eq!(edgehash.len(), edgehash_final_len);

    let mut k = coords.len() - 1;
    for j in 0..coords.len() {
        let mut edge = (j as u32, k as u32);
        if edge.0 > edge.1 { swap(&mut edge.0, &mut edge.1); }
        let users = edgehash[&edge];
        assert!(users == 1);
        k = j;
    }

    for users in edgehash.values() {
        assert!(elem!(*users, 1, 2));
    }
}
fn test_polyfill_winding(coords: &Vec<[Real; 2]>, tris: &Vec<[u32; 3]>) {
    use ::math;
    let mut count: [u32; 2] = [0, 0];
    for t in tris {
        let winding_test = math::cross_tri_v2(
            &coords[t[0] as usize],
            &coords[t[1] as usize],
            &coords[t[2] as usize]);

        if winding_test.abs() > 1e-8 {
            count[if winding_test < 0.0 { 1 } else { 0 }] += 1;
        }
    }

    assert!(elem!(0, count[0], count[1]));
}
fn test_polyfill_area(coords: &Vec<[Real; 2]>, tris: &Vec<[u32; 3]>) {
    use ::math;
    let area_tot = math::area_poly_v2(coords);
    let mut area_tot_tris = 0.0;
    let eps_abs = 0.00001;
    let eps = if area_tot > 1.0 { (area_tot * eps_abs) } else { eps_abs };
    for t in tris {
        area_tot_tris += math::area_tri_v2(&coords[t[0] as usize],
                                           &coords[t[1] as usize],
                                           &coords[t[2] as usize]);
    }
    assert!((area_tot - area_tot_tris) <= eps);
}

fn coords_test_tri_single(
    id: &str,
    is_degenerate: bool,
    coords: &Vec<[Real; 2]>,
    do_write: bool)
{
    let mut tris = Vec::with_capacity(poly_tri_count!(coords.len()));

    polyfill_2d::calc(coords, 0, &mut tris);

    assert_eq!(tris.len(), poly_tri_count!(coords.len()));

    test_polyfill_simple(coords, &tris);
    test_polyfill_topology(coords, &tris);
    if is_degenerate == false {
        test_polyfill_winding(coords, &tris);
        test_polyfill_area(coords, &tris);
    }

    if do_write {
        match write_svg(id, coords, &tris) {
            Ok(_) => (),
            Err(e) => println!("Error {:?}", e),
        }
    }
}

fn coords_test_tri(
    id: &str,
    is_degenerate: bool,
    mut coords: Vec<[Real; 2]>)
{
    // so we can identify specific states
    let mut state = 0;
    for direction in 0..2 {

        if direction == 1 {
            coords.reverse();
        }

        for _ in 0..coords.len() {
            coords_test_tri_single(id, is_degenerate, &coords, state == 0);
            state += 1;
            {
                let co = &mut coords.remove(0);
                coords.push(*co);
            }
        }
    }
}


fn write_svg(id: &str,
             coords: &Vec<[Real; 2]>,
             tris: &Vec<[u32; 3]>)
             -> Result<(), ::std::io::Error> {
    use std::io::prelude::Write;

    let mut f = ::std::fs::File::create(format!("{}.svg", id)).unwrap();
    let w: Real = 512.0;
    let h: Real = 512.0;
    let margin_fac = 0.1;
    let w_margin = w * margin_fac;
    let h_margin = h * margin_fac;

    struct Bounds {
        xmin: Real, ymin: Real,
        xmax: Real, ymax: Real,

        xspan: Real, yspan: Real,
    }

    let mut bounds = Bounds {
        xmin: coords[0][0], ymin: coords[0][1],
        xmax: coords[0][0], ymax: coords[0][1],

        xspan: 0.0, yspan: 0.0,
    };

    for co in coords {
        if bounds.xmin > co[0] { bounds.xmin = co[0]; }
        if bounds.ymin > co[1] { bounds.ymin = co[1]; }
        if bounds.xmax < co[0] { bounds.xmax = co[0]; }
        if bounds.ymax < co[1] { bounds.ymax = co[1]; }
    }
    bounds.xspan = bounds.xmax - bounds.xmin;
    bounds.yspan = bounds.ymax - bounds.ymin;

    try!(writeln!(f, "<?xml version='1.0' encoding='UTF-8'?>"));
    try!(writeln!(f, "<svg version='1.1' height='{}' width='{}' viewBox='{} {} {} {}'>",
        w + w_margin, h + h_margin,
        -w_margin, -h_margin,
        w + w_margin, h + h_margin));

    try!(writeln!(f,
        "  <rect x='{}' y='{}' width='{}' height='{}' fill='black'/>",
        -w_margin, -h_margin, w + w_margin, h + h_margin));

    try!(writeln!(f,
                  "  <g stroke='white' \
                  stroke-opacity='0.75' \
                  stroke-width='1' \
                  fill='white' \
                  fill-opacity='0.5' \
                  >"));
    for tri in tris {
        try!(f.write(b"    <polyline points='"));
        for i in 0..4 {
            let index = tri[i % 3] as usize;
            try!(f.write_fmt(format_args!(
                "{},{} ",
                ((coords[index][0] - bounds.xmin) / bounds.xspan) * w,
                ((coords[index][1] - bounds.ymin) / bounds.yspan) * h
            )));
        }
        try!(writeln!(f, "' />"));
    }
    try!(writeln!(f, "  </g>"));
    try!(writeln!(f, "</svg>"));

    Ok(())
}

macro_rules! polyfill_test {
    ($id:ident, $is_degenerate:expr, $coords:expr) => {
        #[test]
        #[allow(non_snake_case)]  // we could rename, easier not for now
        fn $id() {
            coords_test_tri(stringify!($id), $is_degenerate, $coords);
        }
    }
}

// Simple examples

polyfill_test!(tri_cw, false, vec![[0.0, 0.0], [0.0, 1.0], [1.0, 0.5]]);
polyfill_test!(tri_ccw, false, vec![[0.0, 0.0], [1.0, 0.5], [0.0, 1.0]]);

/* A counterclockwise triangle */
polyfill_test!(TriangleCCW, false, vec![[0.0, 0.0], [0.0, 1.0], [1.0, 0.0]]);

/* A counterclockwise square */
polyfill_test!(SquareCCW, false, vec![[0.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.0, 0.0]]);

/* A clockwise square */
polyfill_test!(SquareCW, false, vec![[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0]]);

/* Starfleet insigna */
polyfill_test!(Starfleet, false, vec![[0.0, 0.0], [0.6, 0.4], [1.0, 0.0], [0.5, 1.0]]);

/* Starfleet insigna with repeated point */
polyfill_test!(StarfleetDegenerate, false, vec![
    [0.0, 0.0], [0.6, 0.4], [0.6, 0.4], [1.0, 0.0], [0.5, 1.0]]);

/* Three collinear points */
polyfill_test!(_3Colinear, false, vec![[0.0, 0.0], [1.0, 0.0], [2.0, 0.0]]);

/* Four collinear points */
polyfill_test!(_4Colinear, false, vec![[0.0, 0.0], [1.0, 0.0], [2.0, 0.0], [3.0, 0.0]]);

/* Non-consecutive collinear points */
polyfill_test!(UnorderedColinear, false, vec![
    [0.0, 0.0], [1.0, 1.0], [2.0, 0.0], [3.0, 1.0], [4.0, 0.0]]);

/* Plus shape */
polyfill_test!(PlusShape, false, vec![
    [1.0, 0.0], [2.0, 0.0], [2.0, 1.0], [3.0, 1.0], [3.0, 2.0], [2.0, 2.0],
    [2.0, 3.0], [1.0, 3.0], [1.0, 2.0], [0.0, 2.0], [0.0, 1.0], [1.0, 1.0]]);

/* Star shape */
polyfill_test!(StarShape, false, vec![
    [4.0, 0.0], [5.0, 3.0], [8.0, 4.0], [5.0, 5.0],
    [4.0, 8.0], [3.0, 5.0], [0.0, 4.0], [3.0, 3.0]]);

/* U shape */
polyfill_test!(UShape, false, vec![
    [1.0, 0.0], [2.0, 0.0], [3.0, 1.0], [3.0, 3.0], [2.0, 3.0],
    [2.0, 1.0], [1.0, 1.0], [1.0, 3.0], [0.0, 3.0], [0.0, 1.0]]);

/* Spiral */
polyfill_test!(Spiral, false, vec![
    [1.0, 0.0], [4.0, 0.0], [5.0, 1.0], [5.0, 4.0],
    [4.0, 5.0], [1.0, 5.0], [0.0, 4.0], [0.0, 3.0],
    [1.0, 2.0], [2.0, 2.0], [3.0, 3.0], [1.0, 3.0],
    [1.0, 4.0], [4.0, 4.0], [4.0, 1.0], [0.0, 1.0]]);

/* Test case from http:# www.flipcode.com/archives/Efficient_Polygon_Triangulation.shtml */
polyfill_test!(TestFlipCode, false, vec![
    [0.0, 6.0], [0.0, 0.0], [3.0, 0.0], [4.0, 1.0],
    [6.0, 1.0], [8.0, 0.0], [12.0, 0.0], [13.0, 2.0],
    [8.0, 2.0], [8.0, 4.0], [11.0, 4.0], [11.0, 6.0],
    [6.0, 6.0], [4.0, 3.0], [2.0, 6.0]]);

/* Self-intersection */
polyfill_test!(SelfIntersect, true, vec![
    [0.0, 0.0], [1.0, 1.0], [2.0, -1.0], [3.0, 1.0], [4.0, 0.0]]);

/* Self-touching */
polyfill_test!(SelfTouch, false, vec![
    [0.0, 0.0], [4.0, 0.0], [4.0, 4.0], [2.0, 4.0], [2.0, 3.0], [3.0, 3.0], [3.0, 1.0],
    [1.0, 1.0], [1.0, 3.0], [2.0, 3.0], [2.0, 4.0], [0.0, 4.0]]);

/* Self-overlapping */
polyfill_test!(SelfOverlap, true, vec![
    [0.0, 0.0], [4.0, 0.0], [4.0, 4.0], [1.0, 4.0], [1.0, 3.0], [3.0, 3.0], [3.0, 1.0],
    [1.0, 1.0], [1.0, 3.0], [3.0, 3.0], [3.0, 4.0], [0.0, 4.0]]);

/* Test case from http:# www.davdata.nl/math/polygons.html */
polyfill_test!(TestDavData, false, vec![
    [190.0, 480.0], [140.0, 180.0], [310.0, 100.0], [330.0, 390.0], [290.0, 390.0], [280.0,
    260.0], [220.0, 260.0], [220.0, 430.0], [370.0, 430.0], [350.0, 30.0], [50.0, 30.0],
    [160.0, 560.0], [730.0, 510.0], [710.0, 20.0], [410.0, 30.0], [470.0, 440.0], [640.0,
    410.0], [630.0, 140.0], [590.0, 140.0], [580.0, 360.0], [510.0, 370.0], [510.0, 60.0],
    [650.0, 70.0], [660.0, 450.0], [190.0, 480.0]]);

/* Issue 815.0, http:# code.google.com/p/libgdx/issues/detail?id=815 */
polyfill_test!(Issue815, false, vec![
    [-2.0, 0.0], [-2.0, 0.5], [0.0, 1.0], [0.5, 2.875],
    [1.0, 0.5], [1.5, 1.0], [2.0, 1.0], [2.0, 0.0]]);

/* Issue 207.0, comment #1, http:# code.google.com/p/libgdx/issues/detail?id=207#c1 */
polyfill_test!(Issue207_1, true, vec![
    [72.42465, 197.07095], [78.485535, 189.92776], [86.12059, 180.92929],
    [99.68253, 164.94557], [105.24325, 165.79604], [107.21862, 166.09814],
    [112.41958, 162.78253], [113.73238, 161.94562], [123.29477, 167.93805],
    [126.70667, 170.07617], [73.22717, 199.51062]]);

/* Issue 207.0, comment #11, http:# code.google.com/p/libgdx/issues/detail?id=207#c11 */
/* Also on issue 1081.0, http:# code.google.com/p/libgdx/issues/detail?id=1081 */
polyfill_test!(Issue207_11, false, vec![
    [2400.0, 480.0], [2400.0, 176.0], [1920.0, 480.0], [1920.0459, 484.22314],
    [1920.1797, 487.91016], [1920.3955, 491.0874], [1920.6875, 493.78125], [1921.0498, 496.01807],
    [1921.4766, 497.82422], [1921.9619, 499.22607], [1922.5, 500.25], [1923.085, 500.92236],
    [1923.7109, 501.26953], [1924.3721, 501.31787], [1925.0625, 501.09375], [1925.7764, 500.62354],
    [1926.5078, 499.9336], [1927.251, 499.0503], [1928.0, 498.0], [1928.749, 496.80908],
    [1929.4922, 495.5039], [1930.2236, 494.11084], [1930.9375, 492.65625], [1931.6279, 491.1665],
    [1932.2891, 489.66797], [1932.915, 488.187], [1933.5, 486.75], [1934.0381, 485.3833],
    [1934.5234, 484.11328], [1934.9502, 482.9663], [1935.3125, 481.96875], [1935.6045, 481.14697],
    [1935.8203, 480.52734], [1935.9541, 480.13623], [1936.0, 480.0]]);

/* Issue 1407.0, http:# code.google.com/p/libgdx/issues/detail?id=1407 */
polyfill_test!(Issue1407, false, vec![
    [3.914329, 1.9008259], [4.414321, 1.903619], [4.8973203, 1.9063174], [5.4979978, 1.9096732]]);

/* Issue 1407.0, http:# code.google.com/p/libgdx/issues/detail?id=1407, */
/* with an additional point to show what is happening. */
polyfill_test!(Issue1407_pt, false, vec![
    [3.914329, 1.9008259], [4.414321, 1.903619], [4.8973203, 1.9063174],
    [5.4979978, 1.9096732], [4.0, 4.0],
]);

/* Simplified from Blender bug T40777 */
polyfill_test!(IssueT40777_colinear, false, vec![
    [0.7, 0.37], [0.7, 0.0], [0.76, 0.0], [0.76, 0.4], [0.83, 0.4], [0.83, 0.0], [0.88, 0.0],
    [0.88, 0.4], [0.94, 0.4], [0.94, 0.0], [1.0, 0.0], [1.0, 0.4], [0.03, 0.62], [0.03, 0.89],
    [0.59, 0.89], [0.03, 1.0], [0.0, 1.0], [0.0, 0.0], [0.03, 0.0], [0.03, 0.37]]);
polyfill_test!(IssueT41986_axis_align, false, vec![
    [-0.25, -0.07], [-0.25, 0.27], [-1.19, 0.14], [-0.06, 0.73], [0.17, 1.25], [-0.25, 1.07],
    [-0.38, 1.02], [-0.25, 0.94], [-0.40, 0.90], [-0.41, 0.86], [-0.34, 0.83], [-0.25, 0.82],
    [-0.66, 0.73], [-0.56, 1.09], [-0.25, 1.10], [0.00, 1.31], [-0.03, 1.47], [-0.25, 1.53],
    [0.12, 1.62], [0.36, 1.07], [0.12, 0.67], [0.29, 0.57], [0.44, 0.45], [0.57, 0.29],
    [0.66, 0.12], [0.68, 0.06], [0.57, -0.36], [-0.25, -0.37], [0.49, -0.74], [-0.59, -1.21],
    [-0.25, -0.15], [-0.46, -0.52], [-1.08, -0.83], [-1.45, -0.33], [-1.25, -0.04]]);

polyfill_test!(IssueT41986_axis_align_co_linear, false, vec![
    [40.0, 0.0], [36.0, 0.0], [36.0, 5.0], [35.0, 5.0], [35.0, 0.0], [30.0, 0.0], [30.0, 5.0],
    [29.0, 5.0], [29.0, 0.0], [24.0, 0.0], [24.0, 3.0], [23.0, 4.0], [23.0, 0.0], [18.0, 0.0],
    [18.0, 5.0], [17.0, 5.0], [17.0, 0.0], [12.0, 0.0], [12.0, 5.0], [11.0, 5.0], [11.0, 0.0],
    [6.0, 0.0], [6.0, 5.0], [5.0, 5.0], [5.0, 0.0], [0.0, 0.0], [0.0, 5.0], [-1.0, 5.0],
    [-1.0, 0.0], [-6.0, 0.0], [-9.0, -3.0], [-6.0, -3.0], [-6.0, -2.0], [-1.0, -2.0],
    [0.0, -2.0], [5.0, -2.0], [6.0, -2.0], [11.0, -2.0], [12.0, -2.0], [17.0, -2.0],
    [18.0, -2.0], [23.0, -2.0], [24.0, -2.0], [29.0, -2.0], [30.0, -2.0], [35.0, -2.0],
    [36.0, -2.0], [40.0, -2.0]]);

// Complex examples!

polyfill_test!(complex_correct, false, vec![
    [-3.2, -0.6], [-2.5, -0.2], [-1.7, 2.0], [-1.5, 1.1], [-0.4, 2.0],
    [-2.8, 2.3], [-2.9, 0.6], [-3.3, 0.6], [-3.9, 3.4], [0.5, 2.45],
    [2.3, -0.9], [-0.4, -1.3], [0.35, 2.3], [-3.5, 3.1], [-3.1, 1.0],
    [-3.1, 2.8], [-1.3, 2.6], [0.0, 2.1], [-0.8, -1.7], [3.0, -1.2],
    [0.1, 4.4], [0.4, 4.5], [4.4, -2.5], [-3.1, -3.9], [-3.4, -3.2],
    [3.5, -2.3], [3.05, -1.35], [-2.7, -2.2], [-0.2, 1.8], [-1.9, 0.1],
    [-1.7, 1.2], [-2.5, -0.6], [-3.2, -0.8],
]);

polyfill_test!(complex_correct_smooth, false, vec![
    [0.4503, 0.4327], [0.4251, 0.3074], [0.3413, 0.2834], [0.2163, 0.3163],
    [0.06739, 0.3614], [-0.08816, 0.3744], [-0.233, 0.3108], [-0.3499, 0.1259],
    [-0.3862, 0.01164], [-0.4041, -0.07595], [-0.4123, -0.1437], [-0.4198, -0.1985],
    [-0.4352, -0.2471], [-0.4675, -0.2965], [-0.5254, -0.3536], [-0.6178, -0.4251],
    [-0.7391, -0.5627], [-0.7983, -0.7424], [-0.8031, -0.9501], [-0.7613, -1.172],
    [-0.6806, -1.394], [-0.5689, -1.601], [-0.4339, -1.78], [-0.2833, -1.917],
    [-0.1134, -1.999], [0.04974, -2.012], [0.1979, -1.965], [0.3228, -1.869],
    [0.4164, -1.733], [0.4705, -1.568], [0.477, -1.384], [0.4276, -1.192],
    [0.387, -1.082], [0.3528, -0.965], [0.3257, -0.8485], [0.3067, -0.7386],
    [0.2967, -0.6417], [0.2967, -0.5645], [0.3075, -0.5133], [0.33, -0.4948],
    [0.5427, -0.4551], [0.7215, -0.3454], [0.8616, -0.18], [0.9584, 0.02686],
    [1.007, 0.2608], [1.003, 0.5076], [0.9416, 0.753], [0.8178, 0.9826],
    [0.6534, 1.158], [0.4368, 1.311], [0.1818, 1.438], [-0.09784, 1.539],
    [-0.3885, 1.608], [-0.6764, 1.645], [-0.9479, 1.647], [-1.189, 1.61],
    [-1.373, 1.548], [-1.549, 1.461], [-1.708, 1.351], [-1.842, 1.221],
    [-1.944, 1.076], [-2.005, 0.9179], [-2.016, 0.751], [-1.97, 0.5784],
    [-1.928, 0.4855], [-1.886, 0.3894], [-1.847, 0.2912], [-1.814, 0.1916],
    [-1.789, 0.09159], [-1.778, -0.007957], [-1.781, -0.1062], [-1.803, -0.2021],
    [-1.895, -0.4392], [-2.004, -0.6715], [-2.123, -0.9012], [-2.242, -1.13],
    [-2.353, -1.361], [-2.448, -1.596], [-2.518, -1.836], [-2.555, -2.084],
    [-2.56, -2.274], [-2.549, -2.477], [-2.523, -2.675], [-2.481, -2.85],
    [-2.424, -2.985], [-2.353, -3.063], [-2.266, -3.066], [-2.165, -2.976],
    [-2.067, -2.821], [-1.994, -2.653], [-1.943, -2.475], [-1.909, -2.289],
    [-1.888, -2.098], [-1.876, -1.906], [-1.867, -1.714], [-1.858, -1.526],
    [-1.835, -1.179], [-1.802, -0.8298], [-1.755, -0.4809], [-1.692, -0.1351],
    [-1.61, 0.205], [-1.505, 0.5366], [-1.375, 0.857], [-1.217, 1.164],
    [-1.22, 0.7199], [-1.224, 0.1175], [-1.221, -0.5885], [-1.203, -1.343],
    [-1.164, -2.092], [-1.096, -2.781], [-0.9905, -3.354], [-0.8408, -3.756],
    [-0.7768, -3.881], [-0.7185, -4.016], [-0.6644, -4.147], [-0.6125, -4.263],
    [-0.5613, -4.348], [-0.5091, -4.392], [-0.4542, -4.38], [-0.3948, -4.3],
    [-0.2433, -4.09], [-0.01113, -3.863], [0.2794, -3.63], [0.6057, -3.401],
    [0.9456, -3.188], [1.276, -3.002], [1.576, -2.853], [1.821, -2.753],
    [1.886, -2.713], [1.849, -2.678], [1.735, -2.645], [1.567, -2.615],
    [1.368, -2.585], [1.161, -2.554], [0.9697, -2.523], [0.8178, -2.488],
    [0.6507, -2.399], [0.6113, -2.277], [0.6753, -2.129], [0.8178, -1.963],
    [1.014, -1.786], [1.24, -1.605], [1.471, -1.427], [1.682, -1.261],
    [1.751, -1.178], [1.765, -1.093], [1.735, -1.005], [1.671, -0.9152],
    [1.585, -0.8223], [1.488, -0.7262], [1.391, -0.6265], [1.306, -0.5227],
    [1.275, -0.3937], [1.306, -0.2055], [1.379, 0.0231], [1.476, 0.2731],
    [1.579, 0.5255], [1.669, 0.7615], [1.728, 0.9619], [1.738, 1.108],
    [1.638, 1.364], [1.457, 1.577], [1.212, 1.751], [0.9208, 1.89],
    [0.6013, 1.998], [0.2708, 2.077], [-0.05311, 2.133], [-0.353, 2.167],
    [-1.289, 2.169], [-2.204, 2.013], [-3.058, 1.709], [-3.813, 1.27],
    [-4.428, 0.7044], [-4.866, 0.02418], [-5.086, -0.7601], [-5.05, -1.638],
    [-5.037, -1.745], [-5.031, -1.876], [-5.028, -2.015], [-5.026, -2.147],
    [-5.022, -2.258], [-5.013, -2.332], [-4.995, -2.353], [-4.966, -2.307],
    [-4.784, -1.939], [-4.58, -1.646], [-4.355, -1.408], [-4.111, -1.202],
    [-3.849, -1.009], [-3.57, -0.8067], [-3.275, -0.5745], [-2.966, -0.2912],
    [-2.964, -0.2376], [-3.011, -0.149], [-3.07, -0.0218], [-3.108, 0.1475],
    [-3.09, 0.3624], [-2.98, 0.6266], [-2.744, 0.9436], [-2.346, 1.317],
    [-2.108, 1.456], [-1.787, 1.565], [-1.406, 1.647], [-0.994, 1.701],
    [-0.5755, 1.729], [-0.1771, 1.731], [0.1752, 1.71], [0.4554, 1.666],
    [0.8403, 1.495], [1.08, 1.219], [1.194, 0.8579], [1.205, 0.4346],
    [1.132, -0.02909], [0.9972, -0.5112], [0.8204, -0.9897], [0.6227, -1.443],
    [0.5033, -1.685], [0.3676, -1.928], [0.2198, -2.153], [0.06439, -2.339],
    [-0.09445, -2.468], [-0.2524, -2.521], [-0.405, -2.478], [-0.5481, -2.321],
    [-0.6298, -2.148], [-0.7035, -1.917], [-0.7676, -1.646], [-0.8203, -1.354],
    [-0.8598, -1.059], [-0.8845, -0.7799], [-0.8927, -0.5345], [-0.8826, -0.3415],
    [-0.829, -0.157], [-0.7229, 0.01097], [-0.5753, 0.163], [-0.3973, 0.2997],
    [-0.1998, 0.4216], [0.006199, 0.5292], [0.2097, 0.6231], [0.3997, 0.7038],
]);
