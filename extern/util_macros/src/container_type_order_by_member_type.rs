// Licensed: Apache 2.0

///
/// Macro to create a container type to be used in a 'BTreeSet' or ordered types
/// to behave like a map where a key in the struct is used for the key.
///
/// For example, data in a set may have a unique identifier which
/// can be used in the struct as well as a key for it's use in the set.
///
///
/// ```.text
/// // Defines 'MyTypeOrd', a container type for existing struct,
/// // using MyType.uuid is used as the key.
/// container_order_by_member_impl(MyTypeOrd, MyType, uuid);
/// ```
///
/// See: http://stackoverflow.com/questions/41035869

#[macro_export]
macro_rules! container_type_order_by_member_struct_impl {
    ($t_ord:ident, $t_base:ty, $t_member:ty, $($member_attr:ident).*) => {
        /// Caller must define the struct, see: container_type_order_by_member_impl
        // pub struct $t_ord(pub $t_base);
        impl PartialEq for $t_ord {
            fn eq(&self, other: &Self) -> bool {
                return (self.0).$($member_attr).*.eq(&(other.0).$($member_attr).*);
            }
        }
        impl PartialOrd for $t_ord {
            fn partial_cmp(&self, other: &Self) -> Option<::std::cmp::Ordering> {
                return (self.0).$($member_attr).*.partial_cmp(&(other.0).$($member_attr).*);
            }
        }
        impl Eq for $t_ord {}
        impl Ord for $t_ord {
            fn cmp(&self, other: &Self) -> ::std::cmp::Ordering {
                return (self.0).$($member_attr).*.cmp(&(other.0).$($member_attr).*);
            }
        }
        impl ::std::borrow::Borrow<$t_member> for $t_ord {
            fn borrow(&self) -> &$t_member {
                return &(self.0).$($member_attr).*;
            }
        }
    }
}

/// Macro that also defines structs.
#[macro_export]
macro_rules! container_type_order_by_member_impl {
    (pub $t_ord:ident, $t_base:ty, $t_member:ty, $($member_attr:ident).*) => {
        pub struct $t_ord(pub $t_base);
        container_type_order_by_member_struct_impl!(
            $t_ord, $t_base, $t_member, $($member_attr).*);
    };
    ($t_ord:ident, $t_base:ty, $($member_attr:ident).*) => {
        struct $t_ord(pub $t_base);
        container_type_order_by_member_struct_impl!(
            $t_ord, $t_base, $t_member, $($member_attr).*);
    };
}

/// Similar to container_type_order_by_member_struct_impl
/// but doesn't assume a container type.
#[macro_export]
macro_rules! order_by_member_struct_impl {
    ($t_base:ty, $t_member:ty, $($member_attr:ident).*) => {
        impl PartialEq for $t_base {
            fn eq(&self, other: &Self) -> bool {
                return (self).$($member_attr).*.eq(&(other).$($member_attr).*);
            }
        }
        impl PartialOrd for $t_base {
            fn partial_cmp(&self, other: &Self) -> Option<::std::cmp::Ordering> {
                return (self).$($member_attr).*.partial_cmp(&(other).$($member_attr).*);
            }
        }
        impl Eq for $t_base {}
        impl Ord for $t_base {
            fn cmp(&self, other: &Self) -> ::std::cmp::Ordering {
                return (self).$($member_attr).*.cmp(&(other).$($member_attr).*);
            }
        }
        impl ::std::borrow::Borrow<$t_member> for $t_base {
            fn borrow(&self) -> &$t_member {
                return &(self).$($member_attr).*;
            }
        }
    }
}
