// Licensed: Apache 2.0

// MIT, Copyright (c) 2015 bluss
//
// from: https://github.com/bluss/maplit/blob/master/src/lib.rs
// we may want to use the mapit crate.

/// Create a **HashSet** from a list of elements.
///
/// ## Example
///
/// ```
/// #[macro_use] extern crate util_macros;
/// # fn main() {
///
/// let set = hashset!{"a", "b"};
/// assert!(set.contains("a"));
/// assert!(set.contains("b"));
/// assert!(!set.contains("c"));
/// # }
/// ```
#[macro_export]
macro_rules! hashset {
    (@single $($x:tt)*) => (());
    (@count $($rest:expr),*) => (<[()]>::len(&[$(hashset!(@single $rest)),*]));
    ($($key:expr,)+) => { hashset!($($key),+) };
    ($($key:expr),*) => {
        {
            let _cap = hashset!(@count $($key),*);
            let mut _set = ::std::collections::HashSet::with_capacity(_cap);
            $(
                _set.insert($key);
            )*
            _set
        }
    };
}

/// Create a **HashMap** from a list of key-value pairs
///
/// ## Example
///
/// ```
/// #[macro_use] extern crate util_macros;
/// # fn main() {
///
/// let map = hashmap!{
///     "a" => 1,
///     "b" => 2,
/// };
/// assert_eq!(map["a"], 1);
/// assert_eq!(map["b"], 2);
/// assert_eq!(map.get("c"), None);
/// # }
/// ```
#[macro_export]
macro_rules! hashmap {
    (@single $($x:tt)*) => (());
    (@count $($rest:expr),*) => (<[()]>::len(&[$(hashmap!(@single $rest)),*]));

    ($($key:expr => $value:expr,)+) => { hashmap!($($key => $value),+) };
    ($($key:expr => $value:expr),*) => {
        {
            let _cap = hashmap!(@count $($key),*);
            let mut _map = ::std::collections::HashMap::with_capacity(_cap);
            $(
                _map.insert($key, $value);
            )*
            _map
        }
    };
}

#[macro_export]
macro_rules! vec_from_all {
    ($($t:tt) *) => {
        {
            let v = Vec::with_capacity(0 $(+ $tlen())*);
            $(
                v.extend(&$t);
            )*
            v
        }
    };
}

///
/// Avoid manually expanding an expression, eg:
///
/// let array =  unpack!([some.vec; 3]);
///
/// Expands into: [some.vec[0], some.vec[1], some.vec[2]]
///
/// Supports expanding into different bracket types based on the input args.
/// Add others as needed.
///
#[macro_export]
macro_rules! unpack {
    ([$v_:expr; 2]) => { { let v = $v_; [v[0], v[1]] } };
    (($v_:expr; 2)) => { { let v = $v_; (v[0], v[1]) } };
    ({$v_:expr; 2}) => { { let v = $v_; {v[0], v[1]} } };

    ([$v_:expr; 3]) => { { let v = $v_; [v[0], v[1], v[2]] } };
    (($v_:expr; 3)) => { { let v = $v_; (v[0], v[1], v[2]) } };
    ({$v_:expr; 3}) => { { let v = $v_; {v[0], v[1], v[2]} } };

    ([$v_:expr; 4]) => { { let v = $v_; [v[0], v[1], v[2], v[3]] } };
    (($v_:expr; 4)) => { { let v = $v_; (v[0], v[1], v[2], v[3]) } };
    ({$v_:expr; 4}) => { { let v = $v_; {v[0], v[1], v[2], v[3]} } };

    // reference versions
    ([$v_:expr; &2]) => { { let v = $v_; [&v[0], &v[1]] } };
    (($v_:expr; &2)) => { { let v = $v_; (&v[0], &v[1]) } };
    ({$v_:expr; &2}) => { { let v = $v_; {&v[0], &v[1]} } };

    ([$v_:expr; &3]) => { { let v = $v_; [&v[0], &v[1], &v[2]] } };
    (($v_:expr; &3)) => { { let v = $v_; (&v[0], &v[1], &v[2]) } };
    ({$v_:expr; &3}) => { { let v = $v_; {&v[0], &v[1], &v[2]} } };

    ([$v_:expr; &4]) => { { let v = $v_; [&v[0], &v[1], &v[2], &v[3]] } };
    (($v_:expr; &4)) => { { let v = $v_; (&v[0], &v[1], &v[2], &v[3]) } };
    ({$v_:expr; &4}) => { { let v = $v_; {&v[0], &v[1], &v[2], &v[3]} } };

    // reference mutable versions
    ([$v_:expr; &mut 2]) => { { let v = $v_; [&mut v[0], &mut v[1]] } };
    (($v_:expr; &mut 2)) => { { let v = $v_; (&mut v[0], &mut v[1]) } };
    ({$v_:expr; &mut 2}) => { { let v = $v_; {&mut v[0], &mut v[1]} } };

    ([$v_:expr; &mut 3]) => { { let v = $v_; [&mut v[0], &mut v[1], &mut v[2]] } };
    (($v_:expr; &mut 3)) => { { let v = $v_; (&mut v[0], &mut v[1], &mut v[2]) } };
    ({$v_:expr; &mut 3}) => { { let v = $v_; {&mut v[0], &mut v[1], &mut v[2]} } };

    ([$v_:expr; &mut 4]) => { { let v = $v_; [&mut v[0], &mut v[1], &mut v[2], &mut v[3]] } };
    (($v_:expr; &mut 4)) => { { let v = $v_; (&mut v[0], &mut v[1], &mut v[2], &mut v[3]) } };
    ({$v_:expr; &mut 4}) => { { let v = $v_; {&mut v[0], &mut v[1], &mut v[2], &mut v[3]} } };
}


#[cfg(target_endian = "little")]
#[macro_export]
macro_rules! u16_code {
    ($w:expr) => {(
        (($w[0] as u16) <<  0) |
        (($w[1] as u16) <<  8) |
        ((*$w as [u8; 2])[0] as u16 * 0)
    )}
}
#[cfg(target_endian = "big")]
#[macro_export]
macro_rules! u16_code {
    ($w:expr) => {(
        (($w[1] as u16) <<  0) |
        (($w[0] as u16) <<  8) |
        ((*$w as [u8; 2])[0] as u16 * 0)
    )}
}

#[cfg(target_endian = "little")]
#[macro_export]
macro_rules! u32_code {
    ($w:expr) => {(
        (($w[0] as u32) <<  0) |
        (($w[1] as u32) <<  8) |
        (($w[2] as u32) << 16) |
        (($w[3] as u32) << 24) |
        ((*$w as [u8; 4])[0] as u32 * 0)
    )}
}
#[cfg(target_endian = "big")]
#[macro_export]
macro_rules! u32_code {
    ($w:expr) => {(
        (($w[3] as u32) <<  0) |
        (($w[2] as u32) <<  8) |
        (($w[1] as u32) << 16) |
        (($w[0] as u32) << 24) |
        ((*$w as [u8; 4])[0] as u32 * 0)
    )}
}

#[cfg(target_endian = "little")]
#[macro_export]
macro_rules! u64_code {
    ($w:expr) => {(
        (($w[0] as u64) <<  0) |
        (($w[1] as u64) <<  8) |
        (($w[2] as u64) << 16) |
        (($w[3] as u64) << 24) |
        (($w[4] as u64) << 32) |
        (($w[5] as u64) << 40) |
        (($w[6] as u64) << 48) |
        (($w[7] as u64) << 56) |
        ((*$w as [u8; 8])[0] as u64 * 0)
    )}
}
#[cfg(target_endian = "big")]
#[macro_export]
macro_rules! u64_code {
    ($w:expr) => {(
        (($w[7] as u64) <<  0) |
        (($w[6] as u64) <<  8) |
        (($w[5] as u64) << 16) |
        (($w[4] as u64) << 24) |
        (($w[3] as u64) << 32) |
        (($w[2] as u64) << 40) |
        (($w[1] as u64) << 48) |
        (($w[0] as u64) << 56) |
        ((*$w as [u8; 8])[0] as u64 * 0)
    )}
}
