// Licensed: Apache 2.0

/// Implements C style enum with a single integer struct.
///
/// Declaration:
/// ```
/// struct_enum_int_impl!(pub struct MyEnum(pub u8));
/// ```
///
/// Usage:
/// ```
/// const ENUM_A: MyEnum = MyEnum(0);
/// const ENUM_B: MyEnum = MyEnum(1);
/// const ENUM_C: MyEnum = MyEnum(2);
///
/// // some common bitflag operations:
/// fn test(mut f: MyEnum) {
///     if f == FLAG_A {
///         foo();
///     }
/// }
/// ```

#[macro_export]
macro_rules! struct_enum_int_impl {

    // pub/pub
    (pub struct $name:ident ( pub $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug, Hash)]
        pub struct $name(pub $t);
        struct_enum_int_impl!(@generate_impl $name, $t);
    };
    // private/pub
    (struct $name:ident ( pub $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug, Hash)]
        struct $name(pub $t);
        struct_enum_int_impl!(@generate_impl $name, $t);
    };
    // pub/private
    (pub struct $name:ident ( $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug, Hash)]
        struct $name($t);
        struct_enum_int_impl!(@generate_impl $name, $t);
    };
    // private/private
    (struct $name:ident ( $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug, Hash)]
        struct $name($t);
        struct_enum_int_impl!(@generate_impl $name, $t);
    };

    // internal, don't call directly!
    (@generate_impl $t:ident, $t_base:ident) => {
        /// Support for comparing with the base type, allows comparison with 0.
        ///
        /// This is used in typical expressions, eg: `if (a & FLAG) != 0 { ... }`
        /// Having to use MyEnum(0) all over is too inconvenient.
        impl PartialEq<$t_base> for $t {
            #[inline]
            fn eq(&self, other: &$t_base) -> bool { self.0 == *other }
        }
    };

}
