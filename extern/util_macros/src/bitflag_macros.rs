// Licensed: Apache 2.0

/// Implements bitflag operators for integer struct.
///
/// Goals:
///
/// * Zero overhead on top of the plain integer types.
/// * Type-safety, to avoid accidentally mixing flags from different types.
/// * Keep it simple, not to define a new API for manipulating bits
///   (see `bitflags` crate).
///
/// Declaration:
/// ```
/// struct_bitflag_impl!(pub struct MyFlag(pub u8));
/// ```
///
/// Usage:
/// ```
/// const FLAG_A: MyFlag = MyFlag(1 << 0);
/// const FLAG_B: MyFlag = MyFlag(1 << 1);
/// const FLAG_C: MyFlag = MyFlag(1 << 2);
///
/// // some common bitflag operations:
/// fn test(mut f: MyFlag) {
///     if (f & FLAG_A) != 0 {
///         f &= !FLAG_B;
///     } else {
///         f ^= FLAG_C;
///     }
/// }
/// ```

#[macro_export]
macro_rules! struct_bitflag_impl {
    // pub/pub
    (pub struct $name:ident ( pub $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug)]
        pub struct $name(pub $t);
        struct_bitflag_impl!(@generate_impl $name, $t);
    };
    // private/pub
    (struct $name:ident ( pub $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug)]
        struct $name(pub $t);
        struct_bitflag_impl!(@generate_impl $name, $t);
    };
    // pub/private
    (pub struct $name:ident ( $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug)]
        struct $name($t);
        struct_bitflag_impl!(@generate_impl $name, $t);
    };
    // private/private
    (struct $name:ident ( $t:tt ) ) => {
        #[derive(PartialEq, Eq, Copy, Clone, Debug)]
        struct $name($t);
        struct_bitflag_impl!(@generate_impl $name, $t);
    };

    // internal pattern
    (@generate_impl $t:ident, $t_base:ident) => {
        impl ::std::ops::BitAnd for $t {
            type Output = $t;
            #[inline]
            fn bitand(self, _rhs: $t) -> $t { $t(self.0 & _rhs.0) }
        }
        impl ::std::ops::BitOr for $t {
            type Output = $t;
            #[inline]
            fn bitor(self, _rhs: $t) -> $t { $t(self.0 | _rhs.0) }
        }
        impl ::std::ops::BitXor for $t {
            type Output = $t;
            #[inline]
            fn bitxor(self, _rhs: $t) -> $t { $t(self.0 ^ _rhs.0) }
        }
        impl ::std::ops::Not for $t {
            type Output = $t;
            #[inline]
            fn not(self) -> $t { $t(!self.0) }
        }
        impl ::std::ops::BitAndAssign for $t {
            #[inline]
            fn bitand_assign(&mut self, _rhs: $t) { self.0 &= _rhs.0; }
        }
        impl ::std::ops::BitOrAssign for $t {
            #[inline]
            fn bitor_assign(&mut self, _rhs: $t) { self.0 |= _rhs.0; }
        }
        impl ::std::ops::BitXorAssign for $t {
            #[inline]
            fn bitxor_assign(&mut self, _rhs: $t) { self.0 ^= _rhs.0; }
        }
        /// Support for comparing with the base type, allows comparison with 0.
        ///
        /// This is used in typical expressions, eg: `if (a & FLAG) != 0 { ... }`
        /// Having to use MyFlag(0) all over is too inconvenient.
        impl PartialEq<$t_base> for $t {
            #[inline]
            fn eq(&self, other: &$t_base) -> bool { self.0 == *other }
        }
        /// API, keep it minimal,
        /// preferring regular bitmask operations
        impl $t {
            #[allow(dead_code)]
            #[inline]
            pub fn set(&mut self, other: $t, value: bool) {
                if value {
                    self.0 |= other.0;
                } else {
                    self.0 &= !other.0;
                }
            }
        }
    };
}

///
/// Optional flag methods:
/// So `(flag & FOO) != 0` can be replaced with `flag.foo_test()`
/// may be convenient to avoid having all flags available in the name-space.
///
/// For a given struct:
/// ```.text
/// struct_bitflag_impl!(pub struct MyFlag(pub u8));
/// const FOO: MyFlag = MyFlag(1 << 0);
/// ```
///
/// ```.text
/// struct_bitflag_flag_fn_impl!(
///     MyFlag, FOO,
///     foo_test, foo_enable, foo_disable, foo_set, foo_toggle);
/// ```
#[macro_export]
macro_rules! struct_bitflag_flag_fn_impl {
    ($t:ty, $flag:expr,
     test=$test_fn:tt,
     enable=$enable_fn:tt,
     disable=$disable_fn:tt,
     set=$set_fn:tt,
     toggle=$toggle_fn:tt
    ) => {
        impl $t {
            #[inline]
            pub fn $test_fn(&self) -> bool {
                (*self & $flag) != 0
            }
            #[inline]
            pub fn $enable_fn(&mut self) {
                *self |= $flag;
            }
            #[inline]
            pub fn $disable_fn(&mut self) {
                *self &= !$flag;
            }
            #[inline]
            pub fn $set_fn(&mut self, value: bool) {
                if value {
                    *self |= $flag;
                } else {
                    *self &= !$flag;
                }
            }
            #[inline]
            pub fn $toggle_fn(&mut self) {
                *self ^= $flag;
            }
        }
    }
}
