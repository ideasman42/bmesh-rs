// Licensed: Apache 2.0
// (c) Campbell Barton

// Basic Logic
//
// Statements we can't easily express in Rust.
//

/// Check if the first argument in any of the following arguments, eg:
///
/// ```.text
/// if elem!(my_var, FOO, BAR, BAZ) { ... }
/// ```
///
#[macro_export]
macro_rules! elem {
    ($val:expr, $($var:expr), *) => {
        $($val == $var) || *
    }
}

/// NOP for now, keep since this may be supported later.
#[macro_export]
macro_rules! unlikely {
    ($body:expr) => {
        $body
    }
}

/// Avoid writing many statements when using Into trait.
/// ```.text
/// a = a.into();
/// b = b.into();
/// c = c.into();
/// ```
///
/// instead simply write:
///
/// ```.text
/// into_expand(a, b, c);
/// ```
///
/// see: http://stackoverflow.com/a/40569658/432509
///
#[macro_export]
macro_rules! into_expand {
    ($($names:ident),*) => {
        $(let $names = ::std::convert::Into::into($names);)*
    };
}

/// Avoid writing deeply nested option checks, eg:
/// ```.text
/// if let Some(a) = foo_a() {
///     if let Some(b) = foo_b() {
///         if let Some(c) = foo_c() {
///             /* code */
///         }
///     }
/// }
/// ```
///
/// Allowing the following expression, eg:
/// ```.text
/// if let Some((a, b, c)) = option_expand_all!(foo_a(), foo_b(), foo_c()) {
///     /* code */
/// }
/// ```
/// see: http://stackoverflow.com/q/40986141/432509
///

#[macro_export]
macro_rules! option_expand_all {
    ($($opt:expr),*) => {{
        if false $(|| $opt.is_none())* {
            None
        } else {
            Some(($($opt.unwrap(),)*))
        }
    }};
}

/// Set or clear flag(s) based on a boolean argument, e.g.:
///
/// ```.text
/// assign_flag_from_bool!(flag_assign, flag_value, bool_test);
/// ```
///
/// This is short-hand for:
///
/// ```.text
/// if bool_test {
///     flag_assign |= flag_value;
/// } else {
///     flag_assign &= !flag_value;
/// }
/// ```

#[macro_export]
macro_rules! assign_flag_from_bool {
    ($flag_assign:expr, $flag_value:expr, $bool_test:expr) => {
        {
            let flag_ref = &mut ($flag_assign);
            let flag_val = $flag_value;
            if $bool_test {
                *flag_ref |= flag_val;
            } else {
                *flag_ref &= !flag_val;
            }
        }
    };
}
