// Licensed: Apache 2.0

///
/// Get the type_id from a (Struct, member) pair.
///
/// ```.text
/// use ::std::any::TypeId;
/// struct MyStruct {
///     value: i64,
/// }
/// fn main() {
///    assert_eq!(TypeId::of::<i64>(), type_id_of!(MyStruct, value));
/// }
/// ```
/// see: http://stackoverflow.com/a/42111851/432509
///
/// note: uses similar trick to 'offset_of_enum' to get an instance from nothing.
/// note: this compiles down to a constant (no std::mem use)
#[macro_export]
macro_rules! type_id_of {
    ($t:ty, $f:ident) => {
        {
            fn type_of<T: 'static + ?Sized>(_: &T) -> ::std::any::TypeId {
                ::std::any::TypeId::of::<T>()
            }
            // note that
            let base: $t = unsafe { ::std::mem::uninitialized() };
            let result = type_of(&base.$f);
            ::std::mem::forget(base);
            result
        }
    }
}
