// Licensed: Apache 2.0

/// Utilities for counting args:
///
/// Separated by white-space.
#[macro_export]
macro_rules! count_args_space {
    ($name:ident) => { 1 };
    ($first:ident $($rest:ident) *) => {
        1 + count_args_space!($($rest) *)
    }
}

/// Separated by commas.
#[macro_export]
macro_rules! count_args_comma {
    ($name:ident) => { 1 };
    ($first:ident, $($rest:ident),*) => {
        1 + count_args_comma!($($rest),*)
    }
}
