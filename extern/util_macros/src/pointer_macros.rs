// Licensed: Apache 2.0

// ------------
// Pointer Size

#[macro_export]
macro_rules! size_of_ptr {
    () => {
        ::std::mem::size_of::<*const u8>()
    }
}

#[macro_export]
macro_rules! size_of_instance_member {
    ($t:ty, $f:ident) => {
        {
            #[inline]
            fn size_of<T: 'static + Sized>(_: &T) -> usize {
                ::std::mem::size_of::<T>()
            }
            let base: $t = unsafe { ::std::mem::uninitialized() };
            let result = size_of(&base.$f);
            ::std::mem::forget(base);
            result
        }
    }
}

#[macro_export]
macro_rules! size_of_ref_type {
    ($t:expr) => {
        {
            #[inline]
            fn size_of<T: 'static + Sized>(_: &T) -> usize {
                ::std::mem::size_of::<T>()
            }
            let result = size_of($t);
            result
        }
    }
}

#[macro_export]
macro_rules! size_of_ptr_type {
    ($t:expr) => {
        {
            #[inline]
            fn size_of<T: 'static + Sized>(_: *const T) -> usize {
                ::std::mem::size_of::<T>()
            }
            let result = size_of($t);
            result
        }
    }
}

// --------------
// Pointer Offset

#[macro_export]
macro_rules! offset_of {
    ($ty:ty, $field:ident) => {
        &(*(0 as *const $ty)).$field as *const _ as usize
    }
}

/// Enum variant offset, eg:
/// ```
/// offset_of_enum(MyEnum::Variant)
/// ```
///
/// note: this compiles down to a constant (no std::mem use)
#[macro_export]
macro_rules! offset_of_enum {
    ($($tt:tt)*) => {
        {
            let base = $($tt)*( ::std::mem::uninitialized() );
            let offset = match base {
                $($tt)*(ref inner) => (inner as *const _ as usize) - (&base as *const _ as usize),
                _ => unreachable!(),
            };
            ::std::mem::forget(base);
            offset
        }
    }
}

#[macro_export]
macro_rules! check_type_pair {
    ($a:expr, $b:expr) => {
        if false {
            let _types_match = if false {$a} else {$b};
        }
    }
}

#[macro_export]
macro_rules! parent_of_mut {
    ($child:expr, $ty:ty, $field:ident) => {
        {
            check_type_pair!(&(*(0 as *const $ty)).$field, &$child);
            let offset = offset_of!($ty, $field);
            &mut *(((($child as *mut _) as usize) - offset) as *mut $ty)
        }
    }
}

#[macro_export]
macro_rules! parent_of {
    ($child:expr, $ty:ty, $field:ident) => {
        {
            check_type_pair!(&(*(0 as *const $ty)).$field, &$child);
            let offset = offset_of!($ty, $field);
            &*(((($child as *const _) as usize) - offset) as *const $ty)
        }
    }
}

// -------------------------------------------
// Transmute data as correctly sized u8 slices

#[macro_export]
macro_rules! size_of_ptr_type {
    ($t:expr) => {
        {
            #[inline]
            fn size_of<T: 'static + Sized>(_: *const T) -> usize {
                ::std::mem::size_of::<T>()
            }
            let result = size_of($t);
            result
        }
    }
}

#[macro_export]
macro_rules! transmute_byte_slice_from_ptr {
    ($p:expr) => {
        {
            #[inline]
            fn transmute_byte_slice_from_ptr<T: Sized>(p: &T) -> &[u8] {
                unsafe {
                    ::std::slice::from_raw_parts(
                        ::std::mem::transmute::<&T, &u8>(p),
                        ::std::mem::size_of::<T>(),
                    )
                }
            }
            transmute_byte_slice_from_ptr($p)
        }
    }
}
