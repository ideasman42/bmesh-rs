
#[macro_use]
extern crate util_macros;

// --------------------------
// Test: option_expand_all!()

#[test]
fn test_some() {
    let foo: Option<u32> = Some(0xF00);
    let bar: Option<u32> = Some(0xBAA);
    assert_eq!(false, option_expand_all!(foo, bar).is_none());
}

#[test]
fn test_none() {
    let foo: Option<u32> = Some(0xF00);
    let bar: Option<u32> = None;
    assert_eq!(false, option_expand_all!(foo, bar).is_some());
}

#[test]
fn test_realistic_success() {
    let foo: Option<u32> = Some(0xF00);
    let bar: Option<u32> = Some(0xBAA);
    let baz: Option<u32> = Some(0xCCC);
    assert_eq!(true, {
        if let Some((_, _, _)) = option_expand_all!(foo, bar, baz) {
            true
        } else {
            false
        }
    });
}

#[test]
fn test_realistic_fail() {
    let foo: Option<u32> = Some(0xF00);
    let bar: Option<u32> = None;
    let baz: Option<u32> = Some(0xCCC);

    assert_eq!(true, {
        if let Some((_, _, _)) = option_expand_all!(foo, bar, baz) {
            false
        } else {
            true
        }
    });
}
