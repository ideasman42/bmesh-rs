// Licensed: GPL v2+

extern crate app_engine;

// extern crate png_encode_mini;

#[test]
fn main() {
    use app_engine::prelude::*;
    use app_engine::exports::{
        PtrMut,
        PtrConst,
        null_mut,
    };

    let mut app = App::new();

    app_env_new(&mut app);
    {
        // Figure out mutability stuff!
        let mut env = app.envs.pop().unwrap();
        app_startup_env_init(&mut env);

        // let filepath = "test.wisk";
        {
            // let mut file = ::std::fs::File::create(filepath).expect("open failed");
            let mut file: Vec<u8> = Vec::new();
            app_engine::core::dna::io::main_data::encode_main::encode_main(
                &mut file,
                &env.main,
                &app.system.dna_info,
            ).unwrap();

            env.main = Default::default();

            // let file = ::std::fs::File::open(filepath).expect("open failed");
            app_engine::core::dna::io::main_data::decode_main::decode_main(
                &*file,
                // &file,
                &mut env.main,
                &app.system.dna_info,
            ).unwrap();
        }

        env.scene = env.main.scene.head_mut();

        app.envs.push(env);

        app_startup_prefs_init(&mut app);
    }

    // ugly but works
    app_window_new(&mut app, &[800, 600]);
    let win = app.windows.last().unwrap();

    let mut image = win.pxbuf_create();

    {
        let mut ctx = AppContext {
            app: PtrConst(&app),
            window: PtrConst(win),

            env: PtrMut(app.envs.last_mut().unwrap()),
            wchain: null_mut(),
        };

        unsafe {
            // How to solve? We want to run an update only!
            // This modifies welem_root but not the window.
            // FIXME!
            let w = win as *const Win;
            let w = w as usize;
            let w = w as *mut Win;
            (*w).welem_root.update(PtrMut(&mut ctx));
        }
    }

    {
        let size_new = win.welem_root.rect.size();
        if {
            let size_old = image.size().clone();
            size_old != size_new
        } {
            image.reshape_uninitialized(&size_new);
        }
    }

    {
        win.welem_root.composite(&mut image);
    }

    /*
    {
        let size = image.size();
        let mut f = std::fs::File::create("test.png").unwrap();
        match png_encode_mini::write_rgba_from_u32(
            &mut f, image.as_slice_u32(),
            size[0] as u32,
            size[1] as u32,
        ) {
            Ok(_) => println!("Written image!"),
            Err(e) => println!("Error {:?}", e),
        }
    }
    */
}
