// Licensed: GPL v2+

///
/// Store context for handlers.
///

use ::context::{
    AppContext,
};

use ::wm::welem::{
    WElemParentChain,
};

use ::plain_ptr::{
    PtrMut,
};

#[derive(Clone)]
pub struct AppContextStore {
    // Currently only store window chain
    // first element is root element, last is the child/leaf window.
    wchain_data: Vec<WElemParentChain>,
    pub wchain: PtrMut<WElemParentChain>,
}

impl AppContextStore {
    pub fn from_context(ctx: &mut AppContext) -> AppContextStore {
        let mut wchain_data = ctx.wchain.as_vec_chain_mut();
        let wchain = PtrMut(&mut wchain_data[0]);
        return AppContextStore {
            wchain_data: wchain_data,
            wchain: wchain,
        };
    }
}
