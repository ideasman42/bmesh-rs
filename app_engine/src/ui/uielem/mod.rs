// Licensed: GPL v2+

///
/// This module defines a hierarchy of UI elements.
///
/// (roughly similar to WElemType),
/// although UI-elements don't have to manage their own pixel buffers,
/// being drawn on update instead of tracking changed-state.
///

/// Implementations of window types.

pub mod types;

// export for general use
pub use self::types::layout::{
    UIElemLayout,
    UIPackState,
    UIDir,
    UIAlign,
};

pub use self::types::push_button::{
    UIElemLayoutPushButtonOpImpl,
};

mod local_prelude {
    pub use ::std::any::Any;
    pub use ::app::{
        App,
        UIElemTypesContainerImpl,
    };

    pub use core::props::{
        PropGroup,
    };

    pub use context::{
        AppContextMutP,
        AppContextConstP,
    };

    pub use context_store::{
        AppContextStore,
    };

    pub use wm::event_system::{
        Event,
    };

    pub use wm::event_handler::{
        EventHandleState,
    };

    pub use pxbuf::{
        PxBuf,
    };

    pub use ::wm::welem::{
        WElemType,
        WElemTypeFn,
        WElemVecImpl,
        WElemParentChain,
        WElemParentChainMutP,
        WElem,
    };

    pub use app::rect::{
        RectI,
        RectF,
    };

    pub use super::{
        UIElem,
        UIElemType,
        UIElemTypeFn,
        UIElemParentChain,
        UIElemFlag,
        UIElemVisitRefFn,
        UIElemVisitMutFn,
    };

    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
}

use self::local_prelude::*;

/// Classify kinds of events for event handlers,
/// so we can catch a general class of a events without having to know the details.
///
/// Currently used so a menu can close once a menu item is executed for eg.
///
pub enum UIActionKind {
    Nop,
    ExecComplete,
}

struct_bitflag_impl!(pub struct UIElemFlag(pub u8));
impl UIElemFlag {
    // Pushed down
    pub const SELECT:  Self = UIElemFlag(1 << 0);
    // Highligted/mouse-over
    pub const ACTIVE:  Self = UIElemFlag(1 << 1);
}

pub type UIElemVisitRefFn = fn(
    uielem: &UIElem, user_data: &Box<Any>,
) -> bool;
pub type UIElemVisitMutFn = fn(
    uielem: &mut UIElem, user_data: &Box<Any>,
) -> bool;


/// Callbacks for window element types.
pub struct UIElemTypeFn {
    pub new_data: fn(Box<Any>) -> Box<Any>,

    // we may want to pass in parents?
    // pub update: fn(uielem: &mut UIElem, ctx: AppContextMutP),

    // Pack UI layout.
    pub pack: fn(uielem: &mut UIElem, pack_init: &UIPackState, app: &App),

    // Parent 'WElem' should call from its 'update'
    // function, so this writes to a cached buffer.
    pub draw: fn(uielem: &UIElem, app: &App, image: &mut PxBuf),

    // event is handled or not... maybe we want to have an enum for this.
    // see: EventHandleState
    pub handle_event: fn(
        uielem: &mut UIElem, ctx: AppContextMutP, event: &Event,
        wchain_parent: UIElemParentChainMutP,
        ctx_store: Option<&AppContextStore>,
    ) -> (EventHandleState, UIActionKind),

    pub rect_set: fn(uielem: &mut UIElem, rect: &RectF),
}

pub struct UIElemContainerTypeFn {
    pub visit_children_ref: fn(
        uielem: &UIElem,
        UIElemVisitRefFn,
        user_data: &Box<Any>,
    ) -> bool,
    pub visit_children_mut: fn(
        uielem: &mut UIElem,
        UIElemVisitMutFn,
        user_data: &Box<Any>,
    ) -> bool,
}

pub struct UIElemType {
    // needs to be unique!
    pub id: String,
    pub cb: UIElemTypeFn,
    pub cb_container: Option<UIElemContainerTypeFn>,
}

pub struct UIElem {
    // XXX, not ideal to use pointer here.
    // but we want to get the system working and UI types aren't typically
    // added/removed at run-time.
    pub elem_type: *const UIElemType,

    // Data only the elem_type knows about.
    pub custom_data: Box<Any>,

    // WElem relative rectangle (values draw directly into the image buffer).
    // note, we may want to transform these for an interface that can scale.
    pub rect: RectF,
    // we could store int version, for now don't.

    // Note that we don't have 'children',
    // although nested data may be stored in own 'custom_data'.

    pub flag: UIElemFlag,
}

impl UIElem {
    pub fn new(app: &App, type_id: &str, type_data: Box<Any>, rect: RectF) -> Self {
        let welem_type = app.system.uielem_types.find_by_id(type_id).unwrap();
        return UIElem {
            // XXX, Cast to pointer isn't ideal
            elem_type: welem_type as *const _,
            custom_data: (welem_type.cb.new_data)(type_data),
            rect: rect,
            flag: UIElemFlag(0),
        };
    }

    /// Re-create UI from context
    pub fn ui(&mut self, _ctx: AppContextConstP) {
        // pass
    }

    pub fn pack(&mut self, pack_init: &UIPackState, app: &App) {
        let elem_type = unsafe { &(*self.elem_type) };
        (elem_type.cb.pack)(self, pack_init, app);
    }

    // Called by 'WElem.update' to fill its pixels.
    pub fn draw(&self, app: &App, image: &mut PxBuf) {
        let elem_type = unsafe { &(*self.elem_type) };
        (elem_type.cb.draw)(self, app, image);
    }

    /// Handle this and typically its children,
    /// unless the callback chooses not to.
    pub fn handle_event(
        &mut self, ctx: AppContextMutP, event: &Event,
        uichain_parent: PtrMut<UIElemParentChain>,
        ctx_store: Option<&AppContextStore>,
    ) -> (EventHandleState, UIActionKind) {
        let elem_type = unsafe { &(*self.elem_type) };
        let mut uichain = UIElemParentChain::new(uichain_parent, PtrMut(self));
        let uichain_p = PtrMut(&mut uichain);
        let ret = (elem_type.cb.handle_event)(self, ctx, event, uichain_p, ctx_store);
        return ret;
    }

    /// Handle this and typically its children,
    /// unless the callback chooses not to.
    pub fn rect_set(&mut self, rect: &RectF) {
        let elem_type = unsafe { &(*self.elem_type) };
        (elem_type.cb.rect_set)(self, rect);
        debug_assert!(self.rect == self.rect);
    }

    pub fn is_container(&self) -> bool {
        let elem_type = unsafe { &(*self.elem_type) };
        return !elem_type.cb_container.is_none();
    }

    pub fn visit_ref(&self, visit_fn: UIElemVisitRefFn, user_data: &Box<Any>) -> bool {
        if !visit_fn(self, user_data) {
            return false;
        }
        let elem_type = unsafe { &(*self.elem_type) };
        if let Some(cb_container) = elem_type.cb_container.as_ref() {
            return (cb_container.visit_children_ref)(self, visit_fn, user_data);
        }
        return true;
    }

    pub fn visit_mut(&mut self, visit_fn: UIElemVisitMutFn, user_data: &Box<Any>) -> bool {
        if !visit_fn(self, user_data) {
            return false;
        }
        let elem_type = unsafe { &(*self.elem_type) };
        if let Some(cb_container) = elem_type.cb_container.as_ref() {
            return (cb_container.visit_children_mut)(self, visit_fn, user_data);
        }
        return true;
    }
}


#[derive(Clone)]
pub struct UIElemParentChain {
    pub parent: PtrMut<UIElemParentChain>,
    pub uielem: PtrMut<UIElem>,
}
pub type UIElemParentChainMutP = PtrMut<UIElemParentChain>;
pub type UIElemParentChainConstP = PtrConst<UIElemParentChain>;


impl UIElemParentChain {
    pub fn new(
        parent: PtrMut<UIElemParentChain>,
        uielem: PtrMut<UIElem>,
    ) -> Self {
        UIElemParentChain {
            parent: parent,
            uielem: uielem,
        }
    }

    pub fn parent_root_mut(&self) -> PtrConst<UIElem> {
        let mut uichain = PtrConst(self);
        while uichain.parent != null_mut() {
            uichain = uichain.parent.as_const();
        }
        return uichain.uielem.as_const();
    }
    pub fn parent_root(&self) -> PtrMut<UIElem> {
        let mut uichain = PtrConst(self);
        while uichain.parent != null_mut() {
            uichain = uichain.parent.as_const();
        }
        return uichain.uielem;
    }

    pub fn parent_level_mut(&self, mut level: usize) -> PtrConst<UIElem> {
        let mut uichain = PtrConst(self);
        while uichain.parent != null_mut() {
            if level == 0 {
                break;
            }
            level -= 1;
            uichain = uichain.parent.as_const();
        }
        return uichain.uielem.as_const();
    }
    pub fn parent_level(&self, mut level: usize) -> PtrMut<UIElem> {
        let mut uichain = PtrConst(self);
        while uichain.parent != null_mut() {
            if level == 0 {
                break;
            }
            level -= 1;
            uichain = uichain.parent.as_const();
        }
        return uichain.uielem;
    }

    // TODO, other functions from WElemParentChain
}
