// Licensed: GPL v2+

/// Container for other UI elements.


mod local_prelude {
    pub use super::super::local_prelude::*;

    pub use super::{
        UIElemLayout,
    };
}

use self::local_prelude::*;

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum UIDir {
    Horizontal,
    Vertical,
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum UIAlign {
    Expand,
    // Left/Upper
    Min,
    Center,
    // Right/Lower
    Max,
}

#[derive(Clone, Copy)]
pub struct UIPackState {
    pub rect: RectF,
    pub dir: UIDir,
    pub align: UIAlign,
}

pub struct UIElemLayout {
    pub dir: UIDir,
    pub align: UIAlign,
    pub children: Vec<UIElem>,
}

impl Default for UIElemLayout {
    fn default() -> Self {
        UIElemLayout {
            align: UIAlign::Min,
            dir: UIDir::Vertical,
            children: Vec::new(),
        }
    }
}

mod layout_ui {
    use super::local_prelude::*;
    use pxbuf_draw::PxBufDraw;

    macro_rules! uielem_data_cast_ref {
        ($uielem:ident) => {
            $uielem.custom_data.downcast_ref::<UIElemLayout>().unwrap()
        }
    }

    macro_rules! uelem_data_cast_mut {
        ($uielem:ident) => {
            $uielem.custom_data.downcast_mut::<UIElemLayout>().unwrap()
        }
    }

    fn new_data(init: Box<Any>) -> Box<Any> {

        let layout = {
            if let Ok(layout) = init.downcast::<UIElemLayout>() {
                *layout
            } else {
                unreachable!();
            }
        };

        return Box::new(layout);
    }

    fn pack(uielem: &mut UIElem, pack_init: &UIPackState, app: &App) {
        let layout = uelem_data_cast_mut!(uielem);

        let mut pack_state = UIPackState {
            dir: layout.dir,
            align: layout.align,
            rect: pack_init.rect
        };

        match layout.dir {

            UIDir::Horizontal => {
                for uielem_child in &mut layout.children {
                    uielem_child.pack(&pack_state, app);
                    if pack_state.align != UIAlign::Max {
                        pack_state.rect.xmax = uielem_child.rect.xmax;
                    } else {
                        pack_state.rect.xmin = uielem_child.rect.xmin;
                    }
                }
            },

            UIDir::Vertical => {
                for uielem_child in &mut layout.children {
                    uielem_child.pack(&pack_state, app);
                    if pack_state.align != UIAlign::Max {
                        pack_state.rect.ymax = uielem_child.rect.ymax;
                    } else {
                        pack_state.rect.ymin = uielem_child.rect.ymin;
                    }
                }
            },

        }
        uielem.rect = pack_state.rect;

    }

    fn draw(uielem: &UIElem, app: &App, image: &mut PxBuf) {
        if (uielem.flag & UIElemFlag::ACTIVE) != 0 {
            image.draw_rect_clip(
                app.prefs.theme.ui.menu.active,
                &uielem.rect.to_xy_lb_i32_round(),
                &uielem.rect.to_xy_rt_i32_round(),
            );
        }

        let layout = uielem_data_cast_ref!(uielem);
        for uielem_child in &layout.children {
            uielem_child.draw(app, image);
        }
    }

    fn handle_event(
        uielem: &mut UIElem, ctx: AppContextMutP, event: &Event,
        uichain_parent: UIElemParentChainMutP,
        ctx_store: Option<&AppContextStore>,
    ) -> (EventHandleState, UIActionKind) {
        let mut uichain = UIElemParentChain::new(uichain_parent, PtrMut(uielem));
        let uichain_p = PtrMut(&mut uichain);

        let co = {
            let welem = ctx.wchain.welem;
            [
                event.state.co[0] - welem.rect_fl.xmin,
                event.state.co[1] - welem.rect_fl.ymin,
            ]
        };

        let layout = uelem_data_cast_mut!(uielem);
        let mut handle_state = EventHandleState::CONTINUE;
        for uielem_child in &mut layout.children {
            if uielem_child.rect.isect_point(&co) {
                let (handle_state_new, action_kind) =
                    uielem_child.handle_event(ctx, event, uichain_p, ctx_store);
                handle_state |= handle_state_new;
                if (handle_state & EventHandleState::BREAK) != 0 {
                    return (handle_state, action_kind);
                }
            }
        }
        return (handle_state, UIActionKind::Nop);
    }

    pub fn rect_set(uielem: &mut UIElem, rect: &RectF) {
        let layout = uelem_data_cast_mut!(uielem);
        let rect_xform = uielem.rect.transform_precalc(rect);
        for uielem_child in &mut layout.children {
            let rect_new = rect_xform.rect(&uielem_child.rect);
            // don't clamp for now
            uielem_child.rect_set(&rect_new);
            debug_assert!(uielem.rect == rect_new);
        }
        uielem.rect = *rect;
    }

    // Container Callbacks
    pub fn visit_children_ref(
        uielem: &UIElem,
        visit_fn: UIElemVisitRefFn,
        user_data: &Box<Any>,
    ) -> bool {
        let layout = uielem_data_cast_ref!(uielem);
        for uielem_child in &layout.children {
            if !uielem_child.visit_ref(visit_fn, user_data) {
                return false;
            }
        }
        return true;
    }

    pub fn visit_children_mut(
        uielem: &mut UIElem,
        visit_fn: UIElemVisitMutFn,
        user_data: &Box<Any>,
    ) -> bool {
        let layout = uelem_data_cast_mut!(uielem);
        for uielem_child in &mut layout.children {
            if !uielem_child.visit_mut(visit_fn, user_data) {
                return false;
            }
        }
        return true;
    }

    pub fn new_type() -> UIElemType {
        UIElemType {
            id: "Layout".to_string(),
            cb: UIElemTypeFn {
                new_data: new_data,
                pack: pack,
                draw: draw,
                handle_event: handle_event,
                rect_set: rect_set,
            },
            cb_container: Some(UIElemContainerTypeFn {
                visit_children_ref: visit_children_ref,
                visit_children_mut: visit_children_mut,
            }),
        }
    }
}

pub fn register_uielems(app: &mut App) {
    app.system.uielem_types.register(layout_ui::new_type());
}
