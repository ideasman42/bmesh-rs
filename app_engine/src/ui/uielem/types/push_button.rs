// Licensed: GPL v2+

/// Push Buttons, where pressing runs some action or function.

mod local_prelude {
    pub use super::super::local_prelude::*;

    pub use ::wm::operator::{
        OperatorType,
    };
}

use self::local_prelude::*;

pub use wm::operator::{
    OpAction,
};

pub mod keymap_events {
    // Button click, execute the button
    pub const EXEC: u32 = 0;
    // Context menu (typically RMB)
    pub const MENU: u32 = 1;
}


// ----------------------------------------------------------------------------
// UI Builders

mod builder {
    use super::local_prelude::*;
    use super::push_button_operator_ui::{
        UIElemPushButtonOp,
    };
    use ::ui::uielem::{
        UIElemLayout,
    };
    pub use wm::operator::{
        OpAction,
    };


    pub struct UIBuilderOp<'a> {
        layout_internal: &'a mut UIElemLayout,
        op_action: OpAction,
        text: Option<String>,
    }

    impl <'a> UIBuilderOp<'a> {
        pub fn new<'b>(layout: &'b mut UIElemLayout, op_action: OpAction) -> UIBuilderOp<'b> {
            return UIBuilderOp::<'b> {
                layout_internal: layout,
                op_action: op_action,
                text: None,
            };
        }

        pub fn exec(self, ctx: AppContextConstP) {
            let ot = self.op_action.ty;

            let text = {
                if let Some(text) = self.text {
                    text
                } else {
                    ot.name.clone()
                }
            };

            self.layout_internal.children.push(
                UIElem::new(
                    &*ctx.app,
                    "PushButtonOp",
                    Box::new(UIElemPushButtonOp {
                        ot: ot,
                        text: text,
                        props: Some(self.op_action.prop_data),
                        .. Default::default()
                    }),
                    RectF::from_zero(),
                ),
            );
        }

        pub fn text(mut self, text: &str) -> UIBuilderOp<'a> {
            self.text = Some(text.to_string());
            self
        }

    }
}

    // ----

    // Example use
    // layout.operator("mesh.select_all").foo().build(ctx);


// ----------------------------------------------------------------------------
// Layout Helpers

pub trait UIElemLayoutPushButtonOpImpl {
    // fn operator(&mut self, ctx: AppContextConstP, &str);
    fn operator(&mut self, op_action: OpAction) -> builder::UIBuilderOp;
}

impl UIElemLayoutPushButtonOpImpl for ::ui::uielem::UIElemLayout {
    fn operator(&mut self, op_action: OpAction) -> builder::UIBuilderOp {
        return builder::UIBuilderOp::new(self, op_action);
    }
}

mod push_button_operator_ui {
    use super::local_prelude::*;
    use pxbuf_draw::PxBufDraw;

    // macro for highly verbose and re-used cast
    macro_rules! uielem_custom_data_cast_ref {
        ($uielem:ident) => {
            $uielem.custom_data.downcast_ref::<UIElemPushButtonOp>().unwrap()
        }
    }
    #[allow(unused_macros)]
    macro_rules! uelem_data_cast_mut {
        ($uielem:ident) => {
            $uielem.custom_data.downcast_mut::<UIElemPushButtonOp>().unwrap()
        }
    }

    pub struct UIElemPushButtonOp {
        pub ot: PtrConst<OperatorType>,
        pub text: String,
        pub props: Option<PropGroup>,
    }
    impl Default for UIElemPushButtonOp {
        fn default() -> Self {
            return UIElemPushButtonOp {
                ot: null_const(),
                text: "".to_string(),
                props: None,
            }
        }
    }

    fn new_data(init: Box<Any>) -> Box<Any> {

        let op_data = {
            if let Ok(op_data) = init.downcast::<UIElemPushButtonOp>() {
                *op_data
            } else {
                unreachable!();
            }
        };

        return Box::new(op_data);
    }

    fn pack(uielem: &mut UIElem, pack_state: &UIPackState, app: &App) {
        // TODO, move into shared function
        let ui_unit = app.prefs.ui.elem_unit * app.prefs.ui.scale_f64;
        match pack_state.dir {
            UIDir::Horizontal => {
                if pack_state.align != UIAlign::Max {
                    uielem.rect.xmin = pack_state.rect.xmax;
                    uielem.rect.xmax = pack_state.rect.xmax + ui_unit;
                    uielem.rect.ymin = pack_state.rect.ymin;
                    uielem.rect.ymax = pack_state.rect.ymax;
                } else {
                    uielem.rect.xmin = pack_state.rect.xmin - ui_unit;
                    uielem.rect.xmax = pack_state.rect.xmin;
                    uielem.rect.ymin = pack_state.rect.ymin;
                    uielem.rect.ymax = pack_state.rect.ymax;
                }
            },

            UIDir::Vertical => {
                if pack_state.align != UIAlign::Max {
                    uielem.rect.xmin = pack_state.rect.xmin;
                    uielem.rect.xmax = pack_state.rect.xmax;
                    uielem.rect.ymin = pack_state.rect.ymax;
                    uielem.rect.ymax = pack_state.rect.ymax + ui_unit;
                } else {
                    uielem.rect.xmin = pack_state.rect.xmin;
                    uielem.rect.xmax = pack_state.rect.xmax;
                    uielem.rect.ymin = pack_state.rect.ymin - ui_unit;
                    uielem.rect.ymax = pack_state.rect.ymin;
                }
            },
        }
    }

    fn draw(uielem: &UIElem, app: &App, image: &mut PxBuf) {
        let but = uielem_custom_data_cast_ref!(uielem);

        if (uielem.flag & UIElemFlag::ACTIVE) != 0 {
            image.draw_rect_clip(
                app.prefs.theme.ui.menu.active,
                &uielem.rect.to_xy_lb_i32_round(),
                &uielem.rect.to_xy_rt_i32_round(),
            );
        }

        let font = &app.system.ui_font;
        let ui_scale = app.prefs.ui.scale_i32;

        let x = uielem.rect.xmin as i32 + (font.width * ui_scale) / 2;
        let y = uielem.rect.cent_y() as i32 - (font.ascent * ui_scale / 2);

        font.draw_string(
            image,
            app.prefs.theme.ui.menu.text,
            &[x, y],
            app.prefs.ui.scale_i32,
            &but.text,
        );
    }

    fn handle_event(
        uielem: &mut UIElem, mut ctx: AppContextMutP, event: &Event,
        uichain_parent: UIElemParentChainMutP,
        ctx_store: Option<&AppContextStore>,
    ) -> (EventHandleState, UIActionKind) {
        use ::wm::event_system::EventID;

        let but = uielem_custom_data_cast_ref!(uielem);

        match event.id {
            EventID::PointerMotion {} => {
                if (uielem.flag & UIElemFlag::ACTIVE) == 0 {
                    // clear active of all others!
                    {
                        fn clear_active(uielem: &mut UIElem, _user_data: &Box<Any>) -> bool {
                            uielem.flag &= !UIElemFlag::ACTIVE;
                            return true;
                        }

                        let user_data = Box::new(()) as Box<Any>;
                        uichain_parent.parent_root().visit_mut(clear_active, &user_data);
                    }

                    uielem.flag |= UIElemFlag::ACTIVE;
                }
                return (EventHandleState::BREAK, UIActionKind::Nop);
            },
            _ => {
                use ::wm::keymap::{
                    KeyMapCustomVecImpl,
                };

                if let Some(value) =
                    ctx.app.prefs.input.keymaps_custom.find_by_id_and_event("UI Button", event)
                {
                    match value {
                        super::keymap_events::EXEC => {
                            use ::wm::welem::WElemAction;
                            // Add the event the current window instead of the context
                            // of the operator it's self since we need to be executed
                            // by the action queue, for predictable order of execution.
                            let mut welem = ctx.welem_mut();

                            welem.action_queue.push(
                                WElemAction::Operator {
                                    id: but.ot.id.clone(),
                                    props: but.props.clone(),
                                    ctx_store: {
                                        if let Some(ctx_store) = ctx_store {
                                            ctx_store.clone()
                                        } else {
                                            ctx.to_context_store()
                                        }
                                    },
                                }
                            );

                            return (
                                EventHandleState::BREAK,
                                UIActionKind::ExecComplete,
                            );
                        },
                        _ => {
                            panic!();
                        },
                    }
                }
            },
        }

        return (EventHandleState::CONTINUE, UIActionKind::Nop);
    }

    pub fn rect_set(uielem: &mut UIElem, rect: &RectF) {
        uielem.rect = *rect;
    }


    pub fn new_type() -> UIElemType {
        UIElemType {
            id: "PushButtonOp".to_string(),
            cb: UIElemTypeFn {
                new_data: new_data,
                pack: pack,
                draw: draw,
                handle_event: handle_event,
                rect_set: rect_set,
            },
            cb_container: None,
        }
    }
}

pub fn register_uielems(app: &mut App) {
    app.system.uielem_types.register(push_button_operator_ui::new_type());
}

