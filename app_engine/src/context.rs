// Licensed: GPL v2+
// (c) Blender Foundation

use ::prelude::*;

use bmesh::prelude::BMesh;
use ::core::edit_mesh::prelude::BMEditMesh;

use ::core::dna::types::{
    ObjectInst,
};

use std::any::Any;

use wm::region::{
    ARegion,
};

use wm::menu::{
    Menu,
};

use wm::welem::{
    WElem,
    WElemParentChain,
};

use view3d::{
    RegionView3D,
    View3D,
};

use plain_ptr::{
    PtrMut,
    PtrConst,
};

pub struct AppContext {
    // XXX, currently a grab-bag of various structs
    // this will need to be better organized later!
    pub app: PtrConst<App>,
    pub window: PtrConst<Win>,

    // this is a bit awkward!
    pub env: PtrMut<Env>,

    // // XXX, in env
    // pub ar: *mut ARegion,
    // pub rv3d: *mut RegionView3D,
    // pub v3d: *mut View3D,

    // Only set when handling events!
    pub wchain: PtrMut<WElemParentChain>,
}

pub type AppContextMutP = PtrMut<AppContext>;
pub type AppContextConstP = PtrConst<AppContext>;

impl AppContext {

    pub fn to_context_store(&mut self) -> AppContextStore {
        return AppContextStore::from_context(self);
    }

    pub fn get_id(&self, id: &str) -> Option<Box<Any>> {
        let mut wchain = self.wchain;
        while !wchain.is_null() {
            let mut welem = wchain.welem;
            let ret = welem.context_get(id);
            if !ret.is_none() {
                return ret;
            }
            wchain = wchain.parent;
        }
        return None;
    }

    pub fn get_id_mut(&mut self, id: &str) -> Option<Box<Any>> {
        let mut wchain = self.wchain;
        while !wchain.is_null() {
            let mut welem = wchain.welem;
            let ret = welem.context_get(id);
            if !ret.is_none() {
                return ret;
            }
            wchain = (*wchain).parent;
        }
        return None;
    }

    pub fn object_inst(&self) -> Option<PtrConst<ObjectInst>> {
        return self.env.scene.object_inst_active.as_const().as_option();
    }
    pub fn object_inst_mut(&mut self) -> Option<PtrMut<ObjectInst>> {
        return self.env.scene.object_inst_active.as_option();
    }

    pub fn editmesh(&self) -> Option<PtrConst<BMEditMesh>> {
        let ref env = self.env;
        if let Some(obj_inst) = env.scene.object_inst_active.as_option() {
            return BMEditMesh::from_object_get(obj_inst.object.as_const());
        }
        return None;
    }
    pub fn editmesh_mut(&mut self) -> Option<PtrMut<BMEditMesh>> {
        let ref env = self.env;
        if let Some(obj_inst) = env.scene.object_inst_active.as_option() {
            return BMEditMesh::from_object_get_mut(obj_inst.object);
        }
        return None;
    }

    pub fn bmesh(&self) -> Option<PtrConst<BMesh>> {
        if let Some(ref mut em) = self.editmesh() {
            return Some(PtrConst(&em.bm));
        }
        return None;
    }
    pub fn bmesh_mut(&mut self) -> Option<PtrMut<BMesh>> {
        if let Some(ref mut em) = self.editmesh_mut() {
            return Some(PtrMut(&mut em.bm));
        }
        return None;
    }

    pub fn welem_root_mut(&mut self) -> PtrMut<WElem> {
        return self.wchain.parent_root_mut();
    }
    pub fn welem_root(&self) -> PtrConst<WElem> {
        return self.wchain.parent_root();
    }

    pub fn welem_level_mut(&mut self, level: usize) -> PtrMut<WElem> {
        return self.wchain.parent_level_mut(level);
    }
    pub fn welem_level(&self, level: usize) -> PtrConst<WElem> {
        return self.wchain.parent_level(level);
    }

    pub fn welem_parent_mut(&mut self) -> PtrMut<WElem> {
        return self.welem_level_mut(1);
    }

    pub fn welem_mut(&mut self) -> PtrMut<WElem> {
        return self.welem_level_mut(0);
    }

    pub fn welem_parent(&self) -> PtrConst<WElem> {
        return self.welem_level(1);
    }

    pub fn welem(&self) -> PtrConst<WElem> {
        return self.welem_level(0);
    }

}

macro_rules! context_get_id_fn {
    ($fn_name:ident, $id_string:expr, $t:ty) => {
        impl AppContext {
            pub fn $fn_name(&self) -> Option<PtrConst<$t>> {
                if let Some(ret) = self.get_id($id_string) {
                    // note: even though we don't want a mutable type here
                    // the 'Any' type is mutable, so that needs to be used.
                    let ret = *ret.downcast_ref::<*mut $t>().unwrap();
                    return Some(PtrConst(ret));
                }
                return None;
            }
        }
    }
}

macro_rules! context_get_id_fn_mut {
    ($fn_name:ident, $id_string:expr, $t:ty) => {
        impl AppContext {
            pub fn $fn_name(&mut self) -> Option<PtrMut<$t>> {
                if let Some(mut ret) = self.get_id_mut($id_string) {
                    let ret = *ret.downcast_mut::<*mut $t>().unwrap();
                    // return Some(unsafe { &mut *ret as &mut $t });
                    return Some(PtrMut(ret));
                }
                return None;
            }
        }
    }
}

context_get_id_fn!(v3d, "v3d", View3D);
context_get_id_fn_mut!(v3d_mut, "v3d", View3D);

context_get_id_fn!(rv3d, "rv3d", RegionView3D);
context_get_id_fn_mut!(rv3d_mut, "rv3d", RegionView3D);

context_get_id_fn!(region, "region", ARegion);
context_get_id_fn_mut!(region_mut, "region", ARegion);

context_get_id_fn!(menu, "menu", Menu);
context_get_id_fn_mut!(menu_mut, "menu", Menu);
