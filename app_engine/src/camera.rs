// Licensed: GPL v2+
// (c) Blender Foundation

use app::rect::{
    RectF,
};

use view3d;
use view3d::{
    View3D,
    RegionView3D,
};

// source/blender/blenkernel/intern/camera.c

const DEFAULT_SENSOR_WIDTH:  f64 = 32.0;
const DEFAULT_SENSOR_HEIGHT: f64 = 18.0;

// values for CameraParams.zoom, need to be taken into account for some operations
#[allow(dead_code)]
const CAMERA_PARAM_ZOOM_INIT_CAMOB: f64 = 1.0;
const CAMERA_PARAM_ZOOM_INIT_PERSP: f64 = 2.0;

const CAMERA_SENSOR_FIT_AUTO: u32  = 0;
const CAMERA_SENSOR_FIT_HOR:  u32  = 1;
const CAMERA_SENSOR_FIT_VERT: u32  = 2;

pub struct CameraParams {
    /* lens */
    pub is_ortho: bool,
    pub lens: f64,
    pub ortho_scale: f64,
    pub zoom: f64,

    pub shiftx: f64,
    pub shifty: f64,
    pub offsetx: f64,
    pub offsety: f64,

    /* sensor */
    pub sensor_x: f64,
    pub sensor_y: f64,
    pub sensor_fit: u32,

    /* clipping */
    pub clipsta: f64,
    pub clipend: f64,

    /* computed viewplane */
    pub ycor: f64,
    pub viewdx: f64,
    pub viewdy: f64,
    pub viewplane: RectF,

    /* computed matrix */
//    winmat: [4][4];
}

// BKE_camera_params_init
impl Default for CameraParams {
    fn default() -> Self {
        CameraParams {
            is_ortho: false,
            lens: 35.0,
            ortho_scale: 6.0,
            zoom: 1.0,

            shiftx: 0.0,
            shifty: 0.0,
            offsetx: 0.0,
            offsety: 0.0,

            clipsta: 0.1,
            clipend: 100.0,
            sensor_x: DEFAULT_SENSOR_WIDTH,
            sensor_y: DEFAULT_SENSOR_HEIGHT,
            sensor_fit: CAMERA_SENSOR_FIT_VERT,
            viewdx: 0.0,
            viewdy: 0.0,
            ycor: 0.0,
            viewplane: RectF::from_zero(),
        }
    }
}

impl CameraParams {
    // BKE_camera_sensor_size
    pub fn sensor_size(
        sensor_fit: u32,
        sensor_x: f64, sensor_y: f64,
    ) -> f64 {
        // TODO
        if sensor_fit == CAMERA_SENSOR_FIT_VERT {
            return sensor_y;
        } else {
            return sensor_x;
        }
    }

    // BKE_camera_sensor_fit
    pub fn sensor_fit(
        sensor_fit: u32,
        size_x: f64, size_y: f64,
    ) -> u32 {
        if sensor_fit == CAMERA_SENSOR_FIT_AUTO {
            if size_x >= size_y {
                return CAMERA_SENSOR_FIT_HOR;
            } else {
                return CAMERA_SENSOR_FIT_VERT;
            }
        }
        return sensor_fit;
    }

    // BKE_camera_params_from_view3d
    pub fn from_view3d(
        &mut self,
        v3d: &View3D,
        rv3d: &RegionView3D,
    ) {
        // common
        self.lens = v3d.lens;
        self.clipsta = v3d.near;
        self.clipend = v3d.far;

        /*
        if (rv3d.persp == RV3D_CAMOB) {
            /* camera view */
            BKE_camera_params_from_object(self, v3d.camera);

            self.zoom = BKE_screen_view3d_zoom_to_fac(rv3d.camzoom);

            self.offsetx = 2.0f * rv3d.camdx * self.zoom;
            self.offsety = 2.0f * rv3d.camdy * self.zoom;

            self.shiftx *= self.zoom;
            self.shifty *= self.zoom;

            self.zoom = CAMERA_PARAM_ZOOM_INIT_CAMOB / self.zoom;
        } else */
        if rv3d.persp == view3d::ViewPersp::ORTHO {
            // orthographic view
            let sensor_size = CameraParams::sensor_size(
                self.sensor_fit, self.sensor_x, self.sensor_y);

            self.clipend *= 0.5;    // otherwise too extreme low zbuffer quality
            self.clipsta = -self.clipend;

            self.is_ortho = true;
            // make sure any changes to this match ED_view3d_radius_to_dist_ortho()
            self.ortho_scale = rv3d.xform.offset_dist * sensor_size / v3d.lens;
            self.zoom = CAMERA_PARAM_ZOOM_INIT_PERSP;
        } else {
            // perspective view
            self.zoom = CAMERA_PARAM_ZOOM_INIT_PERSP;
        }
    }

    // BKE_camera_params_compute_viewplane
    pub fn compute_viewplane(
        &mut self,
        window_size: &[u32; 2],
        aspect: &[f64; 2],
    ) {
        // float pixsize, viewfac, sensor_size, dx, dy;
        // int sensor_fit;
        self.ycor = aspect[1] / aspect[0];
        let mut pixsize;
        if self.is_ortho {
            // orthographic camera
            // scale == 1.0 means exact 1 to 1 mapping
            pixsize = self.ortho_scale;
        }
        else {
            // perspective camera
            let sensor_size = CameraParams::sensor_size(
                self.sensor_fit,
                self.sensor_x,
                self.sensor_y);
            pixsize = (sensor_size * self.clipsta) / self.lens;
        }

        // determine sensor fit
        let sensor_fit = CameraParams::sensor_fit(
            self.sensor_fit,
            aspect[0] * window_size[0] as f64,
            aspect[1] * window_size[1] as f64);

        let viewfac = {
            if sensor_fit == CAMERA_SENSOR_FIT_HOR {
                window_size[0] as f64
            } else {
                self.ycor * window_size[1] as f64
            }
        };

        pixsize /= viewfac;

        // extra zoom factor
        pixsize *= self.zoom;

        debug_assert!(pixsize != 0.0);

        // compute view plane:
        // fully centered, zbuffer fills in jittered between -.5 and +.5
        let mut viewplane = RectF {
            xmin: -0.5 * window_size[0] as f64,
            ymin: -0.5 * self.ycor * window_size[1] as f64,
            xmax:  0.5 * window_size[0] as f64,
            ymax:  0.5 * self.ycor * window_size[1] as f64,
        };

        // lens shift and offset
        let dx = self.shiftx * viewfac + window_size[0] as f64 * self.offsetx;
        let dy = self.shifty * viewfac + window_size[1] as f64 * self.offsety;

        viewplane.xmin += dx;
        viewplane.ymin += dy;
        viewplane.xmax += dx;
        viewplane.ymax += dy;

        // the window matrix is used for clipping, and not changed during OSA steps
        // using an offset of +0.5 here would give clip errors on edges
        viewplane.xmin *= pixsize;
        viewplane.xmax *= pixsize;
        viewplane.ymin *= pixsize;
        viewplane.ymax *= pixsize;

        self.viewdx = pixsize;
        self.viewdy = self.ycor * pixsize;
        self.viewplane = viewplane;
    }
}

