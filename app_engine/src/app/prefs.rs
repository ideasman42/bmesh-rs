// Licensed: Apache 2.0

use ::wm::keymap;

pub struct PrefsThemeView3D {
    pub object_color: u32,
    pub object_select_color: u32,
    pub object_active_color: u32,
    pub wire_color: u32,
    pub vert_color: u32,
    pub face_color: u32,
    pub vert_select_color: u32,
    pub edge_select_color: u32,
    pub face_select_color: u32,
    pub background_color: u32,
}
impl Default for PrefsThemeView3D {
    fn default() -> Self {
        PrefsThemeView3D {
            // object-mode
            object_color: rgb_hex_u32!(0x00_00_00),
            object_select_color: rgb_hex_u32!(0xF1_58_00),
            object_active_color: rgb_hex_u32!(0xFF_AA_40),
            // edit-mode
            wire_color: rgb_hex_u32!(0x00_00_00),
            vert_color: rgb_hex_u32!(0x00_00_00),
            face_color: rgba_hex_u32!(0x00_00_00_00_12),
            vert_select_color: rgb_hex_u32!(0xFF_85_00),
            edge_select_color: rgb_hex_u32!(0xFF_A0_00),
            face_select_color: rgba_hex_u32!(0xFF_85_00_3C),

            // general
            background_color: rgb_hex_u32!(0x39_39_39),
        }
    }
}

pub struct PrefsThemeMenu {
    pub background: u32,
    pub active: u32,
    pub text: u32,
}
impl Default for PrefsThemeMenu {
    fn default() -> Self {
        PrefsThemeMenu {
            background: rgb_hex_u32!(0xAA_AA_AA),
            active: rgb_hex_u32!(0x66_88_BB),
            text: rgb_hex_u32!(0x00_00_00),
        }
    }
}

pub struct PrefsThemeUI {
    pub menu: PrefsThemeMenu,
}
impl Default for PrefsThemeUI {
    fn default() -> Self {
        PrefsThemeUI {
            menu: Default::default(),
        }
    }
}

pub struct PrefsSystem {
    pub undo_steps: usize,
    /// Zero for no memory limit.
    pub undo_memory: usize,
}
impl Default for PrefsSystem {
    fn default() -> Self {
        PrefsSystem {
            undo_steps: 64,
            undo_memory: 0,
        }
    }
}

pub struct PrefsInput {
    pub keymaps: Vec<keymap::KeyMap>,
    pub keymaps_custom: Vec<keymap::KeyMapCustom>,
}
impl Default for PrefsInput {
    fn default() -> Self {
        PrefsInput {
            keymaps: Vec::new(),
            keymaps_custom: Vec::new(),
        }
    }
}

pub struct PrefsUI {
    // Interface scale:
    // - 1.0 for regular UI.
    // - 2.0 for HI-DPI.
    //
    // Only integer values properly supported.
    pub scale_f64: f64,
    pub scale_i32: i32,

    // Button size (typically height)
    pub elem_unit: f64,
}
impl Default for PrefsUI {
    fn default() -> Self {
        let scale: i32 = 2;
        PrefsUI {
            scale_f64: scale as f64,
            scale_i32: scale,
            elem_unit: 22.0,
        }
    }
}
pub struct PrefsTheme {
    pub view3d: PrefsThemeView3D,
    pub ui: PrefsThemeUI,
}
impl Default for PrefsTheme {
    fn default() -> Self {
        PrefsTheme {
            view3d: Default::default(),
            ui: Default::default(),
        }
    }
}

pub struct Prefs {
    pub system: PrefsSystem,
    pub input: PrefsInput,
    pub theme: PrefsTheme,
    pub ui: PrefsUI,
}
impl Default for Prefs {
    fn default() -> Self {
        Prefs {
            system: Default::default(),
            input: Default::default(),
            theme: Default::default(),
            ui: Default::default(),
        }
    }
}

