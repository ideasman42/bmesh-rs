// Licensed: GPL v2+
use super::super::{
    Env,
};

pub fn app_startup_env_init(env: &mut Env) {
    use ::plain_ptr::{
        PtrMut,
        // PtrConst,
        null_mut,
    };

    use ::core::wli;

    use math_misc::translate_m4;

    // Setup data-blocks
    let mut scene = {
        // use ::core::dna::types::{
        //     Object,
        //     Scene,
        // };

        let scene = wli::scene::add(&mut env.main, None);

        env.scene = scene;

        scene
    };

    // Empty
    {
        let obj = wli::object::add(&mut env.main, Some("Empty"), null_mut());

        let mut obj_inst = wli::scene::object_link(&mut env.main, scene, None, obj);
        obj_inst.flag.select_enable();

        // Test matrix!
        translate_m4(&mut obj_inst.matrix, &[1.0, -1.0, 1.0]);

        // scene.object_inst_active = obj_inst;
    }

    let mesh_a = {
        let mut mesh = wli::mesh::add(&mut env.main, None);
        let obj = wli::object::add(&mut env.main, None, PtrMut(&mut mesh.id));

        let mut obj_inst = wli::scene::object_link(&mut env.main, scene, None, obj);
        obj_inst.flag.select_enable();

        // Test matrix!
        translate_m4(&mut obj_inst.matrix, &[1.0, 1.0, 1.0]);

        scene.object_inst_active = obj_inst;

        mesh
    };

    let mesh_b = {
        let mut mesh = wli::mesh::add(&mut env.main, None);
        let obj = wli::object::add(&mut env.main, Some("Mesh B"), PtrMut(&mut mesh.id));

        let mut obj_inst = wli::scene::object_link(&mut env.main, scene, None, obj);
        obj_inst.flag.select_enable();

        // Test matrix!
        translate_m4(&mut obj_inst.matrix, &[-1.0, -1.0, -1.0]);

        // scene.object_inst_active = obj_inst;

        mesh
    };

    // Setup BMesh
    {
        use bmesh;
        use ::bmesh::prelude::*;
        use bmesh::primitives::{
            BMeshPartialGeom,
        };
        let major_seg = 480 / 20;
        let minor_seg = 120 / 20;

        {
            let mesh = mesh_a;
            let mut bm = BMesh::new();
            let mut geom = BMeshPartialGeom::default();
            bmesh::primitives::cube(&mut bm, 1.0, &mut geom);

            ::core::edit_mesh::convert::bmesh_to_mesh(PtrMut(&mut bm), mesh);

            // Comment if we dont want to start in editmode

            // bm.clear();
            // ::core::edit_mesh::convert::bmesh_from_mesh(PtrMut(&mut bm), mesh);
            // mesh.rt.em = Some(Box::new(BMEditMesh::new(bm)));
        }

        {
            let mesh = mesh_b;
            let mut bm = BMesh::new();
            let mut geom = BMeshPartialGeom::default();
            bmesh::primitives::torus(&mut bm, 1.0, 0.25, major_seg, minor_seg, &mut geom);

            ::core::edit_mesh::convert::bmesh_to_mesh(PtrMut(&mut bm), mesh);
        }
    }
}
