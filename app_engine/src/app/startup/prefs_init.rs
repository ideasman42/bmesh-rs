// Licensed: GPL v2+

use ::app::App;

use self::super::prefs_input_keymaps_init::app_startup_prefs_input_keymaps_init;

pub fn app_startup_prefs_init(app: &mut App) {
    app_startup_prefs_input_keymaps_init(app);
}
