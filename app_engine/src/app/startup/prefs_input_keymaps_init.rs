// Licensed: GPL v2+

use ::app::App;

use ::wm::keymap::{
    KeyMap,
    KeyMapCustom,
};

macro_rules! kme {
    ($g:ident, $b:ident, $m:expr) => {
        KeyMapEvent {
            map_code: ::wm::keymap::MapCode::$g,
            value: ::wm::keymap::MapButton::$b,
            modifiers: $m,
        }
    }
}

fn app_startup_prefs_input_keymaps(app: &App) -> Vec<KeyMap> {

    // STUB, but fill some data for purpose of testing.
    use ::wm::keymap::{
        KeyMapItem,
        KeyMapEvent,
        KeyMapAction,
    };

    pub use wm::operator::{
        OpAction,
    };

    // TODO, can we express this in a nicer way?
    #[allow(non_snake_case)] let CTRL = ::wm::keymap::MapCode::CTRL;
    #[allow(non_snake_case)] let SHIFT = ::wm::keymap::MapCode::SHIFT;
    #[allow(non_snake_case)] let ALT = ::wm::keymap::MapCode::ALT;

    let app_p = ::plain_ptr::PtrConst(app);

    let mut keymaps = Vec::new();

    keymaps.push({
        let mut km = KeyMap::new("Window");

        km.items.push(KeyMapItem {
            event: kme!(Z, PRESS, hashset!{CTRL}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "wm.undo_pop")),
        });
        km.items.push(KeyMapItem {
            event: kme!(Z, PRESS, hashset!{CTRL, SHIFT}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "wm.undo_redo")),
        });

        // TODO
        if false {
            km.items.push(KeyMapItem {
                event: kme!(Q, PRESS, hashset!{CTRL}),
                action: KeyMapAction::Operator(OpAction::from_id(app_p, "wm.window_close")),
            });
        }
        km
    });

    keymaps.push({
        let mut km = KeyMap::new("Object");
        km.items.push(KeyMapItem {
            event: kme!(A, PRESS, hashset!{SHIFT}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "wm.menu_call");
                let mut props = op_act.props();
                props.string_set("id", "object.add");
                KeyMapAction::Operator(op_act)
            },
        });
        // Select
        km.items.push(KeyMapItem {
            event: kme!(A, PRESS, hashset!{}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "object.select_all");
                let mut props = op_act.props();
                props.enum_set_id("mode", "TOGGLE");
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(I, PRESS, hashset!{CTRL}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "object.select_invert")),
        });
        km.items.push(KeyMapItem {
            event: kme!(X, PRESS, hashset!{}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "object.delete")),
        });
        km
    });

    keymaps.push({
        let mut km = KeyMap::new("Edit Mesh");
        km.items.push(KeyMapItem {
            event: kme!(A, PRESS, hashset!{SHIFT}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "wm.menu_call");
                let mut props = op_act.props();
                props.string_set("id", "edit_mesh.primitive_add");
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(W, PRESS, hashset!{}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "wm.menu_call");
                let mut props = op_act.props();
                props.string_set("id", "edit_mesh.specials");
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(F, PRESS, hashset!{}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "mesh.edge_face_add")),
        });
        km.items.push(KeyMapItem {
            event: kme!(X, PRESS, hashset!{}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "wm.menu_call");
                let mut props = op_act.props();
                props.string_set("id", "edit_mesh.delete");
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(TAB, PRESS, hashset!{CTRL}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "wm.menu_call");
                let mut props = op_act.props();
                props.string_set("id", "edit_mesh.select_mode");
                KeyMapAction::Operator(op_act)
            },
        });
        // Select
        km.items.push(KeyMapItem {
            event: kme!(A, PRESS, hashset!{}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "mesh.select_all");
                let mut props = op_act.props();
                props.enum_set_id("mode", "TOGGLE");
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(I, PRESS, hashset!{CTRL}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "mesh.select_invert")),
        });
        // Hide
        km.items.push(KeyMapItem {
            event: kme!(H, PRESS, hashset!{}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "mesh.hide");
                let mut props = op_act.props();
                props.bool_set("use_select", true);
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(H, PRESS, hashset!{SHIFT}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "mesh.hide");
                let mut props = op_act.props();
                props.bool_set("use_select", false);
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(H, PRESS, hashset!{ALT}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "mesh.reveal")),
        });
        km
    });

    keymaps.push({
        let mut km = KeyMap::new("View 3D");
        km.items.push(KeyMapItem {
            event: kme!(PAD_5, PRESS, hashset!{}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "view3d.persp_toggle")),
        });
        km.items.push(KeyMapItem {
            event: kme!(MMB, PRESS, hashset!{CTRL}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "view3d.zoom")),
        });
        km.items.push(KeyMapItem {
            event: kme!(MMB, PRESS, hashset!{}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "view3d.rotate")),
        });
        km.items.push(KeyMapItem {
            event: kme!(MMB, PRESS, hashset!{SHIFT}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "view3d.move")),
        });
        km.items.push(KeyMapItem {
            event: kme!(S, PRESS, hashset!{CTRL}),
            action: KeyMapAction::Dummy { text: "There".to_string() },
        });
        km.items.push(KeyMapItem {
            event: kme!(RMB, PRESS, hashset!{}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "view3d.select_pick");
                let mut props = op_act.props();
                props.bool_set("extend", false);
                props.bool_set("toggle", false);
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(RMB, PRESS, hashset!{SHIFT}),
            action: {
                let mut op_act = OpAction::from_id(app_p, "view3d.select_pick");
                let mut props = op_act.props();
                props.bool_set("extend", true);
                props.bool_set("toggle", true);
                KeyMapAction::Operator(op_act)
            },
        });
        km.items.push(KeyMapItem {
            event: kme!(TAB, PRESS, hashset!{}),
            action: KeyMapAction::Operator(OpAction::from_id(app_p, "object.edit_mode_set")),
        });
        km
    });

    return keymaps;
}

// Custom Keymaps - which have an index value
fn app_startup_prefs_input_keymaps_custom() -> Vec<KeyMapCustom> {
    use ::wm::keymap::{
        KeyMapItemCustom,
    };
    use ::wm::keymap::{
        KeyMapEvent,
    };
    let mut keymaps_custom = Vec::new();

    // Keymap for popup menu
    // all operators check for 'menu' and operate on it.
    keymaps_custom.push({
        let mut km = KeyMapCustom::new("UI Menu");
        use ::wm::welem::keymap_events::menu as keymap_events;
        km.items.push(KeyMapItemCustom {
            event: kme!(ESC, PRESS, hashset!{}),
            value: keymap_events::CLOSE,
        });
        km.items.push(KeyMapItemCustom {
            event: kme!(UP_ARROW, PRESS, hashset!{}),
            value: keymap_events::ACTIVE_NEXT,
        });
        km.items.push(KeyMapItemCustom {
            event: kme!(DOWN_ARROW, PRESS, hashset!{}),
            value: keymap_events::ACTIVE_PREV,
        });

        km
    });

    keymaps_custom.push({
        use ::ui::uielem::types::push_button::keymap_events;
        let mut km = KeyMapCustom::new("UI Button");
        km.items.push(KeyMapItemCustom {
            event: kme!(LMB, RELEASE, hashset!{}),
            value: keymap_events::EXEC,
        });
        km.items.push(KeyMapItemCustom {
            event: kme!(RET, PRESS, hashset!{}),
            value: keymap_events::EXEC,
        });
        km.items.push(KeyMapItemCustom {
            event: kme!(RMB, RELEASE, hashset!{}),
            value: keymap_events::MENU,
        });
        km
    });

    return keymaps_custom;
}

pub fn app_startup_prefs_input_keymaps_init(app: &mut App) {
    app.prefs.input.keymaps = app_startup_prefs_input_keymaps(app);
    app.prefs.input.keymaps_custom = app_startup_prefs_input_keymaps_custom();
}
