// Licensed: GPL v2+

///
/// TODO: EnvView, Context - needs to be worked on,
/// for now its just used to get a basic view setup.
///

pub mod rect;
pub mod prefs;

pub mod startup;

use ::prelude::*;

use ::plain_ptr::*;

use ::core::dna::types::{
    Scene,
};

use ::wm::undo_manager::{
    UndoManagerType,
    UndoStep,
};

/// An environment, could also be considered a 'document'
///
/// Each 'env' has its own:
///
/// * Open-file.
/// * File contents (scenes, models... assets).
/// * Undo-steps.
///
/// They don't include:
///
/// * Application state.
/// * User-preferences.
///

pub struct Env {
    pub main: ::core::dna::main_data::MainData,

    /// Could move to screen?
    pub scene: PtrMut<Scene>,

    /// TODO, generic undo system
    pub undo_data: Vec<UndoStep>,
    pub undo_step: usize,
}

impl Env {
    pub fn new() -> Self {
        Env {
            main: Default::default(),
            scene: null_mut(),
            undo_data: Vec::new(),
            undo_step: 0,
        }
    }

    /// Wraps UndoManagerType, handles undo steps.

    pub fn undo_push(&mut self, app: PtrConst<App>, mgr: PtrConst<UndoManagerType>, name: &str) {
        // Remove trailing redo information
        self.undo_data.truncate(self.undo_step + 1);

        // Enforce undo limit
        if self.undo_data.len() > app.prefs.system.undo_steps {
            let truncate = self.undo_data.len() - app.prefs.system.undo_steps;
            self.undo_data.drain(0..truncate);
        }

        self.undo_step = self.undo_data.len();

        let step = (mgr.step_push)(
            app,
            PtrMut(self),
            &mut self.undo_data[..],
            mgr,
            name,
        );

        self.undo_data.push(step);
    }

    pub fn undo_pop(&mut self, app: PtrConst<App>) -> bool {
        if self.undo_step != 0 {
            self.undo_step -= 1;
        }
        if self.undo_step >= self.undo_data.len() {
            return false;
        }

        let env = PtrMut(self).clone();
        let step = &self.undo_data[self.undo_step];
        let mgr = step.ty;
        return (mgr.step_pop)(
            app,
            env,
            PtrConst(step),
        );
    }

    pub fn undo_redo(&mut self, app: PtrConst<App>) -> bool {
        let env = PtrMut(self).clone();
        let undo_step_next = self.undo_step + 1;
        if undo_step_next >= self.undo_data.len() {
            return false;
        }
        let step = &self.undo_data[undo_step_next];
        let mgr = step.ty;
        (mgr.step_pop)(
            app,
            env,
            PtrConst(step),
        );
        self.undo_step = undo_step_next;
        return true;
    }
}

use ::std::collections::BTreeSet;

#[allow(non_camel_case_types)]
pub struct OperatorType_Ord(pub ::wm::operator::OperatorType);
#[allow(non_camel_case_types)]
pub struct MenuType_Ord(pub ::wm::menu::MenuType);
#[allow(non_camel_case_types)]
pub struct WElemType_Ord(pub ::wm::welem::WElemType);
#[allow(non_camel_case_types)]
pub struct UIElemType_Ord(pub ::ui::uielem::UIElemType);
#[allow(non_camel_case_types)]
pub struct StructRNA_Ord(pub ::core::rna::define::StructRNA);
#[allow(non_camel_case_types)]
pub struct UndoManagerType_Ord(pub ::wm::undo_manager::UndoManagerType);

container_type_order_by_member_struct_impl!(
    OperatorType_Ord, ::wm::operator::OperatorType, str, id);
container_type_order_by_member_struct_impl!(
    MenuType_Ord, ::wm::menu::MenuType, str, id);
container_type_order_by_member_struct_impl!(
    WElemType_Ord, ::wm::welem::WElemType, str, id);
container_type_order_by_member_struct_impl!(
    UIElemType_Ord, ::ui::uielem::UIElemType, str, id);
container_type_order_by_member_struct_impl!(
    StructRNA_Ord, ::core::rna::define::StructRNA, str, id);
container_type_order_by_member_struct_impl!(
    UndoManagerType_Ord, ::wm::undo_manager::UndoManagerType, str, id);

/// registered types associated with the running application.
pub struct AppSystem {
    pub operator_types: BTreeSet<OperatorType_Ord>,

    // Menu types
    pub menu_types: BTreeSet<MenuType_Ord>,

    // Window types
    pub welem_types: BTreeSet<WElemType_Ord>,

    // User interface types
    pub uielem_types: BTreeSet<UIElemType_Ord>,

    // Registered struct-types
    pub srna_types: BTreeSet<StructRNA_Ord>,

    // Undo Managers
    pub undo_manager_types: BTreeSet<UndoManagerType_Ord>,

    // for now we only have one
    pub ui_font: ::pxbuf_font::PxBufFont,

    // for serializing dna
    pub dna_info: ::core::dna::io::DNAInfo,
}

// from the perspective of an external user 'App' is a black box.
// we send events into and pull pixels out of for display.
// this design isn't well tested - so we'll see how well it can work.
pub struct App {
    pub windows: Vec<Win>,
    pub envs: Vec<Env>,
    pub prefs: ::app::prefs::Prefs,

    pub system: AppSystem,
}

macro_rules! app_system_types_impl {
    // Could be '$rect_ty:ty' but has issues creating a new struct.
    // See: http://stackoverflow.com/questions/41391522
    ($t_impl:ident, $t_item:ty, $t_item_ord:ident) => {
        pub trait $t_impl {
            fn register(&mut self, $t_item);
            fn find_by_id(&self, id: &str) -> Option<&$t_item>;
        }
        impl $t_impl for BTreeSet<$t_item_ord> {
            #[inline]
            fn register(&mut self, item: $t_item) {
                assert_ne!(0, item.id.len());
                self.insert($t_item_ord(item));
            }
            #[inline]
            fn find_by_id(&self, id: &str) -> Option<&$t_item> {
                if let Some(ret) = self.get(id) {
                    return Some(&ret.0);
                } else {
                    return None;
                }
            }
        }
    }
}

app_system_types_impl!(
    OperatorTypesContainerImpl, ::wm::operator::OperatorType, OperatorType_Ord);
app_system_types_impl!(
    MenuTypesContainerImpl, ::wm::menu::MenuType, MenuType_Ord);
app_system_types_impl!(
    WElemTypesContainerImpl, ::wm::welem::WElemType, WElemType_Ord);
app_system_types_impl!(
    UIElemTypesContainerImpl, ::ui::uielem::UIElemType, UIElemType_Ord);
app_system_types_impl!(
    StructRnaContainerImpl, ::core::rna::define::StructRNA, StructRNA_Ord);
app_system_types_impl!(
    UndoManagerTypesContainerImpl, ::wm::undo_manager::UndoManagerType, UndoManagerType_Ord);


impl App {
    pub fn new() -> Self {
        use pxbuf_font;

        let mut app = App {
            windows: Vec::<Win>::new(),
            envs: Vec::<Env>::new(),
            prefs: ::app::prefs::Prefs::default(),

            system: AppSystem {
                operator_types: BTreeSet::new(),
                menu_types: BTreeSet::new(),
                welem_types: BTreeSet::new(),
                uielem_types: BTreeSet::new(),
                srna_types: BTreeSet::new(),
                undo_manager_types: BTreeSet::new(),
                ui_font: pxbuf_font::PxBufFont::new_fixed_12(),
                dna_info: ::core::dna::io::dna_info_create(),
            },
        };

        // setup system, could be split out, but must only run once.
        {
            ::editor::mesh::construct_ops::register_operators(&mut app);
            ::editor::mesh::delete_ops::register_operators(&mut app);
            ::editor::mesh::marking_ops::register_operators(&mut app);
            ::editor::mesh::primitive_ops::register_operators(&mut app);
            ::editor::mesh::select_ops::register_operators(&mut app);
            ::editor::object::delete_ops::register_operators(&mut app);
            ::editor::object::mode_ops::register_operators(&mut app);
            ::editor::object::select_ops::register_operators(&mut app);
            ::editor::view3d::navigate_ops::register_operators(&mut app);
            ::editor::view3d::select_ops::register_operators(&mut app);
            ::editor::wm::menu_ops::register_operators(&mut app);
            ::editor::wm::undo_ops::register_operators(&mut app);
        }

        // undo managers
        {
            ::editor::wm::undo_manager_global::register_undo_manager(&mut app);
        }

        {
            ::editor::mesh::menu_def::register_menus(&mut app);
        }

        {
            ::wm::welem::register_welems(&mut app);
        }

        {
            ::ui::uielem::types::layout::register_uielems(&mut app);
            ::ui::uielem::types::push_button::register_uielems(&mut app);
        }

        app
    }

    // move to envs.new()?
    pub fn envs_new(&mut self) -> &Env {
        self.envs.push(Env::new());
        return self.envs.last_mut().unwrap();
    }
}

pub fn app_window_new(app: &mut App, size: &[i32; 2]) {

    // We could define the WElem elsewhere, for now this is OK to define here.
    // since all root windows have the same type!
    let welem_root = {
        use app::rect;
        use wm;

        let mut welem_root = wm::welem::WElem::new(
            &app,
            "Window",
            Box::new(()),
            rect::RectI {
                xmin: 0,
                ymin: 0,
                xmax: size[0],
                ymax: size[1],
            }.to_rect_f64(),
        );

        welem_root.children.push(
            wm::welem::WElem::new(
                app,
                "Test", Box::new(()),
                rect::RectI {
                    xmin: size[0] / 4,
                    ymin: size[1] / 4,
                    xmax: size[0] / 2,
                    ymax: size[1] / 2,
                }.to_rect_f64(),
            )
        );

        welem_root.children.push(
            wm::welem::WElem::new(
                app,
                "Test", Box::new(()),
                rect::RectI {
                    xmin: 0,
                    ymin: size[1] / 4,
                    xmax: size[0] / 4,
                    ymax: size[1] / 2,
                }.to_rect_f64(),
            )
        );

        welem_root.children.push(
            wm::welem::WElem::new(
                app,
                "View3D", Box::new(()),
                rect::RectI {
                    xmin: size[0] / 2,
                    ymin: size[1] / 2,
                    xmax: size[0],
                    ymax: size[1],
                }.to_rect_f64(),
            )
        );

        welem_root.children.push(
            wm::welem::WElem::new(
                app,
                "View3D", Box::new(()),
                rect::RectI {
                    xmin: 0,
                    ymin: size[1] / 2,
                    xmax: size[0] / 2,
                    ymax: size[1],
                }.to_rect_f64(),
            )
        );

        welem_root
    };

    app.windows.push(
        Win::new(
            welem_root,
        )
    );
}

pub fn app_env_new(app: &mut App) {
    app.envs.push(Env::new());
}
