
#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub struct RectF {
    pub xmin: f64,
    pub xmax: f64,
    pub ymin: f64,
    pub ymax: f64,
}

#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub struct RectI {
    pub xmin: i32,
    pub xmax: i32,
    pub ymin: i32,
    pub ymax: i32,
}

// Make generic
macro_rules! clamp_min {
    ($value:expr, $clamp:expr) => {
        if !($value >= $clamp) {
            $value = $clamp;
        }
    }
}

macro_rules! clamp_max {
    ($value:expr, $clamp:expr) => {
        if !($value <= $clamp) {
            $value = $clamp;
        }
    }
}

// Second value should be a constant
// Used when first value isn't finite.
macro_rules! max {
    ($value:expr, $clamp:expr) => {
        if !($value >= $clamp) {
            $clamp
        } else {
            $value
        }
    }
}
macro_rules! min {
    ($value:expr, $clamp:expr) => {
        if !($value <= $clamp) {
            $clamp
        } else {
            $value
        }
    }
}

macro_rules! rect_generic_impl {
    // Could be '$rect_ty:ty' but has issues creating a new struct.
    // See: http://stackoverflow.com/questions/41391522
    ($rect_ty:ident, $t_item:ty) => {
        impl $rect_ty {

            // class methods
            pub fn from_zero() -> $rect_ty {
                return $rect_ty {
                    xmin: 0 as $t_item,
                    ymin: 0 as $t_item,
                    xmax: 0 as $t_item,
                    ymax: 0 as $t_item,
                }
            }
            pub fn from_point(co: &[$t_item; 2]) -> $rect_ty {
                return $rect_ty {
                    xmin: co[0],
                    ymin: co[1],
                    xmax: co[0],
                    ymax: co[1],
                }
            }

            // instance methods

            pub fn size_x(&self) -> $t_item { self.xmax - self.xmin }
            pub fn size_y(&self) -> $t_item { self.ymax - self.ymin }
            pub fn size(&self) -> [$t_item; 2] { [self.size_x(), self.size_y()] }

            pub fn cent_x(&self) -> $t_item { (self.xmax + self.xmin) / (2 as $t_item) }
            pub fn cent_y(&self) -> $t_item { (self.ymax + self.ymin) / (2 as $t_item) }
            pub fn cent(&self) -> [$t_item; 2] { [self.cent_x(), self.cent_y()] }

            pub fn isect(&mut self, other: &$rect_ty) {
                clamp_min!(self.xmin, other.xmin);
                clamp_min!(self.ymin, other.ymin);
                clamp_max!(self.xmax, other.xmax);
                clamp_max!(self.ymax, other.ymax);
            }
            pub fn isected(&self, other: &$rect_ty) -> $rect_ty {
                return $rect_ty {
                    xmin: max!(self.xmin, other.xmin),
                    ymin: max!(self.ymin, other.ymin),
                    xmax: min!(self.xmax, other.xmax),
                    ymax: min!(self.ymax, other.ymax),
                }
            }

            pub fn translate(&mut self, offset: &[$t_item; 2]) {
                self.xmin += offset[0];
                self.ymin += offset[1];
                self.xmax += offset[0];
                self.ymax += offset[1];
            }
            pub fn translated(&self, offset: &[$t_item; 2]) -> $rect_ty {
                let mut ret = self.clone();
                ret.translate(offset);
                return ret;
            }

            pub fn recenter(&mut self, center: &[$t_item; 2]) {
                let dx = center[0] - self.cent_x();
                let dy = center[1] - self.cent_y();
                self.translate(&[dx, dy]);
            }
            pub fn recentered(&self, center: &[$t_item; 2]) -> $rect_ty {
                let mut ret = self.clone();
                ret.recenter(center);
                return ret;
            }

            pub fn resize(&mut self, size: &[$t_item; 2]) {
                self.xmin = self.cent_x() - (size[0] / (2 as $t_item));
                self.ymin = self.cent_y() - (size[1] / (2 as $t_item));
                self.xmax = self.xmin + size[0];
                self.ymax = self.ymin + size[1];
            }
            pub fn resized(&self, size: &[$t_item; 2]) -> $rect_ty {
                let mut ret = self.clone();
                ret.resize(size);
                return ret;
            }

            pub fn clamp(&mut self, bounds: &$rect_ty) -> (bool, [$t_item; 2]) {
                let mut changed = false;
                let mut offset = [0 as $t_item; 2];
                if self.xmin < bounds.xmin {
                    let ofs = bounds.xmin - self.xmin;
                    self.xmin += ofs;
                    self.xmax += ofs;
                    offset[0] += ofs;
                    changed = true;
                }
                if self.xmax > bounds.xmax {
                    let ofs = bounds.xmax - self.xmax;
                    self.xmin += ofs;
                    self.xmax += ofs;
                    offset[0] += ofs;
                    changed = true;
                }
                if self.ymin < bounds.ymin {
                    let ofs = bounds.ymin - self.ymin;
                    self.ymin += ofs;
                    self.ymax += ofs;
                    offset[1] += ofs;
                    changed = true;
                }
                if self.ymax > bounds.ymax {
                    let ofs = bounds.ymax - self.ymax;
                    self.ymin += ofs;
                    self.ymax += ofs;
                    offset[1] += ofs;
                    changed = true;
                }
                return (changed, offset);
            }
            pub fn clamped(&mut self, bounds: &$rect_ty) -> ($rect_ty, bool, [$t_item; 2]) {
                let mut ret = self.clone();
                let (changed, offset) = ret.clamp(bounds);
                return (ret, changed, offset);
            }

            // left bottom, right bottom, left top, right top:
            pub fn to_xy_lb(&self) -> [$t_item; 2] { [self.xmin, self.ymin] }
            pub fn to_xy_rb(&self) -> [$t_item; 2] { [self.xmax, self.ymin] }
            pub fn to_xy_lt(&self) -> [$t_item; 2] { [self.xmin, self.ymax] }
            pub fn to_xy_rt(&self) -> [$t_item; 2] { [self.xmax, self.ymax] }

            pub fn isect_point(&self, xy: &[$t_item; 2]) -> bool {
                if xy[0] < self.xmin ||
                   xy[0] > self.xmax ||
                   xy[1] < self.ymin ||
                   xy[1] > self.ymax
                {
                    return false;
                } else {
                    return true;
                }
            }
        }
    };
}

rect_generic_impl!(RectF, f64);
rect_generic_impl!(RectI, i32);

impl RectF {

    pub fn to_rect_i32(&self) -> RectI {
        // Avoids different sizes based on rounding.
        let size_x_fl = self.size_x();
        let size_y_fl = self.size_y();
        let cent_x_fl = self.cent_x();
        let cent_y_fl = self.cent_y();

        let size_x = size_x_fl.round() as i32;
        let size_y = size_y_fl.round() as i32;

        let xmin = (cent_x_fl - (size_x_fl / 2.0)).round() as i32;
        let ymin = (cent_y_fl - (size_y_fl / 2.0)).round() as i32;
        let xmax = xmin + size_x;
        let ymax = ymin + size_y;

        RectI {
            xmin: xmin,
            xmax: xmax,
            ymin: ymin,
            ymax: ymax,
        }
    }

    // Doesn't ensure width remains the same,
    // simple rounding conversion
    pub fn to_rect_i32_round(&self) -> RectI {
        RectI {
            xmin: self.xmin.round() as i32,
            xmax: self.xmax.round() as i32,
            ymin: self.ymin.round() as i32,
            ymax: self.ymax.round() as i32,
        }
    }

    // Where fast/low quality is fine - simply cast.
    pub fn to_rect_i32_fast(&self) -> RectI {
        RectI {
            xmin: self.xmin as i32,
            xmax: self.xmax as i32,
            ymin: self.ymin as i32,
            ymax: self.ymax as i32,
        }
    }

    // Could convert to rect then use to_xy_* methods,
    // but this is so common,
    // better add ability to convert and take a corner with a single method.
    pub fn to_xy_lb_i32_round(&self) -> [i32; 2] {
        [
            self.xmin.round() as i32,
            self.ymin.round() as i32,
        ]
    }
    pub fn to_xy_rb_i32_round(&self) -> [i32; 2] {
        [
            self.xmax.round() as i32,
            self.ymin.round() as i32,
        ]
    }
    pub fn to_xy_lt_i32_round(&self) -> [i32; 2] {
        [
            self.xmin.round() as i32,
            self.ymax.round() as i32,
        ]
    }
    pub fn to_xy_rt_i32_round(&self) -> [i32; 2] {
        [
            self.xmax.round() as i32,
            self.ymax.round() as i32,
        ]
    }
}

impl RectI {
    pub fn to_rect_f64(&self) -> RectF {
        RectF {
            xmin: self.xmin as f64,
            xmax: self.xmax as f64,
            ymin: self.ymin as f64,
            ymax: self.ymax as f64,
        }
    }

    pub fn isect_point_f64(&self, xy: &[f64; 2]) -> bool {
        if xy[0] < self.xmin as f64 ||
           xy[0] > self.xmax as f64 ||
           xy[1] < self.ymin as f64 ||
           xy[1] > self.ymax as f64
        {
            return false;
        } else {
            return true;
        }
    }
}


// ----------------------------------------------------------------------------
// Transform Pre-Calculation

impl RectF {
    pub fn transform_precalc(&self, dst: &RectF) -> RectXFormF {
        let src = self.clone();
        let dst = dst.clone();

        return RectXFormF {
            src_size: src.size(),
            dst_size: dst.size(),
            src: src,
            dst: dst,
        };
    }
}

impl RectXFormF {
    pub fn x(&self, x: f64) -> f64 {
        return (self.dst_size[0] * ((x - self.src.xmin) / self.src_size[0])) + self.dst.xmin;
    }
    pub fn y(&self, y: f64) -> f64 {
        return (self.dst_size[1] * ((y - self.src.ymin) / self.src_size[1])) + self.dst.ymin;
    }

    pub fn xy(&self, pt: &[f64; 2]) -> [f64; 2] {
        return [self.x(pt[0]), self.y(pt[1])];
    }

    pub fn rect(&self, rect: &RectF) -> RectF {
        return RectF {
            xmin: self.x(rect.xmin),
            ymin: self.y(rect.ymin),
            xmax: self.x(rect.xmax),
            ymax: self.y(rect.ymax),
        };
    }
}

#[derive(Debug, Default, Clone)]
pub struct RectXFormF {
    src_size: [f64; 2],
    dst_size: [f64; 2],

    src: RectF,
    dst: RectF,

}

#[derive(Debug, Default, Clone)]
pub struct RectXFormI {
    pub xmin: i32,
    pub xmax: i32,
    pub ymin: i32,
    pub ymax: i32,
}
