// Licensed: Apache 2.0


#[macro_use]
extern crate util_macros;

#[macro_use]
extern crate math_misc;


extern crate list_base;
extern crate mempool;

#[macro_use]
extern crate pxbuf;

extern crate pxbuf_draw;

extern crate pxbuf_font;

extern crate byte_str_util;

extern crate plain_ptr;

// intern
#[macro_use]
extern crate bmesh;

extern crate block_array_cow;

extern crate std_ext;


#[macro_use]
pub mod core;

#[macro_use]
pub mod wm;

pub mod ui;

/// Expose main application API types as top-level
mod app;

pub mod prelude {
    pub use app::{
        App,
        Env,

        OperatorTypesContainerImpl,
        MenuTypesContainerImpl,
        WElemTypesContainerImpl,

        // windows
        app_window_new,

        // environment
        app_env_new,

    };

    pub use app::startup::prefs_init::{
        app_startup_prefs_init,
    };
    pub use app::startup::env_init::{
        app_startup_env_init,
    };

    pub use app::prefs::{
        Prefs,
    };
    pub use app::rect::{
        RectF,
        RectI,
    };

    pub use context::{
        AppContext,
        AppContextMutP,
        AppContextConstP,
    };
    pub use context_store::{
        AppContextStore,
    };

    pub use wm::keymap::{
        KeyMap,
        KeyMapItem,
        KeyMapItemVecImpl,
        KeyMapEvent,
        KeyMapAction,
    };

    pub use wm::window::{
        Win,
    };

    pub use ui::uielem::{
        UIElem,
        UIElemLayout,
    };

}

pub mod camera;
pub mod context;
pub mod context_store;
pub mod view3d;

pub mod editor;

/*
/// Struct to support abstracting application implementations,
/// (headless, windowed, scripted... etc)
pub struct AppType<T> {
    /// Terse identifier, for error messages and the like.
    id: &'static str,
    new_fn: Box<Fn() -> Result<T>>,
    delete_fn: Box<Fn(&mut T)>,
    redraw_fn: Box<Fn(&mut T)>,
    // event_fn: Box<Fn(&mut T)>,
}

pub fn register() {
}
*/


/*
fn main() {
    render_an_object();
}

#[test]
fn test_render() {
    render_an_object();
}

*/

// Local types exported so windowing systems dont need to depend on all our crates
pub mod exports {
    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
}
