// Licensed: GPL v2+

// TODO:
//
// * EnumPropertyRNA
//

use std::borrow::Cow;

use ::plain_ptr::{
    PtrMut,
    PtrConst,
    null_mut,
    null_const,
};
use ::core::dna::types::{
    ID,
};
use ::core::props::{
    PropGroup,
};

use std::any::{
    TypeId,
};

use std::collections::BTreeSet;


// ----
// Bool

#[derive(Debug)]
pub struct BoolPropertyRNA {
    pub default: bool,
}

impl BoolPropertyRNA {
    fn new() -> Self {
        BoolPropertyRNA {
            default: false,
        }
    }
}

// -----
// Float

#[derive(Debug)]
pub struct FloatPropertyRNA {
    pub default: f64,

    min: f64,
    max: f64,

    soft_min: f64,
    soft_max: f64,
}

impl FloatPropertyRNA {
    #[allow(dead_code)]
    fn new() -> Self {
        FloatPropertyRNA {
            default: 0.0,
            min: 0.0,
            max: 1.0,
            soft_min: 0.0,
            soft_max: 1.0,
        }
    }
}

// ---
// Int

#[derive(Debug)]
pub struct IntPropertyRNA {
    pub default: i64,

    min: i64,
    max: i64,

    soft_min: i64,
    soft_max: i64,
}

impl IntPropertyRNA {
    fn new() -> Self {
        IntPropertyRNA {
            default: 0,
            min: 0,
            max: 1,
            soft_min: 0,
            soft_max: 1,
        }
    }
}

// ----
// Enum
//
// Quite like 'Int' internally.

#[derive(Debug, Clone)]
pub struct EnumItem {
    /// Unique identifier or flag.
    pub value: i64,
    /// Upper-case ASCII with underscore separators.
    pub id: String,
    /// Regular text for the UI
    pub name: String,
    /// Tooltip/docs
    pub info: String,
}

impl EnumItem {
    #[inline]
    pub fn new<I>(value: I, id: &str, name: &str, info: &str) -> EnumItem
        where
        I: Into<i64>,
    {
        into_expand!(value);
        EnumItem {
            value: value,
            id: id.to_string(),
            name: name.to_string(),
            info: info.to_string(),
        }
    }
}

pub trait EnumItemImpl {
    fn find_value_from_id(&self, enum_id: &str) -> Option<i64>;
    fn find_id_from_value(&self, value: i64) -> Option<Cow<str>>;
    fn calc_enum_flag_all(&self) -> i64;
}

impl EnumItemImpl for [EnumItem] {
    fn find_value_from_id(&self, enum_id: &str) -> Option<i64> {
        for item in self {
            if item.id == enum_id {
                return Some(item.value);
            }
        }
        return None;
    }
    fn find_id_from_value(&self, value: i64) -> Option<Cow<str>> {
        for item in self {
            if item.value == value {
                return Some(Cow::from(&item.id[..]));
            }
        }
        return None;
    }
    fn calc_enum_flag_all(&self) -> i64 {
        let mut value: i64 = 0;
        for item in self {
            value |= item.value;
        }
        return value;
    }
}

#[derive(Debug)]
pub struct EnumPropertyRNA {
    pub default: i64,
    items: Vec<EnumItem>,

    /// Enum is used as a flag.
    pub is_flag: bool,
}

impl EnumPropertyRNA {
    fn new() -> Self {
        EnumPropertyRNA {
            default: 0,
            items: Vec::new(),
            is_flag: false,
        }
    }

    pub fn items(&self, _ptr: &PointerRNA) -> Cow<[EnumItem]> {
        return Cow::from(&self.items[..]);
    }
}

// ------
// String

#[derive(Debug)]
pub struct StringPropertyRNA {
    pub default: String,
}

impl StringPropertyRNA {
    fn new() -> Self {
        StringPropertyRNA {
            default: "".to_string(),
        }
    }
}

// -----------
// Float Array

#[derive(Debug)]
pub struct FloatArrayPropertyRNA {
    pub default: Option<Vec<f64>>,

    min: f64,
    max: f64,

    soft_min: f64,
    soft_max: f64,

    // TODO, we will want a function
    array_length: usize,
}

impl FloatArrayPropertyRNA {
    #[allow(dead_code)]
    fn new() -> Self {
        FloatArrayPropertyRNA {
            default: None,
            min: 0.0,
            max: 1.0,
            soft_min: 0.0,
            soft_max: 1.0,
            array_length: 0,
        }
    }

    pub fn array_length_get(&self, _ptr: &PointerRNA) -> usize {
        // TODO, callback
        return self.array_length;
    }
}

#[derive(Debug)]
pub enum PropertyRefineRNA {
    Bool(BoolPropertyRNA),
    Float(FloatPropertyRNA),
    Int(IntPropertyRNA),
    Enum(EnumPropertyRNA),

    FloatArray(FloatArrayPropertyRNA),
    // IntArray(IntArrayPropertyRNA),
    // BoolArray(BoolArrayPropertyRNA),

    String(StringPropertyRNA),
}

macro_rules! as_prop_impl {
    ($t:ty, $a:tt, $b:tt) => {
        impl $t {
            #[allow(dead_code)]
            #[inline]
            pub fn as_prop(&self) -> &PropertyRNA {
                // PropertyRNA.data + PropertyRefineRNA::Kind(variant)
                return unsafe {
                    let offset = offset_of!(PropertyRNA, data) + offset_of_enum!($a::$b);
                    &*((((self as *const _) as usize) - offset) as *const PropertyRNA)
                };
            }
        }
    }
}

as_prop_impl!(BoolPropertyRNA, PropertyRefineRNA, Bool);
as_prop_impl!(FloatPropertyRNA, PropertyRefineRNA, Float);
as_prop_impl!(IntPropertyRNA, PropertyRefineRNA, Int);
as_prop_impl!(EnumPropertyRNA, PropertyRefineRNA, Enum);
as_prop_impl!(FloatArrayPropertyRNA, PropertyRefineRNA, FloatArray);
as_prop_impl!(StringPropertyRNA, PropertyRefineRNA, String);

#[derive(Debug)]
pub struct PropertyRNA {
    pub id: String,
    pub name: String,
    pub info: String,
    pub data: PropertyRefineRNA,
}

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub struct PropertyRNA_Ord(pub PropertyRNA);

container_type_order_by_member_struct_impl!(PropertyRNA_Ord, PropertyRNA, str, id);

impl PropertyRNA {
    pub fn new_float_builder(
        id: &str, name: &str,
    ) -> property_builder::FloatBuilderRNA {
        return property_builder::FloatBuilderRNA::new(id, name);
    }
    pub fn new_int_builder(
        id: &str, name: &str,
    ) -> property_builder::IntBuilderRNA {
        return property_builder::IntBuilderRNA::new(id, name);
    }
    pub fn new_bool_builder(
        id: &str, name: &str,
    ) -> property_builder::BoolBuilderRNA {
        return property_builder::BoolBuilderRNA::new(id, name);
    }
    pub fn new_float_array_builder(
        id: &str, name: &str,
    ) -> property_builder::FloatArrayBuilderRNA {
        return property_builder::FloatArrayBuilderRNA::new(id, name);
    }
    pub fn new_enum_builder(
        id: &str, name: &str,
    ) -> property_builder::EnumBuilderRNA {
        return property_builder::EnumBuilderRNA::new(id, name);
    }
    pub fn new_string_builder(
        id: &str, name: &str,
    ) -> property_builder::StringBuilderRNA {
        return property_builder::StringBuilderRNA::new(id, name);
    }
}

pub struct PointerRNA {
    pub id: PtrMut<ID>,
    pub ty: PtrConst<StructRNA>,
    pub data: usize,
    // NOTE: may want to moev this into StructRNA.
    data_type_id: TypeId,
}

impl ::std::fmt::Debug for PointerRNA {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "PointerRNA: <")?;
        if self.ty != null_const() {
            write!(f, "NULL")?;
        } else {
            write!(f, "{}", self.ty.id)?;
        }
        write!(f, " at 0x{:x}", self.data)?;
        Ok(())
    }
}

impl Default for PointerRNA {
    fn default() -> Self {
        PointerRNA {
            id: null_mut(),
            ty: null_const(),
            data: 0,
            // dummy value
            data_type_id: TypeId::of::<()>(),
        }
    }
}

impl PointerRNA {
    pub fn downcast_mut<T: 'static>(&mut self) -> Option<*mut T> {
        if TypeId::of::<T>() == self.data_type_id {
            return Some(self.data as *mut T);
        } else {
            return None;
        }
    }
    pub fn downcast_ref<T: 'static>(&self) -> Option<*const T> {
        if TypeId::of::<T>() == self.data_type_id {
            return Some(self.data as *const T);
        } else {
            return None;
        }
    }
}

pub struct StructRnaIDPropertyFn {
    #[allow(dead_code)]
    get: fn(ptr: &PointerRNA) -> &PropGroup,
    #[allow(dead_code)]
    get_mut: fn(ptr: &mut PointerRNA) -> &mut PropGroup,
    #[allow(dead_code)]
    create: fn(ptr: &mut PointerRNA),
}

pub struct StructRNA {
    pub id: String,
    from: String,

    pub properties: BTreeSet<PropertyRNA_Ord>,

    id_property_fn: Option<StructRnaIDPropertyFn>,
}

impl ::std::fmt::Debug for StructRNA {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(
            f, "StructRNA: <{} from {}, {} properties at 0x{:x}>",
            self.id, self.from, self.properties.len(), self as *const _ as usize,
        )
    }
}

impl Default for StructRNA {
    fn default() -> Self {
        StructRNA {
            id: "".to_string(),
            from: "".to_string(),
            properties: BTreeSet::new(),
            id_property_fn: None,
        }
    }
}

impl StructRNA {
    pub fn new_builder(id: &str) -> struct_builder::StructBuilder {
        return struct_builder::StructBuilder::new(id);
    }
}

macro_rules! find_property_type_fn {
    ($find_property_fn_id:ident, $property_enum_value:ident, $property_type:ty) => {
        #[inline]
        pub fn $find_property_fn_id(&self, id: &str) -> Option<&$property_type> {
            // normally we don't check this,
            // but its handy when operators forget to define their types
            debug_assert!(PtrConst(self) != null_const());
            if let Some(prop) = self.properties.get(id) {
                if let PropertyRefineRNA::$property_enum_value(ref prop_typed) = prop.0.data {
                    return Some(prop_typed);
                }
            }
            return None;
        }
    }
}

impl StructRNA {
    #[inline]
    pub fn find_property(&self, id: &str) -> Option<&PropertyRNA> {
        // normally we don't check this,
        // but its handy when operators forget to define their types
        debug_assert!(PtrConst(self) != null_const());
        if let Some(ret) = self.properties.get(id) {
            return Some(&ret.0);
        } else {
            return None;
        }
    }

    find_property_type_fn!(find_property_bool, Bool, BoolPropertyRNA);
    find_property_type_fn!(find_property_float, Float, FloatPropertyRNA);
    find_property_type_fn!(find_property_int, Int, IntPropertyRNA);
    find_property_type_fn!(find_property_enum, Enum, EnumPropertyRNA);
    find_property_type_fn!(find_property_float_array, FloatArray, FloatArrayPropertyRNA);
    find_property_type_fn!(find_property_string, String, StringPropertyRNA);

    // see Blenders's RNA_pointer_create
    pub fn pointer_create<T: 'static>(&self, id: PtrMut<ID>, data: PtrMut<T>) -> PointerRNA {
        // TODO, refine type
        return PointerRNA {
            id: id,
            ty: PtrConst(self),
            data: data.as_ptr() as usize,
            data_type_id: TypeId::of::<T>(),
        };
    }
}

// ----------------------------------------------------------------------------
// Builders

mod struct_builder {
    use super::{
        StructRNA,
        StructRnaIDPropertyFn,
    };
    pub struct StructBuilder {
        srna: StructRNA,
    }

    impl StructBuilder {

        pub fn exec(self) -> StructRNA {
            self.srna
        }

        pub fn from(mut self, id_from: &str) -> StructBuilder {
            self.srna.from = id_from.to_string();
            self
        }

        pub fn id_property_fn(
            mut self,
            id_property_fn: StructRnaIDPropertyFn,
        ) -> StructBuilder {
            self.srna.id_property_fn = Some(id_property_fn);
            self
        }
    }

    impl StructBuilder {
        pub fn new(id: &str) -> Self {
            StructBuilder {
                srna: StructRNA {
                    id: id.to_string(),
                    .. Default::default()
                },
            }
        }
    }
}

mod property_builder {
    use std::borrow::Cow;

    use super::{
        BoolPropertyRNA,
        FloatPropertyRNA,
        IntPropertyRNA,
        EnumPropertyRNA,
        StringPropertyRNA,

        FloatArrayPropertyRNA,

        PropertyRefineRNA,
        PropertyRNA,
        PropertyRNA_Ord,
        EnumItem,
    };

    macro_rules! prop_native_def_fn {
        ($t:ty, $t_enum:tt) => {
            fn prop_mut(&mut self) -> &mut $t {
                if let PropertyRefineRNA::$t_enum(ref mut value) = self.prop.data {
                    return value;
                }
                unreachable!();
            }
            #[allow(dead_code)]
            fn prop_ref(&self) -> &$t {
                if let PropertyRefineRNA::$t_enum(ref value) = self.prop.data {
                    return value;
                }
                unreachable!();
            }
        }
    }

    macro_rules! prop_range_def_fn {
        ($t:ty, $t_num:ty) => {
            pub fn range(mut self, min: $t_num, max: $t_num) -> Self {
                {
                    let prop = self.prop_mut();
                    prop.min = min;
                    prop.max = max;
                    prop.soft_min = min;
                    prop.soft_max = max;
                }
                self
            }
            pub fn range_soft(mut self, min: $t_num, max: $t_num) -> Self {
                {
                    let prop = self.prop_mut();
                    prop.soft_min = min;
                    prop.soft_max = max;
                }
                self
            }
        }
    }

    macro_rules! prop_array_length_def_fn {
        ($t:ty) => {
            pub fn array_length(mut self, length: usize) -> Self {
                {
                    let prop = self.prop_mut();
                    prop.array_length = length;
                }
                self
            }
        }
    }

    // ----
    // Bool

    pub struct BoolBuilderRNA {
        prop: PropertyRNA,
    }

    impl BoolBuilderRNA {
        prop_native_def_fn!(BoolPropertyRNA, Bool);

        pub fn exec(self) -> PropertyRNA_Ord {
            return PropertyRNA_Ord(self.prop);
        }

        pub fn default(mut self, value: bool) -> Self {
            {
                let prop = self.prop_mut();
                prop.default = value;
            }
            self
        }

        pub fn new(id: &str, name: &str) -> Self {
            BoolBuilderRNA {
                prop: PropertyRNA {
                    id: id.to_string(),
                    name: name.to_string(),
                    info: "".to_string(),
                    data: PropertyRefineRNA::Bool(BoolPropertyRNA::new()),
                }
            }
        }
    }

    // -----
    // Float

    pub struct FloatBuilderRNA {
        prop: PropertyRNA,
    }

    impl FloatBuilderRNA {
        prop_native_def_fn!(FloatPropertyRNA, Float);
        prop_range_def_fn!(FloatBuilderRNA, f64);

        pub fn exec(self) -> PropertyRNA_Ord {
            return PropertyRNA_Ord(self.prop);
        }

        pub fn default(mut self, value: f64) -> Self {
            {
                let prop = self.prop_mut();
                prop.default = value;
            }
            self
        }

        pub fn new(id: &str, name: &str) -> Self {
            FloatBuilderRNA {
                prop: PropertyRNA {
                    id: id.to_string(),
                    name: name.to_string(),
                    info: "".to_string(),
                    data: PropertyRefineRNA::Float(FloatPropertyRNA::new()),
                }
            }
        }
    }

    // ---
    // Int

    pub struct IntBuilderRNA {
        prop: PropertyRNA,
    }

    impl IntBuilderRNA {
        prop_native_def_fn!(IntPropertyRNA, Int);
        prop_range_def_fn!(IntBuilderRNA, i64);

        pub fn exec(self) -> PropertyRNA_Ord {
            return PropertyRNA_Ord(self.prop);
        }

        pub fn default(mut self, value: i64) -> Self {
            {
                let prop = self.prop_mut();
                prop.default = value;
            }
            self
        }

        pub fn new(id: &str, name: &str) -> Self {
            IntBuilderRNA {
                prop: PropertyRNA {
                    id: id.to_string(),
                    name: name.to_string(),
                    info: "".to_string(),
                    data: PropertyRefineRNA::Int(IntPropertyRNA::new()),
                }
            }
        }
    }

    // -----------
    // Float Array

    pub struct FloatArrayBuilderRNA {
        prop: PropertyRNA,
    }

    impl FloatArrayBuilderRNA {
        prop_native_def_fn!(FloatArrayPropertyRNA, FloatArray);
        prop_range_def_fn!(FloatArrayBuilderRNA, f64);
        prop_array_length_def_fn!(FloatArrayBuilderRNA);

        pub fn exec(self) -> PropertyRNA_Ord {
            {
                let prop = self.prop_ref();
                debug_assert!(prop.array_length != 0);
            }
            return PropertyRNA_Ord(self.prop);
        }

        pub fn default(mut self, value: Vec<f64>) -> Self {
            {
                let prop = self.prop_mut();
                prop.default = Some(value);
            }
            self
        }

        pub fn new(id: &str, name: &str) -> Self {
            FloatArrayBuilderRNA {
                prop: PropertyRNA {
                    id: id.to_string(),
                    name: name.to_string(),
                    info: "".to_string(),
                    data: PropertyRefineRNA::FloatArray(FloatArrayPropertyRNA::new()),
                }
            }
        }
    }

    // ------
    // String

    pub struct StringBuilderRNA {
        prop: PropertyRNA,
    }

    impl StringBuilderRNA {
        prop_native_def_fn!(StringPropertyRNA, String);

        pub fn exec(self) -> PropertyRNA_Ord {
            return PropertyRNA_Ord(self.prop);
        }

        pub fn default(mut self, value: &str) -> Self {
            {
                let prop = self.prop_mut();
                prop.default = value.to_string();
            }
            self
        }

        pub fn new(id: &str, name: &str) -> Self {
            StringBuilderRNA {
                prop: PropertyRNA {
                    id: id.to_string(),
                    name: name.to_string(),
                    info: "".to_string(),
                    data: PropertyRefineRNA::String(StringPropertyRNA::new()),
                }
            }
        }
    }

    // ----
    // Enum

    pub struct EnumBuilderRNA {
        prop: PropertyRNA,
    }

    impl EnumBuilderRNA {
        prop_native_def_fn!(EnumPropertyRNA, Enum);

        pub fn exec(self) -> PropertyRNA_Ord {
            return PropertyRNA_Ord(self.prop);
        }

        pub fn default(mut self, value: i64) -> Self {
            {
                let prop = self.prop_mut();
                prop.default = value;
            }
            self
        }

        pub fn items(mut self, items: Cow<[EnumItem]>) -> Self {
            {
                // note: we may want to store items as a Cow.
                let prop = self.prop_mut();
                prop.items = items.to_vec();
            }
            self
        }

        /// So we can re-use items
        pub fn items_ref(mut self, items: &[EnumItem]) -> Self {
            {
                let prop = self.prop_mut();
                debug_assert!(prop.items.is_empty());
                prop.items.clone_from_slice(items);
            }
            self
        }

        pub fn new(id: &str, name: &str) -> Self {
            EnumBuilderRNA {
                prop: PropertyRNA {
                    id: id.to_string(),
                    name: name.to_string(),
                    info: "".to_string(),
                    data: PropertyRefineRNA::Enum(EnumPropertyRNA::new()),
                }
            }
        }
    }

}
