// Licensed: GPL v2+

use std::borrow::Cow;

use super::define::{
    PointerRNA,
    PropertyRNA,

    BoolPropertyRNA,
    FloatPropertyRNA,
    IntPropertyRNA,
    EnumPropertyRNA,
    StringPropertyRNA,

    FloatArrayPropertyRNA,

    EnumItemImpl,
};

pub use core::props::{
    PropAccess,
    PropGroup,
};


fn _println_prop_missing(
    ptr: &PointerRNA,
    id: &str,
) {
    println!("'{}.{}': property missing",
             ptr.ty.id, id
    );
}
macro_rules! println_prop_missing {
    ($ptr:ident, $prop_id:ident) => {
        _println_prop_missing($ptr, $prop_id);
        if cfg!(debug_assertions) {
            panic!();
        }
    }
}

fn _println_prop_array_len_invalid(
    ptr: &PointerRNA,
    prop: &PropertyRNA,
    len_found: usize,
    len_expect: usize,
) {
    println!("'{}.{}': invalid array length: {} expected {}",
             ptr.ty.id, prop.id,
             len_found, len_expect,
    );
}
macro_rules! println_prop_array_len_invalid {
    ($ptr:ident, $prop:ident, $value:expr, $value_expect_len:expr) => {
        _println_prop_array_len_invalid($ptr, $prop.as_prop(), $value.len(), $value_expect_len);
    }
}

impl PointerRNA {

    // ------------------------
    // Property Access (Single)

    pub fn bool_property_get(&self, prop: &BoolPropertyRNA) -> bool {
        if let Some(prop_group) = self.downcast_ref::<PropGroup>() {
            unsafe {
                if let Some(prop_value) = (*prop_group).bool_get(&prop.as_prop().id) {
                    return prop_value;
                }
            }
        }
        return prop.default;
    }
    pub fn bool_property_set(&mut self, prop: &BoolPropertyRNA, value: bool) {
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            unsafe {
                (*prop_group).bool_set(&prop.as_prop().id, value);
            }
        }
    }

    pub fn float_property_get(&self, prop: &FloatPropertyRNA) -> f64 {
        if let Some(prop_group) = self.downcast_ref::<PropGroup>() {
            unsafe {
                if let Some(prop_value) = (*prop_group).float_get(&prop.as_prop().id) {
                    return prop_value;
                }
            }
        }
        return prop.default;
    }
    pub fn float_property_set(&mut self, prop: &FloatPropertyRNA, value: f64) {
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            unsafe {
                (*prop_group).float_set(&prop.as_prop().id, value);
            }
        }
    }

    pub fn int_property_get(&self, prop: &IntPropertyRNA) -> i64 {
        if let Some(prop_group) = self.downcast_ref::<PropGroup>() {
            unsafe {
                if let Some(prop_value) = (*prop_group).int_get(&prop.as_prop().id) {
                    return prop_value;
                }
            }
        }
        return prop.default;
    }
    pub fn int_property_set(&mut self, prop: &IntPropertyRNA, value: i64) {
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            unsafe {
                (*prop_group).int_set(&prop.as_prop().id, value);
            }
        }
    }

    pub fn enum_property_get(&self, prop: &EnumPropertyRNA) -> i64 {
        if let Some(prop_group) = self.downcast_ref::<PropGroup>() {
            unsafe {
                if let Some(prop_value) = (*prop_group).int_get(&prop.as_prop().id) {
                    return prop_value;
                }
            }
        }
        return prop.default;
    }
    pub fn enum_property_set(&mut self, prop: &EnumPropertyRNA, value: i64) {
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            // Sanity check for debug mode, ensure we're setting a value that's in the enum.
            if cfg!(debug_assertions) {
                let items = prop.items(self);
                if prop.is_flag == false {
                    if items.find_id_from_value(value).is_none() {
                        println!("bad enum value {}", value);
                        panic!();
                    }
                } else {
                    let value_test = value & !items.calc_enum_flag_all();
                    if value_test != 0 {
                        println!("bad enum value {}, invalid bits {}", value, value_test);
                        panic!();
                    }
                }
            }
            // end debug sanity check
            unsafe {
                (*prop_group).int_set(&prop.as_prop().id, value);
            }
        }
    }
    pub fn enum_property_set_id(&mut self, prop: &EnumPropertyRNA, enum_id: &str) {
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            let items = prop.items(self);
            if let Some(value) = items.find_value_from_id(enum_id) {
                unsafe {
                    (*prop_group).int_set(&prop.as_prop().id, value);
                }
            } else {
                println!("Missing '{}' from {:?}", enum_id, items);
                panic!();
            }
        }
    }

    pub fn string_property_get<'a>(&'a self, prop: &'a StringPropertyRNA) -> Cow<'a, str> {
        if let Some(prop_group) = self.downcast_ref::<PropGroup>() {
            unsafe {
                if let Some(prop_value) = (*prop_group).string_get(&prop.as_prop().id) {
                    return Cow::from(&prop_value[..]);
                }
            }
        }
        return Cow::from(&prop.default[..]);
    }
    pub fn string_property_set<'a, T: Into<Cow<'a, str>>>(
        &mut self,
        prop: &StringPropertyRNA,
        value: T,
    ) {
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            unsafe {
                (*prop_group).string_set(&prop.as_prop().id, value);
            }
        }
    }

    // -----------------------
    // Property Access (Array)

    pub fn float_array_property_get<'a>(
        &'a self,
        prop: &'a FloatArrayPropertyRNA,
    ) -> Cow<'a, [f64]> {
        if let Some(prop_group) = self.downcast_ref::<PropGroup>() {
            unsafe {
                if let Some(prop_value) = (*prop_group).float_array_get(&prop.as_prop().id) {
                    return Cow::from(&prop_value[..]);
                }
            }
        }
        if let Some(ref prop_value) = prop.default {
            return Cow::from(&prop_value[..]);
        } else {
            return Cow::from(vec![0.0; prop.array_length_get(self)]);
        }
    }
    pub fn float_array_property_set<'a, T>(&mut self, prop: &FloatArrayPropertyRNA, value: T)
        where
        T: Into<Cow<'a, [f64]>>,
    {
        into_expand!(value);
        let array_length = prop.array_length_get(self);
        if value.len() != array_length {
            println_prop_array_len_invalid!(self, prop, value, array_length);
            panic!();
        }
        if let Some(prop_group) = self.downcast_mut::<PropGroup>() {
            unsafe {
                (*prop_group).float_array_set(&prop.as_prop().id, value);
            }
            return;
        }
        unreachable!();
    }

    // -------------
    // String Access

    pub fn bool_get(&self, id: &str) -> bool {
        if let Some(prop) = self.ty.find_property_bool(id) {
            return self.bool_property_get(prop);
        } else {
            println_prop_missing!(self, id);
            return false;
        }
    }
    pub fn bool_set(&mut self, id: &str, value: bool) {
        if let Some(prop) = self.ty.clone().find_property_bool(id) {
            return self.bool_property_set(prop, value);
        } else {
            println_prop_missing!(self, id);
        }
    }

    pub fn float_get(&self, id: &str) -> f64 {
        if let Some(prop) = self.ty.find_property_float(id) {
            return self.float_property_get(prop);
        } else {
            println_prop_missing!(self, id);
            return 0.0;
        }
    }
    pub fn float_set(&mut self, id: &str, value: f64) {
        if let Some(prop) = self.ty.clone().find_property_float(id) {
            return self.float_property_set(prop, value);
        } else {
            println_prop_missing!(self, id);
        }
    }

    pub fn int_get(&self, id: &str) -> i64 {
        if let Some(prop) = self.ty.find_property_int(id) {
            return self.int_property_get(prop);
        } else {
            println_prop_missing!(self, id);
            return 0;
        }
    }
    pub fn int_set(&mut self, id: &str, value: i64) {
        if let Some(prop) = self.ty.clone().find_property_int(id) {
            return self.int_property_set(prop, value);
        } else {
            println_prop_missing!(self, id);
        }
    }

    pub fn enum_get(&self, id: &str) -> i64 {
        if let Some(prop) = self.ty.find_property_enum(id) {
            return self.enum_property_get(prop);
        } else {
            println_prop_missing!(self, id);
            return 0;
        }
    }
    pub fn enum_set(&mut self, id: &str, value: i64) {
        if let Some(prop) = self.ty.clone().find_property_enum(id) {
            return self.enum_property_set(prop, value);
        } else {
            println_prop_missing!(self, id);
        }
    }
    pub fn enum_set_id(&mut self, id: &str, enum_id: &str) {
        if let Some(prop) = self.ty.clone().find_property_enum(id) {
            return self.enum_property_set_id(prop, enum_id);
        } else {
            println_prop_missing!(self, id);
        }
    }

    pub fn string_get(&self, id: &str) -> Option<Cow<str>> {
        if let Some(prop) = self.ty.find_property_string(id) {
            return Some(self.string_property_get(prop));
        } else {
            println_prop_missing!(self, id);
            return None;
        }
    }
    pub fn string_set<'a, T>(&mut self, id: &str, value: T)
        where
        T: Into<Cow<'a, str>>,
    {
        if let Some(prop) = self.ty.clone().find_property_string(id) {
            return self.string_property_set(prop, value);
        } else {
            println_prop_missing!(self, id);
        }
    }

    pub fn float_array_get<'a>(&'a self, id: &str) -> Option<Cow<[f64]>> {
        if let Some(prop) = self.ty.find_property_float_array(id) {
            return Some(self.float_array_property_get(prop));
        } else {
            println_prop_missing!(self, id);
            return None;
        }
    }
    pub fn float_array_set<'a, T>(&mut self, id: &str, value: T)
        where
        T: Into<Cow<'a, [f64]>>,
    {
        if let Some(prop) = self.ty.clone().find_property_float_array(id) {
            return self.float_array_property_set(prop, value);
        } else {
            println_prop_missing!(self, id);
        }
    }

}
