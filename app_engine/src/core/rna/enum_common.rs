// Licensed: GPL v2+

///
/// Reusable enums, shared between operators.
///
/// TODO:
/// Figure out how we can have 'static' items
/// which can be stack and heap allocated.
/// Could be done using `Cow<'static, str>`.
///

mod local_prelude {
    pub use std::borrow::Cow;
    pub use super::super::define::{
        EnumItem,
    };
}

pub mod enum_select_all {
    use super::local_prelude::*;
    #[repr(i64)]
    #[derive(Clone, Copy)]
    pub enum Enum {
        Toggle = 0,
        Select = 1,
        DeSelect = 2,
    }
    rna_enum_impl!(Enum);

    pub fn items() -> Cow<'static, [EnumItem]> {
        return Cow::from(vec![
            EnumItem::new(Enum::Toggle, "TOGGLE", "Toggle", ""),
            EnumItem::new(Enum::Select, "SELECT", "Select", ""),
            EnumItem::new(Enum::DeSelect, "DESELECT", "DeSelect", ""),
        ]);
    }
}

pub mod enum_mesh_elem_noloop {
    use super::local_prelude::*;

    pub use bmesh::prelude::*;

    #[repr(i64)]
    #[derive(Clone, Copy)]
    pub enum Enum {
        // unimplemented constant expression: tuple struct constructors
        /*
        Vert = BM_VERT.0,
        Edge = BM_EDGE.0,
        Face = BM_FACE.0,
        */
        Vert = 1,
        Edge = 2,
        Face = 8,
    }
    rna_enum_impl!(Enum);

    pub fn items() -> Cow<'static, [EnumItem]> {
        return Cow::from(vec![
            EnumItem::new(Enum::Vert, "VERT", "Vert", ""),
            EnumItem::new(Enum::Edge, "EDGE", "Edge", ""),
            EnumItem::new(Enum::Face, "FACE", "Face", ""),
        ]);
    }
}
