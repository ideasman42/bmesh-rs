
///
/// RNA enums use i64 internally,
/// this macro avoids copy-pasting similar impl's.
///
/// This is used for native Rust enum's, not 'EnumItem',
/// to assist mixing the value with regular i64's.
///
#[macro_export]
macro_rules! rna_enum_impl {
    ($t:ty) => {
        impl From<$t> for i64 {
            fn from(value: $t) -> i64 {
                value as i64
            }
        }
        /// So we can use the value within a 'prop_map!{..}'.
        impl $t {
            #[inline]
            pub fn from_native(self) -> ::core::props::PropValue {
                ::core::props::PropValue::Int(self as i64)
            }
        }

        /// So we can compare with int's
        impl PartialEq<i64> for $t {
            fn eq(&self, other: &i64) -> bool {
                &(*self as i64) == other
            }
        }
        impl PartialEq<$t> for i64 {
            fn eq(&self, other: &$t) -> bool {
                self == &(*other as i64)
            }
        }
    }
}


// Similar to 'EnumItem::new' but for when we have consts.
/*
#[macro_export]
macro_rules! rna_enum_item {
    ($value:expr, $id:expr, $name:expr) => {
        ::core::rna::define::EnumItem {
            value: $value as _,
            id: $id,
            name: $name,
        }
    }
}
*/
