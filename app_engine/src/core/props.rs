// Licensed: GPL v2+

use std::borrow::Cow;

/// This is intended to be something like JSON
/// where you can store arbitrary simple data, recursively.
/// Using unordered strings as keys.

// Was originally hashmap macro from maplit crate, modified to use own types.
//
// How to match against '{ body }' in a recursive macro?
#[macro_export]
macro_rules! prop_map {
    (@single $($x:tt)*) => (());
    (@count $($rest:expr),*) => (<[()]>::len(&[$(prop_map!(@single $rest)),*]));

    // <-- How can this match against braces?
    // ($($key:expr => { $( $body:expr ),* } );*) => { $($key => prop_map!( $body ),*) ,* };

    ($($key:expr => $value:expr,)+) => { prop_map!($($key => $value),+) };
    ($($key:expr => $value:expr),*) => {
        {
            // depending on the type - we will only use one,
            // both define self.from_native(), but for different types.
            #[allow(unused_imports)]
            use ::core::props::{
                PropValueFromNativeRef,
                PropValueFromNativeVal,
            };
            let _cap = prop_map!(@count $($key),*);
            let mut _map = ::core::props::PropGroup::with_capacity(_cap);
            $(
                _map.insert(String::from($key), $value.from_native());
            )*
            _map
        }
    };
}

pub type PropGroup = ::std::collections::HashMap<String, PropValue>;

/// Note: types are intentionally limited here,
/// so in future we can have general operations
/// (convert to/from other representations for eg),
/// without causing a lot of hassles.
///
/// New types can be added as needed (Vector, Arrays of ints, floats .. etc)

#[derive(Debug, Clone)]
pub enum PropValue {
    Group(PropGroup),
    Array(Vec<PropValue>),

    Float(f64),
    Int(i64),
    Bool(bool),

    FloatArray(Vec<f64>),
    IntArray(Vec<i64>),
    BoolArray(Vec<bool>),

    String(String),

    // TODO, vectors, arrays of simple values...
    // just add these as needed.
}

// Needed for static strings which can't be passed by value
pub trait PropValueFromNativeRef {
    #[inline] fn from_native(&self) -> PropValue;
}

pub trait PropValueFromNativeVal {
    #[inline] fn from_native(self) -> PropValue;
}

/// The identity function.
fn identity<T>(x: T) -> T { x }

macro_rules! prop_value_from_native_ref_impl {
    ($t_native:ty, $t_enum:tt, $from_expr:expr) => {
        impl PropValueFromNativeRef for $t_native {
            #[inline]
            fn from_native(&self) -> PropValue { PropValue::$t_enum($from_expr(self)) }
        }
    };
    ($t_native:ty, $t_enum:tt) => {
        prop_value_from_native_ref_impl!($t_native, $t_enum, identity);
    };
}

macro_rules! prop_value_from_native_val_impl {
    ($t_native:ty, $t_enum:tt, $from_expr:expr) => {
        impl PropValueFromNativeVal for $t_native {
            #[inline]
            fn from_native(self) -> PropValue { PropValue::$t_enum($from_expr(self)) }
        }
    };
    ($t_native:ty, $t_enum:tt) => {
        prop_value_from_native_val_impl!($t_native, $t_enum, identity);
    };
}

prop_value_from_native_ref_impl!(str, String, String::from);

prop_value_from_native_val_impl!(PropGroup, Group);

prop_value_from_native_val_impl!(f64, Float);
prop_value_from_native_val_impl!(i64, Int);
prop_value_from_native_val_impl!(bool, Bool);
prop_value_from_native_val_impl!(String, String);

// We could allow other kinds to coerce, but for now rather not!
// prop_value_from_native_val_impl!(i32, Int, i64::from);

// -----------------------------------------------------------------------------
// Helpers

pub trait PropAccess {
    fn group_get(&self, key: &str) -> Option<&PropGroup>;
    fn group_get_mut(&mut self, key: &str) -> Option<&mut PropGroup>;

    fn float_get(&self, key: &str) -> Option<f64>;
    fn float_set(&mut self, key: &str, val: f64);

    fn int_get(&self, key: &str) -> Option<i64>;
    fn int_set(&mut self, key: &str, value: i64);

    fn bool_get(&self, key: &str) -> Option<bool>;
    fn bool_set(&mut self, key: &str, value: bool);

    fn string_get(&self, key: &str) -> Option<&String>;
    fn string_get_mut(&mut self, key: &str) -> Option<&mut String>;
    fn string_set<'a, T: Into<Cow<'a, str>>>(&mut self, key: &str, value: T);

    fn float_array_get(&self, key: &str) -> Option<&Vec<f64>>;
    fn float_array_set<'a, T: Into<Cow<'a, [f64]>>>(&mut self, key: &str, val: T);
}

///
/// When a property is found but the wrong type.
///
macro_rules! println_debug_message {
    ($key:expr, $value:expr, $type_name:expr) => {
        if cfg!(debug_assertions) {
            println!(
                "prop: {} found but not of type 'PropValue::{}(i)' {:?}",
                $key, $type_name, $value,
            );
        }
    }
}

impl PropAccess for PropGroup {

    fn group_get(&self, key: &str) -> Option<&PropGroup> {
        if let Some(v) = self.get(key) {
            if let PropValue::Group(ref i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "Group");
            }
        }
        return None;
    }

    fn group_get_mut(&mut self, key: &str) -> Option<&mut PropGroup> {
        if let Some(v) = self.get_mut(key) {
            if let PropValue::Group(ref mut i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "Group");
            }
        }
        return None;
    }

    fn float_get(&self, key: &str) -> Option<f64> {
        if let Some(v) = self.get(key) {
            if let PropValue::Float(i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "Float");
            }
        }
        return None;
    }
    fn float_set(&mut self, key: &str, val: f64) {
        self.insert(key.to_string(), PropValue::Float(val));
    }

    fn int_get(&self, key: &str) -> Option<i64> {
        if let Some(v) = self.get(key) {
            if let PropValue::Int(i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "Int");
            }
        }
        return None;
    }
    fn int_set(&mut self, key: &str, val: i64) {
        self.insert(key.to_string(), PropValue::Int(val));
    }

    fn bool_get(&self, key: &str) -> Option<bool> {
        if let Some(v) = self.get(key) {
            if let PropValue::Bool(i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "Bool");
            }
        }
        return None;
    }
    fn bool_set(&mut self, key: &str, val: bool) {
        self.insert(key.to_string(), PropValue::Bool(val));
    }

    fn string_get(&self, key: &str) -> Option<&String> {
        if let Some(v) = self.get(key) {
            if let PropValue::String(ref i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "String");
            }
        }
        return None;
    }

    fn string_get_mut(&mut self, key: &str) -> Option<&mut String> {
        if let Some(v) = self.get_mut(key) {
            if let PropValue::String(ref mut i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "String");
            }
        }
        return None;
    }

    fn string_set<'a, T>(&mut self, key: &str, value: T)
        where
        T: Into<Cow<'a, str>>,
    {
        into_expand!(value);
        self.insert(key.to_string(), PropValue::String(value.into_owned()));
    }

    fn float_array_get(&self, key: &str) -> Option<&Vec<f64>> {
        if let Some(v) = self.get(key) {
            if let PropValue::FloatArray(ref i) = *v {
                return Some(i)
            } else {
                println_debug_message!(key, v, "FloatArray");
            }
        }
        return None;
    }

    fn float_array_set<'a, T>(&mut self, key: &str, val: T)
        where
        T: Into<Cow<'a, [f64]>>,
    {
        into_expand!(val);
        self.insert(key.to_string(), PropValue::FloatArray(val.into_owned()));
    }
}

