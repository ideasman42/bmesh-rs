// Licensed: GPL v2+

// Core modules for the application,
// not windowing or editors.

// data
pub mod dna;

// id-properties
#[macro_use]
pub mod props;

// data API
pub mod rna;

// data library
pub mod wli;

// BMEditMesh
#[macro_use]
pub mod edit_mesh;
