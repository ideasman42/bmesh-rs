// Licensed: GPL v2+

// Similar in scope to Blender 2.78's.
// source/blender/blenkernel/intern/editmesh.c
// .. but not a direct port.

use bmesh::prelude::*;

use ::core::dna::types::{
    Mesh,
    Object,
};

#[allow(dead_code)]
pub struct BMEditMesh {
    pub bm: BMesh,

    // we store tessellations as triplets of three loops,
    // which each define a triangle.
    pub looptris: Vec<[BMLoopMutP; 3]>,
}

impl BMEditMesh {
    pub fn new(bm: BMesh) -> Self {
        BMEditMesh {
            bm: bm,
            looptris: Vec::new(),
        }
    }

    pub fn from_object_get_mut(object: PtrMut<Object>) -> Option<PtrMut<BMEditMesh>> {
        if let Some(mut data) = object.data.as_option() {
            if let Some(mut mesh) = data.downcast_mut::<Mesh>() {
                if let Some(ref mut em) = mesh.rt.em {
                    return Some(PtrMut(&mut **em));
                }
            }
        }
        return None;
    }

    pub fn from_object_get(object: PtrConst<Object>) -> Option<PtrConst<BMEditMesh>> {
        if let Some(data) = object.data.as_option() {
            if let Some(mesh) = data.downcast_ref::<Mesh>() {
                if let Some(ref em) = mesh.rt.em {
                    return Some(PtrConst(&**em));
                }
            }
        }
        return None;
    }

    #[inline]
    pub fn select_mode_set(
        self: &mut BMEditMesh,
        mode_old: BMElemType,
        mode_new: BMElemType,
    ) {
        use self::super::select::bm_mesh_select_mode_set;
        bm_mesh_select_mode_set(&mut self.bm, mode_old, mode_new);
    }
}
