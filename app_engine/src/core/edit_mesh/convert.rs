// Licensed: GPL v2+

use ::core::dna::types::{
    Mesh,
};

use ::bmesh::prelude::*;

pub fn bmesh_to_mesh(mut bm: PtrMut<BMesh>, mut me: PtrMut<Mesh>) {
    use ::core::dna::types::{
        MVert, MEdge, MLoop, MFace,
    };

    me.verts.clear();
    me.verts.reserve_exact(bm.verts.len());

    me.edges.clear();
    me.edges.reserve_exact(bm.edges.len());

    me.loops.clear();
    me.loops.reserve_exact(bm.loops.len());

    me.faces.clear();
    me.faces.reserve_exact(bm.faces.len());

    {
        let mut index: i32 = 0;
        for mut v in bm.verts.iter_mut() {
            me.verts.push(
                MVert {
                    co: v.co,
                    flag: v.head.hflag.0,
                }
            );
            v.head.index = index;
            index += 1;
        }
        bm.elem_index_dirty &= !BM_VERT;
    }

    {
        let mut index: i32 = 0;
        for mut e in bm.edges.iter_mut() {
            me.edges.push(
                MEdge {
                    verts: [
                        e.verts[0].head.index as u32,
                        e.verts[1].head.index as u32,
                    ],
                    flag: e.head.hflag.0,
                }
            );
            e.head.index = index;
            index += 1;
        }
        bm.elem_index_dirty &= !BM_EDGE;
    }

    {
        let mut face_index: i32 = 0;
        let mut loop_index: i32 = 0;
        for mut f in bm.faces.iter_mut() {
            me.faces.push(
                MFace {
                    loop_start: loop_index as u32,
                    loop_len: f.len as u32,
                    flag: f.head.hflag.0,
                }
            );

            bm_iter_loops_of_face_cycle!(f, l_iter, {
                me.loops.push(
                    MLoop {
                        v: l_iter.v.head.index as u32,
                        e: l_iter.e.head.index as u32,
                    }
                );
                loop_index += 1;
            });
            f.head.index = face_index;
            face_index += 1;
        }
        bm.elem_index_dirty &= !BM_FACE;
    }

}

pub fn bmesh_from_mesh(mut bm: PtrMut<BMesh>, me: PtrMut<Mesh>) {

    let mut vtable: Vec<PtrMut<BMVert>> = Vec::with_capacity(me.verts.len());
    let mut etable: Vec<PtrMut<BMEdge>> = Vec::with_capacity(me.edges.len());
    let mut ltable: Vec<PtrMut<BMLoop>> = Vec::with_capacity(me.loops.len());
    let mut ftable: Vec<PtrMut<BMFace>> = Vec::with_capacity(me.faces.len());

    unsafe {
        vtable.set_len(me.verts.len());
        etable.set_len(me.edges.len());
        ltable.set_len(me.loops.len());
        ftable.set_len(me.faces.len());
    }

    {
        let mut index: i32 = 0;
        for v in &me.verts {
            let mut bm_v = bm.verts.new(
                &v.co,
                None,
                BMElemCreate::NOP,
            );
            bm_v.co = v.co;
            bm_v.head.hflag = BMElemFlag(v.flag);
            bm_v.head.index = index;
            vtable[index as usize] = bm_v;
            index += 1;
        }
        bm.elem_index_dirty &= !BM_VERT;
    }

    {
        let mut index: i32 = 0;
        for e in &me.edges {
            let mut bm_e = bm.edges.new(
                &[
                    vtable[e.verts[0] as usize],
                    vtable[e.verts[1] as usize],
                ],
                None,
                BMElemCreate::NOP,
            );
            bm_e.head.hflag = BMElemFlag(e.flag);
            bm_e.head.index = index;
            etable[index as usize] = bm_e;
            index += 1;
        }
        bm.elem_index_dirty &= !BM_EDGE;
    }

    {
        let mut f_verts: Vec<PtrMut<BMVert>> = Vec::with_capacity(32);
        let mut f_edges: Vec<PtrMut<BMEdge>> = Vec::with_capacity(32);

        let mut index: i32 = 0;
        for f in &me.faces {
            let len = f.loop_len as usize;
            let loop_start = f.loop_start as usize;

            f_verts.reserve(len);
            f_edges.reserve(len);
            unsafe {
                f_verts.set_len(len);
                f_edges.set_len(len);
            }

            for i in 0..len {
                let l = &me.loops[loop_start + i];

                f_verts[i] = vtable[l.v as usize];
                f_edges[i] = etable[l.e as usize];
            }

            let mut bm_f = bm.faces.new(
                &f_verts[..],
                &f_edges[..],
                None,
                BMElemCreate::NOP,
            );

            f_verts.clear();
            f_edges.clear();

            bm_f.head.hflag = BMElemFlag(f.flag);
            bm_f.head.index = index;

            ftable[index as usize] = bm_f;
            index += 1;
        }
        bm.elem_index_dirty &= !BM_FACE;
    }

}
