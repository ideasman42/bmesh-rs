// Licensed: GPL v2+

// when switching select mode, makes sure selection is consistent for editing
// also for paranoia checks to make sure edge or face mode works
//
// was EDBM_selectmode_set (no shared code, just same purpose)

use bmesh::prelude::*;

pub fn bm_mesh_select_mode_set(
    bm: &mut BMesh,
    mode_old: BMElemType,
    mode_new: BMElemType,
) {
    if mode_old == mode_new {
        return;
    }

    // flag checks read a bit verbose, use macro
    macro_rules! is_mode_add {
        ($mode:ident) => { (mode_old & $mode) == 0 && (mode_new & $mode) != 0 }
    }
    macro_rules! is_mode_sub {
        ($mode:ident) => { (mode_old & $mode) != 0 && (mode_new & $mode) == 0 }
    }

    {
        let mut flush = bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
            .skip(BM_ELEM_HIDDEN);

        if is_mode_add!(BM_VERT) {
        // flush down
            flush = flush.set_edge_from_vert_all();
            flush = flush.set_face_from_vert_all();
        } else if is_mode_add!(BM_EDGE) {
            flush = flush.set_face_from_edge_all();
        }

        // flush up
        if is_mode_sub!(BM_VERT) {
            if (mode_new & BM_EDGE) != 0 {
                flush = flush.set_vert_from_edge_any();
            } else if (mode_new & BM_FACE) != 0 {
                flush = flush.set_vert_from_face_any();
            }
        } else if is_mode_sub!(BM_EDGE) {
            if (mode_new & BM_FACE) != 0 {
                flush = flush.set_edge_from_face_any();
            }
        }

        flush.exec();
    }

    bm.select_mode = mode_new;
}
