// Licensed: GPL v2+

use list_base::{
    ListBase,
    ListBaseElemUtils,
    ListBaseIterMut,
    ListBaseIterConst,
};
use mempool::{
    MemPool,
    MemPoolElemUtils,
};

pub trait MainDataSeqTrait:
    MemPoolElemUtils +
    ListBaseElemUtils +
    IDTypeImpl +
    Default
    {}


pub struct MainDataSeq<T>
    where
    T: MainDataSeqTrait,
{
    pool: MemPool<T>,
    // We could expose this, however users shouldn't need to perform direct operations on the list
    // so for now just wrap basic functions.
    list: ListBase<T>,
    // we may want to have a BTreeMap of names too for quick lookups.
}

impl<T: MainDataSeqTrait> MainDataSeq<T> {
    pub unsafe fn alloc_uninitialized(&mut self) -> PtrMut<T> {
        let elem = PtrMut(self.pool.alloc_elem_uninitialized());
        // self.list.push_back(PtrMut(elem));
        return elem;
    }
    // XXX, this function is weak! but serves a purpose for file-reading.
    // List consistency is maintained as long as allocations match list order.
    pub unsafe fn alloc_uninitialized_list_update(&mut self) -> PtrMut<T> {
        let elem = self.alloc_uninitialized();
        {
            use ::plain_ptr::{ null_mut };
            if self.list.head == null_mut() {
                self.list.head = elem;
            }
            self.list.tail = elem;
        }
        return elem;
    }
    pub fn add_default(&mut self) -> PtrMut<T> {
        let elem = PtrMut(self.pool.alloc_elem_from(Default::default()));
        self.list.push_back(elem);
        return elem;
    }
    pub fn add_default_named(&mut self, name: Option<&str>) -> PtrMut<T> {
        let mut elem = self.add_default();
        elem.as_id_mut().name_set(name.unwrap_or(T::default_name()));
        return elem;
    }
    pub fn remove(&mut self, elem: PtrMut<T>) {
        self.list.remove(elem);
        unsafe { ::std::ptr::drop_in_place(elem.as_ptr()) };
        self.pool.free_elem(elem.as_ptr());
    }
    pub fn iter(&self) -> ListBaseIterConst<T> {
        self.list.iter()
    }
    pub fn iter_mut(&mut self) -> ListBaseIterMut<T> {
        self.list.iter_mut()
    }

    // ideally we wouldn't need these,
    // but they're required for accessing elements while decoding.
    pub fn head_mut(&mut self) -> PtrMut<T> {
        self.list.head
    }
    pub fn tail_mut(&mut self) -> PtrMut<T> {
        self.list.tail
    }
}

use super::super::types::{
    Library,
    Scene,
    Object,
    Mesh,

    // impl
    IDTypeImpl,
};

use plain_ptr::{
    PtrMut,
};

// Fairly arbitrary, in general elements are small

macro_rules! id_generic_impl {
    ($($t:tt)*) => ($(
        impl MainDataSeqTrait for $t {}

        impl MemPoolElemUtils for $t {
            #[inline]
            fn default_chunk_size() -> usize {
                return ::core::dna::consts::MEMPOOL_CHUNK_SIZE;
            }
            #[inline]
            fn free_ptr_get(&self) -> *mut Self {
                return self.id.lib.as_ptr() as usize as *mut Self;
            }
            #[inline]
            fn free_ptr_set(&mut self, ptr: *mut Self) {
                self.id.lib = ::plain_ptr::PtrMut(ptr as usize as *mut Library);
                self.id.ty = [0; 2];
            }
            #[inline]
            fn free_ptr_test(&self) -> bool {
                return self.id.ty == [0; 2];
            }
        }

        impl ListBaseElemUtils for $t {
            #[inline]
            fn next_get(&self) -> PtrMut<Self> { PtrMut(self.id.next.as_ptr() as *mut Self) }
            #[inline]
            fn prev_get(&self) -> PtrMut<Self> { PtrMut(self.id.prev.as_ptr() as *mut Self) }

            #[inline]
            fn next_set(&mut self, mut ptr: PtrMut<Self>) { self.id.next = PtrMut(&mut ptr.id); }
            #[inline]
            fn prev_set(&mut self, mut ptr: PtrMut<Self>) { self.id.prev = PtrMut(&mut ptr.id); }
        }

        impl Default for MainDataSeq<$t> {
            fn default() -> Self {
                MainDataSeq {
                    pool: MemPool::new(),
                    list: ListBase::new(),
                }
            }
        }

    )*)
}

id_generic_impl! { Library Scene Object Mesh }
