// Licensed: GPL v2+

///
/// This module defines 'MainDataMemPoolStore'
/// which uses a macro to define memory pools for each DNA type that requires it,
/// typically these are non-ID types, which are members of an ID's ListBase.
/// Many other DNA structs stored in Vec's so they dont need a mempool (verts/edges for eg)
/// so there is no need to use a memory pool for them.
///
/// The macro avoids having to manually list out every memory pool,
/// instead thin inline wrapper functions are used that give direct access to the pool
/// without the caller having to memorize an associated name for each types memory pool.
///

use ::plain_ptr::{
    PtrMut,
};
use ::mempool::{
    MemPool,
    MemPoolElemUtils,
};

use super::super::types::*;

pub trait MemPoolStoreImpl<T: MemPoolElemUtils> {
    fn mempool_generic(pool: &mut MainDataMemPoolStore) -> &mut MemPool<T>;
}

macro_rules! main_data_mem_pool_declare {
    ($($t:tt) *) => {
        #[derive(Default)]
        #[allow(non_snake_case)]
        pub struct MainDataMemPoolStore {
            $(pub $t: MemPool<$t>),*
        }

        $(impl MemPoolStoreImpl<$t> for $t {
            #[inline]
            fn mempool_generic(pool: &mut MainDataMemPoolStore) -> &mut MemPool<$t> {
                &mut pool.$t
            }
        })*
    }
}

impl MainDataMemPoolStore {
    pub unsafe fn alloc_uninitialized<T>(&mut self) -> PtrMut<T>
        where T: MemPoolStoreImpl<T> + MemPoolElemUtils,
    {
        let pool = T::mempool_generic(self);
        return PtrMut(pool.alloc_elem_uninitialized());
    }
    pub fn alloc_from<T>(&mut self, elem_from: T) -> PtrMut<T>
        where T: MemPoolStoreImpl<T> + MemPoolElemUtils,
    {
        let pool = T::mempool_generic(self);
        return PtrMut(pool.alloc_elem_from(elem_from));
    }
    pub fn free<T>(&mut self, elem_free: PtrMut<T>)
        where T: MemPoolStoreImpl<T> + MemPoolElemUtils,
    {
        let pool = T::mempool_generic(self);
        unsafe { ::std::ptr::drop_in_place(elem_free.as_ptr()) };
        pool.free_elem(elem_free.as_ptr());
    }
}


macro_rules! main_data_mem_pool_from_type {
    ($($t:tt) *) => {
        impl MainDataMemPoolStore {
            pub fn alloc_slice_from_type_index(&mut self, type_index: usize) -> Option<&mut [u8]> {
                use ::std::slice::from_raw_parts_mut;
                use ::core::dna::io::DNATypeIndexImpl;

                // XXX, not optimal having series of checks
                // TODO, use direct lookup table.
                $(
                    if $t::dna_type_index() == type_index {
                        unsafe {
                            let id = self.alloc_uninitialized::<$t>();
                            let id_ptr = id.as_ptr() as *mut u8;
                            return Some(from_raw_parts_mut(id_ptr, size_of_ref_type!(&*id)))
                        }
                    }
                ) *

                return None;
            }
        }
    }
}


#[macro_export]
macro_rules! dna_types_apply_mempool_store {
($macro_id:ident) => (
$macro_id! {
ObjectInst
ObjectLayer
})}

dna_types_apply_mempool_store!(main_data_mem_pool_declare);

dna_types_apply_mempool_store!(main_data_mem_pool_from_type);

