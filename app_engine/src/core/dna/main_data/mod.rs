// Licensed: GPL v2+

mod id_seq;
mod mempool_store;

pub use self::id_seq::{
    MainDataSeq,
    MainDataSeqTrait,
};

use super::types::{
    Scene,
    Object,
    Mesh,
};

pub use self::mempool_store::{
    MainDataMemPoolStore,
};

#[derive(Default)]
pub struct MainData {
    pub scene: MainDataSeq<Scene>,
    pub object: MainDataSeq<Object>,
    pub mesh: MainDataSeq<Mesh>,

    // Allocate all non-ID and Vector data.
    // currently all stored in ListBase.
    pub elem: MainDataMemPoolStore,
}

#[macro_export]
macro_rules! dna_main_data_seq_apply {
($macro_id:ident) => (
$macro_id! {
scene
object
mesh
})}
