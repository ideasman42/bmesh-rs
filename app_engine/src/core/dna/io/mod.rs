// Licensed: GPL v2+

///
/// Module to manage reading/writing DNA
/// (serializing to/from memory, file-system... etc).
///
/// Will be used for native file format and likely undo-storage.
///
/// Effectively a memory dump,
/// with special handling of `Vec`, `ListBase`... `String` ? ...etc.
/// and other types.
///
/// TODO: more detailed info!
///

//
// Working notes:
//
// #How to handle packed/non-packed structs?
//
// We could have 2x modes:
//
// * Where no padding is written
//   (requires slow reconstruction for both read and write,
//   will hurt performance especially for large arrays).
//
// * Where padding is included
//   (so read/write can simply 'memcpy',
//   this is what Blender does, extra space used is probably not significant
//   and this will be needed for undo for example.
//

/// Terminology
///
/// * `DNAType~` any type which can be stored in DNA,
///   typically used for implementations.
/// * `DNAStruct~` information about a `DNAType`.

pub mod binary_util;

pub mod main_data;

pub mod type_index_impl;

const SIZE_UNSET: usize = ::std::usize::MAX;


// -----------------------------------------------------------------------------
// Traits
//
// For types stored in 'dna_info.structs'.

/// Trait that declares a DNA Struct can be serialized.
pub trait DNATypeStructMembersImpl {
    fn members() -> Vec<DNAStructMember>;
}

/// Struct's index within dna_info.structs
pub trait DNATypeIndexImpl {
    fn dna_type_index() -> usize;
}


// -----------------------------------------------------------------------------
// Structs

// Kinds of struct members supported
#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(u8)]
pub enum DNASpec {
    Unknown = 0,
    /// Plain-old-data types: (int, float, bool etc...).
    Pod = 1,
    /// Pointer, can clone but needs special handling to remap on reading.
    Pointer = 2,
    /// Can clone data, either as is (when 'is_complex' = false),
    /// or per-member taking into account different offsets
    /// when serialized data doesn't align with run-time.
    Struct = 3,
    /// In fact just a pointer pair,
    /// use its own spec so we can handle reading/writing differently.
    List = 4,
}

/// Box... others? - add as needed.
#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(u8)]
pub enum DNAStore {
    Unknown = 0,
    /// Data is stored in the struct
    Inline = 1,
    /// Data is stored in a Vector
    Vector = 2,
}

#[derive(Debug)]
pub struct DNAStructMemberSizeData {
    /// note that size includes 'array_flat_len'
    pub size: usize,
    pub offset: usize,
}


#[derive(Debug)]
pub struct DNAStructMember {
    /// name as used in the struct
    pub dna_name: String,
    /// Same as: DNAStruct.type_index
    pub type_index: usize,

    pub type_spec: DNASpec,
    pub type_store: DNAStore,

    /// This is flattened for multi-dimensional arrays
    pub array_flat_len: usize,

    /// Serialized sizes (on-disk)
    /// sizeof(..), without any run-time structs.
    /// 'size = array_elem_size * array_flat_len'
    pub serial: DNAStructMemberSizeData,
    /// Runtime vars
    pub runtime: DNAStructMemberSizeData,
}

impl Default for DNAStructMember {
    #[inline]
    fn default() -> Self {
        DNAStructMember {
            dna_name: "".to_string(),
            type_index: 0,
            type_spec: DNASpec::Unknown,
            type_store: DNAStore::Unknown,
            array_flat_len: 1,
            serial: DNAStructMemberSizeData {
                size: SIZE_UNSET,
                offset: 0,
            },
            runtime: DNAStructMemberSizeData {
                size: SIZE_UNSET,
                offset: 0,
            },
        }
    }
}

impl DNAStructMember {
    /// Size of a single element in the struct,
    /// should _not_ include 'array_flat_len',
    /// this will be multiplied by array or vec.
    pub fn size_of_elem_serial(&self, structs: &Vec<DNAStruct>) -> Option<usize> {
        match self.type_store {
            DNAStore::Inline => {
                match self.type_spec {
                    DNASpec::Pod => {
                        let sub_index = self.type_index;
                        return Some(structs[sub_index].serial_size);
                    },
                    DNASpec::Struct => {
                        let sub_index = self.type_index;
                        if structs[sub_index].serial_size == SIZE_UNSET {
                            // caller may calculate size and re-run.
                            return None;
                        }
                        return Some(structs[sub_index].serial_size);
                    },
                    DNASpec::Pointer => {
                        // write memory locations as UID's,
                        // each type handles exact mapping differently.
                        return Some(size_of_ptr!());
                    },
                    DNASpec::List => {
                        return Some(size_of_ptr!() * 2);
                    },
                    DNASpec::Unknown => {
                        unreachable!();
                    },
                }
            },
            DNAStore::Vector => {
                // All storage that isn't inline uses pointers and look-ups.
                // with only their memory address written.
                return Some(size_of_ptr!());
            },
            DNAStore::Unknown => {
                unreachable!()
            },
        }
    }

    pub fn runtime_range(&self) -> ::std::ops::Range<usize> {
        self.runtime.offset..(self.runtime.offset + self.runtime.size)
    }
    pub fn serial_range(&self) -> ::std::ops::Range<usize> {
        self.serial.offset..(self.serial.offset + self.serial.size)
    }

}

#[derive(Debug)]
pub struct DNAStruct {
    pub dna_name: String,
    /// Index as written into DNA-info header
    /// order may change over time (that's fine).
    /// use for both struct's and POD's which are effectively a struct of one.
    pub type_index: usize,

    /// Final size (when serialized)
    /// size of all 'members' combined.
    ///
    /// Note that this excludes real-time data
    /// (called 'rt' by convention and kept as the last struct)
    pub serial_size: usize,

    /// Runtime size
    pub runtime_size: usize,

    pub flag: DNAStructFlag,

    /// Members (in struct order for writing).
    /// when empty this is a POD (plain old data type), int/bool/float etc.
    pub members: Vec<DNAStructMember>,
}

struct_bitflag_impl!(pub struct DNAStructFlag(pub u32));
impl DNAStructFlag {
    /// True when we can't simply write the memory to disk;
    /// (Contains `Vec` and `ListBase` for eg, that needs special handling).
    ///
    /// Note: For most practical purposes this is the opposite of a struct being 'packed',
    /// in that a non-complex struct never has padding between its members.
    /// However there are subtle differences, since trailing padding bits are allowed
    /// for non-complex structs, and we may allow pad-bits by serializing them
    /// with dummy 'DNAStructMember' added to avoid slow struct reconstruction.
    pub const IS_COMPLEX: Self = DNAStructFlag(1 << 0);

    /// Are any 'Inline' structs complex, recursively.
    /// Even if this struct isn't complex, it may use a struct that is,
    /// meaning that we can't simply clone its data.
    ///
    /// Note: In nearly all cases a nested inline struct which is complex
    /// will also be a different size, making this struct complex too.
    /// However there may be some rare times when this isn't the case.
    /// So best not to assume complex structs will automatically
    /// cause their users to be complex too.
    pub const IS_COMPLEX_RECURSE: Self = DNAStructFlag(1 << 1);
}

struct_bitflag_flag_fn_impl!(
    DNAStructFlag, DNAStructFlag::IS_COMPLEX,
    test=complex_test,
    enable=complex_enable, disable=complex_disable,
    set=complex_set, toggle=complex_toggle);

struct_bitflag_flag_fn_impl!(
    DNAStructFlag, DNAStructFlag::IS_COMPLEX_RECURSE,
    test=complex_recurse_test,
    enable=complex_recurse_enable, disable=complex_recurse_disable,
    set=complex_recurse_set, toggle=complex_recurse_toggle);


#[derive(Debug)]
pub struct DNAInfo {
    structs: Vec<DNAStruct>,
}

mod dna_info {
    use super::{
        DNAInfo,
        DNAStruct,
        DNAStructFlag,
        DNAStore,
        DNASpec,
        DNATypeStructMembersImpl,
        SIZE_UNSET,

    };

    use ::core::dna::types::struct_list::{
        DNA_TYPE_POD_NUM,
        DNA_TYPE_TOTAL_NUM,
    };

    fn calc_size_recursive(
        structs: &mut Vec<DNAStruct>,
        index: usize,
    ) {
        println!("calc: {}", structs[index].dna_name);
        let mut size = 0;
        let mut is_complex = false;
        let mut is_complex_recurse = false;
        for i in 0..structs[index].members.len() {
            let sub_index = structs[index].members[i].type_index;
            let elem_size_serial = {
                if let Some(elem_size) = structs[index].members[i].size_of_elem_serial(structs) {
                    elem_size
                } else {
                    calc_size_recursive(structs, sub_index);
                    assert!(structs[sub_index].serial_size != SIZE_UNSET);
                    structs[index].members[i].size_of_elem_serial(structs).unwrap()
                }
            };
            let elem_is_complex_recurse = structs[sub_index].flag.complex_recurse_test();

            {
                let m = &mut structs[index].members[i];
                m.serial.size = elem_size_serial * m.array_flat_len;
                m.serial.offset = size;
                size += m.serial.size;

                if is_complex == false {
                    if (m.serial.size != m.runtime.size) ||
                       (m.serial.offset != m.runtime.offset) ||
                       (m.type_store != DNAStore::Inline)
                    {
                        is_complex = true;
                        is_complex_recurse = true;
                    }
                }
                if is_complex_recurse == false {
                    if m.type_store == DNAStore::Inline {
                        if elem_is_complex_recurse {
                            is_complex_recurse = true;
                        }
                    }
                }
            }
        }

        {
            let s = &mut structs[index];
            s.serial_size = size;
            s.flag.complex_set(is_complex);
            s.flag.complex_recurse_set(is_complex_recurse);
        }
    }

    /// once we have the DNAInfo,
    /// calculate all derived data needed for (de)serializing.
    fn finalize(info: &mut DNAInfo) {

        // TODO: replace index look-ups

        if cfg!(debug_assertions) {
            // Ensure correct order
            for dna_struct in info.structs.iter() {
                for m_pair in dna_struct.members.windows(2) {
                    if m_pair[0].runtime.offset >= m_pair[1].runtime.offset {
                        println!(
                            "DNA out of order '{}' '{}' should be after '{}'",
                            dna_struct.dna_name, m_pair[0].dna_name, m_pair[1].dna_name,
                        );
                        panic!();
                    }
                }
            }
        }

        for i in 0..info.structs.len() {
            if info.structs[i].serial_size == SIZE_UNSET {
                calc_size_recursive(&mut info.structs, i);
            }
        }

        if cfg!(debug_assertions) {
            // Ensure Pod/Struct is used correctly
            for dna_struct in info.structs.iter() {
                for m in dna_struct.members.iter() {
                    if m.type_index >= DNA_TYPE_POD_NUM {
                        if m.type_spec == DNASpec::Pod {
                            println!(
                                "DNA member '{}.{}': found {:?} should be {:?}",
                                dna_struct.dna_name, m.dna_name,
                                DNASpec::Pod, DNASpec::Struct,
                            );
                            panic!();
                        }
                    } else {
                        if m.type_spec == DNASpec::Struct {
                            println!(
                                "DNA member '{}.{}': found {:?} should be {:?}",
                                dna_struct.dna_name, m.dna_name,
                                DNASpec::Struct, DNASpec::Pod,
                            );
                            panic!();
                        }
                    }
                }
            }
        }
        // end debug check

    }

    pub fn create() -> DNAInfo {
        let mut info = DNAInfo {
            structs: Vec::with_capacity(DNA_TYPE_TOTAL_NUM),
        };

        if false {
            macro_rules! print_structs {
                ($($t:tt)*) => ($(
                    println!("type: {:?}", stringify!($t));
                )*)
            }
            dna_types_apply_struct!(print_structs);
        }

        {
            // POD structs (written as structs, but in fact native types)
            macro_rules! def_dna_primitive {
                ($($t:tt)*) => ($(
                    debug_assert_eq!(info.structs.len(), $t::dna_type_index());

                    let runtime_size: usize = ::std::mem::size_of::<$t>();

                    let d = DNAStruct {
                        dna_name: stringify!($t).to_string(),
                        type_index: info.structs.len(),
                        // sizes match for POD
                        serial_size: runtime_size,
                        runtime_size: runtime_size,
                        // set flags later
                        flag: DNAStructFlag(0),
                        members: Vec::new(),
                    };
                    info.structs.push(d);
                )*)
            }

            // Complex DNA structs
            macro_rules! def_dna_structs {
                ($($t:tt)*) => ($(
                    debug_assert_eq!(info.structs.len(), $t::dna_type_index());

                    let runtime_size: usize = ::std::mem::size_of::<$t>();

                    let members = $t::members();

                    let d = DNAStruct {
                        dna_name: stringify!($t).to_string(),
                        type_index: info.structs.len(),
                        members: members,
                        serial_size: SIZE_UNSET,
                        runtime_size: runtime_size,
                        // set flags later
                        flag: DNAStructFlag(0),
                    };
                    info.structs.push(d);
                )*)
            }

            {
                use super::DNATypeIndexImpl;
                use ::core::dna::types::*;
                dna_types_apply_pod!(def_dna_primitive);
                dna_types_apply_struct!(def_dna_structs);
            }
        }

        finalize(&mut info);

        println!("{:#?}", info.structs);

        assert_eq!(info.structs.len(), DNA_TYPE_TOTAL_NUM);

        return info;
    }
}

pub fn dna_info_create() -> DNAInfo {
    return dna_info::create();
}
