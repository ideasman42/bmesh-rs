// Licensed: GPL v2+

/// Support for `dna_type_index` method
/// for any type that can be stored in DNA.

use ::core::dna::types::*;
use super::{
    DNATypeIndexImpl,
};

use ::core::dna::types::struct_list::{
    DNA_TYPE_POD_NUM,
    DNA_TYPE_STRUCT_NUM,
};

macro_rules! count_tts {
    () => {0_usize};
    ($_head:tt $($tail:tt)*) => {1_usize + count_tts!($($tail)*)};
}

macro_rules! dna_type_index_pod_impl {
    () => {};
    ($head:tt $($tail:tt)*) => {
        impl DNATypeIndexImpl for $head {
            #[inline]
            fn dna_type_index() -> usize {
                const INDEX: usize = (
                    DNA_TYPE_POD_NUM - (1 + count_tts!($($tail)*))
                );
                return INDEX;
            }
        }
        dna_type_index_pod_impl!($($tail)*);
    };
}

macro_rules! dna_type_index_struct_impl {
    () => {};
    ($head:tt $($tail:tt)*) => {
        impl DNATypeIndexImpl for $head {
            #[inline]
            fn dna_type_index() -> usize {
                const INDEX: usize = (
                    (DNA_TYPE_POD_NUM + DNA_TYPE_STRUCT_NUM) -
                    (1_usize + count_tts!($($tail)*))
                );
                return INDEX;
            }
        }
        dna_type_index_struct_impl!($($tail)*);
    };
}

dna_types_apply_pod!(dna_type_index_pod_impl);
dna_types_apply_struct!(dna_type_index_struct_impl);

