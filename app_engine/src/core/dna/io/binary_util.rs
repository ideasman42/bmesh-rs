// Licensed: GPL v2+

///
/// Binary functions for encoding/decoding,
/// while they could be moved into a generic crate,
/// they are quite low level and rather not promote wide usage.
/// Use with care!
///

use ::std::{
    slice,
    mem,
};

#[inline]
pub fn slice_u8_from_any<T: Sized>(p: &T) -> &[u8] {
    unsafe {
        slice::from_raw_parts((p as *const T) as *const u8, mem::size_of::<T>())
    }
}
#[inline]
pub fn slice_u8_from_any_mut<T: Sized>(p: &mut T) -> &mut [u8] {
    unsafe {
        slice::from_raw_parts_mut((p as *mut T) as *mut u8, mem::size_of::<T>())
    }
}


#[inline]
pub fn slice_u8_to_any<T: Sized>(data: &[u8]) -> &T {
    debug_assert_eq!(data.len(), mem::size_of::<T>());
    unsafe { &*(data.as_ptr() as *const T) }
}
#[inline]
pub fn slice_u8_to_any_mut<T: Sized>(data: &mut [u8]) -> &mut T {
    debug_assert_eq!(data.len(), mem::size_of::<T>());
    unsafe { &mut *(data.as_ptr() as *mut T) }
}

/// Transmute slices with correct size assurance
#[inline]
pub fn slice_u8_from_slice_any<T: Sized>(s: &[T]) -> &[u8] {
    unsafe {
        slice::from_raw_parts(
            (s.as_ptr()) as *const u8,
            mem::size_of::<T>() * s.len(),
        )
    }
}
#[inline]
pub fn slice_u8_from_slice_any_mut<T: Sized>(s: &mut [T]) -> &mut [u8] {
    unsafe {
        slice::from_raw_parts_mut(
            (s.as_ptr()) as *mut u8,
            mem::size_of::<T>() * s.len(),
        )
    }
}

/// Transmutes a vector into a `Vec<u8>`
/// resizing the length so all content is preserved.
#[inline]
pub fn vec_u8_transmute_from_vec_any<T: Sized>(mut data: Vec<T>) -> Vec<u8> {
    unsafe {
        let data_len_u8 = data.len() * mem::size_of::<T>();
        data.set_len(data_len_u8);
        mem::transmute::<Vec<T>, Vec<u8>>(data)
    }
}


#[inline]
pub fn vec_from_u8_slice<T>(data: &[u8]) -> &Vec<T> {
    debug_assert_eq!(mem::size_of::<Vec<T>>(), data.len());
    unsafe {
        &*((data.as_ptr() as usize) as *const Vec<T>)
    }
}
#[inline]
pub fn vec_from_u8_slice_mut<T>(data: &mut [u8]) -> &mut Vec<T> {
    debug_assert_eq!(mem::size_of::<Vec<T>>(), data.len());
    unsafe {
        &mut *((data.as_ptr() as usize) as *mut Vec<T>)
    }
}


/// Takes memory which contains a vector,
/// and returns a slice of the data _in_ the vector.
#[inline]
pub fn slice_of_vec_from_u8_slice(data: &[u8], elem_size: usize) -> (&[u8], usize) {
    debug_assert_eq!(mem::size_of::<Vec<u8>>(), data.len());
    let data_vec = unsafe {
        &*((data.as_ptr() as usize) as *const Vec<u8>)
    };
    let len = data_vec.len();
    unsafe {
        (slice::from_raw_parts(data_vec.as_ptr(), elem_size * len), len)
    }
}
#[inline]
pub fn slice_of_vec_from_u8_slice_mut(data: &mut [u8], elem_size: usize) -> (&mut [u8], usize) {
    debug_assert_eq!(mem::size_of::<Vec<u8>>(), data.len());
    let data_vec = unsafe {
        &mut *((data.as_mut_ptr() as usize) as *mut Vec<u8>)
    };
    let len = data_vec.len();
    unsafe {
        (slice::from_raw_parts_mut(data_vec.as_mut_ptr(), elem_size * len), len)
    }
}
