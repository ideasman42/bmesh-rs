// Licensed: GPL v2+

use ::std::ops::Range;

use ::core::dna::io::{
    DNAInfo,
    DNAStruct,
    DNAStructMember,
    DNAStructMemberSizeData,
    DNAStructFlag,
};

use ::core::dna::io::main_data::sdna::{
    SDNAHead,

    sdna_code,
};

use super::super::super::binary_util;

use std::{
    mem,
    io,
};

struct SDNACodeRange {
    code: u32,
    range: Range<usize>,
}

macro_rules! swap_byte_iter {
    ($slice:expr) => {
        for i in $slice {
            *i = i.swap_bytes();
        }
    }
}

pub mod decode_impl {
    use std::{
        mem,
        io,
    };
    use super::super::super::super::binary_util;

    fn local_memchr(data: &[u8], index: usize, byte: u8) -> Option<usize> {
        for i in index..data.len() {
            if unsafe { *data.get_unchecked(i) == byte } {
                return Some(i);
            }
        }
        return None;
    }

    fn decode_strings(data: &[u8], capacity: usize) -> Result<Vec<String>, io::Error> {
        // Read null terminated strings
        let mut ret: Vec<String> = Vec::with_capacity(capacity);
        let mut i_offset: usize = 0;
        while i_offset != data.len() {
            let i_span = local_memchr(data, i_offset, 0_u8).unwrap();
            if let Ok(id) = ::std::str::from_utf8(&data[i_offset..i_span]) {
                ret.push(String::from(id));
            } else {
                return Err(io::Error::new(io::ErrorKind::InvalidInput, "Invalid string"));
            }
            i_offset = i_span + 1;
        }
        Ok(ret)
    }

    fn decode_u32(data: &[u8], data_len: usize, endian_swap: bool) -> Vec<u32> {
        debug_assert_eq!(data_len * mem::size_of::<u32>(), data.len());
        let mut ret: Vec<u32> = Vec::with_capacity(data_len);
        for c in data.chunks(mem::size_of::<u32>()) {
            ret.push(*binary_util::slice_u8_to_any(c));
        }
        if endian_swap {
            swap_byte_iter!(&mut ret);
        }
        return ret;
    }

    fn decode_u8(data: &[u8], data_len: usize) -> Vec<u8> {
        debug_assert_eq!(data_len, data.len());
        let mut ret: Vec<u8> = Vec::with_capacity(data_len);
        ret.extend(data);
        return ret;
    }

    /// SIZE
    pub fn size(
        data: &[u8], _struct_len: usize, _member_len: usize, endian_swap: bool,
    ) -> Result<[u32; 2], io::Error> {
        let mut ret: [u32; 2] = *binary_util::slice_u8_to_any(data);
        if endian_swap {
            swap_byte_iter!(&mut ret);
        }
        Ok(ret)
    }
    /// STID
    pub fn struct_id(
        data: &[u8], struct_len: usize, _member_len: usize, _endian_swap: bool,
    ) -> Result<Vec<String>, io::Error> {
        Ok(decode_strings(data, struct_len)?)
    }
    /// STSZ
    pub fn struct_size(
        data: &[u8], struct_len: usize, _member_len: usize, endian_swap: bool,
    ) -> Result<Vec<u32>, io::Error> {
        Ok(decode_u32(data, struct_len, endian_swap))
    }
    /// STMB
    pub fn struct_member_len(
        data: &[u8], struct_len: usize, _member_len: usize, endian_swap: bool,
    ) -> Result<Vec<u32>, io::Error> {
        Ok(decode_u32(data, struct_len, endian_swap))
    }
    /// MBID
    pub fn member_id(
        data: &[u8], _struct_len: usize, member_len: usize, _endian_swap: bool,
    ) -> Result<Vec<String>, io::Error> {
        Ok(decode_strings(data, member_len)?)
    }
    /// MBTY
    pub fn member_type(
        data: &[u8], _struct_len: usize, member_len: usize, endian_swap: bool,
    ) -> Result<Vec<u32>, io::Error> {
        Ok(decode_u32(data, member_len, endian_swap))
    }
    /// MBSZ
    pub fn member_size(
        data: &[u8], _struct_len: usize, member_len: usize, endian_swap: bool,
    ) -> Result<Vec<u32>, io::Error> {
        Ok(decode_u32(data, member_len, endian_swap))
    }
    /// MBOF
    pub fn member_offset(
        data: &[u8], _struct_len: usize, member_len: usize, endian_swap: bool,
    ) -> Result<Vec<u32>, io::Error> {
        Ok(decode_u32(data, member_len, endian_swap))
    }
    /// MBAR
    pub fn member_array(
        data: &[u8], _struct_len: usize, member_len: usize, endian_swap: bool,
    ) -> Result<Vec<u32>, io::Error> {
        Ok(decode_u32(data, member_len, endian_swap))
    }
    /// MBST
    pub fn member_store(
        data: &[u8], _struct_len: usize, member_len: usize, _endian_swap: bool,
    ) -> Result<Vec<u8>, io::Error> {
        Ok(decode_u8(data, member_len))
    }
    /// MBSP
    pub fn member_spec(
        data: &[u8], _struct_len: usize, member_len: usize, _endian_swap: bool,
    ) -> Result<Vec<u8>, io::Error> {
        Ok(decode_u8(data, member_len))
    }
}

fn decode_sdna_blocks(sdna_data: &[u8], endian_swap: bool) -> Vec<SDNACodeRange> {
    let mut sdna_range: Vec<SDNACodeRange> = Vec::new();
    let mut i: usize = 0;
    while i < sdna_data.len() {
        let i_next = i + mem::size_of::<SDNAHead>();
        let shead: &SDNAHead = binary_util::slice_u8_to_any(&sdna_data[i..i_next]);
        sdna_range.push(
            SDNACodeRange {
                code: if endian_swap { shead.code.swap_bytes() } else { shead.code },
                range: i_next..(i_next + shead.size as usize),
            }
        );
        i = i_next + shead.size as usize;
    }
    return sdna_range;
}

fn slice_from_code<'a>(
    sdna_range: &Vec<SDNACodeRange>,
    sdna_data: &'a [u8],
    code: u32,
) -> Option<&'a [u8]> {
    for s in sdna_range {
        if s.code == code {
            return Some(&sdna_data[s.range.clone()]);
        }
    }
    return None;
}

fn slice_from_code_ok<'a>(
    sdna_range: &Vec<SDNACodeRange>,
    sdna_data: &'a [u8],
    code: u32,
) -> Result<&'a [u8], io::Error> {
    if let Some(ret) = slice_from_code(sdna_range, sdna_data, code) {
        Ok(ret)
    } else {
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            format!("missing DNA block '{}'",
                    String::from_utf8_lossy(binary_util::slice_u8_from_any(&code))
            ),

        ))
    }
}

pub fn decode_sdna(
    sdna_data: &[u8],
    endian_swap: bool,
) -> Result<DNAInfo, io::Error> {

    let sdna_range = decode_sdna_blocks(sdna_data, endian_swap);

    macro_rules! sdna_head {
        ($sdna_code:expr, $size:expr, $fn_id:expr) => {
            $fn_id(
                slice_from_code_ok(&sdna_range, sdna_data, $sdna_code)?,
                $size[0], // struct_len
                $size[1], // member_len
                endian_swap,
            )?
        }
    }
    let size = sdna_head!(sdna_code::SIZE, &[0, 0], decode_impl::size);
    let size = [size[0] as usize, size[1] as usize];
    let stid = sdna_head!(sdna_code::STID, size, decode_impl::struct_id);
    let stsz = sdna_head!(sdna_code::STSZ, size, decode_impl::struct_size);
    let stmb = sdna_head!(sdna_code::STMB, size, decode_impl::struct_member_len);
    let mbid = sdna_head!(sdna_code::MBID, size, decode_impl::member_id);
    let mbty = sdna_head!(sdna_code::MBTY, size, decode_impl::member_type);
    let mbsz = sdna_head!(sdna_code::MBSZ, size, decode_impl::member_size);
    let mbof = sdna_head!(sdna_code::MBOF, size, decode_impl::member_offset);
    let mbar = sdna_head!(sdna_code::MBAR, size, decode_impl::member_array);
    let mbst = sdna_head!(sdna_code::MBST, size, decode_impl::member_store);
    let mbsp = sdna_head!(sdna_code::MBSP, size, decode_impl::member_spec);

    // TODO... struct len

    let struct_len = size[0];
    // let member_len = size[1] as usize;

    let mut dna_info = DNAInfo {
        structs: Vec::with_capacity(struct_len),
    };

    {
        const MAX: usize = ::std::usize::MAX;
        let mut i_member = 0;
        for i_struct in 0..struct_len {
            dna_info.structs.push(
                DNAStruct {
                    dna_name: stid[i_struct].clone(), // TODO, iter() and consume
                    type_index: i_struct,
                    serial_size: stsz[i_struct] as usize,
                    runtime_size: ::std::usize::MAX, // ensure we don't use!
                    flag: DNAStructFlag(0),
                    members: {
                        let member_len = stmb[i_struct] as usize;
                        let mut members = Vec::with_capacity(member_len);
                        for _ in 0..member_len {
                            members.push(
                                DNAStructMember {
                                    dna_name: mbid[i_member].clone(), // TODO, iter() and consume
                                    type_index: mbty[i_member] as usize,
                                    type_spec: unsafe { mem::transmute(mbsp[i_member]) },
                                    type_store: unsafe { mem::transmute(mbst[i_member]) },
                                    array_flat_len: mbar[i_member] as usize,
                                    serial: DNAStructMemberSizeData {
                                        size: mbsz[i_member] as usize,
                                        offset: mbof[i_member] as usize,
                                    },
                                    // never use!
                                    runtime: DNAStructMemberSizeData {
                                        size: MAX,
                                        offset: MAX,
                                    },
                                }
                            );
                            i_member += 1;
                        }
                        members
                    },
                }
            );
        }
    }
    println!("DECODED\n{:#?}", dna_info.structs);

    return Ok(dna_info);
}
