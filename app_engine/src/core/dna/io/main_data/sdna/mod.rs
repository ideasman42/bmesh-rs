// Licensed: GPL v2+

///
/// Mini format for Struct DNA info
///
/// BHead ID: SDNA
///
/// Storing DNA information in this manner
/// allows to add new attributes without changing the format.
///


pub mod encode_sdna;
pub mod decode_sdna;

#[repr(C, packed)]
pub struct SDNAHead {
    /// code
    code: u32,
    /// Length in bytes
    size: u32,
}

pub mod sdna_code {

    /// Struct and Member Sizes: [u32; 2]
    ///
    /// Note that this could be calculated from STID and MBID,
    /// but simplifies decoding.
    pub const SIZE: u32 = u32_code!(b"SIZE");

    /// Struct ID: u32, null-terminated strings
    pub const STID: u32 = u32_code!(b"STID");
    /// Struct Sizes:  [u32, ...]
    pub const STSZ: u32 = u32_code!(b"STSZ");
    /// Struct Member Lengths:  [u32, ...]
    pub const STMB: u32 = u32_code!(b"STMB");

    /// Member ID: u32, null terminated strings
    pub const MBID: u32 = u32_code!(b"MBID");
    /// Member Type:  [u32, ...]
    pub const MBTY: u32 = u32_code!(b"MBTY");
    /// Member Size: [u32, ...]
    pub const MBSZ: u32 = u32_code!(b"MBSZ");
    /// Member Offset: [u32, ...]
    pub const MBOF: u32 = u32_code!(b"MBOF");
    /// Member Flat Array Size: [u32, ...]
    pub const MBAR: u32 = u32_code!(b"MBAR");
    /// Member Storage (DNAStore enum): [u8, ...]
    pub const MBST: u32 = u32_code!(b"MBST");
    /// Member Spec (DNASpec enum): [u8, ...]
    pub const MBSP: u32 = u32_code!(b"MBSP");
}
