// Licensed: GPL v2+

use ::core::dna::io::{
    DNAInfo,
};

use ::core::dna::io::main_data::sdna::{
    SDNAHead,
};

use super::super::super::binary_util;


mod encode_impl {
    use ::core::dna::io::main_data::sdna::{
        SDNAHead,

        sdna_code,
    };
    use ::core::dna::io::{
        DNAInfo,
    };

    use super::super::super::super::binary_util;

    /// See: sdna_code module for docs
    macro_rules! sdna_head {
        ($sdna_code:expr, $data:tt) => {
            {
                let $data = binary_util::vec_u8_transmute_from_vec_any($data);
                (
                    SDNAHead {
                        code: $sdna_code,
                        size: $data.len() as u32,
                    },
                    $data,
                )
            }
        }
    }

    /// SIZE
    pub fn size(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        // ----------------
        // Calculate Length
        let struct_len: usize = dna_info.structs.len();
        // ----------
        // Build data
        let data: Vec<u32> = vec![
            struct_len as u32,
            member_len as u32,
        ];
        debug_assert_eq!(2, data.len());
        sdna_head!(sdna_code::SIZE, data)
    }
    /// STID
    pub fn struct_id(dna_info: &DNAInfo) -> (SDNAHead, Vec<u8>) {
        // ----------------
        // Calculate Length
        let mut data_len: usize = 0;
        for s in &dna_info.structs {
            data_len += s.dna_name.len();
        }
        // needed for null terminators
        data_len += dna_info.structs.len();
        // ----------
        // Build data
        let mut data: Vec<u8> = Vec::with_capacity(data_len);
        for s in &dna_info.structs {
            data.extend_from_slice(s.dna_name.as_bytes());
            data.push(0);
        }
        debug_assert_eq!(data_len, data.len());
        sdna_head!(sdna_code::STID, data)
    }
    /// STSZ
    pub fn struct_size(dna_info: &DNAInfo) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u32> = Vec::with_capacity(dna_info.structs.len());
        for dna_struct in &dna_info.structs {
            data.push(dna_struct.serial_size as u32);
        }
        debug_assert_eq!(dna_info.structs.len(), data.len());
        sdna_head!(sdna_code::STSZ, data)
    }
    /// STMB
    pub fn struct_member_len(dna_info: &DNAInfo) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u32> = Vec::with_capacity(dna_info.structs.len());
        for dna_struct in &dna_info.structs {
            data.push(dna_struct.members.len() as u32);
        }
        debug_assert_eq!(dna_info.structs.len(), data.len());
        sdna_head!(sdna_code::STMB, data)
    }
    /// MBID
    pub fn member_id(dna_info: &DNAInfo) -> (SDNAHead, Vec<u8>) {
        // ----------------
        // Calculate Length
        let mut data_len: usize = 0;
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data_len += member.dna_name.len();
            }
            data_len += dna_struct.members.len();
        }
        // needed for null terminators
        // ----------
        // Build data
        let mut data: Vec<u8> = Vec::with_capacity(data_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.extend_from_slice(member.dna_name.as_bytes());
                data.push(0);
            }
        }
        debug_assert_eq!(data_len, data.len());
        sdna_head!(sdna_code::MBID, data)
    }
    /// MBTY
    pub fn member_type(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u32> = Vec::with_capacity(member_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.push(member.type_index as u32);
            }
        }
        debug_assert_eq!(member_len, data.len());
        sdna_head!(sdna_code::MBTY, data)
    }
    /// MBSZ
    pub fn member_size(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u32> = Vec::with_capacity(member_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.push(member.serial.size as u32);
            }
        }
        debug_assert_eq!(member_len, data.len());
        sdna_head!(sdna_code::MBSZ, data)
    }
    /// MBOF
    pub fn member_offset(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u32> = Vec::with_capacity(member_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.push(member.serial.offset as u32);
            }
        }
        debug_assert_eq!(member_len, data.len());
        sdna_head!(sdna_code::MBOF, data)
    }
    /// MBAR
    pub fn member_array(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u32> = Vec::with_capacity(member_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.push(member.array_flat_len as u32);
            }
        }
        debug_assert_eq!(member_len, data.len());
        sdna_head!(sdna_code::MBAR, data)
    }
    /// MBST
    pub fn member_store(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u8> = Vec::with_capacity(member_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.push(member.type_store as u8);
            }
        }
        debug_assert_eq!(member_len, data.len());
        sdna_head!(sdna_code::MBST, data)
    }
    /// MBSP
    pub fn member_spec(dna_info: &DNAInfo, member_len: usize) -> (SDNAHead, Vec<u8>) {
        let mut data: Vec<u8> = Vec::with_capacity(member_len);
        for dna_struct in &dna_info.structs {
            for member in &dna_struct.members {
                data.push(member.type_spec as u8);
            }
        }
        debug_assert_eq!(member_len, data.len());
        sdna_head!(sdna_code::MBSP, data)
    }
}

pub fn encode_sdna(
    dna_info: &DNAInfo,
) -> Vec<u8> {
    let member_len = {
        let mut member_len: usize = 0;
        for dna_struct in &dna_info.structs {
            member_len += dna_struct.members.len();
        }
        member_len
    };

    let chunks: [(SDNAHead, Vec<u8>); 11] = [
        encode_impl::size(dna_info, member_len),
        encode_impl::struct_id(dna_info),
        encode_impl::struct_size(dna_info),
        encode_impl::struct_member_len(dna_info),
        encode_impl::member_id(dna_info),
        encode_impl::member_type(dna_info, member_len),
        encode_impl::member_size(dna_info, member_len),
        encode_impl::member_offset(dna_info, member_len),
        encode_impl::member_array(dna_info, member_len),
        encode_impl::member_store(dna_info, member_len),
        encode_impl::member_spec(dna_info, member_len),
    ];

    let mut len = 0;
    for c in &chunks {
        len += ::std::mem::size_of::<SDNAHead>();
        len += c.1.len();
    }
    let mut sdna_data: Vec<u8> = Vec::with_capacity(len);
    for c in &chunks {
        sdna_data.extend(binary_util::slice_u8_from_any(&c.0));
        sdna_data.extend(&c.1);
    }
    debug_assert_eq!(len, sdna_data.len());

    return sdna_data;
}
