// Licensed: GPL v2+

///
/// TODO:
/// * Correctly free partially initialized structs on failure.
///
use ::std::collections::HashMap;

use ::core::dna::main_data::{
    MainData,
    MainDataSeq,
    MainDataSeqTrait,
};

use ::core::dna::io::{
    DNAInfo,
    DNASpec,
    DNAStore,
    DNAStruct,
    DNAStructMember,
    DNATypeIndexImpl,
};

use ::core::dna::io::main_data::{
    BHead,
    ListBaseLink,

    bhead_code,
};

use list_base::{
    ListBase,
};

use ::std::io;

use super::super::binary_util;

use plain_ptr::{
    PtrMut,
    null_mut,
};

use ::core::dna::types::{
    ID,
};

use ::core::dna::consts::id_code;


// ----------------------------------------------------------------------------
// Consts

/// Where possible, blit content directly into memory when reading from a file.
/// Disable for debugging may be useful.
const USE_DECODE_BLIT: bool = false;

/// Print information about whats being decoded
const USE_DECODE_VERBOSE: bool = false;

const INDENT: usize = 2;

// ----------------------------------------------------------------------------
// Structs

// ReadHandle TODO

// ----------------------------------------------------------------------------
// Macros

macro_rules! read_exact {
    ($f:expr, $r:expr) => {
        if cfg!(debug_assertions) {
            let result = io::Read::read_exact($f, $r);
            if result.is_err() {
                panic!("read error on {}:{}", file!(), line!());
            }
        } else {
            io::Read::read_exact($f, $r)?
        }
    }
}

// ----------------------------------------------------------------------------
// Wrapper for Read trait
//
// Currently only keeps track of the number of bytes read.
// It's useful for sanity checks.
//
// See: http://stackoverflow.com/a/42189386/432509 (read version)

use self::read_wrapper::ReadWrapper;

mod read_wrapper {
    use std::io::{self, Read};

    pub struct ReadWrapper<R> {
        inner: R,
        count: usize,
    }

    impl<R> ReadWrapper<R>
        where R: Read
    {
        pub fn new(inner: R) -> Self {
            ReadWrapper {
                inner: inner,
                count: 0,
            }
        }

        #[allow(dead_code)]
        #[inline]
        pub fn into_inner(self) -> R {
            self.inner
        }
        #[inline]
        pub fn bytes_read(&self) -> usize {
            self.count
        }
    }

    impl<R> Read for ReadWrapper<R>
        where R: Read
    {
        fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
            let res = self.inner.read(buf);
            if let Ok(size) = res {
                self.count += size
            }
            res
        }
    }
}


// ----------------------------------------------------------------------------
// Decoding Functions
/// Read header, 16 bytes

fn println_dna_member(dna_struct: &DNAStruct, member: &DNAStructMember, indent: usize) {
    println!(
        "{:indent$}member: {}.{}[{}] => [{}..{}] ({} {:?}:{:?})", "",
        dna_struct.dna_name, member.dna_name, member.array_flat_len,
        member.serial.offset, member.serial.offset + member.serial.size,
        member.serial.size,
        member.type_store, member.type_spec,
        indent=indent * INDENT
    );
}

#[derive(Debug)]
struct FileHeader {
    // XXX, use enum
    is_big_endian: bool,
    is_64_bit: bool,

    date_year: u32,
    date_month: u32,
    date_day: u32,
}

fn decode_header<R: io::Read>(
    file: &mut ReadWrapper<R>,
) -> Result<FileHeader, io::Error> {
    let mut buf: [u8; 16] = unsafe { ::std::mem::uninitialized() };

    read_exact!(file, &mut buf);

    if b"WISK3D" != &buf[0..6] {
        return Err(io::Error::new(io::ErrorKind::InvalidInput, "Not a Wisk3D file"));
    }

    let is_big_endian = match buf[6] {
        b'_' => { true },
        b'-' => { false },
        _ => {
            return Err(io::Error::new(io::ErrorKind::InvalidInput, "Wisk3D corrupt header"));
        },
    };
    let is_64_bit = match buf[7] {
        b'v' => { true },
        b'V' => { false },
        _ => {
            return Err(io::Error::new(io::ErrorKind::InvalidInput, "Wisk3D corrupt header"));
        },
    };

    Ok(FileHeader {
        is_big_endian: is_big_endian,
        is_64_bit: is_64_bit,
        date_year: 0,
        date_month: 0,
        date_day: 0,
    })
}

/// Roughly the inverse of 'encode_struct_data'
///
/// * We always have destination pre-allocated.
/// * The file is read into the destination directly for non-complex structs.
/// * File IO may fail at any time, take care!
/// * 'Take' is used, so attempt to read too much will EOF.
fn decode_struct_data<R: io::Read>(
    file: &mut ReadWrapper<R>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &mut [u8],
    array_len: usize,
    pmap_vec: &mut HashMap<usize, Vec<u8>>,
    level: usize,
) -> Result<(), io::Error> {
    let file_start = file.bytes_read();

    if USE_DECODE_VERBOSE {
        println!(
            "{:indent$}decode_struct_data: {}[{}]", "",
            dna_struct.dna_name, array_len,
            indent=level * INDENT);
    }

    if USE_DECODE_BLIT && dna_struct.flag.complex_recurse_test() == false {
        debug_assert_ne!(0, array_len);
        if dna_struct.serial_size == dna_struct.runtime_size {
            read_exact!(file, &mut data_base[0..(dna_struct.serial_size * array_len)]);
        } else {
            // Padding bytes at the end of the struct found,
            // read the structs individually
            debug_assert_eq!(0, dna_struct.members[0].runtime.offset);
            let mut data_offset = 0;
            for _ in 0..array_len {
                let data_single = &mut data_base[
                    data_offset..(data_offset + dna_struct.serial_size)
                ];
                read_exact!(file, data_single);
                // TODO, runtime
                // T::init_runtime();
                data_offset += dna_struct.runtime_size;
            }
        }
    } else {
        // Padding bytes with the struct found, read the struct members.
        // TODO: use 'step_by' when stable
        let mut data_offset = 0;
        for _ in 0..array_len {
            let data_offset_next = data_offset + dna_struct.runtime_size;
            let data_single = &mut data_base[data_offset..data_offset_next];

            let file_serial_start = file.bytes_read();

            // reconstruct
            for member in &dna_struct.members {
                let member_struct = &dna_info.structs[member.type_index];
                let member_data = &mut data_single[member.runtime_range()];
                if cfg!(debug_assertions) {
                    let file_serial_curr = file.bytes_read();
                    let file_serial_offset = (file_serial_curr - file_serial_start) as usize;
                    debug_assert_eq!(file_serial_offset, member.serial.offset);
                }
                if USE_DECODE_VERBOSE {
                    println_dna_member(dna_struct, member, level);
                }
                match member.type_store {
                    DNAStore::Inline => {
                        match member.type_spec {
                            DNASpec::Pod | DNASpec::Pointer | DNASpec::List => {
                                // XXX-BITNESS
                                debug_assert_eq!(member.runtime.size, member.serial.size);
                                read_exact!(file, member_data);
                            },
                            DNASpec::Struct => {
                                decode_struct_data(
                                    file, dna_info, member_struct,
                                    member_data, member.array_flat_len,
                                    pmap_vec, level + 1,
                                )?;
                            },
                            DNASpec::Unknown => {
                                unreachable!();
                            },
                        }
                    },
                    DNAStore::Vector => {
                        let mut data_ptr: usize = 0;
                        read_exact!(file, binary_util::slice_u8_from_any_mut(&mut data_ptr));
                        if let Some(data_vec) = pmap_vec.remove(&data_ptr) {
                            unsafe {
                                ::std::ptr::write(
                                    member_data.as_ptr() as *mut Vec<u8>,
                                    data_vec,
                                );
                            }
                        } else {
                            return Err(io::Error::new(
                                io::ErrorKind::InvalidInput,
                                "Error: missing VEC",
                            ));
                        }
                    },
                    DNAStore::Unknown => {
                        unreachable!();
                    },
                }

                if cfg!(debug_assertions) {
                    let file_serial_curr = file.bytes_read();
                    let file_serial_offset = (file_serial_curr - file_serial_start) as usize;
                    debug_assert_eq!(file_serial_offset, member.serial.offset + member.serial.size);
                }
            }
            data_offset = data_offset_next;
        }
    }

    if cfg!(debug_assertions) {
        let file_curr = file.bytes_read();
        let file_size_expect = dna_struct.serial_size * array_len;
        let file_size_actual = file_curr - file_start;
        debug_assert_eq!(file_size_expect, file_size_actual);
    }

    Ok(())
}

fn decode_id<R: io::Read>(
    main: &mut MainData,
    dna_info: &DNAInfo,
    mut file: &mut ReadWrapper<R>,
    id_code: [u8; 2],
    dna_struct: &DNAStruct,
    pmap_vec: &mut HashMap<usize, Vec<u8>>,
) -> Result<PtrMut<ID>, io::Error> {

    let id_data_option: Option<&mut [u8]> = {
        use ::std::slice::from_raw_parts_mut;

        macro_rules! new_id {
            ($main_seq:tt) => {
                unsafe {
                    let id = main.$main_seq.alloc_uninitialized_list_update();
                    let id_ptr = id.as_ptr() as *mut u8;

                    if USE_DECODE_VERBOSE {
                        println!(
                            "NEW ID: {:?} at 0x{:x}",
                            stringify!($main_seq), id_ptr as usize,
                        );
                    }

                    Some(from_raw_parts_mut(id_ptr, size_of_ref_type!(&*id)))
                }
            }
        }

        match id_code {
            id_code::LIBRARY => { None },
            id_code::SCENE => { new_id!(scene) },
            id_code::OBJECT => { new_id!(object) },
            id_code::MESH => { new_id!(mesh) },
            _ => { None }
        }
    };

    let id: PtrMut<ID> = {
        if let Some(id_data) = id_data_option {
            // get scene working first!
            decode_struct_data(
                &mut file, dna_info, dna_struct, id_data, 1,
                pmap_vec, 1,
            )?;
            PtrMut(id_data.as_ptr() as *mut ID)
        } else {
            io::copy(&mut file, &mut io::sink())?; // drain
            null_mut()
        }
    };

    Ok(id)
}


#[inline]
fn pointer_link_single(
    data: &mut [u8],
    pmap_data: &HashMap<usize, usize>,
    level: usize,
) -> Result<(), io::Error> {
    debug_assert_eq!(size_of_ptr!(), data.len());
    let ptr = binary_util::slice_u8_to_any_mut::<usize>(data);
    if *ptr == 0 {
        Ok(())
    } else if let Some(ptr_dst) = pmap_data.get(ptr) {
        if USE_DECODE_VERBOSE {
            println!(
                "{:indent$}pointer_link_single: 0x{:x} -> 0x{:x}", "",
                *ptr, *ptr_dst,
                indent=level * INDENT);
        }
        *ptr = *ptr_dst;
        Ok(())
    } else {
        if USE_DECODE_VERBOSE {
            println!(
                "{:indent$}pointer_link_single: 0x{:x} lookup failure!", "",
                *ptr,
                indent=level * INDENT);
        }
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "Pointer lookup failure",
        ))
    }
}

#[inline]
fn pointer_link(
    data_base: &mut [u8],
    array_len: usize,
    pmap_data: &HashMap<usize, usize>,
    level: usize,
) -> Result<(), io::Error> {
    debug_assert_eq!(size_of_ptr!() * array_len, data_base.len());

    // TODO: use 'step_by' when stable
    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + size_of_ptr!();
        let data_single = &mut data_base[data_offset..data_offset_next];
        pointer_link_single(data_single, pmap_data, level)?;
        data_offset = data_offset_next;
    }
    Ok(())
}


fn decode_list_item_link_owned_recurse(
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    mut item: PtrMut<ListBaseLink>,
    pmap_data: &HashMap<usize, usize>,
    level: usize,
) -> Result<(), io::Error> {
    while item != null_mut() {
        // handled by members
        // (next & prev)
        decode_struct_link_owned_recurse(
            dna_info, dna_struct,
            unsafe {
                ::std::slice::from_raw_parts_mut(item.as_ptr() as *mut u8, dna_struct.runtime_size)
            }, 1,
            pmap_data, level + 1,
        )?;
        item = item.next;
    }
    Ok(())
}

fn decode_list_link_owned_recurse(
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &mut [u8],
    array_len: usize,
    pmap_data: &HashMap<usize, usize>,
    level: usize,
) -> Result<(), io::Error> {
    debug_assert_eq!((size_of_ptr!() * 2) * array_len, data_base.len());
    debug_assert_eq!((size_of_ptr!() * 2), ::std::mem::size_of::<ListBase<ListBaseLink>>());

    // TODO: use 'step_by' when stable
    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + ::std::mem::size_of::<ListBase<ListBaseLink>>();
        let data_single = &mut data_base[data_offset..data_offset_next];

        // 2 == (head & tail)
        pointer_link(data_single, 2, pmap_data, level + 1)?;

        let list: &ListBase<ListBaseLink> = binary_util::slice_u8_to_any(data_single);

        // link as single-linked list, prev pointer gets corrected too.
        decode_list_item_link_owned_recurse(
            dna_info,
            dna_struct,
            list.head,
            pmap_data,
            level + 1,
        )?;
        data_offset = data_offset_next;
    }
    Ok(())
}


fn decode_vec_link_owned_recurse(
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &mut [u8],
    array_len: usize,
    pmap_data: &HashMap<usize, usize>,
    level: usize,
) -> Result<(), io::Error> {
    // TODO: use 'step_by' when stable
    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + ::std::mem::size_of::<Vec<u8>>();
        let data_single = &mut data_base[data_offset..data_offset_next];

        let (data_vec_slice, data_vec_len) =
            binary_util::slice_of_vec_from_u8_slice_mut(data_single, dna_struct.runtime_size);

        if !dna_struct.members.is_empty() && !data_vec_slice.is_empty() {
            decode_struct_link_owned_recurse(
                dna_info, dna_struct,
                data_vec_slice, data_vec_len,
                pmap_data, level + 1,
            )?;
        }
        data_offset = data_offset_next;
    }
    Ok(())
}

/// Function that corrects 'links'
/// that is - map all the old pointers to new pointer locations.
/// To be done after file-reading is finished and we have all the new locations allocated.
fn decode_struct_link_owned_recurse(
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &mut [u8],
    array_len: usize,
    pmap_data: &HashMap<usize, usize>,
    level: usize,
) -> Result<(), io::Error> {

    if USE_DECODE_VERBOSE {
        println!(
            "{:indent$}decode_struct_link_owned_recurse: {}[{}] at 0x{:x}", "",
            dna_struct.dna_name, array_len, data_base.as_ptr() as usize,
            indent=level * INDENT
        );
    }

    // TODO: use 'step_by' when stable
    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + dna_struct.runtime_size;
        let data_single = &mut data_base[data_offset..data_offset_next];

        // reconstruct
        for member in &dna_struct.members {
            let member_struct = &dna_info.structs[member.type_index];
            let member_data = &mut data_single[member.runtime_range()];
            if USE_DECODE_VERBOSE {
                println_dna_member(dna_struct, member, level);
            }
            match member.type_store {
                DNAStore::Inline => {
                    match member.type_spec {
                        DNASpec::Pod => {},
                        DNASpec::Pointer => {
                            pointer_link(member_data, member.array_flat_len, pmap_data, level + 1)?;
                        },
                        DNASpec::Struct => {
                            decode_struct_link_owned_recurse(
                                dna_info, member_struct,
                                member_data, member.array_flat_len,
                                pmap_data, level + 1,
                            )?;
                        },
                        DNASpec::List => {
                            decode_list_link_owned_recurse(
                                dna_info, member_struct,
                                member_data, member.array_flat_len,
                                pmap_data, level + 1,
                            )?;
                        },
                        DNASpec::Unknown => {
                            unreachable!();
                        },
                    }
                },
                DNAStore::Vector => {
                    decode_vec_link_owned_recurse(
                        dna_info, member_struct,
                        member_data, member.array_flat_len,
                        pmap_data, level + 1,
                    )?;
                },
                DNAStore::Unknown => {
                    unreachable!();
                },
            }
        }
        data_offset = data_offset_next;
    }
    Ok(())
}

fn decode_blocks<R: io::Read>(
    mut file: &mut ReadWrapper<R>,
    main: &mut MainData,
    dna_info: &DNAInfo,
    endian_swap: bool,
) -> Result<(), io::Error> {

    use ::std::collections::HashMap;

    // Memory is owned by both: 'main.MainDataSeq' and 'main.elem.*' pools.
    let mut pmap_data: HashMap<usize, usize> = HashMap::new();
    // Memory is transfured into the structs on use.
    let mut pmap_vec: HashMap<usize, Vec<u8>> = HashMap::new();

    loop {
        let mut bhead: BHead = unsafe { ::std::mem::uninitialized() };
        let bhead_read = io::Read::read(file, binary_util::slice_u8_from_any_mut(&mut bhead))?;
        if bhead_read == ::std::mem::size_of::<BHead>() {
            // ok
        } else if bhead_read == 0 {
            break;
        } else {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "Premature end of file or corrupt block size",
            ));
        }

        // so we know what we're dealing with
        if USE_DECODE_VERBOSE {
            println!(
                "read block code: {}, len: {}, size: {}, struct: {} at 0x{:}",
                String::from_utf8_lossy(binary_util::slice_u8_from_any(&bhead.code)),
                bhead.len,
                bhead.size,
                &dna_info.structs[bhead.type_index as usize].dna_name,
                bhead.ptr,
            );
        }

        let bhead_size_final = bhead.len as usize * bhead.size;

        // Take a new chunk for each BHead,
        // this isn't essential, but it means we ensure we never cross
        {
            let file_bhead_start = file.bytes_read();
            // can't use match (causes E0080),
            // see: http://stackoverflow.com/a/42201782/432509
            if bhead.code == bhead_code::SDNA {
                let data_len = bhead.len as usize * bhead.size;
                let mut data = Vec::with_capacity(data_len);
                unsafe { data.set_len(data_len); }
                read_exact!(file, &mut data);
                let _dna_info_src = self::super::sdna::decode_sdna::decode_sdna(
                    &data[..], endian_swap)?;
            } else if bhead.code == bhead_code::DATA {
                if let Some(data) = main.elem.alloc_slice_from_type_index(
                    bhead.type_index as usize,
                ) {
                    let dna_struct = &dna_info.structs[bhead.type_index as usize];
                    decode_struct_data(
                        file, dna_info, dna_struct, data, 1,
                        &mut pmap_vec, 0,
                    )?;
                    pmap_data.insert(bhead.ptr, data.as_ptr() as usize);
                } else {
                    return Err(io::Error::new(
                        io::ErrorKind::InvalidInput,
                        "Unknown data-type",
                    ));
                }
            } else if bhead.code == bhead_code::VEC {
                // TODO, validate index!
                let dna_struct = &dna_info.structs[bhead.type_index as usize];
                // Alignment???
                let data_runtime_len = dna_struct.runtime_size * bhead.len as usize;
                let mut data_vec: Vec<u8> = Vec::with_capacity(data_runtime_len);
                unsafe { data_vec.set_len(data_runtime_len); }

                decode_struct_data(
                    file, dna_info, dna_struct, &mut data_vec[..], bhead.len as usize,
                    &mut pmap_vec, 0,
                )?;
                // real length (once type is translated)
                unsafe { data_vec.set_len(bhead.len as usize); }
                pmap_vec.insert(bhead.ptr, data_vec);
            } else {
                // TODO, just use flag w/ bit shifting
                let code: [u8; 4] = {
                    let code_slice = binary_util::slice_u8_from_any(&bhead.code);
                    unpack!([code_slice; 4])
                };
                if code[0..2] == [b'I', b'D'] {
                    let id_code: [u8; 2] = [code[2], code[3]];
                    let id = decode_id(
                        main,
                        dna_info,
                        file,
                        id_code,
                        &dna_info.structs[bhead.type_index as usize],
                        &mut pmap_vec,
                    )?;

                    pmap_data.insert(bhead.ptr, id.as_ptr() as usize);
                } else {
                    println!(
                        "Unknown code: {}!",
                        String::from_utf8_lossy(binary_util::slice_u8_from_any(&bhead.code))
                    );
                    {
                        let mut file_bhead = io::Read::take(&mut file, bhead_size_final as u64);
                        io::copy(&mut file_bhead, &mut io::sink())?; // drain
                    }
                }
            }
            if cfg!(debug_assertions) {
                let file_bhead_curr = file.bytes_read();
                let bhead_size_actual = file_bhead_curr - file_bhead_start;
                debug_assert_eq!(bhead_size_final, bhead_size_actual);
            }
        }
    }

    if USE_DECODE_VERBOSE {
        println!(
            "\ndecode_main_id_seq_link begin: pmap_data[{}]",
            pmap_data.len());
    }

    {
        fn decode_main_id_seq_link<T>(
            dna_info: &DNAInfo,
            id_seq: &mut MainDataSeq<T>,
            pmap_data: &HashMap<usize, usize>,
        ) -> Result<(), io::Error>
            where
            T: MainDataSeqTrait + DNATypeIndexImpl + 'static,
        {
            let dna_struct = &dna_info.structs[T::dna_type_index()];
            // Can't use iterator since the next/prev pointers aren't yet updated.
            // for mut id in id_seq.iter_mut() ...
            let mut id: PtrMut<T> = id_seq.head_mut();
            while id != null_mut() {
                decode_struct_link_owned_recurse(
                    &dna_info, dna_struct,
                    binary_util::slice_u8_from_any_mut(&mut *id),
                    1, pmap_data, 1,
                )?;
                // not pretty, if we end up needing next/prev members often,
                // then we better expose generic methods, for now just use pointer magic.
                id = PtrMut(id.as_id_mut().next.as_ptr() as *mut T);
            }
            Ok(())
        }
        // this could multi-thread
        macro_rules! decode_main_id_seq_link_member {
            ($($member_id:tt)*) => ($(
                decode_main_id_seq_link(dna_info, &mut main.$member_id, &pmap_data)?;
            )*)
        }
        dna_main_data_seq_apply!(decode_main_id_seq_link_member);
    }
    Ok(())
}


/// main is a return argument!
/// we could just return a new main
///
/// * `file_len` for sanity checks, we don't really need it
///   but this avoids attempting to allocate unreasonably large values
///   (in the case of corrupt data).

pub fn decode_main<R: io::Read>(
    file_base: R,
    main: &mut MainData,
    dna_info: &DNAInfo,
) -> Result<(), io::Error> {

    let mut file = read_wrapper::ReadWrapper::new(file_base);

    let header = decode_header(&mut file)?;

    let endian_swap = header.is_big_endian != cfg!(target_endian = "big");

    if USE_DECODE_VERBOSE {
        println!("{:?}", header);
    }

    decode_blocks(&mut file, main, dna_info, endian_swap)?;
    Ok(())
}
