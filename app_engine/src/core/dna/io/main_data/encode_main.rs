// Licensed: GPL v2+

use ::core::dna::main_data::{
    MainData,
    MainDataSeq,
    MainDataSeqTrait,
};

use ::core::dna::io::{
    DNAInfo,
    DNASpec,
    DNAStore,
    DNAStruct,
    DNATypeIndexImpl,

};

use ::core::dna::io::main_data::{
    bhead_code,
    BHead,

    ListBaseLink,
};

use list_base::{
    ListBase,
};

use ::std::io;
use ::std::io::Write;

use super::super::binary_util;


// ----------------------------------------------------------------------------
// Consts

/// Where possible, blit content directly to the file when writing.
/// Disable for debugging may be useful.
const USE_ENCODE_BLIT: bool = false;


// ----------------------------------------------------------------------------
// Wrapper for Write trait
//
// Currently only keeps track of the number of bytes written.
// It's useful for sanity checks.
//
// See: http://stackoverflow.com/a/42189386/432509

use self::write_wrapper::WriteWrapper;

mod write_wrapper {
    use std::io::{self, Write};

    pub struct WriteWrapper<W> {
        inner: W,
        count: usize,
    }

    impl<W> WriteWrapper<W>
        where W: Write
    {
        pub fn new(inner: W) -> Self {
            WriteWrapper {
                inner: inner,
                count: 0,
            }
        }

        #[inline]
        pub fn into_inner(self) -> W {
            self.inner
        }
        #[inline]
        pub fn bytes_written(&self) -> usize {
            self.count
        }
    }

    impl<W> Write for WriteWrapper<W>
        where W: Write
    {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            let res = self.inner.write(buf);
            if let Ok(size) = res {
                self.count += size
            }
            res
        }

        fn flush(&mut self) -> io::Result<()> {
            self.inner.flush()
        }
    }
}


// ----------------------------------------------------------------------------
// Encoding Functions

/// Writes header, 16 bytes
pub fn encode_header<W: io::Write>(
    file: &mut WriteWrapper<W>,
) -> Result<(), io::Error> {
    // file magic, bitness & endian
    file.write(&[
        b'W', b'I', b'S', b'K', b'3', b'D',
        if cfg!(target_endian = "big") {b'_'} else {b'-'},
        {
            if cfg!(target_pointer_width = "32") {
                b'V'
            } else if cfg!(target_pointer_width = "64") {
                b'v'
            } else {
                unreachable!()
            }
        },

        // format date: YYYY MM DD
        b'Y', b'Y', b'Y', b'Y',
        b'M', b'M',
        b'D', b'D',

    ])?;

    debug_assert_eq!(16, file.bytes_written());

    Ok(())
}

/// Note that 'array_len' is nearly always 1 and unrelated to the list size.
/// A different value will be used for 'some_list: [ListBase<SomeType>; 2]'
/// ... an inline fixed size array of lists.
fn encode_list_owned_recurse<W: io::Write>(
    file: &mut WriteWrapper<W>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &[u8],
    array_len: usize,
) -> Result<(), io::Error> {
    debug_assert_eq!((size_of_ptr!() * 2) * array_len, data_base.len());
    debug_assert_eq!((size_of_ptr!() * 2), ::std::mem::size_of::<ListBase<ListBaseLink>>());

    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + ::std::mem::size_of::<ListBase<ListBaseLink>>();
        let data_single = &data_base[data_offset..data_offset_next];
        let list: &ListBase<ListBaseLink> = binary_util::slice_u8_to_any(data_single);
        if !list.is_empty() {
            let mut bhead = BHead {
                code: bhead_code::DATA,
                type_index: dna_struct.type_index as u32,
                ptr: 0, // update each write
                size: dna_struct.serial_size,
                len: 1,
            };

            for item in list.iter() {
                let data_item = unsafe {
                    ::std::slice::from_raw_parts(
                        item.as_ptr() as *const u8,
                        dna_struct.runtime_size,
                    )
                };

                bhead.ptr = item.as_ptr() as usize;
                encode_struct_bhead_and_owned(
                    file, &dna_info, dna_struct,
                    data_item, 1, &bhead,
                )?;
            }
        }
        data_offset = data_offset_next;
    }

    Ok(())
}

/// Write the data we own (but not our data)
/// Currently only vecs and lists.
///
/// Note that 'array_len' is nearly always 1 and unrelated to the vector size.
/// A different value will be used for 'some_vec: [Vec<SomeType>; 2]'
/// ... an inline fixed size array of vectors.
fn encode_vec_owned_recurse<W: io::Write>(
    file: &mut WriteWrapper<W>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &[u8],
    array_len: usize,
    level: usize,
) -> Result<(), io::Error> {
    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + ::std::mem::size_of::<Vec<u8>>();
        let data_single = &data_base[data_offset..data_offset_next];

        let (data_vec_slice, data_vec_len) =
            binary_util::slice_of_vec_from_u8_slice(data_single, dna_struct.runtime_size);

        if !dna_struct.members.is_empty() && !data_vec_slice.is_empty() {
            encode_struct_owned_recurse(
                file, dna_info, dna_struct, data_vec_slice, data_vec_len,
                level + 1,
            )?;
        }

        let bhead = BHead {
            code: bhead_code::VEC,
            type_index: dna_struct.type_index as u32,
            ptr: data_vec_slice.as_ptr() as usize,
            size: dna_struct.serial_size,
            len: data_vec_len as u64,
        };

        file.write(binary_util::slice_u8_from_any(&bhead))?;
        let file_write_start = file.bytes_written();

        encode_struct_data(
            file, dna_info, dna_struct, data_vec_slice, data_vec_len,
            level + 1,
        )?;

        if cfg!(debug_assertions) {
            let file_write_curr = file.bytes_written();
            debug_assert_eq!(
                bhead.size * bhead.len as usize,
                (file_write_curr - file_write_start)
            );
        }
        data_offset = data_offset_next;
    }

    Ok(())
}

/// Write the data we own (but not this struct)
/// Currently only vecs and lists.
///
/// Note, a _lot_ of this could be optimized,
/// since we're searching down trees of members that may not own any data.
/// For now just get basics working.
///
fn encode_struct_owned_recurse<W: io::Write>(
    file: &mut WriteWrapper<W>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &[u8],
    array_len: usize,
    level: usize,
) -> Result<(), io::Error> {
    // TODO, optimize, should detect structs with no owned data to write
    let mut data_offset = 0;
    for _ in 0..array_len {
        let data_offset_next = data_offset + dna_struct.runtime_size;
        let data_single = &data_base[data_offset..data_offset_next];
        for member in &dna_struct.members {
            // could reduce scope
            let member_struct = &dna_info.structs[member.type_index];
            let member_data = &data_single[member.runtime_range()];

            match member.type_store {
                DNAStore::Inline => {
                    match member.type_spec {
                        DNASpec::Struct => {
                            encode_struct_owned_recurse(
                                file, dna_info, member_struct,
                                member_data, member.array_flat_len,
                                level + 1,
                            )?;
                        },
                        DNASpec::List => {
                            encode_list_owned_recurse(
                                file, dna_info, member_struct,
                                member_data, member.array_flat_len,
                            )?;
                        },
                        DNASpec::Pod | DNASpec::Pointer => {
                            // pass
                        },
                        DNASpec::Unknown => {
                            unreachable!();
                        },
                    }
                },
                DNAStore::Vector => {
                    encode_vec_owned_recurse(
                        file, dna_info, member_struct,
                        member_data, member.array_flat_len,
                        level + 1,
                    )?;
                },
                DNAStore::Unknown => {
                    unreachable!();
                },
            }
        }
        data_offset = data_offset_next;
    }

    Ok(())
}

/// Write the struct it's self (the serialized version)
fn encode_struct_data<W: io::Write>(
    file: &mut WriteWrapper<W>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data_base: &[u8],
    array_len: usize,
    level: usize,
) -> Result<(), io::Error> {
    let file_write_start = file.bytes_written();
    if USE_ENCODE_BLIT && dna_struct.flag.complex_recurse_test() == false {
        debug_assert_ne!(0, array_len);
        if dna_struct.serial_size == dna_struct.runtime_size {
            file.write(&data_base[0..(dna_struct.serial_size * array_len)])?;
        } else {
            // Padding bytes at the end of the struct found,
            // write the structs individually
            let mut data_offset = 0;
            for _ in 0..array_len {
                let data_single = &data_base[data_offset..(data_offset + dna_struct.serial_size)];
                file.write(data_single)?;
                data_offset += dna_struct.runtime_size;
            }
        }
    } else {
        // Padding bytes with the struct found, write the structs individually
        let mut data_offset = 0;
        for i in 0..array_len {
            let data_single = &data_base[data_offset..(data_offset + dna_struct.runtime_size)];
            for member in &dna_struct.members {
                let member_struct = &dna_info.structs[member.type_index];
                let member_data = &data_single[member.runtime_range()];

                if cfg!(debug_assertions) {
                    let file_write_curr = file.bytes_written();
                    let offset_actual = file_write_curr - file_write_start;
                    let offset_expect = member.serial.offset + (i * dna_struct.serial_size);
                    if offset_expect != offset_actual {
                        println!("entry error: '{}.{}'", dna_struct.dna_name, member.dna_name);
                        debug_assert_eq!(offset_expect, offset_actual);
                    }
                }

                match member.type_store {
                    DNAStore::Inline => {
                        match member.type_spec {
                            DNASpec::Pod | DNASpec::Pointer | DNASpec::List => {
                                debug_assert_eq!(member.runtime.size, member.serial.size);
                                file.write(member_data)?;
                            },
                            DNASpec::Struct => {
                                encode_struct_data(
                                    file, dna_info, member_struct,
                                    member_data, member.array_flat_len,
                                    level + 1,
                                )?;
                            },
                            DNASpec::Unknown => {
                                unreachable!();
                            }
                        }
                    },
                    DNAStore::Vector => {
                        let data_vec: &Vec<u8> = binary_util::vec_from_u8_slice(member_data);
                        let data_vec_as_ptr = {
                            if !data_vec.is_empty() {
                                data_vec.as_ptr() as usize
                            } else {
                                0
                            }
                        };
                        file.write(binary_util::slice_u8_from_any(&data_vec_as_ptr))?;
                    },
                    DNAStore::Unknown => {
                        unreachable!();
                    },
                }

                if cfg!(debug_assertions) {
                    let file_write_curr = file.bytes_written();
                    let offset_actual = file_write_curr - file_write_start;
                    let offset_expect = member.serial.offset +
                        member.serial.size + (i * dna_struct.serial_size);
                    if offset_expect != offset_actual {
                        println!("entry error: '{}.{}'", dna_struct.dna_name, member.dna_name);
                        debug_assert_eq!(offset_expect, offset_actual);
                    }
                }
            }
            data_offset += dna_struct.runtime_size;
        }
    }

    if cfg!(debug_assertions) {
        let file_write_curr = file.bytes_written();
        debug_assert_eq!(
            dna_struct.serial_size * array_len,
            (file_write_curr - file_write_start));
    }
    Ok(())
}

fn encode_struct_bhead_recurse<W: io::Write>(
    file: &mut WriteWrapper<W>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data: &[u8],
    array_len: usize,
    bhead: &BHead,
    level: usize,
) -> Result<(), io::Error> {
    file.write(binary_util::slice_u8_from_any(bhead))?;
    let file_write_start = file.bytes_written();

    encode_struct_data(
        file, dna_info, dna_struct, data, array_len,
        level + 1,
    )?;

    if cfg!(debug_assertions) {
        let file_write_curr = file.bytes_written();
        debug_assert_eq!(file_write_curr - file_write_start, bhead.size);
    }
    Ok(())
}

fn encode_struct_bhead_and_owned<W: io::Write>(
    file: &mut WriteWrapper<W>,
    dna_info: &DNAInfo,
    dna_struct: &DNAStruct,
    data: &[u8],
    array_len: usize,
    bhead: &BHead,
) -> Result<(), io::Error> {

    encode_struct_owned_recurse(file, &dna_info, dna_struct, data, array_len, 0)?;

    encode_struct_bhead_recurse(file, &dna_info, dna_struct, data, array_len, &bhead, 0)?;

    Ok(())
}


fn encode_blocks<W: io::Write>(
    file: &mut WriteWrapper<W>,
    main: &MainData,
    dna_info: &DNAInfo,
) -> Result<(), io::Error> {

    // use for writing to disk
    if true {
        let sdna_data = self::super::sdna::encode_sdna::encode_sdna(dna_info);
        let bhead = BHead {
            code: bhead_code::SDNA,
            type_index: 0, // unused
            ptr: 0, // unused
            size: sdna_data.len(),
            len: 1,
        };

        file.write(binary_util::slice_u8_from_any(&bhead))?;
        file.write(&sdna_data)?;
    }

    fn encode_main_id_seq<W: io::Write, T>(
        file: &mut WriteWrapper<W>,
        dna_info: &DNAInfo,
        id_seq: &MainDataSeq<T>,
    ) -> Result<(), io::Error>
        where
        W: io::Write,
        T: MainDataSeqTrait + DNATypeIndexImpl + 'static,
    {
        let dna_struct = &dna_info.structs[T::dna_type_index()];

        let mut bhead = BHead {
            code: bhead_code::from_id_code::<T>(),
            type_index: dna_struct.type_index as u32,
            ptr: 0, // update each write
            size: dna_struct.serial_size,
            len: 1,
        };

        for id in id_seq.iter() {
            bhead.ptr = id.as_ptr() as usize;
            let data = binary_util::slice_u8_from_any(&*id);
            encode_struct_bhead_and_owned(file, &dna_info, dna_struct, data, 1, &bhead)?;
        }

        Ok(())
    }

    macro_rules! encode_main_id_seq_member {
        ($($member_id:tt)*) => ($(
            encode_main_id_seq(file, dna_info, &main.$member_id)?;
        )*)
    }

    dna_main_data_seq_apply!(encode_main_id_seq_member);

    Ok(())
}


pub fn encode_main<W: io::Write>(
    file_base: W,
    main: &MainData,
    dna_info: &DNAInfo,
) -> Result<(), io::Error> {

    let mut file = write_wrapper::WriteWrapper::new(file_base);

    encode_header(&mut file)?;

    encode_blocks(&mut file, main, dna_info)?;

    Ok(())
}
