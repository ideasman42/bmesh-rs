// Licensed: GPL v2+

pub mod sdna;

pub mod encode_main;
pub mod decode_main;

use ::plain_ptr::{
    PtrMut,
};

// ---------------
// BHead (file IO)

// Note that ordering is such that 'packed' shouldn't be needed,
// Just adding because serialization doesn't take pad-bits into account.

// 32bit: 24, 64bit: 32
#[repr(C, packed)]
pub struct BHead {
    // all: 8
    pub code: u32,
    pub type_index: u32,

    // 32bit: 8, 64bit: 16
    /// pointer on writing, remapped on reading
    pub ptr: usize,
    /// Size in bytes (multiplied by 'len' for final size)
    pub size: usize,

    // all: 8
    pub len: u64,
}

#[repr(C, packed)]
pub struct BHead32 {
    pub code: [u8; 4],
    pub type_index: u32,
    pub ptr:  u32, // non-native
    pub size: u32, // non-native
    pub len: u64,
}

#[repr(C, packed)]
pub struct BHead64 {
    pub code: [u8; 4],
    pub type_index: u32,
    pub ptr:  u64, // non-native
    pub size: u64, // non-native
    pub len: u64,
}

/// Keep BHead codes here, makes their use easier to keep track of.
/// TODO: comment use of each.
pub mod bhead_code {

    /// Optional Struct DNA data block,
    /// needed for reading versioned files.
    pub const SDNA: u32 = u32_code!(b"SDNA");

    pub const DATA: u32 = u32_code!(b"DATA");

    pub const VEC:  u32 = u32_code!(b"VEC\0");

    use ::core::dna::types::{
        IDTypeImpl,
    };
    #[inline]
    pub fn from_id_code<T: IDTypeImpl>() -> u32 {
        let id_code = T::id_code();
        let data: [u8; 4] = [b'I', b'D', id_code[0], id_code[1]];
        u32_code!(&data)
    }
}

// ---------------------------------
// Generic Lists (ListBase Elements)

use list_base::{
    ListBaseElemUtils,
};

#[repr(C, packed)]
struct ListBaseLink {
    // XXX, we may want to make these const, for now just don't modify on write!
    next: PtrMut<ListBaseLink>,
    prev: PtrMut<ListBaseLink>,
}

impl ListBaseElemUtils for ListBaseLink {
    #[inline] fn next_get(&self) -> PtrMut<Self> { self.next }
    #[inline] fn prev_get(&self) -> PtrMut<Self> { self.prev }
    #[inline] fn next_set(&mut self, ptr: PtrMut<Self>) { self.next = ptr; }
    #[inline] fn prev_set(&mut self, ptr: PtrMut<Self>) { self.prev = ptr; }
}
