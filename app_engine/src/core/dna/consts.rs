// Licensed: GPL v2+

// Intended for global imports, keep names verbose.

pub const ID_NAME_LEN: usize = 32;

pub const MEMPOOL_CHUNK_SIZE: usize = 64;

// we may want to make an enum
pub mod id_code {
    pub const NUL:           [u8; 2] = [b'\0', b'\0'];
    pub const LIBRARY:       [u8; 2] = [b'L', b'I'];
    pub const SCENE:         [u8; 2] = [b'S', b'C'];
    pub const OBJECT:        [u8; 2] = [b'O', b'B'];
    pub const MESH:          [u8; 2] = [b'M', b'E'];
}
