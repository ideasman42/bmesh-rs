// Licensed: GPL v2+

#[macro_use]
pub mod main_data;

// keep first
#[macro_use]
pub mod types;

#[macro_use]
pub mod io;


pub mod consts;

pub mod prelude {
    pub use self::super::types::*;
    pub use self::super::consts::*;
}


