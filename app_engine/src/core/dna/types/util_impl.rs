// Licensed: GPL v2+

use ::core::dna::consts::*;
use byte_str_util;

use super::{
    ID,
    Library,
    Scene,
    Object,
    Mesh,
};

use ::plain_ptr::*;

// --
// ID


macro_rules! id_type_impl_generic {
    () => {
        fn as_id(&self) -> &ID { &self.id }
        fn as_id_mut(&mut self) -> &mut ID { &mut self.id }
    }
}

pub trait IDTypeImpl {
    fn id_code() -> [u8; 2];
    fn default_name() -> &'static str;

    // id_type_impl_generic
    fn as_id(&self) -> &ID;
    fn as_id_mut(&mut self) -> &mut ID;
}

impl IDTypeImpl for Library {
    fn id_code() -> [u8; 2] {
        return id_code::LIBRARY;
    }
    fn default_name() -> &'static str {
        return "Library";
    }

    id_type_impl_generic!();
}
impl IDTypeImpl for Scene {
    fn id_code() -> [u8; 2] {
        return id_code::SCENE;
    }
    fn default_name() -> &'static str {
        return "Scene";
    }

    id_type_impl_generic!();
}
impl IDTypeImpl for Object {
    fn id_code() -> [u8; 2] {
        return id_code::OBJECT;
    }
    fn default_name() -> &'static str {
        return "Object";
    }

    id_type_impl_generic!();
}
impl IDTypeImpl for Mesh {
    fn id_code() -> [u8; 2] {
        return id_code::MESH;
    }
    fn default_name() -> &'static str {
        return "Mesh";
    }

    id_type_impl_generic!();
}

impl ID {
    pub fn name_set_notest(&mut self, name: &str) {
        byte_str_util::strlcpy_str_utf8(&mut self.name, name);
    }
    pub fn name_set(&mut self, name: &str) {
        // WARNING: O(n^2) lookups here,
        // Will want to maintain a b-tree of names eventually.
        let mut name_tmp: [u8; ID_NAME_LEN] = [0; ID_NAME_LEN];
        let mut name_tmp_len = byte_str_util::strlcpy_str_utf8(&mut name_tmp[..], name);

        fn is_dupe(id: PtrConst<ID>, name_test: &[u8]) -> bool {
            let name_test_len = name_test.len();
            macro_rules! id_search_dir {
                ($step_attr:tt) => {
                    let mut id_step = id.$step_attr.as_const();
                    while id_step != null_const() {
                        if name_test == &id_step.name[0..name_test_len] {
                            return true;
                        }
                        id_step = id_step.$step_attr.as_const();
                    }
                }
            }
            id_search_dir!(prev);
            id_search_dir!(next);
            return false;
        }

        let mut num: u32 = 0;
        while is_dupe(PtrConst(self), &name_tmp[0..name_tmp_len]) {
            let ext = format!(".{:03}", num);
            name_tmp_len = byte_str_util::strlext_str_utf8(&mut name_tmp[..], name, &ext);
            num += 1;
        }
        self.name = name_tmp;
        // println!("using: {:?}:{}", self.ty, self.name_get_str());
    }
    pub fn name_get_str(&self) -> &str {
        // we _know_ names are valid utf8, so its safe
        return unsafe {
            byte_str_util::str_from_u8_nul_utf8_unchecked(&self.name)
        };
    }

    pub fn downcast_mut<T: IDTypeImpl>(self: &mut ID) -> Option<PtrMut<T>> {
        if self.ty == T::id_code() {
            use ::std::mem::transmute;
            return Some(unsafe {
                transmute::<&mut ID, PtrMut<T>>(self)
            });
        }
        return None;
    }

    pub fn downcast_ref<T: IDTypeImpl>(self: &ID) -> Option<PtrConst<T>> {
        if self.ty == T::id_code() {
            use ::std::mem::transmute;
            return Some(unsafe {
                transmute::<&ID, PtrConst<T>>(self)
            });
        }
        return None;
    }
}

mod list_impl {
    use ::plain_ptr::*;
    pub use ::list_base::{
        ListBase,
        ListBaseElemUtils,
    };
    use super::super::scene::{
        ObjectLayer,
        ObjectInst,
    };

    macro_rules! list_base_elem_utils_impl {
        ($($t:ty)*) => ($(
            impl ListBaseElemUtils for $t {
                #[inline]
                fn next_get(&self) -> PtrMut<Self> { PtrMut(self.next.as_ptr() as *mut Self) }
                #[inline]
                fn prev_get(&self) -> PtrMut<Self> { PtrMut(self.prev.as_ptr() as *mut Self) }
                #[inline]
                fn next_set(&mut self, ptr: PtrMut<Self>) { self.next = ptr; }
                #[inline]
                fn prev_set(&mut self, ptr: PtrMut<Self>) { self.prev = ptr; }
            }
        )*)
    }

    list_base_elem_utils_impl!(
        ObjectLayer
        ObjectInst
    );
}

mod pool_impl {
    use ::mempool::{
        // MemPool,
        MemPoolElemUtils,
    };
    use super::super::scene::{
        ObjectLayer,
        ObjectInst,
    };

    impl MemPoolElemUtils for ObjectLayer {
        #[inline]
        fn default_chunk_size() -> usize {
            return ::core::dna::consts::MEMPOOL_CHUNK_SIZE;
        }
        #[inline]
        fn free_ptr_get(&self) -> *mut Self {
            return self.next.as_ptr() as usize as *mut Self;
        }
        #[inline]
        fn free_ptr_set(&mut self, ptr: *mut Self) {
            self.next = ::plain_ptr::PtrMut(ptr as usize as *mut _);
            self.name[0] = 0;
        }
        #[inline]
        fn free_ptr_test(&self) -> bool {
            return self.name[0] == 0;
        }
    }

    impl MemPoolElemUtils for ObjectInst {
        #[inline]
        fn default_chunk_size() -> usize {
            return ::core::dna::consts::MEMPOOL_CHUNK_SIZE;
        }
        #[inline]
        fn free_ptr_get(&self) -> *mut Self {
            return self.object.as_ptr() as usize as *mut Self;
        }
        #[inline]
        fn free_ptr_set(&mut self, ptr: *mut Self) {
            use ::core::dna::types::ObjectInstFlag;
            self.object = ::plain_ptr::PtrMut(ptr as usize as *mut _);
            self.flag = ObjectInstFlag(0xff);
        }
        #[inline]
        fn free_ptr_test(&self) -> bool {
            return self.flag == 0xff;
        }
    }

}
