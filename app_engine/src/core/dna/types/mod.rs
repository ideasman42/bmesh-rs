// Licensed: GPL v2+

// keep first
#[macro_use]
pub mod struct_list;

mod util_impl;
mod io_impl;

mod local_prelude {
    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
    pub use ::std::collections::BTreeSet;
    pub use super::super::consts::*;
    pub use ::list_base::{
        ListBase,
    };
    pub use math_misc::{
        unit_m4_ret,
    };
    pub use super::super::io::{
        DNAStruct,
        DNAStructMember,
        DNATypeStructMembersImpl,
    };

    pub use super::util_impl::{
        IDTypeImpl,
    };
}


mod library {
    use super::local_prelude::*;
    use super::{
        ID,
    };
    #[repr(C)]
    pub struct Library {
        pub id: ID,

        pub filepath: [u8; 1024],
    }

    impl Default for Library {
        fn default() -> Self {
            Library {
                id: ID {
                    next: null_mut(),
                    prev: null_mut(),
                    ty: Self::id_code(),
                    name: Default::default(),
                    lib: null_mut(),
                },
                filepath: [0; 1024],
            }
        }
    }
}

mod id {
    use super::local_prelude::*;
    use super::{
        Library,
    };

    #[repr(C)]
    pub struct ID {
        pub next: PtrMut<ID>,
        pub prev: PtrMut<ID>,

        pub lib: PtrMut<Library>,
        pub ty: [u8; 2],
        pub name: [u8; ID_NAME_LEN],
    }

}

mod scene {
    use super::local_prelude::*;
    use super::{
        ID,
        Object,
    };
    #[repr(C)]
    pub struct Scene {
        pub id: ID,

        /// We may want to split this up differently
        pub object_layers: ListBase<ObjectLayer>,

        pub object_layer_active: PtrMut<ObjectLayer>,
        pub object_inst_active: PtrMut<ObjectInst>,
    }

    impl Default for Scene {
        fn default() -> Self {
            Scene {
                id: ID {
                    next: null_mut(),
                    prev: null_mut(),
                    ty: Self::id_code(),
                    name: Default::default(),
                    lib: null_mut(),
                },
                object_layers: Default::default(),
                object_layer_active: null_mut(),
                object_inst_active: null_mut(),
            }
        }
    }


    #[repr(C)]
    pub struct ObjectLayer {
        pub next: PtrMut<ObjectLayer>,
        pub prev: PtrMut<ObjectLayer>,
        pub name: [u8; 32],
        pub objects: ListBase<ObjectInst>,
    }

    impl Default for ObjectLayer {
        fn default() -> Self {
            ObjectLayer {
                next: null_mut(),
                prev: null_mut(),
                name: [0; 32],
                objects: Default::default(),
            }
        }
    }

    #[repr(C)]
    pub struct ObjectInst {
        pub next: PtrMut<ObjectInst>,
        pub prev: PtrMut<ObjectInst>,
        pub object: PtrMut<Object>,
        pub matrix: [[f64; 4]; 4],
        pub flag: ObjectInstFlag,
    }
    // ObjectInst.flag
    // pub const OBJINST_FLAG_SELECT: u8 = (1 << 0);

    struct_bitflag_impl!(pub struct ObjectInstFlag(pub u8));
    impl ObjectInstFlag {
        pub const SELECT:       Self = ObjectInstFlag(1 << 0);
    }

    struct_bitflag_flag_fn_impl!(
        ObjectInstFlag, ObjectInstFlag::SELECT,
        test=select_test, enable=select_enable, disable=select_disable,
        set=select_set, toggle=select_toggle);

    impl Default for ObjectInst {
        fn default() -> Self {
            ObjectInst {
                next: null_mut(),
                prev: null_mut(),
                object: null_mut(),
                matrix: unit_m4_ret(),
                flag: ObjectInstFlag(0),
            }
        }
    }
}

mod object {
    use super::local_prelude::*;
    use super::{
        ID,
    };

    #[repr(C)]
    pub struct Object {
        pub id: ID,

        pub data_ty: [u8; 2],
        pub data: PtrMut<ID>,
    }

    impl Default for Object {
        fn default() -> Self {
            Object {
                id: ID {
                    next: null_mut(),
                    prev: null_mut(),
                    ty: Self::id_code(),
                    name: Default::default(),
                    lib: null_mut(),
                },
                data_ty: id_code::NUL,
                data: null_mut(),
            }
        }
    }

}

mod mesh {
    use super::local_prelude::*;
    use super::mesh_runtime::{
        MeshRuntime,
    };
    use super::{
        ID,
    };

    #[repr(C)]
    pub struct Mesh {
        pub id: ID,

        pub verts: Vec<MVert>,
        pub edges: Vec<MEdge>,
        pub loops: Vec<MLoop>,
        pub faces: Vec<MFace>,

        // vdata: CustomData,
        // edata: CustomData,
        // ldata: CustomData,
        // mdata: CustomData,

        pub flag: u8,

        pub rt: MeshRuntime,
    }

    #[repr(C)]
    pub struct MVert {
        pub co: [f64; 3],
        pub flag: u8,
    }

    #[repr(C)]
    pub struct MEdge {
        pub verts: [u32; 2],
        pub flag: u8,
    }

    #[repr(C)]
    pub struct MLoop {
        pub v: u32,
        pub e: u32,
    }

    // called MPoly in Blender
    #[repr(C)]
    pub struct MFace {
        pub loop_start: u32,
        pub loop_len: u32,
        pub flag: u8,
    }

    impl Default for Mesh {
        fn default() -> Mesh {
            Mesh {
                id: ID {
                    next: null_mut(),
                    prev: null_mut(),
                    ty: Self::id_code(),
                    name: Default::default(),
                    lib: null_mut(),
                },
                verts: Vec::new(),
                edges: Vec::new(),
                loops: Vec::new(),
                faces: Vec::new(),

                flag: 0,

                rt: Default::default(),
            }
        }
    }
}

// ----------------------------------------------------------------------------
// Run-time

mod mesh_runtime {
    use ::core::edit_mesh::prelude::BMEditMesh;

    pub struct MeshRuntime {
        pub em: Option<Box<BMEditMesh>>,
    }

    impl Default for MeshRuntime {
        fn default() -> MeshRuntime {
            MeshRuntime {
                em: None,
            }
        }
    }
}

// For Exporting
pub use self::library::*;
pub use self::id::*;
pub use self::scene::*;
pub use self::object::*;
pub use self::mesh::*;

// Export impl:
pub use self::util_impl::{
    IDTypeImpl,
};

