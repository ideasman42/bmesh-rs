// Licensed: GPL v2+

/// All structs that can be serialized via DNA
///
/// Macro is to be used to pass into other macros, example use:
///
/// ```.text
/// macro_rules! print_structs {
///     ($($t:ty)*) => ($(
///         println!("type: {:?}", TypeId::of::<$t>());
///     )*)
/// }
/// dna_types_apply_pod!(print_structs);
/// ```
///

#[macro_export]
macro_rules! dna_types_apply_pod {
($macro_id:ident) => (
$macro_id! {

bool

i8 u8
i16 u16
i32 u32
i64 u64

isize usize

f32 f64

// TODO
// String (complex),
// char

})}

#[macro_export]
macro_rules! dna_types_apply_struct {
($macro_id:ident) => (
$macro_id! {

// keep sorted!
ID
Library
MEdge
MFace
MLoop
MVert
Mesh
Object
ObjectInst
ObjectLayer
Scene

})}


pub const DNA_TYPE_POD_NUM: usize = dna_types_apply_pod!(count_args_space);
pub const DNA_TYPE_STRUCT_NUM: usize = dna_types_apply_struct!(count_args_space);
pub const DNA_TYPE_TOTAL_NUM: usize =
    DNA_TYPE_POD_NUM +
    DNA_TYPE_STRUCT_NUM;
