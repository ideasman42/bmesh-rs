// Licensed: GPL v2+

///
/// Define 'DNAStruct' implementations for the 'io' module,
/// so DNA structs can be serialized and read back.
///
/// This could be in-line with the types but gets quite involved, so keep separate.
///

mod local_prelude {
    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
    pub use ::std::collections::BTreeSet;
    pub use super::super::super::consts::*;
    pub use ::list_base::{
        ListBase,
    };
    pub use math_misc::{
        unit_m4_ret,
    };
    pub use super::super::super::io::{
        DNAStruct,
        DNASpec,
        DNAStore,
        DNAStructMember,
        DNAStructMemberSizeData,

        DNATypeIndexImpl,
        DNATypeStructMembersImpl,
    };
    pub use ::core::dna::types::*;

    pub use super::type_utils::{
        size_of,
        dna_type_index,
    };
    pub use super::type_traits::*;
}

// ----------------------------------------------------------------------------
// Pointer Macros
//
// * '$t': type
// * '$m': type.member
// * '$b': temp 'base' instance of '$t'
//    (gets optimized out).
//

mod type_traits {
    pub use super::super::super::io::{
        DNATypeIndexImpl,
    };
    use ::list_base::{
        ListBase,
        ListBaseElemUtils,
    };
    use ::plain_ptr::{
        PtrMut,
        PtrConst,
    };

    pub trait DNATypeIndexOfBase {
        #[inline]
        fn type_index_of_base(&self) -> usize;
    }

    impl<T> DNATypeIndexOfBase for Vec<T>
        where T: DNATypeIndexImpl
    {
        #[inline]
        fn type_index_of_base(&self) -> usize {
            T::dna_type_index()
        }
    }

    impl<T> DNATypeIndexOfBase for ListBase<T>
        where T: ListBaseElemUtils + DNATypeIndexImpl
    {
        #[inline]
        fn type_index_of_base(&self) -> usize {
            T::dna_type_index()
        }
    }

    impl<T> DNATypeIndexOfBase for PtrMut<T>
        where T: DNATypeIndexImpl
    {
        #[inline]
        fn type_index_of_base(&self) -> usize {
            T::dna_type_index()
        }
    }

    impl<T> DNATypeIndexOfBase for PtrConst<T>
        where T: DNATypeIndexImpl
    {
        #[inline]
        fn type_index_of_base(&self) -> usize {
            T::dna_type_index()
        }
    }
}

mod type_utils {
    pub use super::super::super::io::{
        DNATypeIndexImpl,
    };

    #[inline]
    pub fn size_of<T: 'static + Sized>(_: &T) -> usize {
        ::std::mem::size_of::<T>()
    }
    #[inline]
    pub fn dna_type_index<T: DNATypeIndexImpl>(_: &T) -> usize {
        T::dna_type_index()
    }
}

/// 'std::mem' use here gets optimized out.
macro_rules! struct_member_expr {
    ($t:ty, $m:ident, $base:ident, $ex:block) => {
        {
            let $base: $t = unsafe { ::std::mem::uninitialized() };
            let result = $ex;
            ::std::mem::forget($base);
            result
        }
    }
}
macro_rules! size_of {
    ($t:ty, $m:ident) => {
        struct_member_expr!($t, $m, b, { size_of(&b.$m) })
    }
}
macro_rules! size_of_expr {
    ($t:ty, $m:ident, $base:ident, $expr:block) => {
        struct_member_expr!($t, $m, $base, { size_of( $expr ) })
    };
}

// ----------------------------------------------------------------------------
// DNA Declaration Macros

// just to get started
#[allow(unused_macros)]
macro_rules! dna_members_dummy {
    ($t:tt) => {
        impl DNAStructImpl for $t {
            fn members() -> Vec<DNAStructMember> {
                return Vec::new();
            }
        }
    }
}

macro_rules! dna_struct_impl {
    ($t:ty, $ret:ident, $code:block) => {
        impl DNATypeStructMembersImpl for $t {
            fn members() -> Vec<DNAStructMember> {
                return {
                    $code
                }
            }
        }
    }
}

macro_rules! dna_member {

    // all members shared between
    (@base $m:tt, $type_spec:expr, $type_store:expr) => ({
        // fill in all common fields
        DNAStructMember {
            dna_name: stringify!($m).to_string(),
            type_spec: $type_spec,
            type_store: $type_store,
            runtime: DNAStructMemberSizeData {
                size: size_of!(Self, $m),
                offset: unsafe { offset_of!(Self, $m) },
            },
            .. Default::default()
        }
    });

    // Calc data: doesn't create new structs
    (@calc_array_1d $m:tt, $d:ident) => ({
        $d.array_flat_len = size_of!(Self, $m) / size_of_expr!(Self, $m, b, { &b.$m[0] })
    });
    (@calc_array_2d $m:tt, $d:ident) => ({
        $d.array_flat_len = size_of!(Self, $m) / size_of_expr!(Self, $m, b, { &b.$m[0][0] })
    });
    (@calc_array_3d $m:tt, $d:ident) => ({
        $d.array_flat_len = size_of!(Self, $m) / size_of_expr!(Self, $m, b, { &b.$m[0][0][0] })
    });

    ($m:tt, Struct) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m) });
        d
    });
    ($m:tt, Array<Struct[]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m[0]) });
        dna_member!(@calc_array_1d $m, d);
        d
    });
    ($m:tt, Array<Struct[][]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m[0][0]) });
        dna_member!(@calc_array_2d $m, d);
        d
    });
    ($m:tt, StructContainer) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m.0) });
        d
    });
    ($m:tt, Array<StructContainer[]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m[0].0) });
        dna_member!(@calc_array_1d $m, d);
        d
    });
    ($m:tt, Array<StructContainer[][]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m[0][0].0) });
        dna_member!(@calc_array_2d $m, d);
        d
    });
    // note: we might have a vector of anything (Pointer, List... etc)
    ($m:tt, Vector<Struct[]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Vector);
        d.type_index = struct_member_expr!(Self, $m, b, { b.$m.type_index_of_base() });
        d
    });
    ($m:tt, Vector<Struct[][]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Struct, DNAStore::Vector);
        d.type_index = struct_member_expr!(Self, $m, b, { dna_type_index(&b.$m[0][0]) });
        d.type_is_vec = true;
        d
    });

    // Pod is basically the same as struct
    // the types just happen to be primitive, so no need to inspect members.
    //
    // Implementation is duplicate of 'Struct',
    // overwrite type_spec after to avoid a lot of duplication
    ($m:tt, Pod) => ({
        let mut d = dna_member!($m, Struct);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, Array<Pod[]>) => ({
        let mut d = dna_member!($m, Array<Struct[]>);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, Array<Pod[][]>) => ({
        let mut d = dna_member!($m, Array<Struct[][]>);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, PodContainer) => ({
        let mut d = dna_member!($m, StructContainer);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, Array<PodContainer[]>) => ({
        let mut d = dna_member!($m, Array<StructContainer[]>);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, Array<PodContainer[][]>) => ({
        let mut d = dna_member!($m, Array<StructContainer[][]>);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, Vector<Pod[]>) => ({
        let mut d = dna_member!($m, Vector<Struct[][]>);
        d.type_spec = DNASpec::Pod;
        d
    });
    ($m:tt, Vector<Pod[][]>) => ({
        let mut d = dna_member!($m, Vector<Struct[][]>);
        d.type_spec = DNASpec::Pod;
        d
    });

    ($m:tt, Pointer) => ({
        let mut d = dna_member!(@base $m, DNASpec::Pointer, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { b.$m.type_index_of_base() });
        d.serial.size = size_of_ptr!();
        d
    });
    ($m:tt, Array<Pointer[]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Pointer, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { b.$m[0].type_index_of_base() });
        dna_member!(@calc_array_1d $m, d);
        d.size = d.runtime_size;
        d
    });
    ($m:tt, Array<Pointer[][]>) => ({
        let mut d = dna_member!(@base $m, DNASpec::Pointer, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { b.$m[0][0].type_index_of_base() });
        dna_member!(@calc_array_2d $m, d);
        d.size = d.runtime_size;
        d
    });
    ($m:tt, List) => ({
        // Special case, a 'ListBase' is just 2x pointers,
        // so write this as an 'Array<Pointer[2]>', avoids complications.
        let mut d = dna_member!(@base $m, DNASpec::List, DNAStore::Inline);
        d.type_index = struct_member_expr!(Self, $m, b, { b.$m.type_index_of_base() });
        d
    });

}


// ----------------------------------------------------------------------------
// Struct definitions for serialization
//
// Use module layout matching 'mod.rs', for convenience.
//

mod library {
    use super::local_prelude::*;
    dna_struct_impl!(Library, ret, { vec![
        dna_member!(id, Struct),
        dna_member!(filepath, Array<Pod[]>),
    ]});
}

mod id {
    use super::local_prelude::*;
    dna_struct_impl!(ID, ret, { vec![
        dna_member!(next, Pointer),
        dna_member!(prev, Pointer),
        dna_member!(lib, Pointer),
        dna_member!(ty, Array<Pod[]>),
        dna_member!(name, Array<Pod[]>),
    ]});
}

mod scene {
    use super::local_prelude::*;
    dna_struct_impl!(Scene, ret, { vec![
        dna_member!(id, Struct),
        dna_member!(object_layers, List),
        dna_member!(object_layer_active, Pointer),
        dna_member!(object_inst_active, Pointer),
    ]});
    dna_struct_impl!(ObjectLayer, ret, { vec![
        dna_member!(next, Pointer),
        dna_member!(prev, Pointer),
        dna_member!(name, Array<Pod[]>),
        dna_member!(objects, List),
    ]});
    dna_struct_impl!(ObjectInst, ret, { vec![
        dna_member!(next, Pointer),
        dna_member!(prev, Pointer),
        dna_member!(object, Pointer),
        dna_member!(matrix, Array<Pod[][]>),
        dna_member!(flag, PodContainer),
    ]});
}

mod object {
    use super::local_prelude::*;
    dna_struct_impl!(Object, ret, { vec![
        dna_member!(id, Struct),
        dna_member!(data_ty, Array<Pod[]>),
        dna_member!(data, Pointer),
    ]});
}

mod mesh {
    use super::local_prelude::*;
    dna_struct_impl!(Mesh, ret, { vec![
        dna_member!(id, Struct),
        dna_member!(verts, Vector<Struct[]>),
        dna_member!(edges, Vector<Struct[]>),
        dna_member!(loops, Vector<Struct[]>),
        dna_member!(faces, Vector<Struct[]>),
    ]});
    dna_struct_impl!(MVert, ret, { vec![
        dna_member!(co, Array<Pod[]>),
        dna_member!(flag, Pod),
    ]});
    dna_struct_impl!(MEdge, ret, { vec![
        dna_member!(verts, Array<Pod[]>),
        dna_member!(flag, Pod),
    ]});
    dna_struct_impl!(MLoop, ret, { vec![
        dna_member!(v, Pod),
        dna_member!(e, Pod),
    ]});
    dna_struct_impl!(MFace, ret, { vec![
        dna_member!(loop_start, Pod),
        dna_member!(loop_len, Pod),
        dna_member!(flag, Pod),
    ]});
}

