// Licensed: GPL v2+
//
// Wisk library (low level DNA data management API).

// for now include entire hierarchy in this single module

mod local_prelude {
    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
    pub use ::core::dna::main_data::{
        MainData,
    };
}

pub mod scene {
    use super::local_prelude::*;
    pub use ::core::dna::types::{
        Scene,
        Object,
        ObjectInst,
        ObjectLayer,
    };

    pub fn add(main: &mut MainData, name: Option<&str>) -> PtrMut<Scene> {
        let mut scene = main.scene.add_default_named(name);

        let obj_lay = main.elem.alloc_from(
            ObjectLayer {
                .. Default::default()
            }
        );
        scene.object_layers.push_back(obj_lay);
        scene.object_layer_active = obj_lay;

        return scene;
    }

    pub fn object_link(
        main: &mut MainData,
        scene: PtrMut<Scene>,
        obj_lay: Option<PtrMut<ObjectLayer>>,
        obj: PtrMut<Object>,
    ) -> PtrMut<ObjectInst> {
        let mut obj_lay = obj_lay.unwrap_or(scene.object_layer_active);
        let obj_inst = main.elem.alloc_from(
            ObjectInst {
                object: obj,
                .. Default::default()
            }
        );
        obj_lay.objects.push_back(obj_inst);
        return obj_inst;
    }

    pub fn object_unlink(
        main: &mut MainData,
        mut scene: PtrMut<Scene>,
        mut obj_lay: PtrMut<ObjectLayer>,
        obj_inst: PtrMut<ObjectInst>,
    ) {
        assert!(scene.object_layers.contains(obj_lay));
        assert!(obj_lay.objects.contains(obj_inst));
        if obj_inst == scene.object_inst_active {
            scene.object_inst_active = null_mut();
        }
        obj_lay.objects.remove(obj_inst);
        main.elem.free(obj_inst);
    }
}

pub mod object {
    use super::local_prelude::*;
    pub use ::core::dna::types::{
        ID,
        Object,
    };

    pub fn add(
        main: &mut MainData,
        name: Option<&str>,
        data: PtrMut<ID>,
    ) -> PtrMut<Object> {
        let mut object = main.object.add_default_named(
            if name.is_none() && data != null_mut() {
                Some(data.name_get_str())
            } else {
                name
            }
        );
        object.data = data;
        if data != null_mut() {
            object.data_ty = data.ty;
        }
        return object;
    }
}

pub mod mesh {
    use super::local_prelude::*;
    pub use ::core::dna::types::{
        Mesh,
    };

    pub fn add(main: &mut MainData, name: Option<&str>) -> PtrMut<Mesh> {
        let mesh = main.mesh.add_default_named(name);
        return mesh;
    }
}
