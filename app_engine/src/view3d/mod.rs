// Licensed: GPL v2+
// (c) Blender Foundation

pub mod draw;
pub mod view;
pub mod project;

use math_misc::{
    unit_qt_ret,
    zero_m4_ret,
};

struct_enum_int_impl!(pub struct ViewPersp(pub u8));
impl ViewPersp {
    pub const ORTHO:  Self = ViewPersp(0_u8);
    pub const PERSP:  Self = ViewPersp(1_u8);
    pub const CAMERA: Self = ViewPersp(2_u8);
}

#[derive(Debug)]
pub struct ViewXForm {
    /// Global offset position.
    pub offset: [f64; 3],
    /// Distance from the offset.
    pub offset_dist: f64,
    /// View rotation (quaternion).
    pub rotation: [f64; 4],
}

impl Default for ViewXForm {
    fn default() -> Self {
        ViewXForm {
            offset: [0.0; 3],
            offset_dist: 0.0,
            rotation: unit_qt_ret(),
        }
    }
}

#[derive(Debug)]
pub struct ViewMatrix {
    /// GL_PROJECTION matrix
    pub window: [[f64; 4]; 4],
    /// GL_MODELVIEW matrix
    pub view: [[f64; 4]; 4],
    /// inverse of 'view'
    ///
    /// notes:
    /// * `view_inv[2]` is the view Z axis, (pointing towards the view).
    /// * for perspective projections `view_inv[3]` is the view origin.
    ///
    pub view_inv: [[f64; 4]; 4],
    /// 'view' * 'window'
    pub persp: [[f64; 4]; 4],
    /// inverse of 'persp'
    pub persp_inv: [[f64; 4]; 4],

    pub pixel_size: f64,
    pub is_persp: u8,
}

impl Default for ViewMatrix {
    fn default() -> Self {
        ViewMatrix {
            window: zero_m4_ret(),
            view: zero_m4_ret(),
            view_inv: zero_m4_ret(),
            persp: zero_m4_ret(),
            persp_inv: zero_m4_ret(),
            pixel_size: 0.0,
            is_persp: 0_u8,
        }
    }
}

pub struct RegionView3D {
    pub xform:  ViewXForm,
    pub matrix: ViewMatrix,

    /// 0 == ortho, 1 == perspective, 2 == camera
    pub persp: ViewPersp,

    pub is_persp: u8,
}

impl Default for RegionView3D {
    fn default() -> Self {
        RegionView3D {
            xform: ViewXForm::default(),
            matrix: ViewMatrix::default(),

            persp: ViewPersp::ORTHO,
            is_persp: 0_u8,
        }
    }
}

pub struct View3D {
    pub near: f64,
    pub far: f64,
    pub lens: f64,

}

impl Default for View3D {
    fn default() -> Self {
        View3D {
            near: 0.01,
            far: 1000.0,
            lens: 35.0,
        }
    }
}
