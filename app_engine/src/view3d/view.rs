// Licensed: GPL v2+
// (c) Blender Foundation

use math_misc::{
    perspective_m4,
    negated_v3,
    orthographic_m4,
    quat_to_mat4,
    translate_m4,
};

use wm::region::{
    ARegion,
};

use view3d;
use view3d::{
    RegionView3D,
    View3D,
};

use app::rect::RectF;

/// copies logic of get_view3d_viewplane(), keep in sync
///
/// was: ED_view3d_clip_range_get
///
pub fn clip_range_get(
    v3d: &View3D,
    rv3d: &RegionView3D,
    use_ortho_factor: bool,
) -> ([f64; 2], bool) {
    use camera::CameraParams;

    let mut params = CameraParams::default();

    params.from_view3d(v3d, rv3d);

    let clip_range = {
        if use_ortho_factor && params.is_ortho {
            let fac = 2.0 / (params.clipend - params.clipsta);
            [params.clipsta * fac, params.clipend * fac]
        } else {
            [params.clipsta, params.clipend]
        }
    };

    return (clip_range, params.is_ortho);
}

///
/// was: view3d_view.c: ED_view3d_viewplane_get
///
pub fn view_plane_get(
    v3d: &View3D,
    rv3d: &RegionView3D,
    window_size: &[u32; 2],
) -> (
    bool, RectF, f64, f64, f64,
) {
    use camera::CameraParams;

    let mut params = CameraParams::default();

    params.from_view3d(v3d, rv3d);
    params.compute_viewplane(window_size, &[1.0, 1.0]);

    return (
        params.is_ortho,
        params.viewplane,
        params.clipsta,
        params.clipend,
        params.viewdx,  // pixelsize
    );
}

// see: view3d_view.c: view3d_viewmatrix_set
pub fn view_matrix_set(rv3d: &mut RegionView3D) {
    rv3d.matrix.view = quat_to_mat4(&mut rv3d.xform.rotation);
    if rv3d.persp == view3d::ViewPersp::PERSP {
        rv3d.matrix.view[3][2] -= rv3d.xform.offset_dist;
    }
    translate_m4(&mut rv3d.matrix.view, &negated_v3(&rv3d.xform.offset));
}

// see: view3d_view.c: view3d_winmatrix_set
pub fn window_matrix_set(
    ar: &ARegion,
    rv3d: &mut RegionView3D,
    v3d: &View3D,
    rect: Option<&mut RectF>) {

    let (
        is_ortho,
        mut viewplane,
        clipsta,
        clipend,
        _pixel_size,
    ) = view_plane_get(v3d, rv3d, &ar.window_size);

    rv3d.is_persp = if is_ortho { 0 } else { 1 };

    if let Some(rect) = rect {
        // picking
        let x = ar.window_size[0] as f64;
        let y = ar.window_size[1] as f64;
        viewplane = RectF {
            xmin: viewplane.xmin + viewplane.size_x() * (rect.xmin / x),
            ymin: viewplane.ymin + viewplane.size_y() * (rect.ymin / y),
            xmax: viewplane.xmin + viewplane.size_x() * (rect.xmax / x),
            ymax: viewplane.ymin + viewplane.size_y() * (rect.ymax / y),
        };
    }

    if is_ortho {
        rv3d.matrix.window = orthographic_m4(
            viewplane.xmin, viewplane.xmax,
            viewplane.ymin, viewplane.ymax,
            clipsta, clipend,
        );
    } else {
        rv3d.matrix.window = perspective_m4(
            viewplane.xmin, viewplane.xmax,
            viewplane.ymin, viewplane.ymax,
            clipsta, clipend,
        );
    }
}
