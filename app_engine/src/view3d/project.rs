// Licensed: GPL v2+
// (c) Blender Foundation

use math_misc::{
    mul_m4v3_as_v4,
    mul_m4v3,
    mul_project_m4_v3_zfac,
    mul_project_m4_v3,
    normalized_sub_v3v3,
    normalized_v3,
    negated_v3,
};

use wm::region::{
    ARegion,
};

use view3d::{
    RegionView3D,
    ViewPersp,
};

struct_bitflag_impl!(pub struct ProjTestFlag(pub u8));
impl ProjTestFlag {
    pub const NOP:          Self = ProjTestFlag(0);
    pub const CLIP_BB:      Self = ProjTestFlag(1 << 0);
    pub const CLIP_WIN:     Self = ProjTestFlag(1 << 1);
    pub const CLIP_NEAR:    Self = ProjTestFlag(1 << 2);
    pub const CLIP_ZERO:    Self = ProjTestFlag(1 << 3);
}

// TODO, add a lot of checks - see: ed_view3d_project__internal

// When we have pre-calculated zfac, using 'mul_project_m4_v3_zfac'.
#[inline]
pub fn project_i32_zfac(
    ar: &ARegion, rv3d: &RegionView3D, co: &[f64; 3], co_zfac: f64, flag: ProjTestFlag,
) -> Option<[i32; 2]> {
    let vec3 = mul_m4v3(&rv3d.matrix.persp, &co);
    // caller needs to check zfac
    debug_assert!(co_zfac != 0.0);
    let scalar = 1.0 / co_zfac;
    let fx = (ar.window_size[0] as f64 / 2.0) * (1.0 + (vec3[0] * scalar));
    if (flag & ProjTestFlag::CLIP_WIN) == 0 || fx > 0.0 && fx < ar.window_size[0] as f64 {
        let fy = (ar.window_size[1] as f64 / 2.0) * (1.0 + (vec3[1] * scalar));
        if (flag & ProjTestFlag::CLIP_WIN) == 0 || fy > 0.0 && fy < ar.window_size[1] as f64 {
            return Some([
                fx as i32,
                fy as i32
            ]);
        }
    }

    return None;
}

#[inline]
pub fn project_i32(
    ar: &ARegion, rv3d: &RegionView3D, co: &[f64; 3], flag: ProjTestFlag,
) -> Option<[i32; 2]> {
    let vec4 = mul_m4v3_as_v4(&rv3d.matrix.persp, &co);
    let scalar = if vec4[3] != 0.0 { 1.0 / vec4[3] } else { 0.0 };
    let fx = (ar.window_size[0] as f64 / 2.0) * (1.0 + (vec4[0] * scalar));
    if (flag & ProjTestFlag::CLIP_WIN) == 0 || fx > 0.0 && fx < ar.window_size[0] as f64 {
        let fy = (ar.window_size[1] as f64 / 2.0) * (1.0 + (vec4[1] * scalar));
        if (flag & ProjTestFlag::CLIP_WIN) == 0 || fy > 0.0 && fy < ar.window_size[1] as f64 {
            return Some([
                fx as i32,
                fy as i32
            ]);
        }
    }

    return None;
}


// ----------------------------------------------------------------------------
// Note:
//
// Often we're converting from window coords multiple times
// if we're working in pixels its handy,
// but we may want to avoid having to do this in low-level functions.
//
// We could have versions of the 'win_*' functions that
// take normalized-device-coordinates: 'ndc_*'.

///
/// Calculate a 3d difference vector from 2d window offset.
/// note that ED_view3d_calc_zfac() must be called first to determine
/// the depth used to calculate the delta.
/// \param ar The region (used for the window width and height).
/// \param mval The area relative 2d difference (such as event->mval[0] - other_x).
/// \param out The resulting world-space delta.
///
/// Was: ED_view3d_win_to_delta
///
pub fn win_to_delta(
    rv3d: &RegionView3D, win_size: &[f64; 2], mval: [f64; 2], zfac: f64,
) -> [f64; 3]
{
    let dx = 2.0 * mval[0] * zfac / win_size[0];
    let dy = 2.0 * mval[1] * zfac / win_size[1];

    return [
        (rv3d.matrix.persp_inv[0][0] * dx + rv3d.matrix.persp_inv[1][0] * dy),
        (rv3d.matrix.persp_inv[0][1] * dx + rv3d.matrix.persp_inv[1][1] * dy),
        (rv3d.matrix.persp_inv[0][2] * dx + rv3d.matrix.persp_inv[1][2] * dy),
    ];
}

///
/// Calculate a 3d origin from 2d window coordinates.
/// \note Orthographic views have a less obvious origin,
/// Since far clip can be a very large value resulting in numeric precision issues,
/// the origin in this case is close to zero coordinate.
///
/// \param ar The region (used for the window width and height).
/// \param mval The area relative 2d location (such as event->mval converted to floats).
/// \return The resulting normalized world-space direction vector.
///
/// Was: ED_view3d_win_to_origin
///
pub fn win_to_origin(
    rv3d: &RegionView3D, &win_size: &[f64; 2], mval: [f64; 2],
) -> [f64; 3] {
    if rv3d.is_persp != 0 {
        return *as_v3!(&rv3d.matrix.view_inv[3]);
    } else {
        return mul_project_m4_v3(
            &rv3d.matrix.persp_inv,
            &[2.0 * mval[0] / win_size[0] - 1.0,
              2.0 * mval[1] / win_size[1] - 1.0,
              if rv3d.persp == ViewPersp::CAMERA { -1.0 } else { 0.0 }
            ],
        );
    }
}

///
/// Calculate a 3d direction vector from 2d window coordinates.
/// This direction vector starts and the view in the direction of the 2d window coordinates.
/// In orthographic view all window coordinates yield the same vector.
///
/// \note doesn't rely on ED_view3d_calc_zfac
/// for perspective view, get the vector direction to
/// the mouse cursor as a normalized vector.
///
/// \param ar The region (used for the window width and height).
/// \param mval The area relative 2d location (such as event->mval converted to floats).
/// \return The resulting normalized world-space direction vector.
///
/// Was: ED_view3d_win_to_vector
///
pub fn win_to_vector(
    rv3d: &RegionView3D, &win_size: &[f64; 2], mval: [f64; 2],
) -> [f64; 3] {

    if rv3d.is_persp != 0 {
        return normalized_sub_v3v3(
            &mul_project_m4_v3(
                &rv3d.matrix.persp_inv,
                &[2.0 * (mval[0] / win_size[0]) - 1.0,
                  2.0 * (mval[1] / win_size[1]) - 1.0,
                  -0.5],
            ),
            as_v3!(&rv3d.matrix.view_inv[3]),
        ).0;
    } else {
        return normalized_v3(&negated_v3(as_v3!(&rv3d.matrix.view_inv[2]))).0;
    }
}

///
/// Calculate a depth value from \a co, use with #win_to_delta
///
/// Was: ED_view3d_calc_zfac
///
pub fn calc_zfac_ex(
    rv3d: &RegionView3D, co: &[f64; 3],
    epsilon: f64, fallback: f64,
) -> (f64, bool) {
    let mut zfac = mul_project_m4_v3_zfac(&rv3d.matrix.persp, co);
    let is_flip = zfac < 0.0;

    // if x,y,z is exactly the viewport offset, zfac is 0 and we don't want that
    // (accounting for near zero values)
    //
    // Could make this configurable?
    if zfac.abs() < epsilon {
        zfac = fallback;
    }

    // Negative zfac means x, y, z was behind the camera (in perspective).
    // This gives flipped directions, so revert back to ok default case.
    if zfac < 0.0 {
        zfac = -zfac;
    }

    return (zfac, is_flip);
}

pub fn calc_zfac(
    rv3d: &RegionView3D, co: &[f64; 3],
) -> (f64, bool) {
    return calc_zfac_ex(rv3d, co, 1e-6, 1.0);
}
