// Licensed: GPL v2+
// (c) Blender Foundation

use math_misc::{
    inverted_m4,
    len_squared_v3,
    mul_m4m4,
    unit_m4_ret,
};

use wm::region::{
    ARegion,
};

use view3d;
use view3d::{
    RegionView3D,
    View3D,
};

// see: view3d_draw.c: ED_view3d_update_viewmat
pub fn update_view_matrix(
    v3d: &mut View3D,
    ar: &ARegion,
    rv3d: &mut RegionView3D,
    view_matrix: Option<&[[f64; 4]; 4]>,
    window_matrix: Option<&[[f64; 4]; 4]>,
) {
    // RegionView3D *rv3d = ar->regiondata;

    // setup window matrices
    if let Some(m) = window_matrix {
        rv3d.matrix.window = *m;
    } else {
        view3d::view::window_matrix_set(ar, rv3d, v3d, None);
    }

    // setup view matrix
    if let Some(m) = view_matrix {
        rv3d.matrix.view = *m;
    } else {
        // note: calls BKE_object_where_is_calc for camera...
        view3d::view::view_matrix_set(rv3d);

    }

    // update utility matrices
    rv3d.matrix.persp = mul_m4m4(&rv3d.matrix.window, &rv3d.matrix.view);
    rv3d.matrix.persp_inv = inverted_m4(&rv3d.matrix.persp).unwrap_or_else(|| unit_m4_ret());
    rv3d.matrix.view_inv = inverted_m4(&rv3d.matrix.view).unwrap_or_else(|| unit_m4_ret());

    // calculate GLSL view dependent values

    // store window coordinates scaling/offset
    /* TODO
    if (rv3d.persp == RV3D_CAMOB && v3d.camera) {
        rctf cameraborder;
        ED_view3d_calc_camera_border(scene, ar, v3d, rv3d, &cameraborder, false);
        rv3d.viewcamtexcofac[0] = (float)ar.winx / BLI_rctf_size_x(&cameraborder);
        rv3d.viewcamtexcofac[1] = (float)ar.winy / BLI_rctf_size_y(&cameraborder);

        rv3d.viewcamtexcofac[2] = -rv3d.viewcamtexcofac[0] * cameraborder.xmin / (float)ar.winx;
        rv3d.viewcamtexcofac[3] = -rv3d.viewcamtexcofac[1] * cameraborder.ymin / (float)ar.winy;
    }
    else {
        rv3d.viewcamtexcofac[0] = rv3d.viewcamtexcofac[1] = 1.0f;
        rv3d.viewcamtexcofac[2] = rv3d.viewcamtexcofac[3] = 0.0f;
    }
    */

    // calculate pixelsize factor once, is used for lamps and obcenters
    {
        // note:  '1.0f / len_v3(v1)'  replaced  'len_v3(rv3d.viewmat[0])'
        // because of float point precision problems at large values [#23908]
        let v1 = [
            rv3d.matrix.persp[0][0],
            rv3d.matrix.persp[1][0],
            rv3d.matrix.persp[2][0],
        ];
        let v2 = [
            rv3d.matrix.persp[0][1],
            rv3d.matrix.persp[1][1],
            rv3d.matrix.persp[2][1],
        ];


        let len_px = 2.0 / len_squared_v3(&v1).min(len_squared_v3(&v2)).sqrt();
        let len_sc = ::std::cmp::max(ar.window_size[0], ar.window_size[1]) as f64;

        rv3d.matrix.pixel_size = len_px / len_sc;
    }
}
