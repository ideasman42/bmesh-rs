// Licensed: GPL v2+

pub use ::prelude::*;
pub use ::core::dna::types::{
    Mesh,
};

pub fn object_mode_poll(ctx: AppContextConstP) -> bool {
    return ctx.editmesh().is_none();
}

pub fn object_active_mesh_poll(ctx: AppContextConstP) -> bool {
    if let Some(obj_inst) = ctx.object_inst() {
        if let Some(data) = obj_inst.object.data.as_option() {
            return data.downcast_ref::<Mesh>().is_some();
        }
    }
    return false;
}

pub fn edit_mesh_poll(ctx: AppContextConstP) -> bool {
    return !ctx.editmesh().is_none();
}
