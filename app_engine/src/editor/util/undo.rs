// Licensed: GPL v2+

pub use ::prelude::*;
pub use ::core::dna::types::{
    Mesh,
};
use plain_ptr::{
    PtrConst,
};
pub use ::wm::undo_manager::{
    UndoManagerType,
};

pub use app::{
    App,
    Env,
    UndoManagerTypesContainerImpl,
};


pub fn global_undo(ctx: AppContextConstP) -> PtrConst<UndoManagerType> {
    return PtrConst(ctx.app.system.undo_manager_types.find_by_id("global").unwrap());
}
