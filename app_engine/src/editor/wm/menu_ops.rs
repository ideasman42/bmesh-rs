// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
    pub use ::wm::event_system::{
        Event,
    };

    pub use ::wm::welem::{
        WElemAction,
    };

    pub use bmesh::prelude::*;
}

use self::local_prelude::*;

fn menu_poll(ctx: AppContextConstP) -> bool {
    return !ctx.menu().is_none();
}

mod wm_menu_call_op {
    use super::local_prelude::*;

    fn invoke(ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {
        let props = op.props();
        let menu_id = props.string_get("id").unwrap();
        if ::wm::menu::invoke_by_id(ctx, &*menu_id, event) {
            return OpState::FINISHED;
        } else {
            return OpState::PASS_THROUGH;
        }
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_string_builder("id", "Menu ID")
            .exec());

        OperatorType {
            id: "wm.menu_call".to_string(),
            name: "Menu Call".to_string(),
            invoke: Some(invoke),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

mod wm_menu_select_relative_op {
    use super::local_prelude::*;
    use super::{
        menu_poll,
    };

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {
        // use math_misc::ModuloSignedExt;

        let mut _menu = ctx.menu_mut().unwrap();

        /* XXXX
        let dir = op.props.get_int("dir").unwrap();

        menu.index = (menu.index + dir as i32).modulo(menu.items.len() as i32);
        */

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "wm.menu_select_relative".to_string(),
            name: "Menu Select Relative".to_string(),
            poll: Some(menu_poll),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}

mod wm_menu_active_execute_op {
    use super::local_prelude::*;
    use super::{
        menu_poll,
    };

    fn invoke(mut _ctx: AppContextMutP, _op: &mut Operator, _event: &Event) -> OpState {

        // let menu = ctx.menu_mut().unwrap();
        /* XXXX
        let id = menu.items[menu.index as usize].clone();

        let mut welem_parent = ctx.welem_parent_mut();
        welem_parent.action_queue.push(
            WElemAction::Operator {
                id: id,
                props: None,
                ctx_store: menu.ctx_store.clone(),
            }
        );

        use super::menu_queue_remove;
        menu_queue_remove(ctx);
        */

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "wm.menu_active_execute".to_string(),
            name: "Menu Selection Execute".to_string(),
            poll: Some(menu_poll),
            invoke: Some(invoke),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(wm_menu_call_op::new_type());
    app.system.operator_types.register(wm_menu_select_relative_op::new_type());
    app.system.operator_types.register(wm_menu_active_execute_op::new_type());
}
