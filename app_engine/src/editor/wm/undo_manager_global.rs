// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;

    pub use app::{
        App,
        Env,
        UndoManagerTypesContainerImpl,
    };
    pub use ::wm::undo_manager::{
        UndoManagerType,
        UndoStep,
    };

    pub use ::plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
    pub use ::block_array_cow::{BArrayStore, BArrayState};
    pub use ::std::any::Any;
    pub use ::std::rc::Rc;
    pub use ::std::cell::RefCell;
}

use self::local_prelude::*;

/// Undo manager that operates on a file level.
mod undo_manager_global {
    use super::local_prelude::*;

    struct UndoStepGlobal {
        // file_data: Vec<u8>,
        file_data: *mut BArrayState,
        bstore: Rc<RefCell<BArrayStore>>,
    }

    fn step_push(
        app: PtrConst<App>,
        env: PtrMut<Env>,
        steps_prev: &mut [UndoStep],
        mgr: PtrConst<UndoManagerType>,
        name: &str,
    ) -> UndoStep {
        let app = app.clone();

        let mut file_data: Vec<u8> = Vec::new();
        ::core::dna::io::main_data::encode_main::encode_main(
            &mut file_data,
            &env.main,
            &app.system.dna_info,
        ).unwrap();

        // find previous undo step of matching type
        let mut step_data_prev: Option<&UndoStepGlobal> = None;
        for s in steps_prev.iter().rev() {
            step_data_prev = s.data.downcast_ref::<UndoStepGlobal>();
            if !step_data_prev.is_none() {
                break;
            }
        }

        let (bstore, bstate): (Rc<RefCell<BArrayStore>>, Option<*const BArrayState>) = {
            if let Some(ref step_data_prev_ref) = step_data_prev {
                // shallow clone
                (step_data_prev_ref.bstore.clone(), Some(step_data_prev_ref.file_data))
            } else {
                (Rc::new(RefCell::new(BArrayStore::new(1, 128))), None)
            }
        };

        let file_data = bstore.borrow_mut().state_add(&file_data[..], bstate);

        UndoStep {
            name: name.to_string(),

            data: Box::new(UndoStepGlobal {
                file_data: file_data,
                bstore: bstore,
            }),
            ty: mgr,
        }
    }

    fn step_pop(
        app: PtrConst<App>,
        mut env: PtrMut<Env>,
        step: PtrConst<UndoStep>,
    ) -> bool {
        let app = app.clone();
        let step_data = step.data.downcast_ref::<UndoStepGlobal>().unwrap();

        // create file data and throw it away
        let file_data = {
            BArrayStore::state_data_get_alloc(step_data.file_data)
            // step_data.bstore.into_inner().state_data_get_alloc(step_data.bstate)
        };

        let mut main = Default::default();
        ::core::dna::io::main_data::decode_main::decode_main(
            // &step_data.file_data[..],
            &file_data[..],
            &mut main,
            &app.system.dna_info,
        ).unwrap();

        env.main = main;
        env.scene = env.main.scene.head_mut();

        drop(file_data);

        return true;
    }

    pub fn new_type() -> UndoManagerType {
        UndoManagerType {
            id: "global".to_string(),
            step_push: step_push,
            step_pop: step_pop,
        }
    }
}

pub fn register_undo_manager(app: &mut App) {
    app.system.undo_manager_types.register(undo_manager_global::new_type());
}
