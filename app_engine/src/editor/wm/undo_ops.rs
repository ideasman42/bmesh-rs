// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
}

use self::local_prelude::*;

mod undo_pop_op {
    use super::local_prelude::*;
    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {
        let app = ctx.app.clone();
        ctx.env.undo_pop(app);
        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "wm.undo_pop".to_string(),
            name: "Undo Pop".to_string(),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}

mod undo_redo_op {
    use super::local_prelude::*;
    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {
        let app = ctx.app.clone();
        ctx.env.undo_redo(app);
        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "wm.undo_redo".to_string(),
            name: "Undo Redo".to_string(),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(undo_pop_op::new_type());
    app.system.operator_types.register(undo_redo_op::new_type());
}

