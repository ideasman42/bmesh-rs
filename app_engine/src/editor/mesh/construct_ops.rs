// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use bmesh::prelude::*;
    pub use ::editor::util::poll::{
        edit_mesh_poll,
    };
}

use self::local_prelude::*;

// -------------
// Add Edge/Face

mod mesh_edge_face_add {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {
        use bmesh;

        let mut bm = ctx.bmesh_mut().unwrap();

        bmesh::ops::edge_face_add(&mut *bm)
            .verts(|v| { v.is_select() })
            .edges_out(|mut e| {
                e.select_set_noflush(true);
            })
            .faces_out(|mut f| {
                f.hide_set(false);
                f.select_set_noflush(true);
            })
            .exec();

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "mesh.edge_face_add".to_string(),
            name: "Mesh Edge/Face Add".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(mesh_edge_face_add::new_type());
}
