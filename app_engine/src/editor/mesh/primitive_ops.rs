// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };

    pub use bmesh::prelude::*;

    pub use bmesh::primitives::{
        BMeshPartialGeom,
    };


}

use self::local_prelude::*;

fn edit_mesh_or_can_add_edit_mesh_poll(ctx: AppContextConstP) -> bool {
    if ctx.env.scene.id.lib != null_mut() {
        return false;
    }
    return true;
}


macro_rules! bmesh_primitive_add_and_replace_selection {
    ($bm:ident, $geom:ident, $code:block) => {
        $bm.elem_hflag_edit_builder(BM_ELEM_SELECT)
            .set_vert_from_value(false)
            .set_edge_from_value(false)
            .set_face_from_value(false)
            .exec();
        {
            $code
        }
        $geom.elem_hflag_edit_builder(BM_ELEM_SELECT)
            .set_vert_from_value(true)
            .set_edge_from_value(true)
            .set_face_from_value(true)
            .exec();
    }
}

use ::core::edit_mesh::prelude::BMEditMesh;
fn editmesh_get_or_add(mut ctx: AppContextMutP, name: &str) -> PtrMut<BMEditMesh> {
    if let Some(em) = ctx.editmesh_mut() {
        return em;
    } else {
        let scene = ctx.clone().env.scene;
        use ::core::wli;
        let mut mesh = wli::mesh::add(&mut ctx.env.main, Some(name));
        let obj = wli::object::add(&mut ctx.env.main, None, PtrMut(&mut mesh.id));
        let mut obj_inst = wli::scene::object_link(&mut ctx.env.main, scene, None, obj);
        obj_inst.flag.select_enable();
        ctx.env.scene.object_inst_active = obj_inst;
        ::wm::operator::call_by_id(
            ctx, "object.edit_mode_set", ::wm::operator::OpContext::ExecDefault, None);
        return ctx.editmesh_mut().unwrap();
    }
}

// ---------
// Add Plane

mod mesh_primirive_plane_add {
    use super::local_prelude::*;
    use super::{
        edit_mesh_or_can_add_edit_mesh_poll,
        editmesh_get_or_add,
    };

    fn exec(ctx: AppContextMutP, op: &mut Operator) -> OpState {
        let mut em = editmesh_get_or_add(ctx, "Plane");
        let mut bm = &mut em.bm;

        let props = op.props();

        let radius = props.float_get("radius");

        let mut geom = BMeshPartialGeom::default();
        bmesh_primitive_add_and_replace_selection!(bm, geom, {
            ::bmesh::primitives::plane(&mut bm, radius, &mut geom);
        });
        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_float_builder("radius", "Radius")
            .default(1.0)
            .exec());

        OperatorType {
            id: "mesh.primitive_plane_add".to_string(),
            name: "Mesh Add Plane".to_string(),
            poll: Some(edit_mesh_or_can_add_edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

// --------
// Add Cube

mod mesh_primirive_cube_add {
    use super::local_prelude::*;
    use super::{
        edit_mesh_or_can_add_edit_mesh_poll,
        editmesh_get_or_add,
    };

    fn exec(ctx: AppContextMutP, op: &mut Operator) -> OpState {
        let mut em = editmesh_get_or_add(ctx, "Cube");
        let mut bm = &mut em.bm;

        let props = op.props();

        let radius = props.float_get("radius");

        let mut geom = BMeshPartialGeom::default();
        bmesh_primitive_add_and_replace_selection!(bm, geom, {
            ::bmesh::primitives::cube(&mut bm, radius, &mut geom);
        });
        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_float_builder("radius", "Radius")
            .default(1.0)
            .exec());

        OperatorType {
            id: "mesh.primitive_cube_add".to_string(),
            name: "Mesh Add Cube".to_string(),
            poll: Some(edit_mesh_or_can_add_edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

// ---------
// Add Torus

mod mesh_primirive_torus_add {
    use super::local_prelude::*;
    use super::{
        edit_mesh_or_can_add_edit_mesh_poll,
        editmesh_get_or_add,
    };

    fn exec(ctx: AppContextMutP, op: &mut Operator) -> OpState {
        let mut em = editmesh_get_or_add(ctx, "Torus");
        let mut bm = &mut em.bm;

        let props = op.props();

        let major_rad = props.float_get("major_rad");
        let minor_rad = props.float_get("minor_rad");
        let major_seg = props.int_get("major_seq") as usize;
        let minor_seg = props.int_get("minor_seq") as usize;

        let mut geom = BMeshPartialGeom::default();
        bmesh_primitive_add_and_replace_selection!(bm, geom, {
            ::bmesh::primitives::torus(
                &mut bm,
                major_rad, minor_rad,
                major_seg, minor_seg,
                &mut geom,
            );
        });
        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_float_builder("major_rad", "Major Radius")
            .default(1.0)
            .exec());
        srna.properties.insert(
            PropertyRNA::new_float_builder("minor_rad", "Minor Radius")
            .default(0.25)
            .exec());
        srna.properties.insert(
            PropertyRNA::new_int_builder("major_seq", "Major Segments")
            .default(48)
            .exec());
        srna.properties.insert(
            PropertyRNA::new_int_builder("minor_seq", "Minor Segments")
            .default(12)
            .exec());

        OperatorType {
            id: "mesh.primitive_torus_add".to_string(),
            name: "Mesh Add Torus".to_string(),
            poll: Some(edit_mesh_or_can_add_edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(mesh_primirive_plane_add::new_type());
    app.system.operator_types.register(mesh_primirive_cube_add::new_type());
    app.system.operator_types.register(mesh_primirive_torus_add::new_type());
}
