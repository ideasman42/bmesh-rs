// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
    pub use bmesh::prelude::*;
    pub use ::editor::util::poll::{
        edit_mesh_poll,
    };
}

use self::local_prelude::*;

// ----
// Hide

mod mesh_hide_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, op: &mut Operator) -> OpState {

        let props = op.props();

        let mut bm = ctx.bmesh_mut().unwrap();

        let use_select = props.bool_get("use_select");

        if (bm.select_mode & BM_VERT) != 0 {
            for mut v in bm.verts.iter_mut() {
                if v.is_select() == use_select {
                    v.hide_set_noflush(true);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_HIDDEN)
                .add_edge_from_vert_any()
                .add_face_from_vert_any()
                .exec();
        } else if (bm.select_mode & BM_EDGE) != 0 {
            for mut e in bm.edges.iter_mut() {
                if e.is_select() == use_select {
                    e.hide_set_noflush(true);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_HIDDEN)
                .add_vert_from_edge_all()
                .add_face_from_edge_any()
                .exec();
        } else {
            for mut f in bm.faces.iter_mut() {
                if f.is_select() == use_select {
                    f.hide_set_noflush(true);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_HIDDEN)
                .add_vert_from_edge_all()
                .add_edge_from_face_all()
                .exec();
        }

        if use_select {
            bm.elem_hflag_edit_builder(BM_ELEM_SELECT)
                .set_vert_from_value(false)
                .set_edge_from_value(false)
                .set_face_from_value(false)
                .exec();
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_bool_builder("use_select", "Select")
            .default(true)
            .exec());

        OperatorType {
            id: "mesh.hide".to_string(),
            name: "Mesh Hide".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

// ------
// Reveal

mod mesh_reveal_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {

        let mut bm = ctx.bmesh_mut().unwrap();

        if (bm.select_mode & BM_VERT) != 0 {
            for mut v in bm.verts.iter_mut() {
                if v.is_hide() {
                    v.hide_set_noflush(false);
                    v.select_set(true);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_HIDDEN)
                .sub_edge_from_vert_any()
                .sub_face_from_vert_any()
                .exec();
            bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
                .set_edge_from_vert_all()
                .set_face_from_vert_all()
                .exec();
        } else if (bm.select_mode & BM_EDGE) != 0 {
            for mut e in bm.edges.iter_mut() {
                if e.is_hide() {
                    e.hide_set_noflush(false);
                    e.select_set_noflush(true);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_HIDDEN)
                .sub_vert_from_edge_any()
                .sub_face_from_edge_any()
                .exec();
            bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
                .set_vert_from_edge_any()
                .set_face_from_edge_all()
                .exec();
        } else {
            for mut f in bm.faces.iter_mut() {
                if f.is_hide() {
                    f.hide_set_noflush(false);
                    f.select_set_noflush(true);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_HIDDEN)
                .sub_vert_from_face_any()
                .sub_edge_from_face_any()
                .exec();
            bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
                .set_vert_from_face_any()
                .set_edge_from_face_any()
                .exec();
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "mesh.reveal".to_string(),
            name: "Mesh Reveal".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(mesh_hide_op::new_type());
    app.system.operator_types.register(mesh_reveal_op::new_type());
}
