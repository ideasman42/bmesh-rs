// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
        EnumItem,
    };

    pub use bmesh::prelude::*;
    pub use ::editor::util::poll::{
        edit_mesh_poll,
    };
}

use self::local_prelude::*;

// -----------
// Select Mode

mod mesh_select_mode_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, op: &mut Operator) -> OpState {

        let props = op.props();

        let mut em = ctx.editmesh_mut().unwrap();

        let mode_new = BMElemType(props.enum_get("mode") as _) &
            (BM_VERT | BM_EDGE | BM_FACE);

        if mode_new != 0 {
            let mode_old = em.bm.select_mode;
            em.select_mode_set(mode_old, mode_new);
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        use ::core::rna::enum_common::enum_mesh_elem_noloop;

        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_enum_builder("mode", "Mode")
            .items(enum_mesh_elem_noloop::items())
            .exec());

        OperatorType {
            id: "mesh.select_mode".to_string(),
            name: "Mesh Select Mode".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

// ----------
// Select All

mod mesh_select_all_op {
    use super::local_prelude::*;
    use ::core::rna::enum_common::enum_select_all;

    fn exec(mut ctx: AppContextMutP, op: &mut Operator) -> OpState {

        let props = op.props();

        let mut bm = ctx.bmesh_mut().unwrap();

        let mut mode = props.enum_get("mode");

        if mode == enum_select_all::Enum::Toggle {
            let mut is_select = false;
            for v in bm.verts.iter_mut() {
                if v.is_select() {
                    is_select = true;
                    break;
                }
            }
            if is_select {
                mode = enum_select_all::Enum::DeSelect as _;
            } else {
                mode = enum_select_all::Enum::Select as _;
            }
        }

        for mut v in bm.verts.iter_mut() {
            if !v.is_hide() {
                v.select_set(mode == enum_select_all::Enum::Select);
            }
        }

        bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
            .skip(BM_ELEM_HIDDEN)
            .set_edge_from_vert_all()
            .set_face_from_vert_all()
            .exec();

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {

        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_enum_builder("mode", "Mode")
            .items(enum_select_all::items())
            .exec());

        OperatorType {
            id: "mesh.select_all".to_string(),
            name: "Mesh Select All".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

// -------------
// Select Invert

mod mesh_select_invert_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {

        let mut bm = ctx.bmesh_mut().unwrap();

        if (bm.select_mode & BM_VERT) != 0 {
            for mut v in bm.verts.iter_mut() {
                if !v.is_hide() {
                    let is_select = v.is_select();
                    v.select_set(!is_select);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
                .set_edge_from_vert_all()
                .set_face_from_vert_all()
                .exec();
        } else if (bm.select_mode & BM_EDGE) != 0 {
            for mut e in bm.edges.iter_mut() {
                if !e.is_hide() {
                    let is_select = e.is_select();
                    e.select_set_noflush(!is_select);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
                .set_vert_from_edge_any()
                .set_face_from_edge_all()
                .exec();
        } else {
            for mut f in bm.faces.iter_mut() {
                if !f.is_hide() {
                    let is_select = f.is_select();
                    f.select_set_noflush(!is_select);
                }
            }
            bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
                .set_vert_from_face_any()
                .set_edge_from_face_any()
                .exec();
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "mesh.select_invert".to_string(),
            name: "Mesh Select Invert".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(mesh_select_mode_op::new_type());
    app.system.operator_types.register(mesh_select_all_op::new_type());
    app.system.operator_types.register(mesh_select_invert_op::new_type());
}

