// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::menu::{
        Menu,
        MenuType,
    };

    pub use ::ui::uielem::{
        UIElemLayoutPushButtonOpImpl,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PointerRNA,
    };
    pub use wm::operator::{
        OpAction,
    };
}

use self::local_prelude::*;


// currently just meshes
mod object_add_mt {
    use super::local_prelude::*;

    fn ui(ctx: AppContextConstP, menu: &mut Menu) {
        let layout = menu.layout_mut();
        layout.operator(OpAction::from_id(ctx.app, "mesh.primitive_plane_add")).exec(ctx);
        layout.operator(OpAction::from_id(ctx.app, "mesh.primitive_cube_add")).exec(ctx);
        layout.operator(OpAction::from_id(ctx.app, "mesh.primitive_torus_add")).exec(ctx);
    }

    pub fn new_type() -> MenuType {
        MenuType {
            id: "object.add".to_string(),
            name: "Object Add".to_string(),
            poll: None,
            ui: ui,
        }
    }
}

mod mesh_primitive_add_mt {
    use super::local_prelude::*;

    fn ui(ctx: AppContextConstP, menu: &mut Menu) {
        let layout = menu.layout_mut();
        layout.operator(OpAction::from_id(ctx.app, "mesh.primitive_plane_add")).exec(ctx);
        layout.operator(OpAction::from_id(ctx.app, "mesh.primitive_cube_add")).exec(ctx);
        layout.operator(OpAction::from_id(ctx.app, "mesh.primitive_torus_add")).exec(ctx);
    }

    pub fn new_type() -> MenuType {
        MenuType {
            id: "edit_mesh.primitive_add".to_string(),
            name: "Mesh Add".to_string(),
            poll: None,
            ui: ui,
        }
    }
}

mod mesh_specials_mt {
    use super::local_prelude::*;

    fn ui(ctx: AppContextConstP, menu: &mut Menu) {
        let layout = menu.layout_mut();
        layout.operator(OpAction::from_id(ctx.app, "mesh.select_invert")).exec(ctx);
        layout.operator(OpAction::from_id(ctx.app, "mesh.select_all"))
            .text("Select All Toggle")
            .exec(ctx);
    }

    pub fn new_type() -> MenuType {
        MenuType {
            id: "edit_mesh.specials".to_string(),
            name: "Mesh Specials".to_string(),
            poll: None,
            ui: ui,
        }
    }
}

mod mesh_delete_mt {
    use super::local_prelude::*;

    fn ui(ctx: AppContextConstP, menu: &mut Menu) {
        let layout = menu.layout_mut();
        let op_delete = OpAction::from_id(ctx.app, "mesh.delete");
        {
            let mut op_act = op_delete.clone();
            let mut props = op_act.props();
            props.bool_set("use_vert", true);
            layout.operator(op_act)
                .text("Verts")
                .exec(ctx);
        }
        {
            let mut op_act = op_delete.clone();
            let mut props = op_act.props();
            props.bool_set("use_vert", true);
            props.bool_set("use_edge", true);
            layout.operator(op_act)
                .text("Edges")
                .exec(ctx);
        }
        {
            let mut op_act = op_delete.clone();
            let mut props = op_act.props();
            props.bool_set("use_vert", true);
            props.bool_set("use_edge", true);
            props.bool_set("use_face", true);
            layout.operator(op_act)
                .text("Faces")
                .exec(ctx);
        }
        {
            let mut op_act = op_delete.clone();
            let mut props = op_act.props();
            props.bool_set("use_edge", true);
            layout.operator(op_act)
                .text("Only Edges")
                .exec(ctx);
        }
        {
            let mut op_act = op_delete.clone();
            let mut props = op_act.props();
            props.bool_set("use_face", true);
            layout.operator(op_act)
                .text("Only Faces")
                .exec(ctx);
        }
    }

    pub fn new_type() -> MenuType {
        MenuType {
            id: "edit_mesh.delete".to_string(),
            name: "Mesh Delete".to_string(),
            poll: None,
            ui: ui,
        }
    }
}

mod mesh_select_mode_mt {
    use super::local_prelude::*;

    fn ui(ctx: AppContextConstP, menu: &mut Menu) {
        let layout = menu.layout_mut();
        let op_select_mode = OpAction::from_id(ctx.app, "mesh.select_mode");

        // A bit verbose, would eventually automatically generate this list from the enum
        {
            let mut op_act = op_select_mode.clone();
            let mut props = op_act.props();
            props.enum_set_id("mode", "VERT");
            layout.operator(op_act)
                .text("Vertex")
                .exec(ctx);
        }
        {
            let mut op_act = op_select_mode.clone();
            let mut props = op_act.props();
            props.enum_set_id("mode", "EDGE");
            layout.operator(op_act)
                .text("Edge")
                .exec(ctx);
        }
        {
            let mut op_act = op_select_mode.clone();
            let mut props = op_act.props();
            props.enum_set_id("mode", "FACE");
            layout.operator(op_act)
                .text("Face")
                .exec(ctx);
        }
    }

    pub fn new_type() -> MenuType {
        MenuType {
            id: "edit_mesh.select_mode".to_string(),
            name: "Mesh Mode".to_string(),
            poll: None,
            ui: ui,
        }
    }
}

pub fn register_menus(app: &mut App) {
    app.system.menu_types.register(object_add_mt::new_type());
    app.system.menu_types.register(mesh_primitive_add_mt::new_type());
    app.system.menu_types.register(mesh_specials_mt::new_type());
    app.system.menu_types.register(mesh_delete_mt::new_type());
    app.system.menu_types.register(mesh_select_mode_mt::new_type());
}
