// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };

    pub use bmesh::prelude::*;
    pub use ::editor::util::poll::{
        edit_mesh_poll,
    };
}

use self::local_prelude::*;

// ------
// Delete

mod mesh_delete_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, op: &mut Operator) -> OpState {

        let props = op.props();

        use bmesh;

        let mut bm = ctx.bmesh_mut().unwrap();

        let use_vert = props.bool_get("use_vert");
        let use_edge = props.bool_get("use_edge");
        let use_face = props.bool_get("use_face");

        {
            let mut del = bmesh::ops::delete(&mut *bm);

            if use_face {
                // deselect surrounding before removal
                let face_callback = |f: BMFaceMutP|{
                    if f.is_select() {
                        bm_iter_loops_of_face_cycle_mut!(f, mut l_iter, {
                            l_iter.v.select_set(false);
                            l_iter.e.select_set_noflush(false);
                        });
                        true
                    } else {
                        false
                    }
                };
                if use_vert && use_edge {
                    del = del.faces_loose(face_callback);
                } else {
                    del = del.faces(face_callback);
                }
            } else if use_edge {
                // deselect surrounding before removal
                let edge_callback = |mut e: BMEdgeMutP|{
                    if e.is_select() {
                        for v in &mut e.verts {
                            v.select_set(false);
                        }
                        true
                    } else {
                        false
                    }
                };
                if use_vert {
                    del = del.edges_loose(edge_callback);
                } else {
                    del = del.edges(edge_callback);
                }
            } else if use_vert {
                del = del.verts(|v|{ v.is_select() });
            }

            del.exec();
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_bool_builder("use_vert", "Use Vertices")
            .exec());
        srna.properties.insert(
            PropertyRNA::new_bool_builder("use_edge", "Use Edges")
            .exec());
        srna.properties.insert(
            PropertyRNA::new_bool_builder("use_face", "Use Faces")
            .exec());

        OperatorType {
            id: "mesh.delete".to_string(),
            name: "Mesh Delete".to_string(),
            poll: Some(edit_mesh_poll),
            exec: Some(exec),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(mesh_delete_op::new_type());
}

