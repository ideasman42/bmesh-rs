// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
    pub use ::core::dna::types::{
        Mesh,
    };
    pub use ::core::edit_mesh::prelude::BMEditMesh;

    pub use bmesh::prelude::*;
    pub use ::editor::util::poll::{
        object_active_mesh_poll,
    };
}

use self::local_prelude::*;


// ----------------------
// Object EditMode Toggle

mod object_edit_mode_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {

        let mut obj_inst = ctx.object_inst_mut().unwrap();
        let mut mesh = obj_inst.object.data.downcast_mut::<Mesh>().unwrap();

        if let Some(ref mut em) = mesh.clone().rt.em {
            ::core::edit_mesh::convert::bmesh_to_mesh(PtrMut(&mut em.bm), mesh);
            mesh.rt.em = None;
        } else {
            let mut bm = BMesh::new();
            ::core::edit_mesh::convert::bmesh_from_mesh(PtrMut(&mut bm), mesh);
            mesh.rt.em = Some(Box::new(BMEditMesh::new(bm)));
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        /*
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_enum_builder("mode", "Mode")
            .exec());
        */
        OperatorType {
            id: "object.edit_mode_set".to_string(),
            name: "Object Edit Mode".to_string(),
            poll: Some(object_active_mesh_poll),
            exec: Some(exec),
            // srna: srna,
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(object_edit_mode_op::new_type());
}
