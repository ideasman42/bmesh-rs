// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
    pub use ::core::dna::prelude::*;

    pub use ::editor::util::poll::{
        object_mode_poll,
    };
}

use self::local_prelude::*;

// ----------
// Select All

mod object_select_all_op {
    use super::local_prelude::*;
    use ::core::rna::enum_common::enum_select_all;

    fn exec(ctx: AppContextMutP, op: &mut Operator) -> OpState {

        let props = op.props();

        let mut mode = props.enum_get("mode");

        let mut scene = ctx.env.scene;

        if mode == enum_select_all::Enum::Toggle {
            let mut is_select = false;
            for obj_lay in scene.object_layers.iter() {
                for obj_inst in obj_lay.objects.iter() {
                    if obj_inst.flag.select_test() {
                        is_select = true;
                        break;
                    }
                }
            }
            if is_select {
                mode = enum_select_all::Enum::DeSelect as _;
            } else {
                mode = enum_select_all::Enum::Select as _;
            }
        }

        {
            let select = mode == enum_select_all::Enum::Select;
            for mut obj_lay in scene.object_layers.iter_mut() {
                for mut obj_inst in obj_lay.objects.iter_mut() {
                    obj_inst.flag.select_set(select);
                }
            }
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_enum_builder("mode", "Mode")
            .items(enum_select_all::items())
            .exec());

        OperatorType {
            id: "object.select_all".to_string(),
            name: "Object Select All".to_string(),
            poll: Some(object_mode_poll),
            exec: Some(exec),
            undo: Some(::editor::util::undo::global_undo),
            srna: srna,
            .. OperatorType::default()
        }
    }
}

mod object_select_invert_op {
    use super::local_prelude::*;
    use super::{
        object_mode_poll
    };

    fn exec(ctx: AppContextMutP, _op: &mut Operator) -> OpState {

        let mut scene = ctx.env.scene;

        {
            for mut obj_lay in scene.object_layers.iter_mut() {
                for mut obj_inst in obj_lay.objects.iter_mut() {
                    obj_inst.flag.select_toggle();
                }
            }
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "object.select_invert".to_string(),
            name: "Object Select Invert".to_string(),
            poll: Some(object_mode_poll),
            exec: Some(exec),
            undo: Some(::editor::util::undo::global_undo),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(object_select_all_op::new_type());
    app.system.operator_types.register(object_select_invert_op::new_type());
}
