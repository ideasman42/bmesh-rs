// Licensed: GPL v2+

mod local_prelude {
    pub use ::prelude::*;
    pub use ::plain_ptr::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
    pub use ::core::dna::prelude::*;

    pub use ::editor::util::poll::{
        object_mode_poll,
    };
}

use self::local_prelude::*;

mod object_delete_op {
    use super::local_prelude::*;

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {
        use core::wli;

        let scene = ctx.clone().env.scene;

        for obj_lay in scene.clone().object_layers.iter_mut() {
            for obj_inst in obj_lay.clone().objects.iter_mut() {
                if obj_inst.flag.select_test() {
                    wli::scene::object_unlink(&mut ctx.env.main, scene, obj_lay, obj_inst);
                }
            }
        }
        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "object.delete".to_string(),
            name: "Object Delete".to_string(),
            poll: Some(object_mode_poll),
            exec: Some(exec),
            undo: Some(::editor::util::undo::global_undo),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(object_delete_op::new_type());
}
