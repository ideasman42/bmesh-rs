// Licensed: GPL v2+

// TODO:
// object_wire()

use ::core::dna::types::{
    Scene,
    ObjectLayer,
    ObjectInst,
};
use super::super::pick_ray::{
    View3DPickRayParams,
};
use ::plain_ptr::{
    PtrMut,
    PtrConst,
    null_const,
};

use math_misc::*;

pub fn object_center(
    scene: &Scene,
    pick_params: &View3DPickRayParams,
) -> Option<(PtrConst<ObjectLayer>, PtrConst<ObjectInst>)> {
    let pick_origin_proj = pick_params.proj_origin_calc();
    let view_range_offset = pick_params.view_range_offset_calc();

    let mut obj_lay_best: PtrConst<ObjectLayer> = null_const();
    let mut obj_inst_best: PtrConst<ObjectInst> = null_const();
    let mut best_len_sq = -1.0;


    for obj_lay in scene.object_layers.iter() {
        for obj_inst in obj_lay.objects.iter() {
            let co = as_v3!(&obj_inst.matrix[3]);
            if {
                let d = dot_v3v3(&pick_params.view_vector, &co);
                {
                    d < view_range_offset[0] ||
                    d > view_range_offset[1]
                }
            } {
                continue;
            }

            let co_proj = project_plane_v3v3(co, &pick_params.pick_vector);
            let test_len_sq = len_squared_v3v3(&pick_origin_proj, &co_proj);
            if obj_inst_best == null_const() ||
               test_len_sq < best_len_sq
            {
                obj_lay_best = obj_lay.clone();
                obj_inst_best = obj_inst;
                best_len_sq = test_len_sq;
            }
        }
    }

    if obj_inst_best != null_const() {
        return Some((obj_lay_best, obj_inst_best));
    } else {
        return None;
    }
}

pub fn object_center_mut(
    scene: &mut Scene,
    pick_params: &View3DPickRayParams,
) -> Option<(PtrMut<ObjectLayer>, PtrMut<ObjectInst>)> {
    use ::std::mem::transmute;
    return unsafe {
        transmute::<
            Option<(PtrConst<ObjectLayer>, PtrConst<ObjectInst>)>,
            Option<(PtrMut<ObjectLayer>,   PtrMut<ObjectInst>)>,
        >(object_center(scene, pick_params))
    }
}

