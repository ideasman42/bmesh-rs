// Licensed: GPL v2+

use bmesh::prelude::*;
use math_misc::*;

//
// While this operator works fine for general use,
// there are some details that can be improved.
//
// TODO:
//
// - we'll probably want to take arrays of clipping planes
//   so we can avoid selecting points outside the view.
//
//   in fact 2x arrays of planes, one for double sided clipping
//   (near/far, and ortho mode-window bounds).
//   Then single sided clipping, window bounds in perspective view.
//
// - Distance threshold, points outside this can be ignored.
//
// - Scale distance to compensate for perspective views.
//   Currently this just uses nearest to ray,
//   where it should use a 'cone' for perspective views.
//   We could pass in the perspective matrix,
//   while slightly more accurate from user perspective (if you're very picky),
//   it makes the operator rely too much on view context.
//


use super::super::pick_ray::{
    View3DPickRayParams,
};

pub fn vert(
    bm: PtrConst<BMesh>,
    ob_matrix: &[[f64; 4]; 4],
    pick_params: &View3DPickRayParams,
) -> Option<BMVertConstP> {
    let mut v_best = null_const();
    let mut v_best_len_sq = -1.0;

    let pick_origin_proj = pick_params.proj_origin_calc();
    let view_range_offset = pick_params.view_range_offset_calc();

    for v in bm.verts.iter() {
        if !v.is_hide() {
            let co = mul_m4v3(ob_matrix, &v.co);
            if {
                let d = dot_v3v3(&pick_params.view_vector, &co);
                {
                    d < view_range_offset[0] ||
                    d > view_range_offset[1]
                }
            } {
                continue;
            }

            let co_proj = project_plane_v3v3(&co, &pick_params.pick_vector);
            let v_test_len_sq = len_squared_v3v3(&pick_origin_proj, &co_proj);
            if v_best == null_const() ||
               v_test_len_sq < v_best_len_sq
            {
                v_best = v;
                v_best_len_sq = v_test_len_sq;
            }
        }
    }
    return v_best.as_option();
}
pub fn vert_mut(
    bm: PtrMut<BMesh>,
    ob_matrix: &[[f64; 4]; 4],
    pick_params: &View3DPickRayParams,
) -> Option<BMVertMutP> {
    use ::std::mem::transmute;
    return unsafe {
        transmute::<
            Option<BMVertConstP>,
            Option<BMVertMutP>,
        >(vert(bm.as_const(), ob_matrix, pick_params))
    }
}

pub fn edge(
    bm: PtrConst<BMesh>,
    ob_matrix: &[[f64; 4]; 4],
    pick_params: &View3DPickRayParams,
) -> Option<BMEdgeConstP> {
    let mut e_best = null_const();
    let mut e_best_len_sq = -1.0;

    let pick_origin_proj = pick_params.proj_origin_calc();
    let view_range_offset = pick_params.view_range_offset_calc();

    for e in bm.edges.iter() {
        if !e.is_hide() {
            // Find the closest point on the line...
            // then use same logic as vertices and faces.
            //
            // Note that there are some cases that this wont give good results
            // (nearest point might be behind the view for e.g.)

            let co0 = mul_m4v3(ob_matrix, &e.verts[0].co);
            let co1 = mul_m4v3(ob_matrix, &e.verts[1].co);

            if {
                let d0 = dot_v3v3(&pick_params.view_vector, &co0);
                let d1 = dot_v3v3(&pick_params.view_vector, &co1);
                {
                    (d0 < view_range_offset[0] && d1 < view_range_offset[1]) ||
                    (d0 > view_range_offset[1] && d1 > view_range_offset[1])
                }
            } {
                continue;
            }

            let co0_proj = project_plane_v3v3(&co0, &pick_params.pick_vector);
            let co1_proj = project_plane_v3v3(&co1, &pick_params.pick_vector);
            let e_test_len_sq = dist_squared_to_line_segment_v3(
                &pick_origin_proj, &co0_proj, &co1_proj);
            if e_best == null_const() ||
               e_test_len_sq < e_best_len_sq
            {
                e_best = e;
                e_best_len_sq = e_test_len_sq;
            }
        }
    }
    return e_best.as_option();
}
pub fn edge_mut(
    bm: PtrMut<BMesh>,
    ob_matrix: &[[f64; 4]; 4],
    pick_params: &View3DPickRayParams,
) -> Option<BMEdgeMutP> {
    use ::std::mem::transmute;
    return unsafe {
        transmute::<
            Option<BMEdgeConstP>,
            Option<BMEdgeMutP>,
        >(edge(bm.as_const(), ob_matrix, pick_params))
    }
}

pub fn face_center(
    bm: PtrConst<BMesh>,
    ob_matrix: &[[f64; 4]; 4],
    pick_params: &View3DPickRayParams,
) -> Option<BMFaceConstP> {
    let mut f_best = null_const();
    let mut f_best_len_sq = -1.0;

    let pick_origin_proj = pick_params.proj_origin_calc();
    let view_range_offset = pick_params.view_range_offset_calc();

    for f in bm.faces.iter() {
        if !f.is_hide() {
            let co = mul_m4v3(ob_matrix, &f.calc_center_median());
            if {
                let d = dot_v3v3(&pick_params.view_vector, &co);
                {
                    d < view_range_offset[0] ||
                    d > view_range_offset[1]
                }
            } {
                continue;
            }

            let co_proj = project_plane_v3v3(&co, &pick_params.pick_vector);
            let f_test_len_sq = len_squared_v3v3(&pick_origin_proj, &co_proj);
            if f_best == null_const() ||
               f_test_len_sq < f_best_len_sq
            {
                f_best = f;
                f_best_len_sq = f_test_len_sq;
            }
        }
    }
    return f_best.as_option();
}
pub fn face_center_mut(
    bm: PtrMut<BMesh>,
    ob_matrix: &[[f64; 4]; 4],
    pick_params: &View3DPickRayParams,
) -> Option<BMFaceMutP> {
    use ::std::mem::transmute;
    return unsafe {
        transmute::<
            Option<BMFaceConstP>,
            Option<BMFaceMutP>,
        >(face_center(bm.as_const(), ob_matrix, pick_params))
    }
}
