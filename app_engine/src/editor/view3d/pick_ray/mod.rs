// Licensed: GPL v2+

///
/// Use for selection, picking objects, verts, edges, faces etc.
///

pub mod edit_mesh;
pub mod object;

// Data used for a ray-pick action,
// for viewport and cursor vector.
//
// TODO: bounding planes (screen bounds).
pub struct View3DPickRayParams {
    pub view_vector: [f64; 3],
    pub view_range: [f64; 2],

    pub pick_vector: [f64; 3],
    pub pick_origin: [f64; 3],
}

impl View3DPickRayParams {
    ///
    /// Origin projected onto the view vector,
    /// useful so we can project other points onto the plane defined by the 'pick_vector'
    /// then compare their projected distances.
    ///
    pub fn proj_origin_calc(&self) -> [f64; 3] {
        use math_misc::project_plane_v3v3;
        return project_plane_v3v3(&self.pick_origin, &self.pick_vector);
    }

    ///
    /// The near/far viewport clipping values, offset by the 'pick_origin',
    /// this is handy since it means we can check if the world-space coordinates
    /// dot product with the 'view_vector' is in the range of the view-range-offset.
    ///
    pub fn view_range_offset_calc(&self) -> [f64; 2] {
        use math_misc::dot_v3v3;
        let offset = dot_v3v3(&self.view_vector, &self.pick_origin);
        return [
            self.view_range[0] + offset,
            self.view_range[1] + offset,
        ];
    }
}
