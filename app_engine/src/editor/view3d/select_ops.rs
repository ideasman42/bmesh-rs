// Licensed: GPL v2+

// Shared between all operators defined here
mod local_prelude {
    pub use std::borrow::Cow;

    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };

    pub use ::core::rna::define::{
        StructRNA,
        PropertyRNA,
    };
    pub use ::core::edit_mesh::prelude::{
        BMEditMesh,
    };
    pub use ::wm::event_system::{
        Event,
        EventID,
    };
    pub use ::wm::keymap::{
        MapButton,
    };
    pub use ::wm::region::{
        ARegion,
    };

    pub use view3d;
    pub use view3d::{
        View3D,
        RegionView3D,
    };
    pub use ::plain_ptr::*;

    pub use super::super::pick_ray;
    pub use super::super::pick_ray::View3DPickRayParams;
}

use self::local_prelude::*;

pub mod pick_edit_mesh {
    use super::local_prelude::*;
    use bmesh::prelude::*;

    pub fn pick_vertex_select(
        ctx: AppContextMutP,
        use_extend: bool,
        use_toggle: bool,
        pick_params: &View3DPickRayParams,
    ) -> bool {
        let obj_inst = ctx.object_inst().unwrap();
        let mut em = BMEditMesh::from_object_get_mut(obj_inst.object).unwrap();
        let bm = &mut em.bm;

        if !use_extend {
            for mut v in bm.verts.iter_mut() {
                v.select_set(false);
            }
        }

        if let Some(mut v) = pick_ray::edit_mesh::vert_mut(
            PtrMut(bm),
            &obj_inst.matrix,
            pick_params,
        ) {
            let select = if use_toggle {!v.is_select() } else { true };
            v.select_set(select);
        }
        bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
            .skip(BM_ELEM_HIDDEN)
            .set_edge_from_vert_all()
            .set_face_from_vert_all()
            .exec();

        return true;
    }

    pub fn pick_edge_select(
        ctx: AppContextMutP,
        use_extend: bool,
        use_toggle: bool,
        pick_params: &View3DPickRayParams,
    ) -> bool {
        let obj_inst = ctx.object_inst().unwrap();
        let mut em = BMEditMesh::from_object_get_mut(obj_inst.object).unwrap();
        let bm = &mut em.bm;

        if !use_extend {
            for mut e in bm.edges.iter_mut() {
                e.select_set_noflush(false);
            }
        }

        if let Some(mut e) = pick_ray::edit_mesh::edge_mut(
            PtrMut(bm),
            &obj_inst.matrix,
            pick_params,
        ) {
            let select = if use_toggle {!e.is_select() } else { true };
            e.select_set_noflush(select);
        }
        bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
            .skip(BM_ELEM_HIDDEN)
            .set_vert_from_edge_any()
            .set_face_from_edge_all()
            .exec();
        return true;
    }

    pub fn pick_face_select(
        ctx: AppContextMutP,
        use_extend: bool,
        use_toggle: bool,
        pick_params: &View3DPickRayParams,
    ) -> bool {
        let obj_inst = ctx.object_inst().unwrap();
        let mut em = BMEditMesh::from_object_get_mut(obj_inst.object).unwrap();
        let bm = &mut em.bm;

        if !use_extend {
            for mut f in bm.faces.iter_mut() {
                f.select_set_noflush(false);
            }
        }

        if let Some(mut f) = pick_ray::edit_mesh::face_center_mut(
            PtrMut(bm),
            &obj_inst.matrix,
            pick_params,
        ) {
            let select = if use_toggle {!f.is_select() } else { true };
            f.select_set_noflush(select);
        }
        bm.elem_hflag_flush_builder(BM_ELEM_SELECT)
            .skip(BM_ELEM_HIDDEN)
            .set_edge_from_face_any()
            .set_vert_from_edge_any()
            .exec();
        return true;
    }
}

mod pick_scene_object {
    use super::local_prelude::*;

    pub fn pick_object_center(
        ctx: AppContextMutP,
        use_extend: bool,
        use_toggle: bool,
        pick_params: &View3DPickRayParams,
    ) -> bool {
        let mut scene = ctx.env.scene;

        if !use_extend {
            for mut obj_lay in scene.object_layers.iter_mut() {
                for obj_inst in obj_lay.objects.iter_mut() {
                    obj_inst.clone().flag.select_disable();
                }
            }
        }

        if let Some((
            obj_lay,
            mut obj_inst,
        )) = pick_ray::object::object_center_mut(
            &mut *scene,
            pick_params,
        ) {
            let select = {
                if use_toggle {
                    ((obj_inst.flag.select_test() == false) ||
                     (scene.object_inst_active != obj_inst))
                } else {
                    true
                }
            };
            obj_inst.flag.select_set(select);

            // We may want to control what active items are set here.
            scene.object_layer_active = obj_lay;
            scene.object_inst_active = obj_inst;
        }

        return true;
    }
}

mod view3d_select_pick_op {
    use super::local_prelude::*;
    // use super::{
    //     edit_view3d_poll,
    // };

    use math_misc::*;

    fn invoke(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {

        let mut props = op.props();

        let v3d = &mut *ctx.v3d_mut().unwrap();
        let rv3d = &mut *ctx.rv3d_mut().unwrap();

        // TODO, make handy-utility function for this
        let mval = sub_v2v2(&event.state.co, &ctx.welem_level(0).rect_fl.to_xy_lb());
        let win_size = ctx.welem_level(0).rect_fl.size();

        let pick_origin = view3d::project::win_to_origin(rv3d, &win_size, mval);
        let pick_vector = view3d::project::win_to_vector(rv3d, &win_size, mval);

        let view_vector = normalized_v3(
            &negated_v3(as_v3!(&ctx.rv3d().unwrap().matrix.view_inv[2]))).0;
        let view_range = view3d::view::clip_range_get(v3d, rv3d, false).0;

        props.float_array_set("pick_vector", &pick_vector[..]);
        props.float_array_set("pick_origin", &pick_origin[..]);

        props.float_array_set("view_vector", &view_vector[..]);
        props.float_array_set("view_range", &view_range[..]);

        return exec(ctx, op);
    }

    fn exec(ctx: AppContextMutP, op: &mut Operator) -> OpState {

        let props = op.props();

        // let rv3d = ctx.rv3d_mut().unwrap();

        // keymap must set
        let use_extend = props.bool_get("extend");
        let use_toggle = props.bool_get("toggle");

        let pick_params = View3DPickRayParams {
            // picking from pointer.
            pick_origin: unpack!([props.float_array_get("pick_origin").unwrap(); 3]),
            pick_vector: unpack!([props.float_array_get("pick_vector").unwrap(); 3]),
            // clipping from view.
            view_vector: unpack!([props.float_array_get("view_vector").unwrap(); 3]),
            view_range:  unpack!([props.float_array_get("view_range").unwrap(); 2]),
        };

        if let Some(em) = ctx.editmesh() {
            use bmesh::prelude::*;

            if (em.bm.select_mode & BM_VERT) != 0 {
                super::pick_edit_mesh::pick_vertex_select(
                    ctx,
                    use_extend,
                    use_toggle,
                    &pick_params,
                );
            } else if (em.bm.select_mode & BM_EDGE) != 0 {
                super::pick_edit_mesh::pick_edge_select(
                    ctx,
                    use_extend,
                    use_toggle,
                    &pick_params,
                );
            } else if (em.bm.select_mode & BM_FACE) != 0 {
                super::pick_edit_mesh::pick_face_select(
                    ctx,
                    use_extend,
                    use_toggle,
                    &pick_params,
                );
            }
        } else {
            super::pick_scene_object::pick_object_center(
                ctx,
                use_extend,
                use_toggle,
                &pick_params,
            );
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        let mut srna = StructRNA::new_builder("")
            .exec();
        srna.properties.insert(
            PropertyRNA::new_bool_builder("extend", "Extend Selection")
            .exec());
        srna.properties.insert(
            PropertyRNA::new_bool_builder("toggle", "Toggle Selection")
            .exec());
        srna.properties.insert(
            PropertyRNA::new_float_array_builder("pick_vector", "Pick Vector")
            .array_length(3)
            .exec());
        srna.properties.insert(
            PropertyRNA::new_float_array_builder("pick_origin", "Pick Origin")
            .array_length(3)
            .exec());
        srna.properties.insert(
            PropertyRNA::new_float_array_builder("view_vector", "View Vector")
            .array_length(3)
            .exec());
        srna.properties.insert(
            PropertyRNA::new_float_array_builder("view_range", "View Range")
            .array_length(2)
            .exec());

        OperatorType {
            id: "view3d.select_pick".to_string(),
            name: "View3D Select Pick".to_string(),
            // poll: Some(edit_view3d_poll),
            invoke: Some(invoke),
            exec: Some(exec),
            srna: srna,
            undo: Some(::editor::util::undo::global_undo),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(view3d_select_pick_op::new_type());
}
