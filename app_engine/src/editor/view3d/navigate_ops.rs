// Licensed: GPL v2+

// Shared between all operators defined here
mod local_prelude {
    pub use ::prelude::*;
    pub use ::wm::operator::{
        OperatorType,
        Operator,
        OpState,
    };
    pub use ::wm::event_system::{
        Event,
        EventID,
    };
    pub use ::wm::keymap::{
        MapButton,
    };
    pub use ::wm::region::{
        ARegion,
    };

    pub use view3d;
    pub use view3d::{
        View3D,
        RegionView3D,
    };
}

use self::local_prelude::*;


fn edit_view3d_poll(ctx: AppContextConstP) -> bool {
    return !ctx.v3d().is_none();
}

mod view3d_persp_toggle_op {
    use super::local_prelude::*;
    use super::{
        edit_view3d_poll,
    };

    fn exec(mut ctx: AppContextMutP, _op: &mut Operator) -> OpState {
        let mut rv3d = ctx.rv3d_mut().unwrap();

        // maybe nice to make macro for cycling enum values if we do more often
        match rv3d.persp {
            view3d::ViewPersp::PERSP => {
                rv3d.persp = view3d::ViewPersp::ORTHO;
            },
            view3d::ViewPersp::ORTHO => {
                rv3d.persp = view3d::ViewPersp::PERSP;
            },
            _ => {
                // pass
            },
        }

        return OpState::FINISHED;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "view3d.persp_toggle".to_string(),
            name: "View3D Perspective Toggle".to_string(),
            poll: Some(edit_view3d_poll),
            exec: Some(exec),
            .. OperatorType::default()
        }
    }
}
mod view3d_zoom_op {
    use super::local_prelude::*;
    use super::{
        edit_view3d_poll,
    };

    struct CustomZoomData {
        // Initial mouse position
        event_co: [f64; 2],
        // Button pressed to initialize this tool
        event_id: EventID,

        offset_dist: f64,
    }

    fn invoke(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {
        let rv3d = ctx.rv3d_mut().unwrap();

        let data = CustomZoomData {
            event_co: event.state.co,
            event_id: event.id.clone(),

            offset_dist: rv3d.xform.offset_dist,
        };

        op.custom_data = Some(Box::new(data));
        return OpState::RUNNING_MODAL;
    }

    fn modal(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {
        // Too cryptic, figure out a nice way to avoid verbosity.
        let data = op.custom_data.as_ref().unwrap().downcast_ref::
            <CustomZoomData>().unwrap();

        match event.id {
            EventID::PointerMotion {} => {
                let mut rv3d = ctx.rv3d_mut().unwrap();

                let scale = 100.0;
                let offset = 1.0 + ((data.event_co[1] - event.state.co[1]) / scale);

                rv3d.xform.offset_dist = data.offset_dist * offset;
                if rv3d.xform.offset_dist < 0.01 {
                    rv3d.xform.offset_dist = 0.01;
                }
            },

            // TODO, this could be made less verbose!
            EventID::PointerButton {value, button, .. } => {
                if value == false {
                    if let EventID::PointerButton { button: button_orig, .. } = data.event_id {
                        if button == button_orig {
                            return OpState::FINISHED;
                        }
                    }
                }
            },
            EventID::Keyboard {value, scan_code, .. } => {
                if value == false {
                    if let EventID::Keyboard { scan_code: scan_code_orig, .. } = data.event_id {
                        if scan_code == scan_code_orig {
                            return OpState::FINISHED;
                        }
                    }
                }
            },
            _ => {}
        }

        return OpState::RUNNING_MODAL;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "view3d.zoom".to_string(),
            name: "View3D Zoom".to_string(),
            poll: Some(edit_view3d_poll),
            invoke: Some(invoke),
            modal: Some(modal),
            .. OperatorType::default()
        }
    }
}

mod view3d_rotate_op {
    use super::local_prelude::*;
    use super::{
        edit_view3d_poll,
    };

    use math_misc::{
        angle_normalized_v3v3,
        axis_angle_to_quat,
        axis_angle_to_quat_single,
        cross_v3v3,
        dot_v3v3,
        interp_v3v3,
        inverted_m3,
        len_squared_v3v3,
        len_v3,
        mul_qtqt,
        negate_v3,
        normalize_qt,
        quat_to_mat3,
        sub_v3v3,
        unit_m3_ret,
        angle_wrap_rad,
    };
    use ::std::f64::consts::{
        PI,
    };

    // Workaround for gimble lock
    const USE_GIMBLE_LOCK_FIX: bool = true;

    const TRACKBALL_SIZE: f64 = 1.1;

    // Sensitivity will control how fast the viewport rotates.  0.007 was
    // obtained experimentally by looking at viewport rotation sensitivities
    // on other modeling programs.
    //
    // Perhaps this should be a configurable user parameter.
    const TURNTABLE_SENSITIVITY: f64 = 0.007;

    fn trackball_calc_vec(rect: &RectF, event_co: &[f64; 2]) -> [f64; 3] {
        // float x, y, radius, d, z, t;

        let radius = TRACKBALL_SIZE;
        let t = radius / ::std::f64::consts::SQRT_2;

        // normalize x and y
        let x = (rect.cent_x() - event_co[0]) / (rect.size_x() / 4.0);
        let y = (rect.cent_y() - event_co[1]) / (rect.size_y() / 2.0);
        let z;

        let d = (x * x + y * y).sqrt();
        if d < t {
            // Inside sphere
            z = (radius * radius - d * d).sqrt();
        } else {
            // On hyperbola
            z = t * t / d;
        }

        return [x, y, -z];
    }

    // Store rotation state between updates.
    struct RotateApplyData {
        event_co_prev: [f64; 2],

        // Store rotation here, so we can snap the views rotation.
        rotation_orig: [f64; 4],
        rotation_curr: [f64; 4],

        is_reverse: bool,

        // only for trackball
        trackvec: [f64; 3],
    }

    // generic view rotate code
    // viewrotate_apply in Blender
    fn view_rotate_apply(
        rv3d: &mut RegionView3D,
        vod: &mut RotateApplyData,
        winrct: &RectF,
        event_co: &[f64; 2])
    {
        /*
        // need to reset every time because of view snapping
        rv3d->view = RV3D_VIEW_USER;

        if (U.flag & USER_TRACKBALL) {
        */
        if true {
            // XXX, expose for real! FIXME
            let newvec = trackball_calc_vec(winrct, event_co);
            let dvec = sub_v3v3(&newvec, &vod.trackvec);

            // Allow for rotation beyond the interval [-pi, pi]
            let angle = angle_wrap_rad((len_v3(&dvec) / (2.0 * TRACKBALL_SIZE)) * PI);

            // This relation is used instead of the actual angle between vectors
            // so that the angle of rotation is linearly proportional to
            // the distance that the mouse is dragged.

            let axis = cross_v3v3(&vod.trackvec, &newvec);
            let q1 = axis_angle_to_quat(&axis, angle);

            vod.rotation_curr = mul_qtqt(&q1, &vod.rotation_orig);

            // BMESH-RS TODO
            // viewrotate_apply_dyn_ofs(vod, vod.rotation_curr);
        } else {
            // New turntable view code by John Aughey
            let zvec_global = [0.0, 0.0, 1.0];

            // Get the 3x3 matrix and its inverse from the quaternion
            let m = quat_to_mat3(&vod.rotation_curr);
            let m_inv = inverted_m3(&m).unwrap_or_else(|| unit_m3_ret());

            // avoid gimble lock
            let mut xaxis;
            if USE_GIMBLE_LOCK_FIX {
                if len_squared_v3v3(&zvec_global, &m_inv[2]) > 0.001 {
                    xaxis = cross_v3v3(&zvec_global, &m_inv[2]);
                    if dot_v3v3(&xaxis, &m_inv[0]) < 0.0 {
                        negate_v3(&mut xaxis);
                    }
                    let mut fac = angle_normalized_v3v3(&zvec_global, &m_inv[2]) / PI;
                    fac = ((fac - 0.5).abs() * 2.0).powi(2);
                    xaxis = interp_v3v3(&xaxis, &m_inv[0], fac);
                } else {
                    xaxis = m_inv[0];
                }
            } else {
                // simple, non-gimble lock code fix
                xaxis = m_inv[0];
            }

            // Determine the direction of the x vector (for rotating up and down)
            // This can likely be computed directly from the quaternion.

            // Perform the up/down rotation
            let mut quat_local_x = axis_angle_to_quat(
                &xaxis, TURNTABLE_SENSITIVITY * -(event_co[1] - vod.event_co_prev[1]));
            quat_local_x = mul_qtqt(&vod.rotation_curr, &quat_local_x);

            // Perform the orbital rotation
            let quat_global_z = axis_angle_to_quat_single(
                'Z',
                TURNTABLE_SENSITIVITY *
                { if vod.is_reverse { -1.0 } else { 1.0 } } *
                (event_co[0] - vod.event_co_prev[0])
            );
            vod.rotation_curr = mul_qtqt(&quat_local_x, &quat_global_z);

            // BMESH-RS TODO
            // viewrotate_apply_dyn_ofs(vod, vod.rotation_curr);
        }

        // avoid precision loss over time
        normalize_qt(&mut vod.rotation_curr);

        // use a working copy so view rotation locking doesnt overwrite the locked
        // rotation back into the view we calculate with
        rv3d.xform.rotation = vod.rotation_curr;

        // check for view snap,
        // note: don't apply snap to vod.rotation_curr so the view wont jam up

        // BMESH-RS TODO
        /*
        if vod.axis_snap {
            viewrotate_apply_snap(vod);
        }
        */
        vod.event_co_prev = *event_co;

        // BMESH-RS TODO
        /*
        ED_view3d_camera_lock_sync(vod.v3d, rv3d);
        ED_region_tag_redraw(vod.ar);
        */
    }

    struct CustomRotateData {
        // Initial mouse position
        event_id: EventID,

        apply_data: RotateApplyData,
    }

    fn invoke(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {

        // XXX, awkward!
        let winrct = {
            let ar = ctx.region_mut().unwrap();
            ar.winrct.clone()
        };

        let rv3d = ctx.rv3d_mut().unwrap();


        let data = CustomRotateData {
            event_id: event.id.clone(),

            apply_data: RotateApplyData {
                event_co_prev: event.state.co,
                // BMESH-RS TODO (Preference)
                is_reverse: false,
                rotation_orig: rv3d.xform.rotation,
                rotation_curr: rv3d.xform.rotation,

                trackvec: trackball_calc_vec(&winrct, &event.state.co),
            },
        };

        op.custom_data = Some(Box::new(data));
        return OpState::RUNNING_MODAL;
    }

    fn modal(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {
        // Too cryptic, figure out a nice way to avoid verbosity.
        let data = op.custom_data.as_mut().unwrap().downcast_mut::
            <CustomRotateData>().unwrap();

        match event.id {
            EventID::PointerMotion {} => {
                // XXX, awkward!
                let winrct = {
                    let ar = ctx.region_mut().unwrap();
                    ar.winrct.clone()
                };
                let rv3d = &mut *ctx.rv3d_mut().unwrap();

                view_rotate_apply(rv3d, &mut data.apply_data, &winrct, &event.state.co);
            },

            // TODO, this could be made less verbose!
            EventID::PointerButton {value, button, .. } => {
                if value == false {
                    if let EventID::PointerButton { button: button_orig, .. } = data.event_id {
                        if button == button_orig {
                            return OpState::FINISHED;
                        }
                    }
                }
            },
            EventID::Keyboard {value, scan_code, .. } => {
                if value == false {
                    if let EventID::Keyboard { scan_code: scan_code_orig, .. } = data.event_id {
                        if scan_code == scan_code_orig {
                            return OpState::FINISHED;
                        }
                    }
                }
            },
            _ => {},
        }

        return OpState::RUNNING_MODAL;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "view3d.rotate".to_string(),
            name: "View3D Rotate".to_string(),
            poll: Some(edit_view3d_poll),
            invoke: Some(invoke),
            modal: Some(modal),
            .. OperatorType::default()
        }
    }
}

mod view3d_move_op {
    use super::local_prelude::*;
    use super::{
        edit_view3d_poll,
    };

    use math_misc::{
        sub_v2v2,
        iadd_v3_v3,
    };

    // Store rotation state between updates.
    struct MoveApplyData {
        event_co_prev: [f64; 2],

        // offset_orig: [f64; 4],
        zfac: f64,
    }

    fn view_move_apply(
        rv3d: &mut RegionView3D,
        vod: &mut MoveApplyData,
        winrct: &RectF,
        event_co: &[f64; 2])
    {
        // BMESH-RS TODO - other move types
        /*
        if (ED_view3d_offset_lock_check(vod->v3d, vod->rv3d)) {
            vod->rv3d->ofs_lock[0] -= ((vod->oldx - x) * 2.0) / (float)vod->ar->winx;
            vod->rv3d->ofs_lock[1] -= ((vod->oldy - y) * 2.0) / (float)vod->ar->winy;
        }
        else if ((vod->rv3d->persp == RV3D_CAMOB) &&
                 !ED_view3d_camera_lock_check(vod->v3d, vod->rv3d))
        {
            const float zoomfac = BKE_screen_view3d_zoom_to_fac(vod->rv3d->camzoom) * 2.0;
            vod->rv3d->camdx += (vod->oldx - x) / (vod->ar->winx * zoomfac);
            vod->rv3d->camdy += (vod->oldy - y) / (vod->ar->winy * zoomfac);
            CLAMP(vod->rv3d->camdx, -1.0, 1.0);
            CLAMP(vod->rv3d->camdy, -1.0, 1.0);
        } else
        */
        {
            let mval_f = sub_v2v2(&vod.event_co_prev, event_co);
            let dvec = view3d::project::win_to_delta(rv3d, &winrct.size(), mval_f, vod.zfac);
            iadd_v3_v3(&mut rv3d.xform.offset, &dvec);

            /*
            if (vod->rv3d->viewlock & RV3D_BOXVIEW) {
                view3d_boxview_sync(vod->sa, vod->ar);
            }
            */
        }

        vod.event_co_prev = *event_co;

        /*
        ED_view3d_camera_lock_sync(vod->v3d, vod->rv3d);

        ED_region_tag_redraw(vod->ar);
        */
    }

    struct CustomMoveData {
        // Button pressed to initialize this tool
        event_id: EventID,

        apply_data: MoveApplyData,
    }

    fn invoke(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {
        let rv3d = &mut *ctx.rv3d_mut().unwrap();

        let data = CustomMoveData {
            event_id: event.id.clone(),

            apply_data: MoveApplyData {
                event_co_prev: event.state.co,
                zfac: view3d::project::calc_zfac(rv3d, &rv3d.xform.offset).0,
            },
        };

        op.custom_data = Some(Box::new(data));
        return OpState::RUNNING_MODAL;
    }

    fn modal(mut ctx: AppContextMutP, op: &mut Operator, event: &Event) -> OpState {
        // Too cryptic, figure out a nice way to avoid verbosity.
        let data = op.custom_data.as_mut().unwrap().downcast_mut::
            <CustomMoveData>().unwrap();

        match event.id {
            EventID::PointerMotion {} => {
                // XXX, awkward!
                let winrct = {
                    let ar = ctx.region_mut().unwrap();
                    ar.winrct.clone()
                };
                let rv3d = &mut *ctx.rv3d_mut().unwrap();

                view_move_apply(rv3d, &mut data.apply_data, &winrct, &event.state.co);
            },

            // TODO, this could be made less verbose!
            EventID::PointerButton {value, button, .. } => {
                if value == false {
                    if let EventID::PointerButton { button: button_orig, .. } = data.event_id {
                        if button == button_orig {
                            return OpState::FINISHED;
                        }
                    }
                }
            },
            EventID::Keyboard {value, scan_code, .. } => {
                if value == false {
                    if let EventID::Keyboard { scan_code: scan_code_orig, .. } = data.event_id {
                        if scan_code == scan_code_orig {
                            return OpState::FINISHED;
                        }
                    }
                }
            },
            _ => {},
        }

        return OpState::RUNNING_MODAL;
    }

    pub fn new_type() -> OperatorType {
        OperatorType {
            id: "view3d.move".to_string(),
            name: "View3D Move".to_string(),
            poll: Some(edit_view3d_poll),
            invoke: Some(invoke),
            modal: Some(modal),
            .. OperatorType::default()
        }
    }
}

pub fn register_operators(app: &mut App) {
    app.system.operator_types.register(view3d_persp_toggle_op::new_type());
    app.system.operator_types.register(view3d_zoom_op::new_type());
    app.system.operator_types.register(view3d_rotate_op::new_type());
    app.system.operator_types.register(view3d_move_op::new_type());
}
