// Licensed: GPL v2+

/// Define keys for the purpose of keymapping,
/// this merges pointer and keyboard entries.
///
/// Unlike scan_code, we don't differentiate left/right modifier keys,
/// and leave out other keys such as caps-lock.
///
/// Keys can be added if needed.
///

use ::std::collections::HashSet;

use ::wm::operator::{
    OpAction,
};

pub mod map_event_range {
    // Use blocks of 512 for each device type.
    //
    // keyboard:    0..512
    // pointer:    512..768
    // (for later)
    // ndof:      768..1024
    // joystick: 1024..1280

    use ::std::ops::Range;
    pub const KB:             Range<u16> = 0..512;
    pub const POINTER:         Range<u16> = 512..(512 + 256);
    pub const POINTER_BUTTON:  Range<u16> = 512..(512 + (256 - 1));
}


macro_rules! kb {
    ($g:ident) => {
        MapCode(map_event_range::KB.start + ::wm::event_system::ScanCode::$g.0);
    }
}

macro_rules! pointer {
    ($g:ident) => {
        MapCode(map_event_range::POINTER.start + ::wm::event_system::PointerButton::$g.0);
    }
}
macro_rules! pointer_from_last {
    ($n:expr) => {
        MapCode(map_event_range::POINTER.start + (map_event_range::POINTER.end - (1 + $n)));
    }
}

/// An event we can include in a key-map,
/// Mixes keyboard, pointer .. etc.
struct_enum_int_impl!(pub struct MapCode(pub u16));
impl MapCode {

    // Regular keys match event_system::ScanCode for simple conversion
    // except for modifiers where we don't want to care about left/right ctrl/alt/shift/os.
    pub const A:                Self = kb!(A);
    pub const B:                Self = kb!(B);
    pub const C:                Self = kb!(C);
    pub const D:                Self = kb!(D);
    pub const E:                Self = kb!(E);
    pub const F:                Self = kb!(F);
    pub const G:                Self = kb!(G);
    pub const H:                Self = kb!(H);
    pub const I:                Self = kb!(I);
    pub const J:                Self = kb!(J);
    pub const K:                Self = kb!(K);
    pub const L:                Self = kb!(L);
    pub const M:                Self = kb!(M);
    pub const N:                Self = kb!(N);
    pub const O:                Self = kb!(O);
    pub const P:                Self = kb!(P);
    pub const Q:                Self = kb!(Q);
    pub const R:                Self = kb!(R);
    pub const S:                Self = kb!(S);
    pub const T:                Self = kb!(T);
    pub const U:                Self = kb!(U);
    pub const V:                Self = kb!(V);
    pub const W:                Self = kb!(W);
    pub const X:                Self = kb!(X);
    pub const Y:                Self = kb!(Y);
    pub const Z:                Self = kb!(Z);

    pub const NUM_0:            Self = kb!(NUM_0);
    pub const NUM_1:            Self = kb!(NUM_1);
    pub const NUM_2:            Self = kb!(NUM_2);
    pub const NUM_3:            Self = kb!(NUM_3);
    pub const NUM_4:            Self = kb!(NUM_4);
    pub const NUM_5:            Self = kb!(NUM_5);
    pub const NUM_6:            Self = kb!(NUM_6);
    pub const NUM_7:            Self = kb!(NUM_7);
    pub const NUM_8:            Self = kb!(NUM_8);
    pub const NUM_9:            Self = kb!(NUM_9);

    // Modifier keys
    pub const CTRL:             Self = kb!(LEFT_CTRL);
    pub const ALT:              Self = kb!(LEFT_ALT);
    pub const SHIFT:            Self = kb!(LEFT_SHIFT);
    pub const SUPER:            Self = kb!(LEFT_SUPER);

    pub const ESC:              Self = kb!(ESC);
    pub const TAB:              Self = kb!(TAB);
    pub const RET:              Self = kb!(RET);
    pub const SPACE:            Self = kb!(SPACE);
    pub const BACKSPACE:        Self = kb!(BACKSPACE);
    pub const DEL:              Self = kb!(DEL);
    pub const SEMICOLON:        Self = kb!(SEMICOLON);
    pub const PERIOD:           Self = kb!(PERIOD);
    pub const COMMA:            Self = kb!(COMMA);
    pub const QUOTE:            Self = kb!(QUOTE);
    pub const ACCENT_GRAVE:     Self = kb!(ACCENT_GRAVE);
    pub const MINUS:            Self = kb!(MINUS);
    pub const PLUS:             Self = kb!(PLUS);
    pub const SLASH:            Self = kb!(SLASH);
    pub const BACKSLASH:        Self = kb!(BACKSLASH);
    pub const EQUAL:            Self = kb!(EQUAL);
    pub const LEFT_BRACKET:     Self = kb!(LEFT_BRACKET);
    pub const RIGHT_BRACKET:    Self = kb!(RIGHT_BRACKET);

    pub const LEFT_ARROW:       Self = kb!(LEFT_ARROW);
    pub const DOWN_ARROW:       Self = kb!(DOWN_ARROW);
    pub const RIGHT_ARROW:      Self = kb!(RIGHT_ARROW);
    pub const UP_ARROW:         Self = kb!(UP_ARROW);

    pub const PAD_0:            Self = kb!(PAD_0);
    pub const PAD_1:            Self = kb!(PAD_1);
    pub const PAD_2:            Self = kb!(PAD_2);
    pub const PAD_3:            Self = kb!(PAD_3);
    pub const PAD_4:            Self = kb!(PAD_4);
    pub const PAD_5:            Self = kb!(PAD_5);
    pub const PAD_6:            Self = kb!(PAD_6);
    pub const PAD_7:            Self = kb!(PAD_7);
    pub const PAD_8:            Self = kb!(PAD_8);
    pub const PAD_9:            Self = kb!(PAD_9);

    pub const PAD_PERIOD:       Self = kb!(PAD_PERIOD);
    pub const PAD_ASTER:        Self = kb!(PAD_ASTER);
    pub const PAD_SLASH:        Self = kb!(PAD_SLASH);
    pub const PAD_MINUS:        Self = kb!(PAD_MINUS);
    pub const PAD_ENTER:        Self = kb!(PAD_ENTER);
    pub const PAD_PLUS:         Self = kb!(PAD_PLUS);

    pub const PAUSE:            Self = kb!(PAUSE);
    pub const INSERT:           Self = kb!(INSERT);
    pub const HOME:             Self = kb!(HOME);
    pub const PAGE_UP:          Self = kb!(PAGE_UP);
    pub const PAGE_DOWN:        Self = kb!(PAGE_DOWN);
    pub const END:              Self = kb!(END);

    pub const GRLESS:           Self = kb!(GRLESS);

    pub const MEDIA_PLAY:       Self = kb!(MEDIA_PLAY);
    pub const MEDIA_STOP:       Self = kb!(MEDIA_STOP);
    pub const MEDIA_PREV:       Self = kb!(MEDIA_PREV);
    pub const MEDIA_NEXT:       Self = kb!(MEDIA_NEXT);

    pub const F1:               Self = kb!(F1);
    pub const F2:               Self = kb!(F2);
    pub const F3:               Self = kb!(F3);
    pub const F4:               Self = kb!(F4);
    pub const F5:               Self = kb!(F5);
    pub const F6:               Self = kb!(F6);
    pub const F7:               Self = kb!(F7);
    pub const F8:               Self = kb!(F8);
    pub const F9:               Self = kb!(F9);
    pub const F10:              Self = kb!(F10);
    pub const F11:              Self = kb!(F11);
    pub const F12:              Self = kb!(F12);
    pub const F13:              Self = kb!(F13);
    pub const F14:              Self = kb!(F14);
    pub const F15:              Self = kb!(F15);
    pub const F16:              Self = kb!(F16);
    pub const F17:              Self = kb!(F17);
    pub const F18:              Self = kb!(F18);
    pub const F19:              Self = kb!(F19);

    // Pointer buttons
    pub const LMB:              Self = pointer!(LEFT);
    pub const RMB:              Self = pointer!(RIGHT);
    pub const MMB:              Self = pointer!(MIDDLE);
    pub const XMB_4:            Self = pointer!(BUTTON_4);
    pub const XMB_5:            Self = pointer!(BUTTON_5);
    // last pointer event
    pub const MOTION:           Self = pointer_from_last!(0);
}

impl MapCode {
    // is event
    #[inline]
    fn is_kb(&self) -> bool {
        // return event_range::KB.contains(self.0); // RFC#32311
        return self.0 >= map_event_range::KB.start &&
               self.0 <  map_event_range::KB.end;
    }
    #[inline]
    fn is_pointer_motion(&self) -> bool {
        return self.0 == MapCode::MOTION.0;
    }
    #[allow(dead_code)]
    #[inline]
    fn is_pointer(&self) -> bool {
        // return event_range::POINTER.contains(self.0); // RFC#32311
        return self.0 >= map_event_range::POINTER.start &&
               self.0 <  map_event_range::POINTER.end;

    }
    #[inline]
    fn is_pointer_button(&self) -> bool {
        // return event_range::POINTER_BUTTON.contains(self.0); // RFC#32311
        return self.0 >= map_event_range::POINTER_BUTTON.start &&
               self.0 <  map_event_range::POINTER_BUTTON.end;
    }
}

struct_enum_int_impl!(pub struct MapButtonType(pub u16));
#[allow(non_snake_case)]
pub mod MapButton {
    use super::MapButtonType as Type;
    pub const NONE: Type = Type(0);
    pub const PRESS: Type = Type(1);
    pub const RELEASE: Type = Type(2);
    // CLICK / DOUBLE_CLICK... STICKY?

}

impl MapButtonType {
    fn from_bool(value: bool) -> MapButtonType {
        if value {
            return MapButton::PRESS;
        } else {
            return MapButton::RELEASE;
        }
    }
}

// ----------------------------------------------------------------------------
// KeyMapEvent

#[derive(Debug)]
pub struct KeyMapEvent {
    pub map_code: MapCode,
    /// Modifiers can be any keys,
    pub modifiers: HashSet<MapCode>,
    pub value: MapButtonType,
}

/// We may want to have different kinds of actions.
#[derive(Debug)]
pub enum KeyMapAction {
    /// Run a tool.
    Operator(OpAction),
    /// Open a menu (which typically runs an operator).
    Menu {
        id: String,
    },
    /// Just a test - print text
    Dummy {
        text: String,
    },
}

#[derive(Debug)]
pub struct KeyMapItem {
    pub event: KeyMapEvent,
    pub action: KeyMapAction,
}

impl KeyMapEvent {
    fn match_event(&self, e: &::wm::event_system::Event) -> bool {
        use ::wm::event_system::EventID;

        // match modifiers
        if self.modifiers != e.state.map_keys {
            return false;
        }

        match e.id {
            EventID::PointerMotion {} => {
                if self.map_code.is_pointer_motion() {
                    return true;
                }
            },
            EventID::PointerButton { button, value } => {
                if self.map_code.is_pointer_button() {
                    if self.value == MapButtonType::from_bool(value) {
                        if let Some(map_event) = button.as_map_event() {
                            if self.map_code == map_event {
                                return true;
                            }
                        }
                    }
                }
            },
            EventID::Keyboard { scan_code, value, .. } => {
                if self.map_code.is_kb() {
                    if self.value == MapButtonType::from_bool(value) {
                        if let Some(map_event) = scan_code.as_map_event() {
                            if self.map_code == map_event {
                                return true;
                            }
                        }
                    }
                }
            },
            EventID::Window { .. } => {
                // ignore
            }
        }
        return false;
    }

}

pub trait KeyMapItemVecImpl {
    fn find_from_event(&self, event: &::wm::event_system::Event) -> Option<&KeyMapItem>;
}

impl KeyMapItemVecImpl for Vec<KeyMapItem> {
    fn find_from_event(&self, event: &::wm::event_system::Event) -> Option<&KeyMapItem> {
        for kmi in self {
            if kmi.event.match_event(event) {
                return Some(kmi);
            }
        }
        return None;
    }
}

pub struct KeyMap {
    id: String,
    pub items: Vec<KeyMapItem>,
    // TODO, poll, flags...
}

impl KeyMap {
    pub fn new(id: &str) -> Self {
        KeyMap {
            id: id.to_string(),
            items: Vec::new(),
        }
    }
}

pub trait KeyMapVecImpl {
    fn find_by_id(&self, id: &str) -> Option<&KeyMap>;
}

impl KeyMapVecImpl for Vec<KeyMap> {
    fn find_by_id(&self, id: &str) -> Option<&KeyMap> {
        for km in self {
            if &km.id == id {
                return Some(km);
            }
        }
        return None;
    }
}



// --------------
// Custom Keymaps
//
// Use int for values, values are defined by the user.

pub trait KeyMapItemCustomVecImpl {
    fn find_from_event(&self, event: &::wm::event_system::Event) -> Option<&KeyMapItemCustom>;
}

impl KeyMapItemCustomVecImpl for Vec<KeyMapItemCustom> {
    fn find_from_event(&self, event: &::wm::event_system::Event) -> Option<&KeyMapItemCustom> {
        for kmi in self {
            if kmi.event.match_event(event) {
                return Some(kmi);
            }
        }
        return None;
    }
}

pub struct KeyMapCustom {
    id: String,
    pub items: Vec<KeyMapItemCustom>,
}


#[derive(Debug)]
pub struct KeyMapItemCustom {
    pub event: KeyMapEvent,
    pub value: u32,
}

impl KeyMapCustom {
    pub fn new(id: &str) -> Self {
        into_expand!(id);
        KeyMapCustom {
            id: id,
            items: Vec::new(),
        }
    }
}

pub trait KeyMapCustomVecImpl {
    fn find_by_id(&self, id: &str) -> Option<&KeyMapCustom>;

    // only for 'custom' keymap items
    fn find_by_id_and_event(&self, id: &str, event: &::wm::event_system::Event) -> Option<u32>;
}

impl KeyMapCustomVecImpl for Vec<KeyMapCustom> {
    fn find_by_id(&self, id: &str) -> Option<&KeyMapCustom> {
        for km in self {
            if &km.id == id {
                return Some(km);
            }
        }
        return None;
    }

    fn find_by_id_and_event(&self, id: &str, event: &::wm::event_system::Event) -> Option<u32> {
        if let Some(km) = self.find_by_id(id) {
            if let Some(kmi) = km.items.find_from_event(event) {
                return Some(kmi.value)
            }
        }
        return None;
    }
}
