// Licensed: GPL v2+
// (c) Blender Foundation

use app::{
    rect,
};

// not _really_ like Blender's ARegion,
// just a place-holder for the window size
#[derive(Clone)]
pub struct ARegion {
    pub window_size: [u32; 2],
    pub winrct: rect::RectF,
}

impl Default for ARegion {
    fn default() -> Self {
        ARegion {
            window_size: [0, 0],
            winrct: Default::default(),
        }
    }
}
