// Licensed: GPL v2+

use ::std::any::Any;

use ::prelude::*;

use ::plain_ptr::{
    PtrMut,
    PtrConst,
};

pub struct UndoStep {
    /// Label to show in history
    pub name: String,
    /// Data owned by undo manager.
    pub data: Box<Any>,
    /// Convenience to store type here
    pub ty: PtrConst<UndoManagerType>
}

pub struct UndoManagerType {
    pub id: String,

    pub step_push: fn(
        PtrConst<App>,
        PtrMut<Env>,
        &mut [UndoStep],
        PtrConst<UndoManagerType>,
        &str,
    ) -> UndoStep,
    pub step_pop: fn(
        PtrConst<App>,
        PtrMut<Env>,
        PtrConst<UndoStep>,
    ) -> bool,
}
