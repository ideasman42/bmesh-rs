// Licensed: GPL v2+

use super::local_prelude::*;

use ::pxbuf_draw::PxBufDraw;

struct TestColor {
    image: PxBuf,
}

fn new_data(init: Box<Any>) -> Box<Any> {
    assert!(init.is::<()>());
    return Box::new(
        TestColor {
            image: PxBuf::new_empty(),
        }
    );
}

fn update(welem: &mut WElem, ctx: AppContextMutP) {
    let data = welem.custom_data.downcast_mut::<TestColor>().unwrap();

    let welem_size_x = welem.rect.size_x();
    let welem_size_y = welem.rect.size_y();

    if {
        let image_size = data.image.size();
        welem_size_x != image_size[0] ||
        welem_size_y != image_size[1]
    } {
        data.image.reshape_uninitialized(&[welem_size_x, welem_size_y]);
    }

    if false {
        data.image.draw_rect(
            0,
            &[0, 0],
            &[welem_size_x - 1, welem_size_y - 1],
        );
    } else {
        use std::cmp::min;
        let image_x = data.image.size()[0] as usize;

        // buffer (and window) width and height
        let buf_x: u32 = welem.rect.size_x() as u32;
        let buf_y: u32 = welem.rect.size_y() as u32;

        let slice = data.image.as_mut_slice_u32();
        for x in 0..buf_x {
            for y in 0..buf_y {
                let r: u32 = min(((buf_x - x) * 0xFF) / buf_x, ((buf_y - y) * 0xFF) / buf_y);
                let g: u32 = min((         x  * 0xFF) / buf_x, ((buf_y - y) * 0xFF) / buf_y);
                let b: u32 = min(((buf_x - x) * 0xFF) / buf_x, (         y  * 0xFF) / buf_y);
                let index = (image_x * y as usize) + x as usize;
                slice[index] = (0xFF << 24) + (r << 16) + (g << 8) + b;
            }
        }
    }

    {
        ctx.app.system.ui_font.draw_string(
            &mut data.image,
            rgb_hex_u32!(0xFF_FF_FF),
            &[10, 10],
            ctx.app.prefs.ui.scale_i32,
            &"Hello World".to_string(),
        );
    }

    welem.children.update(ctx);
}

fn composite(welem: &WElem, image: &mut PxBuf) {
    let data = welem.custom_data.downcast_ref::<TestColor>().unwrap();

    image.clone_from(&[welem.rect.xmin, welem.rect.ymin], &data.image);

    welem.children.composite(image);
}

pub fn new_type() -> WElemType {
    WElemType {
        id: "Test".to_string(),
        cb: WElemTypeFn {
            new_data: new_data,
            update: update,
            composite: composite,
            .. Default::default()
        },
    }
}

