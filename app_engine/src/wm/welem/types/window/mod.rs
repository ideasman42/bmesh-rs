// Licensed: GPL v2+

// Implement types
//
// We may want to split this up, for how its handy to keep them in one place.
// - Generic types at least.

// use ::prelude::*;

use super::super::local_prelude::*;

fn composite(welem: &WElem, image: &mut PxBuf) {
    // Clear first, assume we use the entire image always!
    // (since we're the window)
    if cfg!(debug_assertions) {
        image.clear(rgb_hex_u32!(0xFF_00_FF));
    }

    welem.children.composite(image);
}

pub fn handle_event(
    welem: &mut WElem, ctx: AppContextMutP, event: &Event,
    wchain_parent: WElemParentChainMutP,
) -> EventHandleState {

    let mut handle_state = EventHandleState::CONTINUE;

    // Very first handle global handlers,
    // typically modal operators which can block others
    {
        use ::wm::event_handler::handle_event_list;
        handle_state |= handle_event_list(ctx, &event, &mut welem.local_event_handlers);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    // unlike other recursive callbacks, run children first.
    {
        handle_state |= welem.children.handle_event(ctx, event, wchain_parent);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    // keymap - could make a generic handler.
    {
        use ::wm::event_handler::handle_event_keymap_by_id;
        handle_state |= handle_event_keymap_by_id(ctx, "Window", event);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    // hard to know at what level to handle this event,
    // for now handle last so any other handlers can compare before/after sizes.
    match event.id {
        ::wm::event_system::EventID::Window { ref action } => {
            match action {
                &::wm::event_system::WindowActionType::Resize(x, y) => {
                    welem.rect_set(
                        &RectI {
                            xmin: 0,
                            ymin: 0,
                            xmax: x as i32,
                            ymax: y as i32,
                        }.to_rect_f64(),
                    );
                },
                _ => {},
            }
        },
        _ => {},
    }

    return handle_state;
}

pub fn new_type() -> WElemType {
    WElemType {
        id: "Window".to_string(),
        cb: WElemTypeFn {
            composite: composite,
            handle_event: handle_event,
            .. Default::default()
        },
    }
}
