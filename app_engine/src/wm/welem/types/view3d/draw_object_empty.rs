// Licensed: GPL v2+

use pxbuf;
use super::pxbuf_draw3d;

pub fn draw_object(
    prefs: &::app::prefs::Prefs,
    draw_ctx: &pxbuf_draw3d::View3DDrawContext,
    ob_matrix: &[[f64; 4]; 4],
    color: u32,
    image: &mut pxbuf::PxBuf,
) {
    // TODO: bound-box test
    use math_misc::{
        mul_m4v3,
    };

    let edges: [[[f64; 3]; 2]; 3] = [
        [[-1.0, 0.0, 0.0], [1.0, 0.0, 0.0]],
        [[0.0, -1.0, 0.0], [0.0, 1.0, 0.0]],
        [[0.0, 0.0, -1.0], [0.0, 0.0, 1.0]],
    ];

    for e in edges.iter() {
        pxbuf_draw3d::line(
            image,
            &draw_ctx,
            &mul_m4v3(ob_matrix, &e[0]),
            &mul_m4v3(ob_matrix, &e[1]),
            color,
            color,
            prefs.ui.scale_i32,
        );
    }
}
