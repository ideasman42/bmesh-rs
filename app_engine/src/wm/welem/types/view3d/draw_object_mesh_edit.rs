// Licensed: GPL v2+

use pxbuf;
use super::pxbuf_draw3d;

use ::core::edit_mesh::prelude::BMEditMesh;

pub fn draw_object(
    prefs: &::app::prefs::Prefs,
    draw_ctx: &pxbuf_draw3d::View3DDrawContext,
    em: &BMEditMesh,
    ob_matrix: &[[f64; 4]; 4],
    image: &mut pxbuf::PxBuf,
) {
    use bmesh::prelude::*;
    use math_misc::{
        mul_m4v3,
    };

    let bm = &em.bm;

    // Will be user controllable
    let show_faces = true;
    let show_edges = true;

    if show_faces {
        for &(is_select_pass, color) in &[
            (false, prefs.theme.view3d.face_color),
            (true, prefs.theme.view3d.face_select_color),
        ] {
            for f in bm.faces.iter() {
                if !f.is_hide() && f.is_select() == is_select_pass {

                    // for now just fan-fill
                    let l_first = f.l_first;
                    let mut l_step = l_first.next;
                    let l_last = l_first.prev;
                    loop {
                        let l_step_next = l_step.next;
                        pxbuf_draw3d::tri(
                            image,
                            &draw_ctx,
                            &mul_m4v3(ob_matrix, &l_first.v.co),
                            &mul_m4v3(ob_matrix, &l_step.v.co),
                            &mul_m4v3(ob_matrix, &l_step_next.v.co),
                            color,
                        );
                        if l_step == l_last {
                            break;
                        }
                        l_step = l_step_next;
                    }
                }
            }
        }
    }

    // Edges (always draw)
    if show_edges {
        let use_blend = (bm.select_mode & BM_VERT) != 0;
        let color_select = prefs.theme.view3d.edge_select_color;
        let color_unselect = prefs.theme.view3d.wire_color;

        for &(is_select_pass,) in &[(false,), (true,)] {
            for e in bm.edges.iter() {
                if !e.is_hide() {
                    let (line0_color, line1_color) = {
                        if e.is_select() {
                            (color_select, color_select)
                        } else if use_blend {
                            if e.verts[0].is_select() {
                                (color_select, color_unselect)
                            } else if e.verts[1].is_select() {
                                (color_unselect, color_select)
                            } else {
                                // XXX, invalid selection flushing,
                                // for now just draw since issues here
                                // must be handled higher level.
                                (color_unselect, color_unselect)
                            }
                        } else {
                            (color_unselect, color_unselect)
                        }
                    };

                    // logic here is bit odd
                    // - first pass draw entirely unselected.
                    // - second pass draw partial or complete selection.
                    //
                    // Check colors just to avoid duplicating checks we already did.
                    //
                    if is_select_pass != (line0_color != color_unselect &&
                                          line1_color != color_unselect)
                    {
                        continue;
                    }

                    pxbuf_draw3d::line(
                        image,
                        &draw_ctx,
                        &mul_m4v3(ob_matrix, &e.verts[0].co),
                        &mul_m4v3(ob_matrix, &e.verts[1].co),
                        line0_color,
                        line1_color,
                        prefs.ui.scale_i32,
                    );
                }
            }
        }
    } else {
        // show_edges == false, just draw wire
        let color = prefs.theme.view3d.wire_color;
        for e in bm.edges.iter() {
            if !e.is_hide() {
                pxbuf_draw3d::line(
                    image,
                    &draw_ctx,
                    &mul_m4v3(ob_matrix, &e.verts[0].co),
                    &mul_m4v3(ob_matrix, &e.verts[1].co),
                    color,
                    color,
                    prefs.ui.scale_i32,
                );
            }
        }
    }

    if (bm.select_mode & BM_VERT) != 0 {
        for &(is_select_pass, color) in &[
            (false, prefs.theme.view3d.vert_color),
            (true, prefs.theme.view3d.vert_select_color),
        ] {
            for v in bm.verts.iter() {
                if !v.is_hide() && is_select_pass == v.is_select() {
                    pxbuf_draw3d::point(
                        image,
                        &draw_ctx,
                        &mul_m4v3(ob_matrix, &v.co),
                        color,
                        prefs.ui.scale_i32,
                    );
                }
            }
        }
    }

    if (bm.select_mode & BM_FACE) != 0 {
        for &(is_select_pass, color) in &[
            (false, prefs.theme.view3d.face_color),
            (true, prefs.theme.view3d.face_select_color),
        ] {
            for f in bm.faces.iter() {
                if !f.is_hide() && is_select_pass == f.is_select() {
                    let co = f.calc_center_median();
                    pxbuf_draw3d::point(
                        image,
                        &draw_ctx,
                        &mul_m4v3(ob_matrix, &co),
                        color,
                        prefs.ui.scale_i32,
                    );
                }
            }
        }
    }
}
