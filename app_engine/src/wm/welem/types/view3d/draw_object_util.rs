// Licensed: GPL v2+

use ::app::prefs::Prefs;
use ::core::dna::types::*;

pub fn object_wire_color(prefs: &Prefs, obj_inst: &ObjectInst, is_active: bool) -> u32 {
    if obj_inst.flag.select_test() {
        if is_active {
            prefs.theme.view3d.object_active_color
        } else {
            prefs.theme.view3d.object_select_color
        }
    }
    else {
        prefs.theme.view3d.object_color
    }
}
