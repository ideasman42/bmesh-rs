// Licensed: GPL v2+

///
/// Helper for drawing into the 3D viewport.
/// Just draw primitive shapes, no data-type specific functions,
/// callers will implement those.
///

use view3d;

use pxbuf;
use pxbuf_draw::PxBufDraw;

use math_misc::{
    mul_project_m4_v3_zfac,
    interp_v3v3,
};

use math_misc::{
    mul_m4v3,
};

use ::view3d::project::{
    ProjTestFlag,
};

/// Vars needed for drawing, avoids passing around too many args,
/// also allows for caching.
pub struct View3DDrawContext {
    pub persp_matrix: [[f64; 4]; 4],
    // needed for ortho mode only
//        pub view_vector: [f64; 3],
    pub window_size: [f64; 2],
    pub window_size_half: [f64; 2],
    pub clip_range_z: [f64; 2],
//        pub is_persp: bool,
}

impl View3DDrawContext {
    pub fn new_from_view3d(
        ar: &::wm::region::ARegion,
        v3d: &::view3d::View3D,
        rv3d: &::view3d::RegionView3D,
    ) -> View3DDrawContext {
        View3DDrawContext {
            persp_matrix: rv3d.matrix.persp,
//                view_vector: *as_v3!(&rv3d.matrix.view_inv[2]),
            window_size: [
                ar.window_size[0] as f64,
                ar.window_size[1] as f64,
            ],
            window_size_half: [
                ar.window_size[0] as f64 / 2.0,
                ar.window_size[1] as f64 / 2.0,
            ],
            clip_range_z: match rv3d.persp {
                view3d::ViewPersp::ORTHO => {
                    [v3d.far / -2.0, v3d.far / 2.0]
                },
                view3d::ViewPersp::CAMERA => {
                    // XXX, todo
                    [v3d.near, v3d.far]
                },
                // view3d::ViewPersp::PERSP => {
                _ => {
                    [v3d.near, v3d.far]
                },
            },
//                is_persp: rv3d.is_persp != 0,
        }
    }
}

#[inline]
fn project_i32_zfac(
    draw_ctx: &View3DDrawContext,
    co: &[f64; 3], co_zfac: f64,
    flag: ::view3d::project::ProjTestFlag,
) -> Option<[i32; 2]> {
    let vec3 = mul_m4v3(&draw_ctx.persp_matrix, &co);
    // caller needs to check zfac
    debug_assert!(co_zfac != 0.0);
    let scalar = 1.0 / co_zfac;
    let fx = (draw_ctx.window_size_half[0]) * (1.0 + (vec3[0] * scalar));
    if (flag & ProjTestFlag::CLIP_WIN) == 0 || fx > 0.0 && fx < draw_ctx.window_size[0] {
        let fy = draw_ctx.window_size_half[1] * (1.0 + (vec3[1] * scalar));
        if (flag & ProjTestFlag::CLIP_WIN) == 0 || fy > 0.0 && fy < draw_ctx.window_size[1] {
            return Some([
                fx as i32,
                fy as i32
            ]);
        }
    }

    return None;
}

#[inline]
pub fn point(
    image: &mut pxbuf::PxBuf,
    draw_ctx: &View3DDrawContext,

    point_co: &[f64; 3],
    point_color: u32,
    point_size: i32,
) {
    let co_zfac = mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, point_co);
    if co_zfac > draw_ctx.clip_range_z[0] && co_zfac < draw_ctx.clip_range_z[1] {
        if let Some(co_proj) = project_i32_zfac(
            draw_ctx,
            point_co, co_zfac, ProjTestFlag::CLIP_WIN)
        {
            image.draw_pixel_size(
                point_color,
                point_size,
                &co_proj,
            );
        }
    }
}

#[inline]
pub fn line(
    image: &mut pxbuf::PxBuf,
    draw_ctx: &View3DDrawContext,

    co0_orig: &[f64; 3],
    co1_orig: &[f64; 3],

    line0_color: u32,
    line1_color: u32,
    line_width: i32,
) {
    let mut co0_zfac = mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co0_orig);
    let mut co1_zfac = mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co1_orig);

    // XXX. zfac is 1.0 in ortho views
    // thats OK for projection, but not needed for clipping.

    /*
    let (mut co0_zfac, mut co1_zfac) = {
        if draw_ctx.is_persp {
            (mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co0_orig),
             mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co1_orig),
            )
        } else {
            // fixme
            ((dot_v3v3(&draw_ctx.view_vector, &co0_orig) - draw_ctx.persp_matrix[2][3]),
             (dot_v3v3(&draw_ctx.view_vector, &co1_orig) - draw_ctx.persp_matrix[2][3]),
            )
        }
    };
    */

    let mut co0_fac = 0.0;
    let mut co1_fac = 0.0;

    let co0_clip_any;
    let co1_clip_any;

    // use when clipped
    let co0_data: [f64; 3];
    let co1_data: [f64; 3];

    // We can likely avoid clipping in ortho mode -
    // but for now respect clipping in all cases.
    let use_clip = true;

    let (co0_ref, co1_ref) = if !use_clip {
        co0_clip_any = false;
        co1_clip_any = false;
        (co0_orig, co1_orig)
    } else {
        // clip the edge
        // near clipping
        let co0_clip_min = co0_zfac < draw_ctx.clip_range_z[0];
        let co1_clip_min = co1_zfac < draw_ctx.clip_range_z[0];
        if co0_clip_min && co1_clip_min {
            return;
        }

        // far clipping
        let co0_clip_max = co0_zfac > draw_ctx.clip_range_z[1];
        let co1_clip_max = co1_zfac > draw_ctx.clip_range_z[1];
        if co0_clip_max && co1_clip_max {
            return;
        }

        // instead of checking: 'co0_fac != 0.0'
        co0_clip_any = co0_clip_min || co0_clip_max;
        co1_clip_any = co1_clip_min || co1_clip_max;

        // override with clipped values as needed
        let mut co0 = co0_orig;
        let mut co1 = co1_orig;

        if co0_clip_any || co1_clip_any {

            let mut co0_zfac_new = co0_zfac;
            let mut co1_zfac_new = co1_zfac;

            if co0_clip_min && !co1_clip_min {
                co0_fac = (co0_zfac - draw_ctx.clip_range_z[0]) / (co0_zfac - co1_zfac);
                co0_zfac_new = draw_ctx.clip_range_z[0];
            } else if !co0_clip_min && co1_clip_min {
                co1_fac = (co1_zfac - draw_ctx.clip_range_z[0]) / (co1_zfac - co0_zfac);
                co1_zfac_new = draw_ctx.clip_range_z[0];
            }

            if co0_clip_max && !co1_clip_max {
                co0_fac = (draw_ctx.clip_range_z[1] - co0_zfac) / (co1_zfac - co0_zfac);
                co0_zfac_new = draw_ctx.clip_range_z[1];
            } else if !co0_clip_max && co1_clip_max {
                co1_fac = (draw_ctx.clip_range_z[1] - co1_zfac) / (co0_zfac - co1_zfac);
                co1_zfac_new = draw_ctx.clip_range_z[1];
            }

            if co0_clip_any {
                co0_data = interp_v3v3(co0_orig, co1_orig, co0_fac);
                co0 = &co0_data;
                co0_zfac = co0_zfac_new;
            }
            if co1_clip_any {
                co1_data = interp_v3v3(co1_orig, co0_orig, co1_fac);
                co1 = &co1_data;
                co1_zfac = co1_zfac_new;
            }
        }
        (co0, co1)
    };

/*
    if !draw_ctx.is_persp {
        co0_zfac = 1.0;
        co1_zfac = 1.0;
    }
*/

    if let Some(co_proj_a) = project_i32_zfac(
        draw_ctx,
        co0_ref, co0_zfac, view3d::project::ProjTestFlag::NOP,
    ) {
        if let Some(co_proj_b) = project_i32_zfac(
            draw_ctx,
            &co1_ref, co1_zfac, view3d::project::ProjTestFlag::NOP,
        ) {
            if line0_color == line1_color {
                image.draw_line_width_clip(
                    line0_color,
                    &co_proj_a, &co_proj_b,
                    line_width,
                );
            } else {
                macro_rules! blend {
                    ($a:expr, $b:expr, $use_fac:expr, $fac:expr) => {
                        {
                            use pxbuf;
                            // common case
                            if $use_fac == false {
                                $a
                            } else {
                                pxbuf::color::rgb_interp(
                                    $a,
                                    $b,
                                    ($fac * 255.0) as u32,
                                )
                            }
                        }
                    }
                }
                image.draw_line_width_blend_clip(
                    blend!(line0_color, line1_color, co0_clip_any, co0_fac),
                    blend!(line1_color, line0_color, co1_clip_any, co1_fac),
                    &co_proj_a, &co_proj_b,
                    line_width,
                );
            }
        }
    }
}

#[inline]
pub fn tri(
    image: &mut pxbuf::PxBuf,
    draw_ctx: &View3DDrawContext,

    co0_orig: &[f64; 3],
    co1_orig: &[f64; 3],
    co2_orig: &[f64; 3],

    color: u32,
) {
    let co0_zfac = mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co0_orig);
    let co1_zfac = mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co1_orig);
    let co2_zfac = mul_project_m4_v3_zfac(&draw_ctx.persp_matrix, co2_orig);

    // near clipping
    let co0_clip_min = co0_zfac < draw_ctx.clip_range_z[0];
    let co1_clip_min = co1_zfac < draw_ctx.clip_range_z[0];
    let co2_clip_min = co2_zfac < draw_ctx.clip_range_z[0];
    // for now we don't support partially behind the view!
    if co0_clip_min || co1_clip_min || co2_clip_min {
        return;
    }

    // far clipping
    let co0_clip_max = co0_zfac > draw_ctx.clip_range_z[1];
    let co1_clip_max = co1_zfac > draw_ctx.clip_range_z[1];
    let co2_clip_max = co2_zfac > draw_ctx.clip_range_z[1];
    // for now we don't support partially in front of the view!
    if co0_clip_max || co1_clip_max || co2_clip_max {
        return;
    }

    if let Some(co_proj_0) = project_i32_zfac(
        draw_ctx,
        &co0_orig, co0_zfac, view3d::project::ProjTestFlag::NOP,
    ) {
        if let Some(co_proj_1) = project_i32_zfac(
            draw_ctx,
            &co1_orig, co1_zfac, view3d::project::ProjTestFlag::NOP,
        ) {
            if let Some(co_proj_2) = project_i32_zfac(
                draw_ctx,
                &co2_orig, co2_zfac, view3d::project::ProjTestFlag::NOP,
            ) {
                image.draw_tri_clip(
                    color,
                    &co_proj_0,
                    &co_proj_1,
                    &co_proj_2,
                );
            }
        }
    }
}
