// Licensed: GPL v2+

mod pxbuf_draw3d;

mod draw_object_util;

mod draw_object_empty;
mod draw_object_mesh;
mod draw_object_mesh_edit;

use super::local_prelude::*;
use view3d;

struct View3DWElemData {
    image: PxBuf,
    // just for convenience
    ar: ::wm::region::ARegion,
    rv3d: view3d::RegionView3D,
    v3d: view3d::View3D,
}

fn new_data(init: Box<Any>) -> Box<Any>{
    assert!(init.is::<()>());

    return Box::new(
        View3DWElemData {
            // will reshape on first use
            image: PxBuf::new(&[0, 0], 0),
            ar: Default::default(),
            rv3d: {
                let mut rv3d = view3d::RegionView3D::default();

                rv3d.persp = view3d::ViewPersp::PERSP;
                rv3d.xform.offset[2] = -0.5;
                rv3d.xform.offset_dist = 10.0;
                {
                    use math_misc::{
                        axis_angle_to_quat,
                        imul_qtqt,
                    };

                    let q = axis_angle_to_quat(&[1.0, 0.0, 0.0], -45.0 * 0.017453292519943295);
                    imul_qtqt(&mut rv3d.xform.rotation, &q);
                }
                rv3d
            },
            v3d: {
                let v3d = view3d::View3D::default();
                v3d
            },
        }
    );
}

fn update(welem: &mut WElem, ctx: AppContextMutP) {

    let data = welem.custom_data.downcast_mut::<View3DWElemData>().unwrap();

    let env = ctx.env;

    let welem_size_x = welem.rect.size_x();
    let welem_size_y = welem.rect.size_y();

    data.ar.window_size[0] = welem_size_x as u32;
    data.ar.window_size[1] = welem_size_y as u32;
    data.ar.winrct = welem.rect.to_rect_f64();

    // env.view.ar = ar.clone();

    // TODO, only do this when its needed!
    {
        view3d::draw::update_view_matrix(
            &mut data.v3d, &data.ar, &mut data.rv3d, None, None);
    }

    let image = &mut data.image;
    let ref prefs = ctx.app.prefs;

    if {
        let image_size = image.size();
        welem_size_x != image_size[0] ||
        welem_size_y != image_size[1]
    } {
        image.reshape_uninitialized(&[welem_size_x, welem_size_y]);
    }

    image.clear(prefs.theme.view3d.background_color);

    {
        let draw_ctx = pxbuf_draw3d::View3DDrawContext::new_from_view3d(
            &data.ar,
            &data.v3d,
            &data.rv3d,
        );

        // TODO, have an active scene
        // for now just draw all
        let scene = env.scene;
        for is_select_pass in &[false, true] {
            for obj_lay in scene.object_layers.iter() {
                for obj_inst in obj_lay.objects.iter() {
                    use ::core::dna::types::{
                        Mesh,
                    };
                    if obj_inst.flag.select_test() == *is_select_pass {
                        use ::core::dna::consts::*;
                        let is_active = obj_inst == scene.object_inst_active.as_const();
                        let obj = obj_inst.object;
                        match obj.data_ty {
                            id_code::NUL => {
                                draw_object_empty::draw_object(
                                    prefs, &draw_ctx,
                                    &obj_inst.matrix,
                                    draw_object_util::object_wire_color(
                                        prefs, &*obj_inst, is_active),
                                    image,
                                );
                            },
                            id_code::MESH => {
                                if let Some(mesh) = obj.data.downcast_ref::<Mesh>() {
                                    if let Some(ref em) = mesh.rt.em {
                                        draw_object_mesh_edit::draw_object(
                                            prefs, &draw_ctx,
                                            &*em, &obj_inst.matrix,
                                            image,
                                        );
                                    } else {
                                        draw_object_mesh::draw_object(
                                            prefs, &draw_ctx,
                                            &*mesh, &obj_inst.matrix,
                                            draw_object_util::object_wire_color(
                                                prefs, &*obj_inst, is_active),
                                            image,
                                        );
                                    }
                                }
                            },
                            _ => {
                                println!("Unknown type: {:?}", obj.data_ty);
                            },
                        }
                    }
                }
            }
        }
    }

    welem.children.update(ctx);
}

fn composite(welem: &WElem, image: &mut PxBuf) {
    let data = welem.custom_data.downcast_ref::<View3DWElemData>().unwrap();

    image.clone_from(&[welem.rect.xmin, welem.rect.ymin], &data.image);

    welem.children.composite(image);
}

pub fn handle_event(
    welem: &mut WElem, ctx: AppContextMutP, event: &Event,
    wchain_parent: PtrMut<WElemParentChain>,
) -> EventHandleState {

    let mut handle_state = EventHandleState::CONTINUE;

    // Very first handle run-time events.
    // typically only added at top-level windows.
    {
        use ::wm::event_handler::handle_event_list;
        handle_state |= handle_event_list(ctx, &event, &mut welem.local_event_handlers);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    // child callbacks
    {
        handle_state |= welem.children.handle_event(ctx, event, wchain_parent);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    // keymap - could make a generic handler.
    {
        use ::wm::event_handler::handle_event_keymap_by_id;
        handle_state |= handle_event_keymap_by_id(ctx, "View 3D", event);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    if !ctx.bmesh().is_none() {
        use ::wm::event_handler::handle_event_keymap_by_id;
        handle_state |= handle_event_keymap_by_id(ctx, "Edit Mesh", event);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    } else {
        use ::wm::event_handler::handle_event_keymap_by_id;
        handle_state |= handle_event_keymap_by_id(ctx, "Object", event);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    return handle_state;
}

fn context_get(welem: &mut WElem, id: &str) -> Option<Box<Any>> {
    let data = welem.custom_data.downcast_mut::<View3DWElemData>().unwrap();
    match id {
        "region" => {
            return Some(Box::new(&mut data.ar as *mut _));
        },
        "rv3d" => {
            return Some(Box::new(&mut data.rv3d as *mut _));
        },
        "v3d" => {
            return Some(Box::new(&mut data.v3d as *mut _));
        },
        _ => {
            return None;
        }
    }
}

pub fn new_type() -> WElemType {
    WElemType {
        id: "View3D".to_string(),
        cb: WElemTypeFn {
            new_data: new_data,
            update: update,
            composite: composite,
            handle_event: handle_event,
            context_get: context_get,
            .. Default::default()
        },
    }
}
