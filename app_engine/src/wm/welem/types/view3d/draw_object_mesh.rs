// Licensed: GPL v2+

use pxbuf;
use super::pxbuf_draw3d;

use ::core::dna::types::Mesh;

pub fn draw_object(
    prefs: &::app::prefs::Prefs,
    draw_ctx: &pxbuf_draw3d::View3DDrawContext,
    me: &Mesh,
    ob_matrix: &[[f64; 4]; 4],
    color: u32,
    image: &mut pxbuf::PxBuf,
) {
    use math_misc::{
        mul_m4v3,
    };

    // Will be user controllable
    let show_edges = !me.edges.is_empty();
    let show_verts = !show_edges;

    if show_edges {
        for e in me.edges.iter() {
            pxbuf_draw3d::line(
                image,
                &draw_ctx,
                &mul_m4v3(ob_matrix, &me.verts[e.verts[0] as usize].co),
                &mul_m4v3(ob_matrix, &me.verts[e.verts[1] as usize].co),
                color,
                color,
                prefs.ui.scale_i32,
            );
        }
    }

    if show_verts {
        for v in me.verts.iter() {
            pxbuf_draw3d::point(
                image,
                &draw_ctx,
                &mul_m4v3(ob_matrix, &v.co),
                color,
                prefs.ui.scale_i32,
            );
        }
    }
}

