// Licensed: GPL v2+

use super::local_prelude::*;

use ::wm::menu::Menu;

// Menu events (only for navigation,
// most events are handled by the buttons themselves).
pub mod keymap_events {
    // Close the menu, typically Escape.
    pub const CLOSE: u32 = 1;
    // Cycle (arrow keys)
    pub const ACTIVE_NEXT: u32 = 2;
    pub const ACTIVE_PREV: u32 = 3;
}


struct MenuWElemData {
    image: PxBuf,

    // we may want to expose outside this module.
    menu: Menu,
}

fn new_data(init: Box<Any>) -> Box<Any>{

    let menu = {
        if let Ok(menu) = init.downcast::<Menu>() {
            *menu
        } else {
            unreachable!();
        }
    };

    return Box::new(
        MenuWElemData {
            image: PxBuf::new_empty(),
            // Vec::new(),
            menu: menu,
        }
    );
}

fn update(welem: &mut WElem, ctx: AppContextMutP) {
    let ref prefs = ctx.app.prefs;

    let data = welem.custom_data.downcast_mut::<MenuWElemData>().unwrap();

    let mut welem_size = welem.rect.size();

    let ui_unit = prefs.ui.elem_unit * prefs.ui.scale_f64;

    use ::ui::uielem::{
        UIPackState,
        UIDir,
        UIAlign,
    };
    use app::rect::RectF;


    {
        let pack_init = UIPackState {
            dir: UIDir::Vertical,
            align: UIAlign::Min,
            rect: RectF {
                xmin: 0.0,
                ymin: 0.0,
                // MENU_WIDTH
                xmax: ui_unit * 10.0,
                ymax: 0.0,
            },
        };

        data.menu.uielem.pack(&pack_init, &*ctx.app);
    }

    let menu_size = data.menu.uielem.rect.to_rect_i32().size();

    if menu_size != welem_size {
        welem.rect_fl = welem.rect_fl.resized(
            &[menu_size[0] as f64, menu_size[1] as f64],
        ).clamped(&ctx.window.welem_root.rect_fl).0;

        welem.rect = welem.rect_fl.to_rect_i32_round();
        welem_size = menu_size;
    }

    if welem_size != *data.image.size() {
        data.image.reshape_uninitialized(&welem_size);
    }

    data.image.clear(prefs.theme.ui.menu.background);

    data.menu.uielem.draw(&*ctx.app, &mut data.image);

    // empty for now
    assert!(welem.children.is_empty());
    // welem.children.update(ctx);
}

fn composite(welem: &WElem, image: &mut PxBuf) {
    let data = welem.custom_data.downcast_ref::<MenuWElemData>().unwrap();

    image.clone_from(&[welem.rect.xmin, welem.rect.ymin], &data.image);

    welem.children.composite(image);
}

pub fn handle_event(
    welem: &mut WElem, ctx: AppContextMutP, event: &Event,
    _wchain_parent: PtrMut<WElemParentChain>,
) -> EventHandleState {

    use ::ui::uielem::{
        UIActionKind,
    };

    let mut handle_state = EventHandleState::CONTINUE;

    // Very first handle global handlers,
    // typically modal operators which can block others
    {
        use ::wm::event_handler::handle_event_list;
        handle_state |= handle_event_list(ctx, &event, &mut welem.local_event_handlers);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    assert!(welem.children.is_empty());

    use ::wm::keymap::{
        KeyMapCustomVecImpl,
    };

    let data = welem.custom_data.downcast_mut::<MenuWElemData>().unwrap();

    if let Some(value) =
        ctx.app.prefs.input.keymaps_custom.find_by_id_and_event("UI Menu", event)
    {
        match value {
            keymap_events::CLOSE => {
                menu_queue_remove(ctx);
                return handle_state | EventHandleState::BREAK;
            },
            keymap_events::ACTIVE_NEXT => {
                menu_active_relative(data, 1);
                return handle_state | EventHandleState::BREAK;
            },
            keymap_events::ACTIVE_PREV => {
                menu_active_relative(data, -1);
                return handle_state | EventHandleState::BREAK;
            },
            _ => {
                panic!();
            }

        }
    }

    {

        let action_kind = {
            let (handle_state_new, action_kind) = data.menu.uielem.handle_event(
                ctx, &event, null_mut(), Some(&data.menu.ctx_store));
            handle_state |= handle_state_new;
            action_kind
        };

        match action_kind {
            UIActionKind::ExecComplete => {
                menu_queue_remove(ctx);
            },
            UIActionKind::Nop => {},

        }

        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }
    }

    return handle_state;
}

fn context_get(welem: &mut WElem, id: &str) -> Option<Box<Any>> {
    let data = welem.custom_data.downcast_mut::<MenuWElemData>().unwrap();
    match id {
        "menu" => {
            return Some(Box::new(&mut data.menu as *mut _));
        },
        _ => {
            return None;
        }
    }
}

pub fn new_type() -> WElemType {
    WElemType {
        id: "Menu".to_string(),
        cb: WElemTypeFn {
            new_data: new_data,
            update: update,
            composite: composite,
            handle_event: handle_event,
            context_get: context_get,
            .. Default::default()
        },
    }
}


// ----------------------------------------------------------------------------
// Utility functions

fn menu_queue_remove(mut ctx: AppContextMutP) {

    pub use ::wm::welem::{
        WElemAction,
    };

    use std_ext::SliceIndexOfExt;
    let mut welem_parent = ctx.welem_parent_mut();
    let welem = ctx.welem();
    let index = welem_parent.children.index_of(&*welem).unwrap();
    welem_parent.action_queue.push(
        WElemAction::RemoveWElem(index)
    );
}


fn menu_active_relative(data: &mut MenuWElemData, dir: i32) {
    use math_misc::ModuloSignedExt;

    use ::ui::uielem::UIElemFlag;
    let layout = data.menu.layout_mut();

    let mut index_old: i32 = -1;
    let mut i: i32 = 0;
    for uielem_child in &mut layout.children {
        if (uielem_child.flag & UIElemFlag::ACTIVE) != 0 {
            index_old = i;
            break;
        }
        i += 1;
    }

    let index_new;
    if index_old != -1 {
        layout.children[index_old as usize].flag &= !UIElemFlag::ACTIVE;
        index_new = (index_old + dir).modulo(layout.children.len() as i32);
    } else {
        // we might want to have some control over how this is handled
        // (no existing selection).
        index_new = layout.children.len() as i32 - 1;
    }

    layout.children[index_new as usize].flag |= UIElemFlag::ACTIVE;
}
