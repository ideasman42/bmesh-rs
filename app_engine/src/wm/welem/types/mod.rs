// Licensed: GPL v2+

pub mod local_prelude {
    pub use context::{
        AppContextMutP,
        AppContextConstP,
    };
    pub use wm::event_system::{
        Event,
    };
    pub use ::wm::welem::{
        WElemType,
        WElemTypeFn,
        WElemVecImpl,
        WElemParentChain,
        WElem,
    };
    pub use wm::event_handler::{
        EventHandleState,
    };
    pub use pxbuf::PxBuf;
    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };

    pub use ::std::any::Any;
}

/// Different window element types

pub mod test;
pub mod menu;
pub mod view3d;
pub mod window;
