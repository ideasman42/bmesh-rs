// Licensed: GPL v2+

///
/// This module defines a hierarchy of window elements.
///
/// The design is that - at a low level every region is based on a unified type.
/// So a window, menu, popup dialog, color picker... etc.
/// All share the same base window element type: `WElem`.
///
/// Note that this might seem like overly long document,
/// this just describes a general interface for window elements and goes into some details.
///
/// Motivation
/// ==========
///
/// Windowing can become involved especially
/// when handling interactions between different interface elements.
///
/// The intention with having a generalized window element which defines a general interface
/// for different window types which can build ontop of this.
///
/// This means we have a general way to perform image compositing and event handling.
///
/// WinElem API
/// ===========
///
/// Drawing (Compositing)
/// -------
///
/// eg: `welem.composite(&pixel_buffer);`
///
/// The draw callback takes a image and is responsible
/// for filling in the sub-region it occupies.
///
/// This allows them to alpha-blend over the existing image, or replace it entirely.
/// Note that there is nothing preventing them from writing outside their boundaries,
/// however in practice (internally) most/all draw functions will keep a cached image
/// exactly fitting its own bounds.
/// So a call to 'draw' will simply composite all the image buffers together.
///
/// While there is nothing stopping the draw calls from creating the entire image needed
/// for display on every draw call - this must be performed on read-only data.
///
/// Since drawing should never modify the internal state,
/// this allows for example, to multi-thread drawing (if thats event useful),
/// but in general is good practice and avoids bugs and feedback loops.
///
/// Other characteristics:
///
/// - WinElem's can form a tree structure where the window is the root node,
///   each WinElem owns its children.
/// - Note that each WinElem must draw within the bounds of its parent.
/// - Each draw call is responsible for calling its children's draw callbacks.
/// - When nothing has changed,
///   typically this function will `blit` a cached image data.
///
/// Updating
/// --------
///
/// eg: `welem.update(&context);`
///
/// Optionally re-generate image data,
/// can use own internal logic to check if the actual redraw is needed.
///
/// Notice that this doesn't write into any image as far as the caller is concerned.
///
/// Events
/// ------
///
/// eg: `handled = welem.handle_event(&mut context, &Event);`
///
/// Perform a recursive event handling for all window elements and their children.
///
/// Open Topics
/// ===========
///
/// Evaluation Order
/// ----------------
///
/// Parent/child order of drawing and event handling may need to be controlled.
///
/// Typically drawing/updating will first draw the parent,
/// then its children (allowing alpha-over).
///
/// Handling events will first check the children
/// (allowing popup menus or buttons to steal events from the parent).
///
/// There may be times especially with event handling -
/// where we want to handle the parent before the children...
/// if this is needed there could be stored in separate lists,
/// or use a flag and use 2x passes.
///
/// Possibly Generic API?
/// ---------------------
///
/// This API doesn't need to know what events are or event perform the actual pixel blending.
/// However it may be overkill it to attempt to make this more generic.
/// The option to do so can stay open, but for now its not a priority.
///
/// Extensions
/// ----------
///
/// Part of the motivation is to easily allow new code to define new windows.
/// This might include extensions/plugins.
/// The design should at least allow for new window types to be defined at runtime.
///
/// Persistence
/// -----------
///
/// How persistent should WElem's be, should a change to their layout modify them in-place?
/// or should they be immutable and re-create on every change.
///
/// While this is up to the user,
/// the API might work a little differently depending on which is chosen.
/// Suspect modify in place with occasional re-building
/// all data when window configuration changes.
///
/// Resizing/Re-Initializing
/// ------------------------
///
/// At what point are WElem's resized, floating popup for example?
/// For now 'update' can resize.
/// If this becomes a problem to mix resizing and buffer creation
/// we could have a 'layout' method where resizing is done.
///

/// Implementations of window types.
mod types;

mod local_prelude {
    pub use ::std::any::Any;
    pub use ::app::{
        App,
        WElemTypesContainerImpl,
    };

    pub use core::props::{
        PropGroup,
    };

    pub use context::{
        AppContextMutP,
        AppContextConstP,
    };

    pub use context_store::{
        AppContextStore,
    };

    pub use wm::event_system::{
        Event,
    };

    pub use wm::event_handler::{
        EventHandleState,
    };

    pub use pxbuf::{
        PxBuf,
    };

    pub use app::rect::{
        RectI,
        RectF,
    };

    pub use super::{
        WElem,
        WElemType,
        WElemTypeFn,
        WElemVecImpl,

        WElemParentChain,
        WElemParentChainMutP,
        WElemParentChainConstP,
    };

    pub use plain_ptr::{
        PtrMut,
        PtrConst,
        null_mut,
        null_const,
    };
}

use self::local_prelude::*;

pub mod keymap_events {
    pub use super::types::menu::keymap_events as menu;
}


/// Callbacks for window element types.
pub struct WElemTypeFn {
    pub new_data: fn(Box<Any>) -> Box<Any>,

    // we may want to pass in parents?
    pub update: fn(welem: &mut WElem, ctx: AppContextMutP),

    pub composite: fn(welem: &WElem, image: &mut PxBuf),

    // event is handled or not... maybe we want to have an enum for this.
    // see: EventHandleState
    pub handle_event: fn(
        welem: &mut WElem, ctx: AppContextMutP, event: &Event,
        wchain_parent: WElemParentChainMutP,
    ) -> EventHandleState,

    pub rect_set: fn(welem: &mut WElem, rect: &RectF),

    pub context_get: fn(welem: &mut WElem, id: &str) -> Option<Box<Any>>,
}

///
/// Default functions, so we can call children
/// without having to define callbacks just to ensure children are handled.
///
mod default_fn_impl {
    use super::local_prelude::*;

    pub fn new_data(init: Box<Any>) -> Box<Any> {
        assert!(init.is::<()>());
        return Box::new(());
    }

    pub fn update(welem: &mut WElem, ctx: AppContextMutP) {
        for welem_child in &mut welem.children {
            welem_child.update(ctx);
        }
    }

    pub fn composite(welem: &WElem, image: &mut PxBuf) {
        welem.children.composite(image);
    }

    pub fn handle_event(
        welem: &mut WElem, ctx: AppContextMutP, event: &Event,
        wchain_parent: WElemParentChainMutP,
    ) -> EventHandleState {

        use ::wm::event_handler::handle_event_list;

        let mut handle_state = handle_event_list(
            ctx, &event, &mut welem.local_event_handlers);
        if (handle_state & EventHandleState::BREAK) != 0 {
            return handle_state;
        }

        handle_state |= welem.children.handle_event(ctx, event, wchain_parent);
        return handle_state;
    }

    pub fn rect_set(welem: &mut WElem, rect: &RectF) {
        let rect_prev = welem.rect_fl.clone();
        welem.rect_fl = rect.clone();
        welem.rect = rect.to_rect_i32_round();
        welem.children.rect_set(&rect_prev, rect);
    }

    // Online others this isn't recursive
    pub fn context_get(_welem: &mut WElem, _id: &str) -> Option<Box<Any>> {
        return None;
    }
}

impl Default for WElemTypeFn {
    fn default() -> Self {
        WElemTypeFn {
            new_data:       default_fn_impl::new_data,
            update:         default_fn_impl::update,
            composite:      default_fn_impl::composite,
            handle_event:   default_fn_impl::handle_event,
            rect_set:       default_fn_impl::rect_set,
            context_get:    default_fn_impl::context_get,
        }
    }
}

pub struct WElemType {
    // needs to be unique!
    pub id: String,
    pub cb: WElemTypeFn,
}

/// Window Element
pub struct WElem {
    // XXX, not ideal to use pointer here.
    // but we want to get the system working and window types aren't typically
    // added/removed at run-time.
    pub elem_type: *const WElemType,

    // Data only the elem_type knows about.
    pub custom_data: Box<Any>,

    // Absolute screen space coords (in pixels).
    //
    // Floating point coords, int coords are derived from these
    // needed so resizing the window doesn't accumulate rounding errors.
    pub rect_fl: RectF,
    // Rounded version: 'rect_fl.to_rect_i32_round()'.
    pub rect: RectI,

    // children we own (inside 'rect' boundaries).
    pub children: Vec<WElem>,

    pub local_event_handlers: Vec<::wm::event_handler::EventHandler>,


    // Actions to perform,
    // currently limit this to running after event handling.
    // we may want to allow after updating too.
    //
    // This could be moved into an argument passed along to handle_event callback
    // but simplifies things to keep here.
    pub action_queue: Vec<WElemAction>,
}

impl WElem {

    pub fn new(app: &App, type_id: &str, type_data: Box<Any>, rect: RectF) -> Self {
        let welem_type = app.system.welem_types.find_by_id(type_id).unwrap();
        let rect_int = rect.to_rect_i32_round();
        return WElem {
            // XXX, Cast to pointer isn't ideal
            elem_type: welem_type as *const _,
            custom_data: (welem_type.cb.new_data)(type_data),
            rect_fl: rect,
            rect: rect_int,
            children: Vec::new(),
            local_event_handlers: Vec::new(),
            action_queue: Vec::new(),
        };
    }

    /// Handle this and typically its children,
    /// unless the callback chooses not to.
    pub fn update(self: &mut WElem, ctx: AppContextMutP) {
        let elem_type = unsafe { &(*self.elem_type) };
        (elem_type.cb.update)(self, ctx);
    }

    /// Handle this and typically its children,
    /// unless the callback chooses not to.
    pub fn composite(&self, image: &mut PxBuf) {
        let elem_type = unsafe { &(*self.elem_type) };
        (elem_type.cb.composite)(self, image);
    }

    fn handle_event_actions(&mut self, mut ctx: AppContextMutP, event: &Event) {
        for mut action in self.action_queue.drain(..) {
            match action {
                WElemAction::AddWElem(welem_add) => {
                    self.children.push(welem_add);
                },
                WElemAction::RemoveWElem(welem_index) => {
                    self.children.remove(welem_index);
                },
                WElemAction::Operator { ref id, ref props, ref mut ctx_store } => {
                    use ::wm::operator::{
                        OpContext,
                        invoke_by_id,
                    };
                    // see: handle_event_keymap_item
                    let wchain_prev = ctx.wchain;
                    ctx.wchain = ctx_store.wchain;

                    let _ret = invoke_by_id(
                        ctx,
                        &id,
                        OpContext::InvokeDefault,
                        props.clone(),
                        &event,
                    );

                    ctx.wchain = wchain_prev;
                },
            }
        }
    }

    /// Handle this and typically its children,
    /// unless the callback chooses not to.
    pub fn handle_event(
        &mut self, mut ctx: AppContextMutP, event: &Event,
        wchain_parent: PtrMut<WElemParentChain>,
    ) -> EventHandleState {
        let elem_type = unsafe { &(*self.elem_type) };
        let mut wchain = WElemParentChain::new(wchain_parent, PtrMut(self));
        ctx.wchain = PtrMut(&mut wchain);
        let handle_state = (elem_type.cb.handle_event)(self, ctx, event, ctx.wchain);
        ctx.wchain = wchain_parent;

        self.handle_event_actions(ctx, event);

        return handle_state;
    }

    /// Handle this and typically its children,
    /// unless the callback chooses not to.
    pub fn rect_set(&mut self, rect: &RectF) {
        let elem_type = unsafe { &(*self.elem_type) };
        (elem_type.cb.rect_set)(self, rect);
        debug_assert!(self.rect == self.rect_fl.to_rect_i32_round());
    }

    pub fn context_get(&mut self, id: &str) -> Option<Box<Any>> {
        let elem_type = unsafe { &(*self.elem_type) };
        return (elem_type.cb.context_get)(self, id);
    }
}

///
/// Special-case data type,
/// used for event handling so we can quickly search up the call stack of window elements.
///
/// These never persist, just for the handle_event callback.
///
#[derive(Clone)]
pub struct WElemParentChain {
    pub parent: PtrMut<WElemParentChain>,
    pub welem: PtrMut<WElem>,
}
pub type WElemParentChainMutP = PtrMut<WElemParentChain>;
pub type WElemParentChainConstP = PtrConst<WElemParentChain>;

impl WElemParentChain {
    pub fn new(
        parent: PtrMut<WElemParentChain>,
        welem: PtrMut<WElem>,
    ) -> WElemParentChain {
        WElemParentChain {
            parent: parent,
            welem: welem,
        }
    }

    pub fn parent_root(&self) -> PtrConst<WElem> {
        let mut wchain = PtrConst(self);
        while wchain.parent != null_mut() {
            wchain = wchain.parent.as_const();
        }
        return wchain.welem.as_const();
    }
    pub fn parent_root_mut(&mut self) -> PtrMut<WElem> {
        let mut wchain = PtrMut(self);
        while wchain.parent != null_mut() {
            wchain = wchain.parent;
        }
        return wchain.welem;
    }

    pub fn parent_level(&self, mut level: usize) -> PtrConst<WElem> {
        let mut wchain = PtrConst(self);
        while wchain.parent != null_mut() {
            if level == 0 {
                break;
            }
            level -= 1;
            wchain = wchain.parent.as_const();
        }
        return wchain.welem.as_const();
    }
    pub fn parent_level_mut(&self, mut level: usize) -> PtrMut<WElem> {
        let mut wchain = PtrConst(self);
        while wchain.parent != null_mut() {
            if level == 0 {
                break;
            }
            level -= 1;
            wchain = wchain.parent.as_const();
        }
        return wchain.welem;
    }



    /// Count the length, we could store it but its not needed very often
    /// and size isn't likely to be > 8.
    pub fn len_calc(&self) -> usize {
        let mut len = 0;
        let mut wchain = PtrConst(self);
        while wchain != null_mut() {
            wchain = wchain.parent.as_const();
            len += 1;
        }
        return len;
    }
    pub fn as_vec_mut(&mut self) -> Vec<*mut WElem> {
        let len = self.len_calc();
        let mut vec: Vec<*mut WElem> = Vec::with_capacity(len);
        let mut wchain = PtrConst(self);
        while wchain != null_mut() {
            vec.push(wchain.welem.as_ptr());
            wchain = wchain.parent.as_const();
            debug_assert!(!vec.last().unwrap().is_null());
        }
        debug_assert!(vec.len() == len);
        return vec;
    }

    // Storing parent in this context is redundant.
    pub fn as_vec_chain_mut(&mut self) -> Vec<WElemParentChain> {
        let len = self.len_calc();
        let mut vec: Vec<WElemParentChain> = Vec::with_capacity(len);
        let mut wchain = PtrMut(self);
        // NOTE: maybe code would read simpler is we _only_ use pointer asthmatic here
        while !wchain.is_null() {
            unsafe {
                let i = vec.len();
                let parent = PtrMut(vec.get_unchecked_mut(i));
                vec.push(WElemParentChain::new(parent, wchain.welem));
                wchain = (*wchain).parent;
            }
        }
        // cleanup last
        {
            vec.last_mut().unwrap().parent = null_mut();
        }
        debug_assert!(vec.len() == len);
        return vec;
    }


    // We could de-duplicate and transmute,
    // its just an array of pointers!
    /*
    pub fn as_vec(&self) -> Vec<*const WElem> {
        let len = self.len_calc();
        let mut vec: Vec<*const WElem> = Vec::with_capacity(len);
        let mut wchain = self as *const WElemParentChain;
        while !wchain.is_null() {
            unsafe {
                vec.push((*wchain).welem);
                wchain = (*wchain).parent;
                debug_assert!(!vec.last().unwrap().is_null());
            }
        }
        debug_assert!(vec.len() == len);
        return vec;
    }
    */

    /// Check all pointers are OK
    pub fn validate_vec_mut(_vec: &Vec<*mut WElem>, mut _welem_ref: &WElem) -> bool {
        // XXX TODO
        return true;
        // for welem in vec.iter().rev() {
        //     let welem != welem_ref as *mut _ {
        //         return false;
        //     }
        // }

    }
}




pub trait WElemVecImpl {
    fn update(&mut self, ctx: AppContextMutP);
    fn composite(&self, image: &mut PxBuf);
    fn handle_event(
        &mut self, ctx: AppContextMutP, event: &Event,
        wchain_parent: PtrMut<WElemParentChain>,
    ) -> EventHandleState;
    fn rect_set(&mut self, rect_src: &RectF, rect_dst: &RectF);
}

impl WElemVecImpl for Vec<WElem> {
    /// Handle only the children.
    fn update(&mut self, ctx: AppContextMutP) {
        for welem_child in self {
            welem_child.update(ctx);
        }
    }

    /// Handle only the children.
    fn composite(&self, image: &mut PxBuf) {
        for welem_child in self {
            welem_child.composite(image);
        }
    }

    /// Handle only the children.
    fn handle_event(
        &mut self, ctx: AppContextMutP, event: &Event,
        wchain_parent: PtrMut<WElemParentChain>,
    ) -> EventHandleState {
        let mut handle_state = EventHandleState::CONTINUE;
        for welem_child in self {
            if welem_child.rect.isect_point_f64(&event.state.co) {
                handle_state |= welem_child.handle_event(ctx, event, wchain_parent);
                if (handle_state & EventHandleState::BREAK) != 0 {
                    break;
                }
            }
        }
        return handle_state;
    }

    fn rect_set(&mut self, rect_src: &RectF, rect_dst: &RectF) {
        if !self.is_empty() {
            let rect_xform = rect_src.transform_precalc(&rect_dst);
            for welem_child in self {
                // we could cache the values needed for transforming.
                // but its not likely to be an issue in this context.
                let mut rect_new = rect_xform.rect(&welem_child.rect_fl);

                // unlikely needed, add this just in case float precision creeps outside
                // its parents bounds.
                rect_new.isect(rect_dst);

                welem_child.rect_set(&rect_new);
            }
        }
    }
}



// ----------------------------------------------------------------------------

// Actions must be executed backwards so removal works properly
// if we end up wanting overly complex scenarios - we may need to sort first,
// for now don't bother.
pub enum WElemAction {

    // Add this element to children.
    AddWElem(WElem),

    // Removes this index from children.
    RemoveWElem(usize),

    // Run an operator (possibly a bit of a misuse?)
    // we might want to keep WElemAction strictly for actions on window elms.
    Operator {
        id: String,
        props: Option<PropGroup>,
        ctx_store: AppContextStore,
    },
}


// ----------------------------------------------------------------------------


pub fn register_welems(app: &mut App) {
    app.system.welem_types.register(types::menu::new_type());
    app.system.welem_types.register(types::test::new_type());
    app.system.welem_types.register(types::view3d::new_type());
    app.system.welem_types.register(types::window::new_type());
}
