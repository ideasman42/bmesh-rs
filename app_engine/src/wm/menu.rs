// Licensed: GPL v2+

use ::prelude::*;

use ::wm::event_system::{
    Event,
};

pub struct MenuType {
    pub id: String,
    pub name: String,

    pub poll: Option<fn(ctx: AppContextConstP, menu: &MenuType) -> bool>,
    pub ui: fn(ctx: AppContextConstP, menu: &mut Menu),
}

pub struct Menu {
    // XXX, for now just list operators,
    // we will eventually want something more advanced
    // (operator properties, sub-menus... or just a generic UI layout)
    // ... one thing at a time!
    /*
    pub items: Vec<String>,
    pub index: i32,
    */

    // Always a UIElem with a UIElemLayout custom_data
    pub uielem: UIElem,

    // Menu it's self is window level,
    // need to store the context we were launched from.
    pub ctx_store: AppContextStore,
}

impl Menu {
    pub fn layout_mut(&mut self) -> &mut UIElemLayout {
        return self.uielem.custom_data.downcast_mut::<UIElemLayout>().unwrap();
    }

    pub fn layout(&mut self) -> &UIElemLayout {
        return self.uielem.custom_data.downcast_ref::<UIElemLayout>().unwrap();
    }
}


// ----------------------------------------------------------------------------
// Menu Opening Functions

// matching operator::invoke_by_ api
pub fn invoke_by_type(
    mut ctx: AppContextMutP,
    mt: &MenuType,
    event: &Event,
) -> bool {
    use app::{
        rect,
    };
    use ::wm::welem::{
        WElem,
        WElemAction,
    };
    use ::ui::uielem::{
        UIElemLayout,
    };
        // UIElem,

    if let Some(poll) = mt.poll {
        if !poll(ctx.as_const(), mt) {
            return false;
        }
    }

    let menu_center = event.state.co;
    let mut menu_data = Menu {
        /*
        items: Vec::new(),
        index: 0,
        */
        uielem: UIElem::new(
            &*ctx.app,
            "Layout",
            Box::new(UIElemLayout {
                .. Default::default()
            }),
            RectF::from_zero(),
        ),
        ctx_store: ctx.to_context_store(),
    };

    (mt.ui)(ctx.as_const(), &mut menu_data);

    let welem_new = WElem::new(
        &*ctx.app,
        "Menu",
        Box::new(menu_data),
        // resizing will correct zero area
        rect::RectF::from_point(&menu_center),
    );

    let mut welem_root = ctx.welem_root_mut();
    welem_root.action_queue.push(WElemAction::AddWElem(welem_new));

    return true;
}

pub fn invoke_by_id(
    ctx: AppContextMutP,
    mt_id: &str,
    event: &Event,
) -> bool {
    if let Some(mt) = ctx.app.system.menu_types.find_by_id(mt_id) {
        return invoke_by_type(ctx, mt, event);
    } else {
        println!("invoke_by_id: no menu named '{}'", mt_id);
        return false;
    }
}
