// Licensed: GPL v2+

use ::std::any::Any;

use ::prelude::*;

use ::plain_ptr::{
    PtrMut,
    PtrConst,
    null_mut,
    null_const,
};
use ::core::props::{
    PropGroup,
};
use ::core::rna::define::{
    StructRNA,
    PointerRNA,
};

use ::wm::event_system::Event;

use ::wm::event_handler::{
    EventHandleState,
};

use ::wm::undo_manager::{
    UndoManagerType,
};

// State of the operator
struct_bitflag_impl!(pub struct OpState(pub u16));
impl OpState {
    pub const RUNNING_MODAL     : Self = OpState(1 << 0);
    pub const CANCELLED         : Self = OpState(1 << 1);
    pub const FINISHED          : Self = OpState(1 << 2);
    pub const PASS_THROUGH      : Self = OpState(1 << 3);
    pub const HANDLED           : Self = OpState(1 << 4);
}

// like Blender's WM_OP_INVOKE_DEFAULT .. etc.
pub enum OpContext {
    // if there's invoke, call it, otherwise exec
    InvokeDefault = 1,
    InvokeRegionWin,
    InvokeRegionChannels,
    InvokeRegionPreview,
    InvokeArea,
    InvokeScreen,

    // only call exec
    ExecDefault,
    ExecRegionWin,
    ExecRegionChannels,
    ExecRegionPreview,
    ExecArea,
    ExecScreen,
}

pub struct OperatorType {
    pub id: String,
    pub name: String,

    pub poll: Option<fn(AppContextConstP) -> bool>,
    pub exec: Option<fn(AppContextMutP, &mut Operator) -> OpState>,
    pub invoke: Option<fn(AppContextMutP, &mut Operator, event: &Event) -> OpState>,

    pub modal: Option<fn(AppContextMutP, &mut Operator, event: &Event) -> OpState>,

    /// Return the undo manager or None.
    pub undo: Option<fn(AppContextConstP) -> PtrConst<UndoManagerType>>,

    /// Definition for property access
    /// (for operators that have properties).
    pub srna: StructRNA,

}

impl ::std::fmt::Debug for OperatorType {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(
            f, "OperatorType: <{} at 0x{:x}>",
            self.id, self as *const _ as usize,
        )?;
        Ok(())
    }
}

impl Default for OperatorType {
    fn default() -> Self {
        OperatorType {
            // must always be overridden, check on 'register'
            id: Default::default(),
            name: Default::default(),

            poll: None,
            exec: None,
            invoke: None,
            modal: None,
            undo: None,

            srna: Default::default(),
        }
    }
}

pub struct Operator {
    ty: PtrConst<OperatorType>,
    /// Optional private data for the operator's own use.
    pub custom_data: Option<Box<Any>>,

    /// Access via 'props()'.
    prop_data: PropGroup,
}

impl Default for Operator {
    fn default() -> Self {
        Operator {
            ty: null_const(),
            custom_data: None,
            prop_data: PropGroup::new(),
        }
    }
}

impl Operator {
    // wm_operator_create in Blender
    fn new_with_data(
        ot: &OperatorType, props: Option<PropGroup> /* TODO: reports */
    ) -> Operator {
        let mut op = Operator::default();
        op.ty = PtrConst(ot);
        if let Some(props) = props {
            op.prop_data = props;
        }
        return op;
    }

    pub fn props(&mut self) -> PointerRNA {
        return self.ty.srna.pointer_create(null_mut(), PtrMut(&mut self.prop_data));
    }

}

pub fn invoke_by_type(
    mut ctx: AppContextMutP, ot: &OperatorType, _wm_context: OpContext, props: Option<PropGroup>,
    event: &Event,
) -> OpState {
    use self::operator_handler::EventHandlerVecForOperator;

    if let Some(poll) = ot.poll {
        if !poll(ctx.as_const()) {
            return OpState::CANCELLED;
        }
    }

    let mut op = Operator::new_with_data(ot, props);

    if let Some(invoke) = ot.invoke {
        let op_state = invoke(ctx, &mut op, event);
        if (op_state & OpState::FINISHED) != 0 {
            if let Some(undo) = ot.undo {
                let undo_mgr = undo(ctx.as_const());
                let app = ctx.app.clone();
                ctx.env.undo_push(app, undo_mgr, &ot.name);
            }
        }

        if (op_state & OpState::RUNNING_MODAL) != 0 {
            // let mut window = ctx.window_mut();
            println!("Adding operator");
            let mut welem = ctx.welem_root_mut();
            let ctx_store = ctx.to_context_store();
            welem.local_event_handlers.add_operator(op, ot, ctx_store);
        }
        return op_state;
    } else if let Some(exec) = ot.exec {
        let op_state = exec(ctx, &mut op);
        if (op_state & OpState::FINISHED) != 0 {
            if let Some(undo) = ot.undo {
                let undo_mgr = undo(ctx.as_const());
                let app = ctx.app.clone();
                ctx.env.undo_push(app, undo_mgr, &ot.name);
            }
        }
        return op_state;
    } else {
        println!("Operator has no exec/invoke");
        return OpState::CANCELLED;
    }
}

pub fn exec_by_type(
    mut ctx: AppContextMutP, ot: &OperatorType, _wm_context: OpContext, props: Option<PropGroup>,
) -> OpState {

    if let Some(poll) = ot.poll {
        if !poll(ctx.as_const()) {
            return OpState::CANCELLED;
        }
    }

    let mut op = Operator::new_with_data(ot, props);

    if let Some(exec) = ot.exec {
        let op_state = exec(ctx, &mut op);
        if (op_state & OpState::FINISHED) != 0 {
            if let Some(undo) = ot.undo {
                let undo_mgr = undo(ctx.as_const());
                let app = ctx.app.clone();
                ctx.env.undo_push(app, undo_mgr, &ot.name);
            }
        }
        return op_state;
    } else {
        println!("Operator has no exec/invoke");
        return OpState::CANCELLED;
    }
}


pub fn invoke_by_id(
    ctx: AppContextMutP, ot_id: &str, wm_context: OpContext, props: Option<PropGroup>,
    event: &Event,
) -> OpState {
    let app = ctx.app;
    if let Some(ot) = app.system.operator_types.find_by_id(ot_id) {
        return invoke_by_type(ctx, ot, wm_context, props, event);
    } else {
        println!("Can't find operator: {}", ot_id);
        return OpState::CANCELLED;
    }
}

pub fn call_by_id(
    ctx: AppContextMutP, ot_id: &str, wm_context: OpContext, props: Option<PropGroup>,
) -> OpState {
    let app = ctx.app;
    if let Some(ot) = app.system.operator_types.find_by_id(ot_id) {
        return exec_by_type(ctx, ot, wm_context, props);
    } else {
        println!("Can't find operator: {}", ot_id);
        return OpState::CANCELLED;
    }
}

pub fn handler_state_from_op_state_invoke(op_state: OpState) -> EventHandleState {

    // may want to re-visit this.
    // logic seems reasonable for now,
    // this isn't based on Blender's code).
    if (op_state & OpState::FINISHED | OpState::CANCELLED) != 0 {
        return EventHandleState::HANDLED | EventHandleState::BREAK;
    } else if (op_state & OpState::PASS_THROUGH) != 0 {
        return EventHandleState::CONTINUE;
    } else {
        panic!();
    }
}

pub fn handler_state_from_op_state_modal(op_state: OpState) -> (EventHandleState, bool) {
    let remove = (op_state & (OpState::FINISHED | OpState::CANCELLED)) != 0;

    // Finished and pass through flag as handled
    if op_state == (OpState::FINISHED | OpState::PASS_THROUGH) {
        // Finished and pass through flag as handled
        return (EventHandleState::HANDLED, remove);
    } else if op_state == (OpState::PASS_THROUGH | OpState::RUNNING_MODAL) {
        // Modal unhandled, break
        return (EventHandleState::BREAK | EventHandleState::MODAL, remove);
    } else if (op_state & OpState::PASS_THROUGH) != 0 {
        return (EventHandleState::CONTINUE, remove);
    } else {
        return (EventHandleState::BREAK, remove);
    }
}

// ---------------------------
// Implement operator handlers

mod operator_handler {
    use ::std::any::Any;

    use super::{
        Operator,
        OperatorType,
        handler_state_from_op_state_modal,
    };
    use ::prelude::{
        AppContextMutP,
        AppContextStore,
    };
    use ::wm::event_handler::{
        EventHandler,
        EventHandleState,
    };
    use ::wm::event_system::{
        Event,
    };

    struct OpHandler {
        op: Operator,
        ot: *const OperatorType,
        // op_area: &mut ScrArea,
        // op_region: *mut ::wm::region::ARegion,
        // op_region_type
        ctx_store: AppContextStore,
    }

    pub trait EventHandlerVecForOperator {
        fn add_operator(
            &mut self,
            op: Operator,
            ot: &OperatorType,
            ctx_store: AppContextStore,
        );
    }

    // wm_handler_operator_call in Blender
    fn handler_operator_call(
        mut ctx: AppContextMutP,
        event: &Event,
        user_data: &mut Box<Any>,
    ) -> (EventHandleState, bool) {
        let user_data = user_data.downcast_mut::<OpHandler>().unwrap();
        // safe in practice - we dont re-register operator types
        let ot = unsafe { &*user_data.ot };

        let wchain_prev = ctx.wchain;

        ctx.wchain = user_data.ctx_store.wchain;

        let op_state = (ot.modal.unwrap())(ctx, &mut user_data.op, event);

        ctx.wchain = wchain_prev;

        return handler_state_from_op_state_modal(op_state);
    }

    impl EventHandlerVecForOperator for Vec<EventHandler> {
        fn add_operator(
            &mut self,
            op: Operator,
            ot: &OperatorType,
            ctx_store: AppContextStore,
        ) {
            use ::wm::event_handler::EventHandlerVec;

            let user_data = OpHandler {
                op: op,
                ot: ot as *const _,
                // op_region: ptr::null_mut(), //(*ctx.env).view.region,
                ctx_store: ctx_store,
            };

            self.add(handler_operator_call, Box::new(user_data));
        }
    }
}

#[derive(Debug, Clone)]
pub struct OpAction {
    pub ty: PtrConst<OperatorType>,
    /// Access via 'props()'.
    pub prop_data: PropGroup,
}

impl OpAction {
    pub fn from_id(app: PtrConst<App>, id: &str) -> OpAction {
        if let Some(ty) = app.system.operator_types.find_by_id(id) {
            return OpAction {
                ty: PtrConst(ty),
                prop_data: PropGroup::new(),
            }
        }
        println!("Operator not found {}", id);
        unreachable!();
    }

    pub fn from_ty(ty: &OperatorType) -> OpAction {
        return OpAction {
            ty: PtrConst(ty),
            prop_data: PropGroup::new(),
        }
    }

    pub fn props(&mut self) -> PointerRNA {
        return self.ty.srna.pointer_create(null_mut(), PtrMut(&mut self.prop_data));
    }
}
