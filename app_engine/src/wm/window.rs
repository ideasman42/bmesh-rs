// Licensed: GPL v2+

use pxbuf;
use wm;

pub struct Win {
    // Every window has one root window element.
    pub welem_root: wm::welem::WElem,
}

impl Win {
    pub fn new(
        welem: wm::welem::WElem,
    ) -> Win {
        Win {
            welem_root: welem,
        }
    }

    /// Create a pxbuf compatible with this window,
    /// (typically will be reused).
    pub fn pxbuf_create(&self) -> pxbuf::PxBuf {
        // we may want to box this?
        return pxbuf::PxBuf::new_empty();
    }
}

