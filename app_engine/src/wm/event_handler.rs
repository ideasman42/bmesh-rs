// Licensed: GPL v2+

use ::prelude::*;

use ::wm::event_system::Event;

/// General event handler which can be stored at different levels
/// (window, region... etc)

struct_bitflag_impl!(pub struct EventHandleState(pub u8));
impl EventHandleState {
    pub const CONTINUE: Self = EventHandleState(0);
    pub const BREAK:    Self = EventHandleState(1 << 0);
    pub const HANDLED:  Self = EventHandleState(1 << 1);
    // MODAL|BREAK means unhandled
    pub const MODAL:    Self = EventHandleState(1 << 2);
}

use ::std::any::Any;

type EventHandlerFn = fn(
    ctx: AppContextMutP, event: &Event, user_data: &mut Box<Any>,
) -> (EventHandleState, bool);

/// Use a vector of this
pub struct EventHandler {
    pub handle_event: EventHandlerFn,
    pub user_data: Box<Any>,
}

pub trait EventHandlerVec {
    fn add(
        &mut self,
        handle_event: EventHandlerFn,
        user_data: Box<Any>,
    );
}

impl EventHandlerVec for Vec<EventHandler> {
    fn add(
        &mut self,
        handle_event: EventHandlerFn,
        user_data: Box<Any>,
    ) {
        self.push(
            EventHandler {
                handle_event: handle_event,
                user_data: user_data,
            }
        );
    }
}

pub fn handle_event_list(
    ctx: AppContextMutP, event: &Event, event_handlers: &mut Vec<EventHandler>
) -> EventHandleState {
    let mut handle_state = EventHandleState::CONTINUE;
    let mut i = 0;
    while i < event_handlers.len() {

        let (handle_state_flag, remove) = {
            let h = &mut event_handlers[i];
            (h.handle_event)(ctx, event, &mut h.user_data)
        };

        handle_state |= handle_state_flag;

        if remove {
            event_handlers.remove(i);
        } else {
            i += 1;
        }

        if (handle_state & EventHandleState::BREAK) != 0 {
            break;
        }
    }

    return handle_state;
}

pub fn handle_event_keymap_item(
    ctx: AppContextMutP, event: &Event, kmi: &KeyMapItem,
) -> EventHandleState {
    let mut handle_state = EventHandleState::CONTINUE;
    match kmi.action {

        KeyMapAction::Operator(ref op_act) => {
            use ::wm::operator::{
                OpContext,
                handler_state_from_op_state_invoke,
                invoke_by_type,
            };
            println!("Trying to run: {} ({:?})", op_act.ty.id, op_act.prop_data);
            let ret = invoke_by_type(
                ctx,
                &*op_act.ty,
                OpContext::InvokeDefault,
                Some(op_act.prop_data.clone()),
                &event,
            );

            // XXX, assume it worked!
            handle_state |= handler_state_from_op_state_invoke(ret);
        },

        KeyMapAction::Menu { ref id } => {
            let _id = id;
            handle_state |= EventHandleState::HANDLED | EventHandleState::BREAK;
        },

        KeyMapAction::Dummy { ref text } => {
            println!("Dummy keymap action: {}", text);
            handle_state |= EventHandleState::HANDLED | EventHandleState::BREAK;
        },
    }

    return handle_state;
}

pub fn handle_event_keymap_by_id(
    ctx: AppContextMutP, keymap_id: &str, event: &Event,
) -> EventHandleState {

    let mut handle_state = EventHandleState::CONTINUE;

    // keymap - could make a generic handler.
    {
        use ::wm::event_handler::{
            EventHandleState,
            handle_event_keymap_item,
        };
        use ::wm::keymap::{
            KeyMapVecImpl,
            KeyMapItemVecImpl,
        };


        // println!("{:?}", event);
        let ref prefs = ctx.app.prefs;

        if let Some(km) = prefs.input.keymaps.find_by_id(keymap_id) {
            if let Some(kmi) = km.items.find_from_event(event) {
                println!("Found match {:?}!", kmi);
                handle_state |= handle_event_keymap_item(ctx, event, kmi);
                if (handle_state & EventHandleState::BREAK) != 0 {
                    return handle_state;
                }
            }
        } else {
            println!("Warning: unknown keymap '{}'", keymap_id);
        }

        return handle_state;
    }
}
