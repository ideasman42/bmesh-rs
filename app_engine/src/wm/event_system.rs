// Licensed: GPL v2+

use ::std::collections::HashSet;

// --------------------------------------------------------------
// Tedious definitions

struct_enum_int_impl!(pub struct PointerButton(pub u16));
impl PointerButton {
    pub const LEFT            : Self = PointerButton(0);
    pub const RIGHT           : Self = PointerButton(1);
    pub const MIDDLE          : Self = PointerButton(2);
    pub const BUTTON_4        : Self = PointerButton(3);
    pub const BUTTON_5        : Self = PointerButton(4);
}

impl PointerButton {
    pub fn as_map_event(&self) -> Option<::wm::keymap::MapCode> {
        return Some(::wm::keymap::MapCode(
            ::wm::keymap::map_event_range::POINTER.start + self.0));
    }
}


struct_bitflag_impl!(pub struct KeyMod(pub u16));
impl KeyMod {
    pub const LCTRL           : Self = KeyMod(1 << 0);
    pub const RCTRL           : Self = KeyMod(1 << 1);
    pub const LALT            : Self = KeyMod(1 << 2);
    pub const RALT            : Self = KeyMod(1 << 3);
    pub const LSHIFT          : Self = KeyMod(1 << 4);
    pub const RSHIFT          : Self = KeyMod(1 << 5);
    pub const LGUI            : Self = KeyMod(1 << 6);
    pub const RGUI            : Self = KeyMod(1 << 7);

    // we don't use these yet!
    /*
    pub const NUM             : Self = KeyMod(0x1000);
    pub const CAPS            : Self = KeyMod(0x2000);
    pub const MODE            : Self = KeyMod(0x4000);
    */
}

pub mod scan_code_range {
    use ::std::ops::Range;
    use super::ScanCode;
    pub const MAP: Range<u16> = 10..512;
    pub const MOD: Range<u16> = ScanCode::LEFT_CTRL.0..(ScanCode::RIGHT_SUPER.0 + 1);
}

/// Note that values must be between 0-512

struct_enum_int_impl!(pub struct ScanCode(pub u16));
impl ScanCode {
    pub const UNKNOWN        : Self = ScanCode(0x0000); // 0
    pub const CAPS_LOCK      : Self = ScanCode(0x0001); // 1
    pub const NUM_LOCK       : Self = ScanCode(0x0002); // 2
    pub const SCROLL_LOCK    : Self = ScanCode(0x0003); // 3

    // end scan_code_ranges::MAP

    pub const A              : Self = ScanCode(0x0061); // 'a'
    pub const B              : Self = ScanCode(0x0062); // 'b'
    pub const C              : Self = ScanCode(0x0063); // 'c'
    pub const D              : Self = ScanCode(0x0064); // 'd'
    pub const E              : Self = ScanCode(0x0065); // 'e'
    pub const F              : Self = ScanCode(0x0066); // 'f'
    pub const G              : Self = ScanCode(0x0067); // 'g'
    pub const H              : Self = ScanCode(0x0068); // 'h'
    pub const I              : Self = ScanCode(0x0069); // 'i'
    pub const J              : Self = ScanCode(0x006A); // 'j'
    pub const K              : Self = ScanCode(0x006B); // 'k'
    pub const L              : Self = ScanCode(0x006C); // 'l'
    pub const M              : Self = ScanCode(0x006D); // 'm'
    pub const N              : Self = ScanCode(0x006E); // 'n'
    pub const O              : Self = ScanCode(0x006F); // 'o'
    pub const P              : Self = ScanCode(0x0070); // 'p'
    pub const Q              : Self = ScanCode(0x0071); // 'q'
    pub const R              : Self = ScanCode(0x0072); // 'r'
    pub const S              : Self = ScanCode(0x0073); // 's'
    pub const T              : Self = ScanCode(0x0074); // 't'
    pub const U              : Self = ScanCode(0x0075); // 'u'
    pub const V              : Self = ScanCode(0x0076); // 'v'
    pub const W              : Self = ScanCode(0x0077); // 'w'
    pub const X              : Self = ScanCode(0x0078); // 'x'
    pub const Y              : Self = ScanCode(0x0079); // 'y'
    pub const Z              : Self = ScanCode(0x007A); // 'z'

    pub const NUM_0          : Self = ScanCode(0x0030); // '0'
    pub const NUM_1          : Self = ScanCode(0x0031); // '1'
    pub const NUM_2          : Self = ScanCode(0x0032); // '2'
    pub const NUM_3          : Self = ScanCode(0x0033); // '3'
    pub const NUM_4          : Self = ScanCode(0x0034); // '4'
    pub const NUM_5          : Self = ScanCode(0x0035); // '5'
    pub const NUM_6          : Self = ScanCode(0x0036); // '6'
    pub const NUM_7          : Self = ScanCode(0x0037); // '7'
    pub const NUM_8          : Self = ScanCode(0x0038); // '8'
    pub const NUM_9          : Self = ScanCode(0x0039); // '9'


    pub const LEFT_CTRL      : Self = ScanCode(0x00D4); // 212
    pub const RIGHT_CTRL     : Self = ScanCode(0x00D5); // 213
    pub const LEFT_ALT       : Self = ScanCode(0x00D6); // 214
    pub const RIGHT_ALT      : Self = ScanCode(0x00D7); // 215
    pub const LEFT_SHIFT     : Self = ScanCode(0x00D8); // 216
    pub const RIGHT_SHIFT    : Self = ScanCode(0x00D9); // 217
    pub const LEFT_SUPER     : Self = ScanCode(0x00DA); // 218
    pub const RIGHT_SUPER    : Self = ScanCode(0x00DB); // 219

    pub const ESC            : Self = ScanCode(0x00DC); // 220
    pub const TAB            : Self = ScanCode(0x00DD); // 221
    pub const RET            : Self = ScanCode(0x00DE); // 222
    pub const SPACE          : Self = ScanCode(0x00DF); // 223
    pub const BACKSPACE      : Self = ScanCode(0x00E1); // 225
    pub const DEL            : Self = ScanCode(0x00E2); // 226
    pub const SEMICOLON      : Self = ScanCode(0x00E3); // 227
    pub const PERIOD         : Self = ScanCode(0x00E4); // 228
    pub const COMMA          : Self = ScanCode(0x00E5); // 229
    pub const QUOTE          : Self = ScanCode(0x00E6); // 230
    pub const ACCENT_GRAVE   : Self = ScanCode(0x00E7); // 231
    pub const MINUS          : Self = ScanCode(0x00E8); // 232
    pub const PLUS           : Self = ScanCode(0x00E9); // 233
    pub const SLASH          : Self = ScanCode(0x00EA); // 234
    pub const BACKSLASH      : Self = ScanCode(0x00EB); // 235
    pub const EQUAL          : Self = ScanCode(0x00EC); // 236
    pub const LEFT_BRACKET   : Self = ScanCode(0x00ED); // 237
    pub const RIGHT_BRACKET  : Self = ScanCode(0x00EE); // 238

    pub const LEFT_ARROW     : Self = ScanCode(0x0089); // 137
    pub const DOWN_ARROW     : Self = ScanCode(0x008A); // 138
    pub const RIGHT_ARROW    : Self = ScanCode(0x008B); // 139
    pub const UP_ARROW       : Self = ScanCode(0x008C); // 140

    pub const PAD_0          : Self = ScanCode(0x0096); // 150
    pub const PAD_1          : Self = ScanCode(0x0097); // 151
    pub const PAD_2          : Self = ScanCode(0x0098); // 152
    pub const PAD_3          : Self = ScanCode(0x0099); // 153
    pub const PAD_4          : Self = ScanCode(0x009A); // 154
    pub const PAD_5          : Self = ScanCode(0x009B); // 155
    pub const PAD_6          : Self = ScanCode(0x009C); // 156
    pub const PAD_7          : Self = ScanCode(0x009D); // 157
    pub const PAD_8          : Self = ScanCode(0x009E); // 158
    pub const PAD_9          : Self = ScanCode(0x009F); // 159

    pub const PAD_PERIOD     : Self = ScanCode(0x00C7); // 199
    pub const PAD_ASTER      : Self = ScanCode(0x00A0); // 160
    pub const PAD_SLASH      : Self = ScanCode(0x00A1); // 161
    pub const PAD_MINUS      : Self = ScanCode(0x00A2); // 162
    pub const PAD_ENTER      : Self = ScanCode(0x00A3); // 163
    pub const PAD_PLUS       : Self = ScanCode(0x00A4); // 164

    pub const PAUSE          : Self = ScanCode(0x00A5); // 165
    pub const INSERT         : Self = ScanCode(0x00A6); // 166
    pub const HOME           : Self = ScanCode(0x00A7); // 167
    pub const PAGE_UP        : Self = ScanCode(0x00A8); // 168
    pub const PAGE_DOWN      : Self = ScanCode(0x00A9); // 169
    pub const END            : Self = ScanCode(0x00AA); // 170
    pub const PRINT_SCREEN   : Self = ScanCode(0x00AB); // 171

    pub const GRLESS         : Self = ScanCode(0x00AE); // 174

    // XXX: Type are these codes ok?
    pub const MEDIA_PLAY     : Self = ScanCode(0x00AF); // 175
    pub const MEDIA_STOP     : Self = ScanCode(0x00B0); // 176
    pub const MEDIA_PREV     : Self = ScanCode(0x00B1); // 177
    pub const MEDIA_NEXT     : Self = ScanCode(0x00B2); // 178

    pub const F1             : Self = ScanCode(0x012C); // 300
    pub const F2             : Self = ScanCode(0x012D); // 301
    pub const F3             : Self = ScanCode(0x012E); // 302
    pub const F4             : Self = ScanCode(0x012F); // 303
    pub const F5             : Self = ScanCode(0x0130); // 304
    pub const F6             : Self = ScanCode(0x0131); // 305
    pub const F7             : Self = ScanCode(0x0132); // 306
    pub const F8             : Self = ScanCode(0x0133); // 307
    pub const F9             : Self = ScanCode(0x0134); // 308
    pub const F10            : Self = ScanCode(0x0135); // 309
    pub const F11            : Self = ScanCode(0x0136); // 310
    pub const F12            : Self = ScanCode(0x0137); // 311
    pub const F13            : Self = ScanCode(0x0138); // 312
    pub const F14            : Self = ScanCode(0x0139); // 313
    pub const F15            : Self = ScanCode(0x013A); // 314
    pub const F16            : Self = ScanCode(0x013B); // 315
    pub const F17            : Self = ScanCode(0x013C); // 316
    pub const F18            : Self = ScanCode(0x013D); // 317
    pub const F19            : Self = ScanCode(0x013E); // 318
}

impl ScanCode {
    pub fn is_modifier(&self) -> bool {
        return self.0 >= scan_code_range::MOD.start &&
               self.0 < scan_code_range::MOD.end;
    }

    pub fn as_map_event(&self) -> Option<::wm::keymap::MapCode> {
        use ::wm::keymap::{
            MapCode,
            map_event_range,
        };
        if self.is_modifier() {
            return Some(MapCode(
                {
                    use self::ScanCode;
                    match *self {
                        ScanCode::LEFT_CTRL  | ScanCode::RIGHT_CTRL  => { ScanCode::LEFT_CTRL },
                        ScanCode::LEFT_ALT   | ScanCode::RIGHT_ALT   => { ScanCode::LEFT_ALT },
                        ScanCode::LEFT_SHIFT | ScanCode::RIGHT_SHIFT => { ScanCode::LEFT_SHIFT },
                        ScanCode::LEFT_SUPER | ScanCode::RIGHT_SUPER => { ScanCode::LEFT_SUPER },
                        _ => {
                            unreachable!();
                        }
                    }
                }.0 - map_event_range::KB.start
            ));
        } else if self.0 >= scan_code_range::MAP.start &&
                  self.0 < scan_code_range::MAP.end
        {
            return Some(MapCode(self.0 - map_event_range::KB.start));
        } else {
            return None;
        }
    }
}

#[derive(Debug, Clone)]
pub enum WindowActionType {
    Resize(u32, u32),
    Close,
}

// --------------------------------------------------------------
// EventID structs

// #[derive(Debug)]
// pub struct PointerMotion {
//     // nothing yet!
// }

// #[derive(Debug)]
// pub struct PointerButton {
// }

#[derive(Debug, Clone)]
pub enum EventID {
    PointerMotion {
        // nothing
    },
    PointerButton {
        button: PointerButton,
        // true == press, false == release
        value: bool,
    },
    Keyboard {
        scan_code: ScanCode,
        value: bool,
        repeat: bool,
    },
    Window {
        action: WindowActionType,
    },
}

// --------------------------------------------------------------


/// The state of the event system at the time its pressed.
/// (pointer location and key modifiers).
#[derive(Debug, Clone)]
pub struct EventState {
    /// Window relative mouse coordinates ([0, 0] is bottom-left).
    pub co: [f64; 2],
    pub key_mod: KeyMod,

    /// All held keys.
    pub scan_keys: HashSet<ScanCode>,
    /// All keys help which can be mapped
    /// The result of 'scan_keys[..].to_map_code()'
    ///
    /// Using this we can simply do set-compare for keymap checks.
    /// With scan-codes its more involved (left/right shift are both shift for eg).
    pub map_keys: HashSet<::wm::keymap::MapCode>,
    // TODO, tablet pressure.
}

impl Default for EventState {
    fn default() -> Self {
        EventState {
            co: [0.0, 0.0],
            key_mod: KeyMod(0),
            scan_keys: HashSet::new(),
            map_keys: HashSet::new(),
        }
    }
}

impl EventState {
    pub fn update_map_keys(&mut self) {
        self.map_keys.clear();
        for k_scan in &self.scan_keys {
            if let Some(k_map) = k_scan.as_map_event() {
                self.map_keys.insert(k_map);
            }
        }
    }
}


// --------------------------------------------------------------

// Loosely based on Blender's WM_types.h wmEvent
#[derive(Debug)]
pub struct Event {
    /// Event code itself (short, is also in keymap).
    pub id: EventID,
    pub state: EventState,
    // maybe we want to do this???
    // pub time: SystemTime,
}

impl Event {
    pub fn new(id: EventID, state: EventState) -> Self {
        Event {
            id: id,
            state: state,
            // time: SystemTime::now(),
        }
    }
}
